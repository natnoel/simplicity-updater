﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simplicity_Updater
{
    class WorkflowStateMapper
    {
        const int COL_ORIGINAL = 0, COL_MAPPING = 1, COL_ORDER = 2;
        private string file;
        private List<List<List<string>>> table;
        public HashSet<string> allStates;
        private int currRowNo;

        public WorkflowStateMapper()
        {
            //Console.WriteLine("Mapper created");
            currRowNo = -1;
        }

        public WorkflowStateMapper(string file)
        {
            currRowNo = -1;
            readStates(file);
        }

        public void readStates(String file)
        {
            this.file = file;

            string[] lines = System.IO.File.ReadAllLines(file);

            table = new List<List<List<string>>>();
            allStates = new HashSet<string>();
            foreach (string line in lines.Skip(1))
            {
                string[] col = line.Split(',');

                //Check for correct number of columns
                if (col.Count() != 3)
                    throw new Exception("Error reading states.csv: Number of columns not equal to 3");
                
                //Store the table
                List<List<string>> row = new List<List<string>>();
                foreach (string c in col)
                {
                    string[] statePatterns = c.Trim().Split(' ');
                    row.Add(new List<string>(statePatterns));
                }
                addToAllStates(row[COL_ORDER]);
                table.Add(row);
            }
            /*foreach (string state in allStates)
                Console.Write(state + " ");
            Console.WriteLine();*/
            validateTable(table);
        }

        void addToAllStates(List<string> states)
        {
            foreach (string state in states)
                allStates.Add(state);
        }

        private void validateTable(List<List<List<string>>> table)
        {
            for (int i = 0; i < table.Count; i++)
            {
                //Original and Order must be distinct
                if (table[i][COL_ORIGINAL].Distinct().Count() != table[i][COL_ORIGINAL].Count())
                    throw new Exception("Table row " + i + " original has duplicate states");
                if (table[i][COL_ORDER].Distinct().Count() != table[i][COL_ORDER].Count())
                    throw new Exception("Table row " + i + " original has duplicate states");

                //Number of states in Original and Mapping must be the same
                if (table[i][COL_ORIGINAL].Count() != table[i][COL_MAPPING].Count())
                {
                    //Console.WriteLine(String.Join(" ", table[i][COL_ORIGINAL]));
                    //Console.WriteLine(String.Join(" ", table[i][COL_MAPPING]));
                    throw new Exception("Table row " + i + " original and mapping have different lengths " + table[i][COL_ORIGINAL].Count() + " and " + table[i][COL_MAPPING].Count());
                }

            }
        }

        public int matchPattern(List<string> states)
        {
            states.Sort();
            for (int i = 0; i < table.Count; i++)
            {
                //Console.WriteLine("Checking...");
                //Console.WriteLine(String.Join(" ", states));
                //Console.WriteLine(String.Join(" ", table[i][COL_ORIGINAL]));
                if (states.SequenceEqual(table[i][COL_ORIGINAL]))
                    return i;
            }
            return currRowNo = -1;
        }

        public bool matchPattern(List<string> states, ref string stateNames, ref string codes)
        {
            if ((currRowNo = matchPattern(states)) == -1)
                return false;

            stateNames = getSimplifiedWorkflowStateNames(currRowNo);
            codes = getSimplifiedWorkflowStateCodes(currRowNo);
            return true;
        }

        public string getSimplifiedWorkflowStateNames(int rowNo)
        {
            return String.Join(",", table[rowNo][COL_ORDER]);
        }

        public string getSimplifiedWorkflowStateCodes(int rowNo)
        {
            List<string> codes = new List<string>();
            for (int i = 0; i < table[rowNo][COL_ORDER].Count(); i++)
            {
                if (table[rowNo][COL_ORDER][i].EndsWith("_X"))
                    codes.Add("X");
                else
                    codes.Add((i+1).ToString());
            }

            return String.Join(",", codes);
        }

        public string mapStateToCode(string state)
        {
            int idx = table[currRowNo][COL_ORIGINAL].IndexOf(state);
            string simplifiedState = table[currRowNo][COL_MAPPING][idx];
            if (simplifiedState.EndsWith("_X"))
                return "X";
            return (table[currRowNo][COL_ORDER].IndexOf(simplifiedState) + 1).ToString();
        }
    }
}
