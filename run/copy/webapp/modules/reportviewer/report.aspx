<%@ Page Language="C#" Theme="Corporate" Inherits="PageBase" Culture="auto" meta:resourcekey="PageResource2"
    UICulture="auto" %>

<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Collections" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="Anacle.DataFramework" %>
<%@ Import Namespace="LogicLayer" %>
<%@ Import Namespace="System.Reflection" %>
<%@ Import Namespace="System.Resources" %>
<%@ Import Namespace="System.Web" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

    Hashtable reportColumnMappings = new Hashtable();
    Object ClickedCellContent = null;
    string UniqueID = string.Empty;

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (!IsPostBack)
        {
            UniqueID = Security.Decrypt(Request["U"]);

            ReportObject.ChartVirtualPath = "~/components/chartdisplay.aspx" + "?U=" + HttpUtility.UrlEncode(Security.Encrypt(UniqueID));
            ReportObject.UniqueKey = UniqueID;

            OReport report = TablesLogic.tReport.Load((Guid)Session["ReportID" + UniqueID]);
            foreach (OReportColumnMapping reportColumnMapping in report.ReportColumnMappings)
                reportColumnMappings[reportColumnMapping.ColumnName] = reportColumnMapping;

            // Removes the drag drop report chart from the disk everytime
            // a new report is generated.
            //
            DiskCache.Remove("DragDropReportChart" + UniqueID);

            //Rachel. Set visible column at start based on the report setting
            if (Request["VisibleColumnAtStart"] != null)
                ReportObject.VisibleColumnsAtStart = Convert.ToInt32(Security.Decrypt(Request["VisibleColumnAtStart"]).ToString());

            //must set the report datasource first before deserializing the report setting. 
            if (DiskCache.GetValue("ReportDataSourceType" + UniqueID) == "DataTable")
            {
                if (DiskCache.ContainsKey("ReportDataSource" + UniqueID))
                {
                    ArrayList al_Columns = new ArrayList();

                    DataTable dt = DiskCache.GetDataTable("ReportDataSource" + UniqueID);
                    int totalColumns = dt.Columns.Count;

                    foreach (OReportColumnLinking links in report.ReportColumnLinkings)
                        if (!al_Columns.Contains(links.ColumnName))
                        {
                            al_Columns.Add(links.ColumnName);
                        }

                    for (int i = 0; i < totalColumns; i++)
                    {
                        string clName = dt.Columns[i].ColumnName;

                        OReportColumnMapping column = reportColumnMappings[clName] as OReportColumnMapping;

                        if (column != null && dt.Columns[i].DataType == typeof(String)
                            && column.ResourceName != null && column.ResourceName.Trim() != "")
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                string resourceAssemblyName = column.ResourceAssemblyName;
                                Assembly asm;
                                if (resourceAssemblyName.Trim() == "")
                                    asm = Assembly.Load("App_GlobalResources");
                                else
                                    asm = Assembly.Load("resourceAssemblyName");

                                if (asm != null)
                                {
                                    ResourceManager rm = new ResourceManager(column.ResourceName, asm);
                                    if (rm != null && dr[clName] != null && dr[clName].ToString() != "")
                                    {
                                        string a = dr[clName].ToString();
                                        string translatedText = rm.GetString(a, System.Threading.Thread.CurrentThread.CurrentUICulture);
                                        if (translatedText != null && translatedText != "")
                                            dr[clName] = translatedText;
                                    }
                                }
                            }
                        }
                    }

                    //ReportObject.BindData(dt);
                    ReportObject.BindData(dt, al_Columns);
                }
            }

            if (Session["ReportCaption" + UniqueID] != null)
                ReportObject.ReportCaption = (string)Session["ReportCaption" + UniqueID];

            if (Session["ReportParametersDisplay" + UniqueID] != null)
                ReportObject.ReportParameters = (ArrayList)Session["ReportParametersDisplay" + UniqueID];

            ReportObject.ReportGeneratedBy = GetLocalResourceObject("GeneratedBy").ToString() + ": " + Audit.UserName;
            ReportObject.ReportGeneratedDate = GetLocalResourceObject("GeneratedDate").ToString() + ": " + DateTime.Now.ToString("dd-MMM-yyyy HH:mm");
            SetTemplatePanelDisplay();
        }

    }
    protected void SetTemplatePanelDisplay()
    {
        UniqueID = Security.Decrypt(Request["U"]);

        //check whether the Template is editable by the current user
        if (Session["ReportTemplateID" + UniqueID] != null)
        {
            Guid templateID = new Guid(Session["ReportTemplateID" + UniqueID].ToString());
            if (OReportTemplate.IsEditableTemplate(templateID, (Guid)AppSession.User.ObjectID))
            {
                this.SaveDialogBox.Button1Visible = true;
            }
            else
            {
                this.SaveDialogBox.Button1Visible = false;
            }

            //populate the report column setting based on the report                
            OReportTemplate.DisplayReportTemplate(ReportObject, templateID);
            //populate template content
            OReportTemplate template = TablesLogic.tReportTemplate[templateID];
            this.TemplateName.Text = template.ObjectName;
            this.AccessControl.SelectedValue = template.AccessControl == null ? "" : template.AccessControl.ToString();
            this.TemplateDescription.Text = template.Description;

        }
        //new template
        else
        {
            this.SaveDialogBox.Button1Visible = false;
        }


        // Sets the header text of the individual columns if they have
        // a corresponding ReportColumnMapping object set up in 
        // the report page. This basically translates the column name
        // into the display text based on the user's current language
        // settings.
        //
        // If there is no mapping for that column, then we just use
        // the column name as the header text.
        // 
        OReport report = TablesLogic.tReport.Load((Guid)Session["ReportID" + UniqueID]);
        string id = Session["ReportID" + UniqueID].ToString();
        foreach (DragDropGridColumn col in ReportObject.Columns)
        {
            col.HeaderText = col.ColumnName;
            OReportColumnMapping mapping = reportColumnMappings[col.ColumnName] as OReportColumnMapping;
            if (mapping != null)
            {
                col.HeaderText = mapping.HeaderText;
                if (mapping.DataFormatString != null)
                    col.DataFormatString = mapping.DataFormatString;
            }
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        tableMessage.Visible = labelMessage.Text.Trim() != "";
    }


    protected void SaveReportTemplate(object sender, EventArgs e)
    {
        try
        {
            UniqueID = Security.Decrypt(Request["U"]);

            //edit
            OReportTemplate template = TablesLogic.tReportTemplate[new Guid(Session["ReportTemplateID" + UniqueID].ToString())];
            BindToReportTemplateObject(template);
            template.ObjectName = this.TemplateName.Text;
            template.AccessControl = Convert.ToInt32(this.AccessControl.SelectedValue);
            template.Description = this.TemplateDescription.Text;
            template.ReportID = (Guid)Session["ReportID" + UniqueID];
            int validate = OReportTemplate.SaveTemplateObject(template, ReportObject);
            if (validate == 1)
                this.labelMessage.Text = Resources.Errors.General_NameDuplicate;
            else
                this.labelMessage.Text = Resources.Messages.General_ItemSaved;
            //refresh report object.
            SetTemplatePanelDisplay();
            Window.Opener.ClickUIButton("RefreshLayoutButton");
        }
        catch (Exception ex)
        {
            //throw ex;
        }
        panelSaveTemplate.Update();
        SaveDialogBox.Show();
    }
    protected void BindToReportTemplateObject(OReportTemplate template)
    {
        UniqueID = Security.Decrypt(Request["U"]);

        template.ObjectName = this.TemplateName.Text;
        template.AccessControl = Convert.ToInt32(this.AccessControl.SelectedValue);
        template.Description = this.TemplateDescription.Text;
        template.ReportID = (Guid)Session["ReportID" + UniqueID];
        if (template.IsNew)
            template.CreatorID = AppSession.User.ObjectID;
    }
    protected void SaveNewReportTemplate(object sender, EventArgs e)
    {
        try
        {
            OReportTemplate template = TablesLogic.tReportTemplate.Create();
            BindToReportTemplateObject(template);
            int validate = OReportTemplate.SaveTemplateObject(template, ReportObject);
            if (validate == 1)
                this.labelMessage.Text = Resources.Errors.General_NameDuplicate;
            else
            {
                //set the template sessionID to the new template ID
                Session["ReportTemplateID" + UniqueID] = template.ObjectID;
                //refresh report object
                SetTemplatePanelDisplay();
                this.labelMessage.Text = Resources.Messages.General_ItemSaved;
            }
            this.TemplatePanel.Style["display"] = "";
            Window.Opener.ClickUIButton("RefreshLayoutButton");
        }
        catch (Exception ex)
        {
            //throw ex;
        }
        panelSaveTemplate.Update();
        SaveDialogBox.Show();
    }


    /// <summary>
    /// Pops up a dialog box for the user to select the report to 
    /// drill-down to.
    /// </summary>
    /// <param name="columnName"></param>
    public void ShowReportOptions(string columnName)
    {
        UniqueID = Security.Decrypt(Request["U"]);

        // Construct a list of all reports accessible
        // by the current logged on user.
        //
        DataTable dt = OReport.GetAllReportsByUserAndCategoryName(AppSession.User);
        List<Guid> reportIDsAccessibleByUser = new List<Guid>();
        foreach (DataRow dr in dt.Rows)
            reportIDsAccessibleByUser.Add((Guid)dr["ObjectID"]);

        // Show the list of reports that can be drilled down to.
        // 
        DataTable dt_LinkedReports = TablesLogic.tReportColumnLinking.SelectDistinct(
            TablesLogic.tReportColumnLinking.LinkedReport.ReportName,
            TablesLogic.tReportColumnLinking.ObjectID)
            .Where(
            TablesLogic.tReportColumnLinking.ReportID == (Guid)Session["ReportID" + UniqueID] &
            TablesLogic.tReportColumnLinking.IsDeleted == 0 &
            TablesLogic.tReportColumnLinking.ColumnName == columnName &
            TablesLogic.tReportColumnLinking.LinkedReportID.In(reportIDsAccessibleByUser))
            .OrderBy(
            TablesLogic.tReportColumnLinking.LinkedReport.ReportName.Asc);

        foreach (DataRow dr in dt_LinkedReports.Rows)
        {
            string text = Resources.Reports.ResourceManager.GetString(dr["ReportName"].ToString());
            if (text != null && text != "")
                dr["ReportName"] = text;
        }

        ddl_LinkedReportID.Bind(dt_LinkedReports, "ReportName", "ObjectID");
        if (ddl_LinkedReportID.Items.Count == 2)
        {
            ddl_LinkedReportID.SelectedIndex = 1;
            ButtonClickedEventArgs args = new ButtonClickedEventArgs();
            args.CommandName = "Open";
            ReportDrillDownDialogBox_ButtonClicked(null, args);
        }
        else
        {
            ReportDrillDownDialogBox.Show();
            op_ReportDrillDown.ClearErrorMessages();
        }
    }

    protected void ReportDrillDownDialogBox_ButtonClicked(object sender, ButtonClickedEventArgs e)
    {
        if (e.CommandName == "Open")
        {
            if (!op_ReportDrillDown.IsValid)
                return;

            // Call another report
            //
            Guid reportColumnLinkId = new Guid(ddl_LinkedReportID.SelectedValue);
            string newUniqueId = Guid.NewGuid().ToString().Replace("-", "");
            OReportColumnLinking l = TablesLogic.tReportColumnLinking.Load(reportColumnLinkId);

            if (l != null)
            {
                Window.Open("../reportviewer/search.aspx?ID=" + HttpUtility.UrlEncode(Security.Encrypt(l.LinkedReportID.Value.ToString())) +
                            "&RCLID=" + HttpUtility.UrlEncode(Security.Encrypt(l.ObjectID.Value.ToString())) +
                            "&OUID=" + HttpUtility.UrlEncode(Request["U"]) +
                            "&NUID=" + HttpUtility.UrlEncode(Security.Encrypt(newUniqueId)),
                            "AnacleEAM_Report" + newUniqueId);
            }
        }
        if (e.CommandName == "Cancel")
        {
        }
    }


    /// <summary>
    /// Occurs when the user drills down by clicking a link on the report.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ReportObject_DrillDown(object sender, DrillDownEventArgs e)
    {
        UniqueID = Security.Decrypt(Request["U"]);

        DiskCache.Add("drillrow_" + UniqueID, e.DataRow.Table);

        ShowReportOptions(e.ColumnName);
    }

    protected void ReportObject_ExportEvent(object sender, ExportEventArgs e)
    {
        if (e.Type == ExportType.XLSX || e.Type == ExportType.XLS)
            foreach (int i in e.DataColIndexes)
                e.DataTable.Columns[i].ExtendedProperties["DataFormat"] = ((string)e.DataTable.Columns[i].ExtendedProperties["DataFormat"]).Replace("{0:", "").Replace("}", "");

        switch (e.Type)
        {
            case ExportType.XLS:
                processExcel(e);
                break;
            case ExportType.XLSX:
                processExcelX(e);
                break;
            case ExportType.CSV:
                processCSV(e);
                break;
            case ExportType.PDF:
                processPDF(e);
                break;
        }
    }

    private void processExcel(ExportEventArgs e)
    {
        MemoryStream stream = new MemoryStream();
        OReport.ExportExcel(e.DataTable, e.GridView, e.DataColIndexes,
            ReportObject.ReportCaption, ReportObject.ReportGeneratedBy, ReportObject.ReportGeneratedDate, ReportObject.ReportParameters).Write(stream);
        this.Page.Response.ClearHeaders();
        this.Page.Response.AddHeader("content-disposition", "attachment; filename=" + HttpUtility.UrlEncode(System.Text.UTF8Encoding.UTF8.GetBytes(ReportObject.ReportCaption)) + ".xls");
        this.Page.Response.AddHeader("pragma", "public");
        this.Page.Response.ContentType = "application/vnd.ms-excel";
        this.Page.Response.OutputStream.Write(stream.ToArray(), 0, stream.ToArray().Length);
        this.Page.Response.End();
        stream.Close();
    }

    private void processExcelX(ExportEventArgs e)
    {
        MemoryStream stream = new MemoryStream();
        OReport.ExportExcelX(e.DataTable, e.GridView, e.DataColIndexes,
            ReportObject.ReportCaption, ReportObject.ReportGeneratedBy, ReportObject.ReportGeneratedDate, ReportObject.ReportParameters).Write(stream);
        this.Page.Response.ClearHeaders();
        this.Page.Response.AddHeader("content-disposition", "attachment; filename=" + HttpUtility.UrlEncode(System.Text.UTF8Encoding.UTF8.GetBytes(ReportObject.ReportCaption)) + ".xlsx");
        this.Page.Response.AddHeader("pragma", "public");
        this.Page.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        this.Page.Response.OutputStream.Write(stream.ToArray(), 0, stream.ToArray().Length);
        this.Page.Response.End();
        stream.Close();
    }

    private void processCSV(ExportEventArgs e)
    {
        MemoryStream stream = new MemoryStream();
        OReport.GenerateCSV(e.GridView, e.DataColIndexes, stream);
        this.Page.Response.ClearHeaders();
        this.Page.Response.AddHeader("content-disposition", "attachment; filename=" + HttpUtility.UrlEncode(System.Text.UTF8Encoding.UTF8.GetBytes(ReportObject.ReportCaption)) + ".csv");
        this.Page.Response.AddHeader("pragma", "public");
        this.Page.Response.ContentType = "text/csv";
        this.Page.Response.OutputStream.Write(stream.ToArray(), 0, stream.ToArray().Length);
        this.Page.Response.End();
        stream.Close();
    }

    private void processPDF(ExportEventArgs e)
    {
        //save output html to file
        string filePath = ConfigurationManager.AppSettings["ReportTempFolder"] + Guid.NewGuid().ToString() + ".htm";
        StreamWriter fileWriter;
        if (File.Exists(filePath))
            fileWriter = File.AppendText(filePath);
        else
            fileWriter = File.CreateText(filePath);
        try
        {
            fileWriter.Write(e.HTML);
        }
        finally
        {
            fileWriter.Close();
        }

        //convert to PDF
        //string pdfURL = DocumentGenerator.WKHtmlToPdf(filePath, PrintingOrientation.Landscape);
        string pdfURL = Helpers.HtmlUrlToPdf(filePath);
        File.Delete(filePath);

        this.Page.Response.ClearHeaders();
        this.Page.Response.WriteFile(pdfURL);
        this.Page.Response.AddHeader("content-disposition", "attachment; filename=" + System.Text.ASCIIEncoding.ASCII.GetString(System.Text.UTF8Encoding.UTF8.GetBytes(ReportObject.ReportCaption)) + ".pdf");
        this.Page.Response.AddHeader("pragma", "public");
        this.Page.Response.ContentType = "application/pdf";
        this.Page.Response.End();
    }

    protected void ReportObject_Load(object sender, EventArgs e)
    {
        //SaveDialogBox.Hide();
    }

    protected void ReportObject_SaveLayoutClicked(object sender, ButtonClickedEventArgs e)
    {
        SaveDialogBox.Show();
    }

    protected void SaveDialogBox_ButtonClicked(object sender, ButtonClickedEventArgs e)
    {
        if (e.CommandName == "SaveChanges")
        {
            SaveReportTemplate(sender, e);
        }
        else if (e.CommandName == "SaveNewLayout")
        {
            SaveNewReportTemplate(sender, e);
        }
        else if (e.CommandName == "Cancel")
        {
            SaveDialogBox.Hide();
        }
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Anacle.EAM</title>
    <meta http-equiv="pragma" content="no-cache" />
    <link href="../../css/v7-dragdrop.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">

    <style>
        #buttOnValidateAndSave, #buttOnValidateAndSaveNew, #buttonCancel 
        {
            background-color: #eeeeee;
            border: 1px solid gray;
        }

        #myDiv
        {
            width: 600px;
            height: 600px;
        }

    </style>

</head>
<body>
    <script type='text/javascript'>
        window.focus();

    </script>
    <form id="form2" runat="server">
        <ui:UIPanel runat="server" ID="panel" Width="100%">
            <ui:UIDialogBox runat="server" ID="SaveDialogBox" Title="Save layout" CloseButtonVisible="False"  OnButtonClicked="SaveDialogBox_ButtonClicked"
                    Button1Text="Save Changes" Button1ConfirmText="Save changes?" Button1ButtonColor="Blue" Button1ButtonType="NormalRaised" Button1CommandName="SaveChanges"
                    Button2Text="Save as new layout" Button2ButtonColor="Blue" Button2ButtonType="NormalRaised" Button2CommandName="SaveNewLayout"
                    Button3Text="Cancel" Button3CausesValidation="false" Button3CommandName="Cancel" >
                <div id="TemplatePanel" runat="server">
                <ui:UIPanel runat="server" ID="panelSaveTemplate">
                        <div id="tableMessage" class='object-message' style="width: 100%" runat="server">
                            <asp:Label runat='server' ID='labelMessage' ForeColor="Red" meta:resourcekey="labelMessageResource1"></asp:Label>
                        </div>
                        <div class="div-form-short">
                            <ui:UIFieldTextBox runat="server" ID="TemplateName" ValidateRequiredField="true"
                                Caption="Layout Name" Span="full" ToolTip="Name of the report layout" MaxLength="100">
                            </ui:UIFieldTextBox>
                            <ui:UIFieldTextBox runat="server" ID="TemplateDescription" Caption="Description"
                                MaxLength="255"
                                Rows="2" TextMode="MultiLine" Span="full" ToolTip="Description of the report layout">
                            </ui:UIFieldTextBox>
                            <ui:UIFieldRadioList runat="server" ID="AccessControl" Caption="Access Control" Span="full"
                                RepeatDirection="vertical" ToolTip="Set whether the layout could be edited by other users or just by the layout creator">
                                <Items>
                                    <asp:ListItem Value="1" Selected="True">Editable by all users</asp:ListItem>
                                    <asp:ListItem Value="2">Editable by me and viewable by all users</asp:ListItem>
                                    <asp:ListItem Value="3">Editable and viewable by me only</asp:ListItem>
                                </Items>
                            </ui:UIFieldRadioList>
                        </div>
                </ui:UIPanel>
            </div>
            </ui:UIDialogBox>

            <ui:UIDragDropGrid ID="ReportObject" runat="server" ImagePath="../../images/" ChartVirtualPath="~/components/chartdisplay.aspx" ItemsPerPage="50"
                OnDrillDown="ReportObject_DrillDown" OnExportEvent="ReportObject_ExportEvent" OnLoad="ReportObject_Load" OnSaveLayoutClicked="ReportObject_SaveLayoutClicked">

            </ui:UIDragDropGrid>
            <ui:UIDialogBox runat="server" ID="ReportDrillDownDialogBox" Title="Report Drill Down"
                Button1Text="Open" Button1CommandName="Open" Button2Text="Cancel" Button2CommandName="Cancel"
                OnButtonClicked="ReportDrillDownDialogBox_ButtonClicked" DialogWidth="700px">
                <ui:UIObjectPanel runat="server" ID="op_ReportDrillDown" BackColor="White" CssClass="div-edit">
                    <ui:UIFieldDropDownList ID="ddl_LinkedReportID" runat="server" Caption="Select Report"
                        PropertyName="LinkedReportID"
                        ValidateRequiredField="True">
                    </ui:UIFieldDropDownList>
                </ui:UIObjectPanel>
            </ui:UIDialogBox>
        </ui:UIPanel>

        <script src='../../scripts/boxover.js' type='text/javascript'></script>

    </form>
</body>
</html>
