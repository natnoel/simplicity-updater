﻿<%@ Page Language="C#" Inherits="PageBase" %>
<%@ Import Namespace="System.Data" %>

<!DOCTYPE html>

<script runat="server">
    /// <summary>
    /// Translates the specified text from the objects.resx file.
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    protected string TranslateMenuItem(string text)
    {
        if (text == "##Reports##")
            return Resources.Strings.Menu_Reports;

        string translatedText = Resources.Objects.ResourceManager.GetString(text);
        if (translatedText == null || translatedText == "")
            return text;
        return translatedText;
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        if (!IsPostBack)
        {
            string query = Request["QUERY"];
            if (query == null)
                query = "";
            string[] array = query.Split(':');

            array[0] = array[0].Trim().ToLower();

            if (array.Length > 1)
            {
                // Searching by functions
                //
                DataTable dt = Helpers.GetCachedMenusAccessibleByUser(AppSession.User);
                foreach (DataRow dr in dt.Rows)
                {
                    string functionName = (dr["FunctionCode"] == DBNull.Value ? "" : dr["FunctionCode"].ToString()) + " " +
                        TranslateMenuItem(dr["FunctionName"].ToString());

                    if (array[0] == functionName.Trim().ToLower())
                    {
                        string mainUrl = this.ResolveUrl(dr["MainUrl"].ToString());

                        mainUrl = mainUrl + (mainUrl.Contains("?") ? "&" : "?") +
                        "TYPE=" + HttpUtility.UrlEncode(Security.Encrypt(dr["ObjectTypeName"].ToString() + ":" + AppSession.SaltID)) +
                        "&S=" + HttpUtility.UrlEncode(array[1]);

                        Window.Open(mainUrl, "frameBottom");
                        return;
                    }
                }

                // Searching by reports.
                //
                DataTable dtReports = Helpers.GetCachedReportsAccessibleByUser(AppSession.User);
                foreach (DataRow dr in dtReports.Rows)
                {
                    string reportName = (dr["ReportCode"] == DBNull.Value ? "" : dr["ReportCode"].ToString()) + " " +
                        TranslateMenuItem(dr["ReportName"].ToString());

                    if (array[0] == reportName.Trim().ToLower())
                    {
                        string reportUrl =  this.ResolveUrl("~/modules/reportviewer/search.aspx") + "?ID=" +
                            HttpUtility.UrlEncode(Security.Encrypt(dr["ObjectID"].ToString() + ":" + AppSession.SaltID));

                        Window.Open(reportUrl, "frameBottom");
                        return;
                    }
                }
            }

            // Doesn't match any of the function names, so we will just search in the current module.
            //
            string searchUrl = this.ResolveUrl("~/home.aspx");
            if (this.Session["CurrentSearchPath"] != null)
                searchUrl = this.Session["CurrentSearchPath"].ToString();
            Window.Open(searchUrl + (searchUrl.Contains("?") ? "&" : "?") + "S=" + HttpUtility.UrlEncode(array[0]), "frameBottom");
        }
    }

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>
</body>
</html>
