<%@ Page Language="C#" Theme="Corporate" Inherits="PageBase" %>

<%@ Register Src="components/menu.ascx" TagPrefix="web" TagName="menu" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.Common" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="System.Configuration" %>
<%@ Import Namespace="System.Collections" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="Anacle.DataFramework" %>
<%@ Import Namespace="LogicLayer" %>

<script runat="server">
    
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (!IsPostBack)
        {
            //20130304 PTB: Use App Setting Home URL
            DataTable createableAnnouncements = OAnnouncement.GetAnnouncementsTable(AppSession.User, DateTime.Now);
            HyperLink val = (HyperLink)Page.FindControl("LinkToAnnouncements");
            gridAnnouncements.Bind(createableAnnouncements);
            gridAnnouncements.CheckBoxColumnVisible = false;

            // Saves the inbox path into Session to support global search.
            // 
            this.Session["CurrentSearchPath"] = this.Page.Request.ApplicationPath + "/home.aspx";
        }

    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        // Force some default settings for the gridview.
        //
        this.gridAnnouncements.ShowCaption = false;
        if (this.gridAnnouncements.TopPagerRow != null)
            PadCells(this.gridAnnouncements.TopPagerRow, true);
        if (this.gridAnnouncements.HeaderRow != null)
            PadCells(this.gridAnnouncements.HeaderRow, false);
        foreach (GridViewRow row in this.gridAnnouncements.Rows)
            PadCells(row, false);
        if (this.gridAnnouncements.FooterRow != null)
            PadCells(this.gridAnnouncements.FooterRow, false);
        if (this.gridAnnouncements.BottomPagerRow != null)
            PadCells(this.gridAnnouncements.BottomPagerRow, true);
    }

    protected void PadCells(GridViewRow row, bool pagerRow)
    {
        int startColumn = 0;
        if (!this.gridAnnouncements.CheckBoxColumnVisible && !pagerRow)
            startColumn = 1;
        for (int i = startColumn; i < row.Cells.Count; i++)
        {
            if (row.Cells[i].Visible || pagerRow)
            {
                row.Cells[i].Style["padding-left"] = "40px";
                break;
            }
        }

        for (int i = row.Cells.Count - 1; i >= 0; i--)
        {
            if (row.Cells[i].Visible || pagerRow)
            {
                row.Cells[i].Style["padding-right"] = "40px";
                break;
            }
        }
    }



</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Anacle.EAM</title>
</head>
<body class="body-home-bg">
    <div class="mdl-title-bar-spacer">
        <div class="mdl-title-bar">
            <div>
                <div style="float:left">
                    <asp:label runat="server" ID="titleLabel" CssClass="mdl-title-bar-label" Text="Announcements"></asp:label>
                </div>
            </div>
        </div>
    </div>
    <form id="form1" runat="server">
        <div class="div-main">
        <ui:UIPanel runat="server" ID="panelAnnouncements" BorderStyle="NotSet" meta:resourcekey="panelAnnouncementsResource" >
            <ui:UIGridView runat="server" ID="gridAnnouncements" SortExpression="PriorityFlag desc, ScheduledStartDateTime desc"
                ShowCaption="false" KeyName="ObjectID" MouseOutColor="#FFFF80" DataKeyNames="ObjectID"
                GridLines="Both" RowErrorColor="" Style="clear: both;" ShowCommands='false' ImageRowErrorUrl="" meta:resourcekey="gridAnnouncementsResource" >
                <RowStyle VerticalAlign="Top" />
                <AlternatingRowStyle VerticalAlign="Top" />
                <PagerSettings Mode="NextPreviousFirstLast" FirstPageText="First" LastPageText="Last"
                    Position="Bottom" />
                <Columns>
                    <ui:UIGridViewBoundColumn PropertyName="CreatedDatetime" DataField="CreatedDatetime"
                        HeaderText="Created Date" DataFormatString="{0:dd-MMM-yyyy}" SortExpression="CreatedDatetime" meta:resourcekey="gridAnnouncementsUIGridViewBoundColumnCreatedDatetimeResource" >
                        <HeaderStyle HorizontalAlign="Left" Width="150px" />
                        <ItemStyle HorizontalAlign="Left" />
                    </ui:UIGridViewBoundColumn>
                    <ui:UIGridViewBoundColumn PropertyName="Announcement" DataField="Announcement" HeaderText="Announcement"
                        SortExpression="Announcement" meta:resourcekey="gridAnnouncementsUIGridViewBoundColumnAnnouncementResource" >
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" />
                    </ui:UIGridViewBoundColumn>
                </Columns>
            </ui:UIGridView>
        </ui:UIPanel>
        </div>
    </form>
</body>
</html>
