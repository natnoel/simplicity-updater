<%@ Page Language="C#" Theme="Corporate" CodeFile="~/apploginupdate.aspx.cs" Inherits="apploginupdate"
    UICulture="auto" Culture="auto" meta:resourcekey="PageResource1" %>

<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="Anacle.DataFramework" %>
<%@ Import Namespace="LogicLayer" %>

<script runat="server">
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        try
        {
            lblCopyright.Text = string.Format("COPYRIGHT &copy; {0} ANACLE SYSTEMS ALL RIGHTS RESERVED", DateTime.Today.Year);
        }
        catch (Exception)
        {
        }
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Anacle.EAM</title>
    <script type="text/javascript" src="scripts/rlogin.js"></script>
    <script type="text/javascript">
        window.onresize = function (event) {
            resize();
        };

        $(document).ready(function () {
            setTimeout(resize, 1);
            $("#body").css("min-height", $("#content").height());
            var scale = $(window).width() / 400;
            $("#viewport").attr("content", "width=400, user-scalable=no, initial-scale=" + scale + ", maximum-scale=" + scale + ", minimum-scale=" + scale);
            $("#block").fadeOut(500);

            $("#LogoImage").load(function () {
                setTimeout(resize, 1);
            });
        });

        function resize() {
            var hh = $("#header").height() + 20;
            var fh = $("#footer").height() + 20;
            var wh = $(window).height();
            $("#body").css("height", wh - hh - fh - 100);
            $("#content").css("top", (($("#body").height() - $("#content").height()) / 2) + "px");
        }
    </script>
</head>
<body style="padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px">
    <form id="form1" runat="server">
        <div id="header" style="padding: 10px 0; background-color:#000;">
            <table style="max-width: 800px; margin-left: auto; margin-right: auto;">
                <tr>
                    <td style="padding-left: 35px; width: 200px;">
                        <asp:Image runat="server" style="width:226px" ID="LogoImage" ImageAlign="Bottom" />
                    </td>
                    <td style="padding-left: 35px;">
                        <asp:Label ID="TitleText"  style="font-size: 30pt; color: white;" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div id="body" style="padding: 50px 0;">
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
                    <td runat="server" id="tableLoginCell" align="center">
                        <div style="padding: 12px 12px 12px 12px">
                            <table runat="server" id="tblNewPassword" cellpadding="0" border="0">
                                <tr>
                                    <td align="center" colspan="2">
                                        <h3>Please enter a new password.</h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <asp:Label runat="server" ID="lblNewPassword" Text="New Password:" />
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtNewPassword" TextMode="Password" MaxLength="50" />
                                        <asp:Label runat="server" ID="lblPasswordRequired" ToolTip="New Password is required."
                                            ForeColor="Red" Visible="false">*</asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <asp:Label runat="server" ID="lblConfirmNewPassword" Text="Confirm Password:" />
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtConfirmNewPassword" TextMode="Password" MaxLength="50" />
                                        <asp:Label runat="server" ID="lblConfirmNewPasswordRequired" ToolTip="Confirm New Password is required."
                                            ForeColor="Red" Visible="false">*</asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" colspan="2" style="color: Red;">
                                        <asp:Label runat="server" ID="lblPasswordError" EnableViewState="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" colspan="2">
                                        <asp:Button runat="server" ID="btnUpdatePassword" Text="Update & Proceed" OnClick="btnUpdatePassword_Click" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div id="footer" style="margin: 0 auto; text-align: center; width: 800px;
            background-color: #000; padding-top: 10px; padding-bottom: 10px; width: 100%;">
            <p style="color: white">
                    <asp:Label runat="server" ID="lblCopyright"></asp:Label>
            </p>
        </div>
    </form>
</body>
</html>
