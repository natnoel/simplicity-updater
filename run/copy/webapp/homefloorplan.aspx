﻿<%@ Page Language="C#" Theme="Corporate" Inherits="PageBase" Culture="auto" meta:resourcekey="PageResource1"
    UICulture="auto" %>

<%@ Import Namespace="System" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="System.Web.UI" %>
<%@ Import Namespace="System.Web.UI.WebControls" %>
<%@ Import Namespace="Anacle.DataFramework" %>
<%@ Import Namespace="LogicLayer" %>

<%@ Implements Interface="System.Web.UI.ICallbackEventHandler" %>
<%--<%@ Register Src="~/components/floorplanDashboard.ascx" TagPrefix="web" TagName="floorplanDashboard" %>--%>

<script runat="server">   
    protected string returnValue; 
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);


        //Update total announcements
        HyperLink announcements = (HyperLink)Page.FindControl("LinkToAnnouncements");
        announcements.Text = string.Format("{0} <sup>{1}</sup>", Resources.Strings.Home_Announcements, OAnnouncement.GetAnnouncementsTable(AppSession.User, DateTime.Now).Rows.Count.ToString());

        //Update total inbox items
        HyperLink tasks = (HyperLink)Page.FindControl("LinkToInbox");
        tasks.Text = string.Format("{0} <sup>{1}</sup>", Resources.Strings.Home_Inbox,
            OActivity.GetOutstandingActivitiesForInbox(
                    AppSession.User, new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 23, 59, 59), "%%", "%%", "%%").Rows.Count);



        //string type = Session["type"] == null ? "Task" : Session["type"].ToString();

        //string childID = Request["cid"];
        //string parentID = Request["pid"];

        List<OPosition> positions = AppSession.User.GetPositionsByObjectType("OLocation");
        List<OLocation> locations = OLocation.GetLocationsByType(LocationType.Building, false, positions, null);
        //locations.AddRange(OLocation.GetLocationsByType("Building", false, positions, null));

        if (!IsPostBack)
        {
            DropDownListSite.Bind(locations);
            DropDownListSite.SelectedValue = AppSession.SiteID.ToString();
            ClientScriptManager cm = Page.ClientScript;
            String cbReference = cm.GetCallbackEventReference(this, "arg", "ReceiveData", "", true);
            String callbackScript = "function Callback(arg, context) {" + cbReference + "; }";
            cm.RegisterClientScriptBlock(this.GetType(), "Callback", callbackScript, true);
        }

        //if (DropDownListClimateControl.Items.Count == 0)
        //       BindClimateControlList();


        if (DropDownListSpaceType.Items.Count == 0)
        {
            BindSpaceTypeList();
            //DropDownListSpaceType.SelectedIndex = 2;
        }

        if (DropDownListLevel.Items.Count == 0)
            BindFloorList(DropDownListSite.SelectedValue);
        //if(DropDownListSizeType.Items.Count == 0)
        //    BindSizeTypeList();
        if (DropDownListLeaseStatus.Items.Count == 0)
            BindLeaseStatus();
        //if (DropDownListOverLockStatus.Items.Count == 0)
        //    BindOverLockStatus();
    }

    public void RaiseCallbackEvent(String eventArgument)
    {
        DateTime dt = DateTime.ParseExact(eventArgument, "yyyy-MM-dd", null);
        //dynamic.InnerHtml = "";
        returnValue = GenerateStackingPlan(TablesLogic.tLocation.Load(new Guid(DropDownListSite.SelectedValue)), dt);
        //returnValue = "";
    }

    public String GetCallbackResult()
    {
        return returnValue;
    }
    
    private void BindFloorList(string id)
    {
        if (id == "")
            return;
        OLocation l = TablesLogic.tLocation.Load(new Guid(id));
        if (l != null)
        {
            List<OLocation> locs = TablesLogic.tLocation.LoadList(
            TablesLogic.tLocation.HierarchyPath.Like(l.HierarchyPath + "%") &
            TablesLogic.tLocation.LocationType.ObjectName == "Level"
            );
            DropDownListLevel.Bind(locs, "ObjectName", "ObjectID");
        }
    }



    private void BindSpaceTypeList()
    {

        //List<OLocationType> locs = TablesLogic.tLocationType.LoadList(
        //TablesLogic.tLocationType.ObjectName.In(SpaceType.SelfStorage,SpaceType.Commercial));

        DropDownListSpaceType.Bind(SpaceType.GetSpaceTypeDataTable(), "Value", "Type");

    }


    //private void BindClimateControlList()
    //{
    //    List<OCode> locs = TablesLogic.tCode.LoadList(
    //    TablesLogic.tCode.CodeType.ObjectName.Like("%Climate%"));

    //    DropDownListClimateControl.Bind(locs, "ObjectName", "ObjectID");
    //}

    //private void BindSizeTypeList()
    //{
    //    List<OCode> locs = TablesLogic.tCode.LoadList(
    //    TablesLogic.tCode.CodeType.ObjectName == "SizeType");

    //    DropDownListSizeType.Bind(locs, "ObjectName", "ObjectID");
    //}

    private void BindLeaseStatus()
    {
        //DropDownListLeaseStatus.Items.Add("");
        ////DropDownListLeaseStatus.Items.Add(LeaseStatus.Draft);
        ////DropDownListLeaseStatus.Items.Add(LeaseStatus.Cancelled);
        //DropDownListLeaseStatus.Items.Add(LeaseStatus.PendingExecution);
        //DropDownListLeaseStatus.Items.Add(LeaseStatus.Active);
        //DropDownListLeaseStatus.Items.Add(LeaseStatus.);
        //DropDownListLeaseStatus.Items.Add(LeaseStatus.SecondReminder);
        //DropDownListLeaseStatus.Items.Add(LeaseStatus.FinalReminder);
        //DropDownListLeaseStatus.Items.Add(LeaseStatus.TerminationNotice);
        //DropDownListLeaseStatus.Items.Add(LeaseStatus.OverLocked);
        //DropDownListLeaseStatus.Items.Add(LeaseStatus.PendingRenewalDocumentation);
        //DropDownListLeaseStatus.Items.Add(LeaseStatus.TerminatedPendingAuction);
        ////DropDownListLeaseStatus.Items.Add(LeaseStatus.Terminated);
        //DropDownListLeaseStatus.Items.Add(LeaseStatus.ExpiredPendingTermination);
        DropDownListLeaseStatus.Bind(LeaseStatus.GetLeaseStatusDataTable(), "Text", "Value");
    }

    private void BindOverLockStatus()
    {
        //DropDownListOverLockStatus.Items.Add("");
        //DropDownListOverLockStatus.Items.Add(LeaseOverlockStatus.NotOverlock);
        //DropDownListOverLockStatus.Items.Add(LeaseOverlockStatus.Overlocked);
        ////DropDownListOverLockStatus.Items.Add(LeaseOverlockStatus.PendingOverlock);
        ////DropDownListOverLockStatus.Items.Add(LeaseOverlockStatus.PendingUnlock);
        //DropDownListOverLockStatus.Items.Add("Pending Over-lock/Un-lock");
    }

    private List<OLease> generateTableForLease(List<OLocation> locationList)
    {
        List<OLease> leaseList = new List<OLease>();

        if (VacantOnly.Checked && !LeaseOnly.Checked)
            return leaseList;

        List<OLocation> locs = locationList.FindAll(p => p.LocationStatus != SpaceStatus.Vacant);

        ExpressionCondition ec = Query.True;

        if (DropDownListLeaseStatus.SelectedValue != "")
            ec = ec & TablesLogic.tLease.Status == DropDownListLeaseStatus.SelectedValue;

        //if (DropDownListOverLockStatus.SelectedValue == "Pending Over-lock/Un-lock")
        //    ec = ec & TablesLogic.tLease.OverlockStatus.In(LeaseOverlockStatus.PendingOverlock,LeaseOverlockStatus.PendingUnlock) ;
        //else if(DropDownListOverLockStatus.SelectedValue != "")
        //    ec = ec & TablesLogic.tLease.OverlockStatus == DropDownListOverLockStatus.SelectedValue;

        //foreach (OLocation loc in locs)
        //{

        //    OLease lease = TablesLogic.tLease.Load(TablesLogic.tLease.LeaseSpaceID == loc.ObjectID & TablesLogic.tLease.IsDeleted == 0 & ec);
        //    if (lease != null)
        //    {
        //        leaseList.Add(lease);
        //    }
        //}

        List<OLease> leaseList2 = TablesLogic.tLease.LoadList(TablesLogic.tLease.LeaseOfferSpaces.SpaceID.In(locs.Select(p => p.ObjectID)) & TablesLogic.tLease.IsDeleted == 0 & TablesLogic.tLease.Status != LeaseStatus.Terminated & ec);
        //leaseList2.Contains(null);
        return leaseList2;
    }
    private List<OLease> generateTableForLease(string id)
    {
        List<OLease> leaseList = new List<OLease>();
        if (VacantOnly.Checked && !LeaseOnly.Checked)
            return leaseList;
        OLocation l = TablesLogic.tLocation.Load(new Guid(id));

        ExpressionCondition ec = Query.True;

        if (DropDownListSpaceType.SelectedValue != "")
            //    //ec = ec & TablesLogic.tLocation.LocationType.ObjectName.In(SpaceType.Commercial,SpaceType.SelfStorage);
            //    ec = ec;
            //else
            ec = ec & TablesLogic.tLocation.LocationTypeID == new Guid(DropDownListSpaceType.SelectedValue);

        //if (DropDownListSizeType.SelectedValue != "")
        //    ec = ec & TablesLogic.tLocation.SizeTypeID == new Guid(DropDownListSizeType.SelectedValue);

        List<OLocation> locs =
                TablesLogic.tLocation.LoadList(TablesLogic.tLocation.HierarchyPath.Like(l.HierarchyPath + "%") & TablesLogic.tLocation.IsDeleted == 0
                & ec & TablesLogic.tLocation.LocationStatus != SpaceStatus.Vacant);

        ec = Query.True;

        if (DropDownListLeaseStatus.SelectedValue != "")
            ec = ec & TablesLogic.tLease.Status == DropDownListLeaseStatus.SelectedValue;

        //if (DropDownListOverLockStatus.SelectedValue == "Pending Over-lock/Un-lock")
        //    ec = ec & TablesLogic.tLease.OverlockStatus.In(LeaseOverlockStatus.PendingOverlock, LeaseOverlockStatus.PendingUnlock);
        //else if (DropDownListOverLockStatus.SelectedValue != "")
        //    ec = ec & TablesLogic.tLease.OverlockStatus == DropDownListOverLockStatus.SelectedValue;
        foreach (OLocation loc in locs)
        {

            OLease lease = TablesLogic.tLease.Load(TablesLogic.tLease.LeaseOfferSpaces.ObjectID == loc.ObjectID & TablesLogic.tLease.IsDeleted == 0 & ec);
            if (lease != null)
            {
                leaseList.Add(lease);
            }
        }

        return leaseList;
    }
    private List<OLocation> generateDataTableForLevel(List<OLocation> locationList, bool datalist)
    {
        return locationList.FindAll(p => p.LocationStatus.Is(SpaceStatus.Vacant, SpaceStatus.UnderMaintenance));
    }

    private DataTable generateDataTableForLevel(List<OLocation> locationList)
    {
        System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
        DataTable locationLevel = new DataTable();
        //for (int i = 0; i < 5; i++)
        //    locationLevel.Columns.Add(new DataColumn());

        //locationLevel.Columns[0].ColumnName = "ObjectID";
        //locationLevel.Columns[1].ColumnName = "UnitNo";
        //locationLevel.Columns[2].ColumnName = "SizeType";
        //locationLevel.Columns[3].ColumnName = "Size";
        //locationLevel.Columns[4].ColumnName = "AskingPrice";


        //if (locationList.Count > 0)
        //{
        //    List<OLocation> locs = locationList.FindAll(p => p.Status == SpaceStatus.Vacant);
        //    System.Diagnostics.Debug.WriteLine("Find All Vacant: "+sw.ElapsedMilliseconds);
        //    foreach (OLocation o in locs)
        //    {
        //        DataRow datarow = locationLevel.NewRow();
        //        datarow[0] = o.ObjectID.ToString();
        //        datarow[1] = o.ObjectName;
        //        if (o.SizeTypeID != null)
        //            datarow[2] = TablesLogic.tCode.Load(o.SizeTypeID).ObjectName;
        //        else
        //            datarow[2] = "N.A.";
        //        datarow[3] = string.Format("{0:#,###.00}", o.NLAFoot);
        //        datarow[4] = string.Format("{0:#,###.00}", o.MonthlyRentalRate);
        //        locationLevel.Rows.Add(datarow);
        //    }
        //    System.Diagnostics.Debug.WriteLine("Put into Datatable: " + sw.ElapsedMilliseconds);
        //}

        return locationLevel;
    }
    private DataTable generateDataTableForLevel(string id)
    {
        DataTable locationLevel = new DataTable();
        //for (int i = 0; i < 5; i++)
        //    locationLevel.Columns.Add(new DataColumn());

        //locationLevel.Columns[0].ColumnName = "ObjectID";
        //locationLevel.Columns[1].ColumnName = "UnitNo";
        //locationLevel.Columns[2].ColumnName = "SizeType";
        //locationLevel.Columns[3].ColumnName = "Size";
        //locationLevel.Columns[4].ColumnName = "AskingPrice";

        //OLocation l = TablesLogic.tLocation.Load(new Guid(id));
        //if (l != null)
        //{
        //    ExpressionCondition ec = Query.True;

        //    if (DropDownListSpaceType.SelectedValue == "")
        //        ec = ec & TablesLogic.tLocation.LocationType.ObjectName.In(SpaceType.SelfStorage,SpaceType.Commercial);
        //    else
        //        ec = ec & TablesLogic.tLocation.LocationTypeID == new Guid(DropDownListSpaceType.SelectedValue);

        //    //if (DropDownListSizeType.SelectedValue != "")
        //    //    ec = ec & TablesLogic.tLocation.SizeTypeID == new Guid(DropDownListSizeType.SelectedValue);

        //    List<OLocation> locs =
        //        TablesLogic.tLocation.LoadList(TablesLogic.tLocation.HierarchyPath.Like(l.HierarchyPath + "%") & TablesLogic.tLocation.IsDeleted == 0
        //        & TablesLogic.tLocation.Status == "Vacant" & ec);
        //    foreach (OLocation o in locs)
        //    {
        //        DataRow datarow = locationLevel.NewRow();
        //        datarow[0] = o.ObjectID.ToString();
        //        datarow[1] = o.ObjectName;
        //        if (o.SizeTypeID != null)
        //            datarow[2] = TablesLogic.tCode.Load(o.SizeTypeID).ObjectName;
        //        else
        //            datarow[2] = "N.A.";
        //        datarow[3] = string.Format("{0:#,###.00}", o.NLAFoot);
        //        datarow[4] = string.Format("{0:#,###.00}", o.MonthlyRentalRate);
        //        locationLevel.Rows.Add(datarow);
        //    }
        //}

        return locationLevel;
    }
    private DataTable generateDataTable(string id)
    {
        DataTable locationLevel = new DataTable();
        //for (int i = 0; i < 12; i++)
        //    locationLevel.Columns.Add(new DataColumn());

        //locationLevel.Columns[0].ColumnName = "ObjectID";
        //locationLevel.Columns[1].ColumnName = "level / Size Type";
        //locationLevel.Columns[2].ColumnName = "Locker";
        //locationLevel.Columns[3].ColumnName = "XX Small";
        //locationLevel.Columns[4].ColumnName = "X Small";
        //locationLevel.Columns[5].ColumnName = "Small";
        //locationLevel.Columns[6].ColumnName = "Medium";
        //locationLevel.Columns[7].ColumnName = "Large";
        //locationLevel.Columns[8].ColumnName = "X Large";
        //locationLevel.Columns[9].ColumnName = "XX Large";
        //locationLevel.Columns[10].ColumnName = "XXX Large";
        //locationLevel.Columns[11].ColumnName = "Total";

        //OLocation l = TablesLogic.tLocation.Load(new Guid(id));
        //if (l != null)
        //{
        //    List<OLocation> locs = TablesLogic.tLocation.LoadList(
        //    TablesLogic.tLocation.HierarchyPath.Like(l.HierarchyPath + "%") &
        //    TablesLogic.tLocation.LocationType.ObjectName == "Level" & TablesLogic.tLocation.IsDeleted == 0
        //    );
        //    OCode locker = TablesLogic.tCode.Load(TablesLogic.tCode.ObjectName == "Locker");
        //    OCode XXSmall = TablesLogic.tCode.Load(TablesLogic.tCode.ObjectName == "XX Small");
        //    OCode XSmall = TablesLogic.tCode.Load(TablesLogic.tCode.ObjectName == "X Small");
        //    OCode Small = TablesLogic.tCode.Load(TablesLogic.tCode.ObjectName == "Small");
        //    OCode Medium = TablesLogic.tCode.Load(TablesLogic.tCode.ObjectName == "Medium");
        //    OCode Large = TablesLogic.tCode.Load(TablesLogic.tCode.ObjectName == "Large");
        //    OCode XLarge = TablesLogic.tCode.Load(TablesLogic.tCode.ObjectName == "X Large");
        //    OCode XXLarge = TablesLogic.tCode.Load(TablesLogic.tCode.ObjectName == "XX Large");
        //    OCode XXXLarge = TablesLogic.tCode.Load(TablesLogic.tCode.ObjectName == "XXX Large");
        //    decimal countLocker = 0, countXXSmall = 0, countXSmall = 0, countSmall = 0,
        //        countMedium = 0, countLarge = 0, countXLarge = 0, countXXLarge = 0, countXXXLarge = 0, countTotal = 0;

        //    decimal countLockerOcc = 0, countXXSmallOcc = 0, countXSmallOcc = 0, countSmallOcc = 0,
        //        countMediumOcc = 0, countLargeOcc = 0, countXLargeOcc = 0, countXXLargeOcc = 0, countXXXLargeOcc = 0, countTotalOcc = 0;

        //    decimal countLockerAll = 0, countXXSmallAll = 0, countXSmallAll = 0, countSmallAll = 0,
        //        countMediumAll = 0, countLargeAll = 0, countXLargeAll = 0, countXXLargeAll = 0, countXXXLargeAll = 0, countTotalAll = 0;

        //    decimal countLockerP = 0, countXXSmallP = 0, countXSmallP = 0, countSmallP = 0,
        //        countMediumP = 0, countLargeP = 0, countXLargeP = 0, countXXLargeP = 0, countXXXLargeP = 0, countTotalP = 0;

        //    ExpressionCondition ec = Query.True;

        //    if (DropDownListSpaceType.SelectedValue == "")
        //        ec = ec & TablesLogic.tLocation.LocationType.ObjectName.In(SpaceType.Commercial,SpaceType.SelfStorage);
        //    else
        //        ec = ec & TablesLogic.tLocation.LocationTypeID == new Guid(DropDownListSpaceType.SelectedValue);

        //    if(DropDownListClimateControl.SelectedValue != "")
        //        ec = ec & TablesLogic.tLocation.ClimateControlID == new Guid(DropDownListClimateControl.SelectedValue);

        //    foreach (OLocation o in locs)
        //    {
        //        List<OLocation> sameLevel =
        //            TablesLogic.tLocation.LoadList
        //            (TablesLogic.tLocation.HierarchyPath.Like(o.HierarchyPath + "%")&
        //            TablesLogic.tLocation.IsDeleted == 0 & !TablesLogic.tLocation.Status.In("Occupied", "OverLocked") & ec);

        //        List<OLocation> sameLevelOcc =
        //            TablesLogic.tLocation.LoadList
        //            (TablesLogic.tLocation.HierarchyPath.Like(o.HierarchyPath + "%") &
        //            TablesLogic.tLocation.IsDeleted == 0 & TablesLogic.tLocation.Status.In("Occupied", "OverLocked") & ec);

        //        List<OLocation> sameLevelAll =
        //            TablesLogic.tLocation.LoadList
        //            (TablesLogic.tLocation.HierarchyPath.Like(o.HierarchyPath + "%") & 
        //            TablesLogic.tLocation.IsDeleted == 0 //& 
        //            //TablesLogic.tLocation.Status.In
        //           // ("Draft", "Pending Activation", "Pending Inactivation", "Vacant", "Occupied", "Reserved", "OverLocked", "Pending Auction", "Under Reinstatement") 
        //            &ec);

        //        int temp;
        //        DataRow datarow = locationLevel.NewRow();
        //        datarow[0] = o.ObjectID.ToString();
        //        datarow[1] = o.ObjectName.ToString();
        //        temp = sameLevel.FindAll(p => p.SizeTypeID == locker.ObjectID).Count;
        //        countLockerOcc += sameLevelOcc.FindAll(p => p.SizeTypeID == locker.ObjectID).Sum(p => p.NetLeasableArea).Value;
        //        countLockerAll += sameLevelAll.FindAll(p => p.SizeTypeID == locker.ObjectID).Sum(p => p.NetLeasableArea).Value;
        //        datarow[2] = temp.ToString(); countLocker += temp;
        //        temp = sameLevel.FindAll(p => p.SizeTypeID == XXSmall.ObjectID).Count;
        //        countXXSmallOcc += sameLevelOcc.FindAll(p => p.SizeTypeID == XXSmall.ObjectID).Sum(p => p.NetLeasableArea).Value;
        //        countXXSmallAll += sameLevelAll.FindAll(p => p.SizeTypeID == XXSmall.ObjectID).Sum(p => p.NetLeasableArea).Value;
        //        datarow[3] = temp.ToString(); countXXSmall += temp;
        //        temp = sameLevel.FindAll(p => p.SizeTypeID == XSmall.ObjectID).Count;
        //        countXSmallOcc += sameLevelOcc.FindAll(p => p.SizeTypeID == XSmall.ObjectID).Sum(p => p.NetLeasableArea).Value;
        //        countXSmallAll += sameLevelAll.FindAll(p => p.SizeTypeID == XSmall.ObjectID).Sum(p => p.NetLeasableArea).Value;
        //        datarow[4] = temp.ToString(); countXSmall += temp;
        //        temp = sameLevel.FindAll(p => p.SizeTypeID == Small.ObjectID).Count;
        //        countSmallOcc += sameLevelOcc.FindAll(p => p.SizeTypeID == Small.ObjectID).Sum(p => p.NetLeasableArea).Value;
        //        countSmallAll += sameLevelAll.FindAll(p => p.SizeTypeID == Small.ObjectID).Sum(p => p.NetLeasableArea).Value;
        //        datarow[5] = temp.ToString(); countSmall += temp;
        //        temp = sameLevel.FindAll(p => p.SizeTypeID == Medium.ObjectID).Count;
        //        countMediumOcc += sameLevelOcc.FindAll(p => p.SizeTypeID == Medium.ObjectID).Sum(p => p.NetLeasableArea).Value;
        //        countMediumAll += sameLevelAll.FindAll(p => p.SizeTypeID == Medium.ObjectID).Sum(p => p.NetLeasableArea).Value;
        //        datarow[6] = temp.ToString(); countMedium += temp;
        //        temp = sameLevel.FindAll(p => p.SizeTypeID == Large.ObjectID).Count;
        //        countLargeOcc += sameLevelOcc.FindAll(p => p.SizeTypeID == Large.ObjectID).Sum(p => p.NetLeasableArea).Value;
        //        countLargeAll += sameLevelAll.FindAll(p => p.SizeTypeID == Large.ObjectID).Sum(p => p.NetLeasableArea).Value;
        //        datarow[7] = temp.ToString(); countLarge += temp;
        //        temp = sameLevel.FindAll(p => p.SizeTypeID == XLarge.ObjectID).Count;
        //        countXLargeOcc += sameLevelOcc.FindAll(p => p.SizeTypeID == XLarge.ObjectID).Sum(p => p.NetLeasableArea).Value;
        //        countXLargeAll += sameLevelAll.FindAll(p => p.SizeTypeID == XLarge.ObjectID).Sum(p => p.NetLeasableArea).Value;
        //        datarow[8] = temp.ToString(); countXLarge += temp;
        //        temp = sameLevel.FindAll(p => p.SizeTypeID == XXLarge.ObjectID).Count;
        //        countXXLargeOcc += sameLevelOcc.FindAll(p => p.SizeTypeID == XXLarge.ObjectID).Sum(p => p.NetLeasableArea).Value;
        //        countXXLargeAll += sameLevelAll.FindAll(p => p.SizeTypeID == XXLarge.ObjectID).Sum(p => p.NetLeasableArea).Value;
        //        datarow[9] = temp.ToString(); countXXLarge += temp;
        //        temp = sameLevel.FindAll(p => p.SizeTypeID == XXXLarge.ObjectID).Count;
        //        countXXXLargeOcc += sameLevelOcc.FindAll(p => p.SizeTypeID == XXXLarge.ObjectID).Sum(p => p.NetLeasableArea).Value;
        //        countXXXLargeAll += sameLevelAll.FindAll(p => p.SizeTypeID == XXXLarge.ObjectID).Sum(p => p.NetLeasableArea).Value;
        //        datarow[10] = temp.ToString(); countXXXLarge += temp;
        //        temp = sameLevel.Count;
        //        countTotalOcc += sameLevelOcc.Sum(p => p.NetLeasableArea).Value;
        //        countTotalAll += sameLevelAll.Sum(p => p.NetLeasableArea).Value;
        //        datarow[11] = temp.ToString(); countTotal += temp;
        //        locationLevel.Rows.Add(datarow);
        //    }


        //    DataRow datarow1 = locationLevel.NewRow();
        //    datarow1[0] = "";
        //    datarow1[1] = "Total";
        //    datarow1[2] = countLocker.ToString();
        //    datarow1[3] = countXXSmall.ToString();
        //    datarow1[4] = countXSmall.ToString();
        //    datarow1[5] = countSmall.ToString();
        //    datarow1[6] = countMedium.ToString();
        //    datarow1[7] = countLarge.ToString();
        //    datarow1[8] = countXLarge.ToString();
        //    datarow1[9] = countXXLarge.ToString();
        //    datarow1[10] = countXXXLarge.ToString();
        //    datarow1[11] = countTotal.ToString();
        //    locationLevel.Rows.Add(datarow1);

        //    countLockerP = (countLockerAll == 0)?0:(decimal)countLockerOcc / countLockerAll;
        //    countXXSmallP = (countXXSmallAll == 0) ? 0 : (decimal)countXXSmallOcc / countXXSmallAll;
        //    countXSmallP = (countXSmallAll == 0) ? 0 : (decimal)countXSmallOcc / countXSmallAll;
        //    countSmallP = (countSmallAll == 0) ? 0 : (decimal)countSmallOcc / countSmallAll;
        //    countMediumP = (countMediumAll == 0) ? 0 : (decimal)countMediumOcc / countMediumAll;
        //    countLargeP = (countLargeAll == 0) ? 0 : (decimal)countLargeOcc / countLargeAll;
        //    countXLargeP = (countXLargeAll == 0) ? 0 : (decimal)countXLargeOcc / countXLargeAll;
        //    countXXLargeP = (countXXLargeAll == 0) ? 0 : (decimal)countXXLargeOcc / countXXLargeAll;
        //    countXXXLargeP = (countXXXLargeAll == 0) ? 0 : (decimal)countXXXLargeOcc / countXXXLargeAll;
        //    countTotalP = (countTotalAll == 0) ? 0 : (decimal)countTotalOcc / countTotalAll;

        //    DataRow datarow2 = locationLevel.NewRow();
        //    datarow2[0] = "";
        //    datarow2[1] = "Occupancy (Size Type) - Area";
        //    datarow2[2] = string.Format("{0:#,###.00}%", countLockerP*100);
        //    datarow2[3] = string.Format("{0:#,###.00}%", countXXSmallP * 100);
        //    datarow2[4] = string.Format("{0:#,###.00}%", countXSmallP * 100);
        //    datarow2[5] = string.Format("{0:#,###.00}%", countSmallP * 100);
        //    datarow2[6] = string.Format("{0:#,###.00}%", countMediumP * 100);
        //    datarow2[7] = string.Format("{0:#,###.00}%", countLargeP * 100);
        //    datarow2[8] = string.Format("{0:#,###.00}%", countXLargeP * 100);
        //    datarow2[9] = string.Format("{0:#,###.00}%", countXXLargeP * 100);
        //    datarow2[10] = string.Format("{0:#,###.00}%", countXXXLargeP * 100);
        //    datarow2[11] = string.Format("{0:#,###.00}%", countTotalP * 100);
        //    locationLevel.Rows.Add(datarow2);
        //}
        return locationLevel;
    }
    private string CreateTempFloorPlan(string parentID)
    {
        string extension = "";
        string filePath = "";
        byte[] filebytes = null;
        string idText = "";
        OLocation o = TablesLogic.tLocation.Load(new Guid(parentID));
        if (o.LocationType.ObjectName.ToUpper().Is("FLOOR", "LEVEL") && o.FloorPlanAttachments != null && o.FloorPlanAttachments.Count > 0)
        {
            OLocation locParent = o;
            OFloorPlanAttachment floorPlan = o.LatestFloorPlan;//o.FloorPlanAttachments[0];

            Guid id = Guid.NewGuid();
            if (floorPlan.ContentType == "image/gif")
                extension = ".gif";
            else if (floorPlan.ContentType == "image/jpeg")
                extension = ".jpg";
            else if (floorPlan.ContentType == "image/png")
                extension = ".png";
            filePath = HttpContext.Current.Server.MapPath("temp//" + id.ToString().Replace("-", "") + extension);
            idText = id.ToString().Replace("-", "");
            filebytes = floorPlan.FileBytes;

            System.IO.FileStream file = System.IO.File.Create(filePath);
            file.Write(filebytes, 0, filebytes.Length);
            file.Close();

            return "temp/" + idText + extension;
        }

        return "images/no_floorplan-2.jpg";
    }

    public String DrawCell(string text)
    {
        StringBuilder html = new StringBuilder();
        html.Append(" <td>");
        html.Append(text);
        html.Append(" </td>");

        return html.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="location"></param>
    /// <returns></returns>
    /// 
    public void GenerateCoordinates(List<OLocation> locationList, List<OLease> leaseList, StringBuilder htmlMain)
    {
        ////List<OLocation> locs = locationList.FindAll(p => p.Status == SpaceStatus.Vacant & p.CoordinateLeft != null & p.CoordinateRight != null);
        ////List<OLease> leases = generateTableForLease(locationList);


        foreach (OLocation location in locationList)
        {
            //if (location.CoordinateRight == null || location.CoordinateLeft == null)
            //continue;

            StringBuilder html = new StringBuilder();
            string cssClassDiv = "homedivmap";
            StringBuilder htmlTooltip = new StringBuilder();
            htmlTooltip.Append("<ul>");
            htmlTooltip.Append("<li>");
            htmlTooltip.Append(DrawCell("Unit No. : <b>" + location.ObjectName + "</b>"));
            htmlTooltip.Append("</li>");
            htmlTooltip.Append("<li>");
            htmlTooltip.Append(DrawCell("Size (sf): <b>" + string.Format("{0:#,###.00}", location.NetLeasableArea) + "</b>"));
            htmlTooltip.Append("</li>");
            htmlTooltip.Append("<li>");
            htmlTooltip.Append(DrawCell(string.Format("({0:#,###.00} x {0:#,###.00} x {0:#,###.00})", location.Height, location.Length, location.Width)));
            htmlTooltip.Append("</li>");
            htmlTooltip.Append("<li>");
            htmlTooltip.Append(DrawCell(string.Format("Price/Month: ${0:#,###}", location.CurrentPricing == null ? 0M : location.CurrentPricing.AskingRental)));
            htmlTooltip.Append("</li>");
            htmlTooltip.Append("</ul>");
            html.Append("<div style=\"");
            html.Append(" left: " + location.CoordinateLeft + "px;");
            html.Append(" top: " + location.CoordinateRight + "px;");
            html.Append(" position: absolute;\"");

            html.Append(" title=\"body=[" + htmlTooltip.ToString() + "] header=[#" + location.ObjectName + "] cssbody=[tooltip-body] cssheader=[tooltip-header]\"");
            html.Append(" class=\"" + cssClassDiv + "\">");
            string url = ResolveUrl("~/modules/asset/location/edit.aspx" +
            ("~/modules/asset/location/edit.aspx".Contains("?") ? "&" : "?") +
            "ID=" + HttpUtility.UrlEncode(Security.Encrypt("EDIT:" + location.ObjectID + ":" + AppSession.SaltID)) +
            "&TYPE=" + HttpUtility.UrlEncode(Security.Encrypt("OLocation" + ":" + AppSession.SaltID)));
            //string tempName = TablesLogic.tCode.Select(TablesLogic.tCode.ObjectName).Where(TablesLogic.tCode.ObjectID == location.SizeTypeID);
            if (location.LocationStatus == SpaceStatus.Vacant)
                html.Append("<div id=\"" + location.ObjectName + "\" align=\"center\"><a href='#' onclick='javascript:window.open(\"" + url + "\", \"AnacleEAM_Window\")'><img src=\"images/floorplan/rsz_vacant.png\"></img></a></div>");
            else if (location.Status == SpaceStatus.UnderMaintenance)
                html.Append("<div id=\"" + location.ObjectName + "\" align=\"center\"><a href='#' onclick='javascript:window.open(\"" + url + "\", \"AnacleEAM_Window\")'><img src=\"images/floorplan/rsz_undermatain.png\"></img></a></div>");
            html.Append(" </div>");

            htmlMain.Append(html.ToString());
        }


        foreach (OLease lease in leaseList)
        {
            foreach (OLeaseOfferSpace loc in lease.LeaseOfferSpaces)
            {
                if (loc.Space.CoordinateRight == null || loc.Space.CoordinateLeft == null)
                    continue;
                StringBuilder html = new StringBuilder();
                string cssClassDiv = "homedivmap";
                StringBuilder htmlTooltip = new StringBuilder();
                htmlTooltip.Append("<ul>");
                htmlTooltip.Append("<li>");
                htmlTooltip.Append(DrawCell("Lease No. : <b>" + lease.ObjectNumber + "</b>"));
                htmlTooltip.Append("</li>");
                htmlTooltip.Append("<li>");
                htmlTooltip.Append(DrawCell("Lease Status : <b>" + lease.Status + "</b>"));
                htmlTooltip.Append("</li>");
                htmlTooltip.Append("<li>");
                htmlTooltip.Append(DrawCell("Size (sf): <b>" + string.Format("{0:#,###.00}", loc.Space.NetLeasableArea) + "</b>"));
                htmlTooltip.Append("</li>");
                htmlTooltip.Append("<li>");
                htmlTooltip.Append(DrawCell("Lease period: <b>" + string.Format("{0:dd-MMM-yyyy} to {0:dd-MMM-yyyy}", lease.CommencementDate, lease.LastBillToDate) + "</b>"));
                htmlTooltip.Append("</li>");
                //htmlTooltip.Append("<li>");
                //htmlTooltip.Append(DrawCell("Over-lock Status : <b>" + lease.OverlockStatus + "</b>"));
                //htmlTooltip.Append("</li>");
                //DateTime osh = TablesLogic.tLeaseOverlockStatusHistory.
                //    SelectTop(1, TablesLogic.tLeaseOverlockStatusHistory.Date).
                //    Where(TablesLogic.tLeaseOverlockStatusHistory.LeaseID == lease.ObjectID & TablesLogic.tLeaseOverlockStatusHistory.IsDeleted == 0).OrderBy(TablesLogic.tLeaseOverlockStatusHistory.Date.Desc);
                //if (osh != null && osh.Year != 1)
                //{
                //    htmlTooltip.Append("<li>");
                //    htmlTooltip.Append(DrawCell("Status since: <b>" + string.Format("{0:dd-MMM-yyyy}", osh) + "</b>"));
                //    htmlTooltip.Append("</li>");
                //}
                htmlTooltip.Append("</ul>");
                html.Append("<div style=\"");
                html.Append(" left: " + loc.Space.CoordinateLeft + "px;");
                html.Append(" top: " + loc.Space.CoordinateRight + "px;");
                html.Append(" position: absolute;\"");

                html.Append(" title=\"body=[" + htmlTooltip.ToString() + "] header=[#" + loc.Space.ObjectName + "] cssbody=[tooltip-body] cssheader=[tooltip-header]\"");
                html.Append(" class=\"" + cssClassDiv + "\">");
                string url = ResolveUrl("~/modules/lease/lease/edit.aspx" +
                       ("~/modules/lease/lease/edit.aspx".Contains("?") ? "&" : "?") +
                       "ID=" + HttpUtility.UrlEncode(Security.Encrypt("EDIT:" + lease.ObjectID + ":" + AppSession.SaltID)) +
                       "&TYPE=" + HttpUtility.UrlEncode(Security.Encrypt("OLease" + ":" + AppSession.SaltID)));
                html.Append("<div id = \"" + Regex.Replace(lease.ObjectNumber, @"[^0-9a-zA-Z]+", "") + "\"align=\"center\">");
                html.Append("<a href='#' onclick='javascript:window.open(\"" + url + "\", \"AnacleEAM_Window\")'><img src=\"images/floorplan/rsz_nolock.png\"></img></a>");
                html.Append("</div>");
                html.Append(" </div>");

                htmlMain.Append(html.ToString());
            }
        }


    }
    //public void GenerateCoordinates(OLocation l, StringBuilder htmlMain)
    //{

    //    ExpressionCondition ec = Query.True;

    //    if (DropDownListSpaceType.SelectedValue == "")
    //        ec = ec & TablesLogic.tLocation.LocationType.ObjectName.In(SpaceType.Commercial,SpaceType.SelfStorage);
    //    else
    //        ec = ec & TablesLogic.tLocation.LocationTypeID == new Guid(DropDownListSpaceType.SelectedValue);

    //    if (DropDownListSizeType.SelectedValue != "")
    //        ec = ec & TablesLogic.tLocation.SizeTypeID == new Guid(DropDownListSizeType.SelectedValue);


    //    List<OLocation> locs =
    //            TablesLogic.tLocation.LoadList(TablesLogic.tLocation.HierarchyPath.Like(l.HierarchyPath + "%") & TablesLogic.tLocation.IsDeleted == 0
    //            & ec & TablesLogic.tLocation.Status!=SpaceStatus.Draft);


    //    ec = Query.True;

    //    if (DropDownListLeaseStatus.SelectedValue != "")
    //        ec = ec & TablesLogic.tLease.Status == DropDownListLeaseStatus.SelectedValue;

    //    if (DropDownListOverLockStatus.SelectedValue == "Pending Over-lock/Un-lock")
    //        ec = ec & TablesLogic.tLease.OverlockStatus.In(LeaseOverlockStatus.PendingOverlock, LeaseOverlockStatus.PendingUnlock);
    //    else if (DropDownListOverLockStatus.SelectedValue != "")
    //        ec = ec & TablesLogic.tLease.OverlockStatus == DropDownListOverLockStatus.SelectedValue;


    //    foreach (OLocation location in locs)
    //    {
    //        if (location.CoordinateRight == null || location.CoordinateLeft == null)
    //            continue;
    //        OLease lease = TablesLogic.tLease.Load(TablesLogic.tLease.LeaseSpaceID == location.ObjectID & ec);

    //        StringBuilder html = new StringBuilder();
    //        string cssClassDiv = "homedivmap";
    //        StringBuilder htmlTooltip = new StringBuilder();
    //        if (location.Status == SpaceStatus.Vacant)
    //        {
    //            htmlTooltip.Append("<ul>");
    //            htmlTooltip.Append("<li>");
    //            htmlTooltip.Append(DrawCell("Unit No. : <b>" + location.ObjectName + "</b>"));
    //            htmlTooltip.Append("</li>");
    //            htmlTooltip.Append("<li>");
    //            htmlTooltip.Append(DrawCell("Size (sf): <b>" + string.Format("{0:#,###.00}", location.NLAFoot) + "</b>"));
    //            htmlTooltip.Append("</li>");
    //            htmlTooltip.Append("<li>");
    //            htmlTooltip.Append(DrawCell(string.Format("({0:#,###.00} x {0:#,###.00} x {0:#,###.00})", location.SpaceHeight, location.SpaceLength, location.SpaceWidth)));
    //            htmlTooltip.Append("</li>");
    //            htmlTooltip.Append("<li>");
    //            htmlTooltip.Append(DrawCell(string.Format("Price/Month: ${0:#,###}", location.MonthlyRentalRate)));
    //            htmlTooltip.Append("</li>");
    //            htmlTooltip.Append("</ul>");
    //        }
    //        else
    //        {
    //            if (lease != null)
    //            {
    //                htmlTooltip.Append("<ul>");
    //                htmlTooltip.Append("<li>");
    //                htmlTooltip.Append(DrawCell("Lease No. : <b>" + lease.ObjectNumber + "</b>"));
    //                htmlTooltip.Append("</li>");
    //                htmlTooltip.Append("<li>");
    //                htmlTooltip.Append(DrawCell("Lease Status : <b>" + lease.Status + "</b>"));
    //                htmlTooltip.Append("</li>");
    //                htmlTooltip.Append("<li>");
    //                htmlTooltip.Append(DrawCell("Size (sf): <b>" + string.Format("{0:#,###.00}", lease.NetRents) + "</b>"));
    //                htmlTooltip.Append("</li>");
    //                htmlTooltip.Append("<li>");
    //                //if(lease.LeaseSpace.LocationType.ObjectName == SpaceType.SelfStorage)
    //                     //htmlTooltip.Append(DrawCell("Lease period: <b>" + string.Format("{0:dd-MMM-yyyy} to {0:dd-MMM-yyyy}", lease.CommencementDate, lease.LastBillToDate) + "</b>"));
    //               // else if(lease.LeaseSpace.LocationType.ObjectName == SpaceType.Commercial)
    //                    //htmlTooltip.Append(DrawCell("Lease period: <b>" + string.Format("{0:dd-MMM-yyyy} to {0:dd-MMM-yyyy}", lease.CommencementDate, lease.ExpiryDate) + "</b>"));
    //                htmlTooltip.Append("</li>");
    //                htmlTooltip.Append("<li>");
    //                //htmlTooltip.Append(DrawCell("Over-lock Status : <b>" + lease.OverlockStatus + "</b>"));
    //                htmlTooltip.Append("</li>");
    //                //DateTime osh = TablesLogic.tLeaseOverlockStatusHistory.
    //                   // SelectTop(1, TablesLogic.tLeaseOverlockStatusHistory.Date).
    //                   // Where(TablesLogic.tLeaseOverlockStatusHistory.LeaseID == lease.ObjectID & TablesLogic.tLeaseOverlockStatusHistory.IsDeleted == 0).OrderBy(TablesLogic.tLeaseOverlockStatusHistory.Date.Desc);
    //               //if (osh != null && osh.Year!=1)
    //                //{
    //                    //htmlTooltip.Append("<li>");
    //                    //htmlTooltip.Append(DrawCell("Status since: <b>" + string.Format("{0:dd-MMM-yyyy}", osh) + "</b>"));
    //                    //htmlTooltip.Append("</li>");
    //                //}
    //                htmlTooltip.Append("</ul>");
    //            }
    //        }




    //        html.Append("<div style=\"");
    //        html.Append(" left: " + location.CoordinateLeft + "px;");
    //        html.Append(" top: " + location.CoordinateRight + "px;");
    //        html.Append(" position: absolute;\"");

    //        html.Append(" title=\"body=[" + htmlTooltip.ToString() + "] header=[#" + location.ObjectName + "] cssbody=[tooltip-body] cssheader=[tooltip-header]\"");
    //        html.Append(" class=\"" + cssClassDiv + "\">");


    //        if (location.Status == SpaceStatus.Vacant)
    //        {
    //            string url = ResolveUrl("~/customizedmodulesforstorhub/location/edit.aspx" +
    //            ("~/customizedmodulesforstorhub/location/edit.aspx".Contains("?") ? "&" : "?") +
    //            "ID=" + HttpUtility.UrlEncode(Security.Encrypt("EDIT:" + location.ObjectID + ":" + AppSession.SaltID)) +
    //            "&TYPE=" + HttpUtility.UrlEncode(Security.Encrypt("OLocation")));
    //            //string tempName = TablesLogic.tCode.Select(TablesLogic.tCode.ObjectName).Where(TablesLogic.tCode.ObjectID == location.SizeTypeID);
    //            //html.Append("<div id=\"" +location.ObjectName+tempName.Replace(" ","") + "\" align=\"center\"><a href='#' onclick='javascript:window.open(\"" + url + "\", \"AnacleEAM_Window\")'><img src=\"images/marker-green.png\"></img></a></div>");
    //        }
    //        else
    //        {
    //            if (lease != null)
    //            {
    //                //OLease lease = TablesLogic.tLease.Load(TablesLogic.tLease.LeaseSpaceID == location.ObjectID);
    //                string url = ResolveUrl("~/customizedmodulesforstorhub/lease/edit.aspx" +
    //                ("~/customizedmodulesforstorhub/lease/edit.aspx".Contains("?") ? "&" : "?") +
    //                "ID=" + HttpUtility.UrlEncode(Security.Encrypt("EDIT:" + lease.ObjectID + ":" + AppSession.SaltID)) +
    //                "&TYPE=" + HttpUtility.UrlEncode(Security.Encrypt("OLease")));
    //                html.Append("<div id = \"" + Regex.Replace(lease.ObjectNumber, @"[^0-9a-zA-Z]+", "") + "\"align=\"center\">");

    //                html.Append("<a href='#' onclick='javascript:window.open(\"" + url + "\", \"AnacleEAM_Window\")'><img src=\"images/marker-red.png\"></img></a></div>");
    //            }

    //        }
    //        html.Append(" </div>");

    //        htmlMain.Append(html.ToString());
    //    }
    //}


    protected void DropDownListSite_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownListSite.ErrorMessage = "";
        string id = DropDownListSite.SelectedValue;
        BindFloorList(id);
        clearAll();
        //string siteID = DropDownListSite.SelectedValue;
        //if (siteID == "")
        //    return;
        //if (viewMode.SelectedValue == "1")
        //{
        //    BindFloorList(siteID);
        //    BindSpaceTypeList();
        //    BindClimateControlList();
        //}
        //else
        //{
        //    BindSpaceTypeList();
        //    BindClimateControlList();
        //    imageLiteral.Text = "";
        //    BindDashboard(siteID);
        //}
    }

    protected void BindDashboard(string siteID)
    {
        //SummaryTable.DataSource = generateDataTable(siteID);
        //SummaryTable.DataBind();
        //webDashboardOccupancy.DashboardID = TablesLogic.tDashboard.Select(TablesLogic.tDashboard.ObjectID)
        //    .Where(TablesLogic.tDashboard.ObjectName == "Current Occupancy by Size Type (For floor plan tab only)");
        //webDashboardVacantUnits.DashboardID = TablesLogic.tDashboard.Select(TablesLogic.tDashboard.ObjectID)
        //    .Where(TablesLogic.tDashboard.ObjectName == "Vacant Units by Size Type (For floor plan tab only)");
        //OLocation temp = TablesLogic.tLocation.Load(new Guid(siteID));
        //string path = temp.HierarchyPath;
        //webDashboardOccupancy.Refresh(path, DropDownListClimateControl.SelectedItem.Text, DropDownListSpaceType.SelectedItem.Text);
        //webDashboardOccupancy.Visible = true;
        //webDashboardVacantUnits.Refresh(path, DropDownListClimateControl.SelectedItem.Text, DropDownListSpaceType.SelectedItem.Text);
        //webDashboardVacantUnits.Visible = true;
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        DropDownListLevel.Visible = TextboxUnit.Visible = DropDownListSpaceType.Visible = DropDownListLeaseStatus.Visible = viewMode.SelectedValue == "1";
        Slider.Visible = viewMode.SelectedValue == "0";
        if (viewMode.SelectedValue == "1")
            dynamic.InnerHtml = "";
        //slider_panel.Visible = viewMode.SelectedValue == "0";
        //DropDownListClimateControl.Visible = viewMode.SelectedValue == "0";
        VacantOnly.Visible = LeaseOnly.Visible = viewMode.SelectedValue == "1";
        SummaryTable.Visible = (DropDownListSite.SelectedValue != "" && viewMode.SelectedValue == "0");
        legend.Visible = dynamic.InnerHtml.Trim() != string.Empty;
        //SummaryLevelTable.Visible = SummaryLeaseTable.Visible = (DropDownListSite.SelectedValue != "" && viewMode.SelectedValue == "1" && DropDownListLevel.SelectedValue != "");
        //DropDownListSite.ValidateRequiredField = true;
        //DropDownListLevel.ValidateRequiredField = viewMode.SelectedValue == "1";

    }

    protected void SummaryTable_Action(object sender, string commandName, List<object> dataKeys)
    {
        //if (commandName == "Check")
        //{
        //    string siteID = dataKeys[0].ToString();
        //    if (siteID == "")
        //        return;
        //    OLocation l = TablesLogic.tLocation.Load(new Guid(siteID));
        //    List<OLocation> locationList = TablesLogic.tLocation.LoadList(TablesLogic.tLocation.HierarchyPath.Like(l.HierarchyPath + "%") 
        //        & TablesLogic.tLocation.IsDeleted == 0 & TablesLogic.tLocation.LocationType.ObjectName.In(SpaceType.Commercial, SpaceType.SelfStorage));
        //    //BindFloorList(siteID);
        //    // BindSpaceTypeList();
        //    //BindClimateControlList();
        //    BindFloorList(DropDownListSite.SelectedValue);
        //    DropDownListLevel.SelectedValue = siteID;
        //    SummaryLevelTable.DataSource = generateDataTableForLevel(locationList, true);
        //    //SummaryLevelTable.DataSource = generateDataTableForLevel(DropDownListLevel.SelectedValue);
        //    SummaryLevelTable.DataBind();

        //   // List<OLease> leaseList = generateTableForLease(locationList);
        //    List<OLease> leaseList = new List<OLease>();
        //    SummaryLeaseTable.DataSource = leaseList;
        //    //SummaryLeaseTable.DataSource = generateTableForLease(DropDownListLevel.SelectedValue);
        //    SummaryLeaseTable.DataBind();

        //    BindFloorPlanImage(locationList, leaseList);

        //    SummaryLeaseTable.Visible = SummaryLevelTable.Visible = true;
        //    //SummaryTable.Visible = webDashboardOccupancy.Visible = webDashboardVacantUnits.Visible = false;
        //    legendDiv.Visible = true;
        //    viewMode.SelectedValue = "1";
        //    VacantOnly.Checked = true;
        //    LeaseOnly.Checked = false;
        //}
    }

    protected void viewMode_SelectedIndexChanged(object sender, EventArgs e)
    {
        clearAll();
        if (viewMode.SelectedValue == "1")
            LeaseOnly.Checked = VacantOnly.Checked = true;
        //SummaryTable.Visible = webDashboardOccupancy.Visible = (DropDownListSite.SelectedValue != "" && viewMode.SelectedValue == "0");
        //SummaryLevelTable.Visible = webDashboardOccupancy.Visible = (DropDownListSite.SelectedValue != "" && viewMode.SelectedValue == "1" && DropDownListLevel.SelectedValue != "");
        //DropDownListLevel.Visible = viewMode.SelectedValue == "1";

        //if (viewMode.SelectedValue == "0")
        //{
        //    //DropDownListLevel
        //}
        //else
        //{
        //}
    }

    protected void DropDownListLevel_SelectedIndexChanged(object sender, EventArgs e)
    {

        DropDownListLevel.ErrorMessage = "";
        if (!String.IsNullOrEmpty(DropDownListLevel.SelectedValue))
        {

        }
    }



    protected void BindFloorPlanImage()
    {
        StringBuilder html2 = new StringBuilder();

        string mapSrc = CreateTempFloorPlan(DropDownListLevel.SelectedValue);
        html2.Append("<img src=\"" + mapSrc + "\" class=\"image\" usemap='#Map'>");
        OLocation loc = TablesLogic.tLocation.Load(new Guid(DropDownListLevel.SelectedValue));
        if (loc == null)
            return;

        //GenerateCoordinates(loc, html2);

        imageLiteral.Text = html2.ToString();
    }

    protected void BindFloorPlanImage(List<OLocation> locationList, List<OLease> leaseList)
    {
        StringBuilder html2 = new StringBuilder();

        string mapSrc = CreateTempFloorPlan(DropDownListLevel.SelectedValue);
        html2.Append("<img src=\"" + mapSrc + "\" class=\"image\" usemap='#Map'>");
        //OLocation loc = TablesLogic.tLocation.Load(new Guid(DropDownListLevel.SelectedValue));
        //if (loc == null)
        //   return;
        if (!mapSrc.Contains("no_floorplan"))
        {
            List<OLocation> locationListFiltered = locationList.FindAll(p => p.LocationStatus.Is(SpaceStatus.Vacant, SpaceStatus.UnderMaintenance) & p.CoordinateLeft != null & p.CoordinateRight != null);
            //List<OLease> leaseListFiltered = leaseList.FindAll(p => p.LeaseOfferSpaces[0].Space.CoordinateLeft != null & p.LeaseOfferSpaces[0].Space.CoordinateRight != null);
            GenerateCoordinates(locationListFiltered, leaseList, html2);
        }

        imageLiteral.Text = html2.ToString();
    }

    protected void clearAll()
    {
        imageLiteral.Text = "";
        //DropDownListLevel.ClearSelection();
        //DropDownListSite.ClearSelection();
        //DropDownListSizeType.ClearSelection();
        //DropDownListClimateControl.ClearSelection();
        //DropDownListLeaseStatus.ClearSelection();
        //DropDownListOverLockStatus.ClearSelection();
        //SummaryTable.Visible = webDashboardOccupancy.Visible = webDashboardVacantUnits.Visible = false;
        SummaryLeaseTable.Visible = SummaryLevelTable.Visible = false;
        legendDiv.Visible = false;
        //DropDownListClimateControl.Bind(null);
        //DropDownListSpaceType.Bind(null);
    }

    protected void DropDownListClimateControl_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (viewMode.SelectedValue == "0")
        //{
        //    string siteID = DropDownListSite.SelectedValue;
        //    BindDashboard(siteID);
        //}
    }

    protected void DropDownListSpaceType_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (viewMode.SelectedValue == "0")
        //{
        //    string siteID = DropDownListSite.SelectedValue;
        //    BindDashboard(siteID);
        //}
    }

    protected void SubmitButton_Click(object sender, EventArgs e)
    {

        string siteID = DropDownListSite.SelectedValue;
        System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
        if (siteID == "")
            return;
        if (viewMode.SelectedValue == "1")
        {
            DropDownListSite.Validate();
            DropDownListLevel.Validate();
            if (DropDownListSite.SelectedValue == "")
            {
                DropDownListSite.ErrorMessage = "Site is required."; return;
            }
            if (DropDownListLevel.SelectedValue == "")
            {
                DropDownListLevel.ErrorMessage = "Level is required."; return;
            }

            OLocation l = TablesLogic.tLocation.Load(new Guid(DropDownListLevel.SelectedValue));

            ExpressionCondition ec = Query.True;

            if (DropDownListSpaceType.SelectedValue != "")
                //    ec = ec & TablesLogic.tLocation.LocationType.ObjectName.In(SpaceType.Commercial, SpaceType.SelfStorage);
                //else
                ec = ec & TablesLogic.tLocation.SpaceType == DropDownListSpaceType.SelectedValue;

            //if (DropDownListSizeType.SelectedValue != "")
            //    ec = ec & TablesLogic.tLocation.SizeTypeID == new Guid(DropDownListSizeType.SelectedValue);

            if (TextboxUnit.Text != "")
                ec = ec & TablesLogic.tLocation.ObjectName.Like("%" + TextboxUnit.Text + "%");

            List<OLocation> locationList = TablesLogic.tLocation.LoadList(TablesLogic.tLocation.HierarchyPath.Like(l.HierarchyPath + "%") & TablesLogic.tLocation.IsDeleted == 0
                & TablesLogic.tLocation.LocationType.ObjectName == LocationType.Unit & ec);
            System.Diagnostics.Debug.WriteLine("Created LocationList: " + sw.ElapsedMilliseconds);
            //BindFloorList(siteID);
            // BindSpaceTypeList();
            //BindClimateControlList();
            List<OLocation> vacantUnits = new List<OLocation>();
            if (LeaseOnly.Checked && !VacantOnly.Checked) { }
            //do nothing
            else
                vacantUnits.AddRange(generateDataTableForLevel(locationList, true));
            SummaryLevelTable.DataSource = vacantUnits;
            //SummaryLevelTable.DataSource = generateDataTableForLevel(DropDownListLevel.SelectedValue);
            SummaryLevelTable.DataBind();
            System.Diagnostics.Debug.WriteLine("Generated Summary Level Table: " + sw.ElapsedMilliseconds);
            List<OLease> leaseList = generateTableForLease(locationList);
            SummaryLeaseTable.DataSource = leaseList;
            //SummaryLeaseTable.DataSource = generateTableForLease(DropDownListLevel.SelectedValue);
            SummaryLeaseTable.DataBind();
            System.Diagnostics.Debug.WriteLine("Generated Summary Lease Table: " + sw.ElapsedMilliseconds);
            BindFloorPlanImage(vacantUnits, leaseList);
            System.Diagnostics.Debug.WriteLine("Generated Image: " + sw.ElapsedMilliseconds);
            SummaryLeaseTable.Visible = SummaryLevelTable.Visible = true;
            //SummaryTable.Visible = webDashboardOccupancy.Visible = webDashboardVacantUnits.Visible = false;
            legendDiv.Visible = true;

            sw.Stop();
            System.Diagnostics.Debug.WriteLine("Total: " + sw.Elapsed.TotalMilliseconds);
        }
        else
        {
            if (DropDownListSite.SelectedValue == "")
            {
                DropDownListSite.ErrorMessage = "Site is required."; return;
            }
            Window.WriteJavascript("init();");
            //if (DropDownListSite.SelectedValue == "")
            //    DropDownListSite.ErrorMessage = "Please choose at least one Site";
            //BindSpaceTypeList();
            //BindClimateControlList();
            imageLiteral.Text = "";
            SummaryLeaseTable.Visible = SummaryLevelTable.Visible = false; ;
            dynamic.InnerHtml = GenerateStackingPlan(TablesLogic.tLocation.Load(new Guid(siteID)), DateTime.Today);
            BindDashboard(siteID);
            //SummaryTable.Visible = webDashboardOccupancy.Visible = webDashboardVacantUnits.Visible = true;
        }
    }

    protected string GenerateStackingPlan(OLocation location, DateTime date)
    {
        StringBuilder sb = new StringBuilder();
        List<OLocation> levelList = TablesLogic.tLocation.LoadList(TablesLogic.tLocation.LocationType.ObjectName == LocationType.Level &
            TablesLogic.tLocation.HierarchyPath.Like(location.HierarchyPath + "%"));
        levelList.Sort("ObjectName", false);
        levelList.Sort("StackingSequence", false);
        foreach (OLocation level in levelList)
        {
            sb.Append("<div class=\"level\">" + level.ObjectName + "</div>");
            sb.Append("<div class=\"progress\">");
            List<OLocation> unitList = TablesLogic.tLocation.LoadList(TablesLogic.tLocation.LocationType.ObjectName == LocationType.Unit &
                TablesLogic.tLocation.ParentID == level.ObjectID &
                (TablesLogic.tLocation.EffectiveStartDate <= date | TablesLogic.tLocation.EffectiveStartDate == null) &
                (TablesLogic.tLocation.EffectiveEndDate >= date | TablesLogic.tLocation.EffectiveEndDate == null));
            if (unitList.Count > 0)
            {
                decimal percent = (decimal)(100 - 1) / unitList.Count;
                //percent.ToString("#.##");
                foreach (OLocation unit in unitList)
                {
                    string status = GetUnitStatus(unit, date);
                    
                    switch (status)
                    {
                        case "Vacant":
                            sb.Append("<div class=\"progress-bar progress-bar-vacant\" role=\"progressbar\" style=\"width: " +
                            percent.ToString("#.##") + "%\">" + unit.ObjectName + "</div>");
                            break;
                        case "Reserved":
                            sb.Append("<div class=\"progress-bar progress-bar-reserved\" role=\"progressbar\" style=\"width: " +
                            percent.ToString("#.##") + "%\">" + unit.ObjectName + "</div>");
                            break;
                        case "Offered":
                            sb.Append("<div class=\"progress-bar progress-bar-offered\" role=\"progressbar\" style=\"width: " +
                            percent.ToString("#.##") + "%\">" + unit.ObjectName + "</div>");
                            break;
                        case "Occupied":
                            sb.Append("<div class=\"progress-bar progress-bar-occupied\" role=\"progressbar\" style=\"width: " +
                            percent.ToString("#.##") + "%\">" + unit.ObjectName + "</div>");
                            break;
                    }
                }
            }
            else
            {
                sb.Append("<div class=\"progress-bar progress-bar-nothing\" role=\"progressbar\" style=\"width: 99" +"%\">"+ "</div>");
            }
            sb.Append("</div>");

        }
        return sb.ToString();
    }

    protected string GetUnitStatus(OLocation Unit, DateTime date)
    {
        OLease lease = TablesLogic.tLease.Load(TablesLogic.tLease.LeaseOfferSpaces.SpaceID == Unit.ObjectID &
            TablesLogic.tLease.ExpiryDate >= date &
            ((TablesLogic.tLease.FittingOutDateFrom != null & TablesLogic.tLease.FittingOutDateFrom <= date) | TablesLogic.tLease.CommencementDate <= date));
        
        if (lease != null)
            return "Occupied";

        OLeaseOffer leaseOffer = TablesLogic.tLeaseOffer.Load(TablesLogic.tLeaseOffer.LeaseOfferSpaces.SpaceID == Unit.ObjectID &
            TablesLogic.tLeaseOffer.LeaseExpiryDate >= date &
            ((TablesLogic.tLeaseOffer.FittingOutDateFrom != null & TablesLogic.tLeaseOffer.FittingOutDateFrom <= date) | TablesLogic.tLeaseOffer.LeaseCommencementDate <= date));

        if (leaseOffer != null)
            return "Offered";

        OUnitReservation reservation = TablesLogic.tUnitReservation.Load(TablesLogic.tUnitReservation.Spaces.ObjectID == Unit.ObjectID &
            TablesLogic.tUnitReservation.ReservationStartDate <= date & TablesLogic.tUnitReservation.ReservationEndDate >= date);

        if (reservation != null)
            return "Reserved";

        return "Vacant";
    }
    protected void SummaryLeaseTable_Action(object sender, string commandName, List<object> dataKeys)
    {
    }

    protected void SummaryLeaseTable_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{
        //    DataRowView rowView = (DataRowView)e.Row.DataItem;
        //    string id = rowView["ObjectID"].ToString();
        //    DateTime osh = TablesLogic.tLeaseOverlockStatusHistory.
        //                SelectTop(1, TablesLogic.tLeaseOverlockStatusHistory.Date).
        //                Where(TablesLogic.tLeaseOverlockStatusHistory.LeaseID == new Guid(id) & TablesLogic.tLeaseOverlockStatusHistory.IsDeleted == 0).OrderBy(TablesLogic.tLeaseOverlockStatusHistory.Date.Desc);
        //    if (osh != null && osh.Year != 1 )
        //    {
        //        e.Row.Cells[5].Text = string.Format("{0:dd-MMM-yyyy}", osh);
        //    }

        //    string[] leaseStatusList = Regex.Split(e.Row.Cells[3].Text, @"(?<!^)(?=[A-Z])");
        //    e.Row.Cells[3].Text = string.Join(" ", leaseStatusList);
        //}   
    }

    protected void SummaryTable_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView rowView = (DataRowView)e.Row.DataItem;
            string id = rowView["ObjectID"].ToString();

            if (id == "")
                e.Row.Cells[3].Text = "";
        }
    }

    protected void postbackButton_Click(object sender, EventArgs e)
    {
        
    }

</script>

<html xmlns="http://www.w3.org/1999/xhtml">

<head id="Head1" runat="server">
    <title>Anacle.EAM</title>
    <link href="App_Themes/Corporate/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="scripts/nouislider/nouislider.min.css" rel="stylesheet">
    <%--<script src="scripts/nouislider/nouislider.js"></script>--%>
    <%--<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">--%>


    <style type="text/css">
        @keyframes tada {
            0% {
                transform: scale(1);
            }

            10%, 20% {
                transform: scale(0.9) rotate(-3deg);
            }

            30%, 50%, 70%, 90% {
                transform: scale(1.1) rotate(3deg);
            }

            40%, 60%, 80% {
                transform: scale(1.1) rotate(-3deg);
            }

            100% {
                transform: scale(1) rotate(0);
            }
        }


        @-webkit-keyframes tada {
            0% {
                -webkit-transform: scale(1);
            }

            10%, 20% {
                -webkit-transform: scale(0.9) rotate(-3deg);
            }

            30%, 50%, 70%, 90% {
                -webkit-transform: scale(1.1) rotate(3deg);
            }

            40%, 60%, 80% {
                -webkit-transform: scale(1.1) rotate(-3deg);
            }

            100% {
                -webkit-transform: scale(1) rotate(0);
            }
        }

        .animated {
            -webkit-animation-duration: 1s;
            animation-duration: 1s;
            -webkit-animation-fill-mode: both;
            animation-fill-mode: both;
            animation-iteration-count: infinite;
            -webkit-animation-iteration-count: infinite;
        }

        .tada {
            -webkit-animation-name: tada;
            animation-name: tada;
        }

        .image {
            border: none;
            max-width: 2000px;
        }

        .divider {
            position: absolute;
            left: 50%;
            top: 10%;
            bottom: 10%;
            border-left: 1px solid black;
        }

        .numberCircle {
            -webkit-border-radius: 999px;
            -moz-border-radius: 999px;
            border-radius: 999px;
            behavior: url(PIE.htc);
            width: 16px;
            height: 16px;
            padding: 2px;
            border: 2px solid black;
            text-align: center;
            text-shadow: 0 0 0.2em #8F7;
            font: 12px Arial, sans-serif;
            cursor: hand;
            cursor: pointer;
        }

        .filter-right {
            float: left;
            width: 35%;
        }

        .filter-left {
            float: left;
            width: 40%;
            margin-top: 35px;
            margin-left: 60px;
        }

        .filter-right2 {
            float: left;
            width: 15%;
        }

        .div-searchform {
            width: 100%;
        }

        legend {
            display: block;
            padding-left: 2px;
            padding-right: 2px;
            border: none;
        }

        .highlight {
            background-color: yellow;
        }

        .floatleft {
            float: left;
        }

        .progress {
            height: 40px;
            /*margin-bottom: 5px;*/
            overflow: hidden;
            /*background-color: #f5f5f5;*/
            /*border-radius: 4px;*/
            /*-webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
            box-shadow: inset 0 1px 2px rgba(0,0,0,.1);*/
            border-width: thin;
            border-color: black;
            border-left-style: solid;
        }

        .progress-bar {
            /*border-width: thin;
            border-color: grey;
            border-left-style: groove;
            border-bottom-style: none;*/
            outline: 1px solid black;
            float: left;
            width: 0;
            height: 100%;
            font-size: 15px;
            line-height: 15px;
            color: #fff;
            text-align: center;
            background-color: #337ab7;
            -webkit-box-shadow: inset 0 -1px 0 rgba(0,0,0,.15);
            box-shadow: inset 0 -1px 0 rgba(0,0,0,.15);
            -webkit-transition: width .6s ease;
            -o-transition: width .6s ease;
            transition: width .6s ease;
        }

        .progress-bar-vacant {
            background-color: #f3ac32;
        }

        .progress-bar-occupied {
            background-color: #b8b8b8;
        }

        .progress-bar-reserved {
            background-color: #0080FF;
        }

        .progress-bar-offered {
            background-color: #04B431;
        }

        .progress-bar-nothing {
            background-color: #FFFFFF;
        }

        .level {
            float: left;
            margin-right: 10px;
            height: 35px;
            width:8%;
            font-size:16px;
            line-height: 25px;
        }

        fieldset {
            font-family: sans-serif;
            /*border: 5px solid #1F497D;
            background: #ddd;
            border-radius: 5px;*/
            padding: 15px;
        }

            fieldset legend {
                /*background: #1F497D;
                color: #fff;*/
                padding: 5px 10px;
                font-size: 16px;
                /*border-radius: 5px;
                box-shadow: 0 0 0 5px #ddd;*/
                margin-left: 20px;
            }

        .rectangle-vacant {
            width: 20px;
            height: 20px;
            background: #f3ac32;
        }

        .rectangle-occupied {
            width: 20px;
            height: 20px;
            background: #b8b8b8;
        }

        .rectangle-reserved {
            width: 20px;
            height: 20px;
            background: #0080FF;
        }

        .rectangle-offered {
            width: 20px;
            height: 20px;
            background: #04B431;
        }

        #slider {
            float: left;
            clear: left;
            width: 800px;
            margin: 15px;
        }
    </style>


    <meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body>

    <form id="form1" runat="server">
        <div class="div-home">
            <div class="tabstrip">
                <ul>
                    <li>
                        <asp:HyperLink runat="server" ID="LinkToInbox" Text="Inbox" NavigateUrl="~/home.aspx" meta:resourcekey="LinkToInboxResource" ></asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink runat="server" ID="LinkToCalendar" Text="Calendar" NavigateUrl="~/homemonthview.aspx" meta:resourcekey="LinkToCalendarResource" ></asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink runat="server" ID="LinkToAnnouncements" Text="Announcements" NavigateUrl="~/homeannouncements.aspx" meta:resourcekey="LinkToAnnouncementsResource" ></asp:HyperLink>
                    </li>
                   <li>
                        <asp:HyperLink runat="server" ID="LinkToPinboard" Text="Pinboard" NavigateUrl="~/homepinboards.aspx" meta:resourcekey="LinkToPinboardResource" ></asp:HyperLink>
                    </li>
                    <li class='selected'>
                        <asp:HyperLink runat="server" ID="LinkToFloorPlan" Text="Floor Plan" meta:resourcekey="LinkToFloorPlanResource" ></asp:HyperLink>
                    </li>
                </ul>
            </div>
        </div>
        <ui:UIPanel ID="SitePanel" runat="server" CssClass="div-home bg search-icon floatleft" meta:resourcekey="SitePanelResource" >
            <div class="filter-left">
                <ui:UIFieldRadioList runat="server" ID="viewMode" Span="Full" CaptionAlign="Center" CaptionPosition="Side"
                    Caption="View Mode" ShowCaption="true" OnSelectedIndexChanged="viewMode_SelectedIndexChanged" meta:resourcekey="viewModeResource" >
                    <Items>
                        <asp:ListItem Value="0" Text="Building Stacking Plan" Selected="True" meta:resourcekey="viewModeListItem0Resource" >
                        </asp:ListItem>
                        <asp:ListItem Value="1" Text="Floor Plan View, By Level" meta:resourcekey="viewModeListItem1Resource" >
                        </asp:ListItem>
                    </Items>
                </ui:UIFieldRadioList>
                <br />

            </div>
            <div class="filter-right">
                <ui:UIFieldSearchableDropDownList runat="server" ID="DropDownListSite" Caption="Building" InternalControlWidth="300px"
                    OnSelectedIndexChanged="DropDownListSite_SelectedIndexChanged" Span="Full" ValidateRequiredField="true" meta:resourcekey="DropDownListSiteResource" >
                </ui:UIFieldSearchableDropDownList>

                <ui:UIFieldDropDownList runat="server" ID="DropDownListLevel" Caption="Level" InternalControlWidth="300px"
                    Span="Full" OnSelectedIndexChanged="DropDownListLevel_SelectedIndexChanged" ValidateRequiredField="true" meta:resourcekey="DropDownListLevelResource" >
                </ui:UIFieldDropDownList>

                <ui:UIFieldTextBox runat="server" ID="TextboxUnit" Caption="Unit" InternalControlWidth="300px"
                    Span="Full" meta:resourcekey="TextboxUnitResource" >
                </ui:UIFieldTextBox>

                <ui:UIFieldDropDownList runat="server" ID="DropDownListSpaceType" Caption="Space Type" InternalControlWidth="300px"
                    Span="Full" OnSelectedIndexChanged="DropDownListSpaceType_SelectedIndexChanged" meta:resourcekey="DropDownListSpaceTypeResource" >
                </ui:UIFieldDropDownList>

                <%--<ui:UIFieldDropDownList runat="server" ID="DropDownListClimateControl" Caption="Climate Control" InternalControlWidth="300px"
            Span="Full" OnSelectedIndexChanged="DropDownListClimateControl_SelectedIndexChanged" meta:resourcekey="DropDownListClimateControlResource" >
        </ui:UIFieldDropDownList>--%>

                <%--<ui:UIFieldDropDownList runat="server" ID="DropDownListSizeType" Caption="Size Type" InternalControlWidth="300px"
            Span="Full" meta:resourcekey="DropDownListSizeTypeResource" >
        </ui:UIFieldDropDownList>--%>

                <ui:UIFieldDropDownList runat="server" ID="DropDownListLeaseStatus" Caption="Lease Status" InternalControlWidth="300px"
                    Span="Full" meta:resourcekey="DropDownListLeaseStatusResource" >
                </ui:UIFieldDropDownList>
                <%--<ui:UIFieldDropDownList runat="server" ID="DropDownListOverLockStatus" Caption="Over-lock Status" InternalControlWidth="300px"
            Span="Full" meta:resourcekey="DropDownListOverLockStatusResource" >
        </ui:UIFieldDropDownList>--%>

                <ui:UIButton runat="server" ID="SubmitButton" Text=">>Show result" meta:resourcekey="SubmitButtonResource" OnClick="SubmitButton_Click" CssClass="btngray" />
            </div>

            <div class="filter-right2">
                <ui:UIFieldCheckBox ID="VacantOnly" runat="server" Caption="Show Vacant" meta:resourcekey="VacantOnlyResource" ></ui:UIFieldCheckBox>
                <ui:UIFieldCheckBox ID="LeaseOnly" runat="server" Caption="Show Lease" meta:resourcekey="LeaseOnlyResource" ></ui:UIFieldCheckBox>
            </div>
        </ui:UIPanel>
        <div style="background-color: #ffffff;">
            <%--<div runat="server" id="divSide" class="div-home" style="width: 350px; float: left">
            <ui:UIFieldRadioList runat="server" ID="CheckBoxListItemCategory" Caption="Type"
                TextAlign="Right" Span="Half" ShowCaption="false" CssClass="field-radio-tabstrip-ver"
                PropertyName="" FieldLayout="Flow" RepeatDirection="Vertical" RepeatLayout="Flow"
                RepeatColumns="0" OnSelectedIndexChanged="CheckBoxListItemCategory_SelectedIndexChanged" meta:resourcekey="CheckBoxListItemCategoryResource" >
            </ui:UIFieldRadioList>
        </div>--%>



            <ui:UIPanel ID="InfoPanel" runat="server" meta:resourcekey="InfoPanelResource" >
                <%--<div runat="server" id="divSide" style="width: 30%; float: left">
                <web:floorplanDashboard runat="server" ID="webDashboardOccupancy" meta:resourcekey="webDashboardOccupancyResource" >
                </web:floorplanDashboard>
                <web:floorplanDashboard runat="server" ID="webDashboardVacantUnits" meta:resourcekey="webDashboardVacantUnitsResource" >
                </web:floorplanDashboard>
            </div>
                --%>
                <%--<div runat="server" id="div1" style="width: 350px; float: left">
                <web:floorplanDashboard runat="server" ID="webDashboardVacantUnits" meta:resourcekey="webDashboardVacantUnitsResource" >
                </web:floorplanDashboard>
            </div>--%>
                 <ui:UIButton ID="postbackButton" runat="server" CssClass="hide" OnClick="postbackButton_Click"  meta:resourcekey="postbackButtonResource" />
                <div runat="server" id="divSide" style="width: 20%; float: left">
                    <ui:UIPanel runat="server" ID="Slider" meta:resourcekey="SliderResource" >
                        <div id="slider-date" style="margin: 50px 50px 0 50px;"></div>
                        <div id="event-start" style="margin-left:50px;margin-top:10px;"></div>
                    </ui:UIPanel>
                </div>

                <div id="summaryLevelLease" style="float: left; width: 20%; overflow: overlay;">

                    <ui:UIPanel runat="server" ID="SummaryLevelPanel" meta:resourcekey="SummaryLevelPanelResource" >
                        <div id="level-div" style="margin-left: 20px;">
                        <ui:UIGridView runat="server" ID="SummaryLevelTable" HeaderStyle-BackColor="#3AC0F2" PageSize="15"
                            HeaderStyle-ForeColor="White" CheckBoxColumnVisible="false" AllowSorting="true" Caption="Vacant Unit Listing" Visible="false" SortExpression="ObjectName" meta:resourcekey="SummaryLevelTableResource" >
                            <Columns>
                                <%--<ui:UIGridViewBoundColumn ID="SizeType" PropertyName="SizeType" HeaderText="Size Type" meta:resourcekey="SummaryLevelTableUIGridViewBoundColumnSizeTypeResource" >
                                </ui:UIGridViewBoundColumn>--%>
                                <ui:UIGridViewBoundColumn ID="Size" PropertyName="NetLeasableArea" HeaderText="Size(sf)" DataFormatString="{0:#,###.00}" meta:resourcekey="SummaryLevelTableUIGridViewBoundColumnNetLeasableAreaResource" >
                                </ui:UIGridViewBoundColumn>
                                <ui:UIGridViewBoundColumn ID="UnitNo" PropertyName="ObjectName" HeaderText="Unit #" meta:resourcekey="SummaryLevelTableUIGridViewBoundColumnObjectNameResource" >
                                </ui:UIGridViewBoundColumn>
                                <ui:UIGridViewTemplateColumn>
                                    <ItemTemplate>
                                        <%--<ui:UIButton ID="selectUnit" runat="server" OnClickScript='<%# "highlight(\"" + Eval("ObjectID" ) + "\" );" % meta:resourcekey="selectUnitResource" >' 
                               ImageUrl="images/view.gif" CssClass="actionBox"/>--%>
                                        <input id='<%# "checkbox" + Regex.Replace(Eval("ObjectName").ToString(),"[^0-9a-zA-Z]+","") %>'
                                            type="checkbox" onclick='<%# "highlight(\"" + Regex.Replace(Eval("ObjectName").ToString(),"[^0-9a-zA-Z]+","")+ "\" );" %>' />
                                        <%--<ui:UIFieldCheckBox ID="checkUnit" runat="server" OnCheckedChanged="checkUnit_CheckedChanged" meta:resourcekey="checkUnitResource" ></ui:UIFieldCheckBox>--%>
                                    </ItemTemplate>
                                </ui:UIGridViewTemplateColumn>
                                <ui:UIGridViewBoundColumn ID="AskingPrice" PropertyName="CurrentPricing.AskingRental" HeaderText="Asking Price / Month" DataFormatString="{0:#,###.00}" meta:resourcekey="checkUnitUIGridViewBoundColumnCurrentPricingAskingRentalResource" >
                                </ui:UIGridViewBoundColumn>
                            </Columns>
                        </ui:UIGridView>
                        </div>
                    </ui:UIPanel>


                    <ui:UIPanel runat="server" ID="SummaryLeasePanel" meta:resourcekey="SummaryLeasePanelResource" >
                        <div id="lease-div" style="margin-left: 20px;">
                        <ui:UIGridView runat="server" ID="SummaryLeaseTable" HeaderStyle-BackColor="#3AC0F2" PageSize="10"
                            HeaderStyle-ForeColor="White" CheckBoxColumnVisible="false" AllowSorting="true"
                            Caption="Lease Listing" OnAction="SummaryLeaseTable_Action" OnRowDataBound="SummaryLeaseTable_RowDataBound" Visible="false"
                            SortExpression="ObjectNumber" meta:resourcekey="SummaryLeaseTableResource" >
                            <Columns>
                                <ui:UIGridViewBoundColumn ID="LeaseNo" PropertyName="ObjectNumber" HeaderText="Lease #" meta:resourcekey="SummaryLeaseTableUIGridViewBoundColumnObjectNumberResource" >
                                </ui:UIGridViewBoundColumn>
                                <ui:UIGridViewTemplateColumn>
                                    <ItemTemplate>
                                        <%--<ui:UIButton ID="selectUnit" runat="server" OnClickScript='<%# "highlight(\"" + Eval("ObjectID" ) + "\" );" % meta:resourcekey="selectUnitResource" >' 
                               ImageUrl="images/view.gif" CssClass="actionBox"/>--%>
                                        <input id='<%# "checkbox" + Regex.Replace(Eval("ObjectNumber").ToString(),"[^0-9a-zA-Z]+","") %>'
                                            type="checkbox" onclick='<%# "highlight(\"" + Regex.Replace(Eval("ObjectNumber").ToString(),"[^0-9a-zA-Z]+","")+ "\" );" %>' />
                                        <%--<ui:UIFieldCheckBox ID="checkUnit" runat="server" OnCheckedChanged="checkUnit_CheckedChanged" meta:resourcekey="checkUnitResource" ></ui:UIFieldCheckBox>--%>
                                    </ItemTemplate>
                                </ui:UIGridViewTemplateColumn>
                                <ui:UIGridViewBoundColumn ID="LeaseStatusColumn" PropertyName="Status" HeaderText="Lease Status" meta:resourcekey="checkUnitUIGridViewBoundColumnStatusResource" >
                                </ui:UIGridViewBoundColumn>
                                <%--<ui:UIGridViewBoundColumn ID="OverlockStatus" PropertyName="OverlockStatus" HeaderText="Overlock Status" meta:resourcekey="checkUnitUIGridViewBoundColumnOverlockStatusResource" >
                                </ui:UIGridViewBoundColumn>
                                <ui:UIGridViewBoundColumn ID="OverlockStatusSince" HeaderText="Status Since" meta:resourcekey="OverlockStatusSinceResource" >
                                </ui:UIGridViewBoundColumn>--%>
                            </Columns>
                        </ui:UIGridView>
                        </div>
                    </ui:UIPanel>

                </div>

                <%--<div class="divider"></div>--%>
                <div id="divRight" style="float: right; width: 80%; overflow: auto; height: 1000px">
                    <div runat="server" id="divImage" style="position: relative; margin-left: 100px; max-width: 2000px">
                        <asp:Literal runat="server" ID="imageLiteral" meta:resourcekey="imageLiteralResource" ></asp:Literal>
                        <div runat="server" id="legendDiv" style="width: 500px; margin-left: 0px" visible="false">
                            <fieldset id="legendFieldSet">
                                <legend>Legend</legend>
                                <img src="images/floorplan/Vacant.png" />Vacant Unit  
                          <img src="images/floorplan/UnderMatain.png" />Under Maintenance
                         <img src="images/floorplan/NoLock.png" />Lease
                          <%--<img src="images/floorplan/OverLock.png" />Lease - Pending Overlock  
                          <img src="images/floorplan/UnLock.png" />Lease - Pending Unlock 
                          <img src="images/floorplan/OverLocke.png" />Lease - OverLocked--%>
                            </fieldset>
                        </div>
                    </div>



                    <div style="float: left; width: 100%">

                        <ui:UIFieldLabel runat="server" ID="SummaryTable" Text="" meta:resourcekey="SummaryTableResource" ></ui:UIFieldLabel>

                        <br />
                        <div id="legend" runat="server" style="width: 10%; float: right">
                            <section style="margin: 10px;">
                                <fieldset style="min-height: 100px;">
                                    <legend><b>Legend </b></legend>

                                    <div class="rectangle-vacant" style="float: left"></div>
                                    <div style="text-align: center">Vacant</div>
                                    <br />


                                    <div class="rectangle-occupied" style="float: left"></div>
                                    <div style="text-align: center">Occupied</div>
                                    <br />


                                    <div class="rectangle-reserved" style="float: left"></div>
                                    <div style="text-align: center">Reserved</div>
                                    <br />


                                    <div class="rectangle-offered" style="float: left"></div>
                                    <div style="text-align: center">Offered</div>

                                </fieldset>
                        </div>
                        <div id="dynamic" runat="server" style="width:90%;">
                        </div>


                        <%--<ui:UIGridView runat="server" ID="SummaryTable" HeaderStyle-BackColor="#3AC0F2" HeaderStyle-ForeColor="White"
                    CheckBoxColumnVisible="false" AllowSorting="false" OnAction="SummaryTable_Action"
                    OnRowDataBound="SummaryTable_RowDataBound" meta:resourcekey="SummaryTableResource" >
                    <Columns>
                        <ui:UIGridViewBoundColumn ID="ObjectID" PropertyName="ObjectID" HeaderText="ObjectID"
                            Visible="false" meta:resourcekey="SummaryTableUIGridViewBoundColumnObjectIDResource" >
                        </ui:UIGridViewBoundColumn>
                        <ui:UIGridViewBoundColumn ID="ObjectName" PropertyName="level / Size Type" HeaderText="level / Size Type" meta:resourcekey="SummaryTableUIGridViewBoundColumnlevelSizeTypeResource" >
                        </ui:UIGridViewBoundColumn>
                        <ui:UIGridViewButtonColumn ID="CheckButton" ButtonType="Image" CommandName="Check" ImageUrl="images/view.gif" meta:resourcekey="SummaryTableUIGridViewButtonColumnCheckResource" >
                        </ui:UIGridViewButtonColumn>
                        <ui:UIGridViewBoundColumn ID="Locker" PropertyName="Locker" HeaderText="Locker" meta:resourcekey="SummaryTableUIGridViewBoundColumnLockerResource" >
                        </ui:UIGridViewBoundColumn>
                        <ui:UIGridViewBoundColumn ID="XXSmall" PropertyName="XX Small" HeaderText="XX Small" meta:resourcekey="SummaryTableUIGridViewBoundColumnXXSmallResource" >
                        </ui:UIGridViewBoundColumn>
                        <ui:UIGridViewBoundColumn ID="XSmall" PropertyName="X Small" HeaderText="X Small" meta:resourcekey="SummaryTableUIGridViewBoundColumnXSmallResource" >
                        </ui:UIGridViewBoundColumn>
                        <ui:UIGridViewBoundColumn ID="Small" PropertyName="Small" HeaderText="Small" meta:resourcekey="SummaryTableUIGridViewBoundColumnSmallResource" >
                        </ui:UIGridViewBoundColumn>
                        <ui:UIGridViewBoundColumn ID="Medium" PropertyName="Medium" HeaderText="Medium" meta:resourcekey="SummaryTableUIGridViewBoundColumnMediumResource" >
                        </ui:UIGridViewBoundColumn>
                        <ui:UIGridViewBoundColumn ID="Large" PropertyName="Large" HeaderText="Large" meta:resourcekey="SummaryTableUIGridViewBoundColumnLargeResource" >
                        </ui:UIGridViewBoundColumn>
                        <ui:UIGridViewBoundColumn ID="XLarge" PropertyName="X Large" HeaderText="X Large" meta:resourcekey="SummaryTableUIGridViewBoundColumnXLargeResource" >
                        </ui:UIGridViewBoundColumn>
                        <ui:UIGridViewBoundColumn ID="XXLarge" PropertyName="XX Large" HeaderText="XX Large" meta:resourcekey="SummaryTableUIGridViewBoundColumnXXLargeResource" >
                        </ui:UIGridViewBoundColumn>
                        <ui:UIGridViewBoundColumn ID="XXXLarge" PropertyName="XXX Large" HeaderText="XXX Large" meta:resourcekey="SummaryTableUIGridViewBoundColumnXXXLargeResource" >
                        </ui:UIGridViewBoundColumn>
                        <ui:UIGridViewBoundColumn ID="Total" PropertyName="Total" HeaderText="Total" meta:resourcekey="SummaryTableUIGridViewBoundColumnTotalResource" >
                        </ui:UIGridViewBoundColumn>
                    </Columns>
                </ui:UIGridView>--%>
                    </div>

                </div>
            </ui:UIPanel>
        </div>
    </form>
    <%-- <input type="button" id="btnEdit" runat="server" onclick="highlight('testanimation')" />--%>
</body>
</html>

<script src="scripts/pngfix.js" type="text/javascript"></script>

<script src="scripts/boxover.js" type="text/javascript"></script>

<script src="scripts/nouislider/nouislider.min.js"></script>
<script src="scripts/nouislider/wNumb.js"></script>

<script type="text/javascript">
    //function refreshDate() {
    //    var dateValue = $("#slider").slider("value"),
    //    d = new Date();
    //    d.setTime(dateValue);
    //    $("#slider_data").text(d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate());
    //    //$("#swatch").css("background-color", "#" + hex);
    //}

    //function init() {
    //    var $j = jQuery.noConflict();
    //    //$j("#datepicker").datepicker();
    //    var currentTime = new Date();
    //    $("#slider").slider({

    //        orientation: "horizontal",
    //        //range: "min",
    //        min: (new Date()).setFullYear(currentTime.getFullYear() - 1),
    //        max: (new Date()).setFullYear(currentTime.getFullYear() + 1),
    //        value: (new Date()).getTime(),
    //        step: 86400000,
    //        slide: refreshDate
    //        //change: refreshDate
    //    })
    //}
    function ReceiveData(info) {
        var dynamicCtl =
            document.getElementById('dynamic');
        dynamicCtl.innerHTML = info;
    }

    function timestamp(str) {
        return new Date(str).getTime();
    }

    function init() {
        var dateSlider = document.getElementById('slider-date');
        var currentTime = new Date();
        noUiSlider.create(dateSlider, {
            // Create two timestamps to define a range.
            range: {
                min: (new Date()).setFullYear(currentTime.getFullYear() - 2),
                max: (new Date()).setFullYear(currentTime.getFullYear() + 2)
            },

            // Steps of one week
            step: 7 * 24 * 60 * 60 * 1000,

            // Two more timestamps indicate the handle starting positions.
            start: [currentTime.getTime()],

            //orientation: "vertical",
            //No decimals
            format: wNumb({
                decimals: 0
            })
        });

        var dateValues =
            document.getElementById('event-start');

        dateSlider.noUiSlider.on('update', function (values) {
            dateValues.innerHTML = formatDate(new Date(+values));
        });

        dateSlider.noUiSlider.on('change', function(values) {
            var newDate = new Date(+values);
            //if (newDate)
            //updateSliderRange((new Date()).setFullYear(newDate.getFullYear() - 1), (new Date()).setFullYear(newDate.getFullYear() + 1));
            Callback((new Date(+values)).toISOString().substring(0, 10));
        });
    }

    function updateSliderRange(min, max) {
        var dateSlider = document.getElementById('slider-date');
        dateSlider.noUiSlider.updateOptions({
            range: {
                'min': min,
                'max': max
            }
        });
    }


</script>

<script type="text/javascript">
    // Create a list of day and monthnames.
    var
        weekdays = [
            "Sunday", "Monday", "Tuesday",
            "Wednesday", "Thursday", "Friday",
            "Saturday"
        ],
        months = [
            "January", "February", "March",
            "April", "May", "June", "July",
            "August", "September", "October",
            "November", "December"
        ];
</script>
<script type="text/javascript">
    // Append a suffix to dates.
    // Example: 23 => 23rd, 1 => 1st.
    function nth(d) {
        if (d > 3 && d < 21) return 'th';
        switch (d % 10) {
            case 1: return "st";
            case 2: return "nd";
            case 3: return "rd";
            default: return "th";
        }
    }

    // Create a string representation of the date.
    function formatDate(date) {
        return weekdays[date.getDay()] + ", " +
            date.getDate() + nth(date.getDate()) + " " +
            months[date.getMonth()] + " " +
            date.getFullYear();
    }

    function formatDate2(date) {
        return date.getDate() + "/" +
            (date.getMonth() + 1) + "/" +
            date.getFullYear();
    }

</script>


<script type="text/javascript">
    function highlight(id, check) {
        //alert(id);
        //$("#"+id).effect("bounce",
        // { times: 10 }, 500);
        var item = $("#" + id);
        if (document.getElementById("checkbox" + id).checked)
            $(item).addClass('animated tada highlight');
        else
            $(item).removeClass('animated tada highlight');
        //$("#" + id).effect("highlight");
    }
</script>

