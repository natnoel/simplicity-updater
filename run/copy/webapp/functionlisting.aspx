﻿<%@ Page Language="C#" Theme="Corporate" Inherits="PageBase" Culture="auto" meta:resourcekey="PageResource1"
    UICulture="auto" %>

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="Anacle.DataFramework" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="LogicLayer" %>

<script runat="server">
    DataTable dtMenu;
    DataTable dtReports;
    string categoryName;

    struct FunctionItem
    {
        public string Url;
        public string Text;
        public string Code;

        public FunctionItem(string url, string code, string text)
        {
            this.Url = url;
            this.Text = text;
            this.Code = code;
            if (this.Code == "")
            {
                this.Code = "-";
            }
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        if (!IsPostBack)
        {
            if (Request["ID"] == null)
                Response.Redirect(Page.ResolveUrl("~/appobjectnoaccess.aspx"));

            string[] args = Security.Decrypt(Request["ID"]).Split(':');

            // Check for the salt in ID 
            //
            if (args.Length < 1 || args[args.Length - 1] != AppSession.SaltID)
                Response.Redirect(Page.ResolveUrl("~/appobjectnoaccess.aspx"));

            categoryName = args[0];

            List<string> subCategories = new List<string>();
            Dictionary<string, List<FunctionItem>> menu = new Dictionary<string, List<FunctionItem>>();
            if (args[0] != "##Reports##")
            {
                // load menu items from the database.
                //
                DataTable dt = Helpers.GetCachedMenusAccessibleByUser(AppSession.User, args[0]);
                dtMenu = dt;

                foreach (DataRow dr in dt.Rows)
                {
                    string subCategory = dr["SubCategoryName"].ToString();
                    string mainUrl = dr["MainUrl"].ToString();
                    if (!menu.ContainsKey(subCategory))
                    {
                        subCategories.Add(subCategory);
                        menu[subCategory] = new List<FunctionItem>();
                    }

                    menu[subCategory].Add(new FunctionItem(mainUrl + (mainUrl.Contains("?") ? "&" : "?") + "TYPE="
                            + HttpUtility.UrlEncode(Security.Encrypt(dr["ObjectTypeName"].ToString() + ":" + AppSession.SaltID)),
                                dr["FunctionCode"].ToString(),
                                TranslateMenuItem(dr["FunctionName"].ToString())));
                }
            }
            else
            {
                // load the reports from the database.
                //
                DataTable reports = Helpers.GetCachedReportsAccessibleByUser(AppSession.User);
                dtReports = reports;

                foreach (DataRow dr in reports.Rows)
                {
                    string category = dr["CategoryName"].ToString();

                    if (!menu.ContainsKey(category))
                    {
                        subCategories.Add(category);
                        menu[category] = new List<FunctionItem>();
                    }

                    menu[category].Add(new FunctionItem(
                        "~/modules/reportviewer/search.aspx?ID=" +
                        HttpUtility.UrlEncode(Security.Encrypt(dr["ObjectID"].ToString() + ":" + AppSession.SaltID)),
                        dr["ReportCode"].ToString(),
                        TranslateReportItem(dr["ReportName"].ToString())
                        ));

                }
            }
            
        }
    }


    /// <summary>
    /// Translates the specified text from the objects.resx file.
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    protected string TranslateMenuItem(string text)
    {
        if (text == "##Reports##")
            return Resources.Strings.Menu_Reports;

        string translatedText = Resources.Objects.ResourceManager.GetString(text);
        if (translatedText == null || translatedText == "")
            return text;
        return translatedText;
    }

    /// <summary>
    /// Translates the specified text from the reports.resx file.
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    protected string TranslateReportItem(string text)
    {
        string translatedText = Resources.Reports.ResourceManager.GetString(text);
        if (translatedText == null || translatedText == "")
            return text;
        return translatedText;
    }

    /// <summary>
    /// Gets the second level category menus
    /// </summary>
    /// <returns></returns>
    protected DataTable GetSecondLevelMenu(string categoryName)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("CategoryName", typeof(string));
        dt.Columns.Add("SubCategoryName", typeof(string));

        Hashtable ht = new Hashtable();
        foreach (DataRow dr in dtMenu.Rows)
        {
            if (dr["CategoryName"] as String == categoryName && ht[dr["SubCategoryName"] as String] == null)
            {
                dt.Rows.Add(dr["CategoryName"], dr["SubCategoryName"]);
                ht[dr["SubCategoryName"] as String] = 1;
            }
        }

        return dt;
    }

    /// <summary>
    /// Gets the leaf level category menus
    /// </summary>
    /// <returns></returns>
    protected DataTable GetThirdLevelMenu(string categoryName, string subCategoryName)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("CategoryName", typeof(string));
        dt.Columns.Add("SubCategoryName", typeof(string));
        dt.Columns.Add("FunctionName", typeof(string));
        dt.Columns.Add("ObjectTypeName", typeof(string));
        dt.Columns.Add("MainUrl", typeof(string));
        dt.Columns.Add("MainMobileUrl", typeof(string));
        dt.Columns.Add("FunctionCode", typeof(string));

        foreach (DataRow dr in dtMenu.Rows)
        {
            if (dr["CategoryName"] as String == categoryName &&
                dr["SubCategoryName"] as String == subCategoryName)
            {
                dt.Rows.Add(categoryName, subCategoryName, dr["FunctionName"], dr["ObjectTypeName"], dr["MainUrl"], dr["MainMobileUrl"], dr["FunctionCode"]);
            }
        }
        return dt;

    }

    /// <summary>
    /// Gets the second level category reports
    /// </summary>
    /// <returns></returns>
    protected DataTable GetSecondLevelReports()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("CategoryName", typeof(string));

        Hashtable ht = new Hashtable();
        foreach (DataRow dr in dtReports.Rows)
        {
            string categoryName = dr["CategoryName"] as String;
            if (ht[categoryName] == null)
            {
                dt.Rows.Add(categoryName);
                ht[categoryName] = 1;
            }
        }

        return dt;
    }


    /// <summary>
    /// Gets the third level category reports
    /// </summary>
    /// <returns></returns>
    protected DataTable GetThirdLevelReports(string categoryName)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("ObjectID", typeof(string));
        dt.Columns.Add("ReportName", typeof(string));
        dt.Columns.Add("ReportCode", typeof(string));

        foreach (DataRow dr in dtReports.Rows)
        {
            if (dr["CategoryName"] as String == categoryName)
            {
                dt.Rows.Add(dr["ObjectID"], dr["ReportName"], dr["ReportCode"]);
            }
        }
        return dt;
    }

    /// <summary>
    /// Generates the URL required for the functions.
    /// </summary>
    /// <param name="objectTypeName"></param>
    /// <param name="mainUrl"></param>
    /// <returns></returns>
    protected string GetMenuUrl(string objectTypeName, string mainUrl)
    {
        return
            ResolveUrl(mainUrl + (mainUrl.Contains("?") ? "&" : "?") +
            "TYPE=" +
            HttpUtility.UrlEncode(Security.Encrypt(objectTypeName + ":" + AppSession.SaltID)));

    }


    /// <summary>
    /// Generates the URL required for the reports.
    /// </summary>
    /// <param name="objectTypeName"></param>
    /// <param name="mainUrl"></param>
    /// <returns></returns>
    protected string GetReportUrl(string objectID)
    {
        return
            ResolveUrl("~/modules/reportviewer/search.aspx?ID=" +
            HttpUtility.UrlEncode(Security.Encrypt(objectID.ToString() + ":" + AppSession.SaltID)));
    }


</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Anacle.EAM</title>
    <meta http-equiv="pragma" content="no-cache" />
</head>
<body class="body-function-bg">
    <form id="form1" runat="server">
        <div class="holder">

            <div runat="server">
                <% if (categoryName != "##Reports##") { %>
                    <span class="mdl-function-listing-title"><%= TranslateMenuItem(categoryName) %></span>
                    <ul class="mdl-function-listing-level2" >
                        <% foreach (DataRow dr2 in GetSecondLevelMenu(categoryName).Rows) { %>
                            <li>
                                <ul class="mdl-function-listing-level3">
                                    <li class="mdl-function-listing__header">
                                        <a href="#">
                                            <% var subCategoryName = (dr2["SubCategoryName"] as String == "") ? categoryName : (dr2["SubCategoryName"] as String); %>
                                            <%= Helpers.CreateHtmlIcon(subCategoryName) %>
                                            <span class="mdl-function-listing__label"><%= TranslateMenuItem(subCategoryName) %></span>
                                        </a>
                                    </li>
                                    <%  var dt3 = GetThirdLevelMenu(dr2["CategoryName"] as String, dr2["SubCategoryName"] as String);
                                        foreach (DataRow dr3 in dt3.Rows) { %>
                                        <li class="mdl-function-listing__item ">
                                            <a href="javascript:void(0)" onclick="openPage('<%= GetMenuUrl(dr3["ObjectTypeName"] as String, dr3["MainUrl"] as String) %>', 'frameBottom')" class="mdl-custom-ripple mdl-custom-ripple-dark mdl-js-ripple-effect">
                                                <i></i>
                                                <span class="mdl-function-listing__code"><%= dr3["FunctionCode"] as String %></span>
                                                <span class="mdl-function-listing__label"><%= TranslateMenuItem(dr3["FunctionName"] as String) %></span>
                                            </a>
                                        </li> 
                                    <% } %>
                                </ul>
                            </li>
                        <% } %>
                    </ul>
                <% } else { %>
                    <asp:Label runat="server" ID="ReportLabel" CssClass="mdl-function-listing-title" Text="Reports"></asp:Label>
                    <ul class="mdl-function-listing-level2" >
                        <% foreach (DataRow dr2 in GetSecondLevelReports().Rows) { %>
                            <li>
                                <ul class="mdl-function-listing-level3">
                                    <li class="mdl-function-listing__header">
                                        <a href="#">
                                            <%= Helpers.CreateHtmlIcon(dr2["CategoryName"] as String) %>
                                            <span><%= TranslateMenuItem(dr2["CategoryName"] as String) %></span>
                                        </a>
                                    </li>
                                    <%  var dt3 = GetThirdLevelReports(dr2["CategoryName"] as String);
                                        foreach (DataRow dr3 in dt3.Rows) { %>
                                        <li class="mdl-function-listing__item">
                                            <a href="javascript:void(0)" onclick="openPage('<%= GetReportUrl(dr3["ObjectID"] as String) %>', 'frameBottom')" class="mdl-custom-ripple mdl-custom-ripple-dark mdl-js-ripple-effect">
                                                <i></i>
                                                <span class="mdl-function-listing__code"><%= dr3["ReportCode"] as String %></span>
                                                <span class="mdl-function-listing__label"><%= TranslateMenuItem(dr3["ReportName"] as String) %></span>
                                            </a>
                                        </li> 
                                    <% } %>
                                </ul>
                            </li>
                        <% } %>
                    </ul>
                <% } %>
            </div>
        </div>
    </form>
</body>
</html>
