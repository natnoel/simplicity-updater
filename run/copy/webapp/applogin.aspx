﻿<!DOCTYPE html>

<%@ Page Language="C#" CodeFile="~/applogin.aspx.cs" Inherits="applogin" UICulture="auto" Culture="auto" meta:resourcekey="PageResource1" %>

<%@ Register Assembly="MSCaptcha" Namespace="MSCaptcha" TagPrefix="cc2" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="Anacle.DataFramework" %>
<%@ Import Namespace="LogicLayer" %>

<script runat="server">
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        try
        {
            if (OApplicationSetting.Current.IsUsingActiveDirectory == 1)
            {
                login.PasswordRecoveryUrl = "";
                login.PasswordRecoveryText = "";
            }
            TextBox PasswordTextBox = login.FindControl("Password") as TextBox;

            UIButton LoginButton = login.FindControl("LoginButton") as UIButton;
            LoginButton.OnClickScript = "javascript:loginbutton_onclick(\"" + PasswordTextBox.ClientID + "\");";

            PasswordTextBox.Attributes["onkeydown"] = "javascript:if (event.keyCode == 13) { loginbutton_onclick(\"" + PasswordTextBox.ClientID + "\"); " + Page.ClientScript.GetPostBackEventReference(LoginButton.LinkButton, "") + "}";

            lblCopyright.Text = string.Format("COPYRIGHT &copy; {0} ANACLE SYSTEMS ALL RIGHTS RESERVED", DateTime.Today.Year);
        }
        catch (Exception)
        {
        }
    }
      
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Simplicity 9.0</title>
    <style type="text/css">
        html {
            display: none;
        }

        .loginlb {
            padding-top: 10px;
        }

        .logintb {
            padding-left: 6px;
            padding-right: 6px;
            padding-top: 6px;
            padding-bottom: 6px;
            background-color: #f7f3f7;
            border: solid 1px #efe7ef;
            font-size: 16pt;
            width: 250px;
            height: 30px;
        }

        a {
            font-size:14pt;
        }

        .btn {
            margin: 0 !important;
        }

        .validator 
        {
            margin-top: -18px;
            margin-bottom: ;
            position: absolute;
            font-size: 11px;
        }
        .button-container
        {
            padding-top: 20px;
        }

    </style>
    <meta id="viewport" name="viewport" content="width=device-width" />
    <style id="antiClickjack" type="text/css">
        body {
            display: none !important;
        }
    </style>
    <script type="text/javascript">
        var topOrigin = window.top.location.protocol + "//" + window.top.location.hostname + ":" + window.top.location.port;
        var selfOrigin = window.self.location.protocol + "//" + window.self.location.hostname + ":" + window.self.location.port;

        if (selfOrigin === topOrigin) {
            var antiClickjack = document.getElementById("antiClickjack");
            antiClickjack.parentNode.removeChild(antiClickjack);
        } else {
            window.top.location = "about:blank";
        }
    </script>
</head>
<body style="background-color: #fff" >

    <script type="text/javascript" src="scripts/rlogin.js"></script>

    <form id="form1" runat="server">

        <div runat="server">
            <script type="text/javascript">
                var clicked = false;

                function init() {
                    if (self == top) {
                        document.documentElement.style.display = "block";
                    } else {
                        top.location = self.location;
                    }

                    if (window.top) {
                        if (window.top.name.indexOf("AnacleEAM") == 0) {
                            if (window.top.opener) {
                                try {
                                    window.top.opener.location = "applogin.aspx";
                                    window.top.opener.focus();
                                }
                                catch (e) {
                                }
                            }
                            window.top.close();
                        }

                        if (window.top.location != window.location)
                            window.top.location = window.location;
                    }
                }

                function loginbutton_onclick(id) {
                    if (!clicked) {
                        clicked = true;
                        setMaxDigits(131);
                        var s = "<%=GetS()%>";
                        var e = "<%=GetE()%>";
                        var m = "<%=GetM()%>";
                        en(e, m, s, id);
                    }
                }

                $(document).ready(function () {
                    init();
                    var scale = $(window).width() / 400;
                    $("#viewport").attr("content", "width=400, user-scalable=no, initial-scale=" + scale + ", maximum-scale=" + scale + ", minimum-scale=" + scale);
                    $("#block").fadeOut(500);

                });

            </script>
        </div>

        <div id="body" style="padding: 0; position: relative" runat="server">
            <div style="background-color: #194680; height: 400px">
                <img id="bg-image" style="width: 100vw; height: 400px; object-fit: cover" src="apploginlogo.aspx" />
            </div>
            <div id="content" style="text-align: center; position: absolute; top: 50px; width: 100vw">
                <div style="display: inline-block; width: 400px; text-align:center">

                    <div class="mdl-card mdl-shadow--2dp" style="display:inline-block">
                        <div class="mdl-card__title">
                                            
                            <h2 class="mdl-card__title-text"><asp:Label ID="TitleText"  runat="server"></asp:Label></h2>
                        </div>
                        <div class="mdl-card__supporting-text mdl-card--border" style="text-align: left">

                            <asp:Login ID="login" runat="server" OnAuthenticate="Login1_Authenticate" OnLoggedIn="Login1_LoggedIn"
                                PasswordRecoveryText="" Font-Names="Tahoma" Font-Size="12px" DisplayRememberMe="False"
                                UserNameLabelText="Username" TextBoxStyle-CssClass="logintb" PasswordLabelText="Password"
                                TextLayout="TextOnTop" LabelStyle-CssClass="loginlb" PasswordRecoveryUrl="apploginreset.aspx"
                                LoginButtonStyle-CssClass="loginbt"  >
                                <LayoutTemplate>

                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                        <label class="mdl-textfield__label"><asp:Label runat="server" ID="UserNameLabel" Text="User Name"></asp:Label></label>
                                        <asp:TextBox ID="UserName" runat="server" CssClass="mdl-textfield__input"></asp:TextBox>
                                    </div>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="UserName"
                                        Text="Required" CssClass="validator"></asp:RequiredFieldValidator>

                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                        <label class="mdl-textfield__label"><asp:Label runat="server" ID="PasswordLabel" Text="Password"></asp:Label></label>
                                        <asp:TextBox ID="Password" runat="server" TextMode="Password" CssClass="mdl-textfield__input"></asp:TextBox>
                                    </div>
                                    <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                                        Text="Required" CssClass="validator"></asp:RequiredFieldValidator>

                                    <div style="color:red">
                                        <asp:Literal ID="FailureText" runat="server" ></asp:Literal>
                                    </div>

                                    <div class="button-container" style="text-align: left">
                                        <ui:UIButton runat="server" ID="LoginButton" CommandName="Login" Text="Login" ButtonColor="Blue" ButtonType="NormalRaised" style="width: 100%" />
                                        <br />
                                        <br />
                                        <asp:HyperLink runat="server" ID="HyperLink1" NavigateUrl="apploginreset.aspx" CssClass="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--default " 
                                            Visible="true" style="width: 100%; padding: 0">Forgot Password</asp:HyperLink>
                                    </div>

                                    <table style="font-size: 14pt; text-align: left; width:250px; color: #555;">
                                        <tr>
                                            <td style="color: Red;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Panel runat="server" ID="panelCaptha">
                                                    <asp:Label runat="server" ID="CaptchaLabel2" Text="Please input the code you see below" />
                                                    <cc2:CaptchaControl ID="ccJoin" runat="server" CaptchaBackgroundNoise="none" CaptchaLength="5"
                                                        CaptchaHeight="60" CaptchaWidth="200" CaptchaLineNoise="None" CaptchaMinTimeout="5"
                                                        CaptchaMaxTimeout="240" />
                                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                        <label class="mdl-textfield__label"><asp:Label runat="server" ID="CaptchaLabel" Text="Code"></asp:Label></label>
                                                        <asp:TextBox ID="CaptchaTextBox" runat="server" TextMode="SingleLine" CssClass="logintb"></asp:TextBox>
                                                    </div>
                                                    <asp:RequiredFieldValidator ID="CaptchaTextBoxRequiredFieldValidator" runat="server"
                                                        ControlToValidate="CaptchaTextBox" Text="*"></asp:RequiredFieldValidator>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                        <tr style="color: black;">
                                            <td>
                                                <div style="float: left;">
                                                    <asp:CheckBox runat="server" ID="MobileSiteCheckBox" Text="Mobile version for handphones" />
                                                </div>
                                                <div style="float: right;">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </LayoutTemplate>
                            </asp:Login>

                        </div>
                        <div class="mdl-card__supporting-text mdl-card--border" style="text-align: left; background-color: #eeeeee">
                            <asp:Label runat="server" ID="labelLicense" meta:resourcekey="labelLicenseResource1" style="font-size:11px"></asp:Label>
                            <br />
                            <br />
                            <asp:Label runat="server" ID="lblCopyright" style="font-size:11px; color: #bdbdbd"></asp:Label>
                        </div>
                    </div>

                    <asp:TextBox runat="server" ID="textMyKey" Visible="false"></asp:TextBox>
                    <asp:Panel runat="server" ID="panelSchemaError" Visible="False" meta:resourcekey="panelSchemaErrorResource1" style="padding-top:20px; padding-bottom: 20px; ">
                        <asp:Label runat="server" ID="labelSchemaError1" Text="Unable to load Application Settings."
                            meta:resourcekey="labelSchemaError1Resource1"></asp:Label>
                        <br />
                        <asp:Label runat="server" ID="labelSchemaErrorException" ForeColor="Red" meta:resourcekey="labelSchemaErrorExceptionResource1"></asp:Label>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="panelNoAccess" Visible="false" style="padding-top:20px; padding-bottom: 20px;">
                        <b>Access Denied</b>
                        <br />
                        Your user account "<asp:Label runat="server" ID="labelUserLoginName"></asp:Label>"
                        was not granted access to this system.<br />
                        <br />
                        Please contact your administrator.
                    </asp:Panel>
                    <asp:Panel runat="server" ID="panelNoAccessNotLoggedOn" Visible="false" style="padding-top:20px; padding-bottom: 20px; ">
                        <b>Access Denied</b>
                        <br />
                        You are not logged as a valid Windows user.
                        <br />
                        Please contact your administrator.
                    </asp:Panel>

                    <asp:Panel runat="server" ID="panelSql" meta:resourcekey="panelSqlResource1" Visible="false"  style="text-align:center; padding-top:20px; padding-bottom: 20px; margin: auto; font-size: 12px">
                        <asp:Label runat="server" ID="labelGeneralSql">SQL Scripts</asp:Label><br />
                        <asp:LinkButton runat="server" ID="buttonGenerateScript" OnClick="buttonGenerateScript_Click"
                            CausesValidation="false" style="font-size: 12px">Application Schema</asp:LinkButton> |
                        <asp:LinkButton runat="server" ID="buttonGenerateScriptForAuditTrail" OnClick="buttonGenerateScriptAuditTrail_Click"
                            CausesValidation="false" style="font-size: 12px" meta:resourcekey="buttonGenerateScriptAuditTrailResource1">Audit Trails Schema</asp:LinkButton> |
                        <asp:LinkButton runat="server" ID="buttonGenerateScriptForWorkflow" OnClick="buttonGenerateScriptForWorkflow_Click"
                            CausesValidation="false" style="font-size: 12px">Workflow Schema</asp:LinkButton>
                    </asp:Panel>

                </div>

            </div>
        </div>
    </form>
</body>
</html>
