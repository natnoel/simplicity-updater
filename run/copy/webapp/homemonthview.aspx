<%@ Page Language="C#" Theme="Corporate" Inherits="PageBase" Culture="auto" meta:resourcekey="PageResource1"
    UICulture="auto" %>

<%@ Import Namespace="System" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="System.Web.UI" %>
<%@ Import Namespace="System.Web.UI.WebControls" %>
<%@ Import Namespace="Anacle.DataFramework" %>
<%@ Import Namespace="LogicLayer" %>
<%@ Import Namespace="Newtonsoft.Json.Linq" %>

<script runat="server">

    public bool flag = false;

    // kf begin: removed to optimize home page loading
    //private List<OActivity> activitiesForTheMonth = null;
    private DataTable dtActivitiesForTheMonth;
    private DataTable dtActivitiesForTheUser;
    // kf end

    //-------------------------------------------------------------------
    // event
    //-------------------------------------------------------------------
    protected override void OnLoad(EventArgs e)
    {
        if (Request["ObjectID"] != null)
        {
            dtActivitiesForTheUser = OActivity.GetAllActivities(AppSession.User);
            DataRow[] foundrows = dtActivitiesForTheUser.Select("ObjectID='" + Request["ObjectID"] + "'");
            if (foundrows.Length == 0)
            {
                Response.Write("Nothing found for " + Request["ObjectID"]);
            }
            else
            {
                foreach (DataRow dr in foundrows)
                {
                    string objType = Resources.Objects.ResourceManager.GetString(dr["ObjectTypeName"].ToString());
                    string status = Resources.WorkflowStates.ResourceManager.GetString(dr["Status"].ToString());

                    Response.Write(objType + "<br>");
                    Response.Write(status + "<br>");
                    //Response.Write(dr["TaskNumber"] + "<br>");

                    if (dr["Priority"].ToString() == "0")
                        Response.Write("Normal<br>");
                    else if (dr["Priority"].ToString() == "1")
                        Response.Write("Urgent<br>");
                    else if (dr["Priority"].ToString() == "2")
                        Response.Write("Emergency<br>");
                    else if (dr["Priority"].ToString() == "3")
                        Response.Write("Very Emergency<br>");
                    else
                        Response.Write(dr["Priority"] + "<br>");

                    Response.Write("Start " + dr["ScheduledStartDateTime"] + "<br>");
                    Response.Write("End " + dr["ScheduledEndDateTime"] + "<br>");
                }
            }

            Response.End();
        }

        base.OnLoad(e);

        if (!IsPostBack)
        {
            //20130304 PTB: Use App Setting Home URL
            DateTime now = DateTime.Now;
            dtActivitiesForTheMonth = OActivity.GetMonthActivities(AppSession.User, now.Year, now.Month);
            Session.Add("dtActivities", dtActivitiesForTheMonth);
            PopulateForm();

            System.Globalization.CultureInfo ci = System.Threading.Thread.CurrentThread.CurrentUICulture;

            // Saves the inbox path into Session to support global search.
            // 
            this.Session["CurrentSearchPath"] = this.Page.Request.ApplicationPath + "/home.aspx";
        }
        else dtActivitiesForTheMonth = (DataTable)Session["dtActivities"];
        dtActivitiesForTheUser = OActivity.GetAllActivities(AppSession.User);
        /*JArray jdt = JArray.FromObject(dtActivitiesForTheUser);
        Response.Write(jdt.ToString());*/

        if (functionHash == null)
        {
            functionHash = new Hashtable();
            List<OFunction> functions = OFunction.GetAllFunctions();
            foreach (OFunction function in functions)
                functionHash[function.ObjectTypeName] = function;
        }

        ScriptManager.RegisterStartupScript(this, this.GetType(), "plotly", @"
$(document).ready(function () {
    $('#calendar').fullCalendar({
        // put your options and callbacks here
        height: 700,
        header: {
            center: 'month,agendaWeek,agendaDay'
        },
        timeFormat: 'hh:mm a',
        eventClick: function(event) {
            if (event.url) {
                openPage(event.url, 'AnacleEAM_Window');
                return false;
            }
        },
        displayEventTime: false,
        eventRender: function(event, element) {
            element.tipso({
                titleContent: event.title,
                //titleColor: event.textColor,
                //titleBackground: event.color,
                background: event.color,
                color: event.textColor,
                ajaxContentUrl : window.location.href + '?ObjectID=' + event.id
            });
        },
        events: " + getFullCalendarEvents(dtActivitiesForTheUser) + @"
    })
    //$('.fc-button').addClass('mdl-button mdl-js-button mdl-js-ripple-effect');
});
            ", true);
    }

    private string getFullCalendarEvents(DataTable dt)
    {
        JArray jaEvents = new JArray();

        foreach (DataRow dr in dt.Rows)
        {
            dynamic joEvent = new JObject();
            joEvent.id = dr["ObjectID"];
            joEvent.title = dr["TaskNumber"] + " " + dr["TaskName"];
            joEvent.start = dr["ScheduledStartDateTime"];
            joEvent.end = dr["ScheduledEndDateTime"];
            /*
            string priority = getPriority((int?)dr["Priority"]);
            if (priority == "0")
                joEvent.color = "#2196f3";  //Blue
            else if (priority == "1")
                joEvent.color = "#C0CA33";  //Green
            else if (priority == "2")
            {
                joEvent.color = "#FDD835";  //Yellow
                joEvent.textColor = "black";
            }
            else if (priority == "3")
                joEvent.color = "#ff5252";  //Red
            */

            string workflowStateCssClass = Helpers.GetWorkflowStateClass(dr["Status"].ToString());
            if ("mdl-chip-gray" == workflowStateCssClass)
                joEvent.color = "#bdbdbd";
            else if ("mdl-chip-yellow" == workflowStateCssClass)
                joEvent.color = "#FDD835";
            else if ("mdl-chip-red" == workflowStateCssClass)
                joEvent.color = "rgb(255,82,82)";
            else if ("mdl-chip-blue" == workflowStateCssClass)
                joEvent.color = "rgb(33,150,243)";
            else if ("mdl-chip-green" == workflowStateCssClass)
                joEvent.color = "#C0CA33";

            //Get the url
            string objectType = (string)dr["ObjectTypeName"];
            OFunction function = functionHash[objectType] as OFunction;
            if (function != null) {
                string p = ResolveUrl(function.EditUrl) +
                            "?ID=" + HttpUtility.UrlEncode(Security.Encrypt("EDIT:" + dr["AttachedObjectID"].ToString() + ":" + AppSession.SaltID)) +
                            "&TYPE=" + HttpUtility.UrlEncode(Security.Encrypt(objectType + ":" + AppSession.SaltID));
                joEvent.url = p;
            }
            jaEvents.Add(joEvent);
        }

        return jaEvents.ToString();
    }

    //-------------------------------------------------------------------
    // event
    //-------------------------------------------------------------------
    protected void PopulateForm()
    {
    }


    //-------------------------------------------------------------------
    // event
    //-------------------------------------------------------------------
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
    }

    protected string encodeString(string s)
    {
        return s.Replace("\"", "\"\"");
    }

    protected void formRow(StringBuilder sb, string columnName, object val)
    {
        formRow(sb, columnName, val, null);
    }

    protected void formRow(StringBuilder sb, string columnName, object val, string dataFormatString)
    {
        sb.Append("<tr><td width='100px'>");
        sb.Append(Resources.Strings.ResourceManager.GetString(columnName));     // translate it from strings.resxs
        sb.Append("</td><td>");
        if (val != null && val != DBNull.Value)
        {
            if (dataFormatString == null)
                sb.Append(encodeString(val.ToString()));
            else
                sb.Append(encodeString(String.Format(dataFormatString, val)));
        }
        sb.Append("</td></tr>");
    }

    protected string getPriority(int? priority)
    {
        if (priority == null)
            return "";
        else
            return priority.ToString();
    }

    [Obsolete]
    protected string formTable(OActivity activity)
    {
        StringBuilder sb = new StringBuilder();
        formRow(sb, "Task_Status", Resources.Objects.ResourceManager.GetString(activity.ObjectName));
        formRow(sb, "Task_Priority", "<img src='" + ResolveUrl("~/images/") + "priority" + getPriority(activity.Priority) + ".gif' align='absmiddle'> ");
        formRow(sb, "Task_ScheduledStartDateTime", activity.ScheduledStartDateTime, "{0:dd-MMM-yyyy hh:mm tt}");
        formRow(sb, "Task_ScheduledEndDateTime", activity.ScheduledEndDateTime, "{0:dd-MMM-yyyy hh:mm tt}");

        return sb.ToString();
    }


    // kf begin: added to improve loading performance at home page    
    //-------------------------------------------------------------------
    /// <summary>
    /// Form the tool tip table from the activity DataRow.
    /// </summary>
    /// <param name="dr"></param>
    /// <returns></returns>
    //-------------------------------------------------------------------
    protected string formTableFromActivity(DataRow dr)
    {
        StringBuilder sb = new StringBuilder();
        formRow(sb, "Task_ObjectName", dr["TaskName"]);
        formRow(sb, "Task_Status", Resources.Objects.ResourceManager.GetString(dr["Status"].ToString()));
        formRow(sb, "Task_Priority", "<img src='" + ResolveUrl("~/images/") + "priority" + getPriority((int?)dr["Priority"]) + ".gif' align='absmiddle'> ");
        formRow(sb, "Task_ScheduledStartDateTime", dr["ScheduledStartDateTime"], "{0:dd-MMM-yyyy hh:mm tt}");
        formRow(sb, "Task_ScheduledEndDateTime", dr["ScheduledEndDateTime"], "{0:dd-MMM-yyyy hh:mm tt}");

        return sb.ToString();
    }
    // kf: end


    //-------------------------------------------------------------------
    // event
    //-------------------------------------------------------------------
    // kf begin: added to improve loading speed of home page
    Hashtable functionHash = null;


</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Anacle.EAM</title>
    <link href="App_Themes/Corporate/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link rel='stylesheet' href='css/fullcalendar.css' />
    <link rel='stylesheet' href='css/materialFullCalendar.css' />
    <link rel='stylesheet' href='css/tipso.css' />
    <style>
        .tabstrip {
            display: none
        }
        #form1 {
            display: none
        }
        .fc-scroller {
           overflow-y: hidden !important;
        }
        table
        {
            background-color: white;
        }

    </style>
</head>
<body class="body-home-bg">
    <form id="form1" runat="server">
        <div class="mdl-title-bar-spacer">
            <div class="mdl-title-bar">
                <div>
                    <div style="float:left">
                        <asp:label runat="server" ID="titleLabel" CssClass="mdl-title-bar-label" Text="Calendar"></asp:label>
                    </div>
                </div>
            </div>
        </div>

        <script src='scripts/jtip.js' type='text/javascript'></script>

    </form>
    <style>
        .fc-toolbar.fc-header-toolbar {
            padding: 1em 1em 0 1em;
        }

        /* title text */
        .fc-toolbar h2 {
            font-size: 24px;
            font-weight: bold;
        }

        .fc-toolbar .fc-button {
            margin-top: 6px;
            margin-bottom: 6px;
        }

        #calendar {
            padding:0 12px 12px 12px;
        }
    </style>
    <div id='calendar'></div>

    <script type='text/javascript' src='scripts/fullcalendar/jquery.min.js'></script>
    <script type='text/javascript' src='scripts/fullcalendar/moment.min.js'></script>
    <script type='text/javascript' src='scripts/fullcalendar/fullcalendar.min.js'></script>
    <script type='text/javascript' src='scripts/tipso.min.js'></script>
</body>
</html>
