using System;
using System.Globalization;
using System.Threading;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Collections;
using System.DirectoryServices;
using System.IO;
using System.Security.Cryptography;
using MSCaptcha;

using Anacle.DataFramework;
using LogicLayer;
using Anacle.UIFramework;

/// <summary>
/// Summary description for applogin
/// </summary>
public partial class applogin : PageBase
{
    private const int saltNoOfDigits = 10;
    public applogin()
    {
        //
        // TODO: Add constructor logic here
        //
    }


    // KF BEGIN 2005.05.07
    /// ------------------------------------------------------------------
    /// <summary>
    /// Clear session.
    /// </summary>
    /// <param name="e"></param>
    /// ------------------------------------------------------------------
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        Session.Clear();
        Audit.UserName = null;
        Workflow.CurrentUser = null;
    }
    // KF END

    /// ------------------------------------------------------------------
    /// <summary>
    /// Initialize the page culture.
    /// </summary>
    /// ------------------------------------------------------------------
    protected override void InitializeCulture()
    {
        string cultureName = System.Configuration.ConfigurationManager.AppSettings["LoginPage_UICulture"];
        if (cultureName == null)
            cultureName = "en-US";

        Thread.CurrentThread.CurrentCulture = new CultureInfo(cultureName);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(cultureName);

        this.Culture = Thread.CurrentThread.CurrentCulture.Name;
        this.UICulture = Thread.CurrentThread.CurrentUICulture.Name;

        Resources.Errors.Culture = Thread.CurrentThread.CurrentCulture;
        Resources.Messages.Culture = Thread.CurrentThread.CurrentCulture;
        Resources.Notifications.Culture = Thread.CurrentThread.CurrentCulture;
        Resources.Objects.Culture = Thread.CurrentThread.CurrentCulture;
        Resources.Roles.Culture = Thread.CurrentThread.CurrentCulture;
        Resources.Strings.Culture = Thread.CurrentThread.CurrentCulture;
        Resources.WorkflowEvents.Culture = Thread.CurrentThread.CurrentCulture;
        Resources.WorkflowStates.Culture = Thread.CurrentThread.CurrentCulture;

        LogicLayer.Global.SetCulture(Thread.CurrentThread.CurrentCulture);
        Anacle.UIFramework.Global.SetCulture(Thread.CurrentThread.CurrentCulture);

        base.InitializeCulture();
    }

    private string passwordSalt = "";
    public void InitializePasswordSalt()
    {
        StringBuilder randomString = new StringBuilder();

        Random rnd = new Random();

        for (int i = 0; i < saltNoOfDigits; i++)
            randomString.Append(rnd.Next(10));

        textMyKey.Text = Security.HashString(randomString.ToString());
        passwordSalt = randomString.ToString();
    }

    public string GetS()
    {
        return passwordSalt;
    }

    /*
    private string GetSalt(string key)
    {
        if (HttpContext.Current.Cache[key] == null)
            return null;
        else
            return HttpContext.Current.Cache[key].ToString();
        return textMyKey.Text;
    }
    */
    /*
    private void RemoveSalt(string key)
    {
        //HttpContext.Current.Cache.Remove(key);
        InitializePasswordSalt();
    }
    */

    private RSAParameters param;

    private RSACryptoServiceProvider getRSACryptoServiceProvider()
    {
        RSACryptoServiceProvider rsa = (RSACryptoServiceProvider)Application["RSA"];
        if (rsa == null)
        {
            CspParameters cspParams = new CspParameters();
            cspParams.Flags = CspProviderFlags.UseMachineKeyStore;
            rsa = new RSACryptoServiceProvider(1024);

            Application["RSA"] = rsa;
        }

        param = rsa.ExportParameters(false);
        
        return rsa;
    }

    // for embedding in the JavaScript
    protected string GetE()
    {
        return StringHelper.BytesToHexString(param.Exponent);
    }

    // for embedding in the JavaScript
    protected string GetM()
    {
        return StringHelper.BytesToHexString(param.Modulus);
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        getRSACryptoServiceProvider();

        // Disable auto-complete
        //
        TextBox UserNameTextBox = login.FindControl("UserName") as TextBox;
        TextBox PasswordTextBox = login.FindControl("Password") as TextBox;
        if (UserNameTextBox != null)
        {
            UserNameTextBox.Attributes["autocomplete"] = "off";
            UserNameTextBox.AutoCompleteType = AutoCompleteType.Disabled;
        }
        if (PasswordTextBox != null)
        {
            PasswordTextBox.Attributes["autocomplete"] = "off";
            PasswordTextBox.AutoCompleteType = AutoCompleteType.Disabled;
        }

        //captcha
        try
        {
            Panel CaptchaPanel = login.FindControl("panelCaptha") as Panel;
            if (CaptchaPanel != null)
            {
                CaptchaPanel.Visible = OApplicationSetting.Current.IsUsingCaptcha == 1;
                TextBox CaptchaTextBox = login.FindControl("CaptchaTextBox") as TextBox;
                if (CaptchaTextBox != null)
                {
                    CaptchaTextBox.Attributes["autocomplete"] = "off";
                    CaptchaTextBox.AutoCompleteType = AutoCompleteType.Disabled;
                }
            }
        }
        catch (Exception ex)
        {
            login.Visible = false;
            panelSchemaError.Visible = true;
            labelSchemaErrorException.Text = ex.Message;
            Helpers.LogException(ex, this.Request);
        }
        

        if (!IsPostBack)
        {
            this.Page.Title = System.Configuration.ConfigurationManager.AppSettings["PageTitle"];

            // Initializes the salt used for password
            // encryption.
            //
            InitializePasswordSalt();

            // Create a try catch block to prevent failure, in 
            // case the application settings object has new fields
            // has been changed, but the database have not yet
            // been updated.
            //
            try
            {
                OApplicationSetting applicationSetting = OApplicationSetting.Current;

                TitleText.Text = applicationSetting.LoginTitle;
                //LogoImage.Visible = false;
                if (applicationSetting.LoginLogoID != null)
                {
                    //LogoImage.Visible = true;
                    //LogoImage.ImageUrl = "apploginlogo.aspx?ID=" + applicationSetting.LoginLogoID;
                    //tableLoginCell.Style["background-image"] = "url('apploginlogo.aspx?ID=" + applicationSetting.LoginLogoID + "')";
                }

            }
            catch (Exception ex)
            {
                login.Visible = false;
                panelSchemaError.Visible = true;
                labelSchemaErrorException.Text = ex.Message;
            }

            System.Web.Configuration.CompilationSection compilationSection =
                (System.Web.Configuration.CompilationSection)System.Configuration.ConfigurationManager.GetSection(@"system.web/compilation");

            // check the DEBUG attribute on the <compilation> element 
            bool isDebugEnabled = compilationSection.Debug;
            panelSql.Visible = isDebugEnabled && Page.Request.Url.AbsoluteUri.StartsWith("http://localhost");
            labelLicense.ForeColor = Color.Empty;
            labelLicense.Font.Bold = false;
            try
            {
                if (Request["LICENSEERROR"] == "1")
                {
                    labelLicense.Text = String.Format(Resources.Messages.License_Invalid);
                    labelLicense.ForeColor = Color.Red;
                    labelLicense.Font.Bold = true;

                    login.Enabled = false;
                }
                else
                {
                    labelLicense.Text = String.Format(Resources.Messages.License_LicensedTo,
                        UIPageBase.GetLicensedOwner());
                    if (!String.IsNullOrEmpty(UIPageBase.GetLicenseExpiryDate()))
                        labelLicense.Text = labelLicense.Text + " Licence expiry date is " + Convert.ToDateTime(UIPageBase.GetLicenseExpiryDate()).ToString("dd-MMM-yyyy");
                }
            }
            catch
            {
                labelLicense.Text = String.Format(Resources.Messages.License_NotLicensed);
            }

            // See if the user is trying to log on through single sign-on
            // Windows Authentication from the browser side.
            //
            if (ConfigurationManager.AppSettings["AuthenticateWithWindowsLogon"].ToLower() == "true")
            {
                bool authenticated = false;
                if (Request.ServerVariables["LOGON_USER"] != "")
                {
                    string userLoginName = Request.ServerVariables["LOGON_USER"];
                    string[] userId = userLoginName.Split('\\');
                    if (userId.Length > 0)
                        userLoginName = userId[userId.Length - 1];
                    authenticated = Authenticate(userLoginName, null);
                }

                if (authenticated)
                {
                    Login1_LoggedIn(this, e);
                }
                else
                {
                    if (Request.ServerVariables["LOGON_USER"] != "")
                    {

                        login.Visible = false;
                        panelNoAccess.Visible = true;
                        labelUserLoginName.Text = Request.ServerVariables["LOGON_USER"];
                    }
                    else
                    {

                        //allow user use Forms authentication to log on to the system when they are outside
                        //login.Visible = true;
                        //panelNoAccessNotLoggedOn.Visible = true;

                    }

                }
            }


            // Mobile Web
            //
            CheckBox MobileSiteCheckBox = login.FindControl("MobileSiteCheckBox") as CheckBox;
            MobileSiteCheckBox.Checked = false;
            string strUserAgent = Request.UserAgent.ToString().ToLower();
            bool MobileDevice = Request.Browser.IsMobileDevice;
            MobileSiteCheckBox.Visible = false;
            /*
            MobileSiteCheckBox.Visible = Request.Browser.IsMobileDevice;
            if (Request.Cookies["MobileDevice"] != null)
            {
                if (Request.Cookies["MobileDevice"].Value == "IgnoreMobile") { MobileDevice = false; }
            }
            else
            {
                if (strUserAgent != null)
                {
                    if (MobileDevice == true || 
                        strUserAgent.Contains("iphone") || 
                        strUserAgent.Contains("blackberry") || 
                        strUserAgent.Contains("mobile") ||
                        strUserAgent.Contains("android") || 
                        strUserAgent.Contains("windows ce") || 
                        strUserAgent.Contains("opera mini") || 
                        strUserAgent.Contains("palm"))
                    {
                        MobileSiteCheckBox.Checked = true;
                    }
                }
            }*/

            // kf begin: load the cookie
            //
            if (Request.Cookies["myCookie"] != null)
            {
                HttpCookie cookie = Request.Cookies.Get("myCookie");
                string userName = cookie.Values["Email"].ToString();
                string password = cookie.Values["Pass"].ToString();
                if (Authenticate(userName, password))
                    Login1_LoggedIn(this, new EventArgs());
            }
            // kf end;
        }
    }

    protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
    {
        bool captchaFlag = true;
        CaptchaControl cc = login.FindControl("ccJoin") as CaptchaControl;
        TextBox tb = login.FindControl("CaptchaTextBox") as TextBox;
        if (OApplicationSetting.Current.IsUsingCaptcha == 1)
        {
            if (tb.Text.Length != 0)
            {
                cc.ValidateCaptcha(tb.Text);
                if (!cc.UserValidated)
                {
                    RequiredFieldValidator rfv = login.FindControl("CaptchaTextBoxRequiredFieldValidator") as RequiredFieldValidator;
                    rfv.IsValid = false;
                    login.FailureText = ""; //only show the captcha fail text
                    rfv.Text = this.GetLocalResourceObject("loginResource1.CaptchaFailureText").ToString();
                    tb.Text = "";
                    captchaFlag = false;

                }
            }
            else captchaFlag = false;
        }

        if (captchaFlag)
        {
            //e.Authenticated = cc.UserValidated;
            e.Authenticated = Authenticate(login.UserName, login.Password);
        }

        if (!e.Authenticated || !captchaFlag)
            InitializePasswordSalt();

    }

    //--------------------------------------------------------------------
    /// <summary>
    /// If user is active directory try to authenticate user from active directory.
    /// </summary>
    /// <param name="OUser"></param>
    //--------------------------------------------------------------------
    protected bool IsActiveDirectory(OUser user)
    {
        using (DirectoryEntry entry = new DirectoryEntry(OApplicationSetting.Current.ActiveDirectoryPath,
            OApplicationSetting.Current.ActiveDirectoryDomain + "\\" + login.UserName, login.Password))
        {

            Object obj = entry.NativeObject;

            return true;
        }
    }


    //--------------------------------------------------------------------
    /// <summary>
    /// If user is active directory try to authenticate user from active directory.
    /// </summary>
    /// <param name="OUser"></param>
    //--------------------------------------------------------------------
    protected bool IsActiveDirectory(string userName, string password)
    {
        using (DirectoryEntry entry = new DirectoryEntry(OApplicationSetting.Current.ActiveDirectoryPath,
            OApplicationSetting.Current.ActiveDirectoryDomain + "\\" + userName, password))
        {

            Object obj = entry.NativeObject;

            return true;
        }
    }

    //--------------------------------------------------------------------
    /// <summary>
    /// Authenticate the user
    /// </summary>
    /// <param name="loginName"></param>
    /// <param name="password"></param>
    //--------------------------------------------------------------------
    protected bool Authenticate(string loginName, string password)
    {
        //always initialize the failuretext back to the original value
        // 
        login.FailureText = this.GetLocalResourceObject("loginResource1.FailureText").ToString();

        bool auth = false;
        OUser user = null;
        Session.Clear();


        user = OUser.GetUserByLoginName(loginName);

        // If the user account is not found, don't allow logging in.
        //
        if (user == null)
        {
            login.FailureText = this.GetLocalResourceObject("loginResource1.FailureText").ToString();
            Security.LogonFailed(
                loginName, Page.Request.UserHostAddress, Page.Request.ApplicationPath, login.FailureText);
            return false;
        }

        // If the user is banned, do not proceed.
        //
        if (user.IsBannedToday)
        {
            login.FailureText = Resources.Errors.User_AccountBanned;
            Security.LogonFailed(
                loginName, Page.Request.UserHostAddress, Page.Request.ApplicationPath, login.FailureText);
            return false;
        }

        // If the user not login in to the system more than 90 days, do not proceed.
        //
        // Jan 4 2013
        //Reinheart Sadie
        if (user.IsMoreThan90Days)
        {
            login.FailureText = Resources.Errors.User_AccountNotLoginTooLong;
            Security.LogonFailed(
                loginName, Page.Request.UserHostAddress, Page.Request.ApplicationPath, login.FailureText);

            using (Connection c = new Connection())
            {
                user.BanUser(1, UserBanReasonCode.IsMoreThan90Days, DateTime.Today);
                c.Commit();
            }
            return false;
        }


        // Try logging on using the user name + password 
        // (provided we are not using AD login)
        //
        Guid sessionId = Guid.Empty;
        if (OApplicationSetting.Current.IsUsingActiveDirectory != 1 && password !=null)
        {
            try
            {
                // 2014.07.03 
                // Kim Foong
                // Fix for long passwords that results in multiple blocks
                // in the encrypted string. Each block needs to be decrypted
                // independently.
                //
                //byte[] bytes = StringHelper.HexStringToBytes(password);
                //RSACryptoServiceProvider rsa = getRSACryptoServiceProvider();
                //bytes = rsa.Decrypt(bytes, false);
                //string decryptedPwWithSalt = StringHelper.ASCIIBytesToString(bytes);
                string[] passwordBlocks = password.Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries);
                string decryptedPwWithSalt = "";
                foreach (string passwordBlock in passwordBlocks)
                {
                    byte[] bytes = StringHelper.HexStringToBytes(passwordBlock);
                    RSACryptoServiceProvider rsa = getRSACryptoServiceProvider();
                    bytes = rsa.Decrypt(bytes, false);
                    decryptedPwWithSalt += StringHelper.ASCIIBytesToString(bytes);
                }

                if (decryptedPwWithSalt == null || decryptedPwWithSalt.Length < saltNoOfDigits)
                    throw new SaltNotMatchException();
                //string salt = GetSalt(decryptedPwWithSalt.Substring(0, saltNoOfDigits));
                string clientSalt = Security.HashString(decryptedPwWithSalt.Substring(0, saltNoOfDigits));
                string pageSalt = textMyKey.Text;
                string decryptedPw = decryptedPwWithSalt.Substring(saltNoOfDigits);

                if (clientSalt != pageSalt)
                    throw new SaltNotMatchException();
                //RemoveSalt(salt);
                sessionId = Security.Logon(loginName, decryptedPw, Page.Request.UserHostAddress, Page.Request.ApplicationPath);
            }
            catch (LoginInvalidException)
            {
                //if password supplied is wrong, update number of retries
                //
                login.FailureText = this.GetLocalResourceObject("loginResource1.FailureText").ToString();
                Security.LogonFailed(
                    loginName, Page.Request.UserHostAddress, Page.Request.ApplicationPath, login.FailureText);
                if (user != null)
                    user.IncrementFailedLoginRetries();
                return false;
            }
            catch (CryptographicException)
            {
                //unable to decrypt password 
                login.FailureText = this.GetLocalResourceObject("loginResource1.DecryptFailureText").ToString();
                Security.LogonFailed(
                    loginName, Page.Request.UserHostAddress, Page.Request.ApplicationPath,
                    this.GetLocalResourceObject("loginResource1.DecryptFailureLog").ToString());
                return false;
            }
            catch (SaltNotMatchException)
            {
                //salt not match
                login.FailureText = this.GetLocalResourceObject("loginResource1.SaltMatchFailureText").ToString();
                Security.LogonFailed(
                    loginName, Page.Request.UserHostAddress, Page.Request.ApplicationPath,
                    this.GetLocalResourceObject("loginResource1.SaltMatchFailureLog").ToString());
                return false;
            }
        }

        Session.Clear();

        // Check for Active Directory Login
        // 
        if (OApplicationSetting.Current.IsUsingActiveDirectory == 1 && password != null)
        {
            string decryptedPw = "";
            try
            {
                // 2014.07.03 
                // Kim Foong
                // Fix for long passwords that results in multiple blocks
                // in the encrypted string. Each block needs to be decrypted
                // independently.
                //
                //byte[] bytes = StringHelper.HexStringToBytes(password);
                //RSACryptoServiceProvider rsa = getRSACryptoServiceProvider();
                //bytes = rsa.Decrypt(bytes, false);
                //string decryptedPwWithSalt = StringHelper.ASCIIBytesToString(bytes);
                string[] passwordBlocks = password.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                string decryptedPwWithSalt = "";
                foreach (string passwordBlock in passwordBlocks)
                {
                    byte[] bytes = StringHelper.HexStringToBytes(passwordBlock);
                    RSACryptoServiceProvider rsa = getRSACryptoServiceProvider();
                    bytes = rsa.Decrypt(bytes, false);
                    decryptedPwWithSalt += StringHelper.ASCIIBytesToString(bytes);
                }

                if (decryptedPwWithSalt == null || decryptedPwWithSalt.Length < saltNoOfDigits)
                    throw new SaltNotMatchException();
                string clientSalt = Security.HashString(decryptedPwWithSalt.Substring(0, saltNoOfDigits));
                string pageSalt = textMyKey.Text;
                decryptedPw = decryptedPwWithSalt.Substring(saltNoOfDigits);

                if (clientSalt != pageSalt)
                    throw new SaltNotMatchException();
            }
            catch (CryptographicException)
            {
                //unable to decrypt password 
                login.FailureText = this.GetLocalResourceObject("loginResource1.DecryptFailureText").ToString();
                Security.LogonFailed(
                    loginName, Page.Request.UserHostAddress, Page.Request.ApplicationPath,
                    this.GetLocalResourceObject("loginResource1.DecryptFailureLog").ToString());
                return false;
            }
            catch (SaltNotMatchException)
            {
                //salt not match
                login.FailureText = this.GetLocalResourceObject("loginResource1.SaltMatchFailureText").ToString();
                Security.LogonFailed(
                    loginName, Page.Request.UserHostAddress, Page.Request.ApplicationPath,
                    this.GetLocalResourceObject("loginResource1.SaltMatchFailureLog").ToString());
                return false;
            }

            try
            {
                // This will throw an exception if the user is not
                // an AD user, or the AD server is down.
                //
                IsActiveDirectory(login.UserName, decryptedPw);
            }
            catch (Exception ex)
            {

                Session.Clear();
                login.FailureText = this.GetLocalResourceObject("loginResource1.FailureText").ToString();
                Security.LogonFailed(
                    loginName, Page.Request.UserHostAddress, Page.Request.ApplicationPath, "Error from AD: " + ex.Message);
                if (user != null)
                    user.IncrementFailedLoginRetries();
                return false;
            }
        }

        // AddedBy: Yiyuan
        // AddedDate: 13-Jan-2012
        // If the user is authenticated and ConcurrentLogin of the same user is not allowed,
        // then check whether the user is currently logged in the system.
        // If yes, reject this attempt to access the system and clear the session.
        //
        //if (OApplicationSetting.Current.AllowConcurrentLogin == 0)
        //{
            //int minuteBuf = int.Parse(ConfigurationManager.AppSettings["CheckConcurrencyInterval"]);

            //if (user.IsLoggedIn(minuteBuf))
            //{
            //    Session.Clear();
            //    login.FailureText = this.GetLocalResourceObject("loginResource1.UserLoginRejected").ToString();
            //    Security.LogonFailed(
            //        loginName, Page.Request.UserHostAddress, Page.Request.ApplicationPath, login.FailureText);
            //    return false;
            //}
        //}

        // ---------------------------------------------
        // Authentication is SUCCESSFUL.
        // Generate the session ID if necessary.
        // And save the session ID + user object into the Session.
        // ---------------------------------------------
        if (sessionId == Guid.Empty)
            sessionId = Security.Logon(loginName, Page.Request.UserHostAddress, Page.Request.ApplicationPath);
        Session["SessionId"] = sessionId;
        Session["User"] = user;
        
        // change to save the sessionid of the new one if concurrentlogin is not allowed
        // 2016.12.21
        // Kien Trung
        // BUG FIX: Update SessionID for user to check concurrent login.
        //
        int retryCount = 5;
        while (retryCount > 0)
        {
            try
            {
                if (OApplicationSetting.Current.AllowConcurrentLogin == 0)
                    using (Connection c = new Connection())
                    {
                        //user.SessionID = sessionId.ToString();
                        NonQuery.Update(TablesLogic.tUser, TablesLogic.tUser.ObjectID == user.ObjectID,
                                        new object[] { TablesLogic.tUser.SessionID, sessionId });
                        c.Commit();
                    }
                break;
            }
            catch (Exception e)
            {
                retryCount--;
                int rand = (new Random()).Next(10) + 1000;
                System.Threading.Thread.Sleep(rand);
            }
        }
        
        
        //if successful login and login retries > 0, reset number of retries
        user.ClearLoginRetries();

        // touch all the resource files to load them up
        //
        System.Globalization.CultureInfo ci;
        ci = Resources.Errors.Culture;
        ci = Resources.Messages.Culture;
        ci = Resources.Notifications.Culture;
        ci = Resources.Objects.Culture;
        ci = Resources.Roles.Culture;
        ci = Resources.Strings.Culture;
        ci = Resources.WorkflowEvents.Culture;
        ci = Resources.WorkflowStates.Culture;

        // touch one of the logic layer tables to force ASP.NET to load
        // all objects in TablesLogic up.
        //
        TUser tUser = TablesLogic.tUser;

        // 2010.11.24
        // Li Shan
        // clean temp chart directory
        CleanTempChart();
        CleanPDF();
        CleanGeneratedPDFs();

        // Create a random encryption key to salt the object IDs
        //
        int i = new Random().Next(0x7fffffff);
        Session["SaltID"] = "SLT-" + i.ToString("x");

        return true;
    }

    public static DateTime LastCacheRemovalDate
    {
        get
        {
            if (HttpContext.Current.Application["LastCacheRemovalDate"] == null)
                HttpContext.Current.Application["LastCacheRemovalDate"] = DateTime.MinValue;
            return (DateTime)HttpContext.Current.Application["LastCacheRemovalDate"];
        }
        set
        {
            HttpContext.Current.Application["LastCacheRemovalDate"] = value;
        }

    }

    protected void Login1_LoggedIn(object sender, EventArgs e)
    {
        // Clear old items in cache if necessary
        //
        if (((TimeSpan)DateTime.Now.Subtract(LastCacheRemovalDate)).TotalDays > 1)
        {
            // Remove cache items.
            //
            DiskCache.RemoveOldItems();
            LastCacheRemovalDate = DateTime.Now;

            // Clear messages and log in histories.
            //
            OApplicationSetting applicationSetting = OApplicationSetting.Current;
            OSessionAudit.ClearLoginHistory(DateTime.Now.AddDays(-applicationSetting.NumberOfDaysToKeepLoginHistory.Value));
            OMessage.ClearMessageHistory(DateTime.Now.AddDays(-applicationSetting.NumberOfDaysToKeepMessageHistory.Value));

        }

        OUser user = Session["User"] as OUser;
        // Checks if the user's password has expired or
        // a change in the password is required. If so,
        // redirect to the update password page.
        //
        if (ConfigurationManager.AppSettings["AuthenticateWithWindowsLogon"].ToLower() != "true" &&
            OApplicationSetting.Current.IsUsingActiveDirectory == 0 &&
            (user.HasPasswordExpired() || user.IsPasswordChangeRequired == 1))
        {
            Response.Redirect("apploginupdate.aspx", true);
        }
        else
        {
            // AddedBy: Yiyuan
            // AddedDate: 13-Jan-2012
            // Record the login server time for the current user and update last login time
            //
            using (Connection c = new Connection())
            {
                OUser u = TablesLogic.tUser.Load(user.ObjectID);
                //u.LastLoginTime = OUser.GetServerTime();
                //u.LastLoginTimeForAutoBanned = OUser.GetServerTime();
                //u.Save();

                NonQuery.Update(TablesLogic.tUser,
                    TablesLogic.tUser.ObjectID == u.ObjectID,
                    new object[] { TablesLogic.tUser.LastLoginTime, OUser.GetServerTime()},
                    new object[] { TablesLogic.tUser.LastLoginTimeForAutoBanned, OUser.GetServerTime()},
                    new object[] { TablesLogic.tUser.VersionNumber, TablesLogic.tUser.VersionNumber + 1 },
                    new object[] { TablesLogic.tUser.ModifiedUser, Audit.UserName },
                    new object[] { TablesLogic.tUser.ModifiedDateTime, DateTime.Now });
                c.Commit();
            }

            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(login.UserName, true, 120);

            HttpCookie cookie = new HttpCookie(
                FormsAuthentication.FormsCookieName,
                FormsAuthentication.Encrypt(ticket));
            Response.Cookies.Clear();
            Response.Cookies.Add(cookie);

            // Mobile Web
            //
            CheckBox MobileSiteCheckBox = login.FindControl("MobileSiteCheckBox") as CheckBox;
            Session["MobileMode"] = MobileSiteCheckBox.Checked;

            Response.Redirect("apptop.aspx");
        }
    }


    protected void buttonGenerateScript_Click(object sender, EventArgs e)
    {
        if (panelSql.Visible)
        {
            Response.AddHeader("content-disposition", "attachment; filename=database.sql");
            Response.Write(DatabaseSetup.GenerateSetupSQL(typeof(TablesLogic)));
            Response.End();
        }
    }


    protected void buttonGenerateScriptAuditTrail_Click(object sender, EventArgs e)
    {
        if (panelSql.Visible)
        {
            Response.AddHeader("content-disposition", "attachment; filename=database.sql");
            Response.Write(DatabaseSetup.GenerateSetupSQL(typeof(TablesAuditTrail)));
            Response.End();
        }
    }

    protected void buttonGenerateScriptForWorkflow_Click(object sender, EventArgs e)
    {
        if (panelSql.Visible)
        {
            Response.AddHeader("content-disposition", "attachment; filename=database.sql");
            Response.Write(DatabaseSetup.GenerateSetupSQL(typeof(Anacle.WorkflowFramework.TablesWorkflow)));
            Response.End();
        }
    }

    // 2010.11.24
    // Li Shan
    /// <summary>
    /// Cleans the temporary chart folder.
    /// </summary>
    private void CleanTempChart()
    {
        string chartDirPath = Page.Request.PhysicalApplicationPath + "temp\\charts";

        try
        {
            DirectoryInfo chartDir = new DirectoryInfo(chartDirPath);

            if (chartDir != null)
            {
                FileInfo[] fileList = chartDir.GetFiles();

                if (fileList != null && fileList.Length > 0)
                    foreach (FileInfo f in fileList)
                    {
                        if (f.CreationTime < DateTime.Now.AddHours(-24))
                            f.Delete();
                    }
            }
        }
        catch
        {
        }

    }


    // 2011.04.15
    // Chee Meng
    /// <summary>
    /// Cleans the wkhtml folder of PDF
    /// </summary>
    private void CleanPDF()
    {
        string wkhtmlDirPath = Page.Request.PhysicalApplicationPath + "wkhtmltopdf\\";

        try
        {
            DirectoryInfo wkhtml = new DirectoryInfo(wkhtmlDirPath);

            if (wkhtml != null)
            {
                FileInfo[] fileList = wkhtml.GetFiles();

                if (fileList != null && fileList.Length > 0)
                    foreach (FileInfo f in fileList)
                    {
                        if (f.CreationTime < DateTime.Now.AddHours(-24) && f.Extension == ".pdf")
                            f.Delete();
                    }
            }
        }
        catch
        {
        }
    }

    // 2012.7.11
    // Liu Xin
    /// <summary>
    /// Cleans the GeneratedPDFs folder.
    /// </summary>
    private void CleanGeneratedPDFs()
    {
        string chartDirPath = Page.Request.PhysicalApplicationPath + "GeneratedPDFs\\";

        try
        {
            DirectoryInfo chartDir = new DirectoryInfo(chartDirPath);

            if (chartDir != null)
            {
                FileInfo[] fileList = chartDir.GetFiles();

                if (fileList != null && fileList.Length > 0)
                    foreach (FileInfo f in fileList)
                    {
                        if (f.CreationTime < DateTime.Now.AddHours(-24) && f.Extension == ".pdf")
                            f.Delete();
                    }
            }
        }
        catch
        {
        }

    }

}

public class StringHelper
{
    public static byte[] HexStringToBytes(string hex)
    {
        if (hex.Length == 0)
        {
            return new byte[] { 0 };
        }

        if (hex.Length % 2 == 1)
        {
            hex = "0" + hex;
        }

        byte[] result = new byte[hex.Length / 2];

        for (int i = 0; i < hex.Length / 2; i++)
        {
            result[i] = byte.Parse(hex.Substring(2 * i, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
        }

        return result;
    }

    public static string BytesToHexString(byte[] input)
    {
        if (input == null)
            return null;
        StringBuilder hexString = new StringBuilder(64);

        for (int i = 0; i < input.Length; i++)
        {
            hexString.Append(String.Format("{0:X2}", input[i]));
        }
        return hexString.ToString();
    }

    public static string BytesToDecString(byte[] input)
    {
        StringBuilder decString = new StringBuilder(64);

        for (int i = 0; i < input.Length; i++)
        {
            decString.Append(String.Format(i == 0 ? "{0:D3}" : "-{0:D3}", input[i]));
        }
        return decString.ToString();
    }

    // Bytes are string
    public static string ASCIIBytesToString(byte[] input)
    {
        System.Text.ASCIIEncoding enc = new ASCIIEncoding();
        return enc.GetString(input);
    }
    public static string UTF16BytesToString(byte[] input)
    {
        System.Text.UnicodeEncoding enc = new UnicodeEncoding();
        return enc.GetString(input);
    }
    public static string UTF8BytesToString(byte[] input)
    {
        System.Text.UTF8Encoding enc = new UTF8Encoding();
        return enc.GetString(input);
    }

    // Base64
    public static string ToBase64(byte[] input)
    {
        return Convert.ToBase64String(input);
    }

    public static byte[] FromBase64(string base64)
    {
        return Convert.FromBase64String(base64);
    }
}

public class SaltNotMatchException : System.Exception
{
    public SaltNotMatchException()
    {
    }
}

