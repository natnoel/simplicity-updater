<%@ Page Language="C#" Theme="Corporate" Culture="auto" meta:resourcekey="PageResource1"
    UICulture="auto" Inherits="PageBase" %>

<%@ Import Namespace="System" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="System.Web.Services" %>
<%@ Import Namespace="System.Web.UI" %>
<%@ Import Namespace="System.Web.UI.WebControls" %>
<%@ Import Namespace="Anacle.DataFramework" %>
<%@ Import Namespace="LogicLayer" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        LoadPinBoard();
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        message.Visible = (int?)TablesLogic.tUser
            .Select(TablesLogic.tUser.HidePinboardMessageIndicator)
            .Where(TablesLogic.tUser.ObjectID == AppSession.User.ObjectID) != 1;
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (!IsPostBack)
        {
            // Saves the inbox path into Session to support global search.
            // 
            this.Session["CurrentSearchPath"] = this.Page.Request.ApplicationPath + "/home.aspx";
        }
    }

    protected void LoadPinBoard()
    {

        RepeaterDashboard.DataSource = OPinboard.GetPinsDataTableByUser(AppSession.User);
        RepeaterDashboard.DataBind();
    }

    [WebMethod]
    public static void UpdatePinPosition(string coords)
    {

        if (HttpContext.Current.Session == null || AppSession.User == null)
            return;

        try
        {
            string[] pin_coords = coords.Split(':');

            foreach (string p in pin_coords)
            {
                string[] attr = p.Split(',');

                if (attr[0] == "null")
                    continue;

                using (Connection c = new Connection())
                {
                    OPinboard pb = TablesLogic.tPinboard
                        .Load
                        (TablesLogic.tPinboard.ObjectID == new Guid(attr[0]) &
                        TablesLogic.tPinboard.UserID == AppSession.User.ObjectID);

                    pb.PositionX = Int32.Parse(attr[1]);
                    pb.PositionY = Int32.Parse(attr[2]);

                    pb.Save();
                    c.Commit();
                }
            }

        }
        catch (Exception e)
        {

        }

    }

    [WebMethod]
    public static void DeletePin(string id)
    {
        if (HttpContext.Current.Session == null || AppSession.User == null)
            return;

        using (Connection c = new Connection())
        {
            OPinboard pb = TablesLogic.tPinboard
                .Load
                (TablesLogic.tPinboard.ObjectID == new Guid(id) &
                TablesLogic.tPinboard.UserID == AppSession.User.ObjectID);


            if (pb.PinnedType == PinboardType.ObjectRecord)
            {
                OPinboard pb1 = TablesLogic.tPinboard
                    .Load
                    (TablesLogic.tPinboard.PinnedObjectID == pb.PinnedObjectID &
                    TablesLogic.tPinboard.PinnedObjectTypeName == pb.PinnedObjectTypeName &
                    TablesLogic.tPinboard.ObjectID != pb.ObjectID);


                if (pb1 == null)
                {
                    OPinboardDetail pbd = OPinboardDetail.GetPinboardDetail(pb.PinnedObjectID.Value, pb.PinnedObjectTypeName);
                    pbd.Delete();
                }
            }

            pb.Delete();
            c.Commit();
        }
    }

    protected void hideMessage_Click(object sender, EventArgs e)
    {
        using (Connection c = new Connection())
        {
            OUser u = TablesLogic.tUser.Load(TablesLogic.tUser.ObjectID == AppSession.User.ObjectID);

            if (u.HidePinboardMessageIndicator == null || u.HidePinboardMessageIndicator == 0)
            {
                u.HidePinboardMessageIndicator = 1;
                u.Save();
                c.Commit();
            }
        }
    }

    /// <summary>
    /// Pops up a dialog box to allow users to add selected dashboards.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void buttonAddDashboards_Click(object sender, EventArgs e)
    {
        SdbDashboard.Show();
    }


    protected void BtnPinEditPage_Click(object sender, EventArgs e)
    {
        SdbEditPin.Show();
    }

    protected void BtnPinSearchPage_Click(object sender, EventArgs e)
    {
        SdbSearchPin.Show();
    }

    protected void RepeaterDashboard_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        HtmlGenericControl li = e.Item.FindControl("ListItemDashboard") as HtmlGenericControl;
        HtmlIframe frame = e.Item.FindControl("FramePin") as HtmlIframe;
        HtmlGenericControl pin = e.Item.FindControl("DivObjPin") as HtmlGenericControl;
        Panel container = e.Item.FindControl("DivContainer") as Panel;
        HtmlGenericControl name = e.Item.FindControl("DivPinName") as HtmlGenericControl;

        if (li != null)
        {
            container.Attributes["data-id"] = ((System.Data.DataRowView)e.Item.DataItem)["ObjectID"].ToString();

            li.Attributes["data-id"] = ((System.Data.DataRowView)e.Item.DataItem)["ObjectID"].ToString();
            li.Attributes["data-row"] = ((System.Data.DataRowView)e.Item.DataItem)["PositionX"].ToString();
            li.Attributes["data-col"] = ((System.Data.DataRowView)e.Item.DataItem)["PositionY"].ToString();
            li.Attributes["data-sizex"] = ((System.Data.DataRowView)e.Item.DataItem)["SizeRow"].ToString();
            li.Attributes["data-sizey"] = ((System.Data.DataRowView)e.Item.DataItem)["SizeColumn"].ToString();

            if ((int)((System.Data.DataRowView)e.Item.DataItem)["PinnedType"] == (int)PinboardType.ObjectEditPage ||
                (int)((System.Data.DataRowView)e.Item.DataItem)["PinnedType"] == (int)PinboardType.ObjectSearchPage)
            {
                string objectType = ((System.Data.DataRowView)e.Item.DataItem)["PinnedObjectTypeName"].ToString();
                string translatedText = Resources.Objects.ResourceManager.GetString(objectType);
                OFunction function = OFunction.GetFunctionByObjectType(objectType);
                string text = "";

                //if entry not in resource file, use function name instead
                translatedText = translatedText ?? function.FunctionName;

                if ((int)((System.Data.DataRowView)e.Item.DataItem)["PinnedType"] == (int)PinboardType.ObjectEditPage)
                {

                    string url = this.ResolveUrl(function.EditUrl)
                        + (function.EditUrl.Contains("?") ? "&" : "?")
                        + "ID=" + HttpUtility.UrlEncode(Security.Encrypt("NEW:" + ":" + AppSession.SaltID))
                        + "&TYPE=" + HttpUtility.UrlEncode(Security.Encrypt(objectType));
                    text =
                        String.Format
                        ("<a class='fg-color-darken' href='#' onclick='window.open(\"{0}\",\"{1}\",\"location=no,status=yes,menubar=no,toolbar=no,resizable=yes,scrollbars=yes\");' style='vertical-align: middle; display: block; position: absolute; height:100%; width:90%; padding: 10px 5%;'>{2}</a>",
                        url, "AnacleEAM_Window", translatedText);
                }
                else
                {
                    string url = this.ResolveUrl(function.MainUrl) + (function.MainUrl.Contains("?") ? "&" : "?") + "&TYPE=" + HttpUtility.UrlEncode(Security.Encrypt(objectType));
                    text =
                        String.Format("<a class='fg-color-darken' href='{0}' target='{1}' style='vertical-align:middle; display:block; position: absolute; height:100%; width:90%; padding: 10px 5%;'>{2}</a>", url, "_self", translatedText);
                }
                pin.InnerHtml = text;
                frame.Visible = false;
                pin.Visible = true;

            }
            else if ((int)((System.Data.DataRowView)e.Item.DataItem)["PinnedType"] == (int)PinboardType.Dashboard)
            {
                Guid id = (Guid)((System.Data.DataRowView)e.Item.DataItem)["PinnedObjectID"];
                ODashboard db = TablesLogic.tDashboard[id];

                string title = db.ObjectName;
                string translatedText = Resources.Dashboards.ResourceManager.GetString(title);
                if (translatedText != null && translatedText != "")
                    title = translatedText;

                //120 = 100 + 20 (height/height + grid spacing) (see gridster javascript code in homepinboard)
                //    - 20 initial
                //    - 5px top padding
                //    height - 60 for header, padding, etc
                int height = ((db.Height ?? 4) * 120) - 20 - 30 - 5;
                height = height > 0 ? height : 0;
                int width = ((db.Width ?? 4) * 120) - 20 - 15;
                width = width > 0 ? width : 0;

                if (db.DashboardType != (int)JSChartType.Summarized)
                {
                    name.Style["color"] = "black";
                    frame.Style["height"] = "calc(100% - 33px)";
                    frame.Style["margin-top"] = "33px";
                }
                else
                {
                    name.Style["color"] = "transparent";
                }
                name.InnerText = title;
                name.Attributes["title"] = title;

                string UniqueID = Guid.NewGuid().ToString().Replace("-", "");

                if (db.DashboardType.Value == (int)JSChartType.Summarized)
                {
                    frame.Attributes["src"] = String.Format(this.ResolveUrl("~/modules/report/dashboard/summarydashboard.aspx") + "?ID={0}", HttpUtility.UrlEncode(Security.Encrypt(id.ToString() + ":" + UniqueID)));
                } else
                {
                    frame.Attributes["src"] = String.Format(this.ResolveUrl("~/modules/report/dashboard/plotlychart.aspx") + "?ID={0}", HttpUtility.UrlEncode(Security.Encrypt(id.ToString() + ":" + UniqueID)));
                    //frame.Attributes["src"] = String.Format(this.ResolveUrl("~/modules/report/dashboard/jschart.aspx") + "?ID={0}", HttpUtility.UrlEncode(Security.Encrypt(id.ToString() + ":" + UniqueID)));
                }

                frame.Visible = true;
                pin.Visible = false;
            }
        }
    }

    protected void SdbDashboard_Search(object sender, SearchEventArgs e)
    {
        e.CustomCondition = TablesLogic.tDashboard.Roles.RoleCode.In(AppSession.User.GetRoleCodes());
    }

    protected void SdbEditPin_Search(object sender, SearchEventArgs e)
    {
        e.CustomCondition = TablesLogic.tFunction.RoleFunctions.Role.RoleCode.In(AppSession.User.GetRoleCodes())
            & TablesLogic.tFunction.RoleFunctions.AllowCreate == 1
            & TablesLogic.tFunction.EditUrl != "~/"
            ;
    }

    protected void SdbSearchPin_Search(object sender, SearchEventArgs e)
    {
        e.CustomCondition = TablesLogic.tFunction.RoleFunctions.Role.RoleCode.In(AppSession.User.GetRoleCodes())
            & TablesLogic.tFunction.RoleFunctions.AllowViewAll == 1;
    }

    protected void SdbDashboard_Selected(object sender, EventArgs e)
    {
        foreach (Guid item in SdbDashboard.SelectedDataKeys)
            OPinboard.AddPins(AppSession.User, PinboardType.Dashboard, "Dashboard", item);
        LoadPinBoard();

    }

    protected void SdbEditPin_Selected(object sender, EventArgs e)
    {
        foreach (Guid item in SdbEditPin.SelectedDataKeys)
        {
            OFunction o = TablesLogic.tFunction[item];
            OPinboard.AddPins(AppSession.User, PinboardType.ObjectEditPage, o.ObjectTypeName);
        }

        LoadPinBoard();
    }

    protected void SdbSearchPin_Selected(object sender, EventArgs e)
    {
        foreach (Guid item in SdbSearchPin.SelectedDataKeys)
        {
            OFunction o = TablesLogic.tFunction[item];
            OPinboard.AddPins(AppSession.User, PinboardType.ObjectSearchPage, o.ObjectTypeName);
        }

        LoadPinBoard();
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Anacle.EAM</title>
    <link href="css/gridster/jquery.gridster.css" rel="stylesheet" type="text/css" />
    <link href="css/gridster/gridster.css" rel="stylesheet" type="text/css" />
</head>
<body class="body-home-bg">
    <form id="form1" runat="server">
        <div class="mdl-title-bar-spacer">
            <div class="mdl-title-bar">
                <div>
                    <div style="float:left">
                        <asp:label runat="server" ID="titleLabel" CssClass="mdl-title-bar-label" Text="Pinboard"></asp:label>
                    </div>
                </div>
            </div>
        </div>
        <div style="margin: 30px;">
            <ui:UIPanel runat="server" ID="panel" meta:resourcekey="panelResource" >
                <asp:Panel runat="server" CssClass="float-right">
                    <ui:UIButton runat="server" CssClass="btnblue" ID="buttonAddDashboards" Text="Pin Dashboards"
                        CausesValidation="False" OnClick="buttonAddDashboards_Click"  meta:resourcekey="buttonAddDashboardsResource" />
                    <ui:UIButton runat="server" CssClass="btnblue" ID="BtnPinEditPage" Text="Pin Create Shortcut"
                        CausesValidation="False" OnClick="BtnPinEditPage_Click"  meta:resourcekey="BtnPinEditPageResource" />
                    <ui:UIButton runat="server" CssClass="btnblue" ID="BtnPinSearchPage" Text="Pin Search Shortcut"
                        CausesValidation="False" OnClick="BtnPinSearchPage_Click"  meta:resourcekey="BtnPinSearchPageResource" />
                </asp:Panel>
                <asp:Panel CssClass="message" ID="message" runat="server" meta:resourcekey="messageResource" >
                    <table>
                        <tr>
                            <td>
                                <asp:Image ImageUrl="~/images/pinboard-yellow-256.png" runat="server" />
                            </td>
                            <td>
                                <h1>welcome to your pinboard</h1>
                                <p>
                                    This is where you can add links to commonly used functions,<br />
                                    dashboards, live stats that are of interest to you. When you open any records<br />
                                    in the system, you can also pin them here!
                                    <br />
                                    <br />
                                    Pin something by clicking on the add pins button on the top right.
                                </p>
                                <asp:LinkButton ID="hideMessage" runat="server" Text="Don't show me this welcome message again."
                                    OnClick="hideMessage_Click" meta:resourcekey="hideMessageResource" ></asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </ui:UIPanel>
            <ui:UIPanel runat="server" ID="PanelSearch" meta:resourcekey="PanelSearchResource" >
                <ui:UISearchDialogBox runat="server" ID="SdbDashboard" Title="What do you want to pin?"
                    SearchBehavior="SearchOnDemand" SearchOnDemandBaseTableName="tDashboard" SearchOnDemandAutoSearchOnShow="true"
                    SearchOnDemandType="ObjectQuery" AllowMultipleSelection="true" SearchOnDemandPropertyNames="ObjectName"
                    OnSearch="SdbDashboard_Search" OnSelected="SdbDashboard_Selected" meta:resourcekey="SdbDashboardResource" >
                    <Columns>
                        <ui:UIGridViewBoundColumn HeaderStyle-Width="650px" HeaderText="Name" PropertyName="ObjectName"
                            ResourceName="Resources.Dashboards" meta:resourcekey="SdbDashboardObjectNameResource">
                        </ui:UIGridViewBoundColumn>
                    </Columns>
                </ui:UISearchDialogBox>
                <ui:UISearchDialogBox runat="server" ID="SdbEditPin" Title="What edit page do you want to pin?"
                    SearchBehavior="SearchOnDemand" SearchOnDemandBaseTableName="tFunction" SearchOnDemandAutoSearchOnShow="true"
                    SearchOnDemandType="ObjectQuery" AllowMultipleSelection="true" OnSelected="SdbEditPin_Selected"
                    OnSearch="SdbEditPin_Search" meta:resourcekey="SdbEditPinResource" >
                    <Columns>
                        <ui:UIGridViewBoundColumn HeaderStyle-Width="650px" HeaderText="Name" PropertyName="FunctionName"
                            ResourceName="Resources.Objects" meta:resourcekey="SdbEditPinFunctionNameResource">
                        </ui:UIGridViewBoundColumn>
                    </Columns>
                </ui:UISearchDialogBox>
                <ui:UISearchDialogBox runat="server" ID="SdbSearchPin" Title="What search page do you want to pin?"
                    SearchBehavior="SearchOnDemand" SearchOnDemandBaseTableName="tFunction" SearchOnDemandAutoSearchOnShow="true"
                    SearchOnDemandType="ObjectQuery" AllowMultipleSelection="true" OnSelected="SdbSearchPin_Selected"
                    OnSearch="SdbSearchPin_Search" meta:resourcekey="SdbSearchPinResource" >
                    <Columns>
                        <ui:UIGridViewBoundColumn HeaderStyle-Width="650px" HeaderText="Name" PropertyName="FunctionName"
                            ResourceName="Resources.Objects" meta:resourcekey="SdbSearchPinFunctionNameResource">
                        </ui:UIGridViewBoundColumn>
                    </Columns>
                </ui:UISearchDialogBox>
            </ui:UIPanel>
            <ui:UIPanel ID="PanelPinboard" runat="server" CssClass="pinboards" meta:resourcekey="PanelPinboardResource">
                <div class="gridster">
                    <ul runat="server" id="ListDashboard">
                        <asp:Repeater runat="server" ID="RepeaterDashboard" OnItemDataBound="RepeaterDashboard_ItemDataBound" meta:resourcekey="RepeaterDashboardResource" >
                            <ItemTemplate>
                                <li runat="server" id="ListItemDashboard">
                                    <asp:Panel runat="server" ID="DivContainer" CssClass="gs_w" meta:resourcekey="DivContainerResource" >
                                        <div id="PinHeader">
                                            <div runat="server" id="DivPinName" class="gs_w_handler gs_w_title"></div>
                                            <div runat="server" id="DivUnpin" class="gs_w_rm_btn" onclick="javascript:DeletePin($(this));">
                                                <i class="material-icons">close</i>
                                            </div>
                                        </div>
                                        <asp:Panel runat="server" ID="DivPinHolder" CssClass="gs_w_dbholder" meta:resourcekey="DivPinHolderResource" >
                                            <div runat="server" id="DivObjPin" class="gs_w_obj" visible="false">
                                            </div>
                                            <iframe runat="server" id="FramePin" style="width: 100%; height: 100%; " frameborder="0" scrolling="no"
                                                visible="false"></iframe>
                                        </asp:Panel>
                                    </asp:Panel>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </div>
            </ui:UIPanel>
        </div>
        <div class="dashboard-expand" style="display: none;">
            <div class="dashboard-expand-head"><span id="dashboardTitle"></span>
                <div class="dashboard-expand-close" onclick="closePopup();" style="padding-top: 4.5px;">
                    <i class="material-icons">close</i>
                </div>
            </div>
            <div style="padding: 5px 5px 0 5px;">
                <iframe id="expandIframe" width="100%" frameborder="0" scrolling="no"></iframe>
            </div>
        </div>
        <div class="dashboard-expand-overlay" style="display: none;" onclick="closePopup();">
        </div>

        <style>
            #PinHeader {
                position: absolute;
                z-index: 2;
                width: 100%;
            }

            #PinHeader:hover {
                background-color: #e5e5e5;
                opacity: 0.8;
            }

            .dragging ul {
                background: url("images/tile.png");
            }
        </style>

        <script src="scripts/gridster/jquery.gridster.js" type="text/javascript"></script>

        <script type="text/javascript">
            var gridster;
            var $dashboardExpand;
            var $dashboardTitle;
            var $overlay;
            var $body;
            var $expandIFrame;
            var $window;

            function upd_w(e) {
                var wdcontainer = e.parent().parent();
                var iframe = wdcontainer.find(".gs_w_dbholder > iframe");
                iframe.attr('src', function (i, val) { return val; });
            }

            function initgs() {
                if (!matchMediaMobile.matches) {
                    gridster = $(".gridster ul").gridster({
                        widget_margins: [10, 10],
                        widget_base_dimensions: [100, 100],
                        draggable:
                        {
                            handle: '.gs_w_handler',

                            start: function (event, ui, $widget) {
                                //use a div overlay
                                $("<div class='dashboard_overlay' />").css({
                                    position: "absolute",
                                    width: "100%",
                                    height: "100%",
                                    left: 0,
                                    top: 0,
                                    zIndex: 1000
                                }).appendTo($(".gs_w_dbholder"));
                            },

                            stop: function (event, ui, $widget) {
                                $(".dashboard_overlay").remove();
                                UpdatePin();
                            }
                        },
                        //add 30 col, limit max 30 col, so regardless of browser size, we have 30 cols
                        max_cols: 30,
                        extra_cols: 30
                    }).data('gridster');
                }

                $('.gs_w_handler').dblclick(function expand() {
                    $body.css('overflow', 'hidden');
                    var $this = $(this);
                    var url = $this.parent().parent().find('iframe').attr('src');
                    var w = $window.width() - $dashboardExpand.outerWidth(true) + $dashboardExpand.width();
                    var h = $window.height() - $dashboardExpand.outerHeight(true) + $dashboardExpand.height();
                    x = $this;
                    $dashboardTitle.html($this.html());
                    // @todo: to update. hardcode header to 34px first.
                    $expandIFrame
                        .css('height', (h - 39) + 'px')
                        .attr('src', url);
                    $overlay.show();
                    $dashboardExpand
                        .css('width', w + 'px')
                        .css('height', h + 'px')
                        .css('top', ($window.scrollTop() - 1) + 'px')
                        .css('left', ($window.scrollLeft() - 1) + 'px')
                        .fadeIn();
                });

                $dashboardExpand = $('.dashboard-expand');
                $overlay = $('.dashboard-expand-overlay');
                $body = $('body');
                $expandIFrame = $('#expandIframe');
                $window = $(window);
                $dashboardTitle = $('#dashboardTitle');
            }

            function closePopup() {
                $expandIFrame.attr('src', '');
                $overlay.hide();
                $dashboardExpand.fadeOut();
                $body.css('overflow', '');
            }

            function UpdatePin() {
                var items = $('.gridster ul').find("li");
                var arr = [];
                for (var i = 0; i < items.length; i++) {
                    if ($(items[i]).attr("data-id")) {
                        var a = $(items[i]).attr("data-id") + ',' +
                                $(items[i]).attr("data-row") + ',' +
                                $(items[i]).attr("data-col")

                        arr.push(a);
                    }
                }

                var dataToSend = '';
                for (var i = 0; i < arr.length; i++)
                    dataToSend += (dataToSend == '' ? '' : ':') + arr[i];

                var options = {
                    type: "POST",
                    async: true,
                    url: "homepinboards.aspx/UpdatePinPosition",
                    data: "{'coords' :'" + dataToSend + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) { },
                    error: function (err) { }
                };
                $.ajax(options);

            }

            function DeletePin(e) {
                var w = e.parent().parent().parent();

                if (!isMobile) //Non-mobile
                    gridster.remove_widget(w);
                else //Mobile
                    w.hide('slow', function () { w.remove(); });

                var options = {
                    type: "POST",
                    async: true,
                    url: "homepinboards.aspx/DeletePin",
                    data: "{'id' :'" + w.attr('data-id') + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) { },
                    error: function (err) { }
                };
                $.ajax(options);

                UpdatePin();
            }

            var isMobile = false;
            var matchMediaMobile = window.matchMedia("(max-width: 700px)")

            $(document).ready(function () {
                initgs();

                if (matchMediaMobile.matches) {  //Mobile

                    //Sets the height to the correct ratio in the grid (since width will stretch accordingly)
                    $("#ListDashboard > li").each(function (index) {
                        var height = $(this).width() / $(this).attr("data-sizex") * $(this).attr("data-sizey");
                        $(this).height(height);
                        $(this).css("padding-bottom", '10px');
                        $(this).css("max-width", $(this).width());
                        $(this).css("min-width", $(this).width());
                    });

                    var parentWidth = $('#ListDashboard').parent().width();
                    var width = $('#ListDashboard').width();
                    var leftPadding = (parentWidth - width) / 2;
                    $('#ListDashboard').css('-webkit-padding-start', leftPadding);

                    $('#buttonAddDashboards, #BtnPinEditPage, #BtnPinSearchPage').css('width', '100%');
                    isMobile = true;
                }
            });

            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
                initgs();
            });
        </script>
    </form>
</body>
</html>
