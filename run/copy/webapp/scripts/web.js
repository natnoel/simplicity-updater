﻿
function openPage(url, target)
{
    if (target == 'frameBottom' && window.name != target)
    {
        window.top.document.getElementById('frameBottom').style.display = '';
        window.top.document.getElementById('AnacleEAM_Window').style.display = 'none';
        //window.top.document.getElementById('frameBottom').src = "about:blank";
    }
    else if (target == 'AnacleEAM_Window' && window.name != target)
    {
        closePage();
        window.top.document.getElementById('frameBottom').style.display = 'none';
        window.top.document.getElementById('AnacleEAM_Window').style.display = '';
        
    }

    window.setTimeout(function() { window.open(url, target) }, 10);
}

function closePage()
{
    if (window.top.document.getElementById('AnacleEAM_Window').style.display == '') {
        window.top.document.getElementById('frameBottom').style.display = '';
        window.top.document.getElementById('AnacleEAM_Window').style.display = 'none';
        window.top.document.getElementById('AnacleEAM_Window').src = "anacleeamframe.html";
    }
}

function showToast(elementId, messageText, closeText)
{
    //$("#" + elementId).slideDown(300)
    function show()
    {
        var snackbarContainer = document.querySelector('#' + elementId);
        if (snackbarContainer == null || snackbarContainer.MaterialSnackbar == null)
            window.setTimeout(show, 500);

        var data = {
            message: messageText,
            timeout: 30000,
            actionText: closeText,
            actionHandler: function () {
                snackbarContainer.MaterialSnackbar.cleanup_();
            }
        };
        snackbarContainer.MaterialSnackbar.showSnackbar(data);
    }
    window.setTimeout(show, 500);

}

function hideToast(elementId) {
    document.querySelector("#" + elementId).classList.remove("mdl-snackbar--active")
}

