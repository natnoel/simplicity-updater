<%@ Page Language="C#" Theme="Corporate" Inherits="PageBase" Culture="auto" meta:resourcekey="PageResource1"
    UICulture="auto" %>

<%@ Import Namespace="System" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="System.Web.UI" %>
<%@ Import Namespace="System.Web.UI.WebControls" %>
<%@ Import Namespace="Anacle.DataFramework" %>
<%@ Import Namespace="LogicLayer" %>

<script runat="server">

    /// <summary>
    /// Gets or sets the selected node's value in the At-A-Glance
    /// treeview or the selected date in the Calendar.
    /// </summary>
    protected string SelectedItem
    {
        get { return ViewState["SelectedItem"].ToString(); }
        set { ViewState["SelectedItem"] = value; }
    }


    private void gridTasks_Export(object sender, GridViewExportEventArgs args)
    {
        if (args.ExportedData != null)
            Window.Download(
                args.ExportedData.ToArray(),
                Resources.Strings.Home_Inbox + Path.GetExtension(args.FileName),
                MimeTypes.GetContentType(Path.GetExtension(args.FileName)));
    }

    OUserSearchGridViewPreference pref = null;

    //-------------------------------------------------------------------
    // event
    //-------------------------------------------------------------------
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);


    }

    
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (!IsPostBack)
        {
            pref = OUserSearchGridViewPreference.GetPreference(AppSession.User.ObjectID, "#Inbox");
            if (pref != null)
            {
                gridTasks.SetColumnPreferenceJson(pref.Columns);
            }
        }

        gridTasks.ListBoxColumnChooser.Title = Resources.Strings.SearchPanel_SelectColumns;
        gridTasks.ColumnReorderFixedLeftColumns = 2;
        gridTasks.FixedHeader = true;
        gridTasks.FixedHeaderOffset = 70;
        gridTasks.AllowColumnReorder = true;
        gridTasks.Export += new UIGridView.GridViewExportEventHandler(gridTasks_Export);
        gridTasks.ColumnsChanged += new EventHandler(gridTasks_ColumnsChanged);
        buttonColumnChooser.OnClickScript = "document.getElementById('" + this.gridTasks.ListBoxColumnChooser.ClientID + "_tb').click()";

        if (!IsPostBack)
        {

            SelectedItem = "";

            SelectedItem = "%/%";
            PopulateQuickSearch();

            PopulateInbox("");

            // 18-06-2014 Peter - Add PopulateAtAGlance()
            PopulateAtAGlance();

            string searchFilter = "";
            if (Request["S"] != null)
            {
                searchFilter = Request["S"];
                //SearchTextBox.Text = searchFilter;
                gridTasks.Filter(searchFilter);
            }

            System.Globalization.CultureInfo ci = System.Threading.Thread.CurrentThread.CurrentUICulture;
            linkClose.Attributes["onclick"] = "document.getElementById('" + tableMessage.ClientID + "').style.visibility = 'hidden'";
            //SearchTextBox.Attributes.Add("onkeypress", "return clickButton(event,'" + SearchButton.LinkButton.ClientID + "')");

            treeGlance.Nodes[0].Select();
            OnSelectedNodeChangedTreeGlance(treeGlance, new EventArgs());

            // Saves the URL of the current search page into Session to support global search.
            // 
            this.Session["CurrentSearchPath"] = this.Page.Request.Path;

        }

        ScriptManager.RegisterStartupScript(this, this.GetType(), "PopUp_" + this.ClientID + "2",
            "popup.initp('#" + this.spanSearchFor.ClientID + "', '#" + this.spanSearchFor.ClientID + "', '#" + this.panelSearchFor.ClientID + "', null, null, null, true);", true);

        labelMessage.Text = "";
    }


    /// <summary>
    /// Updates and saves the grid view column order/visibility preference.
    /// </summary>
    protected void UpdateSearchPreference()
    {
        if (this.gridTasks != null)
        {
            string json = this.gridTasks.GetColumnPreferenceJson();
            var pref = OUserSearchGridViewPreference.GetPreference(AppSession.User.ObjectID, "#Inbox");
            if (pref == null)
            {
                pref = TablesLogic.tUserSearchGridViewPreference.Create();
                pref.UserID = AppSession.User.ObjectID;
                pref.ObjectTypeName = "#Inbox";
            }
            pref.Columns = json;
            //pref.MaxResultCount = (int)MaximumNumberOfResults;
            //pref.PagingCount = (int)ResultsPerPage;
            using (Connection c = new Connection())
            {
                pref.Save();
                c.Commit();
            }
        }
    }


    protected void gridTasks_ColumnsChanged(object sender, EventArgs e)
    {
        UpdateSearchPreference();
    }


    /// <summary>
    /// Populate all tasks views on the home page by populating
    /// the following controls:
    /// 1. At-a-glance tree
    /// 2. Inbox
    /// 3. Calendar.
    /// </summary>
    protected void PopulateTasks()
    {
        // 18-06-2014 Peter - Add PopulateAtAGlance and PopulateCalendar
        PopulateAtAGlance();
        PopulateInbox("");
    }


    /// <summary>
    /// Populates the dropdown list with a list of objects searchable by
    /// the user.
    /// </summary>
    protected void PopulateQuickSearch()
    {
        if (Session["SearchableFunctions"] == null)
        {
            DataTable accessibleFunctions = OFunction.GetMenusAccessibleByUser(AppSession.User);

            List<ListItem> listItems = new List<ListItem>();
            foreach (DataRow dr in accessibleFunctions.Rows)
            {
                string functionName = dr["FunctionName"].ToString();

                string translatedText = Resources.Objects.ResourceManager.GetString(functionName);
                if (translatedText != null && translatedText != "")
                    functionName = translatedText;

                listItems.Add(new ListItem(functionName, dr["MainUrl"].ToString() + "\x255" + dr["ObjectTypeName"]));
            }

            listItems.Sort((a, b) => String.Compare(a.Text, b.Text));
            Session["SearchableFunctions"] = listItems;
        }

        /*
        SearchDropDownList.Items.Add(new ListItem(Resources.Strings.Home_Inbox, ""));
        foreach (ListItem item in (List<ListItem>)Session["SearchableFunctions"])
            SearchDropDownList.Items.Add(new ListItem(item.Text, item.Value));*/

    }

    /// <summary>
    /// Translates the specified text from the objects.resx file.
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    protected string TranslateMenuItem(string text)
    {
        string translatedText = Resources.Objects.ResourceManager.GetString(text);
        if (translatedText == null || translatedText == "")
            return text;
        return translatedText;
    }



    protected void PopulateInbox(string searchFilter)
    {
        if (SelectedItem != "" &&
            !SelectedItem.StartsWith("DATE:"))
        {
            // Here we determine which node is selected in the
            // at-a-glance tree. From that we can determine which
            // is the selected object type/status selected.
            // 
            string[] values = SelectedItem.Split('/');


            //sli 04-Aug-2011: Reset the PageIndex to 0 when user click the search button.
            if (hiddenRefresh.Value != "1")
                gridTasks.Grid.PageIndex = 0;
            else
                hiddenRefresh.Value = null;
            //end of sli 04-Aug-2011

            // Then, we filter the inbox accordingly.
            //
            using (Connection c = new Connection(Anacle.DataFramework.IsolationLevel.ReadUncommitted))
            {
                //Shimin 27/3/2014 Remove Work Objects from this selection
                //gridTasks.DataSource = OActivity.GetOutstandingActivitiesWithoutWorkForInbox(
                //    AppSession.User, new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 23, 59, 59),
                //    values[0], values[1], "%" + searchFilter + "%"); ;



                gridTasks.DataSource = OActivity.GetOutstandingActivitiesForInbox(
                    AppSession.User, new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 23, 59, 59),
                    values[0], values[1], "%" + searchFilter + "%"); ;

                gridTasks.DataBind();
                HideOrShowMassActionButtons();
            }
        }
    }


    //-------------------------------------------------------------------
    // event
    //-------------------------------------------------------------------
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        Page.ClientScript.RegisterClientScriptBlock(typeof(string), "Refresh",
            "<scr" + "ipt type='text/javascript'>\n" +
            "   function Refresh() { document.getElementById('" + hiddenRefresh.ClientID + "').value='1'; \n" +
            "      " + Page.ClientScript.GetPostBackEventReference(buttonRefresh.LinkButton, "") +
            "   } \n" +
            "</scr" + "ipt>\n");

        panelMessage.Visible = labelMessage.Text != "";

        // Force some default settings for the gridview.
        //
        this.gridTasks.ShowCaption = false;

        Window.WriteStartupJavascript("toggleActionSheet();");

    }

    private string ConvertUrlsToLinks(string msg)
    {
        string regex = @"((www\.|(http|https|ftp|news|file)+\:\/\/)[&#95;.a-z0-9-]+\.[a-z0-9\/&#95;:@=.+?,##%&~-]*[^.|\'|\# |!|\(|?|,| |>|<|;|\)])";
        Regex r = new Regex(regex, RegexOptions.IgnoreCase);
        return r.Replace(msg, "<a href=\"$1\" title=\"Click to open in a new window or tab\" target=\"&#95;blank\">$1</a>").Replace("href=\"www", "href=\"http://www");
    }


    Hashtable objectStates = new Hashtable();


    //-------------------------------------------------------------------
    // event
    //-------------------------------------------------------------------
    protected void gridTasks_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // Show the task information
            //
            DataRowView drv = ((DataRowView)e.Row.DataItem);
            objectStates[drv["Status"]] = 1;

            //Shimin 27/3/2014 Add color to the status column
            string statusname = drv.Row.ItemArray[2].ToString();
            string workflowClass = Helpers.GetWorkflowStateClass(drv["Status"].ToString());
            ((LinkButton)e.Row.Cells[2].Controls[0]).Text = drv["TaskNumber"].ToString();

            // Assumption: Last column is always the STATUS column
            //
            if (drv["TaskType"] != DBNull.Value && drv["TaskType"].ToString() != "")
            {
                string text = Resources.Objects.ResourceManager.GetString(drv["TaskType"].ToString());
                if (text == null || text == "")
                    text = drv["TaskType"].ToString();

                // Modified: Yiyuan 3/7/ 2012
                // change the cell number because two cells were added
                e.Row.Cells[5].Text = e.Row.Cells[5].Text + " (" + text + ")";
            }


            // Added: Yiyuan 3/7/2012
            // Mark urgent items in red and show the image if the priority flag is 1
            // else hide the image
            //
            if (((DataRowView)e.Row.DataItem)["PriorityFlag"] != DBNull.Value && (int?)((DataRowView)e.Row.DataItem)["PriorityFlag"] == 1)
            {
                //e.Row.BackColor = Color.FromArgb(0xf2, 0xde, 0xde);
                /*e.Row.ForeColor = System.Drawing.Color.Red;
                e.Row.Cells[1].Controls[0].Visible = true;
                //Shimin 27/3/2014 Set Urgent Color to Red
                e.Row.Cells[6].BackColor = System.Drawing.Color.Red;
                e.Row.Cells[6].ForeColor = System.Drawing.Color.White;*/

            }
            else
                e.Row.Cells[1].Controls[0].Visible = false;
        }
    }


    //-------------------------------------------------------------------
    // event
    //-------------------------------------------------------------------
    protected void buttonRefresh_Click(object sender, EventArgs e)
    {
        SelectedItem = "%/%";
        PopulateTasks();
    }



    /// <summary>
    /// Occurs when the user clicks on the search button to perform
    /// a search for tasks.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void buttonSearch_Click(object sender, EventArgs e)
    {
        if (SelectedItem == "")
            SelectedItem = "%/%";

        if (SelectedItem.Contains("/"))
        {
            // Search for tasks based on the select node in the tree.
            //
            //PopulateInbox(SearchTextBox.Text);
        }
        else
        {
            //sli 04-Aug-2011: Reset the PageIndex to 0 when user click the search button.
            if (hiddenRefresh.Value != "1")
                gridTasks.Grid.PageIndex = 0;
            else
                hiddenRefresh.Value = null;
            //end of sli 04-Aug-2011

            // Search for tasks based on the selected day in the calendar.
            //
            //gridTasks.DataSource = OActivity.GetOutstandingTasksAtDate(AppSession.User,
            //    calendarTasks.SelectedDate,
            //    "%" + SearchTextBox.Text + "%");
            //gridTasks.DataBind();
            HideOrShowMassActionButtons();
        }
    }

    protected override void Render(HtmlTextWriter writer)
    {
        System.IO.StringWriter sw = new System.IO.StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        base.Render(hw);

        writer.Write(sw.ToString().Replace("color:AntiqueWhite", "                  "));
    }



    /// <summary>
    /// Hides or shows mass action buttons.
    /// </summary>
    protected void HideOrShowMassActionButtons()
    {
        // Hide the Approve, Reject, Cancelled buttons
        // if there are no objects in PendingApproval state.
        //
        gridTasks.Commands[0].Visible = (objectStates["PendingApproval"] != null);
        gridTasks.Commands[1].Visible = (objectStates["PendingApproval"] != null);
        gridTasks.Commands[2].Visible = (objectStates["PendingApproval"] != null);
        //gridTasks.CheckBoxColumnVisible = (objectStates["PendingApproval"] != null);
    }


    /// <summary>
    /// Performs mass action on the tasks.
    /// </summary>
    /// <param name="action"></param>
    /// <param name="comments"></param>
    protected void PerformMassAction(string action, string comments)
    {
        // Validate to ensure that at least one checkbox selected.
        //
        int numberOfSelectedItems = 0;
        foreach (GridViewRow row in gridTasks.Rows)
            if ((row.Cells[0].Controls[0] as CheckBox).Checked)
                numberOfSelectedItems++;
        if (numberOfSelectedItems == 0)
        {
            labelMessage.Text = Resources.Errors.Task_CannotPerformMassActionNoTasksSelected;
            return;
        }

        // Load up all the relevant objects, and 
        // trigger the workflow event. 
        // 
        StringBuilder errorTasks = new StringBuilder();
        int count = 0;
        foreach (GridViewRow row in gridTasks.Rows)
        {
            if ((row.Cells[0].Controls[0] as CheckBox).Checked)
            {
                string objectTypeName = row.Cells[7].Text;
                try
                {
                    Type type = typeof(TablesLogic).Assembly.GetType("LogicLayer." + objectTypeName);
                    if (type != null)
                    {
                        using (Connection c = new Connection())
                        {
                            OActivity activity = TablesLogic.tActivity[(Guid)gridTasks.DataKeys[row.RowIndex][0]];
                            LogicLayerWorkflowPersistentObject obj = PersistentObject.LoadObject(type, activity.AttachedObjectID.Value) as LogicLayerWorkflowPersistentObject;
                            if (obj != null && obj.CurrentActivity.ObjectName == "PendingApproval")
                            {
                                obj.CurrentActivity.TriggeringEventName = action;
                                obj.CurrentActivity.TaskCurrentComments = comments;
                                obj.Save();
                                obj.TriggerWorkflowEvent(action);
                                obj.Save();
                                c.Commit();
                            }
                            else
                            {
                                throw new Exception();
                            }
                        }
                        count++;
                    }
                }
                catch (Exception ex)
                {
                    // the task type and task number.
                    //
                    errorTasks.Append(row.Cells[6].Text + "; ");
                }
            }
        }

        string message = "";
        if (count > 0)
            message += String.Format(Resources.Messages.Task_ActionSuccessful, action, count);
        if (numberOfSelectedItems - count > 0)
            message += String.Format(Resources.Messages.Task_ActionUnSuccessful, numberOfSelectedItems - count, action.ToLower(), errorTasks);

        PopulateTasks();

        labelMessage.Text = message;
    }


    /// <summary>
    /// Occurs when a button in the mass action dialog box is clicked.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void MassActionDialogBox_ButtonClicked(object sender, ButtonClickedEventArgs e)
    {
        if (e.CommandName == "Confirm")
        {
            string action = ViewState["MASSACTION"] as string;
            if (action != null)
                PerformMassAction(action, CommentTextBox.Text);

        }
    }


    protected void SearchButton_Click(object sender, EventArgs e)
    {
        /*
        string v = SearchDropDownList.SelectedValue;

        if (v == "")
        {
            // Simply perform search on the inbox
            //
            if (SearchTextBox.Text == "")
            {
                //PopulateInbox(SearchTextBox.Text);
                gridTasks.Filter("");
            }
            else
                gridTasks.Filter(SearchTextBox.Text);

        }
        else
        {
            string[] param = v.Split('\x255');
            string url = Page.ResolveUrl(param[0]);
            string objectTypeName = param[1];
            Window.Open(url +
                (url.IndexOf("?") > 0 ? "&" : "?") +
                "S=" + HttpUtility.UrlEncode(SearchTextBox.Text) +
                "&TYPE=" + HttpUtility.UrlEncode(Security.Encrypt(objectTypeName)), "frameBottom");
        }*/
    }

    protected void SearchDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        //SearchTextBox.Focus();
    }

    /// <summary>
    /// Populates the at-a-glance part. This requires that
    /// we retrieve all the tasks assigned to the current user
    /// grouped by their object types and their statuses.
    /// 18-06-2014 Peter
    /// </summary>
    protected void PopulateAtAGlance()
    {
        treeGlance.Nodes.Clear();

        // 18-06-2014 Peter
        // Change GetOutstandingTasksGroupedByObjectTypeAndStatus to GetOutstandingTasksGroupedByObjectTypeAndStatusWithoutWork.
        DataTable dt = OActivity.GetOutstandingTasksGroupedByObjectTypeAndStatus(AppSession.User, DateTime.Now);

        // Compute the total number of outstanding items
        //
        int taskCount = 0;
        foreach (DataRow dr in dt.Rows)
            taskCount += (int)dr["Count"];

        //// Create the root level Announcements node.
        ////
        //TreeNode announcementNode = new TreeNode("&nbsp;" + Resources.Strings.Home_Announcements + " (" + ViewState["AnnouncementCount"] + ")&nbsp;", "%");
        //announcementNode.Expanded = true;
        //announcementNode.Value = "";
        //treeGlance.Nodes.Add(announcementNode);

        // Create the root level Inbox node
        //
        TreeNode inboxNode = new TreeNode("&nbsp;" + Resources.Strings.Home_Inbox + " (" + taskCount + ")&nbsp;", "%/%");
        inboxNode.Expanded = true;
        inboxNode.Value = "%/%";
        treeGlance.Nodes.Add(inboxNode);

        // Create the object type nodes.
        // 
        Hashtable typeCount = new Hashtable();
        Hashtable h = new Hashtable();
        foreach (DataRow dr in dt.Rows)
        {
            // Add the object type
            //
            TreeNode typeNode = null;
            if (h[dr["ObjectTypeName"]] == null)
            {
                string typeName = Resources.Objects.ResourceManager.GetString(dr["ObjectTypeName"].ToString());
                typeNode = new TreeNode("&nbsp;" + (typeName == null ? dr["ObjectTypeName"].ToString() : typeName) + "&nbsp;", "");
                typeNode.Expanded = false;
                typeNode.Value = dr["ObjectTypeName"].ToString() + "/%";
                inboxNode.ChildNodes.Add(typeNode);

                h[dr["ObjectTypeName"]] = typeNode;
            }
            else
                typeNode = h[dr["ObjectTypeName"]] as TreeNode;

            // Compute the total count of outstanding tasks per
            // object type.
            //
            int count = 0;
            if (typeCount[dr["ObjectTypeName"]] != null)
                count = (int)typeCount[dr["ObjectTypeName"]];
            count += (int)dr["Count"];
            typeCount[dr["ObjectTypeName"]] = count;

            // Add the object status underneath the type
            //
            string statusName = Resources.WorkflowStates.ResourceManager.GetString(dr["ObjectName"].ToString());
            TreeNode statusNode = new TreeNode("&nbsp;" + (statusName == null ? dr["ObjectName"].ToString() : statusName) +
                " (" + dr["Count"] + ")&nbsp;");
            statusNode.Value = dr["ObjectTypeName"].ToString() + "/" + dr["ObjectName"].ToString();
            statusNode.Expanded = true;
            typeNode.ChildNodes.Add(statusNode);
        }

        // Append the count to the type name
        //
        foreach (object objectTypeName in h.Keys)
        {
            TreeNode typeNode = h[objectTypeName] as TreeNode;
            typeNode.Text += " (" + typeCount[objectTypeName] + ")";
        }

        // Selects the appropriate node.
        //
        foreach (TreeNode node in treeGlance.Nodes)
            SelectNode(node);
    }

    /// <summary>
    /// Selects appropriate node from At-A-Glance part.
    /// 18-06-2014 Peter
    /// </summary>
    /// <param name="node"></param>
    protected void SelectNode(TreeNode node)
    {
        if (node.Value == SelectedItem)
        {
            node.Selected = true;
            TreeNode currentNode = node;
            while (currentNode != null)
            {
                currentNode.Expanded = true;
                currentNode = currentNode.Parent;
            }
            return;
        }

        foreach (TreeNode childNode in node.ChildNodes)
            SelectNode(childNode);
    }

    /// <summary>
    /// Handles the OnSelectedNodeChanged of treeGlance controls
    /// to filters the inbox by the object type/status selected.
    /// 18-06-2014 Peter
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void OnSelectedNodeChangedTreeGlance(object sender, EventArgs e)
    {
        string value = "/";
        if (treeGlance.SelectedNode != null)
            value = treeGlance.SelectedNode.Value;
        SelectedItem = value;

        PopulateTasks();
        //if (value != "")
        //    PopulateTasks();
        //else
        //    PopulateAnnouncements();

        //SearchTextBox.Text = "";

        string s = treeGlance.SelectedNode.Text.Replace("&nbsp;", "");

        if (treeGlance.SelectedNode.Parent != null && treeGlance.SelectedNode.Parent.Parent != null)
        {
            int i = treeGlance.SelectedNode.Parent.Text.IndexOf('(');
            string s1 = treeGlance.SelectedNode.Parent.Text.Substring(0, i).Replace("&nbsp;", "");
            s = s1 + ">" +
                treeGlance.SelectedNode.Text.Replace("&nbsp;", "");
        }

        searchSelected.Text = s;
    }

    //-------------------------------------------------------------------
    // event
    //-------------------------------------------------------------------
    protected void gridTasks_Action(object sender, string commandName, System.Collections.Generic.List<object> objectIds)
    {
        if (commandName == "EditObject")
        {
            foreach (object id in objectIds)
            {
                if (id is Guid)
                {
                    OActivity activity = TablesLogic.tActivity[(Guid)id];
                    Window.OpenEditObjectPage(Page, activity.ObjectTypeName, activity.AttachedObjectID.ToString(), "SEARCH=1");
                }
            }
        }
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Anacle.EAM</title>
</head>
<body class="body-home-bg">
    <form id="form1" runat="server">
        <div class="mdl-title-bar-spacer">
            <div class="mdl-title-bar">
                <div>
                    <div style="float:left">
                        <asp:label runat="server" ID="titleLabel" CssClass="mdl-title-bar-label" Text="Inbox"></asp:label>
                    </div>

                    <%-- Show action sheet button --%>
                    <div style="float:right" class="mdl-title-bar-toggle-extra-buttons">
                        <ui:UIButton runat="server" ID="buttonExpandActionSheet" IconCssClass="material-icons" IconText="arrow_drop_down" ButtonType="RoundMiniFlat" ButtonColor="Default" OnClickScript="$('.mdl-title-bar-extra-buttons').toggle(300)" DisableAfterClick="false" DoPostBack="false" />
                        &nbsp;
                    </div>

                    <%-- Search Grid Settings --%>
                    <div style="float: right;" class="mdl-title-bar-extra-buttons">
                        <ui:UIButton runat="server" ID="buttonColumnChooser" IconCssClass="fas fa-columns" ButtonType="RoundMiniFlat"  ButtonColor="Default" DisableAfterClick="false" DoPostBack="false" />

                        <span runat="server" id="spanSearchFor" title="Filter Inbox">
                            <button class="mdl-button mdl-js-button mdl-js-ripple-effect">
                                <asp:Label runat="server" ID="label12" Text="Filter:" meta:resourcekey="label12Resource" ></asp:Label>
                                <asp:Label runat="server" ID="searchSelected" Text="" meta:resourcekey="searchSelectedResource" ></asp:Label>
                                <i class="material-icons">arrow_drop_down</i>
                            </button>
                        </span>
                        <asp:Panel runat="server" ID="panelSearchFor" Style="display: none;"
                            CssClass="mdl-dropdown mdl-treeview-dropdown" meta:resourcekey="panelSearchForResource">
                            <asp:TreeView runat="server" ID="treeGlance" SkinID="AtAGlance" OnSelectedNodeChanged="OnSelectedNodeChangedTreeGlance"
                                meta:resourcekey="treeGlanceResource">
                            </asp:TreeView>
                        </asp:Panel>
                    </div>

                    <div style="clear:both">
                    </div>
                </div>
            </div>
        </div>
        <ui:UIPanel runat="server" ID="panel" BorderStyle="NotSet" meta:resourcekey="panelResource">
            <div>
                <%-- old announcement grid --%>
                <div style="display: none">
                    <ui:UIButton runat="server" ID="buttonRefresh" Text='Refresh Inbox' ImageUrl="~/images/symbol-refresh-big.gif"
                        OnClick="buttonRefresh_Click" meta:resourcekey="buttonRefreshResource"></ui:UIButton>
                </div>
                      
                <ui:UIPanel runat="server" ID="panelTasks" BorderStyle="NotSet">
                    <ui:UIGridView runat="server" ID="gridTasks" SortExpression="ScheduledStartDateTime desc"
                        ShowCaption="false" OnRowDataBound="gridTasks_RowDataBound" OnAction="gridTasks_Action"
                        KeyName="ObjectID" meta:resourcekey="gridTasksResource" MouseOutColor="#FFFF80"
                        DataKeyNames="ObjectID" GridLines="Both" RowErrorColor="" Style="clear: both;" 
                        ShowCommands='false' ImageRowErrorUrl="" CheckBoxColumnVisible="false" 
                        >
                        <RowStyle VerticalAlign="Top" />
                        <AlternatingRowStyle VerticalAlign="Top" />
                        <PagerSettings Mode="NextPreviousFirstLast" FirstPageText="First" LastPageText="Last"
                            Position="Bottom" />
                        <Commands>
                            <ui:UIGridViewCommand CommandName="ApproveTasks" CommandText="Approve Selected" ActsOnSelectedItems="true"  meta:resourcekey="gridTasksUIGridViewCommandApproveSelectedResource" />
                            <ui:UIGridViewCommand CommandName="QueryTasks" CommandText="Query Selected" ActsOnSelectedItems="true"  meta:resourcekey="gridTasksUIGridViewCommandQuerySelectedResource" />
                            <ui:UIGridViewCommand CommandName="RejectTasks" CommandText="Reject Selected" ActsOnSelectedItems="true"  meta:resourcekey="gridTasksUIGridViewCommandRejectSelectedResource" />
                        </Commands>
                        <Columns>
                            <ui:UIGridViewButtonColumn HeaderText="" ImageUrl="~/images/important-red-16.png">
                                <HeaderStyle HorizontalAlign="Center" Width="16px" />
                                <ItemStyle HorizontalAlign="Center" />
                            </ui:UIGridViewButtonColumn>
                            <ui:UIGridViewButtonColumn HeaderText="Task Number" MobileDeviceVisibility="DesktopModeOnly"
                                ControlStyle-CssClass="home-linkbutton"
                                SortExpression="TaskNumber" CommandName="EditObject" ButtonType="Link" Text="TaskNumberRow"
                                HeaderStyle-Width="15%" meta:resourcekey="gridTasksUIGridViewButtonColumnEditObjectResource" >
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </ui:UIGridViewButtonColumn>
                            <ui:UIGridViewBoundColumn PropertyName="TaskName" HeaderText="Description" ResourceName="Resources.Objects"
                                DataField="TaskName" SortExpression="TaskName" meta:resourcekey="gridTasksUIGridViewBoundColumnTaskNameResource" >
                                <HeaderStyle HorizontalAlign="Left" Width="35%" />
                                <ItemStyle HorizontalAlign="Left" />
                            </ui:UIGridViewBoundColumn>
                            <ui:UIGridViewBoundColumn PropertyName="ObjectTypeName" HeaderText="Task Type" ResourceName="Resources.Objects"
                                meta:resourcekey="gridTasksUIGridViewBoundColumnObjectTypeNameResource" DataField="ObjectTypeName" ResourceAssemblyName=""
                                SortExpression="ObjectTypeName" MobileDeviceVisibility="DesktopModeOnly">
                                <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                <ItemStyle HorizontalAlign="Left" />
                            </ui:UIGridViewBoundColumn>                                    
                            <ui:UIGridViewBoundColumn PropertyName="CreatedDatetime" DataField="CreatedDatetime"
                                HeaderText="Created Date" DataFormatString="{0:dd-MMM-yyyy}" SortExpression="CreatedDatetime"
                                MobileDeviceVisibility="DesktopModeOnly" meta:resourcekey="gridTasksUIGridViewBoundColumnCreatedDatetimeResource" >
                                <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                <ItemStyle HorizontalAlign="Left" />
                            </ui:UIGridViewBoundColumn>
                            <ui:UIGridViewBoundColumn PropertyName="CreatedUser" HeaderText="Created By" DataField="CreatedUser" 
                                SortExpression="CreatedUser" MobileDeviceVisibility="DesktopModeOnly" meta:resourcekey="gridTasksUIGridViewBoundColumnCreatedUserResource" >
                                <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                <ItemStyle HorizontalAlign="Left" />
                            </ui:UIGridViewBoundColumn>
                            <ui:UIGridViewTemplateColumn SortExpression="Status" HeaderText="Status">
                                <ItemTemplate>
                                    <span class='mdl-chip <%# Helpers.GetWorkflowStateClass(((DataRowView)Container.DataItem)["Status"].ToString()) %>'>
                                        <span class='mdl-chip__text'>
                                            <%# Helpers.TranslateWorkflowStateItem(((DataRowView)Container.DataItem)["Status"].ToString()) %>
                                        </span>
                                    </span>
                                </ItemTemplate>
                            </ui:UIGridViewTemplateColumn>

                        </Columns>
                    </ui:UIGridView>
                    <ui:UIDialogBox runat="server" ID="MassActionDialogBox" Title="" Button1Text="Confirm"
                        Button1CausesValidation="true" Button1CommandName="Confirm" Button2Text="Cancel"
                        Button2CommandName="Cancel" Button2CausesValidation="false" OnButtonClicked="MassActionDialogBox_ButtonClicked" meta:resourcekey="MassActionDialogBoxResource" >
                        <ui:UIFieldTextBox runat="server" ID="CommentTextBox" Caption="Comments" CaptionPosition="Top"
                            Rows="5" TextMode="MultiLine" meta:resourcekey="CommentTextBoxResource" >
                        </ui:UIFieldTextBox>
                        <br />
                        <br />
                    </ui:UIDialogBox>
                </ui:UIPanel>
                                
                <ui:UIPanel runat="server" ID="panelMessage" HorizontalAlign="Center" BorderStyle="NotSet"
                    meta:resourcekey="panelMessageResource">
                    <div runat="server" id="tableMessage" class="object-message" style="top: -50px; width: 100%;
                        position: absolute;">
                        <table style="width: 100%" cellpadding="5">
                            <tr>
                                <td>
                                    <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                        <tr valign="top">
                                            <td align="left">
                                                <asp:Label runat='server' ID='labelMessage' meta:resourcekey="labelMessageResource"></asp:Label>
                                            </td>
                                            <td align="right">
                                                <asp:HyperLink runat="server" ID="linkClose" Text="Hide" NavigateUrl="javascript:void(0)"
                                                    meta:resourcekey="linkCloseResource"></asp:HyperLink>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </ui:UIPanel>
                <asp:HiddenField ID="hiddenRefresh" runat="server" meta:resourcekey="hiddenRefreshResource" ></asp:HiddenField>
            </div>
        </ui:UIPanel>
<script type="text/javascript">
    var toggleActionSheet = function () {
        if ($(window).width() < 960) {
            $('.mdl-title-bar-toggle-extra-buttons').show();
            $('.mdl-title-bar-extra-buttons').hide();
        }
        else {
            $('.mdl-title-bar-toggle-extra-buttons').hide();
            $('.mdl-title-bar-extra-buttons').show();
        }
    };
    $(window).resize(toggleActionSheet);
</script>
    </form>
</body>
</html>
