<%@ Page Language="C#" Theme="Corporate" Inherits="PageBase" Culture="auto" meta:resourcekey="PageResource1"
    UICulture="auto" %>

<%@ Register Src="~/components/webpartDashboard.ascx" TagPrefix="web" TagName="webpartDashboard" %>
<%@ Import Namespace="System" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="System.Web.UI" %>
<%@ Import Namespace="System.Web.UI.WebControls" %>
<%@ Import Namespace="Anacle.DataFramework" %>
<%@ Import Namespace="LogicLayer" %>

<%--
<script runat="server">
    
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        webpartManager.DisplayMode = WebPartManager.DesignDisplayMode;
        if (Request["Print"] != null)
        {
            //for printing purpose
            this.MainTab.Visible = false;
            this.buttonAddDashboards.Visible = false;
            this.buttonPrintPreview.Text = Resources.Strings.Dashboard_Print;
            this.buttonPrintPreview.ImageUrl = "~/images/printer.gif";
            this.buttonBack.Visible = true;
        }
        else
        {
            this.buttonPrintPreview.Text = Resources.Strings.Dashboard_Preview;
            this.buttonPrintPreview.ImageUrl = "~/images/preview.gif";
            this.buttonBack.Visible = false;
        }

        if (!IsPostBack)
        {
            //20130304 PTB: Use App Setting Home URL
            if (OApplicationSetting.Current.HomePageUrl != null && OApplicationSetting.Current.HomePageUrl != "")
                LinkToInbox.NavigateUrl = OApplicationSetting.Current.HomePageUrl;
            System.Globalization.CultureInfo ci = System.Threading.Thread.CurrentThread.CurrentUICulture;
        }
        //2013 Feb 05, Katy
        //Update total announcements
        HyperLink val = (HyperLink)Page.FindControl("LinkToAnnouncements");
        val.Text = string.Format("{0} <sup>{1}</sup>", "Announcements", OAnnouncement.GetAnnouncementsTable(AppSession.User, DateTime.Now).Rows.Count.ToString());

        //Update total inbox items
        HyperLink tasks = (HyperLink)Page.FindControl("LinkToInbox");
        tasks.Text = string.Format("{0} <sup>{1}</sup>", "Inbox",
            OActivity.GetOutstandingActivitiesForInbox(
                    AppSession.User, new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 23, 59, 59), "%%", "%%", "%%").Rows.Count);

    }


    protected void BindDashboardWebParts()
    {
        List<ODashboard> dashboards = ODashboard.GetDashboardByUserRoles(AppSession.User);
        checkboxlistDashboards.Bind(dashboards, "ObjectName", "ObjectID");

        foreach (ListItem item in checkboxlistDashboards.Items)
        {
            string text = item.Text;
            string translatedText = Resources.Dashboards.ResourceManager.GetString(text);
            if (translatedText != null && translatedText != "")
                item.Text = translatedText;
        }
    }


    //-------------------------------------------------------------------
    // event
    //-------------------------------------------------------------------
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
    }


    /// <summary>
    /// Pops up a dialog box to allow users to add selected dashboards.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void buttonAddDashboards_Click(object sender, EventArgs e)
    {
        BindDashboardWebParts();

        AddDashboardDialogBox.Show();
        panelError.Visible = false;
    }



    /// <summary>
    /// In normal mode: button text = "Preview"
    /// In preview mode: button text = "Print"
    /// Occurs when user clicks on the Preview/Print button:
    /// -- when "Preview" button is clicked: navigates to same page but in preview mode which displays content for printing only (remove tabs, unused button)
    /// -- when "Print" button is clicked: the print dialog would be displayed.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void buttonPrintPreview_Click(object sender, EventArgs e)
    {
        if (Request["Print"] == null)
            Window.Open("homedashboard.aspx?Print=1");
        else
            Window.WriteJavascript("window.print();");
    }

    /// <summary>
    /// Occurs when user clicks on the Back button.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void buttonBack_Click(object sender, EventArgs e)
    {
        if (Request["Print"] == null)
            Window.Open("homedashboard.aspx");
        Window.Close();
    }


    /// <summary>
    /// Adds selected dashboards, or closes the add dashboard dialog box.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void AddDashboardDialogBox_ButtonClicked(object sender, ButtonClickedEventArgs e)
    {
        if (e.CommandName == "Confirm")
        {

            AddDashboardDialogBox.Hide();

            WebPartZone zoneToAddDashboard = null;
            if (dropWebPartZones.SelectedIndex == 0)
                zoneToAddDashboard = webpartZone1;
            if (dropWebPartZones.SelectedIndex == 1)
                zoneToAddDashboard = webpartZone2;
            if (dropWebPartZones.SelectedIndex == 2)
                zoneToAddDashboard = webpartZone3;

            foreach (ListItem item in checkboxlistDashboards.Items)
            {
                if (item.Selected)
                {
                    Guid dashboardId = new Guid(item.Value);
                    ODashboard dashboard = TablesLogic.tDashboard.Load(dashboardId);

                    if (zoneToAddDashboard != null && dashboard != null)
                    {
                        webpartDashboard d = new webpartDashboard();
                        d.ID = "db" + dashboardId.ToString().Replace("-", "");
                        WebPart wp = webpartManager.AddWebPart(
                            webpartManager.CreateWebPart(d), zoneToAddDashboard, 0);

                        string title = dashboard.ObjectName;
                        string translatedText = Resources.Dashboards.ResourceManager.GetString(title);
                        if (translatedText != null && translatedText != "")
                            title = translatedText;

                        wp.Title = title;
                        ((webpartDashboard)wp.Controls[0]).DashboardID = dashboardId;
                        ((webpartDashboard)wp.Controls[0]).Refresh();
                    }
                }
            }
        }
        else if (e.CommandName == "Cancel")
        {
        }
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Anacle.EAM</title>
    <link href="css/v7-home.css" rel="stylesheet" type="text/css" />
    <link href="App_Themes/Corporate/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="App_Themes/PrintStyle.css" rel="stylesheet" type="text/css" media="print" />
</head>
<body class="homepage">
    <form id="form1" runat="server">
    <ui:UIPanel runat="server" ID="panelMain" BeginningHtml="" BorderStyle="NotSet" EndingHtml=""
        meta:resourcekey="panelMainResource1">
        <asp:Panel runat='server' ID='MainTab' class='div-home'>
            <div class='tabstrip'>
                <ul>
                    <li>
                        <asp:HyperLink runat="server" ID="LinkToInbox" NavigateUrl="~/home.aspx">Inbox</asp:HyperLink></li>
                    <li>
                        <asp:HyperLink runat="server" ID="LinkToCalendar" NavigateUrl="~/homemonthview.aspx">Calendar</asp:HyperLink></li>
                    <li class='selected'>
                        <asp:HyperLink runat="server" ID="LinkToDashboard">Dashboard</asp:HyperLink></li>
                    <li>
                        <asp:HyperLink runat="server" ID="LinkToAnnouncements" NavigateUrl="~/homeannouncements.aspx">Announcements</asp:HyperLink></li>
                </ul>
            </div>
        </asp:Panel>
        <div class="div-home">
            <ui:UIPanel runat="server" ID="panelDashboards" BeginningHtml="" BorderStyle="NotSet"
                EndingHtml="" meta:resourcekey="panelDashboardsResource1">
                <asp:WebPartManager runat="server" ID="webpartManager" DeleteWarning="Are you sure you want to hide this dashboard? (You can add it back again by clicking on the 'Add Dashboards' button later)">
                </asp:WebPartManager>
                <div class="div-form" id="dvPrintContent">
                    <ui:UIButton runat="server" CssClass="btn-link" ID="buttonAddDashboards" Text="Add Dashboards"
                        ImageUrl="~/images/add.gif" CausesValidation="False" OnClick="buttonAddDashboards_Click"
                        meta:resourcekey="buttonAddDashboardsResource1" />
                    <ui:UIButton runat="server" CssClass="btn-link" ID="buttonBack" Text="Back" Visible="False"
                        CausesValidation="False" OnClick="buttonBack_Click" meta:resourcekey="buttonBackResource1" />
                    <ui:UIButton runat="server" CssClass="btn-link" ID="buttonPrintPreview" Text="Print Preview"
                        ImageUrl="~/images/preview.gif" CausesValidation="False" OnClick="buttonPrintPreview_Click"
                         />
                    <br />
                    <br />
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tr valign="top">
                            <td class="webpart-zonecontainerleft">
                                <asp:WebPartZone runat="server" ID="webpartZone1" BorderStyle="None" WebPartVerbRenderMode="TitleBar"
                                    EmptyZoneText="Click 'Add Dashboards' to add a new dashboard to this zone, or drag a dashboard from another zone to this zone."
                                    Width="310px" HeaderText=" " meta:resourcekey="webpartZone1Resource1">
                                    <CloseVerb Visible="False" />
                                    <DeleteVerb Text="Hide" Description="Hide {0}" />
                                    <MinimizeVerb Visible="False" />
                                    <PartChromeStyle BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" />
                                    <PartTitleStyle BackColor="#EEEEEE" />
                                </asp:WebPartZone>
                            </td>
                            <td class="webpart-zonecontainer">
                                <asp:WebPartZone runat="server" ID="webpartZone2" BorderStyle="None" WebPartVerbRenderMode="TitleBar"
                                    EmptyZoneText="Click 'Add Dashboards' to add a new dashboard to this zone, or drag a dashboard from another zone to this zone."
                                    Width="310px" HeaderText=" " meta:resourcekey="webpartZone2Resource1">
                                    <CloseVerb Visible="False" />
                                    <DeleteVerb Text="Hide" Description="Hide {0}" />
                                    <MinimizeVerb Visible="False" />
                                    <PartChromeStyle BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" />
                                    <PartTitleStyle BackColor="#EEEEEE" />
                                </asp:WebPartZone>
                            </td>
                            <td class="webpart-zonecontainer">
                                <asp:WebPartZone runat="server" ID="webpartZone3" BorderStyle="None" WebPartVerbRenderMode="TitleBar"
                                    EmptyZoneText="Click 'Add Dashboards' to add a new dashboard to this zone, or drag a dashboard from another zone to this zone."
                                    Width="310px" HeaderText=" " meta:resourcekey="webpartZone3Resource1">
                                    <CloseVerb Visible="False" />
                                    <DeleteVerb Text="Hide" Description="Hide {0}" />
                                    <MinimizeVerb Visible="False" />
                                    <PartChromeStyle BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" />
                                    <PartTitleStyle BackColor="#EEEEEE" />
                                </asp:WebPartZone>
                            </td>
                        </tr>
                    </table>
                </div>
                <ui:UIDialogBox runat='server' ID="AddDashboardDialogBox" Title='which dashboards do you want to add?'
                    Button1Text="Confirm" Button1CommandName="Confirm" Button1AutoClosesDialogBox="false"
                    Button2Text="Cancel" Button2CausesValidation="false" Button2CommandName="Cancel"
                    OnButtonClicked="AddDashboardDialogBox_ButtonClicked">
                    <asp:Panel runat="server" ID="panelError" meta:resourcekey="panelErrorResource1">
                        <asp:Label runat="server" ID="labelError" Text="Please select one or more dashboards from the list below.<br/>"
                            ForeColor="Red" Font-Bold="True" meta:resourcekey="labelErrorResource1"></asp:Label>
                        <br />
                    </asp:Panel>
                    <div style="height: 200px; overflow: scroll; border: solid 1px silver">
                        <ui:UIFieldCheckboxList runat="server" ID="checkboxlistDashboards" ShowCaption='False'
                            ValidateRequiredField="True" Caption="Dashboards" meta:resourcekey="checkboxlistDashboardsResource1"
                            TextAlign="Right">
                        </ui:UIFieldCheckboxList>
                    </div>
                    <br />
                    <br />
                    <asp:Label runat="server" ID="labelAddToZone" Text="Add the selected dashboards to: "
                        meta:resourcekey="labelAddToZoneResource1"></asp:Label>
                    <asp:DropDownList runat="server" ID="dropWebPartZones" meta:resourcekey="dropWebPartZonesResource1">
                        <asp:ListItem Text="Zone 1" meta:resourcekey="ListItemResource1"></asp:ListItem>
                        <asp:ListItem Text="Zone 2" meta:resourcekey="ListItemResource2"></asp:ListItem>
                        <asp:ListItem Text="Zone 3" meta:resourcekey="ListItemResource3"></asp:ListItem>
                    </asp:DropDownList>
                    <br />
                </ui:UIDialogBox>
            </ui:UIPanel>
        </div>
    </ui:UIPanel>

    <script src='scripts/jtip.js' type='text/javascript'></script>

    </form>
</body>
</html>
--%>