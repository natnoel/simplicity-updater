<%@ Page Language="C#" Theme="Corporate" CodeFile="~/apploginreset.aspx.cs" Inherits="apploginreset"
    UICulture="auto" Culture="auto" meta:resourcekey="PageResource1" %>

<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="Anacle.DataFramework" %>
<%@ Import Namespace="LogicLayer" %>

<script runat="server">
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        try
        {
            lblCopyright.Text = string.Format("COPYRIGHT &copy; {0} ANACLE SYSTEMS ALL RIGHTS RESERVED", DateTime.Today.Year);
        }
        catch (Exception)
        {
        }
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Anacle.EAM</title>
    <script type="text/javascript" src="scripts/rlogin.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var scale = $(window).width() / 400;
            $("#viewport").attr("content", "width=400, user-scalable=no, initial-scale=" + scale + ", maximum-scale=" + scale + ", minimum-scale=" + scale);
            $("#block").fadeOut(500);

        });
    </script>
</head>
<body style="padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px">
    <style>
        .validator 
        {
            margin-top: -18px;
            margin-bottom: ;
            position: absolute;
            font-size: 11px;
        }
        .button-container
        {
            padding-top: 20px;
        }
    </style>
    <form id="form1" runat="server">
        <div id="body" style="padding: 0; position: relative" runat="server">
            <div style="background-color: #194680; height: 400px">
                <img src="apploginlogo.aspx" style="width: 100vw; height: 400px; object-fit: cover" />
            </div>
            <div id="content" style="text-align: center; position: absolute; top: 50px; width: 100vw">
                <div style="display: inline-block; width: 400px; text-align:center">

                    <div class="mdl-card mdl-shadow--2dp" style="display:inline-block">
                        <div class="mdl-card__title">
                            <h2 class="mdl-card__title-text"><asp:Label ID="ResetPasswordLabel"  runat="server" Text="Reset Password"></asp:Label></h2>
                        </div>
                        <div class="mdl-card__supporting-text mdl-card--border" style="text-align: left">
                            <asp:Label runat="server" ID="InstructionLabel" Text="Provide your details to reset your password"></asp:Label>
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <label class="mdl-textfield__label"><asp:Label runat="server" ID="UserNameLabel" Text="User Name"></asp:Label></label>
                                <asp:TextBox ID="txtUserLogin" runat="server" CssClass="mdl-textfield__input"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtUserLogin"
                                Text="Required" CssClass="validator"></asp:RequiredFieldValidator>

                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <label class="mdl-textfield__label"><asp:Label runat="server" ID="EmailLabel" Text="Email"></asp:Label></label>
                                <asp:TextBox ID="txtEmail" runat="server" CssClass="mdl-textfield__input"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEmail"
                                Text="Required" CssClass="validator"></asp:RequiredFieldValidator>

                            <div>
                                <asp:Label runat="server" ID="lblResetError" EnableViewState="false" ForeColor="Red" Font-Size="12px" />
                            </div>

                            <div class="button-container">
                                <asp:Button runat="server" ID="btnResetPassword" Text="Reset Password" CssClass="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--green mdl-button--raised " OnClick="btnResetPassword_Click" />
                                <asp:Button runat="server" ID="btnBack" Text="Back" CausesValidation="false" CssClass="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--default" OnClick="btnBack_Click" />
                            </div>
                        </div>
                        <div class="mdl-card__supporting-text mdl-card--border" style="text-align: left; background-color: #eeeeee">
                            <asp:Label runat="server" ID="labelLicense" meta:resourcekey="labelLicenseResource1" style="font-size:11px"></asp:Label>
                            <br />
                            <br />
                            <asp:Label runat="server" ID="lblCopyright" style="font-size:11px; color: #bdbdbd"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--<div id="header" style="padding: 10px 0; background-color:#000;">
            <table style="max-width: 800px; margin-left: auto; margin-right: auto;">
                <tr>
                    <td style="padding-left: 35px; width: 200px;">
                        <asp:Image runat="server" style="width:226px" ID="LogoImage" ImageAlign="Bottom" />
                    </td>
                    <td style="padding-left: 35px;">
                        <asp:Label ID="TitleText"  style="font-size: 30pt; color: white;" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>-->
    </form>
</body>
</html>
