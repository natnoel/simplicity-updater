﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Anacle.DataFramework;

namespace LogicLayer
{
    public class TUserSearchGridViewPreference : LogicLayerSchema<OUserSearchGridViewPreference>
    {
        public SchemaGuid UserID;
        public SchemaString ObjectTypeName;
        [Size(1000)]
        public SchemaString Columns;
        public SchemaInt MaxResultCount;
        public SchemaInt PagingCount;
    }

    public abstract class OUserSearchGridViewPreference : LogicLayerPersistentObject
    {
        public abstract Guid? UserID { get; set; }
        public abstract string ObjectTypeName { get; set; }
        public abstract string Columns { get; set; }
        public abstract int? MaxResultCount { get; set; }
        public abstract int? PagingCount { get; set; }


        public static OUserSearchGridViewPreference GetPreference(Guid? userId, string objectTypeName)
        {
            return TablesLogic.tUserSearchGridViewPreference.Load(
                TablesLogic.tUserSearchGridViewPreference.UserID == userId &
                TablesLogic.tUserSearchGridViewPreference.ObjectTypeName == objectTypeName);
        }
    }
}
