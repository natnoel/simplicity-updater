<%@ Control Language="C#" ClassName="objectChecklist" %>
<%@ Import Namespace="System" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="System.Web.UI" %>
<%@ Import Namespace="System.Web.UI.WebControls" %>
<%@ Import Namespace="System.ComponentModel" %>
<%@ Import Namespace="Anacle.DataFramework" %>
<%@ Import Namespace="LogicLayer" %>
<%@ Register Src="objectPanel.ascx" TagPrefix="web2" TagName="object" %>


<script runat="server">
    public string ChecklistIDPropertyName
    {
        get { return ChecklistID.PropertyName; }
        set { ChecklistID.PropertyName = value; }
    }

    public string ChecklistNamePropertyName
    {
        get { return ChecklistName.PropertyName; }
        set { ChecklistName.PropertyName = value; }
    }

    public string ChecklistGridViewErrorMsg
    {
        get { return ChecklistGridView.ErrorMessage; }
        set { ChecklistGridView.ErrorMessage = value; }
    }


    private bool MakeButtonsVisible = true;
    public bool MakeButtonsVisibleIfMoreThanOneRow
    {
        get
        {
            return this.MakeButtonsVisible;
        }
        set
        {
            this.MakeButtonsVisible = value;
        }
    }

    public bool AttachChecklistVisible
    {
        get
        {
            return this.ChecklistGridView.Commands[0].Visible;
        }
        set
        {
            this.ChecklistGridView.Commands[0].Visible = value;
        }
    }

    public bool ClearChecklistVisible
    {
        get
        {
            return this.ChecklistGridView.Commands[1].Visible;
        }
        set
        {
            this.ChecklistGridView.Commands[1].Visible = value;
        }
    }


    public bool ValidateRequiredField
    {
        get
        {
            return (bool)ViewState["ValidateRequiredField"];
        }
        set
        {
            ViewState["ValidateRequiredField"] = value;
            UpdateValidationRequiredFields();
        }
    }

    private string bindChecklistType;
    public string BindChecklistType
    {
        get
        {
            return this.bindChecklistType;
        }

        set
        {
            this.bindChecklistType = value;
        }
    }

    public Unit PopupDialogMaxHeight
    {
        get
        {
            return SearchChecklistDialogBox.DialogMaximumHeight;
        }

        set
        {
            SearchChecklistDialogBox.DialogMaximumHeight = value;
        }
    }

    Hashtable checklistResponseSets = new Hashtable();



    /// <summary>
    /// Populates the dropdown lists.
    /// </summary>
    /// <param name="e"></param>
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ViewState["ValidateRequiredField"] = true;
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        UpdateValidationRequiredFields();
    }


    /// <summary>
    /// Finds and returns the objectPanel.ascx control.
    /// </summary>
    /// <param name="c"></param>
    /// <returns></returns>
    protected objectPanel getPanel(Control c)
    {
        if (c.GetType() == typeof(objectPanel))
            return (objectPanel)c;
        foreach (Control child in c.Controls)
        {
            objectPanel o = getPanel(child);
            if (o != null)
                return o;
        }
        return null;
    }

    /// <summary>
    /// Updates all the compulsory flags.
    /// </summary>
    protected void UpdateValidationRequiredFields()
    {
        foreach (GridViewRow row in ChecklistGridView.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                Guid id = (Guid)ChecklistGridView.DataKeys[row.RowIndex][0];
                LogicLayerPersistentObject persistentObject = (LogicLayerPersistentObject)getPanel(this.Page).SessionObject;
                OTaskChecklistItem item = persistentObject.TaskChecklistItems.FindObject(id) as OTaskChecklistItem;
                UIFieldTextBox additionalTextBox = row.FindControl("ChecklistItemRemarks") as UIFieldTextBox;
                if (item != null && item.ChecklistItem != null)
                {
                    if (item.ChecklistItem.ChecklistType == ChecklistItemType.SingleSelectionFewChoices)
                    {
                        UIFieldRadioList radioList = row.FindControl("ChecklistRadioList") as UIFieldRadioList;

                        if (radioList != null)
                        {
                            radioList.ValidateRequiredField = (item.ChecklistItem.IsMandatoryField == 1 && this.ValidateRequiredField);
                            radioList.PropertyName = "SelectedResponseID";

                            if (additionalTextBox != null && item.ChecklistItem.HasSingleTextboxField == 1)
                            {
                                additionalTextBox.PropertyName = "Remarks";
                            }
                        }
                    }
                    else if (item.ChecklistItem.ChecklistType == ChecklistItemType.SingleSelectionManyChoices)
                    {
                        UIFieldDropDownList dropList = row.FindControl("ChecklistDropList") as UIFieldDropDownList;

                        if (dropList != null)
                        {
                            dropList.ValidateRequiredField = (item.ChecklistItem.IsMandatoryField == 1 && this.ValidateRequiredField);
                            dropList.PropertyName = "SelectedResponseID";

                            if (additionalTextBox != null && item.ChecklistItem.HasSingleTextboxField == 1)
                            {
                                additionalTextBox.PropertyName = "Remarks";
                            }
                        }
                    }
                    else if (item.ChecklistItem.ChecklistType == ChecklistItemType.MultipleSelections)
                    {
                        UIFieldCheckboxList cbl = row.FindControl("ChecklistCheckboxList") as UIFieldCheckboxList;

                        if (cbl != null)
                        {
                            cbl.ValidateRequiredField = (item.ChecklistItem.IsMandatoryField == 1 && this.ValidateRequiredField);
                            cbl.PropertyName = "SelectedResponses";

                            if (additionalTextBox != null && item.ChecklistItem.HasSingleTextboxField == 1)
                            {
                                additionalTextBox.PropertyName = "Remarks";
                            }
                        }
                    }
                    else if (item.ChecklistItem.ChecklistType == ChecklistItemType.SingleLineFreeText)
                    {
                        UIFieldTextBox textBox = row.FindControl("ChecklistSingleLineTextBox") as UIFieldTextBox;

                        if (textBox != null)
                        {
                            textBox.ValidateRequiredField = (item.ChecklistItem.IsMandatoryField == 1 && this.ValidateRequiredField);
                            textBox.PropertyName = "Remarks";
                        }
                    }
                    else if (item.ChecklistItem.ChecklistType == ChecklistItemType.MultiLineFreeText)
                    {
                        UIFieldTextBox textBox = row.FindControl("ChecklistMultiLineTextBox") as UIFieldTextBox;

                        if (textBox != null)
                        {
                            textBox.ValidateRequiredField = (item.ChecklistItem.IsMandatoryField == 1 && this.ValidateRequiredField);
                            textBox.PropertyName = "Remarks";
                        }
                    }
                }
            }
        }
    }


    /// <summary>
    /// Occurs when a data row is bound to data.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ChecklistGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Guid id = (Guid)ChecklistGridView.DataKeys[e.Row.RowIndex][0];
            LogicLayerPersistentObject persistentObject = (LogicLayerPersistentObject)getPanel(this.Page).SessionObject;
            OTaskChecklistItem item = persistentObject.TaskChecklistItems.FindObject(id) as OTaskChecklistItem;

            if (item != null && item.ChecklistItem != null)
            {
                if (item.ChecklistItem.ChecklistType == ChecklistItemType.SingleSelectionFewChoices)
                {
                    UIFieldRadioList radioList = e.Row.FindControl("ChecklistRadioList") as UIFieldRadioList;

                    if (radioList != null)
                    {
                        radioList.Visible = true;
                        radioList.Bind(item.ChecklistItem.ChecklistResponseSet.ChecklistResponses.Order(
                            TablesLogic.tChecklistResponse.DisplayOrder.Asc));
                        radioList.ValidateRequiredField = (item.ChecklistItem.IsMandatoryField == 1 && this.ValidateRequiredField);

                        if (item.SelectedResponseID != null)
                        {
                            radioList.SelectedValue = item.SelectedResponseID.ToString();
                        }

                        if (item.ChecklistItem.HasSingleTextboxField == 1)
                        {
                            UIFieldTextBox t = (UIFieldTextBox)e.Row.FindControl("ChecklistItemRemarks");
                            t.Visible = true;
                            t.Text = item.Remarks;
                        }
                    }
                }
                else if (item.ChecklistItem.ChecklistType == ChecklistItemType.SingleSelectionManyChoices)
                {
                    UIFieldDropDownList dropList = e.Row.FindControl("ChecklistDropList") as UIFieldDropDownList;

                    if (dropList != null)
                    {
                        dropList.Visible = true;
                        dropList.Bind(item.ChecklistItem.ChecklistResponseSet.ChecklistResponses.Order(TablesLogic.tChecklistResponse.DisplayOrder.Asc));
                        dropList.ValidateRequiredField = (item.ChecklistItem.IsMandatoryField == 1 && this.ValidateRequiredField);

                        if (item.SelectedResponseID != null)
                        {
                            dropList.SelectedValue = item.SelectedResponseID.ToString();
                        }

                        if (item.ChecklistItem.HasSingleTextboxField == 1)
                        {
                            UIFieldTextBox t = (UIFieldTextBox)e.Row.FindControl("ChecklistItemRemarks");
                            t.Visible = true;
                            t.Text = item.Remarks;
                        }
                    }
                }
                else if (item.ChecklistItem.ChecklistType == ChecklistItemType.MultipleSelections)
                {
                    UIFieldCheckboxList cbl = e.Row.FindControl("ChecklistCheckboxList") as UIFieldCheckboxList;

                    if (cbl != null)
                    {
                        cbl.Visible = true;
                        cbl.Bind(item.ChecklistItem.ChecklistResponseSet.ChecklistResponses.Order(TablesLogic.tChecklistResponse.DisplayOrder.Asc));
                        cbl.ValidateRequiredField = (item.ChecklistItem.IsMandatoryField == 1 && this.ValidateRequiredField);

                        if (item.SelectedResponses.Count != 0)
                        {
                            foreach (OChecklistResponse CR in item.SelectedResponses)
                            {
                                foreach (ListItem LI in cbl.Items)
                                {
                                    if (LI.Value == CR.ObjectID.Value.ToString())
                                    {
                                        LI.Selected = true;
                                        break;
                                    }
                                }
                            }
                        }


                        if (item.ChecklistItem.HasSingleTextboxField == 1)
                        {
                            UIFieldTextBox t = (UIFieldTextBox)e.Row.FindControl("ChecklistItemRemarks");
                            t.Visible = true;
                            t.Text = item.Remarks;
                        }
                    }
                }
                else if (item.ChecklistItem.ChecklistType == ChecklistItemType.SingleLineFreeText)
                {
                    UIFieldTextBox textBox = e.Row.FindControl("ChecklistSingleLineTextBox") as UIFieldTextBox;

                    if (textBox != null)
                    {
                        textBox.Visible = true;
                        textBox.Text = item.Remarks;
                        textBox.ValidateRequiredField = (item.ChecklistItem.IsMandatoryField == 1 && this.ValidateRequiredField);
                        textBox.MaxLength = (item.ChecklistItem.MaxCharacterLength != null) ?
                            item.ChecklistItem.MaxCharacterLength.Value : 255;
                    }
                }
                else if (item.ChecklistItem.ChecklistType == ChecklistItemType.MultiLineFreeText)
                {
                    UIFieldTextBox textBox = e.Row.FindControl("ChecklistMultiLineTextBox") as UIFieldTextBox;

                    if (textBox != null)
                    {
                        textBox.Visible = true;
                        textBox.Text = item.Remarks;
                        textBox.ValidateRequiredField = (item.ChecklistItem.IsMandatoryField == 1 && this.ValidateRequiredField);
                        textBox.MaxLength = (item.ChecklistItem.MaxCharacterLength != null) ?
                            item.ChecklistItem.MaxCharacterLength.Value : 255;
                    }
                }
                else if (item.ChecklistItem.ChecklistType == ChecklistItemType.None)
                {
                    e.Row.Font.Bold = true;
                    e.Row.BackColor = System.Drawing.Color.Orange;
                }
            }
        }
    }


    /// <summary>
    /// Occurs when the user clicks a command in the gridview.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ChecklistGridView_Action(object sender, string commandName, List<object> dataKeys)
    {
        if (commandName == "AttachChecklist")
            SearchChecklistDialogBox.Show();
        if (commandName == "ClearChecklist")
        {
            objectPanel panel = getPanel(this.Page);
            LogicLayerPersistentObject persistentObject = (LogicLayerPersistentObject)panel.SessionObject;
            persistentObject.UpdateChecklist(null);

            if (ChecklistIDPropertyName != null && ChecklistIDPropertyName != "")
                DataFrameworkBinder.SetValue(persistentObject, ChecklistIDPropertyName, null);
            ChecklistID.Text = "";

            ChecklistPanel.BindObjectToControls(persistentObject);
        }
    }


    /// <summary>
    /// Occurs when the user confirms his selection in the dialog box.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void SearchChecklistDialogBox_Selected(object sender, EventArgs e)
    {
        foreach (Guid id in SearchChecklistDialogBox.SelectedDataKeys)
        {
            objectPanel panel = getPanel(this.Page);
            LogicLayerPersistentObject persistentObject = (LogicLayerPersistentObject)panel.SessionObject;
            persistentObject.UpdateChecklist(id);

            if (ChecklistIDPropertyName != null && ChecklistIDPropertyName != "")
                DataFrameworkBinder.SetValue(persistentObject, ChecklistIDPropertyName, id);
            ChecklistID.Text = id.ToString();

            ChecklistPanel.BindObjectToControls(persistentObject);
            break;
        }
        if (!MakeButtonsVisibleIfMoreThanOneRow)
        {
            AttachChecklistVisible = false;
            ClearChecklistVisible = false;
        }
    }

    protected void SearchChecklistDialogBox_Search(object sender, SearchEventArgs e)
    {
        if (this.BindChecklistType != "All")
        {
            e.CustomCondition = TablesLogic.tChecklist.ObjectTypeName == (string.IsNullOrEmpty(this.BindChecklistType) ?
                ((PageBase)this.Page).GetRequestObjectType() : this.BindChecklistType);
        }
    }
</script>

<ui:UIPanel runat="server" ID="ChecklistPanel" meta:resourcekey="ChecklistPanelResource">
    <ui:UISeparator runat='server' ID="ChecklistSeparator" Caption="Checklist" meta:resourcekey="ChecklistSeparatorResource" />
    <ui:UIFieldLabel runat="server" ID="ChecklistName" ShowCaption="false" PropertyName="Checklist.ObjectName"
        Font-Bold="true" Font-Size="Larger" meta:resourcekey="ChecklistNameResource">
    </ui:UIFieldLabel>
    <ui:UIFieldTextBox runat="server" ID="ChecklistID" ShowCaption="false" PropertyName="ChecklistID"
        Style='display: none' meta:resourcekey="ChecklistIDResource">
    </ui:UIFieldTextBox>
    <ui:UIGridView runat="server" ID="ChecklistGridView" Caption="Checklist" CheckBoxColumnVisible="False"
        ShowCaption="false" PropertyName="TaskChecklistItems" SortExpression="ChecklistItem.StepNumber"
        OnRowDataBound="ChecklistGridView_RowDataBound" BindObjectsToRows="True" KeyName="ObjectID"
        Width="100%" DataKeyNames="ObjectID" GridLines="Both" ImageRowErrorUrl="" RowErrorColor=""
        Style="clear: both;" OnAction="ChecklistGridView_Action" meta:resourcekey="ChecklistGridViewResource">
        <PagerSettings Mode="NumericFirstLast" />
        <Commands>
            <ui:UIGridViewCommand CommandName="AttachChecklist" CommandText="Choose a Checklist to Attach" meta:resourcekey="ChecklistGridViewUIGridViewCommandChooseaChecklisttoAttachResource" />
            <ui:UIGridViewCommand CommandName="ClearChecklist" CommandText="Clear Checklist" meta:resourcekey="ChecklistGridViewUIGridViewCommandClearChecklistResource" />
        </Commands>
        <Columns>
            <ui:UIGridViewBoundColumn DataField="ChecklistItem.StepNumber" HeaderText="Step"
                PropertyName="ChecklistItem.StepNumber" ResourceAssemblyName="" SortExpression="ChecklistItem.StepNumber"
                HeaderStyle-Width="50px" ItemStyle-Width="50px" meta:resourcekey="ChecklistGridViewUIGridViewBoundColumnChecklistItemStepNumberResource">
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" />
            </ui:UIGridViewBoundColumn>
            <ui:UIGridViewBoundColumn DataField="ChecklistItem.Description" HeaderText="Description" ID="ChecklistGridViewDescription"
                PropertyName="ChecklistItem.Description" ResourceAssemblyName="" SortExpression="ChecklistItem.Description" meta:resourcekey="ChecklistGridViewUIGridViewBoundColumnChecklistItemObjectNameResource">
                <HeaderStyle HorizontalAlign="Left" Width="50%" />
                <ItemStyle HorizontalAlign="Left" />
            </ui:UIGridViewBoundColumn>
            <ui:UIGridViewTemplateColumn>
                <ItemTemplate>
                    <ui:UIFieldRadioList ID="ChecklistRadioList" runat="server"
                        ShowCaption="false" RepeatColumns="0" Visible="false" meta:resourcekey="ChecklistRadioListResource" />
                    <ui:UIFieldDropDownList runat="server" ID="ChecklistDropList"
                        ShowCaption="false" Visible="false" meta:resourcekey="ChecklistDropListResource" />
                    <ui:UIFieldCheckboxList ID="ChecklistCheckboxList" runat="server" CaptionWidth="1px"
                        RepeatColumns="0" Width="100%" Visible="false" meta:resourcekey="ChecklistCheckboxListResource">
                    </ui:UIFieldCheckboxList>
                    <ui:UIFieldTextBox ID="ChecklistSingleLineTextBox" runat="server"
                        ShowCaption="false" Visible="false" meta:resourcekey="ChecklistSingleLineTextBoxResource" />
                    <ui:UIFieldTextBox ID="ChecklistMultiLineTextBox" runat="server"
                        ShowCaption="false" TextMode="MultiLine" Rows="3" Visible="false" meta:resourcekey="ChecklistMultiLineTextBoxResource" />
                    <br />
                    <ui:UIFieldTextBox ID="ChecklistItemRemarks" runat="server" ShowCaption="false"
                        MaxLength="500" Visible="false">
                    </ui:UIFieldTextBox>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" />
            </ui:UIGridViewTemplateColumn>
        </Columns>
    </ui:UIGridView>
    <ui:UISearchDialogBox runat='server' ID="SearchChecklistDialogBox" SearchBehavior="SearchOnDemand"
        SearchOnDemandBaseTableName="tChecklist" SearchOnDemandAutoSearchOnShow="true"
        SearchOnDemandPropertyNames="ObjectName" SearchOnDemandType="SelectQuery" OnSelected="SearchChecklistDialogBox_Selected"
        Title="Choose Checklist" OnSearch="SearchChecklistDialogBox_Search" meta:resourcekey="SearchChecklistDialogBoxResource" ClickRowCommandName="EditObject">
        <Columns>
            <ui:UIGridViewBoundColumn PropertyName="ObjectName" HeaderText="Name" HeaderStyle-Width="200px"
                ItemStyle-Width="200px" meta:resourcekey="ChecklistDropListUIGridViewBoundColumnObjectNameResource">
            </ui:UIGridViewBoundColumn>
            <ui:UIGridViewBoundColumn PropertyName="Benchmark" HeaderText="Benchmark Score" HeaderStyle-Width="400px"
                ItemStyle-Width="400px" DataFormatString="{0:0.00}" meta:resourcekey="ChecklistDropListUIGridViewBoundColumnBenchmarkResource">
            </ui:UIGridViewBoundColumn>
        </Columns>
    </ui:UISearchDialogBox>
</ui:UIPanel>
