﻿<%@ Control Language="C#" ClassName="pageHeader" %>

<script runat="server">
    public string PageTitle
    {
        get { return (string)ViewState["PageTitle"]; }
        set { ViewState["PageTitle"] = value;  }
    }

</script>

<div class="material-title-white">
    <div>
        <span class="title"><%= PageTitle %></span>
    </div>
</div>
