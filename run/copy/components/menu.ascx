<%@ Control Language="C#" ClassName="menu2" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data.Common" %>
<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="System.Web.UI" %>
<%@ Import Namespace="System.Web.UI.WebControls" %>
<%@ Import Namespace="System.ComponentModel" %>
<%@ Import Namespace="Anacle.UIFramework" %>
<%@ Import Namespace="Anacle.DataFramework" %>
<%@ Import Namespace="LogicLayer" %>

<script runat="server">
    public string MobileMenuLinkText
    {
        get { return MobileMenuLink.Text; }
        set { MobileMenuLink.Text = value; }
    }

    public string MobileMenuLinkNavigateUrl
    {
        get { return MobileMenuLink.NavigateUrl; }
        set { MobileMenuLink.NavigateUrl = value; }
    }

    public bool HorizontalMenu
    {
        get { if (ViewState["HorizontalMenu"] == null) return true; return (bool)ViewState["HorizontalMenu"]; }
        set { ViewState["HorizontalMenu"] = value; }
    }

    public bool MobileMenuButtonVisible
    {
        get { return MobileMenuButtonDiv.Visible; }
        set { MobileMenuButtonDiv.Visible = value; }
    }

    public bool MenuVisible
    {
        get { return MenuLiteralDiv.Visible; }
        set { MenuLiteralDiv.Visible = value; }
    }

    public bool MenuUserLoginVisible
    {
        get { return MenuUserLogin.Visible; }
        set { MenuUserLogin.Visible = value; }
    }

    class HtmlListItem
    {
        public string Url;
        public string Text;
        public List<HtmlListItem> Items;

        public HtmlListItem(string url, string text)
        {
            this.Url = url;
            this.Text = text;
            this.Items = new List<HtmlListItem>();
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (!IsPostBack)
        {
            logo.Attributes["src"] = ResolveClientUrl("~/../images/menulogo.png");

            using (Connection c = new Connection())
            {
                // set the user name
                //
                labelName.Text = AppSession.User.ObjectName;

                if (Session["MenuHtml"] == null)
                {
                    // Initialize the menu.
                    //
                    HtmlListItem root = new HtmlListItem("", "");
                    Hashtable parentMenu = new Hashtable();
                    HtmlListItem separator = new HtmlListItem("", "|");

                    // Insert the home / analysis menu items
                    //
                    root.Items.Add(new HtmlListItem(string.IsNullOrEmpty(OApplicationSetting.Current.HomePageUrl) ? "~/home.aspx" : OApplicationSetting.Current.HomePageUrl, Resources.Strings.Menu_Home));

                    // Mobile Web
                    // Only show reports when we are not in the mobile web mode.
                    //
                    if (!((UIPageBase)this.Page).MobileMode)
                    {
                        root.Items.Add(separator);
                        root.Items.Add(new HtmlListItem("~/functionlisting.aspx?ID="
                                + HttpUtility.UrlEncode(Security.Encrypt("##Reports##:" + AppSession.SaltID)), Resources.Strings.Menu_Reports));
                    }

                    // set up the link URL
                    //
                    OFunction userFunction = OFunction.GetFunctionByObjectType("OUser");
                    linkEditProfile.NavigateUrl =
                    linkEditProfile2.NavigateUrl =
                        userFunction.EditUrl +
                        (userFunction.EditUrl.Contains("?") ? "&" : "?") +
                        "ID=" + HttpUtility.UrlEncode(Security.Encrypt("EDIT:" + AppSession.User.ObjectID.ToString() + ":EDITPROFILE:" + AppSession.SaltID)) +
                        "&TYPE=" + HttpUtility.UrlEncode(Security.Encrypt("OUser" + ":" + AppSession.SaltID));

                    Session["EditProfileUrl"] = linkEditProfile.NavigateUrl;

                    // load menu items from the database.
                    //
                    DataTable dt = OFunction.GetMenusAccessibleByUser(AppSession.User);
                    dt.Columns.Add("TranslatedFunctionName");
                    foreach (DataRow dr in dt.Rows)
                    {
                        dr["TranslatedFunctionName"] = TranslateMenuItem(dr["FunctionName"].ToString());
                    }

                    dt.DefaultView.Sort = "DisplayOrder, TranslatedFunctionName";
                    foreach (DataRowView drv in dt.DefaultView)
                    {
                        if (((UIPageBase)this.Page).MobileMode)
                        {
                            if (drv["MainMobileUrl"] == DBNull.Value || drv["MainMobileUrl"].ToString().Trim() == "")
                                continue;
                        }
                        else
                        {
                            if (drv["MainUrl"] == DBNull.Value || drv["MainUrl"].ToString().Trim() == "")
                                continue;
                        }

                        DataRow dr = drv.Row;
                        HtmlListItem parentMenuItem = null;
                        HtmlListItem subMenuItem = null;

                        // now we add this into the menu
                        //
                        if (parentMenu.ContainsKey(dr["CategoryName"].ToString()))
                        {
                            parentMenuItem = parentMenu[dr["CategoryName"].ToString()] as HtmlListItem;
                        }
                        else
                        {
                            parentMenuItem = new HtmlListItem("~/functionlisting.aspx?ID="
                                + HttpUtility.UrlEncode(Security.Encrypt(dr["CategoryName"].ToString() + ":" + AppSession.SaltID)),
                                TranslateMenuItem(dr["CategoryName"].ToString()));

                            parentMenu[dr["CategoryName"].ToString()] = parentMenuItem;
                            root.Items.Add(separator);
                            root.Items.Add(parentMenuItem);
                        }

                        // we add a new sub menu if present
                        //
                        if (dr["SubCategoryName"].ToString() != "")
                        {
                            if (parentMenu.ContainsKey(dr["CategoryName"].ToString() + ":::" + dr["SubCategoryName"].ToString()))
                            {
                                subMenuItem = parentMenu[dr["CategoryName"].ToString() + ":::" + dr["SubCategoryName"].ToString()] as HtmlListItem;
                            }
                            else
                            {
                                subMenuItem = new HtmlListItem("", TranslateMenuItem(dr["SubCategoryName"].ToString()));
                                parentMenu[dr["CategoryName"].ToString() + ":::" + dr["SubCategoryName"].ToString()] = subMenuItem;
                                parentMenuItem.Items.Add(subMenuItem);
                            }
                        }

                        // Mobile Web
                        // Generate a different URL link for mobile web.
                        //
                        string mainUrl = dr["MainUrl"].ToString();
                        if (((UIPageBase)this.Page).MobileMode)
                            mainUrl = dr["MainMobileUrl"].ToString();
                        HtmlListItem childMenuItem = childMenuItem = new HtmlListItem(
                            mainUrl + (mainUrl.Contains("?") ? "&" : "?") +
                            "TYPE=" +
                            HttpUtility.UrlEncode(Security.Encrypt(dr["ObjectTypeName"].ToString() + ":" + AppSession.SaltID)),
                            TranslateMenuItem(dr["FunctionName"].ToString())); ;

                        if (subMenuItem != null)
                            subMenuItem.Items.Add(childMenuItem);
                        else
                            parentMenuItem.Items.Add(childMenuItem);
                    }


                    // Mobile Web
                    // Only show reports when we are not in the mobile web mode.
                    //
                    if (!((UIPageBase)this.Page).MobileMode)
                    {
                        // load the reports from the database.
                        //
                        DataTable reports = OReport.GetAllReportsByUserAndCategoryName(AppSession.User);

                        // figure out which category names we should show
                        //
                        List<string> categoryNames = new List<string>();
                        foreach (DataRow dr in reports.Rows)
                        {
                            if (!categoryNames.Contains(dr["CategoryName"].ToString()))
                                categoryNames.Add(dr["CategoryName"].ToString());
                        }
                        categoryNames.Sort();

                        foreach (string categoryName in categoryNames)
                        {
                            HtmlListItem subMenu = new HtmlListItem("", TranslateReportItem(categoryName));
                            root.Items[2].Items.Add(subMenu);

                            foreach (DataRow report in reports.Rows)
                            {
                                if (report["CategoryName"].ToString() == categoryName)
                                {
                                    HtmlListItem reportMenu = new HtmlListItem(
                                        "~/modules/reportviewer/search.aspx" +
                                        "?ID=" +
                                        HttpUtility.UrlEncode(Security.Encrypt(report["ObjectID"].ToString() + ":" + AppSession.SaltID)),
                                        report["ReportCode"] + " - " + TranslateReportItem(report["ReportName"].ToString()));
                                    subMenu.Items.Add(reportMenu);
                                }
                            }
                        }
                    }

                    colors = new Dictionary<string, string>();
                    foreach (OApplicationSettingMenuItem item in OApplicationSetting.Current.TempMenuItems)
                    {
                        colors.Add(TranslateMenuItem(item.MenuName), item.DarkColor);
                    }

                    // Render the menu out to HTML
                    //
                    StringBuilder sb = new StringBuilder();
                    RenderMenuRoot(root, sb);
                    MenuLiteral.Text = sb.ToString();

                    Session["MenuHtml"] = MenuLiteral.Text;
                }
                else
                {
                    MenuLiteral.Text = (string)Session["MenuHtml"];
                    linkEditProfile.NavigateUrl = (string)Session["EditProfileUrl"];
                    linkEditProfile2.NavigateUrl = (string)Session["EditProfileUrl"];
                }
            }
        }

        if (!HorizontalMenu)
        {
            MenuLiteralDiv.CssClass = "cssmenum";

            ScriptManager.RegisterStartupScript(this, this.GetType(), "menu", @"        
                $('.cssmenum > ul > li > a').on('click', function() {
                    if ($(this).next('ul').is(':visible')) {
                        $('.cssmenum > ul > li > ul')
                            .offset({left:0, top:0})
                            .hide();
                    }
                    else {
                        $('.cssmenum > ul > li > ul')
                            .offset({left:0, top:0})
                            .hide();
                        $(this).next('ul').show();
                    }
                });

                $('.cssmenum > ul > li > ul > li > a').on('click', function() {
                    if ($(this).next('ul').is(':visible')) {
                        $('.cssmenum > ul > li > ul > li > ul')
                            .offset({left:0, top:0})
                            .hide();
                    }
                    else {
                        $('.cssmenum > ul > li > ul > li > ul')
                            .offset({left:0, top:0})
                            .hide();
                        $(this).next('ul').show();
                    }
                });", true);
        }
        else
        {
            MenuLiteralDiv.CssClass = "cssmenu";

            ScriptManager.RegisterStartupScript(this, this.GetType(), "menu", @"        
                function hideLevel2Menus()
                {
                    $('.cssmenu > ul > li > ul')
                        .offset({left:0, top:0})
                        .hide();
                }
                function hideLevel3Menus()
                {
                    $('.cssmenu > ul > li > ul > li > ul')
                        .offset({left:0, top:0})
                        .css('max-height','')
                        .hide();
                }
                function hideMenus()
                {
                    $('.cssmenu > ul > li > ul > li > ul')
                        .offset({left:0, top:0})
                        .hide();                        
                    $('.cssmenu > ul > li > ul')
                        .offset({left:0, top:0})
                        .hide();
                    $('#menuoverlay').hide();
                }

                function showNextLevel(e, w, h, lvl)
                {
                    var el = $(e);
                    var ofs = el.offset();
                    var fleft = ofs.left + w;
                    var ftop = ofs.top + h;
                    var sw = $(window).width();
                    var sh = $(window).height();
                    var elN = el.next('ul');
                    var mw = elN.width();
                    var mh = elN.height();

                    if (lvl==2) 
                    {
                        if(fleft + mw > sw)     fleft = sw - mw;
                        if(fleft < 0)           fleft = 0;
                        if(ftop + mh > sh)      ftop = sh - mh;
                        if(ftop < 0)            ftop = 0;
                    }
                    else if (lvl==3)
                    {
                        if(fleft + mw > sw)     fleft = ofs.left - mw;
                        if(fleft < 0)           fleft = 0;
                        if(ftop + mh > sh)      ftop = sh - mh;
                        if(ftop < 0)            ftop = 0;
                    }
                    elN
                        .offset({ left: fleft, top: ftop })
                        .css('margin-left','0')
                        .css('max-height','100%')
                        .show();
                    $('#menuoverlay').show();
                }

                $('#menuoverlay').on('mouseover', function(event) { hideMenus(); });
                                            
                $('body').on('click', function(event) {
                    var el = event.target;
                    if(!$(el).parent('.cssmenu').length)
                    {
                        hideMenus();
                    }
                });

                $('.cssmenu > ul > li > span').on('mouseover', function(event) {
                    hideMenus();
                });

                $('.cssmenu > ul > li > a').on('mouseover', function(event) {
                    event.stopPropagation();
                    hideLevel3Menus();
                    hideLevel2Menus();
                    showNextLevel(this, 0, 50, 2);
                });

                $('.cssmenu > ul > li > ul > li > a').on('mouseover', function(event) {
                    event.stopPropagation();                
                    hideLevel3Menus();   
                    showNextLevel(this, 250, 0, 3);
                });

                $('.cssmenu > ul > li > a').on('click', function(event) {
                    event.stopPropagation();                
                    if( !$(this).next('ul').length ) 
                    {
                        hideMenus();
                        return;
                    }
                    hideLevel3Menus();   
                    hideLevel2Menus();   
                    showNextLevel(this, 0, 50, 2);
                });

                $('.cssmenu > ul > li > ul > li > a').on('click', function(event) {
                    event.stopPropagation();                
                    if( !$(this).next('ul').length ) 
                    {
                        hideMenus();
                        return;
                    }
                    hideLevel3Menus();   
                    showNextLevel(this, 250, 0, 3);
                });

                $('.cssmenu > ul > li > ul > li > ul > li > a').on('click', function(event) {
                    event.stopPropagation();                
                    if( !$(this).next('ul').length ) 
                    {
                        hideMenus();
                        return;
                    }
                });

                $(window).on('resize', function () {
                    menuResize();
                });

                var scroll;
                var $menu;
                var margin = 0;
                var cW;
                var mW;

                $('.cssmenu-scrollRight').on('mouseover', function(event) {
                    hideMenus();
                    scroll = setInterval(function(){
                        if(margin <= 0)
                            $('.cssmenu-scrollLeft').show();

                        if((mW - cW - margin) <= 0) {
                            clearInterval(scroll);
                            margin = mW - cW;
                            $('.cssmenu-scrollRight').hide();
                        }
                        else
                            margin+=10;

                        $menu.css('margin-left', '-' + margin + 'px');
                    },24);
                });

                $('.cssmenu-scrollRight').on('mouseout', function(event) {
                    clearInterval(scroll);
                });

                $('.cssmenu-scrollLeft').on('mouseover', function(event) {
                    hideMenus();
                    scroll = setInterval(function(){
                        if(margin >= mW - cW - margin)
                            $('.cssmenu-scrollRight').show();

                        if(margin <= 0) {
                            clearInterval(scroll);
                            margin = 0;
                            $('.cssmenu-scrollLeft').hide();
                        }
                        else
                            margin-=10;

                        $menu.css('margin-left', '-' + margin + 'px');
                    },24);
                });

                $('.cssmenu-scrollLeft').on('mouseout', function(event) {
                    clearInterval(scroll);
                });

                function menuResize() {
                    cW = $container.width();
                    mW = $menu.width();
                    if (cW > mW) {
                        $('.cssmenu-scrollRight').hide();
                        $('.cssmenu-scrollLeft').hide();
                        margin=0;
                        $menu.css('margin-left', '-' + margin + 'px');
                    }
                    else {
                        if((mW - cW - margin) <= 0) {
                            clearInterval(scroll);
                            margin = mW - cW;
                            $('.cssmenu-scrollRight').hide();
                            $menu.css('margin-left', '-' + margin + 'px');
                        }
                        else {
                            $('.cssmenu-scrollRight').show();
                        }
                    }
                }

                $container = $('.cssmenu');
                $menu = $('.cssmenu ul');
                $(document).ready(function(){
                    menuResize();
                    $('.cssmenu-scrollLeft').hide();
                    $('form').css('overflow','hidden');
                    $('body').css('min-width','0');
                });
                ", true);
            // due to a weird bug in chrome (if we set .cssmenu{overflow:hidden}, and the page contain an iframe with canvas, the menu will be hidden)
            // to overcome this, we set the overflow:hidden at form tag
        }
    }

    Dictionary<string, string> colors;
    /// <summary>
    /// Renders the root menu to a literal.
    /// </summary>
    /// <param name="menu"></param>
    /// <param name="sb"></param>
    /// <returns></returns>
    void RenderMenuRoot(HtmlListItem root, StringBuilder sb)
    {
        int width = 43;
        if (root.Items.Count > 1)
        {
            width += (root.Items.Count - 1) / 2 * 155;
        }

        sb.Append("<ul style='width:" + width + "px;'>");
        foreach (HtmlListItem menu in root.Items)
        {
            if (menu.Text == "|")
            {
                sb.Append("<li><span/><li>");
            }
            else if (menu.Text == Resources.Strings.Menu_Home)
            {
                string href = "javascript:v(0)";
                if (menu.Url != "")
                    href = ResolveUrl(menu.Url);
                sb.Append("<li>");
                sb.Append("<a href='" + href + "' style='width:auto;padding:0 5px;vertical-align:middle;' target='frameBottom'><img src='images/home.png'></a>");
                sb.Append("</li>");
            }
            else
            {
                if (menu.Text != "" || menu.Url != "")
                {
                    if (menu.Items.Count > 0)
                        sb.Append("<li class='has-sub'>");
                    else
                        sb.Append("<li>");

                    string href = "javascript:v(0)";
                    if (menu.Url != "")
                        href = ResolveUrl(menu.Url);

                    if (((UIPageBase)this.Page).MobileMode)
                    {
                        if (href != "javascript:v(0)")
                        {
                            href = ResolveUrl("~/apptop.aspx?url=" + HttpUtility.UrlEncode(href));
                            sb.Append("<a href='" + href + "'>" + HttpUtility.HtmlEncode(menu.Text) + "</a>");
                        }
                        else
                        {
                            sb.Append("<a href='" + href + "'>" + HttpUtility.HtmlEncode(menu.Text) + "</a>");
                        }
                    }
                    else
                        sb.Append("<a href='" + href + "' style='background-color:" + colors[menu.Text] + ";' target='frameBottom'><div class='cssmenu-item-label'>" + HttpUtility.HtmlEncode(menu.Text)
                            + "</div><div class='cssmenu-item-right'></div>"
                            + "</a>");
                }

                if (menu.Items.Count > 0)
                {
                    sb.Append("<ul>");
                    foreach (HtmlListItem subItem in menu.Items)
                        RenderMenu(subItem, sb);
                    sb.Append("</ul>");
                }

                if (menu.Text != "" || menu.Url != "")
                {
                    sb.Append("</li>");
                }
            }
        }
        sb.Append("</ul>");
    }


    /// <summary>
    /// Renders the menu to a literal.
    /// </summary>
    /// <param name="menu"></param>
    /// <param name="sb"></param>
    /// <returns></returns>
    void RenderMenu(HtmlListItem menu, StringBuilder sb)
    {
        if (menu.Text != "" || menu.Url != "")
        {
            if (menu.Items.Count > 0)
                sb.Append("<li class='has-sub'>");
            else
                sb.Append("<li>");

            string href = "javascript:v(0)";
            if (menu.Url != "")
                href = ResolveUrl(menu.Url);

            if (((UIPageBase)this.Page).MobileMode)
            {
                if (href != "javascript:v(0)")
                {
                    href = ResolveUrl("~/apptop.aspx?url=" + HttpUtility.UrlEncode(href));
                    sb.Append("<a href='" + href + "'>" + HttpUtility.HtmlEncode(menu.Text) + "</a>");
                }
                else
                {
                    sb.Append("<a href='" + href + "'>" + HttpUtility.HtmlEncode(menu.Text) + "</a>");
                }
            }
            else
                sb.Append("<a href='" + href + "' target='frameBottom'>" + HttpUtility.HtmlEncode(menu.Text) + "</a>");
        }

        if (menu.Items.Count > 0)
        {
            sb.Append("<ul>");
            foreach (HtmlListItem subItem in menu.Items)
                RenderMenu(subItem, sb);
            sb.Append("</ul>");
        }

        if (menu.Text != "" || menu.Url != "")
        {
            sb.Append("</li>");
        }
    }


    /// <summary>
    /// Translates the specified text from the objects.resx file.
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    protected string TranslateMenuItem(string text)
    {
        if (text == "##Reports##")
            return Resources.Strings.Menu_Reports;

        string translatedText = Resources.Objects.ResourceManager.GetString(text);
        if (translatedText == null || translatedText == "")
            return text;
        return translatedText;
    }


    /// <summary>
    /// Translates the specified text from the reports.resx file.
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    protected string TranslateReportItem(string text)
    {
        string translatedText = Resources.Reports.ResourceManager.GetString(text);
        if (translatedText == null || translatedText == "")
            return text;
        return translatedText;
    }


    protected void linkLogout_Click(object sender, EventArgs e)
    {
        OUser user = Session["User"] as OUser;
        if (Session["SessionId"] != null)
        {
            try
            {
                Security.Logoff((Guid)Session["SessionId"]);
            }
            catch
            {
                // Since the user is logging off from the system
                // we should worry about the log off method
                // throwing exceptions due to SQL failures.
                //
                // So we just ignore, and proceed to clear the
                // session and direct the user back to the
                // log in page.
            }
        }
        Session.Clear();

        // AddedBy: Yiyuan
        // AddedDate: 13-Jan-2012
        // Set the user LastAccessTime to null when clicking logoff button
        //
        using (Connection c = new Connection())
        {
            OUser u = TablesLogic.tUser.Load(user.ObjectID);
            //u.LastAccessTime = null;
            u.SessionID = null;
            u.Save();
            c.Commit();
        }

        // 2010.09.27
        // Kim Foong
        // If we are logging on with Windows, then when the user clicks on
        // the log out button, instead of redirecting him/her back to the
        // log in page (which will cause him/her to auto login again), 
        // we simply just close the window.
        //
        if (ConfigurationManager.AppSettings["AuthenticateWithWindowsLogon"].ToLower() == "true")
            Window.Close();
        else
            Response.Redirect(ResolveUrl("~/applogin.aspx"));
    }


    protected override void Render(HtmlTextWriter writer)
    {
        if (!this.Page.Request.FilePath.Contains("menumbl.aspx"))
        {
            if (((UIPageBase)this.Page).MobileMode)
            {
                MenuUserLogin.Visible = false;
                MenuLiteralDiv.Visible = false;
            }
            else
            {
                MobileMenuButtonDiv.Visible = false;
            }
        }
        base.Render(writer);

    }

</script>

<asp:Panel runat='server' ID="MenuUserLogin" CssClass='menu menu-top' meta:resourcekey="MenuUserLoginResource" >
    <img runat="server" id="logo" src="" height="21" class="menulogo" />
    <asp:Label runat='server' Visible="false" ID="WelcomeLabel" Text="welcome" meta:resourcekey="WelcomeLabelResource" ></asp:Label>
    <asp:Label ID="labelName" runat="server" meta:resourcekey="labelNameResource1"></asp:Label>
    <asp:HyperLink runat="server" ID="linkEditProfile" Text="Edit Profile" Target="AnacleEAM_Window"
        meta:resourcekey="linkEditProfileResource1" CssClass="hlink-white"></asp:HyperLink>
    |
    <asp:LinkButton ID="linkLogout" runat="server" Text="Logout" OnClick="linkLogout_Click"
        meta:resourcekey="linkLogoutResource1" CssClass="hlink-white"></asp:LinkButton>
</asp:Panel>
<div id='menuoverlay' style='width: 100%; height: 100%; top: 0; position: fixed;
    z-index: 10; display: none'>
</div>
<asp:Panel runat='server' ID="MobileMenuButtonDiv" class="menu menu-top" meta:resourcekey="MobileMenuButtonDivResource" >
    <table width='100%' cellpadding='5'>
        <tr>
            <td align='left'>
                <asp:HyperLink runat='server' ID='MobileMenuLink' Text="Menu" NavigateUrl="~/menumbl.aspx"
                    CssClass="hlink-white" meta:resourcekey="MobileMenuLinkResource" >
                </asp:HyperLink>
            </td>
            <td align='right'>
                <asp:HyperLink runat="server" ID="linkEditProfile2" Text="Edit Profile"
                    meta:resourcekey="linkEditProfileResource1" CssClass="hlink-white"></asp:HyperLink>
                |
                <asp:LinkButton ID="LinkButton1" runat="server" Text="Logout" OnClick="linkLogout_Click"
                    meta:resourcekey="linkLogoutResource1" CssClass="hlink-white"></asp:LinkButton>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel runat='server' ID="MenuLiteralDiv" class="menu" meta:resourcekey="MenuLiteralDivResource" >
    <asp:Literal runat='server' ID='MenuLiteral' meta:resourcekey="MenuLiteralResource" ></asp:Literal>
    <div class="cssmenu-scrollRight">
        <img src="images/menu-right-arrow.png" />
    </div>
    <div class="cssmenu-scrollLeft">
        <img src="images/menu-left-arrow.png" />
    </div>
</asp:Panel>
