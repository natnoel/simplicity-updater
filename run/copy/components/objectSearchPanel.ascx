<%@ Control Language="C#" ClassName="objectSearchPanel" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="System.Web.UI" %>
<%@ Import Namespace="System.Web.UI.WebControls" %>
<%@ Import Namespace="System.ComponentModel" %>
<%@ Import Namespace="Anacle.DataFramework" %>
<%@ Import Namespace="LogicLayer" %>
<%@ Import Namespace="Anacle.WorkflowFramework" %>

<script runat="server">

    /// <summary>
    /// Show paging setting on search page
    /// </summary>
    [Default(false)]
    private bool customPageSize = false;
    public bool CustomPageSize
    {
        get { return customPageSize; }
        set { customPageSize = value; }
    }

    /// Mobile Web
    /// <summary>
    /// Gets and sets a flag on the UIPageBase object to indicate if
    /// this page will be rendered in a mobile mode.
    /// </summary>
    public bool MobileMode
    {
        get
        {
            return ((UIPageBase)this.Page).MobileMode;
        }
        set
        {
            ((UIPageBase)this.Page).MobileMode = value;
        }
    }


    /// <summary>
    /// Gets or sets a flag indicating whether the search should be automatically
    /// performed when the page is loaded.
    /// </summary>
    public bool AutoSearchOnLoad
    {
        get
        {
            if (ViewState["AutoSearchOnLoad"] == null)
                return true;
            return (bool)ViewState["AutoSearchOnLoad"];
        }
        set
        {
            ViewState["AutoSearchOnLoad"] = value;
        }
    }



    /// <summary>
    /// Gets the simple search UIFieldTextBox control.
    /// </summary>
    [DefaultValue(null), Localizable(false)]
    public UIFieldTextBox SearchTextBox
    {
        get
        {
            return SimpleSearch;
        }
    }


    /// <summary>
    /// Gets or sets the visibility of the simple search textbox.
    /// </summary>
    [DefaultValue(""), Localizable(false)]
    public string SearchTextBoxHint
    {
        get
        {
            return this.SimpleSearchHint.Text;
        }
        set
        {
            this.SimpleSearchHint.Text = value;
        }
    }


    /// <summary>
    /// Gets or sets the visibility of the simple search textbox.
    /// </summary>
    [DefaultValue(false), Localizable(false)]
    public bool SearchTextBoxVisible
    {
        get
        {
            return this.panelSimpleSearch.Visible;
        }
        set
        {
            this.panelSimpleSearch.Visible = value;
        }
    }

    /// <summary>
    /// Comma seperated values of property names used for simple search
    /// </summary>
    protected string simpleSearchFields = "";

    /// <summary>
    /// Gets or sets the visibility of the simple search textbox.
    /// </summary>
    [DefaultValue(""), Localizable(false)]
    public string SearchTextBoxPropertyNames
    {
        get
        {
            return this.simpleSearchFields;
        }
        set
        {
            this.simpleSearchFields = value;
        }
    }

    /// <summary>
    /// Value for the ID of the advanced search panel
    /// </summary>
    protected string advSearchID = "";

    /// <summary>
    /// Gets or sets the visibility of the simple search textbox.
    /// </summary>
    [DefaultValue(""), Localizable(false), IDReferenceProperty]
    public string AdvancedSearchPanelID
    {
        get
        {
            return this.advSearchID;
        }
        set
        {
            this.advSearchID = value;
        }
    }

    protected bool enableResultsCap = true;

    /// <summary>
    /// Gets or sets maximum number of results returned by the search
    /// </summary>

    public bool EnableResultsCap
    {
        get
        {
            return this.enableResultsCap;
        }
        set
        {
            this.enableResultsCap = value;
        }
    }

    public enum MaxNumberOfResults { _50 = 50, _100 = 100, _300 = 300, _500 = 500, _1000 = 1000, _1500 = 1500, _2000 = 2000, _5000 = 5000 };

    /// <summary>
    /// Gets or sets maximum number of results returned by the search
    /// </summary>
    [DefaultValue(MaxNumberOfResults._500), Localizable(false)]
    public MaxNumberOfResults MaximumNumberOfResults
    {
        get
        {
            if (ViewState["MaximumNumberOfResults"] == null)
                ViewState["MaximumNumberOfResults"] = MaxNumberOfResults._500;

            return (MaxNumberOfResults)ViewState["MaximumNumberOfResults"];
        }
        set
        {
            ViewState["MaximumNumberOfResults"] = value;
        }
    }

    public enum NumberOfResults { _20 = 20, _50 = 50, _100 = 100, _200 = 200, _500 = 500, _1000 = 1000, _1500 = 1500, _2000 = 2000, _5000 = 5000 };

    /// <summary>
    /// Gets or sets maximum number of results returned by the search
    /// </summary>
    [DefaultValue(NumberOfResults._20), Localizable(false)]
    public NumberOfResults ResultsPerPage
    {
        get
        {
            if (ViewState["ResultsPerPage"] == null)
                ViewState["ResultsPerPage"] = NumberOfResults._20;

            return (NumberOfResults)ViewState["ResultsPerPage"];
        }
        set
        {
            ViewState["ResultsPerPage"] = value;
        }
    }

    /// <summary>
    /// Gets or sets the caption that appears in the title of
    /// the objectSearchPanel.ascx control.
    /// </summary>
    [Localizable(true), Description("Gets or sets the caption that appears in the title of the objectSearchPanel.ascx control.")]
    public string Caption
    {
        get
        {
            return labelCaption.Text;
        }
        set
        {
            labelCaption.Text = value;
        }
    }


    // 2013.07.06
    // Kim Foong
    /// <summary>
    /// A flag that indicates whether the message text was changed.
    /// </summary>
    private bool messageChanged = false;


    /// <summary>
    /// Gets or sets the message that appears in the message bar. The message
    /// bar will show only if the Message property is not an empty string.
    /// </summary>
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    [Description("Gets or sets the message that appears in the message bar. The message bar will show only if the Message property is not an empty string.")]
    public string Message
    {
        get
        {
            return labelMessage.Text;
        }
        set
        {
            //if (value != null && value.Trim() != "")
            //    panelMessage.Update();
            //labelMessage.Text = value;
            labelMessage.Text = Server.HtmlDecode(value);

            // 2013.07.26
            // Kim Foong
            // Set the message changed to true to force the animation to run.
            //
            if (value != null && value.Trim() != "")
                messageChanged = true;
            else
                messageChanged = false;
        }
    }

    /// <summary>
    /// Gets the Panel control identified by the ID specified in AdvancedSearchPanelID.
    /// </summary>
    [DefaultValue(""), Localizable(false), IDReferenceProperty]
    [Description("Gets the Panel control identified by the ID specified in AdvancedSearchPanelID.")]
    public UIPanel AdvancedSearchPanel
    {
        get
        {
            Control c = null;
            if (NamingContainer != null)
                c = NamingContainer.FindControl(advSearchID);
            if (c == null)
                c = Page.FindControl(advSearchID);
            if (c is UIPanel)
                return (UIPanel)c;
            else
                return null;
        }
    }


    protected UIDialogBox DialogBox
    {
        get
        {
            // look for the containing UIDialogBox control.
            //
            if (this.AdvancedSearchPanel != null)
            {
                Control c = this.AdvancedSearchPanel.Parent;
                while (c != null)
                {
                    if (c is UIDialogBox)
                        break;
                    c = c.Parent;
                }

                if (c != null && c is UIDialogBox)
                    return (UIDialogBox)c;
                return null;
            }
            return null;
        }
    }


    /// <summary>
    /// Gets the UIObjectPanel that contains this search panel control.
    /// <para></para>
    /// Note: A UITabStrip is a subclass of the UIObjectPanel class.
    /// </summary>
    [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    [Description("Gets the UIObjectPanel that contains this search panel control.")]
    public UIObjectPanel ObjectPanel
    {
        get
        {
            // look for the containing UIForm control, and calls
            // the ValidateControls method.
            //
            Control c = this.Parent;
            while (c != null)
            {
                if (c is UIObjectPanel)
                    break;
                c = c.Parent;
            }

            // Perform the validation if possible. If the validation
            // fails, then we simply return just return without
            // calling the Clicked event.
            //
            if (c != null && c is UIObjectPanel)
                return (UIObjectPanel)c;
            else
                throw new Exception("The web:panel control must be placed in a UIObjectPanel control, but it is currently not.");
        }
    }


    /// <summary>
    /// The ID of the UIGridView control to which all results will be bound.
    /// </summary>
    protected string gridViewID = "";


    /// <summary>
    /// Gets or sets the ID of the UIGridView control to which all results will be bound.
    /// </summary>
    [DefaultValue(""), Localizable(false), IDReferenceProperty]
    [Description("Gets or sets the ID of the UIGridView control to which all results will be bound.")]
    public string GridViewID
    {
        get
        {
            return gridViewID;
        }
        set
        {
            gridViewID = value;
        }
    }


    /// <summary>
    /// The UIGridView control to which all results will be bound.
    /// </summary>
    [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public UIGridView GridView
    {
        get
        {
            Control c = null;
            if (NamingContainer != null)
                c = NamingContainer.FindControl(gridViewID);
            if (c == null)
                c = Page.FindControl(gridViewID);
            if (c is UIGridView)
                return (UIGridView)c;
            else
                throw new Exception("There are no UIGridViews with the ID '" + gridViewID + "'");
        }
    }


    /// <summary>
    /// Gets or sets a value to indicate whether the search will be performed
    /// using a select using (tXXXX.Select) query or an Object (tXXXX.LoadList) 
    /// query.
    /// </summary>
    /// <remarks>
    /// There are two modes of searching: SelectQuery, ObjectQuery.
    /// <para></para>
    /// The SelectQuery is achieved by using the TablesLogic.tXXXX.Select(column1,
    /// column2, ...).Where(condition) method. The columns used for the query
    /// will be based on the Bound columns' PropertyName defined in the UIGridView.
    /// <para></para>
    /// The advantage of this method is fast because it performs the entire search 
    /// in a single SQL query. The disadvantage is that complex computations or
    /// transformation of data is more difficult to achieve.
    /// <para></para>
    /// The ObjectQuery is achieved by using the TablesLogic.tXXXX.LoadList(condition) 
    /// method. This method loads all objects and binds it to the UIGridView, and
    /// data from the individual PersistentObject's properties will be bound to 
    /// each individual column.
    /// <para></para>
    /// The advantage of this method is more flexibility because it uses the 
    /// PersistentObject properties, which can either be the object's database field,
    /// or a transformation programmed in C#. The disadvantage is that
    /// this method can be potentially slow, especially when the properties are
    /// too complex to compute, or executes additional queries, which mean more 
    /// access to the database.
    /// </remarks>
    [Description("Gets or sets a value to indicate whether the search will be performed using a select using (tXXXX.Select) query or an Object (tXXXX.LoadList) query.")]
    public PanelSearchType SearchType
    {
        get
        {
            if (ViewState["SearchType"] == null)
                return PanelSearchType.SelectQuery;
            return (PanelSearchType)ViewState["SearchType"];
        }
        set
        {
            ViewState["SearchType"] = value;
        }
    }


    /// <summary>
    /// Contains a list of all possible search types that the objectSearchPanel.ascx
    /// control supports.
    /// </summary>
    public enum PanelSearchType
    {
        SelectQuery,
        ObjectQuery
    }


    /// <summary>
    /// Gets or sets a flag to indicate if the Create New button is visible.
    /// </summary>
    [DefaultValue(true), Localizable(false)]
    [Description("Gets or sets a flag to indicate if the Create New button is visible.")]
    public bool AddButtonVisible
    {
        get
        {
            return spanAdd.Visible;
        }
        set
        {
            spanAdd.Visible = value;
        }
    }


    /// <summary>
    /// Gets or sets a flag to indicate if the Create New button is visible.
    /// </summary>
    [DefaultValue(true), Localizable(false)]
    [Description("Gets or sets a flag to indicate if the Add Selected button is visible.")]
    public bool AddSelectedButtonVisible
    {
        get
        {
            //return spanAddSelected.Visible;
            return commandAddSelected.Visible;
            //return buttonAddSelected.Visible;
        }
        set
        {
            //spanAddSelected.Visible = value;
            //buttonAddSelected.Visible = value;
            //literalBreak.Visible = value;
            commandAddSelected.Visible = value;
            commandVisibleSet = true;
        }
    }


    /// <summary>
    /// A flag to indicate if the Edit button is visible
    /// </summary>
    private bool editButtonVisible = true;


    /// <summary>
    /// Gets or sets a flag to indicate if the Edit button
    /// is visible.
    /// </summary>
    [DefaultValue(true), Localizable(false)]
    [Description("Gets or sets a flag to indicate if the Edit button is visible.")]
    public bool EditButtonVisible
    {
        get
        {
            return editButtonVisible;
        }
        set
        {
            editButtonVisible = value;
            spanEdit.Visible = value;
        }
    }


    /// <summary>
    /// A flag to indicate if the View button is visible
    /// </summary>
    private bool viewButtonVisible = true;


    /// <summary>
    /// Gets or sets a flag to indicate if the View button
    /// is visible.
    /// </summary>
    [DefaultValue(true), Localizable(false)]
    [Description("Gets or sets a flag to indicate if the View button is visible.")]
    public bool ViewButtonVisible
    {
        get
        {
            return viewButtonVisible;
        }
        set
        {
            viewButtonVisible = value;
            spanEdit.Visible = value;
        }
    }



    /// <summary>
    /// Gets or sets a flag to indicate if the Perform Search button is visible.
    /// </summary>
    [DefaultValue(false), Localizable(false)]
    [Description("Gets or sets a flag to indicate if the Perform Search button is visible.")]
    public bool SearchButtonVisible
    {
        get
        {
            return spanSearch.Visible;
        }
        set
        {
            spanSearch.Visible = value;
        }
    }



    /// <summary>
    /// Gets or sets a flag to indicate if the Reset Fields button is visible.
    /// </summary>
    [DefaultValue(false), Localizable(false)]
    [Description("Gets or sets a flag to indicate if the Reset Fields button is visible.")]
    public bool ResetButtonVisible
    {
        get
        {
            return spanReset.Visible;
        }
        set
        {
            spanReset.Visible = value;
        }
    }


    /// <summary>
    /// Gets or sets a flag that indicates if the Close Window button
    /// is visible to the user.
    /// </summary>
    [DefaultValue(true), Localizable(false)]
    public bool CloseWindowButtonVisible
    {
        get
        {
            return buttonCancel.Visible;
        }
        set
        {
            buttonCancel.Visible = value;
        }
    }


    /*
        /// <summary>
        /// Gets or sets a boolean value indicating whether the 
        /// Show Items Assigned to Me checkbox is checked.
        /// </summary>
        [DefaultValue(true), Localizable(false)]
        [Description("Gets or sets a boolean value indicating whether the Show Items Assigned to Me checkbox is checked.")]
        public bool SearchAssignedOnly
        {
            get
            {
                return AssignedOnly.Checked;
            }
            set
            {
                AssignedOnly.Checked = value;
            }
        }
    */

    // 2016.09.20
    // Kien Trung
    // NEW: To allow submittor / creator to view access to their own document
    //
    bool viewAccess = false;
    bool editAccess = false;
    int clickColumnIndex = -1;
    int editColumnIndex = -1;
    int viewColumnIndex = -1;
    int deleteColumnIndex = -1;

    bool commandVisibleSet = false;
    UIButton buttonAddSelected = new UIButton();
    Literal literalBreak = new Literal();
    UIGridViewCommand commandAddSelected = new UIGridViewCommand();

    OUserSearchGridViewPreference pref = null;


    /// <summary>
    /// Initialize.
    /// </summary>
    /// <param name="e"></param>
    protected override void OnInit(EventArgs e)
    {
        // grid view events
        //
        if (GridView != null)
        {
            GridView.Action += new UIGridView.RowActionEventHandler(SearchResultView_Action);
            GridView.RowDataBound += new GridViewRowEventHandler(GridView_RowDataBound);
            GridView.KeepLinksToObjectListInSession = false;
            GridView.AcquireObject += new UIGridView.AcquireObjectHandler(GridView_AcquireObject);

            GridView.ListBoxColumnChooser.Title = Resources.Strings.SearchPanel_SelectColumns;
            GridView.FixedHeader = true;
            GridView.FixedHeaderOffset = 70;
            GridView.AllowColumnReorder = true;
            GridView.Export += new UIGridView.GridViewExportEventHandler(GridView_Export);

            if (Request["TYPE"] != null && Request["TYPE"].Trim() != "")
            {
                viewAccess = AppSession.User.AllowViewAll(GetRequestObjectType());
                editAccess = AppSession.User.AllowEditAll(GetRequestObjectType());
            }

            // Add a "Add Selected" button at the top of the gridview.
            //
            /*
            buttonAddSelected.ID = "buttonAddSelected2";
            buttonAddSelected.Text = "Add Selected";
            buttonAddSelected.ImageUrl = "~/images/add.gif";
            buttonAddSelected.Click += new EventHandler(buttonAddSelected_Click);
            GridView.Parent.Controls.AddAt(0, buttonAddSelected);

            literalBreak.ID = "literalBreak";
            literalBreak.Text = "<br/>";
            GridView.Parent.Controls.AddAt(1, literalBreak);
                * */
            commandAddSelected.CommandText = Resources.Strings.Results_AddSelected;
            commandAddSelected.CommandName = "AddSelected";
            commandAddSelected.ImageUrl = "~/images/add.gif";
            commandAddSelected.CausesValidation = true;
            if (!commandVisibleSet)
                commandAddSelected.Visible = false;
            GridView.Commands.Add(commandAddSelected);
        }

        UIButton1.Click += new EventHandler(UIButton1_Click);
        UIButton2.Click += new EventHandler(UIButton2_Click);
        UIButton3.Click += new EventHandler(UIButton3_Click);
        UIButton4.Click += new EventHandler(UIButton4_Click);
        UIButton5.Click += new EventHandler(UIButton5_Click);
        UIButton6.Click += new EventHandler(UIButton6_Click);
        UIButton7.Click += new EventHandler(UIButton7_Click);
        UIButton8.Click += new EventHandler(UIButton8_Click);

        searchLimitControls.Visible = CustomPageSize;


        base.OnInit(e);
    }



    private void GridView_Export(object sender, GridViewExportEventArgs args)
    {
        if (args.ExportedData != null)
            Window.Download(
                args.ExportedData.ToArray(),
                this.Caption + Path.GetExtension(args.FileName),
                MimeTypes.GetContentType(Path.GetExtension(args.FileName)));
    }


    /// <summary>
    /// Temporary hashtable used for quick access to the list of objects
    /// loaded from the database.
    /// 
    /// This variable will be populated only when the search button is
    /// clicked for the first time.
    /// </summary>
    private Hashtable hashObjects = null;


    /// <summary>
    /// Handle the method to load the object from the database
    /// and return it to the GridView for populating content.
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    object GridView_AcquireObject(ArrayList list, object key)
    {
        if (SearchType == PanelSearchType.ObjectQuery)
        {
            if (hashObjects != null)
                return hashObjects[key];
            else
            {
                if (table == null)
                    GetBaseTable();

                if (list != null)
                {
                    using (Connection c = new Connection(Anacle.DataFramework.IsolationLevel.ReadUncommitted))
                    {
                        hashObjects = new Hashtable();
                        IEnumerable ie = table.LoadObjects(table.ObjectID.In(list));
                        foreach (PersistentObject po in ie)
                            hashObjects[po.ObjectID.Value] = po;
                    }
                    return hashObjects[(Guid)key];
                }

                return table.LoadObject((Guid)key);
            }
        }

        return null;
    }


    Hashtable listOfAssignedObjects = null;
    // 2016.09.20
    // Kien Trung
    // NEW: To allow submittor / creator to view access to their own document
    //
    Hashtable listOfViewableObjects = null;


    /// <summary>
    /// This checks if the object at the current row is assigned to the
    /// current user, and shows or hides the "Edit" icon accordingly.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (GridView.DataKeys[e.Row.RowIndex][0] == DBNull.Value)
            {
                /*if (editColumnIndex != -1)
                    if (e.Row.Cells[editColumnIndex].Controls.Count > 0)
                        e.Row.Cells[editColumnIndex].Controls[0].Visible = false;
                if (viewColumnIndex != -1)
                    if (e.Row.Cells[viewColumnIndex].Controls.Count > 0)
                        e.Row.Cells[viewColumnIndex].Controls[0].Visible = false;*/
                /*if (deleteColumnIndex != -1)
                    if (e.Row.Cells[deleteColumnIndex].Controls.Count > 0)
                        e.Row.Cells[deleteColumnIndex].Controls[0].Visible = false;
                        */
                return;
            }

            // Do not bother to hide/show the edit button if
            // the user has already selected to show only tasks assigned to her/himself.
            //
            //if (!divAssignedOnly.Visible || !AssignedOnly.Checked)
            {
                bool editEnabled = true;
                bool viewEnabled = true;

                Guid objectId = (Guid)GridView.DataKeys[e.Row.RowIndex][0];

                if (listOfAssignedObjects == null)
                {
                    List<Guid> listOfIds = new List<Guid>();
                    foreach (Guid id in GridView.ListOfDataKeys)
                        listOfIds.Add(id);
                    listOfAssignedObjects = OActivity.GetAssignmentHash(AppSession.User, listOfIds);
                }

                if (/*editColumnIndex != -1 &&*/ !editAccess && listOfAssignedObjects[objectId] == null)
                {
                    //if (e.Row.Cells[editColumnIndex].Controls.Count > 0)
                    //    e.Row.Cells[editColumnIndex].Controls[0].Visible = false;
                    editEnabled = false;
                }


                if (/*viewColumnIndex != -1*/true)
                {
                    if (listOfViewableObjects == null)
                    {
                        GetBaseTable();
                        listOfViewableObjects = new Hashtable();


                        {
                            List<Guid> listOfIds = new List<Guid>();
                            foreach (Guid id in GridView.ListOfDataKeys)
                                listOfIds.Add(id);
                            IEnumerable ie = table.LoadObjects(table.ObjectID.In(listOfIds));
                            if (ie != null)
                            {
                                foreach (PersistentObject p in ie)
                                {
                                    if (table.GetPersistentObjectType().IsSubclassOf(typeof(LogicLayerWorkflowPersistentObject)))
                                    {
                                        if (((LogicLayerWorkflowPersistentObject)p).CreatedID == AppSession.User.ObjectID)
                                            listOfViewableObjects[p.ObjectID.Value] = 1;
                                    }

                                    if (listOfViewableObjects[p.ObjectID.Value] == null &&
                                        table.GetPersistentObjectType().IsSubclassOf(typeof(LogicLayerApprovalPersistentObject)))
                                    {
                                        if (((LogicLayerApprovalPersistentObject)p).SubmittorID == AppSession.User.ObjectID)
                                            listOfViewableObjects[p.ObjectID.Value] = 1;
                                    }
                                }
                            }
                        }
                    }

                    // 2016.09.20
                    // Kien Trung
                    // NEW: To allow submittor / creator / Assigned user to view the task
                    //
                    if (!viewAccess && listOfViewableObjects[objectId] == null && listOfAssignedObjects[objectId] == null)
                    {
                        //if (e.Row.Cells[viewColumnIndex].Controls.Count > 0)
                        //    e.Row.Cells[viewColumnIndex].Controls[0].Visible = false;
                        viewEnabled = false;
                    }
                }

                if (!editButtonVisible)
                    editEnabled = false;

                if (!viewButtonVisible)
                    viewEnabled = false;

                if (editEnabled)
                {
                    e.Row.CssClass = "grid-row grid-row-editable";
                    if (clickColumnIndex != -1)
                        e.Row.Cells[clickColumnIndex].Text = "<a class='grid-row-editable-link' href=\"javascript:" + Page.ClientScript.GetPostBackEventReference(this.GridView.Grid, "EditObject$" + e.Row.RowIndex) + "\">" + e.Row.Cells[clickColumnIndex].Text + "</a>";
                    //e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackEventReference(this.GridView.Grid, "EditObject$" + e.Row.RowIndex);
                }
                else if (viewEnabled)
                {
                    e.Row.CssClass = "grid-row grid-row-viewable";
                    if (clickColumnIndex != -1)
                        e.Row.Cells[clickColumnIndex].Text = "<a class='grid-row-viewable-link' href=\"javascript:" + Page.ClientScript.GetPostBackEventReference(this.GridView.Grid, "ViewObject$" + e.Row.RowIndex) + "\">" + e.Row.Cells[clickColumnIndex].Text + "</a>";
                    //e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackEventReference(this.GridView.Grid, "ViewObject" + e.Row.RowIndex);
                }
                else
                {
                    e.Row.CssClass = "grid-row grid-row-disabled";
                }
            }
        }
    }


    /// <summary>
    /// Performs the appropriate action on the object, or objects selected
    /// via the checkbox.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="commandName"></param>
    /// <param name="objectIds"></param>
    void SearchResultView_Action(object sender, string commandName, List<object> objectIds)
    {
        GetBaseTable();

        if (commandName == "DeleteObject")
        {
            bool deleted = false;

            try
            {
                Hashtable undeletedItems = new Hashtable();
                int numberOfItemsDeleted = 0;
                using (Connection c = new Connection())
                {
                    foreach (Guid objectId in objectIds)
                    {
                        PersistentObject o = table.LoadObject(objectId);
                        if (o != null)
                        {
                            bool wasDeleted = o.Deactivate();
                            if (wasDeleted)
                            {
                                deleted = true;
                                numberOfItemsDeleted++;
                            }
                            else
                                undeletedItems[objectId] = true;
                        }
                    }
                    c.Commit();
                }

                // redo search immediately after delete;
                //
                if (deleted)
                {
                    buttonSearch_Click(this, null);

                    if (numberOfItemsDeleted != objectIds.Count)
                    {
                        // Re-check those items in the checkbox
                        // that were not successfully deleted.
                        foreach (GridViewRow row in this.GridView.Rows)
                        {
                            object key = this.GridView.DataKeys[row.RowIndex][0];
                            if (undeletedItems[key] != null)
                                ((CheckBox)row.Cells[0].Controls[0]).Checked = true;
                            else
                                ((CheckBox)row.Cells[0].Controls[0]).Checked = false;
                        }
                    }
                }

                // show message to user indicating status.
                //
                string message = "";
                if (numberOfItemsDeleted > 0)
                    message += String.Format(Resources.Messages.General_ItemsDeleted, numberOfItemsDeleted) + " ";
                if (numberOfItemsDeleted != objectIds.Count)
                    message += String.Format(Resources.Messages.General_ItemsCannotBeDeleted, (objectIds.Count - numberOfItemsDeleted));
                this.Message = message;
            }
            catch (Exception ex)
            {
                Helpers.LogException(ex, this.Request);
                if (Helpers.IsDebug())
                    this.Message = ex.Message;
                else
                    this.Message = String.Format(Resources.Errors.General_ErrorOccurred, DateTime.Now);

            }
        }
        else if (commandName == "EditObject")
        {
            if (objectIds.Count > 0)
            {
                // pop-up the edit page for this object type
                //
                Window.OpenEditObjectPage(this.Page, GetRequestObjectType(), objectIds[0].ToString(), "SEARCH=1");
            }
        }
        else if (commandName == "ViewObject")
        {
            if (objectIds.Count > 0)
            {
                // pop-up the edit page for this object type
                //
                Window.OpenViewObjectPage(this.Page, GetRequestObjectType(), objectIds[0].ToString(), "SEARCH=1");
            }
        }
        else if (commandName == "AddSelected")
        {
            // Gets the parent UIObjectPanel that contains the UIGridView.
            // Then checks for any error messages in there.
            //
            Control control = GridView;
            while (control != null && !(control is UIObjectPanel))
                control = control.Parent;
            UIObjectPanel containingObjectPanel = control as UIObjectPanel;
            if (containingObjectPanel == null)
                throw new Exception("Unable to find a UIObjectPanel that contains the search results GridView");

            // Ensures that the everything in the object
            // panel is valid to begin within.
            //
            this.Message = "";
            if (!containingObjectPanel.IsValid)
            {
                this.Message = containingObjectPanel.CheckErrorMessages();
                return;
            }

            // Ensure that the current object is stored in session
            //
            if (Session["::SessionObject::"] == null)
            {
                Window.Close();
                return;
            }

            // Ensures that at least one item in the row is selected.
            //
            int count = 0;
            List<GridViewRow> selectedRows = new List<GridViewRow>();
            foreach (GridViewRow row in GridView.Rows)
            {
                if (row.Cells[0].Controls[0] is CheckBox &&
                    ((CheckBox)row.Cells[0].Controls[0]).Checked)
                {
                    selectedRows.Add(row);
                    count++;
                }
            }
            if (count == 0)
            {
                this.Message = Resources.Errors.General_SelectOneOrMoreItemsToAdd;
                return;
            }

            // Call the ValidateAndAddSelected event. It's up to the developer to 
            // decide what to do inside, and how to refresh the parent window,
            // and whether he/she wants the window to close.
            //
            if (ValidateAndAddSelected != null)
                ValidateAndAddSelected(this, EventArgs.Empty);

            // Show any error messages
            //
            if (!containingObjectPanel.IsValid)
            {
                this.Message = containingObjectPanel.CheckErrorMessages();
                return;
            }

            // Clear up all selected checkboxes
            //
            foreach (GridViewRow row in GridView.Rows)
                if (row.Cells[0].Controls[0] is CheckBox)
                    ((CheckBox)row.Cells[0].Controls[0]).Checked = false;
        }

    }


    /// <summary>
    /// Populates the quick search dropdown list.
    /// </summary>
    /*
    protected void PopulateQuickSearch()
    {
        if (Session["SearchableFunctions"] == null)
        {
            DataTable accessibleFunctions = Helpers.GetCachedMenusAccessibleByUser(AppSession.User);

            List<ListItem> listItems = new List<ListItem>();
            foreach (DataRow dr in accessibleFunctions.Rows)
            {
                string functionName = dr["FunctionName"].ToString();

                string translatedText = Resources.Objects.ResourceManager.GetString(functionName);
                if (translatedText != null && translatedText != "")
                    functionName = translatedText;

                listItems.Add(new ListItem(functionName, dr["MainUrl"].ToString() + "\x255" + dr["ObjectTypeName"]));
            }

            listItems.Sort((a, b) => String.Compare(a.Text, b.Text));
            Session["SearchableFunctions"] = listItems;
        }

        SimpleSearchDropDownList.Items.Add(new ListItem(Resources.Strings.Home_Inbox, ""));
        foreach (ListItem item in (List<ListItem>)Session["SearchableFunctions"])
            SimpleSearchDropDownList.Items.Add(new ListItem(item.Text, item.Value));

        // Automatically select the current object
        //
        if (Request["TYPE"] != null)
        {
            string objectTypeName = GetRequestObjectType();
            foreach (ListItem listItem in SimpleSearchDropDownList.Items)
            {
                string[] p = listItem.Value.Split('\x255');
                if (p.Length > 1 && p[1] == objectTypeName)
                {
                    listItem.Selected = true;
                    break;
                }
            }
        }
    }
    */

    protected bool controlHasValue(object value)
    {
        if (value != null && !value.ToString().IsNullOrEmpty())
            return true;
        return false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="control"></param>
    /// <param name="dt"></param>
    protected void BuildSearchedDataTable(Control control, DataTable dt)
    {
        if (control == null)
            return;

        foreach (Control c in control.Controls)
        {
            if (c is UIFieldBase)
            {
                UIFieldBase fieldBase = c as UIFieldBase;

                if (
                    (fieldBase.SearchType == UISearchType.Range && (controlHasValue(fieldBase.ControlValue) || controlHasValue(fieldBase.ControlValueTo))) ||
                    (fieldBase.SearchType == UISearchType.Like && (controlHasValue(fieldBase.ControlValue))) ||
                    (fieldBase.SearchType == UISearchType.Equals && (controlHasValue(fieldBase.ControlValue))) ||
                    (fieldBase is UIFieldCheckBox && ((UIFieldCheckBox)fieldBase).Checked))
                {
                    //x.Append(x.Length == 0 ? "" : "&nbsp;");

                    string stringFormat = "<b>{0}</b>: {1}";

                    if (fieldBase is UIFieldCheckBox && ((UIFieldCheckBox)fieldBase).Checked)
                        dt.Rows.Add(String.Format("has:<b>{0}</b>", fieldBase.Caption), fieldBase.ClientID);
                    else if (fieldBase is UIFieldDropDownList && ((UIFieldDropDownList)fieldBase).SelectedValue != "")
                        dt.Rows.Add(String.Format(stringFormat, fieldBase.Caption, ((UIFieldDropDownList)fieldBase).ControlText), fieldBase.ClientID);
                    else if (fieldBase is UIFieldSearchableDropDownList && ((UIFieldSearchableDropDownList)fieldBase).SelectedValue != "")
                        dt.Rows.Add(String.Format(stringFormat, fieldBase.Caption, ((UIFieldSearchableDropDownList)fieldBase).ControlText), fieldBase.ClientID);
                    else if (fieldBase is UIFieldRadioList && ((UIFieldRadioList)fieldBase).SelectedValue != "")
                        dt.Rows.Add(String.Format(stringFormat, fieldBase.Caption, ((UIFieldRadioList)fieldBase).ControlText), fieldBase.ClientID);
                    else if (fieldBase is UIFieldTreeList && ((UIFieldTreeList)fieldBase).SelectedValue != "")
                        dt.Rows.Add(String.Format(stringFormat, fieldBase.Caption, ((UIFieldTreeList)fieldBase).SelectedNode.Text), fieldBase.ClientID);
                    else if (fieldBase is UIFieldTextBox && ((UIFieldTextBox)fieldBase).Text != "")
                        dt.Rows.Add(String.Format(stringFormat, fieldBase.Caption, ((UIFieldTextBox)fieldBase).Text), fieldBase.ClientID);
                    else if (fieldBase is UIFieldDateTime)
                    {
                        if (((UIFieldDateTime)fieldBase).SearchType == UISearchType.Range)
                        {
                            string value = (
                                ((UIFieldDateTime)fieldBase).ControlValue != null ? "after " +
                                ((DateTime)((UIFieldDateTime)fieldBase).ControlValue).ToString(((UIFieldDateTime)fieldBase).DateFormatString) +
                                ((((DateTime)((UIFieldDateTime)fieldBase).ControlValue).ToString("HHmmss") == "000000" ? "" : "&nbsp;" + ((DateTime)((UIFieldDateTime)fieldBase).ControlValue).ToString(((UIFieldDateTime)fieldBase).TimeFormatString))) : "")
                            + (
                                ((UIFieldDateTime)fieldBase).ControlValueTo != null ? (((UIFieldDateTime)fieldBase).ControlValue != null ? "&nbsp;" : "") + "before " +
                                ((DateTime)((UIFieldDateTime)fieldBase).ControlValueTo).ToString(((UIFieldDateTime)fieldBase).DateFormatString) +
                                ((((DateTime)((UIFieldDateTime)fieldBase).ControlValueTo).ToString("HHmmss") == "000000" ? "" : "&nbsp;" + ((DateTime)((UIFieldDateTime)fieldBase).ControlValueTo).ToString(((UIFieldDateTime)fieldBase).TimeFormatString))) : "");

                            dt.Rows.Add(String.Format(stringFormat, fieldBase.Caption, value), fieldBase.ClientID);
                        }
                        else
                            dt.Rows.Add(String.Format(stringFormat, fieldBase.Caption, ((DateTime)((UIFieldDateTime)fieldBase).ControlValue).ToString(((UIFieldDateTime)fieldBase).DateFormatString)), fieldBase.ClientID);
                    }
                    else if (fieldBase is UIFieldCheckboxList &&
                        ((UIFieldCheckboxList)fieldBase).SelectedValue != null &&
                        ((UIFieldCheckboxList)fieldBase).SelectedValue != "")
                    {
                        dt.Rows.Add(String.Format(stringFormat, fieldBase.Caption, ((UIFieldCheckboxList)fieldBase).ControlText), fieldBase.ClientID);
                    }
                    else if (fieldBase is UIFieldListBox &&
                        ((UIFieldListBox)fieldBase).SelectedValue != null &&
                        ((UIFieldListBox)fieldBase).SelectedValue != "")
                    {
                        dt.Rows.Add(String.Format(stringFormat, fieldBase.Caption, fieldBase.ControlText), fieldBase.ClientID);
                    }
                }
            }

            if (c.Controls.Count > 0)
                BuildSearchedDataTable(c, dt);
        }
    }

    protected void BuildQuerySearchedString(Control control, ref StringBuilder x)
    {
        if (control == null)
            return;
        foreach (Control c in control.Controls)
        {
            if (c is UIFieldBase)
            {
                UIFieldBase fieldBase = c as UIFieldBase;

                if (((UIFieldBase)c).ControlValue != null &&
                    !((UIFieldBase)c).ControlValue.ToString().IsNullOrEmpty() ||
                    (fieldBase is UIFieldCheckBox && ((UIFieldCheckBox)fieldBase).Checked))
                {
                    x.Append(x.Length == 0 ? "" : "&nbsp;");
                    string stringFormat = "<span class='chip'>{1}</span>";

                    if (fieldBase is UIFieldCheckBox && ((UIFieldCheckBox)fieldBase).Checked)
                        x.AppendFormat("has:<b>{0}</b>", fieldBase.Caption);
                    else if (fieldBase is UIFieldDropDownList && ((UIFieldDropDownList)fieldBase).SelectedValue != "")
                        x.AppendFormat(stringFormat, fieldBase.Caption, ((UIFieldDropDownList)fieldBase).ControlText);
                    else if (fieldBase is UIFieldSearchableDropDownList && ((UIFieldSearchableDropDownList)fieldBase).SelectedValue != "")
                        x.AppendFormat(stringFormat, fieldBase.Caption, ((UIFieldSearchableDropDownList)fieldBase).ControlText);
                    else if (fieldBase is UIFieldRadioList && ((UIFieldRadioList)fieldBase).SelectedValue != "")
                        x.AppendFormat(stringFormat, fieldBase.Caption, ((UIFieldRadioList)fieldBase).ControlText);
                    else if (fieldBase is UIFieldTreeList && ((UIFieldTreeList)fieldBase).SelectedValue != "")
                        x.AppendFormat(stringFormat, fieldBase.Caption, ((UIFieldTreeList)fieldBase).SelectedNode.Text);
                    else if (fieldBase is UIFieldTextBox && ((UIFieldTextBox)fieldBase).Text != "")
                        x.AppendFormat(stringFormat, fieldBase.Caption, ((UIFieldTextBox)fieldBase).Text);
                    else if (fieldBase is UIFieldDateTime &&
                        (((UIFieldDateTime)fieldBase).ControlValue != null ||
                        (((UIFieldDateTime)fieldBase).SearchType == UISearchType.Range &&
                        (((UIFieldDateTime)fieldBase).ControlValueTo != null || ((UIFieldDateTime)fieldBase).ControlValue != null))))
                    {
                        if (((UIFieldDateTime)fieldBase).SearchType == UISearchType.Range)
                        {
                            x.AppendFormat(stringFormat, fieldBase.Caption,
                                (((UIFieldDateTime)fieldBase).ControlValue != null ? "after " + ((DateTime)((UIFieldDateTime)fieldBase).ControlValue).ToString(((UIFieldDateTime)fieldBase).DateFormatString) : "") +
                                (((UIFieldDateTime)fieldBase).ControlValueTo != null ? (((UIFieldDateTime)fieldBase).ControlValue != null ? " " : "") + "before " + ((DateTime)((UIFieldDateTime)fieldBase).ControlValueTo).ToString(((UIFieldDateTime)fieldBase).DateFormatString) : ""));
                        }
                        else
                            x.AppendFormat(stringFormat, fieldBase.Caption, ((DateTime)((UIFieldDateTime)fieldBase).ControlValue).ToString(((UIFieldDateTime)fieldBase).DateFormatString));
                    }

                    else if (fieldBase is UIFieldListBox &&
                        ((UIFieldListBox)fieldBase).SelectedValue != null &&
                        ((UIFieldListBox)fieldBase).SelectedValue != "")
                    {
                        x.AppendFormat(stringFormat, fieldBase.Caption, fieldBase.ControlText);
                    }
                }
            }

            if (c.Controls.Count > 0)
                BuildQuerySearchedString(c, ref x);
        }
    }

    /// <summary>
    /// Analyzes and sets the number of fixed columns.
    /// </summary>
    protected void SetGridViewFixedColumns()
    {
        int fixedLeftColumnCount = 0;

        if (GridView.CheckBoxColumnVisible)
            fixedLeftColumnCount++;

        for (int i = 0; i < GridView.Columns.Count; i++)
        {
            if (GridView.Columns[i].Visible)
                fixedLeftColumnCount++;

            if (GridView.Columns[i].Visible && (GridView.Columns[i] is BoundField || GridView.Columns[i] is TemplateField))
            {
                // The first column in the GridView should have its DisallowFromColumnChoosing set to true.
                //
                bool allowChoosing = true;
                if (GridView.Columns[i] is UIGridViewBoundColumn && (GridView.Columns[i] as UIGridViewBoundColumn).DisallowFromColumnChoosing)
                    allowChoosing = false;
                if (GridView.Columns[i] is UIGridViewTemplateColumn && (GridView.Columns[i] as UIGridViewTemplateColumn).DisallowFromColumnChoosing)
                    allowChoosing = false;

                if (allowChoosing)
                {
                    if (GridView.Columns[i] is UIGridViewBoundColumn)
                        (GridView.Columns[i] as UIGridViewBoundColumn).DisallowFromColumnChoosing = true;
                    else if (GridView.Columns[i] is UIGridViewTemplateColumn)
                        (GridView.Columns[i] as UIGridViewTemplateColumn).DisallowFromColumnChoosing = true;
                }

                break;
            }
        }
        GridView.ColumnReorderFixedLeftColumns = fixedLeftColumnCount;
    }

    /// <summary>
    /// Load.
    /// </summary>
    /// <param name="e"></param>
    protected override void OnLoad(EventArgs e)
    {
        this.Message = "";

        base.OnLoad(e);

        if (!IsPostBack)
        {
            if (ConfigurationManager.AppSettings["LoadTesting"] == "false")
            {
                if (Request["TYPE"] != null)
                {
                    if (!AppSession.User.AllowSearch(GetRequestObjectType()))
                        Response.Redirect(Page.ResolveUrl("~/appobjectnoaccess.aspx"), true);

                    // Check for the salt. 
                    //
                    string[] args = Security.Decrypt(Request["TYPE"]).Split(':');
                    if (args.Length > 0 && args[args.Length - 1].StartsWith("SLT-"))
                    {
                        if (args[args.Length - 1] != AppSession.SaltID)
                            Response.Redirect(Page.ResolveUrl("~/appobjectnoaccess.aspx"), true);
                    }
                }
                else
                    Response.Redirect(Page.ResolveUrl("~/appobjectnoaccess.aspx"), true);
            }

            //PopulateQuickSearch();

            if (PopulateForm != null)
                PopulateForm(this, new EventArgs());

            //hide the advance search, if simple search is enabled

            panelSimpleSearch.Visible = false;
            if (AdvancedSearchPanel != null)
            {

            }

            /*if (panelSimpleSearch.Visible)
            {
                if (advSearchID != "")
                {
                    UIPanel advancedSearchPanel = AdvancedSearchPanel;
                    if (advancedSearchPanel != null)
                        advancedSearchPanel.Visible = false;
                }
                else
                {
                    //hide the show advance search button if no advancesearchpanelid is provided
                    //buttonAdvanceSearch.Visible = false;
                }
            }*/

            // If a search parameter has been passed in, use it
            // to populate the simple search textbox.
            //
            if (Request["S"] != null)
                this.SimpleSearch.Text = HttpUtility.HtmlEncode(Request["S"]);

            // check access control, and hide the relevant view/edit/create buttons
            //
            if (Request["TYPE"] != null && Request["TYPE"].Trim() != "")
            {
                buttonAdd.Visible = AppSession.User.AllowCreate(GetRequestObjectType());
                //buttonEdit.Visible = AppSession.User.AllowEditAll(GetRequestObjectType());
            }

            // 2011.09.13
            // Kim Foong
            // Focus the cursor on the search textbox
            //
            if (this.SearchTextBoxVisible)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "FocusSearch",
                    "window.setTimeout(function () { document.getElementById('" + this.SearchTextBox.Control.ClientID + "').focus(); }, 100); ", true);
            }

            // Load the preferences and apply to the search
            //
            if (pref != null)
            {
                NoOfRecordsPerRows.SelectedValue = pref.PagingCount.ToString();
                MaxNoOfRecords.SelectedValue = pref.MaxResultCount.ToString();
            }
            else
            {
                NoOfRecordsPerRows.SelectedValue = ResultsPerPage.ToString().Substring(1);
                MaxNoOfRecords.SelectedValue = MaximumNumberOfResults.ToString().Substring(1);
            }
            MaximumNumberOfResults = (MaxNumberOfResults)Enum.Parse(typeof(MaxNumberOfResults), "_" + MaxNoOfRecords.SelectedValue);
            ResultsPerPage = (NumberOfResults)Enum.Parse(typeof(NumberOfResults), "_" + NoOfRecordsPerRows.SelectedValue);

            // Saves the URL of the current search page into Session to support global search.
            // 
            this.Session["CurrentSearchPath"] = this.Page.Request.Path + "?TYPE=" + HttpUtility.UrlEncode(this.Page.Request["TYPE"]);

            buttonColumnChooser.OnClickScript = "document.getElementById('" + this.GridView.ListBoxColumnChooser.ClientID + "_tb').click()";

        }
        this.GridView.ColumnsChanged += GridView_ColumnsChanged;

        if (!IsPostBack)
        {
            pref = OUserSearchGridViewPreference.GetPreference(AppSession.User.ObjectID, GetRequestObjectType());
            if (pref != null)
            {
                GridView.SetColumnPreferenceJson(pref.Columns);
            }


        }

        SetGridViewFixedColumns();


        int i = 0;
        if (Request["TYPE"] != null && Request["TYPE"].Trim() != "")
        {
            foreach (DataControlField column in GridView.Grid.Columns)
            {
                if (column is ButtonField)
                {
                    ButtonField buttonColumn = column as ButtonField;
                    if (buttonColumn.CommandName == "EditObject")
                    {
                        //buttonColumn.Visible = editAccess;
                        editColumnIndex = i;
                        buttonColumn.ImageUrl = "~/images/edit-black-16.png";
                    }
                    if (buttonColumn.CommandName == "ViewObject")
                    {
                        // 2016.09.20
                        // Kien Trung
                        // NEW: To allow default creator / submittor to view task
                        // Even it's not assigned to the user (i.e. Pending Approval);
                        //
                        //buttonColumn.Visible = AppSession.User.AllowViewAll(GetRequestObjectType());
                        viewColumnIndex = i;
                        buttonColumn.ImageUrl = "~/images/view-black-16.png";
                    }
                    if (buttonColumn.CommandName == "DeleteObject")
                    {
                        buttonColumn.Visible = AppSession.User.AllowDeleteAll(GetRequestObjectType());
                        deleteColumnIndex = i;
                        buttonColumn.ImageUrl = "~/images/delete-red-16.png";
                    }
                }
                else if (column is UIGridViewBoundColumn)
                {
                    if ((column as UIGridViewBoundColumn).DisallowFromColumnChoosing)
                        clickColumnIndex = i;
                }
                else if (column is UIGridViewTemplateColumn)
                {
                    if ((column as UIGridViewTemplateColumn).DisallowFromColumnChoosing)
                        clickColumnIndex = i;
                }
                i++;
            }

            foreach (UIGridViewCommand command in GridView.Commands)
            {
                if (command.CommandName == "DeleteObject")
                {
                    command.Visible = AppSession.User.AllowDeleteAll(GetRequestObjectType());
                    command.ImageUrl = "~/images/delete-red-16.png";
                }
            }

        }

        
        if(!IsPostBack)
            // inital search
            if (AutoSearchOnLoad)
                PerformSearch();

        var dialogBox = this.DialogBox;

        if (dialogBox != null)
        {
            dialogBox.Title = Resources.Strings.DialogBox_AdvancedSearch;
            dialogBox.CloseButtonVisible = false;

            dialogBox.Button1.Text = Resources.Strings.DialogBox_Search;
            dialogBox.Button1.IconCssClass = "material-icons";
            dialogBox.Button1.IconText = "search";
            dialogBox.Button1.ButtonColor = UIButtonColor.Blue;
            dialogBox.Button1.ButtonType = UIButtonType.NormalRaised;
            dialogBox.Button1.CommandName = "Search";

            dialogBox.Button2.Text = Resources.Strings.DialogBox_ClearFilters;
            dialogBox.Button2.IconCssClass = "material-icons";
            dialogBox.Button2.IconText = "close";
            dialogBox.Button2.ButtonColor = UIButtonColor.Default;
            dialogBox.Button2.ButtonType = UIButtonType.NormalFlat;
            dialogBox.Button2.CommandName = "Clear";

            dialogBox.ButtonClicked += DialogBox_ButtonClicked;
        }
        else
        {
            if (this.AdvancedSearchPanel != null)
                throw new Exception("The Panel '" + this.AdvancedSearchPanelID + "' containing controls for advance search filters must be contained in a UIDialogBox");
        }


        // Mobile Web
        //
        if (this.Page is UIPageBase && ((UIPageBase)this.Page).MobileMode)
        {
            tableSimpleSearch.Rows[2].Cells[0].Style["width"] = "250px";
            SimpleSearch.InternalControlWidth = Unit.Pixel(250);
        }
    }


    /// <summary>
    /// Updates and saves the grid view column order/visibility preference.
    /// </summary>
    protected void UpdateSearchPreference()
    {
        if (this.GridView != null)
        {
            string json = this.GridView.GetColumnPreferenceJson();
            var pref = OUserSearchGridViewPreference.GetPreference(AppSession.User.ObjectID, GetRequestObjectType());
            if (pref == null)
            {
                pref = TablesLogic.tUserSearchGridViewPreference.Create();
                pref.UserID = AppSession.User.ObjectID;
                pref.ObjectTypeName = GetRequestObjectType();
            }
            pref.Columns = json;
            pref.MaxResultCount = (int)MaximumNumberOfResults;
            pref.PagingCount = (int)ResultsPerPage;
            using (Connection c = new Connection())
            {
                pref.Save();
                c.Commit();
            }
        }
    }


    protected void GridView_ColumnsChanged(object sender, EventArgs e)
    {
        UpdateSearchPreference();
    }


    protected void DialogBox_ButtonClicked(object sender, ButtonClickedEventArgs e)
    {
        if (e.CommandName == "Search")
        {
            PerformSearch();
            DialogBox.Hide();
        }
        else if (e.CommandName == "Clear")
        {
            ResetFields();
            DialogBox.Hide();
        }
    }


    /// <summary>
    /// Recursively gets the first tree list. 
    /// </summary>
    /// <param name="c"></param>
    /// <returns></returns>
    protected UIFieldTreeList GetUIFieldTreeList(Control c)
    {
        if (c is UIFieldTreeList)
            return (UIFieldTreeList)c;

        foreach (Control child in c.Controls)
        {
            UIFieldTreeList t = GetUIFieldTreeList(child);
            if (t != null)
                return t;
        }
        return null;
    }


    protected void PadCells(GridViewRow row, bool pagerRow)
    {
        /*
        int startColumn = 0;
        if (!this.GridView.CheckBoxColumnVisible && !pagerRow)
            startColumn = 1;
        for (int i = startColumn; i < row.Cells.Count; i++)
        {
            if (row.Cells[i].Visible || pagerRow)
            {
                // Mobile web
                //
                if (this.Page is UIPageBase && ((UIPageBase)this.Page).MobileMode)
                    row.Cells[i].Style["padding-left"] = "20px";
                else
                    row.Cells[i].Style["padding-left"] = "40px";
                break;
            }
        }

        for (int i = row.Cells.Count - 1; i >= 0; i--)
        {
            if (row.Cells[i].Visible || pagerRow)
            {
                // Mobile web
                //
                if (this.Page is UIPageBase && ((UIPageBase)this.Page).MobileMode)
                    row.Cells[i].Style["padding-right"] = "20px";
                else
                    row.Cells[i].Style["padding-right"] = "40px";
                break;
            }
        }*/
    }


    /// <summary>
    /// Pre-render.
    /// </summary>
    /// <param name="e"></param>
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        this.Page.Form.Attributes["class"] = "searchpage";

        Window.WriteJavascript(
            "function Refresh() { document.getElementById('" + hiddenRefresh.ClientID + "').value='1'; " +
            Page.ClientScript.GetPostBackEventReference(buttonSearchNoFocus.LinkButton, "refresh") + " }");

        // Ensure that the Edit button appears only when the first UIField control
        // is selected.
        //
        /*if (EditButtonVisible)
        {
            UIFieldTreeList treeList = GetUIFieldTreeList(this.Page);
            if (treeList != null)
                spanEditAfterTreeSelected.Visible = treeList.SelectedValue != "";
            else
                spanEditAfterTreeSelected.Visible = false;
        }*/

        // Hides/shows the custom buttons.
        //
        UIButton1.Visible = Button1_CommandName.Trim() != "";
        UIButton2.Visible = Button2_CommandName.Trim() != "";
        UIButton3.Visible = Button3_CommandName.Trim() != "";
        UIButton4.Visible = Button4_CommandName.Trim() != "";
        UIButton5.Visible = Button5_CommandName.Trim() != "";
        UIButton6.Visible = Button6_CommandName.Trim() != "";
        UIButton7.Visible = Button7_CommandName.Trim() != "";
        UIButton8.Visible = Button8_CommandName.Trim() != "";

        // Force some default settings for the gridview.
        //
        if (this.GridView != null)
        {
            this.GridView.ShowCaption = false;
            if (this.GridView.TopPagerRow != null)
                PadCells(this.GridView.TopPagerRow, true);
            if (this.GridView.HeaderRow != null)
                PadCells(this.GridView.HeaderRow, false);
            foreach (GridViewRow row in this.GridView.Rows)
                PadCells(row, false);
            if (this.GridView.FooterRow != null)
                PadCells(this.GridView.FooterRow, false);
            if (this.GridView.BottomPagerRow != null)
                PadCells(this.GridView.BottomPagerRow, true);
        }

        // Force some default settings for the advanced search panel
        //
        //buttonAdvanceSearch.Visible = (this.AdvancedSearchPanel != null && !this.AdvancedSearchPanel.Visible);
        if (this.AdvancedSearchPanel != null)
        {
            this.AdvancedSearchPanel.CssClass = "mdl-search-adv-div";
        }

        if (messageChanged)
            Window.WriteStartupJavascript(@"showToast('mdl_toast', '" + labelMessage.Text.Replace("'", "\\'").Replace("\"", "\\\"") + @"', 'OK')");

        Window.WriteStartupJavascript("toggleActionSheet();");
    }




    /// <summary>
    /// Clear data entered into all controls.
    /// </summary>
    /// <param name="control"></param>
    void ClearControls(Control control)
    {
        if (control.GetType().IsSubclassOf(typeof(UIFieldBase)))
        {
            if (control.ID != "SimpleSearchDropDownList")
            {
                //do not clear the search for dropdownlist.
                ((UIFieldBase)control).ControlValue = null;
                ((UIFieldBase)control).ControlValueTo = null;
            }
        }
        else
        {
            foreach (Control childControl in control.Controls)
                ClearControls(childControl);
        }
    }


    /// <summary>
    /// The name of the base table as declared in the TablesLogic class.
    /// This should be something like "tWork", or "tLocation", or
    /// "tUser".
    /// </summary>
    protected string baseTable;


    /// <summary>
    /// Gets or sets the name of the base table as declared in the TablesLogic
    /// class. This should be something like "tWork", or "tLocation", or
    /// "tUser".
    /// </summary>
    public string BaseTable
    {
        get
        {
            return baseTable;
        }
        set
        {
            baseTable = value;
        }
    }


    SchemaBase table = null;


    /// <summary>
    /// The Search event handler.
    /// </summary>
    /// <param name="e"></param>
    public delegate void SearchEventHandler(SearchEventArgs e);


    /// <summary>
    /// Occurs when the user performs a search by clicking on the
    /// Perform Search button, or when the application calls
    /// the PerformSearch() method.
    /// </summary>
    [Browsable(true)]
    public event SearchEventHandler Search;


    /// <summary>
    /// The Process Result event handler.
    /// </summary>
    /// <param name="e"></param>
    public delegate void ProcessResultEventHandler(Object e);


    /// <summary>
    /// Occurs before the search result binds to the gridview
    /// in PerformSearch() method.
    /// </summary>
    [Browsable(true)]
    public event ProcessResultEventHandler ProcessResult;


    public delegate void PagePanelClickEventHandler(object sender, string commandName);

    /// <summary>
    /// Occurs when a button is clicked.
    /// </summary>
    [Browsable(true)]
    public event PagePanelClickEventHandler Click;


    /// <summary>
    /// Gets the currently searching object type.
    /// </summary>
    /// <returns></returns>
    private string GetRequestObjectType()
    {
        if (Request["TYPE"] == null)
            return "";
        return Security.Decrypt(Request["TYPE"]).Split(':')[0];
    }


    /// <summary>
    /// Reflect on the Tables class and get the object whose name is
    /// specified in the BaseTable property.
    /// </summary>
    private void GetBaseTable()
    {
        Type baseTableType = typeof(TablesLogic);
        string baseTableName = baseTable;
        if (baseTable.StartsWith("TablesLogic."))
        {
            baseTableType = typeof(TablesLogic);
            baseTableName = baseTableName.Replace("TablesLogic.", "");
        }
        else if (baseTable.StartsWith("TablesWorkflow."))
        {
            baseTableType = typeof(TablesWorkflow);
            baseTableName = baseTableName.Replace("TablesWorkflow.", "");
        }
        else if (baseTable.StartsWith("TablesAuditTrail."))
        {
            baseTableType = typeof(TablesAuditTrail);
            baseTableName = baseTableName.Replace("TablesAuditTrail.", "");
        }

        table = (SchemaBase)UIBinder.GetValue(baseTableType, baseTableName, true);
        if (table == null)
            throw new Exception("Unknown table '" + baseTable + "'");
    }


    /// <summary>
    /// Builds an ExpressionCondition from all UIField controls on the page.
    /// </summary>
    /// <returns>An ExpressionCondition representing the tree of the
    /// SQL WHERE condition.</returns>
    public ExpressionCondition GetCondition()
    {
        if (Validate != null)
            Validate(this, new EventArgs());

        string errorMessage = ObjectPanel.CheckErrorMessages();
        if (errorMessage != null)
        {
            this.Message = errorMessage;
            return null;
        }

        if (ObjectPanel == null || GridView == null)
            return null;

        GetBaseTable();
        ExpressionCondition cond = null;
        try
        {
            if (DialogBox != null)
            {
                bool oldVisible = DialogBox.Visible;
                DialogBox.Visible = true;
                cond = UIBinder.BuildCondition(table, DialogBox.ObjectPanel);
                DialogBox.Visible = oldVisible;
            }

            if (cond == null)
                cond = Query.True;
        }
        catch (Exception ex)
        {
            this.Message = ObjectPanel.CheckErrorMessages();
            return null;
        }

        /*
        // add condition to filter works that are assigned to the current user
        //
        if (divAssignedOnly.Visible && AssignedOnly.Checked)
        {
            TActivity tActivity = (TActivity)UIBinder.GetValue(table, "CurrentActivity", false);

            if (tActivity != null)
            {
                cond = cond &
                    (tActivity.Users.ObjectID == AppSession.User.ObjectID |
                    tActivity.Positions.ObjectID.In(AppSession.User.Positions));
            }
        }
         * */

        //build condition for the simple search textbox
        if (simpleSearchFields.Trim() != "" && SimpleSearch.Text.Trim() != "")
        {
            string[] fields = simpleSearchFields.Split(',');
            string[] searchvalue = SimpleSearch.Text.Trim().Split(' ');
            ExpressionCondition scond = null;

            for (int j = 0; j < searchvalue.Length; j++)
            {
                // 2013.08.12
                // Kim Foong
                //
                searchvalue[j] = ExpressionDataString.EscapeLikeString(searchvalue[j]);

                ExpressionCondition valuecond = null;
                for (int i = 0; i < fields.Length; i++)
                {
                    if (valuecond == null)
                        valuecond = ((ExpressionDataString)UIBinder.GetValue(table, fields[i], false)).Like("%" + searchvalue[j] + "%");
                    else
                        valuecond = valuecond | ((ExpressionDataString)UIBinder.GetValue(table, fields[i], false)).Like("%" + searchvalue[j] + "%");
                }

                if (scond == null)
                    scond = valuecond;
                else
                    scond = scond & valuecond;
            }

            cond = cond & (scond);
        }


        cond = cond & ((ExpressionData)UIBinder.GetValue(table, "IsDeleted", false)) == 0;



        return cond;
    }


    /// <summary>
    /// Builds an ExpressionCondition from all UIField controls on the page,
    /// then calls the Search event and ANDs the built condition and 
    /// any custom condition that the developer has defined in the
    /// SearchEventArgs.CustomCondition field.
    /// </summary>
    /// <returns>An ExpressionCondition representing the tree of the
    /// SQL WHERE condition.</returns>
    public ExpressionCondition GetConditionAndCustomCondition()
    {
        ExpressionCondition cond = GetCondition();
        if (cond == null)
            return Query.True;

        SearchEventArgs searchEventArgs = new SearchEventArgs(cond);

        // Added: Wang Yiyuan, 3/12/2012
        // Add the default sort order. ObjectName ascending and CreatedDateTime descending
        //
        searchEventArgs.CustomSortOrder = new List<ColumnOrder>();
        searchEventArgs.CustomSortOrder.Add(Case.IsNull(table.ObjectName, "").Asc);
        //searchEventArgs.CustomSortOrder.Add(table.ObjectName.Asc);
        searchEventArgs.CustomSortOrder.Add(table.CreatedDateTime.Desc);

        if (Search != null)
            Search(searchEventArgs);

        // build the where condition automatically
        //
        if (searchEventArgs.CustomCondition != null)
        {
            ExpressionCondition cond2 = searchEventArgs.CustomCondition;
            if (cond2 == null)
                cond2 = Query.True;
            cond = cond & cond2;
        }

        return cond;
    }


    bool refreshTree = false;


    /// <summary>
    /// Peforms a search by:
    /// <list>
    /// <item>1. Obtaining the search condition through the data entered 
    ///          into the fields.</item>
    /// <item>2. Call the custom search for any add-on conditions</item>
    /// <item>3. Construct and execute the query</item>
    /// <item>4. Bind the results to the grid.</item>
    /// </list>
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void buttonSearch_Click(object sender, EventArgs e)
    {
        int maxSearchResult = (int)MaximumNumberOfResults;

        this.Message = "";
        if (!ObjectPanel.IsValid)
        {
            this.Message = ObjectPanel.CheckErrorMessages();
            return;
        }

        // Checks if the SimpleSearch dropdown list is pointing
        // to the current object. If not, we redirect the user
        // to the search page that searches for the object selected
        // in the dropdown list.
        //
        /*if (SimpleSearchDropDownList.Visible)
        {
            string[] p = SimpleSearchDropDownList.SelectedValue.Split('\x255');
            if (SimpleSearchDropDownList.SelectedValue == "")
            {
                string url = Page.ResolveUrl(OApplicationSetting.Current.HomePageUrl);
                Window.Open(url +
                    (url.IndexOf("?") > 0 ? "&" : "?") +
                    "S=" + HttpUtility.UrlEncode(SearchTextBox.Text), "frameBottom");
                return;
            }
            else if (p[1] != GetRequestObjectType())
            {
                string url = Page.ResolveUrl(p[0]);
                string objectTypeName = p[1];
                Window.Open(url +
                    (url.IndexOf("?") > 0 ? "&" : "?") +
                    "S=" + HttpUtility.UrlEncode(SearchTextBox.Text) +
                    "&TYPE=" + HttpUtility.UrlEncode(Security.Encrypt(objectTypeName)), "frameBottom");
                return;
            }
        }*/


        GridView.PageSize = (int)ResultsPerPage;


        try
        {
            System.Data.DataTable dt = null;
            IEnumerable ie = null;
            ArrayList al = new ArrayList();

            // build the condition from the form input
            //
            ExpressionCondition cond = GetCondition();
            if (cond == null)
                return;

            // call the delegate function to see if there are additional conditions,
            // or if there is a custom dataset queried by the delegate function.
            //
            SearchEventArgs searchEventArgs = new SearchEventArgs(cond);

            searchEventArgs.CustomSortOrder = new List<ColumnOrder>();
            if ((object)(table.ObjectName) != null)
                searchEventArgs.CustomSortOrder.Add(Case.IsNull(table.ObjectName, "").Asc);
            //searchEventArgs.CustomSortOrder.Add(table.ObjectName.Asc);
            if ((object)(table.CreatedDateTime) != null)
                searchEventArgs.CustomSortOrder.Add(table.CreatedDateTime.Desc);

            if (Search != null)
                Search(searchEventArgs);

            // 2011.10.20
            // Kim Foong
            // Changed this to use ReadUncommitted so that searching does not get
            // locked up when any of the records on the main and joined tables 
            // are currently locked for updates. Of course, this means that 
            // non-committed data may make its way into the search page, but
            // since search pages are NOT used for reporting, the validity of the
            // data is not as critical as an actual report. Furthermore, the
            // chances of a transaction rolling back is rare, so the reading
            // non-committed data should be relatively safe.
            // 
            //using (Connection t = new Connection(IsolationLevel.NoTransaction))
            using (Connection t = new Connection(Anacle.DataFramework.IsolationLevel.ReadUncommitted))
            {
                if (searchEventArgs.CustomResultTable == null)
                {
                    // build the where condition automatically
                    //
                    if (searchEventArgs.CustomCondition != null)
                    {
                        ExpressionCondition cond2 = searchEventArgs.CustomCondition;
                        if (cond2 == null)
                            cond2 = Query.True;
                        cond = cond & cond2;
                    }

                    /*
                    // build condition to test if object is currently assigned
                    // to logged on user
                    //
                    if (divAssignedOnly.Visible && AssignedOnly.Checked)
                    {
                        TActivity tActivity = (TActivity)UIBinder.GetValue(table, "CurrentActivity", false);oni

                        if (tActivity != null)
                        {
                            cond = cond &
                                (tActivity.Users.ObjectID == AppSession.User.ObjectID |
                                tActivity.Positions.ObjectID.In(AppSession.User.Positions));
                        }
                    }
                    */

                    if (SearchType == PanelSearchType.SelectQuery)
                    {
                        Hashtable columnsAdded = new Hashtable();

                        // get the results for the select().where() query
                        //
                        // build the select clause automatically
                        //
                        List<ColumnAs> selectColumns = new List<ColumnAs>();
                        foreach (DataControlField c in this.GridView.Columns)
                        {
                            if (c is BoundField)
                            {
                                object obj = UIBinder.GetValue(table, ((BoundField)c).DataField, false);
                                if (obj is ExpressionData)
                                {
                                    if (columnsAdded[((ExpressionData)obj).As(((BoundField)c).DataField)] == null)
                                    {
                                        selectColumns.Add(((ExpressionData)obj).As(((BoundField)c).DataField));
                                        columnsAdded[((ExpressionData)obj).As(((BoundField)c).DataField)] = 1;
                                    }
                                }

                            }
                        }

                        // make sure the main table's object ID is selected, and that the
                        // main table's object's isDeleted flag = 0
                        // Add the ObjectName and CraetedDateTime fields into the selected columns to for order by clause
                        // Modified: Wang yiyuan, 3/12/2012
                        //
                        if (columnsAdded["ObjectID"] == null)
                            selectColumns.Add(table.ObjectID.As("ObjectID"));

                        if (columnsAdded["_ONSort_"] == null)
                            selectColumns.Add(Case.IsNull(table.ObjectName, "").As("_ONSort_"));

                        if (columnsAdded["_CDSort_"] == null)
                            selectColumns.Add(table.CreatedDateTime.As("_CDSort_"));

                        if (this.enableResultsCap)
                        {
                            Query query = null;
                            if (maxSearchResult != 0)
                                query = table.SelectDistinctTop(maxSearchResult + 1, selectColumns.ToArray()).Where(cond);
                            else
                                query = table.SelectDistinct(selectColumns.ToArray()).Where(cond);

                            if (searchEventArgs.CustomGroupBy != null)
                                query = query.GroupBy(searchEventArgs.CustomGroupBy);
                            if (searchEventArgs.CustomHavingCondition != null)
                                query = query.Having(searchEventArgs.CustomHavingCondition);
                            if (searchEventArgs.CustomSortOrder != null)
                                query = query.OrderBy(searchEventArgs.CustomSortOrder.ToArray());

                            dt = query;

                            if (maxSearchResult != 0 && dt.Rows.Count > maxSearchResult)
                            {
                                dt.Rows.RemoveAt(maxSearchResult);
                                //display msg
                                this.Message = String.Format(Resources.Strings.SearchPanel_ResultExceedMax, maxSearchResult);
                            }
                        }
                        else
                        {
                            Query query = table.SelectDistinct(selectColumns.ToArray()).Where(cond);
                            if (searchEventArgs.CustomGroupBy != null)
                                query = query.GroupBy(searchEventArgs.CustomGroupBy);
                            if (searchEventArgs.CustomHavingCondition != null)
                                query = query.Having(searchEventArgs.CustomHavingCondition);
                            if (searchEventArgs.CustomSortOrder != null)
                                query = query.OrderBy(searchEventArgs.CustomSortOrder.ToArray());

                            dt = query;
                        }


                    }
                    else
                    {
                        // build the TablesLogic.tXXXX[] query
                        //
                        if (this.enableResultsCap)
                        {
                            if (searchEventArgs.CustomSortOrder != null)
                            {
                                if (maxSearchResult != 0)
                                {
                                    if (searchEventArgs.CustomHavingCondition != null)
                                        ie = table.LoadObjects(cond, searchEventArgs.CustomHavingCondition, false, maxSearchResult + 1, searchEventArgs.CustomSortOrder.ToArray());
                                    else
                                        ie = table.LoadObjects(cond, null, false, maxSearchResult + 1, searchEventArgs.CustomSortOrder.ToArray());
                                }
                                else
                                {
                                    if (searchEventArgs.CustomHavingCondition != null)
                                        ie = table.LoadObjects(cond, searchEventArgs.CustomHavingCondition, false, searchEventArgs.CustomSortOrder.ToArray());
                                    else
                                        ie = table.LoadObjects(cond, null, false, searchEventArgs.CustomSortOrder.ToArray());
                                }
                            }
                            else
                            {
                                if (maxSearchResult != 0)
                                {
                                    if (searchEventArgs.CustomHavingCondition != null)
                                        ie = table.LoadObjects(cond, searchEventArgs.CustomHavingCondition, false, maxSearchResult + 1, null);
                                    else
                                        ie = table.LoadObjects(cond, null, false, maxSearchResult + 1, null);
                                }
                                else
                                {
                                    if (searchEventArgs.CustomHavingCondition != null)
                                        ie = table.LoadObjects(cond, searchEventArgs.CustomHavingCondition, false, null);
                                    else
                                        ie = table.LoadObjects(cond, null, false, null);
                                }
                            }
                        }
                        else
                        {
                            if (searchEventArgs.CustomSortOrder != null)
                            {
                                if (searchEventArgs.CustomHavingCondition != null)
                                    ie = table.LoadObjects(cond, searchEventArgs.CustomHavingCondition, false, searchEventArgs.CustomSortOrder.ToArray());
                                else
                                    ie = table.LoadObjects(cond, null, false, searchEventArgs.CustomSortOrder.ToArray());
                            }
                            else
                            {
                                if (searchEventArgs.CustomHavingCondition != null)
                                    ie = table.LoadObjects(cond, searchEventArgs.CustomHavingCondition, false, null);
                                else
                                    ie = table.LoadObjects(cond, null, false, null);
                            }
                        }

                        // Build a temporary hash table so
                        // that we can use it in the "AcquireObject".
                        // event handler.
                        //
                        hashObjects = new Hashtable();
                        listOfViewableObjects = new Hashtable();
                        foreach (PersistentObject p in ie)
                        {
                            hashObjects[p.ObjectID.Value] = p;

                            // 2016.09.20
                            // Kien Trung
                            // NEW: to allow submittor / creator view access to their own document
                            //
                            if (table.GetPersistentObjectType().IsSubclassOf(typeof(LogicLayerWorkflowPersistentObject)))
                            {
                                if (((LogicLayerWorkflowPersistentObject)p).CreatedID == AppSession.User.ObjectID)
                                    listOfViewableObjects[p.ObjectID.Value] = 1;
                            }

                            if (listOfViewableObjects[p.ObjectID.Value] == null && table.GetPersistentObjectType().IsSubclassOf(typeof(LogicLayerApprovalPersistentObject)))
                            {
                                if (((LogicLayerApprovalPersistentObject)p).SubmittorID == AppSession.User.ObjectID)
                                    listOfViewableObjects[p.ObjectID.Value] = 1;
                            }

                            al.Add(p);
                        }

                        if (maxSearchResult != 0 && al.Count > maxSearchResult && this.enableResultsCap)
                        {
                            al.RemoveAt(maxSearchResult);
                            //display msg   
                            this.Message = String.Format(Resources.Strings.SearchPanel_ResultExceedMax, maxSearchResult);

                        }
                    }
                }
                else
                {
                    dt = searchEventArgs.CustomResultTable;
                }

                //sli 04-Aug-2011: Reset the PageIndex to 0 when user click the search button.
                if (hiddenRefresh.Value != "1")
                    GridView.Grid.PageIndex = 0;
                else
                    hiddenRefresh.Value = null;

                if (dt != null)
                {
                    if (ProcessResult != null)
                        ProcessResult(dt);

                    GridView.DataSource = dt;
                }
                else if (al != null)
                {
                    if (ProcessResult != null)
                        ProcessResult(al);

                    GridView.DataSource = al;
                }
                GridView.DataBind();


                // If there is a tree list, refresh it.
                //
                if (refreshTree && EditButtonVisible)
                {
                    UIFieldTreeList treeList = GetUIFieldTreeList(this.Page);
                    if (treeList != null)
                    {
                        string selectedValue = treeList.SelectedValue;
                        treeList.PopulateTree(true);
                        treeList.SelectedValue = selectedValue;
                    }
                }

                if (sender == buttonSearch)
                    GridView.SetFocus();

                //t.Commit();
            }
        }
        catch (Exception ex)
        {
            Helpers.LogException(ex, this.Request);
            if (Helpers.IsDebug())
                this.Message = ex.Message + "<br/>" + ex.StackTrace.Replace("\n", "<br/>");
            else
                this.Message = String.Format(Resources.Errors.General_ErrorOccurred, DateTime.Now);
        }

        if (AdvancedSearchPanel != null)
        {
            DataTable dtx = new DataTable();
            dtx.Columns.Add("Text");
            dtx.Columns.Add("ObjectID");

            BuildSearchedDataTable(AdvancedSearchPanel, dtx);

            ListAdvancedSearch.DataSource = dtx;
            ListAdvancedSearch.DataBind();

            ListAdvancedSearchPanel.Visible = dtx.Rows.Count > 0;
            if (dtx.Rows.Count > 0)
            {
                buttonAdvanceSearch.ButtonColor = UIButtonColor.White;
                buttonAdvanceSearch.ButtonType = UIButtonType.NormalRaised;
            }
            else
            {
                buttonAdvanceSearch.ButtonColor = UIButtonColor.Default;
                buttonAdvanceSearch.ButtonType = UIButtonType.NormalFlat;
            }
        }


    }


    /// <summary>
    /// Programmatically performs a search, as if the user has clicked
    /// on the Reset Fields button.
    /// </summary>
    public void ResetFields()
    {
        buttonReset_Click(this, new EventArgs());
    }

    /// <summary>
    /// Programmatically performs a search, as if the user has clicked
    /// on the Perform Search button.
    /// </summary>
    public void PerformSearch()
    {
        buttonSearch_Click(this, new EventArgs());
    }


    /// <summary>
    /// Refresh the treeview if necessary, and re-do the search
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void buttonSearchNoFocus_Click(object sender, EventArgs e)
    {
        refreshTree = true;
        buttonSearch_Click(sender, e);
    }


    /// <summary>
    /// This event resets all fields in the page.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void buttonReset_Click(object sender, EventArgs e)
    {
        // recursively traverses through all controls in the list, and
        // sets the value to null to all controls.
        // 
        if (ObjectPanel != null)
            ClearControls(ObjectPanel);
        PerformSearch();

    }


    /// <summary>
    /// Represents a set of arguments to be used by the Search event
    /// to customize the condition of the search, or the results of
    /// the search.
    /// </summary>
    public class SearchEventArgs
    {
        /// <summary>
        /// The ExpressionCondition representing the SQL where condition
        /// built automatically from all UIField controls using their
        /// PropertyName and the value entered by the user.
        /// </summary>
        public readonly ExpressionCondition InputCondition = null;

        /// <summary>
        /// The ExpressionCondition representing the SQL where condition
        /// that will be AND-ed with the InputCondition (the condition
        /// built automatically from all UIField controls using their
        /// PropertyName and the value entered by the user) to form
        /// the condition used to run against the database.
        /// </summary>
        public ExpressionCondition CustomCondition = null;

        /// <summary>
        /// An array of ExpressionData representing the columns
        /// in the table or the output to group the results by.
        /// </summary>
        public ExpressionData[] CustomGroupBy = null;

        /// <summary>
        /// The ExpressionCondition representing the SQL having
        /// condition that will be used as part of the query.
        /// </summary>
        public ExpressionCondition CustomHavingCondition = null;

        /// <summary>
        /// The order that the results should be sorted by.
        /// <para></para>
        /// Note: The results are always sorted by ObjectName by default. 
        /// </summary>
        public List<ColumnOrder> CustomSortOrder = null;

        /// <summary>
        /// The custom result DataTable that will be bound to the results.
        /// When specified, the objectSearchPanel.ascx will not perform
        /// a search against the database but instead bind this 
        /// CustomResultTable to the GridView.
        /// </summary>
        public System.Data.DataTable CustomResultTable = null;


        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="conditionFromUserInput"></param>
        public SearchEventArgs(ExpressionCondition conditionFromUserInput)
        {
            InputCondition = conditionFromUserInput;
        }
    }


    /// <summary>
    /// This adds a new item by opening up the object edit window with
    /// the appropriate querystring parameters.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void buttonAdd_Click(object sender, EventArgs e)
    {
        string additionalQueryString = "SEARCH=1";
        /*if (EditButtonVisible)
        {
            UIFieldTreeList treeList = GetUIFieldTreeList(this.Page);
            if (treeList != null && treeList.SelectedValue != "")
                additionalQueryString = additionalQueryString + "&TREEOBJID=" + HttpUtility.UrlEncode(Security.Encrypt(treeList.SelectedValue));
        }*/

        Window.OpenAddObjectPage(this.Page, GetRequestObjectType(), additionalQueryString);
    }


    /// <summary>
    /// Occurs when the user clicks on the Close Window button,
    /// which instructs the system to close the window.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void buttonCancel_Click(object sender, EventArgs e)
    {
        Window.Close();
    }


    /// <summary>
    /// This edits the item selected on the results grid by opening
    /// up the object edit window with the appropriate querystring
    /// parameters.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void buttonEdit_Click(object sender, EventArgs e)
    {
        UIFieldTreeList treeList = GetUIFieldTreeList(this.Page);

        if (treeList != null && treeList.SelectedValue != "")
        {
            Window.OpenEditObjectPage(this.Page, GetRequestObjectType(), treeList.SelectedValue, "");
        }
    }


    /// <summary>
    /// Occurs when the user clicks on the Search button, 
    /// before the search is actually run against the database.
    /// </summary>
    public event EventHandler Validate;


    /// <summary>
    /// Occurs when the form is first loaded. This event is
    /// not called during post backs.
    /// </summary>
    public event EventHandler PopulateForm;

    /// <summary>
    /// Occurs after the user clicks the Add Selected button,
    /// and after all the necessary validations have taken
    /// place.
    /// </summary>
    public event EventHandler ValidateAndAddSelected;

    public void ShowAdvancedSearchPanel()
    {
        AdvancedSearchPanel.Visible = true;
    }

    public void HideAdvancedSearchPanel()
    {
        AdvancedSearchPanel.Visible = false;
    }


    //--------------------------------------------
    // button1
    //--------------------------------------------
    [Localizable(true)]
    public string Button1_Caption
    {
        get { return UIButton1.Text; }
        set { UIButton1.Text = value; }
    }

    public string Button1_ImageUrl
    {
        get { return UIButton1.ImageUrl; }
        set { UIButton1.ImageUrl = value; }
    }

    public string Button1_CommandName
    {
        get { return UIButton1.CommandName; }
        set { UIButton1.CommandName = value; }
    }

    public bool Button1_CausesValidation
    {
        get { return UIButton1.CausesValidation; }
        set { UIButton1.CausesValidation = value; }
    }

    public bool Button1_AlwaysEnabled
    {
        get { return UIButton1.AlwaysEnabled; }
        set { UIButton1.AlwaysEnabled = value; }
    }

    public bool Button1_Visible
    {
        get { return Button1Span.Visible; }
        set { Button1Span.Visible = value; }
    }

    [Localizable(true)]
    public string Button1_ConfirmText
    {
        get { return UIButton1.ConfirmText; }
        set { UIButton1.ConfirmText = value; }
    }


    //--------------------------------------------
    // button2
    //--------------------------------------------
    [Localizable(true)]
    public string Button2_Caption
    {
        get { return UIButton2.Text; }
        set { UIButton2.Text = value; }
    }

    public string Button2_ImageUrl
    {
        get { return UIButton2.ImageUrl; }
        set { UIButton2.ImageUrl = value; }
    }

    public string Button2_CommandName
    {
        get { return UIButton2.CommandName; }
        set { UIButton2.CommandName = value; }
    }

    public bool Button2_CausesValidation
    {
        get { return UIButton2.CausesValidation; }
        set { UIButton2.CausesValidation = value; }
    }

    public bool Button2_AlwaysEnabled
    {
        get { return UIButton2.AlwaysEnabled; }
        set { UIButton2.AlwaysEnabled = value; }
    }

    public bool Button2_Visible
    {
        get { return Button2Span.Visible; }
        set { Button2Span.Visible = value; }
    }

    [Localizable(true)]
    public string Button2_ConfirmText
    {
        get { return UIButton2.ConfirmText; }
        set { UIButton2.ConfirmText = value; }
    }


    //--------------------------------------------
    // button3
    //--------------------------------------------
    [Localizable(true)]
    public string Button3_Caption
    {
        get { return UIButton3.Text; }
        set { UIButton3.Text = value; }
    }

    public string Button3_ImageUrl
    {
        get { return UIButton3.ImageUrl; }
        set { UIButton3.ImageUrl = value; }
    }

    public string Button3_CommandName
    {
        get { return UIButton3.CommandName; }
        set { UIButton3.CommandName = value; }
    }

    public bool Button3_CausesValidation
    {
        get { return UIButton3.CausesValidation; }
        set { UIButton3.CausesValidation = value; }
    }

    public bool Button3_AlwaysEnabled
    {
        get { return UIButton3.AlwaysEnabled; }
        set { UIButton3.AlwaysEnabled = value; }
    }

    public bool Button3_Visible
    {
        get { return Button3Span.Visible; }
        set { Button3Span.Visible = value; }
    }

    [Localizable(true)]
    public string Button3_ConfirmText
    {
        get { return UIButton3.ConfirmText; }
        set { UIButton3.ConfirmText = value; }
    }



    //--------------------------------------------
    // button4
    //--------------------------------------------
    [Localizable(true)]
    public string Button4_Caption
    {
        get { return UIButton4.Text; }
        set { UIButton4.Text = value; }
    }

    public string Button4_ImageUrl
    {
        get { return UIButton4.ImageUrl; }
        set { UIButton4.ImageUrl = value; }
    }

    public string Button4_CommandName
    {
        get { return UIButton4.CommandName; }
        set { UIButton4.CommandName = value; }
    }

    public bool Button4_CausesValidation
    {
        get { return UIButton4.CausesValidation; }
        set { UIButton4.CausesValidation = value; }
    }

    public bool Button4_AlwaysEnabled
    {
        get { return UIButton4.AlwaysEnabled; }
        set { UIButton4.AlwaysEnabled = value; }
    }

    public bool Button4_Visible
    {
        get { return Button4Span.Visible; }
        set { Button4Span.Visible = value; }
    }

    [Localizable(true)]
    public string Button4_ConfirmText
    {
        get { return UIButton4.ConfirmText; }
        set { UIButton4.ConfirmText = value; }
    }


    //--------------------------------------------
    // button5
    //--------------------------------------------
    [Localizable(true)]
    public string Button5_Caption
    {
        get { return UIButton5.Text; }
        set { UIButton5.Text = value; }
    }

    public string Button5_ImageUrl
    {
        get { return UIButton5.ImageUrl; }
        set { UIButton5.ImageUrl = value; }
    }

    public string Button5_CommandName
    {
        get { return UIButton5.CommandName; }
        set { UIButton5.CommandName = value; }
    }

    public bool Button5_CausesValidation
    {
        get { return UIButton5.CausesValidation; }
        set { UIButton5.CausesValidation = value; }
    }

    public bool Button5_AlwaysEnabled
    {
        get { return UIButton5.AlwaysEnabled; }
        set { UIButton5.AlwaysEnabled = value; }
    }

    public bool Button5_Visible
    {
        get { return Button5Span.Visible; }
        set { Button5Span.Visible = value; }
    }

    [Localizable(true)]
    public string Button5_ConfirmText
    {
        get { return UIButton5.ConfirmText; }
        set { UIButton5.ConfirmText = value; }
    }


    //--------------------------------------------
    // button6
    //--------------------------------------------
    [Localizable(true)]
    public string Button6_Caption
    {
        get { return UIButton6.Text; }
        set { UIButton6.Text = value; }
    }

    public string Button6_ImageUrl
    {
        get { return UIButton6.ImageUrl; }
        set { UIButton6.ImageUrl = value; }
    }

    public string Button6_CommandName
    {
        get { return UIButton6.CommandName; }
        set { UIButton6.CommandName = value; }
    }

    public bool Button6_CausesValidation
    {
        get { return UIButton6.CausesValidation; }
        set { UIButton6.CausesValidation = value; }
    }

    public bool Button6_AlwaysEnabled
    {
        get { return UIButton6.AlwaysEnabled; }
        set { UIButton6.AlwaysEnabled = value; }
    }

    public bool Button6_Visible
    {
        get { return Button6Span.Visible; }
        set { Button6Span.Visible = value; }
    }

    [Localizable(true)]
    public string Button6_ConfirmText
    {
        get { return UIButton6.ConfirmText; }
        set { UIButton6.ConfirmText = value; }
    }


    //--------------------------------------------
    // button7
    //--------------------------------------------
    [Localizable(true)]
    public string Button7_Caption
    {
        get { return UIButton7.Text; }
        set { UIButton7.Text = value; }
    }

    public string Button7_ImageUrl
    {
        get { return UIButton7.ImageUrl; }
        set { UIButton7.ImageUrl = value; }
    }

    public string Button7_CommandName
    {
        get { return UIButton7.CommandName; }
        set { UIButton7.CommandName = value; }
    }

    public bool Button7_CausesValidation
    {
        get { return UIButton7.CausesValidation; }
        set { UIButton7.CausesValidation = value; }
    }

    public bool Button7_AlwaysEnabled
    {
        get { return UIButton7.AlwaysEnabled; }
        set { UIButton7.AlwaysEnabled = value; }
    }

    public bool Button7_Visible
    {
        get { return Button7Span.Visible; }
        set { Button7Span.Visible = value; }
    }

    [Localizable(true)]
    public string Button7_ConfirmText
    {
        get { return UIButton7.ConfirmText; }
        set { UIButton7.ConfirmText = value; }
    }


    //--------------------------------------------
    // button8
    //--------------------------------------------
    [Localizable(true)]
    public string Button8_Caption
    {
        get { return UIButton8.Text; }
        set { UIButton8.Text = value; }
    }

    public string Button8_ImageUrl
    {
        get { return UIButton8.ImageUrl; }
        set { UIButton8.ImageUrl = value; }
    }

    public string Button8_CommandName
    {
        get { return UIButton8.CommandName; }
        set { UIButton8.CommandName = value; }
    }

    public bool Button8_CausesValidation
    {
        get { return UIButton8.CausesValidation; }
        set { UIButton8.CausesValidation = value; }
    }

    public bool Button8_AlwaysEnabled
    {
        get { return UIButton8.AlwaysEnabled; }
        set { UIButton8.AlwaysEnabled = value; }
    }

    public bool Button8_Visible
    {
        get { return Button8Span.Visible; }
        set { Button8Span.Visible = value; }
    }

    [Localizable(true)]
    public string Button8_ConfirmText
    {
        get { return UIButton8.ConfirmText; }
        set { UIButton8.ConfirmText = value; }
    }


    void UIButton8_Click(object sender, EventArgs e)
    {
        if (Click != null)
            Click(this, this.Button8_CommandName);
    }

    void UIButton7_Click(object sender, EventArgs e)
    {
        if (Click != null)
            Click(this, this.Button7_CommandName);
    }

    void UIButton6_Click(object sender, EventArgs e)
    {
        if (Click != null)
            Click(this, this.Button6_CommandName);
    }

    void UIButton5_Click(object sender, EventArgs e)
    {
        if (Click != null)
            Click(this, this.Button5_CommandName);
    }

    void UIButton4_Click(object sender, EventArgs e)
    {
        if (Click != null)
            Click(this, this.Button4_CommandName);
    }

    void UIButton3_Click(object sender, EventArgs e)
    {
        if (Click != null)
            Click(this, this.Button3_CommandName);
    }

    void UIButton2_Click(object sender, EventArgs e)
    {
        if (Click != null)
            Click(this, this.Button2_CommandName);
    }

    void UIButton1_Click(object sender, EventArgs e)
    {
        if (Click != null)
            Click(this, this.Button1_CommandName);
    }


    void MaxNoOfRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (MaxNoOfRecords.SelectedValue != "")
        {
            MaximumNumberOfResults = (MaxNumberOfResults)Enum.Parse(typeof(MaxNumberOfResults), "_" + MaxNoOfRecords.SelectedValue);
            UpdateSearchPreference();
            PerformSearch();
        }
    }

    void NoOfRecordsPerRows_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (NoOfRecordsPerRows.SelectedValue != "")
        {
            ResultsPerPage = (NumberOfResults)Enum.Parse(typeof(NumberOfResults), "_" + NoOfRecordsPerRows.SelectedValue);
            UpdateSearchPreference();
            PerformSearch();
        }
    }

    public void ShowException(Exception ex)
    {
        Exception innerException = ex;
        while (innerException != null)
        {
            if (innerException is LogicLayerValidationException)
            {
                this.Message = innerException.Message;
                return;
            }

            innerException = innerException.InnerException;
        }

        if (Helpers.IsDebug())
        {
            StringBuilder sb = new StringBuilder();
            Exception currentException = ex;
            while (currentException != null)
            {
                sb.Append(currentException.Message + "\n" + currentException.StackTrace + "\n\n");
                currentException = currentException.InnerException;
            }
            this.Message =
                HttpUtility.HtmlEncode(sb.ToString()).Replace("\n", "<br>");
        }
        else
            this.Message = String.Format(Resources.Errors.General_ErrorOccurred, DateTime.Now);
    }

    protected void buttonAdvanceSearch_Click(object sender, EventArgs e)
    {
        this.DialogBox.Show();
    }

    protected void buttonExportExcel_Click(object sender, EventArgs e)
    {
        this.GridView.ExportData(ExportType.XLSX);
    }

    protected void buttonExportWord_Click(object sender, EventArgs e)
    {
        this.GridView.ExportData(ExportType.DOCX);
    }

    protected void buttonExportCSV_Click(object sender, EventArgs e)
    {
        this.GridView.ExportData(ExportType.CSV);
    }

    protected void ListAdvancedSearch_Action(object sender, string commandName, List<object> dataKeys)
    {

    }

    protected void ListAdvancedSearch_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        if (e.CommandName == "DeleteFilter")
        {
            Control c = Page.FindControl(e.CommandArgument.ToString());
            if (c != null && c is UIFieldBase)
                ((UIFieldBase)c).ControlValue = null;
            if (c != null && c is UIFieldDateTime)
                ((UIFieldBase)c).ControlValueTo = null;

            PerformSearch();
        }
    }
</script>


<div class="mdl-title-bar-spacer">
    <div class="mdl-title-bar">
        <div>
            <%-- Title --%>
            <div style="float:left">
                <asp:Label ID="labelCaption" runat="server" meta:resourcekey="labelCaptionResource1" CssClass="mdl-title-bar-label"></asp:Label>
            </div>

            <%-- Add button --%>
            <div style="float:right">
                <span runat="server" ID="spanAdd"  >
                    <ui:UIButton runat="server" ID="buttonAdd" 
                        OnClick="buttonAdd_Click"
                        IconCssClass="material-icons" IconText="add" 
                        ConfirmText="" 
                        ButtonType="RoundMiniRaised" ButtonColor="Green">
                    </ui:UIButton>
                </span>
            </div>

            <%-- Show action sheet button --%>
            <div style="float:right" class="mdl-title-bar-toggle-extra-buttons">
                <ui:UIButton runat="server" ID="buttonExpandActionSheet" IconCssClass="material-icons" IconText="arrow_drop_down" ButtonType="RoundMiniFlat" ButtonColor="Default" OnClickScript="$('.mdl-title-bar-extra-buttons').toggle(300)" DisableAfterClick="false" DoPostBack="false" />
                &nbsp;
            </div>

            <%-- Search Grid Settings --%>
            <div style="float: right;" class="mdl-title-bar-extra-buttons">
                <div>
                <ui:UIButton runat="server" ID="buttonAdvanceSearch" IconCssClass="material-icons" IconText="find_in_page" ButtonType="NormalFlat" Text="Advanced Search" ButtonColor="Default" OnClick="buttonAdvanceSearch_Click" />
                <ui:UIButton runat="server" ID="buttonExportExcel" IconCssClass="fas fa-file-excel" ButtonType="RoundMiniFlat" ButtonColor="Default" OnClick="buttonExportExcel_Click" />
                <ui:UIButton runat="server" ID="buttonExportWord" IconCssClass="fas fa-file-word" ButtonType="RoundMiniFlat" ButtonColor="Default" OnClick="buttonExportWord_Click" />
                <ui:UIButton runat="server" ID="buttonExportCSV" IconCssClass="fas fa-file-alt" ButtonType="RoundMiniFlat" ButtonColor="Default" OnClick="buttonExportCSV_Click" />
                <ui:UIButton runat="server" ID="buttonColumnChooser" IconCssClass="fas fa-columns" ButtonType="RoundMiniFlat"  ButtonColor="Default" DisableAfterClick="false" DoPostBack="false" />
                <div id="searchLimitControls" runat="server" class="mdl-dropdown-container">
                    <ui:UIButton runat="server" ID="SettingsButton" IconCssClass="material-icons" IconText="settings" ButtonType="RoundMiniFlat" ButtonColor="Default" DoPostBack="false" OnClickScript="popup.show('', '#searchGridSetupMenu', '', event); return false;" />
                    <div class='mdl-dropdown mdl-dropdown-right' id='searchGridSetupMenu' style='display: none; ' onclick="event.stopPropagation();" >
                        <div style="padding: 20px;">
                        <ui:UIFieldDropDownList runat="server" ID="MaxNoOfRecords" Caption="Max Records" CaptionPosition="Side"
                            OnSelectedIndexChanged="MaxNoOfRecords_SelectedIndexChanged" Span="Full"
                            meta:resourcekey="MaxNoOfRecordsResource" >
                            <Items>
                                <asp:ListItem Text="50" Value="50"  meta:resourcekey="MaxNoOfRecordsListItem50Resource" />
                                <asp:ListItem Text="100" Value="100"  meta:resourcekey="MaxNoOfRecordsListItem100Resource" />
                                <asp:ListItem Text="300" Value="300"  meta:resourcekey="MaxNoOfRecordsListItem300Resource" />
                                <asp:ListItem Text="500" Value="500" Selected="True"  meta:resourcekey="MaxNoOfRecordsListItem500Resource" />
                                <asp:ListItem Text="1000" Value="1000"  meta:resourcekey="MaxNoOfRecordsListItem1000Resource" />
                                <asp:ListItem Text="1500" Value="1500"  meta:resourcekey="MaxNoOfRecordsListItem1500Resource" />
                                <asp:ListItem Text="2000" Value="2000"  meta:resourcekey="MaxNoOfRecordsListItem2000Resource" />
                                <asp:ListItem Text="5000" Value="5000"  meta:resourcekey="MaxNoOfRecordsListItem5000Resource" />
                            </Items>
                        </ui:UIFieldDropDownList>
                        <ui:UIFieldDropDownList runat="server" ID="NoOfRecordsPerRows" Caption="Records/page" CaptionPosition="Side"
                            OnSelectedIndexChanged="NoOfRecordsPerRows_SelectedIndexChanged"  Span="Full"
                            meta:resourcekey="NoOfRecordsPerRowsResource">
                            <Items>
                                <asp:ListItem Text="20" Value="20" Selected="True"  meta:resourcekey="NoOfRecordsPerRowsListItem20Resource" />
                                <asp:ListItem Text="50" Value="50"  meta:resourcekey="NoOfRecordsPerRowsListItem50Resource" />
                                <asp:ListItem Text="100" Value="100"  meta:resourcekey="NoOfRecordsPerRowsListItem100Resource" />
                                <asp:ListItem Text="500" Value="500"  meta:resourcekey="NoOfRecordsPerRowsListItem500Resource" />
                                <asp:ListItem Text="1000" Value="1000"  meta:resourcekey="NoOfRecordsPerRowsListItem1000Resource" />
                                <asp:ListItem Text="1500" Value="1500"  meta:resourcekey="NoOfRecordsPerRowsListItem1500Resource" />
                                <asp:ListItem Text="2000" Value="2000"  meta:resourcekey="NoOfRecordsPerRowsListItem2000Resource" />
                                <asp:ListItem Text="5000" Value="5000"  meta:resourcekey="NoOfRecordsPerRowsListItem5000Resource" />
                            </Items>
                        </ui:UIFieldDropDownList>
                        </div>
                    </div>
                </div>
                &nbsp;
                </div>
            </div>


            <div style="clear:both"></div>
        </div>
    </div>
</div>

<asp:Panel CssClass="mdl-search-adv-list-div" ID="ListAdvancedSearchPanel" Visible="false" runat="server">
    <div class="mdl-search-adv-list-label">
        <asp:Label runat="server" ID="ListAdvancedSearchLabel" Text="Advanced Search Filter(s): "></asp:Label>
    </div>
    <div class="mdl-search-adv-list">
        <asp:ListView runat="server" ID="ListAdvancedSearch" FieldLayout="Flow" ShowCaption="false" OnItemCommand="ListAdvancedSearch_ItemCommand">
            <ItemTemplate>
                <span class="mdl-chip mdl-chip--deletable">
                    <span class="mdl-chip__text"><%# ((DataRowView)Container.DataItem)["Text"] %></span>
                    <button type="button" class="mdl-chip__action" onclick="<%# "__doPostBack('" + ListAdvancedSearch.UniqueID + "$ctrl" + Container.DataItemIndex.ToString() + "$DeleteFilter', '" + ((DataRowView)Container.DataItem)["ObjectID"].ToString() + "')" %>"><i class="material-icons">cancel</i></button>
                    <asp:Button runat="server" ID="DeleteFilter" CommandName="DeleteFilter" CommandArgument='<%# ((DataRowView)Container.DataItem)["ObjectID"].ToString() %>' style="display:none" />
                </span>
            </ItemTemplate>
        </asp:ListView>
    </div>
</asp:Panel>

<div style="font-size: 0pt;" class="bg" id="searchHeader">

    <div id="FullPanel" style="float: left" class='bg' runat="server">
       <%-- <div class='search-icon'>--%>
        
        <ui:UIPanel runat="server" ID="panelMain" Style="font-size: 0pt" meta:resourcekey="panelMainResource">
            <ui:UIPanel runat="server" ID="ButtonPanel" meta:resourcekey="ButtonPanelResource" Visible="false" >
                <div>
                    <table cellpadding="0" cellspacing="0" border="0" >
                        <%-- <tr>--%>
                            <td>
                                <table border='0' cellpadding='0' cellspacing='0'>
                                    <tr valign="bottom">
                                        <td>
                                            <ui:UIButton runat="server" ID="buttonCancel" ImageUrl="~/images/window-delete-big.gif"
                                                Text="Close Window" Visible="false" OnClick="buttonCancel_Click" meta:resourcekey="buttonCancelResource1">
                                            </ui:UIButton>
                                        </td>
                                        <td>
                                            <ui:UIPanel runat="server" ID="spanEditAfterTreeSelected" meta:resourcekey="spanEditAfterTreeSelectedResource" >
                                                <ui:UIPanel runat="server" ID="spanEdit" Visible="false" meta:resourcekey="spanEditResource" >
                                                    <ui:UIButton runat="server" ID="buttonEdit" ImageUrl="~/images/symbol-edit-big.gif"
                                                        Text="Edit Item" OnClick="buttonEdit_Click" ConfirmText="" meta:resourcekey="buttonEditResource1"
                                                        Visible='false'></ui:UIButton>
                                                </ui:UIPanel>
                                            </ui:UIPanel>
                                        </td>
                                        <td>
                                            <span runat="server" id="Button1Span">
                                                <ui:UIButton runat="server" ID="UIButton1" CssClass="btn-add" MouseDownCssClass="btn-add"  meta:resourcekey="UIButton1Resource" />
                                            </span><span runat="server" id="Button2Span">
                                                <ui:UIButton runat="server" ID="UIButton2" CssClass="btn-add" MouseDownCssClass="btn-add"  meta:resourcekey="UIButton2Resource" />
                                            </span><span runat="server" id="Button3Span">
                                                <ui:UIButton runat="server" ID="UIButton3" CssClass="btn-add" MouseDownCssClass="btn-add"  meta:resourcekey="UIButton3Resource" />
                                            </span><span runat="server" id="Button4Span">
                                                <ui:UIButton runat="server" ID="UIButton4" CssClass="btn-add" MouseDownCssClass="btn-add"  meta:resourcekey="UIButton4Resource" />
                                            </span><span runat="server" id="Button5Span">
                                                <ui:UIButton runat="server" ID="UIButton5" CssClass="btn-add" MouseDownCssClass="btn-add"  meta:resourcekey="UIButton5Resource" />
                                            </span><span runat="server" id="Button6Span">
                                                <ui:UIButton runat="server" ID="UIButton6" CssClass="btn-add" MouseDownCssClass="btn-add"  meta:resourcekey="UIButton6Resource" />
                                            </span><span runat="server" id="Button7Span">
                                                <ui:UIButton runat="server" ID="UIButton7" CssClass="btn-add" MouseDownCssClass="btn-add"  meta:resourcekey="UIButton7Resource" />
                                            </span><span runat="server" id="Button8Span">
                                                <ui:UIButton runat="server" ID="UIButton8" CssClass="btn-add" MouseDownCssClass="btn-add"  meta:resourcekey="UIButton8Resource" />
                                            </span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        <%-- </tr>--%>
                    </table>
                </div>
                <div style="display: none">
                    <ui:UIButton runat="server" ID="buttonSearchNoFocus" Text="Search" OnClick="buttonSearchNoFocus_Click" meta:resourcekey="buttonSearchNoFocusResource" >
                    </ui:UIButton>
                </div>
            </ui:UIPanel>
            <br />


        </ui:UIPanel>
            
        <asp:Panel runat="server" ID="panelSimpleSearch" Visible="false" meta:resourcekey="panelSimpleSearchResource" HorizontalAlign="Left">
            <table runat='server' id="tableSimpleSearch" cellpadding="0" cellspacing="0" style="width: 100%">
                <tr>
                    <td>
                        <ui:UIFieldSearchableDropDownList runat="server" ID="SimpleSearchDropDownList" Caption="Search for"
                            InternalControlWidth="300px" Visible="false" meta:resourcekey="SimpleSearchDropDownListResource" >
                        </ui:UIFieldSearchableDropDownList>
                    </td>
                </tr>
                <tr>
                    <td colspan='2'>
                        <asp:Label runat='server' ID="SimpleSearchHint" Text="what would you like to find?" Visible="false"
                            CssClass="search-hint" meta:resourcekey="SimpleSearchHintResource" ></asp:Label>
                    </td>
                </tr>
                <tr valign='top'>
                    <td style='width: 350px; padding-right: 10px'>
                        <ui:UIFieldTextBox runat="server" ID="SimpleSearch" MaxLength="0" ShowCaption="false"
                            InternalControlWidth="350px" FieldLayout="Flow" meta:resourcekey="SimpleSearchResource" >
                        </ui:UIFieldTextBox>
                    </td>
                    <td>
                        <span runat="server" id="spanSearch">
                            <asp:ImageButton ID="buttonSearch" runat="server" ImageUrl="~/img/mainsearch4_img.png" OnClick="buttonSearch_Click" style="color: white; background-color: transparent" />
                        </span><span runat="server" id="spanReset">
                            <asp:ImageButton ID="buttonReset" runat="server" ImageUrl="~/img/delete4_img.png" OnClick="buttonReset_Click" style="color: white; background-color: transparent" />
                                    
                            <%--  <ui:UIButton runat="server" ID="buttonReset" ImageUrl="~/img/delete1_img.png"
                                OnClick="buttonReset_Click" ConfirmText="" meta:resourcekey="buttonResetResource" >
                            </ui:UIButton>--%>
                        </span>
                    </td>
                    <td></td>
                </tr>
            </table>
        </asp:Panel>
            
        <asp:HiddenField ID="hiddenRefresh" runat="server" meta:resourcekey="hiddenRefreshResource" ></asp:HiddenField>
        
    </div>
    <div style="clear: both"></div>

</div>
<div id="mdl_toast" class="mdl-js-snackbar mdl-snackbar">
    <div class="mdl-snackbar__text"></div>
    <button class="mdl-snackbar__action" type="button"></button>
</div>
<asp:Label runat='server' ID='labelMessage' meta:resourcekey="labelMessageResource1" Visible="false"></asp:Label>

<script type="text/javascript">
    var toggleActionSheet = function () {
        if ($(window).width() < 960) {
            $('.mdl-title-bar-toggle-extra-buttons').show();
            $('.mdl-title-bar-extra-buttons').hide();
        }
        else {
            $('.mdl-title-bar-toggle-extra-buttons').hide();
            $('.mdl-title-bar-extra-buttons').show();
        }
    };
    $(window).resize(toggleActionSheet);
</script>