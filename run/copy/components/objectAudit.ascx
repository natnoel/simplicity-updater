<%@ Control Language="C#" ClassName="objectAudit" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.Collections.ObjectModel" %>
<%@ Import Namespace="System.Drawing.Imaging" %>
<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="System.Web.UI" %>
<%@ Import Namespace="System.Web.UI.WebControls" %>
<%@ Import Namespace="System.Workflow.ComponentModel" %>
<%@ Import Namespace="System.Workflow.ComponentModel.Design" %>
<%@ Import Namespace="System.Workflow.Activities" %>
<%@ Import Namespace="System.Workflow.Runtime" %>
<%@ Import Namespace="System.ComponentModel" %>
<%@ Import Namespace="System.ComponentModel.Design" %>
<%@ Import Namespace="System.ComponentModel.Design.Serialization" %>
<%@ Import Namespace="Anacle.DataFramework" %>
<%@ Import Namespace="Anacle.WorkflowFramework" %>
<%@ Import Namespace="LogicLayer" %>
<%@ Register Src="objectPanel.ascx" TagName="object" TagPrefix="web2" %>


<script runat="server">
    /// <summary>
    /// Finds and returns the objectPanel object.
    /// </summary>
    /// <param name="c">The control within which to find.</param>
    /// <returns>The objectPanel.ascx object.</returns>
    protected objectPanel getPanel(Control c)
    {
        if (c.GetType() == typeof(objectPanel))
            return (objectPanel)c;
        foreach (Control child in c.Controls)
        {
            objectPanel o = getPanel(child);
            if (o != null)
                return o;
        }
        return null;
    }

    /// <summary>
    /// Initializes the control.
    /// </summary>
    /// <param name="e"></param>
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        objectPanel panel = getPanel(Page);
        panel.PopulateForm += new EventHandler(panel_PopulateForm);

        if (!(panel.SessionObject is LogicLayerWorkflowPersistentObject))
        {
            labelAssignedUserNames.PropertyName = "";
            labelAssignedUserPositions.PropertyName = "";
        }
    }

    
    /// <summary>
    /// Updates the audit information.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void panel_PopulateForm(object sender, EventArgs e)
    {
        objectPanel panel = getPanel(Page);
        string CreatedByDisplay = "";
        string ModifiedByDisplay = "";

        PersistentObject o = (PersistentObject)panel.SessionObject;
        if (o.CreatedDateTime != null)
        {
            if (o.CreatedDateTime < DateTime.Now)
                CreatedByDisplay = String.Format(Resources.Strings.Audit_DateTime, o.CreatedUser, o.CreatedDateTime.Value.ToFriendlyString());
            else
                CreatedByDisplay = String.Format(Resources.Strings.Audit_DateTime, o.CreatedUser, DateTime.Now.AddMilliseconds(-100).ToFriendlyString());
        }
        LabelCreatedByDisplay.Text = CreatedByDisplay;

        if (o.ModifiedDateTime != null)
        {
            if (o.ModifiedDateTime < DateTime.Now)
                ModifiedByDisplay = String.Format(Resources.Strings.Audit_DateTime, o.ModifiedUser, o.ModifiedDateTime.Value.ToFriendlyString());
            else
                ModifiedByDisplay = String.Format(Resources.Strings.Audit_DateTime, o.ModifiedUser, DateTime.Now.AddMilliseconds(-100).ToFriendlyString());
        }
        LabelModifiedByDisplay.Text = ModifiedByDisplay;
    }
    
    /// <summary>
    /// Hides/shows controls.
    /// </summary>
    /// <param name="e"></param>
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        objectPanel panel = getPanel(Page);
        if (panel.SessionObject is PersistentObject)
            panelAuditInformation.Visible = !((PersistentObject)panel.SessionObject).IsNew;
        
        labelAssignedUserPositions.Visible = labelAssignedUserPositions.Text.Trim() != "";
        labelAssignedUserNames.Visible = labelAssignedUserNames.Text.Trim() != "";
    }
</script>

<ui:UIPanel runat="server" ID="panelAuditInformation" BorderStyle="NotSet" meta:resourcekey="panelAuditInformationResource" >
    <div style="clear: both" class='mdl-audit'>
    <div style="clear: both" class='mdl-audit-int'>
    <div style='clear:both'></div>
    <ui:UIFieldLabel runat="server" ID="LabelCreatedByDisplay" Caption="First Created By" CaptionWidth="150px" meta:resourcekey="LabelCreatedByDisplayResource1"></ui:UIFieldLabel>
    <ui:UIFieldLabel runat="server" ID="LabelModifiedByDisplay" Caption="Last Modified By" CaptionWidth="150px" meta:resourcekey="LabelModifiedByDisplayResource1"></ui:UIFieldLabel>
    <ui:UIFieldLabel runat="server" ID="labelAssignedUserNames" Caption="Assigned User(s)" CaptionWidth="150px"
        PropertyName="AssignedUserNames" 
        meta:resourcekey="labelAssignedUserNamesResource1" DataFormatString="" />
    <ui:UIFieldLabel runat="server" ID="labelAssignedUserPositions" Caption="Assigned Position(s)" CaptionWidth="150px"
        PropertyName="AssignedUserPositionsWithUserNames" Visible='False' 
        meta:resourcekey="labelAssignedUserPositionsResource1" DataFormatString="" />
    <div style='clear:both'></div>
    </div>
    </div>
</ui:UIPanel>
