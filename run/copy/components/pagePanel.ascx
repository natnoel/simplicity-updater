<%@ Control Language="C#" ClassName="pagePanel" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="System.Web.UI" %>
<%@ Import Namespace="System.Web.UI.WebControls" %>
<%@ Import Namespace="System.ComponentModel" %>
<%@ Import Namespace="Anacle.DataFramework" %>

<script runat="server">
    [Localizable(true)]
    public string Caption
    {
        get
        {
            return labelCaption.Text;
        }
        set
        {
            labelCaption.Text = value;
        }
    }

    [Localizable(true)]
    public string Message
    {
        get
        {
            return labelMessage.Text;
        }
        set
        {
            labelMessage.Text = value;

            if (value != null && value.Trim() != "")
                messageChanged = true;

        }
    }


        /// <summary>
    /// The ID of the UIGridView control to which all results will be bound.
    /// </summary>
    protected string tabStripID = "";


    /// <summary>
    /// Gets or sets the ID of the UITabStrip control that will be tabbed back and forth when the previous/next buttons
    /// are clicked.
    /// </summary>
    [DefaultValue(""), Localizable(false), IDReferenceProperty]
    [Description("Gets or sets the ID of the UITabStrip control that will be tabbed back and forth when the previous/next buttons are clicked.")]
    public string TabStripID
    {
        get
        {
            return tabStripID;
        }
        set
        {
            tabStripID = value;
        }
    }


    /// <summary>
    /// The UITabStrip control that will be tabbed back and forth when the previous/next buttons
    /// are clicked.
    /// </summary>
    [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public UITabStrip TabStrip
    {
        get
        {
            /*
            Control c = null;
            if (NamingContainer != null)
                c = NamingContainer.FindControl(tabStripID);
            if (c == null)
                c = Page.FindControl(tabStripID);
            if (c is UITabStrip)
                return (UITabStrip)c;
            else
                throw new Exception("There are no UITabStrips with the ID '" + tabStripID + "'");
             * */
            // Finds the first UITabStrip
            // 
            return GetTabStrip(this.Page);
        }
    }





    protected override void OnInit(EventArgs e)
    {
        UIButton1.Click += new EventHandler(UIButton1_Click);
        UIButton2.Click += new EventHandler(UIButton2_Click);
        UIButton3.Click += new EventHandler(UIButton3_Click);
        UIButton4.Click += new EventHandler(UIButton4_Click);

        base.OnInit(e);
    }



    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

    }

    //--------------------------------------------
    // button1
    //--------------------------------------------
    [Localizable(true)]
    public string Button1_Caption
    {
        get { return UIButton1.Text; }
        set { UIButton1.Text = value; }
    }

    public string Button1_ImageUrl
    {
        get { return UIButton1.ImageUrl; }
        set { UIButton1.ImageUrl = value; }
    }

    public string Button1_CommandName
    {
        get { return UIButton1.CommandName; }
        set { UIButton1.CommandName = value; }
    }

    public bool Button1_CausesValidation
    {
        get { return UIButton1.CausesValidation; }
        set { UIButton1.CausesValidation = value; }
    }

    [Localizable(true)]
    public string Button1_ConfirmText
    {
        get { return UIButton1.ConfirmText; }
        set { UIButton1.ConfirmText = value; }
    }
    

    //--------------------------------------------
    // button2
    //--------------------------------------------
    [Localizable(true)]
    public string Button2_Caption
    {
        get { return UIButton2.Text; }
        set { UIButton2.Text = value; }
    }

    public string Button2_ImageUrl
    {
        get { return UIButton2.ImageUrl; }
        set { UIButton2.ImageUrl = value; }
    }

    public string Button2_CommandName
    {
        get { return UIButton2.CommandName; }
        set { UIButton2.CommandName = value; }
    }

    public bool Button2_CausesValidation
    {
        get { return UIButton2.CausesValidation; }
        set { UIButton2.CausesValidation = value; }
    }

    [Localizable(true)]
    public string Button2_ConfirmText
    {
        get { return UIButton2.ConfirmText; }
        set { UIButton2.ConfirmText = value; }
    }
    

    //--------------------------------------------
    // button3
    //--------------------------------------------
    [Localizable(true)]
    public string Button3_Caption
    {
        get { return UIButton3.Text; }
        set { UIButton3.Text = value; }
    }

    public string Button3_ImageUrl
    {
        get { return UIButton3.ImageUrl; }
        set { UIButton3.ImageUrl = value; }
    }

    public string Button3_CommandName
    {
        get { return UIButton3.CommandName; }
        set { UIButton3.CommandName = value; }
    }

    public bool Button3_CausesValidation
    {
        get { return UIButton3.CausesValidation; }
        set { UIButton3.CausesValidation = value; }
    }

    [Localizable(true)]
    public string Button3_ConfirmText
    {
        get { return UIButton3.ConfirmText; }
        set { UIButton3.ConfirmText = value; }
    }
    
   

    //--------------------------------------------
    // button4
    //--------------------------------------------
    [Localizable(true)]
    public string Button4_Caption
    {
        get { return UIButton4.Text; }
        set { UIButton4.Text = value; }
    }

    public string Button4_ImageUrl
    {
        get { return UIButton4.ImageUrl; }
        set { UIButton4.ImageUrl = value; }
    }

    public string Button4_CommandName
    {
        get { return UIButton4.CommandName; }
        set { UIButton4.CommandName = value; }
    }

    public bool Button4_CausesValidation
    {
        get { return UIButton4.CausesValidation; }
        set { UIButton4.CausesValidation = value; }
    }

    [Localizable(true)]
    public string Button4_ConfirmText
    {
        get { return UIButton4.ConfirmText; }
        set { UIButton4.ConfirmText = value; }
    }
    
    
    
    void UIButton4_Click(object sender, EventArgs e)
    {
        if (Click != null)
            Click(this, this.Button4_CommandName);
    }

    void UIButton3_Click(object sender, EventArgs e)
    {
        if (Click != null)
            Click(this, this.Button3_CommandName);
    }

    void UIButton2_Click(object sender, EventArgs e)
    {
        if (Click != null)
            Click(this, this.Button2_CommandName);
    }

    void UIButton1_Click(object sender, EventArgs e)
    {
        if (Click != null)
            Click(this, this.Button1_CommandName);
    }


    /// <summary>
    /// A flag that indicates whether the message text was changed.
    /// </summary>
    private bool messageChanged = false;



    UITabStrip firstTabstrip = null;


    /// <summary>
    /// Finds the first UITabStrip available.
    /// </summary>
    /// <param name="c"></param>
    /// <returns></returns>
    protected UITabStrip GetTabStrip(Control c)
    {
        if (firstTabstrip != null)
            return firstTabstrip;

        if (c is UITabStrip)
        {
            firstTabstrip = (UITabStrip)c;
            return (UITabStrip)c;
        }

        foreach (Control child in c.Controls)
        {
            UITabStrip tabStrip = GetTabStrip(child);
            if (tabStrip != null)
                return tabStrip;
        }
        return null;
    }




    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        UIButton1.Visible = Button1_CommandName.Trim() != "";
        UIButton2.Visible = Button2_CommandName.Trim() != "";
        UIButton3.Visible = Button3_CommandName.Trim() != "";
        UIButton4.Visible = Button4_CommandName.Trim() != "";

        UITabStrip tabstrip = GetTabStrip(this.Page);
        if (tabstrip != null)
        {
            // Generate the dropdown list for the responsive tab.
            //
            dropTab.Items.Clear();
            //if (!IsPostBack)
            {
                var tabViews = tabstrip.TabViews;
                int itemCount = 0;
                foreach (UITabView tabView in tabViews)
                {
                    if (tabView.Visible)
                    {
                        dropTab.Items.Add(new ListItem(tabView.Caption.Replace("<sup>", "(").Replace("</sup>", ")"), tabView.TabIndex.ToString()));
                        if (tabView.TabIndex == tabstrip.SelectedIndex)
                            dropTab.SelectedIndex = itemCount;
                        itemCount++;
                    }
                }
            }
        }

        if (messageChanged)
            Window.WriteStartupJavascript(@"showToast('mdl_toast', '" + labelMessage.Text.Replace("'", "\\'").Replace("\"", "\\\"") + @"', 'OK')");

        Window.WriteStartupJavascript("toggleActionSheet();");

    }


    public delegate void PagePanelClickEventHandler(object sender, string commandName);


    [Browsable(true)]
    public event PagePanelClickEventHandler Click;


    protected void dropTab_SelectedIndexChanged(object sender, EventArgs e)
    {
        UITabStrip tabStrip = this.TabStrip;

        if (tabStrip != null)
        {
            int tabIndex = Convert.ToInt32(dropTab.SelectedValue);

            var tabViews = tabStrip.TabViews;
            var tabView = tabViews.Find(x => x.TabIndex == tabIndex);

            if (tabView != null)
                tabStrip.ChangeTab(tabView, true);
        }
    }

</script>

<div class="mdl-title-bar-spacer">
    <div class="mdl-title-bar">
        <div>

            <%-- Title --%>
            <div class="mdl-title-bar-title-and-tab">
                <asp:Label ID="labelCaption" runat="server" CssClass="mdl-title-bar-label" meta:resourcekey="labelCaptionResource1"
                    Text="Object Name" ></asp:Label>
                <asp:DropDownList runat="server" ID="dropTab" CssClass="mdl-title-bar-tab" AutoPostBack="true" OnSelectedIndexChanged="dropTab_SelectedIndexChanged"></asp:DropDownList>
            </div>

            <%-- Show action sheet button --%>
            <div style="float:right" class="mdl-title-bar-toggle-extra-buttons">
                <ui:UIButton runat="server" ID="buttonExpandActionSheet" IconCssClass="material-icons" IconText="arrow_drop_down" ButtonType="RoundMiniFlat" ButtonColor="Default" OnClickScript="$('.mdl-title-bar-extra-buttons').toggle(300)" DisableAfterClick="false" DoPostBack="false" />
            </div>

            <%-- Buttons --%>
            <div style="float: right;" class="mdl-title-bar-extra-buttons">
                <div>
                <ui:UIButton runat="server" ID="UIButton1" CssClass="btnblack"  />
                <ui:UIButton runat="server" ID="UIButton2" CssClass="btnblack"  />
                <ui:UIButton runat="server" ID="UIButton3" CssClass="btnblack"  />
                <ui:UIButton runat="server" ID="UIButton4" CssClass="btnblack"  />
                </div>
            </div>

            <div style="clear:both"></div>

        </div>
    </div>
</div>

<div id="mdl_toast" class="mdl-js-snackbar mdl-snackbar">
    <div class="mdl-snackbar__text"></div>
    <button class="mdl-snackbar__action" type="button"></button>
</div>
<asp:Label runat='server' ID='labelMessage' meta:resourcekey="labelMessageResource1" Visible="false"></asp:Label>

<script type="text/javascript">
    var toggleActionSheet = function () {
        if ($(window).width() < 960) {
            $('.mdl-title-bar-toggle-extra-buttons').show();
            $('.mdl-title-bar-extra-buttons').hide();
        }
        else {
            $('.mdl-title-bar-toggle-extra-buttons').hide();
            $('.mdl-title-bar-extra-buttons').show();
        }
    }
    $(window).resize(toggleActionSheet)
</script>




