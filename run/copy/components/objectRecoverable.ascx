﻿<%@ Control Language="C#" ClassName="objectRecoverable" %>
<%@ Import Namespace="System" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="System.Web.UI" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Web.UI.WebControls" %>
<%@ Import Namespace="System.ComponentModel" %>
<%@ Import Namespace="Anacle.DataFramework" %>
<%@ Import Namespace="LogicLayer" %>
<%@ Import Namespace="System.Reflection" %>

<%@ Register TagPrefix="internalWeb" TagName="subpanel" Src="objectSubPanel.ascx" %>
<%@ Register TagPrefix="internalWeb" TagName="glaccount" Src="objectGLAccount.ascx" %>
<%@ Register Src="objectPanel.ascx" TagPrefix="web2" TagName="object" %>
<%@ Assembly Name="LogicLayer" %>



<script runat="server">

    /// <summary>
    /// Finds and returns the objectPanel.ascx control.
    /// </summary>
    /// <param name="c"></param>
    /// <returns></returns>
    protected objectPanel getPanel(Control c)
    {
        if (c.GetType() == typeof(objectPanel))
            return (objectPanel)c;
        foreach (Control child in c.Controls)
        {
            objectPanel o = getPanel(child);
            if (o != null)
                return o;
        }
        return null;
    }

    private string GetRequestObjectType()
    {
        if (Request["TYPE"] == null)
            return "";
        return Security.Decrypt(Request["TYPE"]).Split(':')[0];
    }

    public bool IsDocumentProvideBusinessEntity
    {
        get
        {            
            if (ViewState["IsDocumentProvideBusinessEntity"] != null)
                return (bool)ViewState["IsDocumentProvideBusinessEntity"];
            else
                return false;
        }
        set
        {
            ViewState["IsDocumentProvideBusinessEntity"] = value;
        }
    }
 
    public Guid? BusinessEntityID
    {
        get
        {
            if (!string.IsNullOrEmpty(BusinessEntity.SelectedValue))
                return new Guid(BusinessEntity.SelectedValue);
            else
                return null;
        }
    }

    public bool Enabled
    {
        get { return RecoverableMainPanel.Enabled; }
        set { RecoverableMainPanel.Enabled = value; }
    }
    
    public bool BusinessEntityVisible
    {
        get { return BusinessEntity.Visible; }
        set { BusinessEntity.Visible = value; }
    }
    
    public bool BusinessEntityEnabled
    {
        get { return BusinessEntity.Enabled; }
        set { BusinessEntity.Enabled = value; }
    }
    
    public string BusinessEntity_ErrorMessage
    {
        get { return BusinessEntity.ErrorMessage; }
        set { BusinessEntity.ErrorMessage = value; }
    }
    
    public string RecoverableItemsPropertyName
    {
        get { return RecoverableItems.PropertyName; }
        set { RecoverableItems.PropertyName = value; }
    }

    public string RecoverableItems_ErrorMessage
    {
        get { return RecoverableItems.ErrorMessage; }
        set { RecoverableItems.ErrorMessage = value; }
    }

    public bool RevenueAccountID_Visible
    {
        get { return RevenueAccountID.Visible; }
        set { RevenueAccountID.Visible = value; }
    }

    public bool RevenueAccountID_Enabled
    {
        get { return RevenueAccountID.Enabled; }
        set { RevenueAccountID.Enabled = value; }
    }

    public bool RevenueAccountID_GLSegmentValue1Enabled
    {
        get { return RevenueAccountID.GLSegmentValue1Enabled; }
        set { RevenueAccountID.GLSegmentValue1Enabled = value; }
    }

    public bool RevenueAccountID_ValidateRequiredField
    {
        get { return RevenueAccountID.ValidateRequiredField; }
        set { RevenueAccountID.ValidateRequiredField = value; }
    }

    public string RevenueAccountID_ErrorMessage
    {
        get { return RevenueAccountID.ErrorMessage; }
        set { RevenueAccountID.ErrorMessage = value; }
    }
       
    public void InitiateRecoverable(Guid? businessEntityID, 
        bool isDocumentProvideBusinessEntity)
    {
        objectPanel panel = getPanel(this.Page);
        LogicLayerPersistentObject parentObject = panel.SessionObject as LogicLayerPersistentObject;
        
        PropertyInfo recoverableMode = parentObject.GetType().GetProperty("RecoverableMode");        
        PropertyInfo recoverables = parentObject.GetType().GetProperty("RecoverableItems");
        DataListBase listRecs;
        DataList<ORecoverable> recs = null;
        bool isAllItemSameCurrency = true;
        bool isUsingBaseCurrency = true;
        if (recoverables != null)
        {
            listRecs = recoverables.GetValue(parentObject, null) as DataListBase;
            if (listRecs != null)
            {
                recs = (DataList<ORecoverable>)listRecs;
                Guid? currencyID = null;
                foreach(ORecoverable item in recs)
                {
                    if (currencyID == null)
                        currencyID = item.CurrencyID;
                    else
                        if (item.CurrencyID != currencyID)                        
                            isAllItemSameCurrency = false;

                    if (item.CurrencyID != OApplicationSetting.Current.BaseCurrencyID)
                        isUsingBaseCurrency = false;
                }
            }
        }
        
        IsDocumentProvideBusinessEntity = isDocumentProvideBusinessEntity;        
        OGLSegmentValue businessEntity = TablesLogic.tGLSegmentValue[businessEntityID];
        List<OGLSegmentValue> accessibleBE = OGLSegmentValue.GetAccessibleSegmentValue(AppSession.User,
            businessEntityID,
            GetRequestObjectType(),
            GLSegmentTypeCode.BusinessEntity,
            false);
        BusinessEntity.Bind(accessibleBE, "SegmentCodeAndName", "ObjectID");

        if (isDocumentProvideBusinessEntity && businessEntityID != null)
            BusinessEntity.SelectedValue = businessEntityID.ToString();        
        else if (recs != null && recs.Count > 0)        
            BusinessEntity.SelectedValue = recs[0].BusinessEntityID.ToString();

        UIGridViewBoundColumn colAmount = (UIGridViewBoundColumn)RecoverableItems.GetColumnByID("colAmount");
        UIGridViewBoundColumn colAmountInSelectedCurrency = (UIGridViewBoundColumn)RecoverableItems.GetColumnByID("colAmountInSelectedCurrency");
        UIGridViewBoundColumn colTaxAmount = (UIGridViewBoundColumn)RecoverableItems.GetColumnByID("colTaxAmount");
        UIGridViewBoundColumn colTaxAmountInSelectedCurrency = (UIGridViewBoundColumn)RecoverableItems.GetColumnByID("colTaxAmountInSelectedCurrency");
        UIGridViewBoundColumn colAmountIncludingTax = (UIGridViewBoundColumn)RecoverableItems.GetColumnByID("colAmountIncludingTax");
        UIGridViewBoundColumn colAmountIncludingTaxInSelectedCurrency = (UIGridViewBoundColumn)RecoverableItems.GetColumnByID("colAmountIncludingTaxInSelectedCurrency");

        colAmount.HeaderText = string.Format(Resources.Strings.General_Finance_AmountAndSymbol, OApplicationSetting.Current.BaseCurrency.CurrencySymbol);
        colTaxAmount.HeaderText = string.Format(Resources.Strings.General_Finance_TaxAmountAndSymbol, OApplicationSetting.Current.BaseCurrency.CurrencySymbol);
        colAmountIncludingTax.HeaderText = string.Format(Resources.Strings.General_Finance_AmountIncludingTaxAndSymbol, OApplicationSetting.Current.BaseCurrency.CurrencySymbol);            

        if(isAllItemSameCurrency)
        {
            if(isUsingBaseCurrency)
            {
                colAmount.Visible = colTaxAmount.Visible = colAmountIncludingTax.Visible = true;
                colAmountInSelectedCurrency.Visible = colTaxAmountInSelectedCurrency.Visible
                 = colAmountIncludingTaxInSelectedCurrency.Visible = false;
            }
            else
            {
                colAmount.Visible = colAmountInSelectedCurrency.Visible = colTaxAmount.Visible = colTaxAmountInSelectedCurrency.Visible
                    = colAmountIncludingTax.Visible = colAmountIncludingTaxInSelectedCurrency.Visible = true;
            }
        }
        else
        {
            colAmount.Visible = colAmountInSelectedCurrency.Visible = colTaxAmount.Visible = colTaxAmountInSelectedCurrency.Visible
                = colAmountIncludingTax.Visible = colAmountIncludingTaxInSelectedCurrency.Visible = true;            
        }
        
        // Only show/enable the Send Recoverable for Billing if the document is using Add Recoverable after approval mode
        //
        if(recoverableMode != null)
        {
            int? recoverableModeValue = (int?)recoverableMode.GetValue(parentObject);
            RecoverableItems.Commands[1].Button.Enabled
                = RecoverableItems.Commands[1].Visible
                = recoverableModeValue == RecoverableModes.AddRecoverableAfterApproved;                             
        }        
    }   

    protected void Item_SubPanel_PopulateForm(object sender, EventArgs e)
    {
        ORecoverable o = Item_SubPanel.SessionObject as ORecoverable;
        bool isUsingForeignCurrency = o.CurrencyID != OApplicationSetting.Current.BaseCurrencyID;

        Amount.Caption = string.Format(Resources.Strings.General_Finance_AmountAndSymbol, OApplicationSetting.Current.BaseCurrency.CurrencySymbol);
        TaxAmount.Caption = string.Format(Resources.Strings.General_Finance_TaxAmountAndSymbol, OApplicationSetting.Current.BaseCurrency.CurrencySymbol);
        AmountIncludingTax.Caption = string.Format(Resources.Strings.General_Finance_AmountIncludingTaxAndSymbol, OApplicationSetting.Current.BaseCurrency.CurrencySymbol);

        AmountInSelectedCurrency.Caption = string.Format(Resources.Strings.General_Finance_AmountAndSymbol, o.Currency.CurrencySymbol);        
        TaxAmountInSelectedCurrency.Caption = string.Format(Resources.Strings.General_Finance_TaxAmountAndSymbol, o.Currency.CurrencySymbol);
        AmountIncludingTaxInSelectedCurrency.Caption = string.Format(Resources.Strings.General_Finance_AmountIncludingTaxAndSymbol, o.Currency.CurrencySymbol);
        UnitPriceInSelectedCurrency.Caption = string.Format(Resources.Strings.General_Finance_UnitPriceAndSymbol, o.Currency.CurrencySymbol);
        InventorySellingPrice.Caption = string.Format(Resources.Strings.Recoverable_InventorySellingPrice, DateTime.Today.ToString("dd-MMM-yyyy"));            

        Amount.Visible = TaxAmount.Visible = AmountIncludingTax.Visible = isUsingForeignCurrency;

        ARChargeTypeID.Bind(OARChargeType.GetAllARChargeTypes(o.BusinessEntityID, o.ARChargeTypeID));
        PopulateCustomerAccountDDL(o, o.CustomerAccountID);
        TaxCodeID.Bind(OBusinessEntitySettings.GetTaxCodes(o.BusinessEntityID, o.TaxCodeID, o.OriginatingDocumentDate, GetTaxCodeOptions.BothAPAndARTaxCodes));

        WOEstimatedWorkCostPanel.Visible = o.OriginatingObjectTypeName == "OWork";

        InventorySellingPrice.Visible = o.Catalog != null;
        ServiceSellingPrice.Visible = o.FixRate != null;
        
        Item_SubPanel.ObjectPanel.Enabled = o.GLPostedIndicator == 0;
                 
        Item_SubPanel.ObjectPanel.BindObjectToControls(o);                
    }

    /// <summary>
    /// Validate and update Recoverable Item.
    /// Also update associated Purchase Budget. 
    /// But need the associated Document to have Purchase Budget List as "PurchaseBudgets" Property.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Item_SubPanel_ValidateAndUpdate(object sender, EventArgs e)
    {
        ORecoverable o = Item_SubPanel.SessionObject as ORecoverable;
        Item_SubPanel.ObjectPanel.BindControlsToObject(o);
        objectPanel panel = getPanel(this.Page);
        LogicLayerPersistentObject parentObject = panel.SessionObject as LogicLayerPersistentObject;

        if (o.RevenueAccount != null && o.RevenueAccount.SegmentValue3 != null &&
            o.RevenueAccount.SegmentValue3.Type == GLSegmentItemType.Category)
        {
            RevenueAccountID.ErrorMessage = String.Format(Resources.Errors.Recoverable_CannotHaveCategoryRevenueAccount, o.RevenueAccount.SegmentValue3.ObjectNumber);
            return;
        }
     
        PropertyInfo pi = parentObject.GetType().GetProperty("PurchaseBudgets");
        if (pi != null)
        {
            DataListBase dataListBase = pi.GetValue(parentObject, null) as DataListBase;

            if (dataListBase != null)
            {
                if (o.PurchaseBudgets != null && o.PurchaseBudgets.Count > 0)
                {
                    foreach (OPurchaseBudget pb in o.PurchaseBudgets)
                        dataListBase.RemoveGuid(pb.ObjectID.Value);
                    o.PurchaseBudgets.Clear();
                }
                if(o.RecoverableMode == RecoverableModes.AddRecoverableBeforeApproval)
                {
                    OPurchaseBudget purchaseBudget = o.CreatePurchaseBudget(null);
                    if (purchaseBudget != null)
                    {
                        o.PurchaseBudgets.Add(purchaseBudget);
                        dataListBase.AddObject(purchaseBudget);
                    }
                }                
            }                
        }        
        
        if (!Item_SubPanel.ObjectPanel.IsValid)
            return;

        Item_SubPanel.ObjectPanel.BindObjectToControls(o);                
        panel.ObjectPanel.BindObjectToControls(parentObject);        
    }

    protected void RecoverableMainPanel_PreRender(object sender, EventArgs e)
    {
        BusinessEntity.Enabled = !IsDocumentProvideBusinessEntity && RecoverableItems.Rows.Count == 0;
        RevenueAccountID.Enabled = !string.IsNullOrEmpty(CustomerSelection.SelectedValue) && !string.IsNullOrEmpty(CustomerAccount.SelectedValue);               
    }

    /// <summary>
    /// Delete the selected Recoverable Item(s), and also delete the related Purchase Budget(s)
    /// But depends on the documents to have 2 list of Recoverable Item(s) and Purchase Budget(s)
    /// as "PurchaseBudgets" and "RecoverableItems".
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="commandName"></param>
    /// <param name="dataKeys"></param>
    protected void RecoverableItems_Action(object sender, string commandName, List<object> dataKeys)
    {        
        if (commandName == "DeleteObject")
        {        
            objectPanel panel = getPanel(this.Page);
            LogicLayerPersistentObject parentObject = panel.SessionObject as LogicLayerPersistentObject;

            PropertyInfo pbs = parentObject.GetType().GetProperty("PurchaseBudgets");
            PropertyInfo recoverables = parentObject.GetType().GetProperty("RecoverableItems");
            if (recoverables != null && pbs != null)
            {
                DataListBase listPB = pbs.GetValue(parentObject, null) as DataListBase;
                DataListBase listRecs = recoverables.GetValue(parentObject, null) as DataListBase;

                if (listPB != null)
                {
                    foreach (Guid key in dataKeys)
                    {
                        ORecoverable item = (ORecoverable)listRecs.FindObject(key);
                        if(item.PurchaseBudgets != null && item.PurchaseBudgets.Count > 0)
                        {
                            foreach(OPurchaseBudget pb in item.PurchaseBudgets)
                                listPB.RemoveGuid(pb.ObjectID.Value);           
                        }                                  
                    }                        
                }
            }
            panel.ObjectPanel.BindObjectToControls(parentObject);
        }
        
        if(commandName == "PostGL")
        {
            objectPanel panel = getPanel(this.Page);
            LogicLayerPersistentObject parentObject = panel.SessionObject as LogicLayerPersistentObject;
            
            PropertyInfo recoverables = parentObject.GetType().GetProperty("RecoverableItems");
            if (recoverables != null)
            {
                DataListBase listRecs = recoverables.GetValue(parentObject, null) as DataListBase;
                List<ORecoverable> listRecoverables = new List<ORecoverable>();
                using (Connection c = new Connection())
                {
                    foreach (Guid key in dataKeys)
                    {
                        ORecoverable item = (ORecoverable)listRecs.FindObject(key);
                        if(item != null)
                        {
                            if (item.RevenueAccount == null ||
                                item.Customer == null ||
                                item.CustomerAccount == null ||
                                (item.Amount ?? 0) == 0)
                            {
                                RecoverableItems.ErrorMessage = string.Format(Resources.Errors.Recoverable_NotEnoughInformation, item.OriginatingDocumentItemNumber);                                    
                                return;
                            }
                            listRecoverables.Add(item);
                            if(item.OriginatingObjectTypeName == "OWork")
                            {
                                OWorkCost wc = TablesLogic.tWorkCost[item.OriginatingDocumentItemID];
                                if (wc != null)
                                {
                                    item.StampOriginatingDocumentNumberAndItemNumber(parentObject.ObjectNumber, wc.DisplayOrder);
                                    item.StampSourceDocumentNumberAndItemNumber(parentObject.ObjectNumber, wc.DisplayOrder);
                                }
                            }                                                     
                        }
                    }                    
                    parentObject.Save();

                    try
                    {
                        ORecoverable.PostEntriesFromAPSide(listRecoverables,
                            DateTime.Today,
                            parentObject.ObjectID.Value,
                            parentObject.ObjectNumber,
                            parentObject.ObjectTypeName,
                            false);
                    }
                    catch(Exception ex)
                    {
                        RecoverableItems.ErrorMessage = ex.Message;
                    }
                    
                    c.Commit();                    
                }                
            }
            
            // TDT: 15-Jun-2016
            // To follow the current design of Object Panel, reload the page to get new instance ID
            //
            Window.OpenEditObjectPage(this.Page, parentObject.ObjectTypeName, parentObject.ObjectID.ToString(),
                QueryString.New("TAB", Security.Encrypt(panel.TabStrip.SelectedIndex.ToString())));       
        }
    }

    protected void BusinessEntity_SelectedIndexChanged(object sender, EventArgs e)
    {
       
    }

    protected void AmountInSelectedCurrency_TextChanged(object sender, EventArgs e)
    {
        ORecoverable item = Item_SubPanel.SessionObject as ORecoverable;
        Item_SubPanel.ObjectPanel.BindControlsToObject(item);
        item.TaxAmountInSelectedCurrency = OTaxCode.GetTaxAmount(item.AmountInSelectedCurrency, item.TaxCodeID, item.OriginatingDocumentDate) ?? 0;
        item.AmountIncludingTaxInSelectedCurrency = item.AmountInSelectedCurrency + item.TaxAmountInSelectedCurrency;
        item.UpdateBaseAmountFromAmountInSelectedCurrency();
        Item_SubPanel.ObjectPanel.BindObjectToControls(item);
    }

    protected void RevenueAccountID_ChangeGLAccountButtonClicked(object sender, ButtonClickedEventArgs e)
    {
        ORecoverable item = Item_SubPanel.SessionObject as ORecoverable;
        Item_SubPanel.ObjectPanel.BindControlsToObject(item);

        if (item.OriginatingDocumentDate != null && item.RevenueAccount != null)
        {
            RevenueAccountID.PopulateGLSegmentValues(item.OriginatingDocumentDate.Value,
                item.RevenueAccount.SegmentValue1ID,
                item.RevenueAccount.SegmentValue2ID,
                item.RevenueAccount.SegmentValue3ID,
                item.RevenueAccount.SegmentValue4ID,
                item.RevenueAccount.SegmentValue5ID,
                item.RevenueAccount.SegmentValue6ID,
                item.RevenueAccount.SegmentValue7ID,
                item.RevenueAccount.SegmentValue8ID
                );
        }
        else if (item.OriginatingDocumentDate != null)
        {           
            Guid? costCenterID = null;
            Guid? naturalAccountID = null;
            Guid? activityID = null;

            if (item.ARChargeType != null)
                naturalAccountID = item.ARChargeType.NaturalAccountID;

            OGLSegmentValue.PopulateCostCentreAndActivityForCustomer(item.ARChargeType, item.CustomerAccount, item.BusinessEntityID, out costCenterID, out activityID);
            
            RevenueAccountID.PopulateGLSegmentValuesByBusinessEntityCostCentreAndNaturalAccount(
                item.OriginatingDocumentDate.Value,
                item.BusinessEntityID, 
                costCenterID, 
                naturalAccountID,
                activityID);
        }      
    }

    protected void CustomerSelection_SelectedValueChanged(object sender, EventArgs e)
    {
        ORecoverable item = Item_SubPanel.SessionObject as ORecoverable;
        Item_SubPanel.ObjectPanel.BindControlsToObject(item);
        
        item.CustomerAccountID = null;               
        PopulateCustomerAccountDDL(item, null);        

        Item_SubPanel.ObjectPanel.BindObjectToControls(item);
    }

    protected void CustomerSelection_Search(object sender, SearchEventArgs e)
    {
        e.CustomCondition = (TablesLogic.tCustomer.ObjectID.In(
                                TablesLogic.tCustomerAccount.SelectDistinct(TablesLogic.tCustomerAccount.CustomerID)
                                    .Where(TablesLogic.tCustomerAccount.CustomerID != null &
                                            TablesLogic.tCustomerAccount.IsDeleted == 0)));
    }
    
    private void PopulateCustomerAccountDDL(ORecoverable item, Guid? includingCustomerAccountID)
    {
        if (item == null)
            return;

        if (item.CustomerID == null)
        {
            CustomerAccount.Items.Clear();
            return;
        }

        List<OCustomerAccount> customerAccounts =
            OCustomerAccount.GetCustomerAccountsByCustomerID(
            item.CustomerID, includingCustomerAccountID, item.BusinessEntityID);
        CustomerAccount.Bind(customerAccounts, "CustomerAccountText", "ObjectID");
    }


    protected void CustomerAccount_SelectedIndexChanged(object sender, EventArgs e)
    {
        ORecoverable item = Item_SubPanel.SessionObject as ORecoverable;
        Item_SubPanel.ObjectPanel.BindControlsToObject(item);        
        Item_SubPanel.ObjectPanel.BindObjectToControls(item);
    }

    protected void ARChargeTypeID_SelectedIndexChanged(object sender, EventArgs e)
    {
        ORecoverable item = Item_SubPanel.SessionObject as ORecoverable;
        Item_SubPanel.ObjectPanel.BindControlsToObject(item);
        item.RevenueAccountID = OGLAccount.GetGLAccountFromChargeType(item.BusinessEntityID, null,
            item.CustomerAccount, 
            item.ARChargeType, 
            item.OriginatingDocumentDate.Value);
        Item_SubPanel.ObjectPanel.BindObjectToControls(item);        
    }

    protected void TaxCodeID_SelectedIndexChanged(object sender, EventArgs e)
    {
        ORecoverable item = Item_SubPanel.SessionObject as ORecoverable;
        Item_SubPanel.ObjectPanel.BindControlsToObject(item);
        item.UpdateTax(item.TaxCodeID);        
        Item_SubPanel.ObjectPanel.BindObjectToControls(item);        
    }

    protected void RecoverableItems_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if(e.Row.RowType == DataControlRowType.DataRow)
        {
            objectPanel panel = getPanel(this.Page);
            LogicLayerPersistentObject parentObject = panel.SessionObject as LogicLayerPersistentObject;
            
            PropertyInfo recoverables = parentObject.GetType().GetProperty("RecoverableItems");
            if (recoverables != null)
            {
                DataListBase listRecs = recoverables.GetValue(parentObject, null) as DataListBase;
                ORecoverable item = (ORecoverable)listRecs.FindObject(new Guid(RecoverableItems.DataKeys[e.Row.RowIndex][0].ToString()));
                if(item.GLPostedIndicator != 0)
                {                                        
                    e.Row.Cells[0].Controls[0].Visible = false; // checkbox
                    //e.Row.Cells[1].Controls[0].Visible = false; // edit button
                    e.Row.Cells[2].Controls[0].Visible = false; // delete button
                }
            }
        }        
    }

    protected void Quantity_TextChanged(object sender, EventArgs e)
    {
        ORecoverable item = Item_SubPanel.SessionObject as ORecoverable;
        Item_SubPanel.ObjectPanel.BindControlsToObject(item);
        item.CalculateAmount();
        item.CalculateAmountInSelectedCurrency();
        item.TaxAmountInSelectedCurrency = OTaxCode.GetTaxAmount(item.AmountInSelectedCurrency, item.TaxCodeID, item.OriginatingDocumentDate) ?? 0;
        item.AmountIncludingTaxInSelectedCurrency = item.AmountInSelectedCurrency + item.TaxAmountInSelectedCurrency;
        item.UpdateBaseAmountFromAmountInSelectedCurrency();
        Item_SubPanel.ObjectPanel.BindObjectToControls(item);
    }

    protected void UnitPriceInSelectedCurrency_TextChanged(object sender, EventArgs e)
    {
        ORecoverable item = Item_SubPanel.SessionObject as ORecoverable;
        Item_SubPanel.ObjectPanel.BindControlsToObject(item);
        item.UnitPrice = OCurrency.GetBaseAmountFromAmountInSelectedCurrency(item.UnitPriceInSelectedCurrency ?? 0, item.ForeignToBaseExchangeRate ?? 0);
        item.CalculateAmount();
        item.CalculateAmountInSelectedCurrency();
        item.TaxAmountInSelectedCurrency = OTaxCode.GetTaxAmount(item.AmountInSelectedCurrency, item.TaxCodeID, item.OriginatingDocumentDate) ?? 0;
        item.AmountIncludingTaxInSelectedCurrency = item.AmountInSelectedCurrency + item.TaxAmountInSelectedCurrency;
        item.UpdateBaseAmountFromAmountInSelectedCurrency();
        Item_SubPanel.ObjectPanel.BindObjectToControls(item);
    }
    
</script>

<ui:UIPanel runat="server" ID="RecoverableMainPanel" OnPreRender="RecoverableMainPanel_PreRender">    
    <ui:UIFieldSearchableDropDownList runat="server" ID="BusinessEntity" Caption="Business Entity"        
        OnSelectedIndexChanged="BusinessEntity_SelectedIndexChanged" />
    <ui:UIGridView runat="server" ID="RecoverableItems" Caption="Recoverable Item(s)"
        PropertyName="" 
        DataKeyNames="ObjectID" AllowPaging="true" BindObjectsToRows="true"
        SortExpression="OriginatingDocumentItemNumber"
        OnAction="RecoverableItems_Action"
        OnRowDataBound="RecoverableItems_RowDataBound"
        meta:resourcekey="RecoverableItemsResource">
        <Commands>            
            <ui:UIGridViewCommand AlwaysEnabled="False" CausesValidation="False" CommandName="DeleteObject"
                CommandText="Delete Item(s)" ConfirmText="Are you sure you wish to delete the selected items?"
                ImageUrl="~/images/delete.gif" meta:resourcekey="DeleteObjectResource"/>
            <ui:UIGridViewCommand AlwaysEnabled="false" CausesValidation="False" CommandName="PostGL"
                CommandText="Send Recoverable(s) for Pending Billing"                
                ImageUrl="~/images/right.png" />            
        </Commands>
        <Columns>
            <ui:UIGridViewButtonColumn ButtonType="Image" CommandName="EditObject" ImageUrl="~/images/edit.gif"
                AlwaysEnabled="true">
                <HeaderStyle HorizontalAlign="Left" Width="16px" />
                <ItemStyle HorizontalAlign="Left" />
            </ui:UIGridViewButtonColumn>
            <ui:UIGridViewButtonColumn ButtonType="Image" CommandName="DeleteObject" ConfirmText="Are you sure you wish to delete this item?"
                ImageUrl="~/images/delete.gif">
                <HeaderStyle HorizontalAlign="Left" Width="16px" />
                <ItemStyle HorizontalAlign="Left" />
            </ui:UIGridViewButtonColumn>
            <ui:UIGridViewBoundColumn HeaderText="<%$ Resources:OriginatingDocumentItemNumberResource %>" PropertyName="OriginatingDocumentItemNumber"
                HeaderStyle-Width="50px"/>
            <ui:UIGridViewBoundColumn HeaderText="<%$ Resources:CustomerObjectNameResource %>" PropertyName="Customer.ObjectName" HeaderStyle-Width="100px" 
                />
            <ui:UIGridViewBoundColumn HeaderText="<%$ Resources:ExpenseAccountCodeTextResource %>" PropertyName="ExpenseAccountCodeText"
                HeaderStyle-Width="150px" />
            <ui:UIGridViewBoundColumn HeaderText="<%$ Resources:RevenueAccountCodeTextResource %>" PropertyName="RevenueAccountCodeText"
                HeaderStyle-Width="150px" />           
            <ui:UIGridViewBoundColumn HeaderText="<%$ Resources:OriginatingDocumentItemDescriptionResource %>" 
                PropertyName="OriginatingDocumentItemDescription"
                HeaderStyle-Width="250px" />
            <ui:UIGridViewBoundColumn HeaderText="<%$ Resources:CurrencyObjectNameResource %>" PropertyName="Currency.ObjectName" 
                />
            <ui:UIGridViewBoundColumn ID="colAmount" HeaderText="<%$ Resources:AmountResource %>" PropertyName="Amount" 
                DataFormatString="{0:#,##0.00##}" HeaderStyle-Width="100px" />
            <ui:UIGridViewBoundColumn ID="colAmountInSelectedCurrency" HeaderText="<%$ Resources:AmountInSelectedCurrencyResource %>" PropertyName="AmountInSelectedCurrency" 
                DataFormatString="{0:#,##0.00##}" HeaderStyle-Width="100px" />
            <ui:UIGridViewBoundColumn HeaderText="<%$ Resources:TaxCodeNameResource %>" PropertyName="TaxCodeName" HeaderStyle-Width="100px" 
                />
            <ui:UIGridViewBoundColumn ID="colTaxAmount" HeaderText="<%$ Resources:TaxAmountResource %>" PropertyName="TaxAmount" 
                DataFormatString="{0:#,##0.00##}" HeaderStyle-Width="100px"/>
            <ui:UIGridViewBoundColumn ID="colTaxAmountInSelectedCurrency" HeaderText="<%$ Resources:TaxAmountInSelectedCurrencyResource %>" PropertyName="TaxAmountInSelectedCurrency" 
                DataFormatString="{0:#,##0.00##}" HeaderStyle-Width="100px"/>
            <ui:UIGridViewBoundColumn ID="colAmountIncludingTax" HeaderText="<%$ Resources:AmountIncludingTaxResource %>" PropertyName="AmountIncludingTax" 
                DataFormatString="{0:#,##0.00##}" HeaderStyle-Width="100px"/>
            <ui:UIGridViewBoundColumn ID="colAmountIncludingTaxInSelectedCurrency" HeaderText="<%$ Resources:AmountIncludingTaxInSelectedCurrencyResource %>" PropertyName="AmountIncludingTaxInSelectedCurrency" 
                DataFormatString="{0:#,##0.00##}" HeaderStyle-Width="100px"/>
            <ui:UIGridViewBoundColumn HeaderText="<%$ Resources:RemarksResource %>" PropertyName="Remarks" />
            <ui:UIGridViewBoundColumn HeaderText="<%$ Resources:RecoverableStatusResource %>" PropertyName="RecoverableStatus" />
            <ui:UIGridViewBoundColumn HeaderText="<%$ Resources:GeneratedInvoiceNumberResource %>" PropertyName="GeneratedInvoiceNumber" LinkedObjectPropertyName="GeneratedInvoice" 
                />
            <ui:UIGridViewBoundColumn HeaderText="<%$ Resources:GeneratedInvoiceStatusResource %>" PropertyName="GeneratedInvoiceStatus" 
                /> 
        </Columns>
    </ui:UIGridView>
    <ui:UIDialogBox runat="server" ID="Item_DialogBox">
    <ui:UIObjectPanel ID="Item_ObjectPanel" runat="server" BorderStyle="NotSet">
        <internalWeb:subpanel ID="Item_SubPanel" runat="server" GridViewID="RecoverableItems"            
            OnPopulateForm="Item_SubPanel_PopulateForm"             
            OnValidateAndUpdate="Item_SubPanel_ValidateAndUpdate"
            UpdateAndNewButtonVisible="false" />
        <ui:UIPanel runat="server" ID="Item_Panel">

            <ui:UISeparator runat="server" ID="DetailsSep" Caption="Recoverable Details" 
                meta:resourcekey="DetailsSepResource"/>
            <ui:UIFieldLabel runat="server" ID="OriginatingDocumentItemNumber" Caption="Originating Item No." 
                PropertyName="OriginatingDocumentItemNumber" meta:resourcekey="OriginatingDocumentItemNumberResource"/>
            <ui:UIFieldLabel runat="server" ID="OriginatingDocumentItemDescription" Caption="Originating Item Desc." 
                PropertyName="OriginatingDocumentItemDescription" meta:resourcekey="OriginatingDocumentItemDescriptionResource" />
            <ui:UIFieldLabel runat="server" ID="ExpenseAccountID" Caption="Expense Account" PropertyName="ExpenseAccount.ObjectNumber" 
                 meta:resourcekey="ExpenseAccountIDResource"/>         
            
            <ui:UISeparator runat="server" ID="UISeparator1" Caption="Customer Details" 
                 meta:resourcekey="UISeparator1Resource"/>                               
            <ui:UIFieldDialogSingleSelection runat="server" ID="CustomerSelection" 
                SortExpression="ObjectName ASC, ObjectNumber ASC"
                Caption="Customer" PropertyName="CustomerID" TextPropertyName="Customer.ObjectName"
                SearchBehavior="SearchOnDemand"
                SearchOnDemandAutoSearchOnShow="true" 
                SearchOnDemandBaseTableClassName="LogicLayer.TablesLogic"
                SearchOnDemandBaseTableName="tCustomer" 
                SearchOnDemandType="ObjectQuery" 
                SearchOnDemandPropertyNames="ObjectName, ObjectNumber"
                SearchTextBoxHint="Search using: Name, NRIC / UEN" 
                ValidateRequiredField="true" 
                ContextMenuAlwaysEnabled="true"
                OnSelectedValueChanged="CustomerSelection_SelectedValueChanged" 
                OnSearch="CustomerSelection_Search"                
                meta:resourcekey="CustomerSelectionResource" >
                <Columns>
                    <ui:UIGridViewButtonColumn ImageUrl="~/images/tick.gif" HeaderStyle-Width="30px"
                        CommandName="Select"  meta:resourcekey="CustomerSelectionUIGridViewButtonColumnSelectResource" />
                    <ui:UIGridViewBoundColumn PropertyName="CustomerTypeText" SortExpression="CustomerTypeText"
                        HeaderStyle-Width="120px" HeaderText="Customer Type"  meta:resourcekey="CustomerSelectionUIGridViewBoundColumnCustomerTypeTextResource" />
                    <ui:UIGridViewBoundColumn PropertyName="ObjectNumber" SortExpression="ObjectNumber"
                        HeaderStyle-Width="130px" HeaderText="UEN/NRIC/ID"  meta:resourcekey="CustomerSelectionUIGridViewBoundColumnObjectNumberResource" />
                    <ui:UIGridViewBoundColumn PropertyName="ObjectName" SortExpression="ObjectName" HeaderStyle-Width="400px"
                        HeaderText="Customer Name"  meta:resourcekey="CustomerSelectionUIGridViewBoundColumnObjectNameResource" />
                </Columns>
            </ui:UIFieldDialogSingleSelection>                            
            <ui:UIFieldSearchableDropDownList runat="server" ID="CustomerAccount" PropertyName="CustomerAccountID"
                Caption="Customer Account" ValidateRequiredField="true" 
                OnSelectedIndexChanged="CustomerAccount_SelectedIndexChanged"                 
                meta:resourcekey="CustomerAccountResource" />
            <ui:UIFieldSearchableDropDownList runat="server" ID="ARChargeTypeID"  
                Caption="Charge Type" PropertyName="ARChargeTypeID"
                ValidateRequiredField="false" 
                OnSelectedIndexChanged="ARChargeTypeID_SelectedIndexChanged"
                meta:resourcekey="ARChargeTypeIDResource" />
            <internalWeb:glaccount runat="server" ID="RevenueAccountID" Caption="Revenue Account"
                GLAccountIDPropertyName="RevenueAccountID" 
                GLAccountLabelPropertyName="RevenueAccount.ObjectNumber" 
                GLSegmentValue1Enabled="false" 
                ValidateRequiredField="true" 
                OnChangeGLAccountButtonClicked="RevenueAccountID_ChangeGLAccountButtonClicked"
                meta:resourcekey="RevenueAccountIDResource" />

            <ui:UISeparator runat="server" ID="RecoverableAmountSep" Caption="Recoverable Amount Details" meta:resourcekey="RecoverableAmountSepResource"/>
            <ui:UIPanel runat="server" ID="WOEstimatedWorkCostPanel">
                <ui:UIFieldLabel runat="server" ID="WOEstimatedQuantity" Caption="Estimated Quantity" Span="Half"
                    PropertyName="WOEstimatedQuantity" DataFormatString="{0:#,##0.00}" meta:resourcekey="WOEstimatedQuantityResource"/>
                <ui:UIFieldLabel runat="server" ID="WOEstimatedUnitPrice" Caption="Estimated Unit Price" Span="Half"
                    PropertyName="WOEstimatedUnitPrice" DataFormatString="{0:#,##0.00}" meta:resourcekey="WOEstimatedUnitPriceResource"/>                
            </ui:UIPanel>
            <ui:UIPanel runat="server" ID="OriginalItemDetailPanel">
                <ui:UIFieldLabel runat="server" ID="OriginalQuantity" Caption="Original Quantity" Span="Half"
                    PropertyName="OriginalQuantity" DataFormatString="{0:#,##0.00}" meta:resourcekey="OriginalQuantityResource"/>
                <ui:UIFieldLabel runat="server" ID="OriginalUnitPrice" Caption="Original Unit Price" Span="Half"
                    PropertyName="OriginalUnitPrice" DataFormatString="{0:#,##0.00}" meta:resourcekey="OriginalUnitPriceResource"/>
                <ui:UIFieldLabel runat="server" ID="InventorySellingPrice" PropertyName="InventorySellingPrice" CaptionWidth="300px"
                    Caption="Inventory Selling Price" DataFormatString="{0:#,##0.00}" />
                <ui:UIFieldLabel runat="server" ID="ServiceSellingPrice" PropertyName="ServiceSellingPrice" 
                    Caption="Service Selling Price" DataFormatString="{0:#,##0.00}" />
            </ui:UIPanel>            
            <ui:UIFieldTextBox ID="Quantity" runat="server" Caption="Quantity" Span="Half"
                PropertyName="Quantity" 
                ValidateDataTypeCheck="True" ValidateRangeField="True"
                ValidateRequiredField="True" ValidationDataType="Double" ValidationRangeMax="99999999999999"
                ValidationRangeMin="0" ValidationRangeMinInclusive="false" ValidationRangeType="Double" DataFormatString="{0:#,##0.00##}" ValidationNumberOfDecimalPlaces="4"
                OnTextChanged="Quantity_TextChanged" meta:resourcekey="QuantityResource"/>

            <ui:UIFieldTextBox ID="UnitPriceInSelectedCurrency" runat="server" Caption="Unit Price" Span="Half"
                PropertyName="UnitPriceInSelectedCurrency" 
                ValidateDataTypeCheck="True" ValidateRangeField="True"
                ValidateRequiredField="True" ValidationDataType="Currency" ValidationRangeMax="99999999999999"
                ValidationRangeMin="0" ValidationRangeType="Currency" DataFormatString="{0:#,##0.00##}"
                OnTextChanged="UnitPriceInSelectedCurrency_TextChanged" meta:resourcekey="UnitPriceInSelectedCurrencyResource1"/>

            <ui:UIFieldTextBox ID="AmountInSelectedCurrency" runat="server" Caption="Amount" Enabled="false"
                PropertyName="AmountInSelectedCurrency" 
                ValidateDataTypeCheck="True" ValidateRangeField="True"
                ValidateRequiredField="True" ValidationDataType="Currency" ValidationRangeMax="99999999999999"
                ValidationRangeMin="0" ValidationRangeType="Currency" DataFormatString="{0:#,##0.00##}"
                OnTextChanged="AmountInSelectedCurrency_TextChanged" />
            <ui:UIFieldLabel runat="server" ID="Amount" Caption="Amount" PropertyName="Amount" DataFormatString="{0:#,##0.00##}" />
            <ui:UIFieldDropDownList runat="server" ID="TaxCodeID" Caption="Tax Code" PropertyName="TaxCodeID" ValidateRequiredField="true" OnSelectedIndexChanged="TaxCodeID_SelectedIndexChanged" 
                meta:resourcekey="TaxCodeIDResource"/>            
            <ui:UIFieldLabel runat="server" ID="TaxAmount" Caption="Tax Amount" PropertyName="TaxAmount" DataFormatString="{0:#,##0.00##}" />
            <ui:UIFieldLabel runat="server" ID="TaxAmountInSelectedCurrency" Caption="Tax Amount" PropertyName="TaxAmountInSelectedCurrency" DataFormatString="{0:#,##0.00##}" />
            <ui:UIFieldLabel runat="server" ID="AmountIncludingTax" Caption="Amount Including Tax" PropertyName="AmountIncludingTax" DataFormatString="{0:#,##0.00##}" />
            <ui:UIFieldLabel runat="server" ID="AmountIncludingTaxInSelectedCurrency" Caption="Amount Including Tax" PropertyName="AmountIncludingTaxInSelectedCurrency" DataFormatString="{0:#,##0.00##}" />
            <ui:UIFieldTextBox runat="server" ID="Remarks" Caption="Remarks" PropertyName="Remarks" meta:resourcekey="RemarksResource"/>
        </ui:UIPanel>
    </ui:UIObjectPanel>
    </ui:UIDialogBox>
</ui:UIPanel>