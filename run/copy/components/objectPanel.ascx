<%@ Control Language="C#" ClassName="objectPanel" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Reflection" %>
<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="System.Web.UI" %>
<%@ Import Namespace="System.Web.UI.WebControls" %>
<%@ Import Namespace="System.ComponentModel" %>
<%@ Import Namespace="System.Workflow.ComponentModel.Compiler" %>
<%@ Import Namespace="Anacle.DataFramework" %>
<%@ Import Namespace="LogicLayer" %>
<%@ Import Namespace="Anacle.WorkflowFramework" %>
<%--<%@ Import Namespace="TheArtOfDev.HtmlRenderer.PdfSharp" %>
<%@ Import Namespace="TheArtOfDev.HtmlRenderer" %>--%>


<%@ Register TagPrefix="cc1" Namespace="Anacle.UIFramework" Assembly="Anacle.UIFramework" %>
<script runat="server">

    /// Mobile Web
    /// <summary>
    /// Gets and sets a flag on the UIPageBase object to indicate if
    /// this page will be rendered in a mobile mode.
    /// </summary>
    public bool MobileMode
    {
        get
        {
            return ((UIPageBase)this.Page).MobileMode;
        }
        set
        {
            ((UIPageBase)this.Page).MobileMode = value;
        }
    }


    /// <summary>
    /// For DMS use: Disable all the panel buttons
    /// Added By: Yiyuan
    /// Added Date: 24-Nov-2011
    /// </summary>
    public void DisableModification()
    {
        buttonDelete.Enabled = false;
        buttOnValidateAndSave.Enabled = false;
        buttOnValidateAndSaveAndNew.Enabled = false;
        buttOnValidateAndSaveAndClose.Enabled = false;
    }

    /// <summary>
    /// SubCaption below the Object Type Name
    /// </summary>
    public string SubCaption
    {
        get
        {
            return "";
        }
        set
        {
        }
    }

    /// <summary>
    /// SubCaption Visible Flag
    /// </summary>
    public bool SubCaptionVisible
    {
        get
        {
            return false;
        }
        set
        {
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public Panel ButtonPanel
    {
        get
        {
            return ButtonContainerPanel;
        }
    }

    public bool TaskCurrentCommentsValidateRequired
    {
        get
        {
            return textTaskCurrentComments.ValidateRequiredField;
        }
        set
        {
            textTaskCurrentComments.ValidateRequiredField = value;
        }
    }

    /// <summary>
    /// Gets or sets a flag that indicates if the Close Window button
    /// is visible to the user.
    /// </summary>
    [DefaultValue(true), Localizable(false)]
    public bool CloseWindowButtonVisible
    {
        get
        {
            return buttonReturn.Visible;
        }
        set
        {
            buttonReturn.Visible = value;
        }
    }

    public bool PrevNextTabButtonsVisible
    {
        get
        {
            return PrevNextTabButtons.Visible;
        }
        set
        {
            PrevNextTabButtons.Visible = value;
        }
    }


    /// <summary>
    /// Gets or sets a flag to indicate if the Delete button is visible.
    /// </summary>
    [DefaultValue(true), Localizable(false)]
    public bool DeleteButtonVisible
    {
        get
        {
            return spanDelete2.Visible;
        }
        set
        {
            spanDelete2.Visible = value;
        }
    }

    /// <summary>
    /// Gets or sets a flag to indicate if the ShowWorkflowDialog is show before transit.
    /// </summary>
    /// 
    bool showWorkflowDialogBox = false;
    [DefaultValue(false), Localizable(false)]
    public bool ShowWorkflowDialogBox
    {
        get
        {
            return showWorkflowDialogBox;
        }
        set
        {
            showWorkflowDialogBox = value;
        }
    }



    /// <summary>
    /// Gets or sets a flag to indicate if the Save button is visible.
    /// </summary>
    [DefaultValue(false), Localizable(false)]
    public bool SaveButtonsAlwaysVisible
    {
        get
        {
            if (ViewState["SaveButtonsAlwaysVisible"] == null)
                return false;
            else
                return (bool)ViewState["SaveButtonsAlwaysVisible"];
        }
        set
        {
            ViewState["SaveButtonsAlwaysVisible"] = value;
        }
    }


    /// <summary>
    /// Gets or sets a flag to indicate if the Save button is visible.
    /// </summary>
    [DefaultValue(true), Localizable(false)]
    public bool SaveButtonsVisible
    {
        get
        {
            return spanSaveButtonsInner.Visible;
        }
        set
        {
            spanSaveButtonsInner.Visible = value;
        }
    }

    /// <summary>
    /// Gets or sets a flag to indicate if the Clone button is visible.
    /// </summary>
    [DefaultValue(true), Localizable(false)]
    public bool CloneButtonVisible
    {
        get
        {
            return spanCloneButtons.Visible;
        }
        set
        {
            spanCloneButtons.Visible = value;
        }
    }


    /// <summary>
    /// Gets or sets a flag to indicate if the "Save" button 
    /// is visible.
    /// <para></para>
    /// NOTE: This property is different from the SaveButtonsVisible
    /// (note the plural form), which sets the visible flag 
    /// of the span containing ALL save buttons.
    /// </summary>
    [DefaultValue(true), Localizable(false)]
    public bool SaveButtonVisible
    {
        get
        {
            return this.buttOnValidateAndSave.Visible;
        }
        set
        {
            this.buttOnValidateAndSave.Visible = value;
        }
    }


    /// <summary>
    /// Gets or sets a flag to indicate if the "Save and Close" button 
    /// is visible.
    /// <para></para>
    /// NOTE: This property is different from the SaveButtonsVisible
    /// (note the plural form), which sets the visible flag 
    /// of the span containing ALL save buttons.
    /// </summary>
    [DefaultValue(true), Localizable(false)]
    public bool SaveAndCloseButtonVisible
    {
        get
        {
            return this.buttOnValidateAndSaveAndClose.Visible;
        }
        set
        {
            this.buttOnValidateAndSaveAndClose.Visible = value;
        }
    }


    /// <summary>
    /// Gets or sets a flag to indicate if the "Save and Create" button 
    /// is visible.
    /// <para></para>
    /// NOTE: This property is different from the SaveButtonsVisible
    /// (note the plural form), which sets the visible flag 
    /// of the span containing ALL save buttons.
    /// </summary>
    [DefaultValue(true), Localizable(false)]
    public bool SaveAndNewButtonVisible
    {
        get
        {
            return this.buttOnValidateAndSaveAndNew.Visible;
        }
        set
        {
            this.buttOnValidateAndSaveAndNew.Visible = value;
        }
    }


    // 2010.08.16
    // Kim Foong 
    // Added spell-check capability for all forms.
    /// <summary>
    /// Gets or sets a flag to indicate if the "Spell Check" button 
    /// is visible.
    /// </summary>
    [DefaultValue(true), Localizable(false)]
    public bool SpellCheckButtonVisible
    {
        get
        {
            return false;
        }
        set
        {
        }
    }


    /// <summary>
    /// The ID of the UIGridView control to which all results will be bound.
    /// </summary>
    protected string tabStripID = "";


    /// <summary>
    /// Gets or sets the ID of the UITabStrip control that will be tabbed back and forth when the previous/next buttons
    /// are clicked.
    /// </summary>
    [DefaultValue(""), Localizable(false), IDReferenceProperty]
    [Description("Gets or sets the ID of the UITabStrip control that will be tabbed back and forth when the previous/next buttons are clicked.")]
    public string TabStripID
    {
        get
        {
            return tabStripID;
        }
        set
        {
            tabStripID = value;
        }
    }


    /// <summary>
    /// The UITabStrip control that will be tabbed back and forth when the previous/next buttons
    /// are clicked.
    /// </summary>
    [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public UITabStrip TabStrip
    {
        get
        {
            /*
            Control c = null;
            if (NamingContainer != null)
                c = NamingContainer.FindControl(tabStripID);
            if (c == null)
                c = Page.FindControl(tabStripID);
            if (c is UITabStrip)
                return (UITabStrip)c;
            else
                throw new Exception("There are no UITabStrips with the ID '" + tabStripID + "'");
             * */
            // Finds the first UITabStrip
            // 
            return GetTabStrip(this.Page);
        }
    }


    /// <summary>
    /// Gets or sets a flag to indicate whether customized
    /// object fields should be shown in the edit page.
    /// </summary>
    [DefaultValue(true), Localizable(false)]
    public bool EnableCustomizedObjects
    {
        get
        {
            if (ViewState["EnableCustomizedObjects"] == null)
                return true;
            return (bool)ViewState["EnableCustomizedObjects"];
        }
        set
        {
            ViewState["EnableCustomizedObjects"] = true;
        }
    }


    /// <summary>
    /// Gets or sets the confirmation text that will appear if the user clicks
    /// on any of the save button.
    /// </summary>
    public string SavingConfirmationText
    {
        get
        {
            return buttOnValidateAndSave.ConfirmText;
        }
        set
        {
            buttOnValidateAndSaveAndClose.ConfirmText = value;
            buttOnValidateAndSaveAndNew.ConfirmText = value;
            buttOnValidateAndSave.ConfirmText = value;
        }
    }


    /// <summary>
    /// Gets or sets the number of objectSubPanel.ascx controls that are open.
    /// <para></para>
    /// This is set only by the objectSubPanel.ascx control, and the 
    /// application must NOT set this directly.
    /// </summary>
    [Browsable(false), DefaultValue(0), Localizable(false)]
    public int SubPanelCount
    {
        get
        {
            if (textSubPanelCount.Text == "")
                textSubPanelCount.Text = "0";
            return Convert.ToInt32(textSubPanelCount.Text);
        }
        set
        {
            textSubPanelCount.Text = value.ToString();
        }
    }


    /// <summary>
    /// Gets the object currently stored in the session. 
    /// <para>
    /// </para>
    /// Application developers are encouraged to use this
    /// together with the panel's BindObjectToControls
    /// and BindControlsToObject methods to perform
    /// binding, instead of CurrentObject.
    /// </summary>
    [Browsable(false)]
    public PersistentObject SessionObject
    {
        get
        {
            // Updates the current object's ID into the viewstate.
            if (!IsPostBack)
                UpdateCurrentObjectInstanceKey();

            return Page.Session[GetObjectSessionKey()] as PersistentObject;
        }
    }

    ///
    /// <summary>
    /// </summary>
    /// 
    public bool ApprovalLimitColumnVisible
    {
        get
        {
            return this.gridApprovalProcess.Columns[1].Visible;
        }
        set
        {
            this.gridApprovalProcess.Columns[1].Visible = value;
        }
    }

    // 2010.10.15
    // Kim Foong
    /// <summary>
    /// Gets the datalist control for the workflow buttons.
    /// </summary>
    public DataList DataListWorkflowButtons
    {
        get
        {
            return this.datalistWorkflowButtons;
        }
    }


    /// <summary>
    /// Stores the selected workflow action.
    /// </summary>
    //string selectedWorkflowAction = "";



    /// <summary>
    /// Gets the selected workflow action.
    /// </summary>
    public string SelectedWorkflowAction
    {
        get { return selectedWorkflowAction.Value; }
    }


    PersistentObject currentObject;

    /// <summary>
    /// Gets the object currently stored in the session and
    /// binds it to and from the user interface at the same time.
    /// <para>
    /// </para>
    /// Application developers are encouraged to use SessionObject
    /// together with the panel's BindObjectToControls and 
    /// BindControlsToObject methods instead of this.
    /// </summary>
    [Obsolete("Consider using SessionObject instead.")]
    [Browsable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public PersistentObject CurrentObject
    {
        get
        {
            if (currentObject == null)
            {
                PersistentObject o = (PersistentObject)Page.Session[GetObjectSessionKey()];
                if (o != null)
                    ObjectPanel.BindControlsToObject(o);
                currentObject = o;
            }
            return currentObject;
        }
        set
        {
            // set the object
            //
            currentObject = value;
            Page.Session[GetObjectSessionKey()] = value;

            // Updates the current object's ID into the viewstate
            //
            if (!IsPostBack)
                UpdateCurrentObjectInstanceKey();

            // There might be quite a good number of loads from the database,
            // and each may open a connection separately. To improve performance
            // wrap a connection boundary around the binding.
            //
            using (Connection c = new Connection())
            {
                ObjectPanel.BindObjectToControls(value);
            }
        }
    }

    /// <summary>
    /// Gets or sets the caption that appears in the title of
    /// the objectPanel.ascx control.
    /// </summary>
    [Localizable(true)]
    public string Caption
    {
        get
        {
            return labelCaption.Text;
        }
        set
        {
            labelCaption.Text = value;
        }
    }

    //20130307
    //PTB
    /// <summary>
    /// A flag that indicates whether the workflow button is clicked.
    /// </summary>
    private bool workflowButtonClicked = false;

    // 2011.09.08
    // Kim Foong
    /// <summary>
    /// A flag that indicates whether the message text was changed.
    /// </summary>
    private bool messageChanged = false;


    /// <summary>
    /// Gets or sets the message that appears in the message bar. The message
    /// bar will show only if the Message property is not an empty string.
    /// </summary>
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public string Message
    {
        get
        {
            return labelMessage.Text;
        }
        set
        {
            //if (value.Trim() != "")
            //    panelMessage.Update();

            //20130306 PTB: Decode the message so we can display HTML tag.
            labelMessage.Text = Server.HtmlDecode(value);

            // 2011.09.08
            // Kim Foong
            // Set the message changed to true to force the animation to run.
            //
            if (value != null && value.Trim() != "")
                messageChanged = true;
            else
                messageChanged = false;
        }
    }


    /// <summary>
    /// A flag that indicates whether the object panel in
    /// the current page will support automatic binding to/from
    /// and the user interface and automatic saving.
    /// </summary>
    private bool automaticBindingAndSaving = false;


    /// <summary>
    /// Gets or sets a flag that indicates whether the object panel in
    /// the current page will support automatic binding to/from
    /// and the user interface and automatic saving. 
    /// <para></para>
    /// By default, this property is false. 
    /// </summary>
    public bool AutomaticBindingAndSaving
    {
        get { return automaticBindingAndSaving; }
        set { automaticBindingAndSaving = true; }
    }


    /// <summary>
    /// The name of the base table as declared in the TablesLogic class.
    /// This should be something like "tWork", or "tLocation", or
    /// "tUser".
    /// </summary>
    protected string baseTable = "";


    /// <summary>
    /// Gets or sets the name of the base table as declared in the TablesLogic
    /// class. This should be something like "tWork", or "tLocation", or
    /// "tUser".
    /// </summary>
    [DefaultValue(""), Localizable(false)]
    public string BaseTable
    {
        get
        {
            return baseTable;
        }
        set
        {
            baseTable = value;
        }
    }


    /// <summary>
    /// Gets the UIObjectPanel that contains this edit panel.
    /// <para></para>
    /// Note: A UITabStrip is a subclass of the UIObjectPanel class.
    /// </summary>
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public UIObjectPanel ObjectPanel
    {
        get
        {
            // look for the containing UIForm control, and calls
            // the ValidateControls method.
            //
            Control c = this.Parent;
            while (c != null)
            {
                if (c is UIObjectPanel)
                    break;
                c = c.Parent;
            }
            if (c != null && c is UIObjectPanel)
                return (UIObjectPanel)c;
            else
                throw new Exception("The web:panel control must be placed in a UIObjectPanel control, but it is currently not.");
        }
    }


    /// <summary>
    /// A flag to indicate if this window should be in focus after 
    /// the request or post back of this page is complete. If this
    /// is true, a javascript will be emitted to focus the window
    /// that is display this page.
    /// </summary>
    bool focusWindow = true;


    /// <summary>
    /// Gets or sets a flag to indicate if this window should be in focus after 
    /// the request or post back of this page is complete. If this
    /// is true, a javascript will be emitted to focus the window
    /// that is display this page.
    /// </summary>
    public bool FocusWindow
    {
        get
        {
            return focusWindow;
        }
        set
        {
            focusWindow = value;
        }
    }


    /// <summary>
    /// Gets or sets a flag that indicates whether or
    /// not the parent opener window should be refreshed
    /// after the user saves the object.
    /// </summary>
    public bool RefreshOpenerAfterSave
    {
        get
        {
            if (ViewState["RefreshOpenerAfterSave"] == null)
                return true;
            return (bool)ViewState["RefreshOpenerAfterSave"];
        }
        set
        {
            ViewState["RefreshOpenerAfterSave"] = value;
        }
    }


    /// <summary>
    /// The object base control.
    /// </summary>
    protected object objectBase;

    /// <summary>
    /// The object attachment control.
    /// </summary>
    public object objectAttachment;


    /// <summary>
    /// Gets or sets the object base control.
    /// </summary>
    public object ObjectBase
    {
        get { return objectBase; }
        set { objectBase = value; }
    }


    /// <summary>
    /// The object base control.
    /// </summary>
    protected object objectPanel2;


    /// <summary>
    /// Gets or sets the object base control.
    /// </summary>
    public object ObjectPanel2
    {
        get { return objectPanel2; }
        set { objectPanel2 = value; }
    }

    /// <summary>
    /// Gets or sets a flag to indicate if the document template
    /// button is visible.
    /// </summary>
    [DefaultValue(true), Localizable(false)]
    public bool DocumentTemplatesButtonVisible
    {
        get
        {
            return panelDocumentTemplates.Visible;
        }
        set
        {
            panelDocumentTemplates.Visible = value;
        }
    }

    /// <summary>
    /// Gets a key representing the uniqueness of the object
    /// that is to be used to determine if the page is
    /// editing or viewing the correctly loaded persistent
    /// object.
    /// </summary>
    /// <param name="persistentObject"></param>
    /// <returns></returns>
    protected string GetObjectInstanceKey(PersistentObject persistentObject)
    {
        if (System.Configuration.ConfigurationManager.AppSettings["LoadTesting"] == "true")
            return "";

        string key = persistentObject.ObjectID.ToString() + "::";
        if (persistentObject.ModifiedDateTime != null)
            key = key + persistentObject.ModifiedDateTime.Value.ToString("yyyy-MM-dd HH:mm:ss.f");

        return Security.Encrypt(key);
    }



    /// <summary>
    /// Sets the current object's ID into a hidden INPUT value
    /// of this page.
    /// </summary>
    /// <param name="id"></param>
    public void UpdateCurrentObjectInstanceKey()
    {
        PersistentObject persistentObject = Session[GetObjectSessionKey()] as PersistentObject;
        if (persistentObject == null)
            return;

        CurrentObjectInstanceKey.Value = GetObjectInstanceKey(persistentObject);
    }


    /// <summary>
    /// Each time a post back occurs, the ObjectID of the 
    /// persistent object in session is compared with the one stored
    /// in the hidden INPUT. If they are different, then the page disables
    /// itself and an error is shown.   
    /// </summary>
    /// <param name="id"></param>
    protected void AssertCurrentObjectInstanceIsValid()
    {
        PersistentObject persistentObject = Session[GetObjectSessionKey()] as PersistentObject;
        /*if (persistentObject == null)
            return;
        if (Page.Request.Form[CurrentObjectInstanceKey.UniqueID] == null ||
            Page.Request.Form[CurrentObjectInstanceKey.UniqueID] == "")
            return;

        if ((string)Page.Request.Form[CurrentObjectInstanceKey.UniqueID] == GetObjectInstanceKey(persistentObject))
            return;

        string path = Page.Request.ApplicationPath;
        if (!path.EndsWith("/"))
            path = path + "/";
        Response.Redirect(path + "/appobjecterr.aspx");
        */

        // 2014.12.18
        // KL/Linn
        //
        // Re-arrange the condition
        // Added a condition to check if the session object is null during a postback
        // this is to prevent an error when a user open an edit page, then logout and login
        // the main page without closing the edit page, then issue a postback action
        // in the edit page

        if (!(persistentObject == null
            || Page.Request.Form[CurrentObjectInstanceKey.UniqueID] == null
            || Page.Request.Form[CurrentObjectInstanceKey.UniqueID] == ""
            || (string)Page.Request.Form[CurrentObjectInstanceKey.UniqueID] == GetObjectInstanceKey(persistentObject))
            || (Page.IsPostBack && persistentObject == null))
        {
            string path = Page.Request.ApplicationPath;
            if (!path.EndsWith("/"))
                path = path + "/";
            Response.Redirect(path + "/appobjecterr.aspx");
        }

    }


    /// <summary>
    /// Clears all keys created by the previous edit page.
    /// </summary>
    protected void ClearPageSession()
    {
        string pageSessionPath = "::SessionEditPagePath::";
        if (Request["N"] != null)
            pageSessionPath = pageSessionPath + Request["N"];

        // Clears all session variables that contains this
        // page's full request path. All other session
        // variables are retained.
        //
        string path = (string)Session[pageSessionPath];
        if (path != null && path != Page.Request.Path)
        {
            for (int i = Session.Keys.Count - 1; i >= 0; i--)
            {
                string key = Session.Keys[i];
                if (key.Contains(path))
                    Session.RemoveAt(i);
            }
        }

        // Once we are done clearing the data,
        // Set this page's path into the session,
        // so that it can be cleared the next time.
        //
        Session[pageSessionPath] = Page.Request.Path;

        // Clear also the persistent object and the workflow
        // as they are likely to be taking up a lot of space
        // in memory.
        //
        //Session[GetObjectSessionKey()] = null;
    }


    SchemaBase table = null;


    /// <summary>
    /// Reflect on the Tables class and get the object whose name is
    /// specified in the BaseTable property.
    /// </summary>
    private void GetBaseTable()
    {
        Type baseTableType = typeof(TablesLogic);
        string baseTableName = baseTable;
        if (baseTable.StartsWith("TablesLogic."))
        {
            baseTableType = typeof(TablesLogic);
            baseTableName = baseTableName.Replace("TablesLogic.", "");
        }
        else if (baseTable.StartsWith("TablesWorkflow."))
        {
            baseTableType = typeof(TablesWorkflow);
            baseTableName = baseTableName.Replace("TablesWorkflow.", "");
        }
        else if (baseTable.StartsWith("TablesAuditTrail."))
        {
            baseTableType = typeof(TablesAuditTrail);
            baseTableName = baseTableName.Replace("TablesAuditTrail.", "");
        }

        table = (SchemaBase)UIBinder.GetValue(baseTableType, baseTableName, true);
        if (table == null)
            throw new Exception("Unknown table '" + baseTable + "'");
    }

    /// <summary>
    /// Gets the currently searching object type.
    /// </summary>
    /// <returns></returns>
    public string GetRequestObjectType()
    {
        if (Request["TYPE"] == null)
            return "";
        return Security.Decrypt(Request["TYPE"]).Split(':')[0];
    }


    protected void RedirectToNoAccessPage()
    {
        Response.Write(
            "<" + "script type='text/javascript'>window.open('" + 
            Page.ResolveUrl("~/appobjectnoaccess.aspx") + 
            "', '_self');" + 
            "<" + "/script>");
        Response.End();
    }


    /// <summary>
    /// Initializes the PersistentObject by loading it from
    /// the database, or by creating a new one, depending
    /// on the parameter passed in via the query string.
    /// </summary>
    /// <returns></returns>
    protected void InitializePersistentObject()
    {
        string arg = Security.Decrypt(Request["ID"]);
        string[] args = arg.Split(':');

        // This should be an abstract class that provides
        // the security settings of this object. In 
        // Anacle.EAM v6.0, security settings are 
        // stored in the database.
        //
        spanDelete.Visible = AppSession.User.AllowDeleteAll(GetRequestObjectType());

        GetBaseTable();
        if (table == null)
            throw new Exception("Unknown table '" + baseTable + "'");

        Type persistentObjectType = table.GetPersistentObjectType();

        // Create or load the object depending on the mode
        //
        bool shouldAllowCreateNew = AppSession.User.AllowCreate(GetRequestObjectType(), Request["AttachedObjectTypeName"]);

        PersistentObject o = null;
        if (args[0] == "NEW")
        {
            try
            {
                // 2016.12.16, Kien Trung
                // BUG FIXED: Should not allow Create NEW when the user is not allowed to Create NEW
                //
                if (!shouldAllowCreateNew)
                    RedirectToNoAccessPage();

                o = table.CreateObject() as PersistentObject;
                spanDelete3.Visible = false;

                // Check to see if the application has to display any important notes
                // about this page.
                //
                OFunction function = OFunction.GetFunctionByObjectType(GetRequestObjectType());
                if (function != null && function.ShowCreateObjectImportantNotes == 1)
                {
                    ImportantNotesLabel.Text = function.CreateObjectImportantNotes;
                    ImportantNotesDialogBox.Show();
                }

                PopulateCurrentUserBusinessEntity(o);
            }
            catch (WorkflowValidationFailedException ex)
            {
                string errors = "";
                foreach (ValidationError err in ex.Errors)
                    errors += err.ErrorText + "\n";
                throw new Exception("The workflow failed validation with the following errors:\n" + errors);
            }
        }
        else if (args[0] == "NEW2")
        {
            // 2016.12.16, Kien Trung
            // BUG FIXED: Should not allow Create NEW when the user is not allowed to Create NEW
            //
            if (!shouldAllowCreateNew)
                RedirectToNoAccessPage();

            spanDelete3.Visible = false;
            o = this.SessionObject as PersistentObject;

            PopulateCurrentUserBusinessEntity(o);
        }
        else if (args[0] == "EDIT" || args[0] == "VIEW") // for EDIT ALL
        {
            o = table.LoadObject(new Guid(args[1]), true) as PersistentObject;
            if (o == null)
                throw new Exception("Unable to load the object from the database. Please make sure you have specified the correct BaseTable in the objectPanel control and the correct ObjectID through the querystring. Also make sure that the object that you are loading has not been deleted from the database, or has its IsDeleted flag set to 1.");
        }
        Page.Session[GetObjectSessionKey()] = o;
        //BindWorkflowTransitionDropdownList(o);

        if (!(o is LogicLayerWorkflowPersistentObject))
            PriorityFlag.PropertyName = "";


    }





    /// <summary>
    /// Occurs when the page is initialized. 
    /// </summary>
    /// <param name="e"></param>
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        UIButton1.Click += new EventHandler(UIButton1_Click);
        UIButton2.Click += new EventHandler(UIButton2_Click);
        UIButton3.Click += new EventHandler(UIButton3_Click);
        UIButton4.Click += new EventHandler(UIButton4_Click);
        UIButton5.Click += new EventHandler(UIButton5_Click);
        UIButton6.Click += new EventHandler(UIButton6_Click);
        UIButton7.Click += new EventHandler(UIButton7_Click);
        UIButton8.Click += new EventHandler(UIButton8_Click);


        // Clear all previous edit page's Session
        // values. This helps to free up a little Session
        // memory.
        //
        if (!IsPostBack)
        {
            ClearPageSession();
            InitializePersistentObject();
        }
        BindWorkflowTransitionDropdownList(this.SessionObject);

        // Clear all session values and initialize the customized
        // object.
        //
        if (EnableCustomizedObjects)
        {
            if (!IsPostBack)
                ClearCustomizedObjectSession();
            InitCustomizedObject();
        }

    }


    /// <summary>
    /// Occurs when the initialization of the form is complete
    /// and the controls have been loaded.
    /// </summary>
    /// <remarks>
    /// There are several modes that the objectPanel.ascx control
    /// handles, and this mode is passed into this page through
    /// ID in the querystring. The modes handled by this control are:
    /// NEW, EDIT, VIEW.
    /// <para></para>
    /// If the mode is NEW, the following happens:
    /// <list>
    /// <item>Creates a new PersistentObject from the schema specified in the BaseTable</item>
    /// <item>Binds the PersistentObject to the form controls.</item>
    /// <item>Calls the PopulateForm event.</item>
    /// <item>Binds the PersistentObject to the form controls again.</item>
    /// <item>Creates and populates customized object fields.</item>
    /// </list>
    /// If the mode is EDIT, the following happens:
    /// <list>
    /// <item>Loads a PersistentObject from the schema specified in the BaseTable, and using
    /// the ObjectID specified in the query string.</item>
    /// <item>If the PersistentObject has been deactivated, disables the entire page.</item>
    /// <item>Binds the PersistentObject to the form controls.</item>
    /// <item>Calls the PopulateForm event.</item>
    /// <item>Binds the PersistentObject to the form controls again.</item>
    /// <item>Creates and populates customized object fields.</item>
    /// </list>
    /// If the mode is VIEW, the following happens:
    /// <list>
    /// <item>Loads a PersistentObject from the schema specified in the BaseTable, and using
    /// the ObjectID specified in the query string.</item>
    /// <item>Disables the entire page.</item>
    /// <item>Binds the PersistentObject to the form controls.</item>
    /// <item>Calls the PopulateForm event.</item>
    /// <item>Binds the PersistentObject to the form controls again.</item>
    /// <item>Creates and populates customized object fields.</item>
    /// </list>
    /// </remarks>
    /// <param name="e"></param>
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        ((UIPageBase)this.Page).ScriptManager.RegisterPostBackControl(this.buttonGeneratePDF);

        // Sets a click event for the summary tabviews.
        //
        UITabStrip t = GetTabStrip(this.Page);
        if (t != null)
        {
            t.FirstSummaryTabView.Click += new EventHandler(SummaryTabView_Click);
            t.LastSummaryTabView.Click += new EventHandler(SummaryTabView_Click);
        }


        AssertCurrentObjectInstanceIsValid();

        // this is where we load / create the new object
        // depends entirely on the ID query string
        //
        if (!IsPostBack)
        {
            PersistentObject o = this.SessionObject as PersistentObject;
            bool disablePage = false;

            if (Request["ID"] == null)
                RedirectToNoAccessPage();

            string[] args = Security.Decrypt(Request["ID"]).Split(':');

            if (ConfigurationManager.AppSettings["LoadTesting"] == "false")
            {
                // Check for the salt in ID
                //
                if (args.Length > 0 && args[args.Length - 1].StartsWith("SLT-"))
                {
                    if (args[args.Length - 1] != AppSession.SaltID)
                        RedirectToNoAccessPage();
                }
                else
                    RedirectToNoAccessPage();

                // Check for the salt in TYPE
                //
                string[] typeArgs = Security.Decrypt(Request["TYPE"]).Split(':');
                if (typeArgs.Length > 0 && typeArgs[typeArgs.Length - 1].StartsWith("SLT-"))
                {
                    if (typeArgs[typeArgs.Length - 1] != AppSession.SaltID)
                        RedirectToNoAccessPage();
                }
                else
                    RedirectToNoAccessPage();
            }

            string type = GetRequestObjectType();
            if (args[0] == "VIEW" || args[0] == "EDIT")
            {

                // 2016.12.16, Kien Trung
                // BUG FIXED: Critical bug linked object can be view by anyone
                // 
                bool shouldAllowEdit = AppSession.User.AllowEditAll(type);
                bool shouldAllowView = AppSession.User.AllowViewAll(type);

                if (o is LogicLayerWorkflowPersistentObject)
                {
                    LogicLayerWorkflowPersistentObject ow = o as LogicLayerWorkflowPersistentObject;
                    shouldAllowEdit =
                        shouldAllowEdit || (ow.CurrentActivity != null && ow.CurrentActivity.IsAssignedToUser(AppSession.User));
                    shouldAllowView = shouldAllowView || (ow.CurrentActivity != null && ow.CreatedID == AppSession.User.ObjectID);
                    if (o is LogicLayerApprovalPersistentObject)
                    {
                        shouldAllowView =
                            shouldAllowView || (ow.CurrentActivity != null && ((LogicLayerApprovalPersistentObject)ow).SubmittorID == AppSession.User.ObjectID);
                    }
                }

                if (!shouldAllowEdit)
                    disablePage = true;

                if (!shouldAllowEdit && !shouldAllowView)
                    RedirectToNoAccessPage();
            }


            //Sli start: data level access right check
            if (!AppSession.User.AllowBusinessEntityLevelAccess(o))
                RedirectToNoAccessPage();
            //Sli end: data level access right check

            if (Request["SEARCH"] != null && Request["SEARCH"].ToString() == "1")
            {
                Window.ClearReturnDataTable();
            }


            // 2011.08.12
            // Edwin Lee
            // Show the tab index passed by the Request["TAB"]

            if (Request["TAB"] != null && Request["TAB"] != "")
            {
                if (TabStrip != null)
                {
                    try
                    {
                        int tabIndex = Int32.Parse(Security.Decrypt(Request["TAB"]));
                        TabStrip.SelectedIndex = tabIndex;
                    }
                    catch (Exception ex) { }
                }
            }

            // 2011.07.29
            // Edwin Lee
            // Show the return to button if there is a return in the querystring.
            //
            String hasNextReturnObject = Window.HasNextReturnObject();
            if (hasNextReturnObject != null)
            {
                //string translated = Resources.Objects.ResourceManager.GetString(hasNextReturnObject);
                //ObjectTypeLabel.Text = translated;
                /*buttonReturn.Text = translated == null ? 
                    String.Format(Resources.Strings.General_BackToObject, hasNextReturnObject) : 
                    String.Format(Resources.Strings.General_BackToObject, translated);*/
                buttonReturn.Visible = true;
                //ObjectTypeLabel.Visible = true;

            }

            // 2010.05.19
            // Kim Foong
            // If the user does not have create rights, hide the save and new button.
            //
            if (!AppSession.User.AllowCreate(GetRequestObjectType()))
                buttOnValidateAndSaveAndNew.Visible = false;

            // If the object has been deleted, then disable the page.
            //
            if (o.IsDeleted == 1)
                disablePage = true;

            // sometimes the object may be lost. if that is the case, 
            // make sure we don't go any further,
            // and close the window.
            //
            if (o == null)
            {
                Window.Close();
            }
            else
            {
                if (disablePage)
                    DisablePage();

                CallPopulateFormEvent();

                if (EnableCustomizedObjects)
                    PopulateCustomizedObject();

                // Show any message that a previous page might have passed in
                //
                if (Request["MESSAGE"] != null)
                    this.Message = HttpUtility.HtmlEncode(Request["MESSAGE"]);
            }

            // Show the prev/next buttons whenever there is a
            // tabstrip available.
            //
            PrevNextTabButtons.Visible = (t != null);
            if (t != null)
            {
                t.ShowSummaryAsLastTabView = true;
                t.WizardStyle = true;

                int c = 1000;
                while (!t.SelectedTab.Visible && c > 0)
                {
                    t.GoToNextTab();
                    c--;
                }
            }
        }

        /*ScriptManager.RegisterStartupScript(Page, typeof(Page), "UPDATEOBJECTPANEL",
            "if(ge('objectPanelFooter')) ge('objectPanelFooter').innerHTML = ge('" + ButtonPanel.ClientID + "').innerHTML; ", true);*/
        /*ScriptManager.RegisterStartupScript(Page, typeof(Page), "UPDATEOBJECTPANEL",
            "if(ge('" + this.buttonShadow.ClientID + "')) ge('" + this.buttonShadow.ClientID + "').innerHTML = ge('" + ButtonPanel.ClientID + "').innerHTML; ", true);
            */
        // Mobile web
        //
        if (this.Page is UIPageBase && ((UIPageBase)this.Page).MobileMode)
        {
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "UPDATEOBJECTPANEL2",
                "ge('" + ButtonContainerPanel.ClientID + "').style.display = 'none'; ", true);
        }

        //CloseWindowButtonVisible = false;

        PopulateDocumentTemplate(null);
        //if (EnableCustomizedObjects)
        //    PopulateCustomizedObject();

        // If the tabstrip has been set to a different tab index,
        // call the tabview's click event.
        //
        if (Request["TAB"] != null && Request["TAB"] != "")
        {
            if (TabStrip.SelectedTab != null)
                TabStrip.SelectedTab.TriggerClickEvent();
        }

        // Mobile Web
        //
        if (this.Page is UIPageBase && ((UIPageBase)this.Page).MobileMode)
        {
            AdditionalCommandsCell.Visible = false;

        }
        else
        {
        }
    }


    /// <summary>
    /// Automatically binds the object to the page.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void SummaryTabView_Click(object sender, EventArgs e)
    {
        this.ObjectPanel.BindControlsToObject(this.SessionObject);
        this.ObjectPanel.BindObjectToControls(this.SessionObject);
    }


    // 2011.01.25
    // Kim Foong
    /// <summary>
    /// Gets the workflow action button corresponding to the event name.
    /// </summary>
    /// <param name="eventName"></param>
    /// <returns></returns>
    public UIButton GetWorkflowActionButton(string eventName)
    {
        if (eventName == "-")
            return buttonWorkflowSave;

        foreach (DataListItem item in this.datalistWorkflowButtons.Items)
        {
            UIButton button = item.FindControl("buttonWorkflowAction") as UIButton;
            if (button.CommandName == eventName)
                return button;
        }
        return null;
    }


    /// <summary>
    /// Sets the workflow action button visibility.
    /// </summary>
    /// <param name="eventName"></param>
    /// <returns></returns>
    public void SetWorkflowActionButtonVisibility(string eventName, bool visible)
    {
        Helpers.SetButtonVisibility(this.GetWorkflowActionButton(eventName), visible);
    }


    /// <summary>
    /// Sometimes the page is used for viewing a separate object so the session key should be different from the edit page session key
    /// if it is view object and with Query String N then we use different session object key
    /// </summary>
    /// <returns></returns>
    public string GetObjectSessionKey()
    {
        string arg = Security.Decrypt(Request["ID"]);
        string[] args = arg.Split(':');
        if (Request["N"] != null)
        {
            return "::SessionObject::" + "_" + Request["N"];
        }
        else
            return "::SessionObject::";
    }


    /// <summary>
    /// Checks if the objectMemo / objectAttachment exists
    /// in a control.
    /// </summary>
    /// <param name="c"></param>
    /// <returns></returns>
    protected bool ContainsControlsCannotBeDisabled(Control c)
    {
        if (c.GetType().Name == "objectMemo" ||
            c.GetType().Name == "objectAttachment")
            return true;

        foreach (Control child in c.Controls)
            if (ContainsControlsCannotBeDisabled(child))
                return true;

        return false;
    }


    /// <summary>
    /// Disables the Panel control if there are
    /// no attachment or memo controls inside.
    /// </summary>
    protected bool DisablePanelControl(Control c)
    {
        return false;
    }


    /// <summary>
    /// Disables the entire page by setting the Enabled flag of the buttons
    /// and the ObjectPanel to false.
    /// </summary>
    public void DisablePage()
    {
        // disable the controls container
        //
        spanDelete3.Visible = false;
        buttOnValidateAndSave.Enabled = false;
        buttOnValidateAndSaveAndNew.Enabled = false;
        buttOnValidateAndSaveAndClose.Enabled = false;
        datalistWorkflowButtons.Enabled = false;

        if (ObjectPanel != null && ObjectPanel is WebControl)
            ((WebControl)ObjectPanel).Enabled = false;
    }


    /// <summary>
    /// Calls the populate form event, and automatically
    /// binds the session object to the user interface, if automatic
    /// binding is turned on.
    /// </summary>
    public void CallPopulateFormEvent()
    {
        using (Connection c = new Connection())
        {
            PopulateDocumentTemplate(null);

            if (PopulateForm != null)
                PopulateForm(this, EventArgs.Empty);



            if (AutomaticBindingAndSaving)
                this.ObjectPanel.BindObjectToControls(this.SessionObject);

            //Populate New Documente Template Listing

        }
    }

    public void PopulateDocumentTemplate(Guid? documentTemplateID)
    {

        for (int i = panelDocumentTemplates.Controls.Count - 1; i >= 0; i--)
        {
            if (panelDocumentTemplates.Controls[i] is UIButton)
                panelDocumentTemplates.Controls.RemoveAt(i);
        }


        Hashtable numberOfTimesPrinted = ODocument.GetNumberOfTimesPrinted(this.SessionObject.ObjectID);
        if (documentTemplateID != null)
        {
            if (numberOfTimesPrinted[documentTemplateID.Value] != null)
                numberOfTimesPrinted[documentTemplateID.Value] = (int)numberOfTimesPrinted[documentTemplateID.Value] + 1;
            else
                numberOfTimesPrinted[documentTemplateID.Value] = 1;
        }

        // Populate the document template dropdowns
        //
        string currentState = "";

        if (!(this.SessionObject is LogicLayerPersistentObject))
            return;

        if (this.SessionObject is LogicLayerWorkflowPersistentObject && ((LogicLayerWorkflowPersistentObject)this.SessionObject).CurrentActivity != null)
            currentState = ((LogicLayerWorkflowPersistentObject)this.SessionObject).CurrentActivity.ObjectName;
        List<ODocumentTemplate> docTemplates = ODocumentTemplate.GetDocumentTemplates(this.SessionObject.GetType().BaseType.Name, this.SessionObject, currentState);

        foreach (ODocumentTemplate dt in docTemplates)
        {
            UIButton newButton = new UIButton();
            newButton.ID = Security.EncryptToHex(dt.ObjectID.ToString());
            newButton.CausesValidation = false;
            newButton.AlwaysEnabled = true;
            newButton.IconCssClass = "material-icons";
            newButton.IconText = "print";

            int numberOfTimesPrintedInt = 0;
            if (numberOfTimesPrinted[dt.ObjectID.Value] != null)
                numberOfTimesPrintedInt = (int)numberOfTimesPrinted[dt.ObjectID.Value];
            string NumberOfTimesPrinted = "<span class='div-viewtimes'>" + String.Format(Resources.Strings.DocumentTemplateLog_ButtonText, numberOfTimesPrintedInt) + "</span>";
            newButton.Text = dt.FileDescription + " " + NumberOfTimesPrinted;
            newButton.Height = buttonDelete.Height;
            //newButton.InlineBlockText = true;
            newButton.Click += new EventHandler(documentTemplate_Click);

            if (_PrintTemplateCode != null && _PrintEventHandler != null)
            {
                if (dt.DocumentTemplateCode.Is(_PrintTemplateCode))
                    newButton.Click += _PrintEventHandler;
            }
            panelDocumentTemplates.Controls.Add(newButton);


        }

        panelDocumentTemplates2.Visible = docTemplates.Count > 0;
    }

    protected void documentTemplate_Click(object sender, EventArgs e)
    {
        textClickedButton.Text = "buttonPrint";
        CurrentDocumentTemplateID.Value = ((UIButton)sender).ID;

        if (GetRequestObjectType().Is("OARInvoice", "OARCreditNote"))
        {
            MassPrintingDialogBox.Show();
        }
        else
        {
            this.FocusWindow = false;
            UIButton b = (UIButton)sender;
            Window.DownloadUrl(
                this.ResolveUrl(Page.Request.ApplicationPath + "/components/document.aspx?templateID=" + HttpUtility.UrlEncode(b.ID) + "&t=" + DateTime.Now.ToString("hhmmssfff") + "&session=" + GetObjectSessionKey() + "&format=" + HttpUtility.UrlEncode(Security.Encrypt("word"))));

            using (Connection c = new Connection())
            {
                //add a document template log
                ODocumentTemplateLog log = TablesLogic.tDocumentTemplateLog.Create();
                log.DocumentTemplateID = new Guid(Security.DecryptFromHex(b.ID));
                log.AttachedObjectID = this.SessionObject.ObjectID;
                log.ObjectTypeName = this.SessionObject.GetType().BaseType.Name;

                log.Save();
                c.Commit();
            }

            PopulateDocumentTemplate(new Guid(Security.DecryptFromHex(b.ID)));
        }
    }

    protected string[] _PrintTemplateCode
    {
        get { return Session["_PrintTemplateCode"] as string[]; }
        set { Session["_PrintTemplateCode"] = value; }
    }

    protected EventHandler _PrintEventHandler
    {
        get { return Session["_PrintEventHandler"] as EventHandler; }
        set { Session["_PrintEventHandler"] = value; }
    }
    public void OnPrintLetter(String[] TemplateCode, EventHandler e)
    {
        _PrintEventHandler = e;
        _PrintTemplateCode = TemplateCode;
    }

    /// <summary>
    /// Initializes the workflow transition dropdown list
    /// with a list of all transitions available to the user.
    /// </summary>
    public void BindWorkflowTransitionDropdownList(PersistentObject persistentObject)
    {
        if (!(persistentObject is LogicLayerWorkflowPersistentObject))
            return;

        LogicLayerWorkflowPersistentObject logicLayerPersistentObject = persistentObject as LogicLayerWorkflowPersistentObject;

        if (logicLayerPersistentObject != null &&
            logicLayerPersistentObject.CurrentActivity != null &&
            logicLayerPersistentObject.CurrentActivity.WorkflowInstanceID != null)
        {
            // 2010.10.15
            // Kim Foong
            // Create a data table to be used to form the workflow buttons
            //
            System.Data.DataTable dt = new System.Data.DataTable();
            dt.Columns.Add("CommandName", typeof(string));
            dt.Columns.Add("Text", typeof(string));
            dt.Columns.Add("ImageUrl", typeof(string));
            dt.Columns.Add("IconCssClass", typeof(string));
            dt.Columns.Add("IconText", typeof(string));

            // Adds the first line item into the radio button list.
            // This allows the user to save the object without performing
            // any transition to another state.
            //
            if (logicLayerPersistentObject.CurrentActivity.ObjectName != "-" &&
                logicLayerPersistentObject.CurrentActivity.ObjectName != "" &&
                logicLayerPersistentObject.CurrentActivity.ObjectName != "Start")
            {
                //dt.Rows.Add("-", Resources.Strings.Workflow_Save, "", "material-icons", "save");
                buttonWorkflowSave.Visible = true;
            }
            else
                buttonWorkflowSave.Visible = false;

            // Gets the list of all events that can be triggered on
            // the workflow at its current status.
            //
            if (logicLayerPersistentObject.CurrentActivity != null)
            {
                string workflowInstanceId =
                    logicLayerPersistentObject.CurrentActivity.WorkflowInstanceID;

                // 2011.09.07 
                // Kim Foong
                // Enhanced workflow engine to avoid persistence to database, and in-flight changes of workflow versions.
                //
                List<WorkflowEventInfo> workflowEvents = null;
                try
                {
                    // 2014.11.26
                    // Kim Foong
                    // Optimized to use the faster method to get the workflow events
                    // direct from the workflow XML file.
                    //                    
                    workflowEvents = WorkflowEngine.Engine.GetWorkflowEventsFast(logicLayerPersistentObject, logicLayerPersistentObject.CurrentActivity.ObjectName);
                }
                catch (WorkflowValidationFailedException ex)
                {
                    string errors = "";
                    foreach (ValidationError err in ex.Errors)
                        errors += err.ErrorText + "\n";
                    throw new Exception("The workflow failed validation with the following errors:\n" + errors);
                }

                // 2010.10.15
                // Kim Foong
                // Since we are unable to get the order of the events from the workflow.xoml
                // file, we have to force the ordering.
                //
                List<string> eventNames = new List<string>();
                foreach (WorkflowEventInfo workflowEvent in workflowEvents)
                {
                    string eventName = workflowEvent.EventName;
                    if (eventName == "SaveAsDraft")
                        eventName = "000:" + eventName;
                    else if (eventName == "Verify")
                        eventName = "010:" + eventName;
                    else if (eventName == "Support")
                        eventName = "010:" + eventName;
                    else if (eventName == "Approve")
                        eventName = "010:" + eventName;
                    else if (eventName == "Query")
                        eventName = "011:" + eventName;
                    else if (eventName == "Reject")
                        eventName = "012:" + eventName;
                    else if (eventName == "Close")
                        eventName = "900:" + eventName;
                    else if (eventName == "Cancel")
                        eventName = "901:" + eventName;
                    else
                        eventName = "500:" + eventName;
                    eventNames.Add(eventName);
                }
                eventNames.Sort();

                foreach (string eventName in eventNames)
                {
                    string actualEventName = eventName.Split(':')[1];
                    string translatedText = Resources.WorkflowEvents.ResourceManager.GetString(actualEventName);
                    if (translatedText == null)
                        translatedText = actualEventName;

                    if (actualEventName == "Approve")
                    {
                        translatedText = GetAlternateEventName();
                    }

                    string iconText = "send";
                    if (actualEventName == "SaveAsDraft")
                        iconText = "save";
                    else if (actualEventName == "Verify")
                        iconText = "done";
                    else if (actualEventName == "Support")
                        iconText = "done";
                    else if (actualEventName == "Approve")
                        iconText = "done";
                    else if (actualEventName == "Complete")
                        iconText = "done";
                    else if (actualEventName == "Close")
                        iconText = "cancel";
                    else if (actualEventName == "Reject")
                        iconText = "cancel";
                    else if (actualEventName == "Cancel")
                        iconText = "cancel";
                    else if (actualEventName == "Query")
                        iconText = "assignment_return";

                    dt.Rows.Add(actualEventName, translatedText, "", "material-icons", iconText);
                }

                this.DataListWorkflowButtons.DataSource = dt;
                this.DataListWorkflowButtons.DataBind();

                this.spanWorkflow.Visible = true;
            }
        }
    }



    /// <summary>
    /// Registers the save buttons for a full PostBack instead
    /// of an AJAX partial postback.
    /// <para></para>
    /// This method can be called by the .aspx during the page's 
    /// OnInit event.
    /// </summary>
    public void RegisterPostBackControlForSaveButtons()
    {
        // Register the buttonUpload button to force a full
        // postback whenever a file is uploaded.
        //
        if (Page is UIPageBase)
        {
            ((UIPageBase)Page).ScriptManager.RegisterPostBackControl(buttOnValidateAndSave);
            ((UIPageBase)Page).ScriptManager.RegisterPostBackControl(buttOnValidateAndSaveAndClose);
            ((UIPageBase)Page).ScriptManager.RegisterPostBackControl(buttOnValidateAndSaveAndNew);
        }
    }


    /// <summary>
    /// Hides/shows element.
    /// </summary>
    /// <param name="e"></param>
    protected override void OnPreRender(EventArgs e)
    {
        string type = GetRequestObjectType();
        if (type == null)
            return;
        //if (type != null)
        //{
        //if (this.SessionObject != null && this.SessionObject.GetType().BaseType.Name != type)
        //    return;
        //}

        base.OnPreRender(e);

        //Rachel 13April 2007 Customized object       
        //set visibility of the no attribute message
        if (NoAttributeMessageID != null)
        {
            Control control = Page.FindControl(NoAttributeMessageID);
            if (control != null)
                control.Visible = !HasAttributeField;
        }

        //endrachel        

        // Show message only if the message is not an empty string
        //
        //panelMessage.Visible = this.Message.Trim() != "";
        //panelMessage.Visible = true;

        //Hide view journal transaction button if it doesn't implement IJournalTransaction
        //If it does, the visibility is determined by property ShowJournalTransaction
        PersistentObject o = this.SessionObject;
        if (o is IJournalTransaction)
        {
            IJournalTransaction jt = o as IJournalTransaction;
            buttonJournalTransaction.Visible = jt.ShowJournalTransaction;
        }
        else
            buttonJournalTransaction.Visible = false;

        // Mobile Web
        // Always hide the thin action buttons.
        //
        if (this.Page is UIPageBase && ((UIPageBase)this.Page).MobileMode)
        {
            buttonDocumentLog.Visible = false;
            buttonJournalTransaction.Visible = false;
        }

        // hide the delete button if the object cannot be deactivated
        if (o != null)
        {
            buttonDelete.Visible = o.IsDeactivatable();
            spanDeleteSeparator.Visible = buttonDelete.Visible;
        }

        // Show the clone button only if the object
        // is cloneable.
        //
        buttonClone.Visible = o is ICloneable && AppSession.User.AllowCreate(o.TypeName);
        buttonClone.Enabled = !o.IsNew;

        if (o is IWorkflowEnabled)
        {
            panelUrgent.Visible = true;
            panelUrgent.CssClass = PriorityFlag.Checked ? "div-urgent-yes" : "div-urgent-no";
        }


        // Hide all save buttons if at least one of the sub panels is
        // open.
        //
        // 2010.10.17
        // Chee Meng
        // Allow save buttons to be visible even if the subpanels are open

        //spanCloneButtons.Visible = 
        //    spanSaveButtons.Visible = (SubPanelCount == 0);

        // Emits a javascript to focus this window after it loads.
        //
        if (focusWindow)
            Window.WriteJavascript("if( window.focus ) window.focus();");

        // Set a javascript to pop up a confirmation dialog when the 
        // user clicks on a cancel button when some items have been
        // detected as modified.
        //
        // 2014.10.15
        // KL
        // If !UIPageBase.IsAjaxEnabled fails, then UIPageBase.IsAjaxEnabled must be true
        // so only need !IsPostBack instead of (UIPageBase.IsAjaxEnabled && !IsPostBack)
        //
        // if (!UIPageBase.IsAjaxEnabled || (UIPageBase.IsAjaxEnabled && !IsPostBack))
        if (!UIPageBase.IsAjaxEnabled || !IsPostBack)
        {
            /*buttonReturn.OnClickScript =
                "if( document.getElementById('ModifiedFlag') && document.getElementById('ModifiedFlag').value!='' ) " +
                "if( !confirm( '" + Resources.Messages.General_ItemModifiedConfirmClose + "' ) ) return false; ";*/
            //buttonReturn.ConfirmText = "";
        }

        // added so that we can force a refresh in pop-ups
        //
        // 2014.10.15
        // KL
        // Same as above, do not need to include UIPageBase.IsAjaxEnabled in the second half
        // of the condition
        // If the postback is a full postback, we also need to output the refersh script again
        //
        //if (!UIPageBase.IsAjaxEnabled || (UIPageBase.IsAjaxEnabled && !IsPostBack))
        if (!UIPageBase.IsAjaxEnabled || !IsPostBack || (Page is UIPageBase && !((UIPageBase)Page).ScriptManager.IsInAsyncPostBack))
        {
            Window.WriteJavascript(
                "function Refresh() { " +
                Page.ClientScript.GetPostBackEventReference(buttonRefresh.LinkButton, "") + " }");
            Window.WriteJavascript(
                "function RefreshSession() { " +
                Page.ClientScript.GetPostBackEventReference(buttonRefreshSession.LinkButton, "") + " }");

            // added to reset the timer on prerender.

            int timeOutPromptInMilliseconds = 10000;
            if (this.Session.Timeout <= 5)
                timeOutPromptInMilliseconds = 30000;
            else if (this.Session.Timeout <= 10)
                timeOutPromptInMilliseconds = 60000;
            else if (this.Session.Timeout <= 15)
                timeOutPromptInMilliseconds = 90000;
            else
                timeOutPromptInMilliseconds = 120000;

            Window.WriteJavascript(@"
                var timeToPrompt = " + (this.Session.Timeout * 60000 - timeOutPromptInMilliseconds) + @";
                var mytime;
                function endSession() {
                    alert('" + Resources.Messages.Session_Expiring + @"');
                    RefreshSession();
                    mytime = window.setTimeout('endSession();', timeToPrompt);
                }
                function stopTimer() {
                    clearTimeout(mytime);
                    mytime = window.setTimeout('endSession();', timeToPrompt);
                }
                window.onload = function() { mytime = window.setTimeout('endSession();', timeToPrompt); }
            ");

            Window.WriteJavascript(@"
                window.onresize = function (event) {
                    resize();
                };

                $(document).ready(function () {
                    setTimeout(resize, 0);
                });

                function resize() {
                    $('.tabstrip-bg table:first').css('height', $(window).height() - $('body').height() + $('.tabstrip-bg table:first').height() + 'px')
                }

                function fadeTab() {
                    setTimeout(resize, 0);
                    fadeEle('.div-edit');
                }

                function fadeEle(id)
                {
                    $(id).hide().fadeIn(500);
                }
            ");

            Window.WriteStartupJavascript("toggleActionSheet();");

        }



        // 2014.10.15
        // KL
        // reset the refresh timer only if it is a partial postback
        //
        if (IsPostBack && Page is UIPageBase && ((UIPageBase)Page).ScriptManager.IsInAsyncPostBack)
            Window.WriteJavascript("stopTimer();");


        // KL - not efficient but is at client side so should be okay.
        // @todo
        // KL - moved to framework, UIDialogBox, to update later.
        if (IsPostBack)
        {
            //Window.WriteJavascript("fadeEle('.dialog-bg');");
            //Window.WriteJavascript("fadeEle('.dialog');");
        }

        // 2010.05.18
        // Kim Foong
        // Disable the workflow save button if there are no approvers
        //
        WorkflowDialogBox.Button1.Enabled = !labelNoApproversFound.Visible;


        UIButton1.Visible = Button1_CommandName.Trim() != "";
        UIButton2.Visible = Button2_CommandName.Trim() != "";
        UIButton3.Visible = Button3_CommandName.Trim() != "";
        UIButton4.Visible = Button4_CommandName.Trim() != "";
        UIButton5.Visible = Button5_CommandName.Trim() != "";
        UIButton6.Visible = Button6_CommandName.Trim() != "";
        UIButton7.Visible = Button7_CommandName.Trim() != "";
        UIButton8.Visible = Button8_CommandName.Trim() != "";

        spanSaveButtonsInner.Visible = (
            SaveButtonsAlwaysVisible || !(o is IWorkflowEnabled));

        // This hides the workflow save button if the workflow
        // has no other buttons available.
        //
        if (datalistWorkflowButtons.Items.Count > 0)
        {
            if (datalistWorkflowButtons.Items.Count == 1 &&
                datalistWorkflowButtons.Items[0].Controls[1] is UIButton &&
                ((UIButton)datalistWorkflowButtons.Items[0].Controls[1]).CommandArgument == "-")
            {
                //if (!(this.SessionObject is OTenancyTermination &&
                //    ((OTenancyTermination)this.SessionObject).CurrentActivity.ObjectName == "TerminationConfirmed"))
                //{
                //    datalistWorkflowButtons.Items[0].Visible = false;
                //}
            }

        }

        // 2011.11.22
        // Kim Foong
        // Disable the previous/next buttons on the first/last tab.
        //
        UITabStrip tabstrip = GetTabStrip(this.Page);
        if (tabstrip != null)
        {
            PrevTabButton.Enabled = tabstrip.AbleToGoToPreviousTab();
            NextTabButton.Enabled = tabstrip.AbleToGoToNextTab();

            if (IsPostBack && tabstrip.PreviousSelectedIndex != tabstrip.SelectedIndex)
            {
                Window.WriteJavascript("fadeTab();");
            }

            // Generate the dropdown list for the responsive tab.
            //
            dropTab.Items.Clear();
            //if (!IsPostBack)
            {
                var tabViews = tabstrip.TabViews;
                int itemCount = 0;
                foreach (UITabView tabView in tabViews)
                {
                    if (tabView.Visible)
                    {
                        dropTab.Items.Add(new ListItem(tabView.Caption.Replace("<sup>", "(").Replace("</sup>", ")"), tabView.TabIndex.ToString()));
                        if (tabView.TabIndex == tabstrip.SelectedIndex)
                            dropTab.SelectedIndex = itemCount;
                        itemCount++;
                    }
                }
            }
        }

        // 2011.09.08
        // Kim Foong
        // If the message text was changed, then show the animation.
        //
        if (messageChanged)
            Window.WriteStartupJavascript(@"showToast('mdl_toast', '" + labelMessage.Text.Replace("'", "\\'").Replace("\"", "\\\"") + @"', 'OK')");

        // 2013.10.09
        // Kim Foong
        // Mobile Web - Hide the summary tabs
        //
        if (((UIPageBase)this.Page).MobileMode && TabStrip != null)
        {
            TabStrip.ShowSummaryAsFirstTabView = false;
            TabStrip.ShowSummaryAsLastTabView = false;
        }


        //disable urgent checkbox if in summary tab
        if (tabstrip != null &&
            (tabstrip.SelectedTab == tabstrip.FirstSummaryTabView ||
            tabstrip.SelectedTab == tabstrip.LastSummaryTabView))
        {
            PriorityFlag.Enabled = false;
        }
        else
        {
            PriorityFlag.Enabled = true;
        }

        buttonGeneratePDF.Visible = false;
        if (this.TabStrip != null &&
            this.TabStrip.SelectedTab == this.TabStrip.LastSummaryTabView)
        {
            buttonGeneratePDF.Visible = true;
        }

        OtherButtonsPanel.Visible =
            UIButton1.Visible || UIButton2.Visible || UIButton3.Visible || UIButton4.Visible ||
            UIButton5.Visible || UIButton6.Visible || UIButton7.Visible || UIButton8.Visible;

        this.panelWorkflowMenu.Visible = this.datalistWorkflowButtons.Items.Count > 0;

        Window.WriteStartupJavascript("toggleActionSheet();");
    }



    protected override void Render(HtmlTextWriter writer)
    {

        base.Render(writer);
    }



    /// <summary>
    /// Occurs when the Close button is clicked.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void buttonCancel_Click(object sender, EventArgs e)
    {
        Window.Close();
    }


    /// <summary>
    /// Occurs when the hidden Refresh button is clicked via
    /// javascript from another page.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void buttonRefresh_Click(object sender, EventArgs e)
    {
        CallPopulateFormEvent();
    }


    /// <summary>
    /// Occurs when the user clicks on the alert box to refresh
    /// the session.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void buttonRefreshSession_Click(object sender, EventArgs e)
    {
    }


    /// <summary>
    /// Occurs when the delete button is clicked.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void buttonDelete_Click(object sender, EventArgs e)
    {
        ObjectPanel.ClearErrorMessages();

        using (Connection c = new Connection())
        {
            PersistentObject obj = SessionObject;
            obj.Deactivate();

            if (Page is UIPageBase)
                ((UIPageBase)Page).ClearModifiedFlag();

            this.Message = Resources.Messages.General_ItemDeleted;
            buttOnValidateAndSave.Enabled = false;
            spanDelete3.Visible = false;
            buttOnValidateAndSaveAndClose.Enabled = false;
            datalistWorkflowButtons.Enabled = false;
            if (ObjectPanel != null && ObjectPanel is WebControl)
                ((WebControl)ObjectPanel).Enabled = false;

            c.Commit();

            if (RefreshOpenerAfterSave)
                Window.Opener.Refresh();
        }
        DisablePage();
    }



    // 2011.05.05
    // Edwin LEE
    // Get the alternate event name
    private string GetAlternateEventName()
    {
        LogicLayerWorkflowPersistentObject logicLayerPersistentObject = this.SessionObject as LogicLayerWorkflowPersistentObject;

        if (logicLayerPersistentObject.CurrentActivity != null && logicLayerPersistentObject.CurrentActivity.ApprovalProcessID != null)
        {
            int currentApprovalLevel = (int)logicLayerPersistentObject.CurrentActivity.CurrentApprovalLevel;

            // Yiyuan, 2012-08-14
            // Add the null checking for approval process and approval hierarchy
            //
            OApprovalHierarchyLevel ahl = null;
            if (logicLayerPersistentObject.CurrentActivity.ApprovalProcess != null
                && logicLayerPersistentObject.CurrentActivity.ApprovalProcess.ApprovalHierarchy != null)
                ahl = TablesLogic.tApprovalHierarchyLevel.Load(
                                            TablesLogic.tApprovalHierarchyLevel.ApprovalHierarchyID == logicLayerPersistentObject.CurrentActivity.ApprovalProcess.ApprovalHierarchy.ObjectID &
                                            TablesLogic.tApprovalHierarchyLevel.ApprovalLevel == currentApprovalLevel &
                                            TablesLogic.tApprovalHierarchyLevel.IsDeleted == 0);

            string alternateEventNameFLEE = "";
            if (ahl != null)
                alternateEventNameFLEE = ahl.AlternateEventName;



            if (alternateEventNameFLEE != "" && alternateEventNameFLEE != null)
            {
                ExpressionEvaluator e = new ExpressionEvaluator();
                e["obj"] = logicLayerPersistentObject;
                string alternateEventName = e.CompileAndEvaluate<string>(alternateEventNameFLEE);

                if (alternateEventName != "" && alternateEventName != null)
                    return alternateEventName;
            }

        }

        return "Approve";
    }


    /// <summary>
    /// Validates that there are no errors.
    /// </summary>
    protected bool IsValid()
    {
        // Note: Basic validation has already been performed,
        // as it usually happens with normal ASP.NET controls.
        //
        if (!ObjectPanel.IsValid)
        {
            this.Message = ObjectPanel.CheckErrorMessages();
            return false;
        }
        return true;
    }


    /// <summary>
    /// Saves the PersistentObject into the database. 
    /// </summary>
    /// <remarks>
    /// The following happens in this method:
    /// <list>
    /// <item>1. Binds the data entered in the form controls to the PersistentObject.</item>
    /// <item>2. All error messages are cleared from the controls within the UIObjectPanel/UITabStrip control.</item>
    /// <item>3. All basic validations are performed on all controls within the UIObjectPanel/UITabStrip control.</item>
    /// <item>4. If at least one of the controls encounter a validation error, the first error will be displayed, and
    /// the processing stops. The object is NOT saved.</item>
    /// <item>5. If the basic validation succeeds, the Validate event will be called for
    /// the application to perform other customized validation. The application must set
    /// an error message to the control that failed validation.</item>
    /// <item>6. If at least one of the controls encounter a validation error, the first error will be displayed, and
    /// the processing stops. The object is NOT saved.</item>
    /// <item>7. The Saving event will be called.</item>
    /// <item>8. The PersistentObject is saved by calling the Save() method.</item>
    /// <item>9. The PopulateForm event will be called.</item>
    /// <item>10. The Saved event will be called.</item>
    /// </list>
    /// </remarks>
    public bool SaveObjectAndTransit()
    {
        bool save = false;

        // Touch the session object to force it to save,
        // regardless whether the user has made any changes to
        // the fields.
        //
        SessionObject.Touch();

        try
        {
            // Clears all error messages.
            //
            this.ObjectPanel.ClearErrorMessages();

            // Save the object into the database
            // 
            List<OAttachment> attList = NewAttachmentsUploaded;
            using (Connection c = new Connection())
            {

                // If the object implements the IWorkflowEnabled interface, save the current login 
                // user id as the CreatedID(Object Created User ID) in the object base.
                // Added: Wang Yiyuan, 12/3/2012
                //
                if (SessionObject is LogicLayerApprovalPersistentObject && SessionObject is IWorkflowEnabled)
                {
                    string selectedAction = selectedWorkflowAction.Value;
                    if (selectedAction.StartsWith("SubmitForApproval"))
                        ((LogicLayerApprovalPersistentObject)SessionObject).SubmittorID = AppSession.User.ObjectID;
                }

                // If binding is automatic, then perform the binding
                // before calling the Save event.
                //
                if (AutomaticBindingAndSaving)
                {
                    this.ObjectPanel.BindControlsToObject(this.SessionObject);

                    // Calls the Validate event
                    //
                    if (Validate != null)
                        Validate(this.ObjectPanel, this.SessionObject);

                    if (!ObjectPanel.IsValid)
                    {
                        this.Message = ObjectPanel.CheckErrorMessages();
                        workflowButtonClicked = !String.IsNullOrEmpty(selectedWorkflowAction.Value.Trim());
                        return false;
                    }
                }

                ValidateAttachments(selectedWorkflowAction.Value);

                // Take care of the validation, pre-processing.
                // and post-processing.
                //
                if (ValidateAndSave != null)
                {
                    ValidateAndSave(this, EventArgs.Empty);
                    if (!ObjectPanel.IsValid)
                    {
                        this.Message = ObjectPanel.CheckErrorMessages();
                        workflowButtonClicked = !String.IsNullOrEmpty(selectedWorkflowAction.Value.Trim());
                        return false;
                    }
                }

                UpdateCurrentUserBusinessEntity();

                // If saving is automatic, then save the object
                // into the database.
                //
                if (AutomaticBindingAndSaving)
                {
                    // Calls the Saving event.
                    //
                    if (Saving != null)
                        Saving(this.ObjectPanel, this.SessionObject);

                    this.SessionObject.Save();
                }

                // Saves the customized object fields and attributes.
                //
                if (EnableCustomizedObjects)
                    SaveCustomizedObject(SessionObject.ObjectID.Value);

                // Perform workflow transition. Note that the Transit
                // method must be called when we are OUTSIDE of the 
                // connection boundary.
                //
                // Bear in mind that the actual transition of the workflow
                // occurs within a different thread, and has no visibility
                // of any variables kept within this thread. Also, any
                // variables set on the SessionObject before calling the 
                // Transit method will be lost, because it will be reloaded 
                // again in the workflow thread.
                //
                if (SessionObject is LogicLayerWorkflowPersistentObject)
                {
                    try
                    {
                        string selectedAction = selectedWorkflowAction.Value;
                        if (selectedAction == "Approve")
                            ((LogicLayerWorkflowPersistentObject)SessionObject).TriggerWorkflowEvent(selectedAction, GetAlternateEventName());
                        else
                            ((LogicLayerWorkflowPersistentObject)SessionObject).TriggerWorkflowEvent(selectedAction);


                    }
                    catch (WorkflowTransitionException ex)
                    {
                        this.Message =
                            String.Format(
                            Resources.Errors.Workflow_TransitionErrorRecordLocked,
                            Resources.Errors.Workflow_TransitionInvalid);
                        //this.DisablePage();
                        UpdateCurrentObjectInstanceKey();
                        throw ex;
                    }
                    catch (WorkflowAssignmentException ex)
                    {
                        this.Message =
                            String.Format(
                            Resources.Errors.Workflow_TransitionErrorRecordLocked,
                            Resources.Errors.Workflow_NoUsersToAssign);
                        //this.DisablePage();
                        UpdateCurrentObjectInstanceKey();
                        throw ex;
                    }
                    catch (Exception ex)
                    {
                        //this.DisablePage();
                        UpdateCurrentObjectInstanceKey();
                        /*throw new Exception(String.Format(
                            Resources.Errors.Workflow_TransitionErrorRecordLocked + (ex.InnerException != null ? "<br>" + ex.InnerException.Message : ""),
                            ex.Message));*/
                        Exception innerException = ex;
                        while (innerException != null)
                        {
                            if (innerException is LogicLayerValidationException || innerException is LogicLayer.ApplicationException)
                                throw innerException;

                            innerException = innerException.InnerException;
                        }

                        throw ex;
                    }
                }
                c.Commit();

            }
            if (FileScan.Enabled)
            {
                ScanAttachment(attList);
            }

            save = true;
            this.Message = String.Format(Resources.Messages.General_ItemSaved, this.Caption);
            spanDelete3.Visible = true;

            UpdateCurrentObjectInstanceKey();
            if (RefreshOpenerAfterSave)
                Window.Opener.Refresh();

            if (Page is UIPageBase)
                ((UIPageBase)Page).ClearModifiedFlag();

            // If it is found that the object is no longer assigned to
            // the current user because of a change in workflow assignments,
            // then disable the entire page.
            // 2017.02.27
            // Kien Trung
            // UPDATED: Allow to edit object when workflow enabled is not assigned.
            // Edit All to override the workflow assignment but user cannot transit the workflow status
            //
            bool disablePage = false;
            bool shouldAllowEdit = AppSession.User.AllowEditAll(GetRequestObjectType());

            if (SessionObject is LogicLayerWorkflowPersistentObject &&
                ((LogicLayerWorkflowPersistentObject)SessionObject).CurrentActivity != null &&
                (
                    !((LogicLayerWorkflowPersistentObject)SessionObject).CurrentActivity.IsAssignedToUser(AppSession.User)
                    // TDT: 12-Dec-2016
                    // Disable page if Document is in Pending Approval, and current user has already approved the document
                    //
                    ||
                    (SessionObject is LogicLayerApprovalPersistentObject &&
                        ((LogicLayerApprovalPersistentObject)SessionObject).CurrentActivity.CurrentApprovalLevel > 0 &&
                        ((LogicLayerApprovalPersistentObject)SessionObject).CurrentActivity.ApprovedUsers.Find(x => x.ObjectID == AppSession.User.ObjectID) != null
                    )
                ))
                disablePage = true;

            BindWorkflowTransitionDropdownList(this.SessionObject);

            if (disablePage && shouldAllowEdit)
            {
                disablePage = false;
                /*
                if (datalistWorkflowButtons != null && datalistWorkflowButtons.Items.Count > 0)
                {
                    foreach (DataListItem item in this.datalistWorkflowButtons.Items)
                    {
                        UIButton button = item.FindControl("buttonWorkflowAction") as UIButton;
                        if (button != null)
                            button.Enabled = (button.CommandName == "-");
                    }
                }*/
            }

            if (disablePage)
                DisablePage();


        }
        catch (OutOfMemoryException ex)
        {
            // Catch the OutOfMemory exception
            //
            Helpers.LogException(ex, this.Request);
            HttpRuntime.UnloadAppDomain();
            Response.Redirect(Request.ApplicationPath + "/applogin.aspx");
        }
        catch (ObjectModifiedException ex)
        {
            // Catch and handle the object modified exception
            //
            // Clear the URL of the message querystring
            //
            Helpers.LogException(ex, this.Request);
            string rawUrl = Page.Request.RawUrl;
            if (rawUrl.Contains("MESSAGE="))
            {
                int indexOfMessage = rawUrl.IndexOf("MESSAGE=");
                int indexOfAmpersand = rawUrl.IndexOf("&", indexOfMessage);
                if (indexOfAmpersand < 0)
                    indexOfAmpersand = rawUrl.Length;
                rawUrl = rawUrl.Remove(indexOfMessage, indexOfAmpersand - indexOfMessage);
            }

            // Redirect to this same URL again and show 
            // a new message.
            //
            Response.Redirect(rawUrl +
                (Page.Request.RawUrl.Contains("?") ? "&" : "?") +
                "MESSAGE=" + HttpUtility.UrlEncode(String.Format(Resources.Errors.General_ObjectModified, ex.ObjectType.Name, ex.ObjectID, ex.ObjectName, ex.ObjectNumber)));
        }
        catch (Exception ex)
        {
            Helpers.LogException(ex, this.Request);
            string errorMessage = ObjectPanel.CheckErrorMessages();
            if (errorMessage != null)
                this.Message = errorMessage;
            else
                ShowException(ex);

            return false;
        }

        CallPopulateFormEvent();
        BindAttachments();

        // Calls the Saved event.
        //
        if (AutomaticBindingAndSaving)
        {
            if (Saved != null)
                Saved(this.ObjectPanel, this.SessionObject);
        }

        return save;
    }

    protected void UpdateCurrentUserBusinessEntity()
    {
        if (SessionObject == null)
            return;

        PropertyInfo pi = SessionObject.GetType().GetProperty("BusinessEntityID");

        if (pi == null)
            return;

        AppSession.User.CurrentBusinessEntityID = (Guid?)pi.GetValue(SessionObject, null);

    }

    protected void PopulateCurrentUserBusinessEntity(PersistentObject o)
    {
        if (o == null || !o.IsNew || AppSession.User.CurrentBusinessEntityID == null)
            return;

        PropertyInfo pi = o.GetType().GetProperty("BusinessEntityID");

        if (pi == null)
            return;

        MethodInfo setMethod = pi.GetSetMethod();
        if (setMethod == null)
            return;

        if (pi.GetValue(o) == null)
        {
            pi.SetValue(o, AppSession.User.CurrentBusinessEntityID);

            if (!AppSession.User.AllowBusinessEntityLevelAccess(o))
                pi.SetValue(o, null);
        }
    }

    /// <summary>
    /// Shows an exception in the message label.
    /// </summary>
    /// <param name="ex"></param>
    public void ShowException(Exception ex)
    {
        if (ex is LogicLayerValidationException || ex is LogicLayer.ApplicationException)
            this.Message = ex.Message;
        else if (Helpers.IsDebug())
        {
            StringBuilder sb = new StringBuilder();
            Exception currentException = ex;
            while (currentException != null)
            {
                sb.Append(currentException.Message + "\n" + currentException.StackTrace + "\n\n");
                currentException = currentException.InnerException;
            }
            this.Message =
                HttpUtility.HtmlEncode(sb.ToString()).Replace("\n", "<br>");
        }
        else
            this.Message = String.Format(Resources.Errors.General_ErrorOccurred, DateTime.Now);
    }


    /// <summary>
    /// Shows the workflow dialog.
    /// </summary>
    protected void ShowWorkflowDialog()
    {
        try
        {
            Message = "";
            // 2016.10.07
            // Kien Trung
            // NEW: show rejection hint.
            //
            HintRejectAction.Visible = false;
            WorkflowDialogBox.Title = selectedWorkflowAction.Value;
            if (WorkflowDialogBox.Title == "Approve")
            {
                WorkflowDialogBox.Title = GetAlternateEventName();
            }
            string tmpEventName = Resources.WorkflowEvents.ResourceManager.GetString(WorkflowDialogBox.Title);
            if (tmpEventName != null && tmpEventName.Trim().Length > 0)
                WorkflowDialogBox.Title = tmpEventName;


            textTaskCurrentComments.Text = "";

            // Populate the approval process dropdown list
            //
            if (selectedWorkflowAction.Value.StartsWith("SubmitForApproval"))
            {
                // 2010.04.23
                // We must bind controls to object to get the latest
                // data entered from the user interface, so that
                // when we call GetApprovalProcesses, we have the
                // latest data to obtain the TaskLocations/
                // TaskEquipment/TaskTypeOfServices/TaskAmount.
                //
                this.ObjectPanel.BindControlsToObject(this.SessionObject);

                panelApprovalProcess.Visible = true;
                dropApprovalProcess.Visible = true;
                dropApprovalProcess.Bind(
                    OApprovalProcess.GetApprovalProcesses(
                    (LogicLayerPersistentObject)SessionObject),
                    "Description", "ObjectID", false);

                if (dropApprovalProcess.Items.Count == 1)
                    dropApprovalProcess.Enabled = false;
                else
                    dropApprovalProcess.Enabled = true;

                // 2010.05.16
                // Kim Foong
                // Resets the skip checkbox to true, so that it's always by default
                // set to true whenever the dialog box opens up.
                //
                checkSkipLevels.Checked = true;


                if (dropApprovalProcess.Items.Count > 0 &&
                    dropApprovalProcess.Items[0].Value != "")
                    ShowApprovalHierarchy(new Guid(dropApprovalProcess.Items[0].Value));
                else
                    ShowApprovalHierarchy(null);

            }
            else if (selectedWorkflowAction.Value == "Reject")
            {
                // 2016.10.07
                // Kien Trung
                // NEW: show rejection hint.
                //
                HintRejectAction.Visible = true;
                panelApprovalProcess.Visible = false;
                dropApprovalProcess.Visible = true;
            }
            else if (selectedWorkflowAction.Value == "Approve")
            {
                panelApprovalProcess.Visible = true;
                dropApprovalProcess.Visible = false;

                if (SessionObject is LogicLayerWorkflowPersistentObject)
                    ShowApprovalHierarchy(((LogicLayerWorkflowPersistentObject)SessionObject).CurrentActivity.ApprovalProcessID);
            }
            else
            {
                panelApprovalProcess.Visible = false;
                dropApprovalProcess.Visible = true;
            }

            WorkflowDialogBox.Show();
            mainPanel.Update();
        }
        catch (Exception ex)
        {
            Helpers.LogException(ex, this.Request);
            ShowException(ex);
        }
    }


    /// <summary>
    /// Show the approval hierarchy on the gridview.
    /// </summary>
    /// <param name="approvalProcessDetailId"></param>
    protected void ShowApprovalHierarchy(Guid? approvalProcessId)
    {
        // Bind the object before displaying the approval
        // hierarchy, because displaying the approval hierarchy
        // 
        // 2010.04.23
        // This should not be included here, but rather in the
        // ShowApprovalDialog above. 
        //
        // this.ObjectPanel.BindControlsToObject(this.SessionObject);

        if (approvalProcessId == null)
        {
            labelModeOfForwarding.Text = "";
            gridApprovalProcess.DataSource = null;
            gridApprovalProcess.DataBind();

            gridApprovalProcess.Visible = false;
            labelApproved.Visible = false;
            labelApprovalNotRequired.Visible = false;
            labelNoApproversFound.Visible = false;
        }
        else
        {
            OApprovalProcess approvalProcess =
                TablesLogic.tApprovalProcess.Load(approvalProcessId);

            labelModeOfForwarding.Text = approvalProcess.ModeOfForwardingText;

            if (SessionObject is LogicLayerPersistentObject)
            {
                List<OApprovalHierarchyLevel> approvalHierarchyLevels = new List<OApprovalHierarchyLevel>();
                int? nextApprovalLevel = 0;

                if ((approvalProcess.ModeOfForwarding == ApprovalModeOfForwarding.HierarchicalWithSkipping) &&
                 ((LogicLayerWorkflowPersistentObject)SessionObject).CurrentActivity.CurrentApprovalLevel == null &&
                 ((LogicLayerWorkflowPersistentObject)SessionObject).CurrentActivity.LastApprovalLevel != null)
                {

                    if (((LogicLayerWorkflowPersistentObject)SessionObject).CurrentActivity.PreviousTaskAmount !=
                        ((LogicLayerWorkflowPersistentObject)SessionObject).TaskAmount)
                        checkSkipLevels.Checked = false;
                    else
                        checkSkipLevels.Checked = true;

                    checkSkipLevels.Enabled = false;
                }

                // 2010.05.16
                // Kim Foong
                // Pass in the checkSkipLevels value to skip approval levels
                // where applicable.
                //
                ((LogicLayerWorkflowPersistentObject)SessionObject).CurrentActivity.SkipToLastRejectedLevel = checkSkipLevels.Checked ? 1 : 0;

                // 2010.05.16
                // Kim Foong
                checkSkipLevels.Visible =
                    (approvalProcess.ModeOfForwarding == ApprovalModeOfForwarding.HierarchicalWithSkipping) &&
                    ((LogicLayerWorkflowPersistentObject)SessionObject).CurrentActivity.CurrentApprovalLevel == null &&
                    ((LogicLayerWorkflowPersistentObject)SessionObject).CurrentActivity.LastApprovalLevel != null;

                // 2010.05.16
                // Kim Foong
                int approvalResult =
                    approvalProcess.GetApprovalHierarchyLevels(
                    (LogicLayerWorkflowPersistentObject)SessionObject,
                    approvalHierarchyLevels,
                    out nextApprovalLevel);

                gridApprovalProcess.DataSource = approvalHierarchyLevels;
                gridApprovalProcess.DataBind();
                gridApprovalProcess.Visible = approvalResult == ApprovalResult.ApprovalRequired;
                labelApproved.Visible = approvalResult == ApprovalResult.Approved;
                labelApprovalNotRequired.Visible = approvalResult == ApprovalResult.NoApprovalRequired;
                labelNoApproversFound.Visible = approvalResult == ApprovalResult.NoApproversFound;
            }
        }

        WorkflowDialogBox.Show();
    }


    /// <summary>
    /// Occurs when the user selects a different approval process, in
    /// which case, we will update the approval hierarchy details on
    /// the grid.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropApprovalProcess_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (dropApprovalProcess.SelectedValue == "")
            ShowApprovalHierarchy(null);
        else
            ShowApprovalHierarchy(new Guid(dropApprovalProcess.SelectedValue));
    }


    /// <summary>
    /// Occurs when the Save Item button is clicked.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void buttOnValidateAndSave_Click(object sender, EventArgs e)
    {
        bool causesValidation = true;
        if (sender is UIButton)
            causesValidation = ((UIButton)sender).CausesValidation;

        if (causesValidation && !IsValid())
            return;

        string selectedAction = selectedWorkflowAction.Value;
        if (selectedAction != "-" && selectedAction != "" &&
            (ShowWorkflowDialogBox ||
            selectedAction == "Approve" ||
            selectedAction == "Reject" ||
            selectedAction == "Query" ||
            selectedAction == "NotInOrder" ||
            selectedAction == "Cancel" ||
            selectedAction.StartsWith("SubmitForApproval")))
        {
            textClickedButton.Text = "buttOnValidateAndSave";
            bool isSubPanelValid = ProcessSubPanel(this.ObjectPanel);
            // 2010.10.17
            // Chee Meng
            // Checks if any sub panels are open and validate page for errors
            if (!causesValidation || (Page.IsValid && isSubPanelValid))
                ShowWorkflowDialog();
        }
        else
        {
            // 2010.10.17
            // Chee Meng
            // Checks if any sub panels are open and validate page for errors
            //if (SubPanelCount > 0)
            {
                bool isSubPanelValid = ProcessSubPanel(this.ObjectPanel);
                if (!causesValidation || (Page.IsValid && isSubPanelValid))
                    SaveObjectAndTransit();
            }
        }
    }

    private void SetContainerVisible(Control control)
    {
        Control c = control;

        while (c != null)
        {
            if (c.Visible)
                return;

            c.Visible = true;
            c = c.Parent;
        }
    }

    // 2010.10.17
    // Chee Meng
    // Recursive function to validate all the open subpanels
    protected bool ProcessSubPanel(Control c)
    {
        foreach (Control child in c.Controls)
        {
            if (child.GetType().FullName == "ASP.objectSubPanel")
            {
                IObjectSubPanel objectSubPanel = (IObjectSubPanel)child;
                UIObjectPanel subObjectPanel = getPanel(child);
                Page.Validate(subObjectPanel.ClientID);
                /// Manually calls the click buttonUpdate event as per normal
                /// Get the subObjectPanel error messages and append to this ObjectPanel.Message

                if (child.Visible || objectSubPanel.IsOpen)
                {
                    MethodInfo buttonUpdate_Click = child.GetType().GetMethod("buttonUpdate_Click", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance);

                    if (!child.Visible)
                        SetContainerVisible(child);

                    try
                    {
                        buttonUpdate_Click.Invoke(child, new object[] { "ProcessSubPanel", EventArgs.Empty });
                    }
                    catch (Exception ex)
                    {
                        Helpers.LogException(ex, this.Request);
                        ShowException(ex);
                        return false;
                    }

                    if (!subObjectPanel.IsValid)
                    {
                        this.Message = subObjectPanel.CheckErrorMessages();
                        return false;
                    }

                    child.Visible = false;
                    subObjectPanel.Visible = false;
                }
            }
            else
            {
                bool isSubPanelValid = ProcessSubPanel(child);

                if (!isSubPanelValid)
                    return false;
            }
        }

        return true;
    }

    // 2010.10.17
    // Chee Meng
    // Gets UIObjectPanel from control
    protected UIObjectPanel getPanel(Control c)
    {
        c = c.Parent;
        while (c != null)
        {
            if (c is UIObjectPanel)
                break;
            c = c.Parent;
        }
        if (c != null && c is UIObjectPanel)
            return (UIObjectPanel)c;
        else
            return null;
    }


    UITabStrip firstTabstrip = null;


    /// <summary>
    /// Finds the first UITabStrip available.
    /// </summary>
    /// <param name="c"></param>
    /// <returns></returns>
    protected UITabStrip GetTabStrip(Control c)
    {
        if (firstTabstrip != null)
            return firstTabstrip;

        if (c is UITabStrip)
        {
            firstTabstrip = (UITabStrip)c;
            return (UITabStrip)c;
        }

        foreach (Control child in c.Controls)
        {
            UITabStrip tabStrip = GetTabStrip(child);
            if (tabStrip != null)
                return tabStrip;
        }
        return null;
    }


    /// <summary>
    /// Occurs when the Save and New button is clicked.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void buttOnValidateAndSaveAndNew_Click(object sender, EventArgs e)
    {
        bool causesValidation = true;
        if (sender is UIButton)
            causesValidation = ((UIButton)sender).CausesValidation;

        if (causesValidation && !IsValid())
            return;

        string selectedAction = selectedWorkflowAction.Value;
        if (selectedAction != "-" && selectedAction != "" &&
            (ShowWorkflowDialogBox ||
            selectedAction == "Approve" ||
            selectedAction == "Reject" ||
            selectedAction == "Query" ||
            selectedAction == "NotInOrder" ||
            selectedAction == "Cancel" ||
            selectedAction.StartsWith("SubmitForApproval")))
        {
            textClickedButton.Text = "buttOnValidateAndSaveAndNew";
            bool isSubPanelValid = ProcessSubPanel(this.ObjectPanel);
            // 2010.10.17
            // Chee Meng
            // Checks if any sub panels are open and validate page for errors
            if (!causesValidation || (Page.IsValid && isSubPanelValid))
                ShowWorkflowDialog();
        }
        else
        {
            // 2010.10.17
            // Chee Meng
            // Checks if any sub panels are open and validate page for errors
            //if (SubPanelCount > 0)
            {
                bool isSubPanelValid = ProcessSubPanel(this.ObjectPanel);
                if (!causesValidation || (Page.IsValid && isSubPanelValid))
                    if (SaveObjectAndTransit())
                    {
                        string queryString = "";
                        foreach (string key in Page.Request.QueryString.Keys)
                            if (key != "TYPE" && key != "ID" && key != "MESSAGE")
                                queryString += (queryString == "" ? "" : "&") + key + "=" + HttpUtility.UrlEncode(Page.Request.QueryString[key]);
                        queryString += (queryString == "" ? "" : "&") + "MESSAGE=" + Resources.Messages.General_ItemSavedAndNew;

                        Window.OpenAddObjectPage(Page, GetRequestObjectType(), queryString);
                    }
            }
        }

    }


    /// <summary>
    /// Occurs when the Save and Close button is clicked.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void buttOnValidateAndSaveAndClose_Click(object sender, EventArgs e)
    {
        bool causesValidation = true;
        if (sender is UIButton)
            causesValidation = ((UIButton)sender).CausesValidation;

        if (causesValidation && !IsValid())
            return;

        string selectedAction = selectedWorkflowAction.Value;
        if (selectedAction != "-" && selectedAction != "" &&
            (ShowWorkflowDialogBox ||
            selectedAction == "Approve" ||
            selectedAction == "Reject" ||
            selectedAction == "Query" ||
            selectedAction == "NotInOrder" ||
            selectedAction == "Cancel" ||
            selectedAction.StartsWith("SubmitForApproval")))
        {
            textClickedButton.Text = "buttOnValidateAndSaveAndClose";
            bool isSubPanelValid = ProcessSubPanel(this.ObjectPanel);
            // 2010.10.17
            // Chee Meng
            // Checks if any sub panels are open and validate page for errors
            if (!causesValidation || (Page.IsValid && isSubPanelValid))
                ShowWorkflowDialog();
        }
        else
        {
            // 2010.10.17
            // Chee Meng
            // Checks if any sub panels are open and validate page for errors
            //if (SubPanelCount > 0)
            {
                bool isSubPanelValid = ProcessSubPanel(this.ObjectPanel);
                if (!causesValidation || (Page.IsValid && isSubPanelValid))
                {
                    if (SaveObjectAndTransit())
                        Window.Close();
                }
            }
        }
    }


    protected void buttOnValidateAndSaveWorkflowPopup_Click(object sender, EventArgs e)
    {

    }



    protected void buttonCancelWorkflowPopup_Click(object sender, EventArgs e)
    {
    }

    protected void PriorityFlag_CheckedChanged(object sender, EventArgs e)
    {

    }


    public delegate void ObjectEventHandler(object sender, PersistentObject obj);

    /// <summary>
    /// Occurs immediately when the form is loaded and when the
    /// PersistentObject is saved successfully into the database.
    /// </summary>
    public event EventHandler PopulateForm;


    /// <summary>
    /// Occurs when the panel tries to save the object
    /// into the database. 
    /// </summary>
    public event EventHandler ValidateAndSave;


    /// <summary>
    /// Occurs when the panel after basic validation, 
    /// before the object is saved into the database.
    /// </summary>
    public event ObjectEventHandler Validate;


    /// <summary>
    /// Occurs when the panel after basic validation, 
    /// before the object is saved into the database.
    /// </summary>
    public event ObjectEventHandler Saving;


    /// <summary>
    /// Occurs when the panel the object is saved successfully
    /// into the database. 
    /// </summary>
    public event ObjectEventHandler Saved;


    public event ObjectEventHandler Print;

    public delegate void PagePanelClickEventHandler(object sender, string commandName);


    [Browsable(true)]
    public event PagePanelClickEventHandler Click;

    #region CustomizedObject_13Apr2007

    /// <summary>
    /// Gets or sets a flag that indicates if this object
    /// has attributes.
    /// </summary>
    private bool HasAttributeField
    {
        get
        {
            if (Session["HasAttributeField"] == null)
                return false;
            return Convert.ToBoolean(Session["HasAttributeField"]);
        }
        set
        {
            Session["HasAttributeField"] = value;
        }
    }

    /// <summary>
    /// Gets or sets a flag that indicates if the object has 
    /// records.
    /// </summary>
    private bool HasRecordField
    {
        get
        {
            if (Session["HasRecordField"] == null)
                return false;
            return Convert.ToBoolean(Session["HasRecordField"]);

        }
        set
        {
            Session["HasRecordField"] = value;
        }
    }


    /// <summary>
    /// Gets or sets the attached property name.
    /// </summary>
    private string AttachedPropertyName
    {
        get
        {
            if (Session["AttachedPropertyName"] == null)
                return null;
            else
                return Session["AttachedPropertyName"].ToString();
        }
        set
        {
            Session["AttachedPropertyName"] = value;
        }
    }


    /// <summary>
    /// Gets or sets the GUID of the attached property.
    /// </summary>
    [Browsable(false), DefaultValue(null), Localizable(false)]
    private Guid? AttributeAttachedPropertyID
    {
        get
        {
            if (Session["AttributeAttachedPropertyID"] == null)
                return null;
            return new Guid(Session["AttributeAttachedPropertyID"].ToString());
        }
        set
        {
            Session["AttributeAttachedPropertyID"] = value;
        }
    }

    /// <summary>
    /// Store a list of control ID build by Customized Attribute component, 
    /// in order to remove from the page those when necessary
    /// </summary>
    private List<OCustomizedAttributeField> CurrentAttributeFieldList
    {
        get
        {
            if (Session["AttributeFieldIDList"] == null)
                return null;
            return (List<OCustomizedAttributeField>)Session["AttributeFieldIDList"];
        }
        set
        {
            Session["AttributeFieldIDList"] = value;
        }
    }


    /// <summary>
    /// Store the id of the tabview control that hold all attribute fields, 
    /// in order to remove from the page those when necessary
    /// </summary>
    private string AttributeTabViewID
    {
        get
        {
            if (Session["AttributeTabViewID"] == null)
                return null;
            return Session["AttributeTabViewID"].ToString();
        }
        set
        {
            Session["AttributeTabViewID"] = value;
        }
    }


    /// <summary>
    /// Gets the current PersistentObjectID.
    /// </summary>
    private Guid? CurrentObjectID
    {
        get
        {
            //for edit and view request
            try
            {
                string objectID = Security.Decrypt(Request["ID"]).Split(':')[1];
                //sometimes ID is not passed in (usually for add new object)        
                return new Guid(objectID);
            }
            catch
            {
                return null;
            }
        }
    }
    private string NoAttributeMessageID
    {
        get
        {
            if (Session["AttributeMessageID"] == null)
                return null;
            return Session["AttributeMessageID"].ToString();
        }
        set
        {
            Session["AttributeMessageID"] = value;
        }
    }

    private void ClearCustomizedObjectSession()
    {
        Session["HasAttributeField"] = null;
        Session["HasRecordField"] = null;
        Session["AttachedPropertyName"] = null;
        Session["AttributeAttachedPropertyID"] = null;
        Session["AttributeFieldIDList"] = null;
        Session["AttributeTabViewID"] = null;
        Session["AttributeMessageID"] = null;
    }


    //retrieve object type id to get correct list of object record field
    protected OCustomizedRecordObject GetCurrentObjectType()
    {
        if (Request["TYPE"] != null)
        {
            string objectTypeID = GetRequestObjectType();
            List<OCustomizedRecordObject> record = TablesLogic.tCustomizedRecordObject[TablesLogic.tCustomizedRecordObject.AttachedObjectName == objectTypeID];
            if (record != null && record.Count > 0)
                return record[0];
            else
                return null;
        }
        return null;
    }
    //Add customized object record fields at the tabstrip before memo or attachment.(else last tabstrip)
    protected void InitObjectRecordFields()
    {
        OCustomizedRecordObject record = GetCurrentObjectType();
        if (record != null)
        {
            InitCustomizedFields(Resources.Strings.CustomizedObject_Fields, (record.TabViewPosition == null ? -1 : record.TabViewPosition.Value), record.CustomizedRecordFields, null);
            HasRecordField = true;
        }
        else
        {
            HasRecordField = false;
        }
    }
    protected void PopulateCustomizedRecordFieldValues()
    {
        if (HasRecordField)
            BindCustomizedFieldValues(TablesLogic.tCustomizedRecordFieldValue[TablesLogic.tCustomizedRecordFieldValue.AttachedObjectID == CurrentObjectID]);

    }

    protected void SaveCustomizedRecordObject(Guid objID)
    {
        if (!HasRecordField)
            return;
        OCustomizedRecordObject record = GetCurrentObjectType();
        if (record != null)
        {
            using (Connection c = new Connection())
            {
                foreach (OCustomizedRecordField field in record.CustomizedRecordFields)
                {
                    //do not set value for separator field
                    if (field.ControlType == "Separator")
                        continue;
                    //update existing record value if exist, else create new
                    List<OCustomizedRecordFieldValue> fieldValues = TablesLogic.tCustomizedRecordFieldValue[TablesLogic.tCustomizedRecordFieldValue.AttachedObjectID == objID & TablesLogic.tCustomizedRecordFieldValue.ColumnName == field.ColumnName];
                    OCustomizedRecordFieldValue fieldValue;
                    if (fieldValues == null || fieldValues.Count == 0)
                    {
                        fieldValue = TablesLogic.tCustomizedRecordFieldValue.Create();
                        fieldValue.AttachedObjectID = objID;
                        fieldValue.ColumnName = field.ColumnName;
                    }
                    else
                        fieldValue = fieldValues[0];
                    Control control = Page.FindControl(field.ColumnName);
                    if (control != null && control is UIFieldBase)
                    {
                        if (((UIFieldBase)control).ControlValue != null && ((UIFieldBase)control).ControlValue.ToString().Trim() != "")
                            fieldValue.FieldValue = Convert.ToString(((UIFieldBase)control).ControlValue);
                        else
                            fieldValue.FieldValue = null;
                        //do not save if the field value is empty for add case
                        if (!fieldValue.IsNew || (fieldValue.IsNew && fieldValue.FieldValue != null))
                            fieldValue.Save();
                    }
                }
                c.Commit();
            }

        }

    }



    protected void InitObjectAttributeFields()
    {
        //initialize attached property name and attribute tab view ID
        if (AttachedPropertyName == null)
        {
            string[] setting = OCustomizedObjectConsumer.GetAttachedObjectSetting(BaseTable);
            if (setting != null)
            {
                AttachedPropertyName = setting[0];
                AttributeTabViewID = setting[1];
            }
            else
            {
                EmptyAttributeSetting();
                return;
            }
        }

        //initialize attribute attached property at the first time an object is edited (not for add case)
        if (AttributeAttachedPropertyID == null && CurrentObjectID != null)
        {
            AttributeAttachedPropertyID = OCustomizedObjectConsumer.GetAttachedObjectAttributeID(BaseTable, AttachedPropertyName, CurrentObjectID.Value);
            GetAttributeFieldsForInitialization();
        }

        if (AttributeTabViewID == null)
            return;
        if (HasAttributeField)
        {
            InitCustomizedFields(null, -1, CurrentAttributeFieldList, AttributeTabViewID);
        }
    }
    private void InitNoAttributeMessage()
    {
        if (AttributeTabViewID != null)
        {
            UITabView tabView = Page.FindControl(AttributeTabViewID) as UITabView;
            UIFieldLabel message;
            if (NoAttributeMessageID == null || Page.FindControl(NoAttributeMessageID) == null)
            {
                //for attribute fields, if there is no fields, display a label that say there is no attribute fields                      
                message = new UIFieldLabel();
                tabView.Controls.Add(message);
                message.ID = "___customizedAttributesMessage___";
                message.ForeColor = System.Drawing.Color.Red;
                message.ControlValue = Resources.Messages.CustomizedObject_NoAttributeFields;
                //message.CaptionWidth = new Unit("1");
                NoAttributeMessageID = message.ID;
            }
        }
    }

    /// <summary>
    /// set all flag for initialization and loading of attribute fields to false to avoid any attempt to build attribute fields
    /// </summary>
    private void EmptyAttributeSetting()
    {
        CurrentAttributeFieldList = null;
        HasAttributeField = false;
        AttributeAttachedPropertyID = null;
    }
    /// <summary>
    /// retrieve attribute fields based on the latest attribute attachedpropertyID. 
    /// And update the currentAttributeFieldList, and hasattributeField
    /// </summary>
    /// <returns></returns>
    private void GetAttributeFieldsForInitialization()
    {
        if (AttributeAttachedPropertyID == null)
        {
            EmptyAttributeSetting();
            return;
        }
        else
        {
            List<OCustomizedAttributeField> fields = OCustomizedObjectConsumer.GetCustomizedAttributeFields(BaseTable, AttributeAttachedPropertyID.Value, true);
            if (fields == null || fields.Count == 0)
                HasAttributeField = false;
            else
            {
                HasAttributeField = true;
                if (CurrentAttributeFieldList == null)
                    CurrentAttributeFieldList = new List<OCustomizedAttributeField>();
                CurrentAttributeFieldList = fields;
            }

        }

    }
    /// <summary>
    /// this function will be called by the page that display attribute fields, whenever the attribte type if changed. (i.e. LocationTypeID is change)
    /// rebuild the attribute fields and set attributeattachedpropertyId to new id
    /// </summary>

    public void UpdateCustomizedAttributeFields(Guid? newPropertyID)
    {
        InitNoAttributeMessage();
        //if the same propertyid, do not proceed          
        if (AttributeAttachedPropertyID != null && newPropertyID != null && AttributeAttachedPropertyID.Value.ToString().Equals(newPropertyID.Value.ToString()))
            return;
        //Remove the current attribute fields
        //set the current attribute visibility to false
        if (CurrentAttributeFieldList != null)
        {
            foreach (OCustomizedAttributeField field in CurrentAttributeFieldList)
            {
                Control control = Page.FindControl(field.ColumnName);
                if (control != null)
                    control.Visible = false;
            }
        }
        EmptyAttributeSetting();
        AttributeAttachedPropertyID = newPropertyID;
        GetAttributeFieldsForInitialization();
        if (CurrentAttributeFieldList != null && CurrentAttributeFieldList.Count > 0)
        {
            if (AttributeTabViewID == null)
                return;
            InitCustomizedFields(null, -1, CurrentAttributeFieldList, AttributeTabViewID);
        }
    }

    protected void PopulateCustomizedAttributeFieldValues()
    {
        if (!HasAttributeField)
            return;
        BindCustomizedFieldValues(TablesLogic.tCustomizedAttributeFieldValue[TablesLogic.tCustomizedAttributeFieldValue.AttachedObjectID == CurrentObjectID & TablesLogic.tCustomizedAttributeFieldValue.AttachedPropertyID == AttributeAttachedPropertyID.Value]);
    }

    protected void SaveCustomizedAttributeObject(Guid objID)
    {
        if (!HasAttributeField)
            return;
        //get all the attribute fields for the object
        List<OCustomizedAttributeField> fields = OCustomizedObjectConsumer.GetCustomizedAttributeFields(BaseTable, AttributeAttachedPropertyID.Value, false);
        if (fields == null || fields.Count == 0)
            return;
        using (Connection c = new Connection())
        {
            foreach (OCustomizedAttributeField field in fields)
            {
                if (field.IsVisible == 0)
                    continue;
                //update existing record value if exist, else create new
                List<OCustomizedAttributeFieldValue> fieldValues = TablesLogic.tCustomizedAttributeFieldValue[TablesLogic.tCustomizedAttributeFieldValue.AttachedObjectID == objID & TablesLogic.tCustomizedAttributeFieldValue.AttachedPropertyID == AttributeAttachedPropertyID.Value & TablesLogic.tCustomizedAttributeFieldValue.ColumnName == field.ColumnName];
                OCustomizedAttributeFieldValue fieldValue;
                if (fieldValues == null || fieldValues.Count == 0)
                {
                    fieldValue = TablesLogic.tCustomizedAttributeFieldValue.Create();
                    //fieldValue.AttachedObjectID = currentObject.ObjectID;
                    fieldValue.AttachedObjectID = SessionObject.ObjectID;
                    fieldValue.AttachedPropertyID = AttributeAttachedPropertyID;
                    fieldValue.ColumnName = field.ColumnName;
                }
                else
                    fieldValue = fieldValues[0];
                Control control = Page.FindControl(field.ColumnName);
                if (control != null && control is UIFieldBase)
                {
                    if (((UIFieldBase)control).ControlValue != null && ((UIFieldBase)control).ControlValue.ToString().Trim() != "")
                        fieldValue.FieldValue = Convert.ToString(((UIFieldBase)control).ControlValue);
                    else
                        fieldValue.FieldValue = null;
                    //do not save if the field value is empty for add case
                    if (!fieldValue.IsNew || (fieldValue.IsNew && fieldValue.FieldValue != null))
                        fieldValue.Save();
                }
            }
            c.Commit();
        }
    }
    protected void SaveCustomizedObject(Guid objID)
    {
        //Save object record value
        SaveCustomizedRecordObject(objID);
        //Save object attribute value
        SaveCustomizedAttributeObject(objID);

    }
    protected void BindCustomizedFieldValues(IEnumerable fieldData)
    {
        foreach (OCustomizedFieldValue field in fieldData)
        {
            Control control = Page.FindControl(field.ColumnName);
            if (control != null && control is UIFieldBase)
            {
                //to avoid user updating the setting of the control such as changing from string to integer type, do not display those value
                try
                {
                    UIFieldBase fieldControl = (UIFieldBase)control;
                    if (fieldControl.ValidateDataTypeCheck)
                    {
                        if (fieldControl.ValidationDataType == ValidationDataType.Currency)
                            Convert.ToDecimal(field.FieldValue);
                        else if (fieldControl.ValidationDataType == ValidationDataType.Date)
                            Convert.ToDateTime(field.FieldValue);
                        else if (fieldControl.ValidationDataType == ValidationDataType.Double)
                            Convert.ToDouble(field.FieldValue);
                        else if (fieldControl.ValidationDataType == ValidationDataType.Integer)
                            Convert.ToInt32(field.FieldValue);
                    }
                    //Control such as checkbox only accept boolean or int value, not string. Convert to boolean first before pass in                          
                    if (control is UIFieldCheckBox)
                        ((UIFieldBase)control).ControlValue = Convert.ToBoolean(field.FieldValue);
                    else
                        ((UIFieldBase)control).ControlValue = field.FieldValue;

                }
                catch
                {
                }

            }
        }
    }
    protected void PopulateCustomizedObject()
    {
        PopulateCustomizedRecordFieldValues();
        //populate object attribute details
        PopulateCustomizedAttributeFieldValues();

    }

    protected void InitCustomizedObject()
    {
        //Add customized object record fields at the tabstrip at the stage that populate form has been called and on every postback
        InitObjectRecordFields();
        //add object attribute fields
        InitObjectAttributeFields();
        InitNoAttributeMessage();
    }

    public UITabView GetTabView(ControlCollection controls, ref int remainTabViewCount)
    {
        foreach (Control ctl in controls)
        {
            if (ctl is UITabView)
            {
                remainTabViewCount--;
                if (remainTabViewCount == 0)
                    return (UITabView)ctl;
                else
                    continue;
            }
            if (ctl.Controls.Count > 0)
            {
                UITabView tabview = GetTabView(ctl.Controls, ref remainTabViewCount);
                if (tabview != null)
                    return tabview;
            }
        }
        return null;
    }
    /// <summary>
    /// //Add customized object record fields at the tabstrip before memo or attachment.(else last tabstrip)
    /// </summary>
    /// <param name="tabViewCaption"></param>
    /// <param name="tabViewPosition"></param>
    /// <param name="fields"></param>
    /// <param name="tabViewID">pass in a null value if a new tabview is to be created to add the fields</param>
    /// <returns>return the tabview ID that is created from this method</returns>
    protected void InitCustomizedFields(string tabViewCaption, int? tabViewPosition, IEnumerable fields, string controlID)
    {
        Control container = null;
        //if the container controlID exist, do not create new control
        if (controlID != null)
        {
            container = Page.FindControl(controlID);
            if (container == null)
                throw new Exception("Error encountered while building customized object control. There is no control with id: " + controlID + " exist in the page");
            if (container is UITabView)
                ((UITabView)container).Update();
        }
        else if (tabViewPosition != null)
        {
            UITabStrip tabStrip = GetPageTabStrip(Page);
            if (tabStrip != null)
            {
                int i = tabViewPosition.Value;
                UITabView tabView = GetTabView(tabStrip.Controls, ref i);
                if (tabView != null)
                    container = tabView;
            }
            if (container != null)
                ((UITabView)container).Update();
        }
        if (container == null)
        {
            //create a tab view to accomodate all of the fields
            container = new UITabView();
            ((UITabView)container).CssClass = "div-form";
            ((UITabView)container).SkinID = "tabviewEdit";
            //Explicit set the tabview ID
            container.ID = "___customizedAttributeContainer___";
            InsertTabviewToPage(((UITabView)container), tabViewPosition);
            ((UITabView)container).Caption = tabViewCaption;
            ((UITabView)container).Update();
        }
        //Make sure that customized control values are not "lost" when control is created again
        List<UIFieldBase> customizedcontrols = null;
        if (Session["CustomizedControls"] != null)
            customizedcontrols = (List<UIFieldBase>)Session["CustomizedControls"];
        if (container is UITabView)
            Session.Add("CustomizedControls", OCustomizedObjectConsumer.BuildCustomizedControls(((UITabView)container), fields, customizedcontrols == null ? null : customizedcontrols));
        else
            Session.Add("CustomizedControls", OCustomizedObjectConsumer.BuildCustomizedControls(container, fields, customizedcontrols == null ? null : customizedcontrols));
    }

    /// <summary>
    /// Get current customized attribute values as key-value pairs
    /// </summary>
    /// <returns></returns>
    public Hashtable GetCustomizedAttributeValues()
    {
        Hashtable ht = new Hashtable();
        //get all the attribute fields for the object
        List<OCustomizedAttributeField> fields = OCustomizedObjectConsumer.GetCustomizedAttributeFields(BaseTable, AttributeAttachedPropertyID.Value, false);
        if (fields == null || fields.Count == 0)
            return ht;
        foreach (OCustomizedAttributeField field in fields)
        {
            if (field.IsVisible == 0)
                continue;
            Control control = Page.FindControl(field.ColumnName);
            if (control != null && control is UIFieldBase)
            {
                if (((UIFieldBase)control).ControlValue != null)
                    ht.Add(((UIFieldBase)control).Caption, Convert.ToString(((UIFieldBase)control).ControlValue));
                else
                    ht.Add(((UIFieldBase)control).Caption, null);
            }
        }
        return ht;
    }

    protected void InsertTabviewToPage(UITabView tab, int? position)
    {
        UITabStrip tabStrip = GetPageTabStrip(Page);
        //Hash table contain the position for each tab. Since the tabstrip control contains more than just tabview
        Hashtable viewIndex = new Hashtable();
        //get number of visible tab views in the tabstrip
        int count = 0;
        for (int i = 0; i < tabStrip.Controls.Count; i++)
        {
            if (tabStrip.Controls[i] is UITabView && tabStrip.Controls[i].Visible)
            {
                count++;
                viewIndex.Add(count, i);
            }
        }
        //if the desired position is higher than number of visible tabview, insert the tabview at the end, else at desired position
        if (position == null || position < 0 || position > count)
            tabStrip.Controls.Add(tab);
        else
        {
            //Get the index of the tabview currently at the tabviewposition
            int tabIndex = Convert.ToInt32(viewIndex[position.Value]);
            tabStrip.Controls.AddAt(tabIndex + 1, tab);
        }
    }
    protected UITabStrip GetPageTabStrip(Control c)
    {
        if (c.GetType() == typeof(UITabStrip))
            return (UITabStrip)c;
        foreach (Control child in c.Controls)
        {
            UITabStrip o = GetPageTabStrip(child);
            if (o != null)
                return o;
        }
        return null;
    }
    #endregion
    //end Rachel


    /// <summary>
    /// Binds the URL of the document template to the hyperlink.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void listDocumentTemplatesList_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        UIButton btn = e.Item.FindControl("downloadTemplate") as UIButton;
        ODocumentTemplate dt = e.Item.DataItem as ODocumentTemplate;

        Hashtable numberOfTimesPrinted = ODocument.GetNumberOfTimesPrinted(this.SessionObject.ObjectID);
        if (dt != null)
        {
            if (numberOfTimesPrinted[dt.ObjectID.Value] != null)
                numberOfTimesPrinted[dt.ObjectID.Value] = (int)numberOfTimesPrinted[dt.ObjectID.Value] + 1;
            else
                numberOfTimesPrinted[dt.ObjectID.Value] = 1;
        }

        btn.ID = Security.EncryptToHex(dt.ObjectID.ToString());
        btn.ImageUrl = "~/images/print-white-16.png";
        btn.CausesValidation = false;
        btn.AlwaysEnabled = true;
        btn.CssClass = "btn btnblack";

        int numberOfTimesPrintedInt = 0;
        if (numberOfTimesPrinted[dt.ObjectID.Value] != null)
            numberOfTimesPrintedInt = (int)numberOfTimesPrinted[dt.ObjectID.Value];
        string NumberOfTimesPrinted = "<span style='font-size:9px;'>" + String.Format(Resources.Strings.DocumentTemplateLog_ButtonText, numberOfTimesPrintedInt) + "</span>";
        btn.Text = dt.FileDescription + " " + NumberOfTimesPrinted;
        btn.Height = buttonDelete.Height;
        //btn.InlineBlockText = true;
        btn.Click += new EventHandler(documentTemplate_Click);

        if (_PrintTemplateCode != null && _PrintEventHandler != null)
        {
            if (dt.DocumentTemplateCode.Is(_PrintTemplateCode))
                btn.Click += _PrintEventHandler;
        }
    }


    /// <summary>
    /// Clone the object.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void buttonClone_Click(object sender, EventArgs e)
    {
        ICloneable c = this.SessionObject as ICloneable;

        if (c != null)
        {
            Window.OpenAddObjectPage(this.Page, c.Clone() as PersistentObject, "MESSAGE=" + HttpUtility.UrlEncode(Resources.Messages.Generate_ItemCloned));
        }
    }

    protected void buttonPrint_Click(object sender, EventArgs e)
    {
        if (Print != null)
            Print(this, CurrentObject);
    }


    /// <summary>
    /// Display the total approvals given.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridApprovalProcess_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }

    // 2010.05.16
    // Kim Foong
    // Implementation of the choice to allow skipping or not.
    //
    protected void checkSkipLevels_CheckedChanged(object sender, EventArgs e)
    {
        if (dropApprovalProcess.SelectedValue == "")
            ShowApprovalHierarchy(null);
        else
            ShowApprovalHierarchy(new Guid(dropApprovalProcess.SelectedValue));
    }


    // 2010.10.15
    // Kim Foong
    // A flag that indicates if the workflow separator visibility has already
    // been set once if the datalistWorkflowButtons_ItemDataBound method.
    //
    bool spanWorkflowSeparatorVisibilitySet = false;


    // 2010.10.15
    // Kim Foong
    /// <summary>
    /// Creates the workflow buttons
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void datalistWorkflowButtons_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        System.Data.DataRowView drv = e.Item.DataItem as System.Data.DataRowView;

        UIButton buttonWorkflowAction = e.Item.FindControl("buttonWorkflowAction") as UIButton;
        if (buttonWorkflowAction != null)
        {
            buttonWorkflowAction.CommandArgument = drv["CommandName"].ToString();
            buttonWorkflowAction.CommandName = drv["CommandName"].ToString();
            buttonWorkflowAction.Text = drv["Text"].ToString();
            buttonWorkflowAction.ImageUrl = drv["ImageUrl"].ToString();
            buttonWorkflowAction.IconCssClass = drv["IconCssClass"].ToString();
            buttonWorkflowAction.IconText = drv["IconText"].ToString();
            //Bypass validation if action is "Cancel"
            buttonWorkflowAction.CausesValidation = buttonWorkflowAction.Text.ToUpper() != "CANCEL";
            buttonWorkflowAction.ButtonType = UIButtonType.NormalFlat;
            buttonWorkflowAction.ButtonColor = UIButtonColor.Default;

        }
    }


    // 2010.10.15
    // Kim Foong
    /// <summary>
    /// Occurs when the user click the workflow buttons.
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void datalistWorkflowButtons_ItemCommand(object source, DataListCommandEventArgs e)
    {
        UIButton button = e.Item.FindControl("buttonWorkflowAction") as UIButton;

        if (datalistWorkflowButtons.Enabled &&
            button != null && button.Visible && button.IsContainerEnabled() && button.Enabled)
        {
            selectedWorkflowAction.Value = e.CommandArgument.ToString();
            buttOnValidateAndSave_Click(this.GetWorkflowActionButton(selectedWorkflowAction.Value), EventArgs.Empty);
        }
    }


    /// <summary>
    /// Occurs when the user clicks on the save button (in the workflow panel)
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void buttonWorkflowSave_Click(object sender, EventArgs e)
    {
        UIButton button = sender as UIButton;

        if (datalistWorkflowButtons.Enabled &&
            button != null && button.Visible && button.IsContainerEnabled() && button.Enabled)
        {
            selectedWorkflowAction.Value = "-";
            buttOnValidateAndSave_Click(this.GetWorkflowActionButton(selectedWorkflowAction.Value), EventArgs.Empty);
        }
    }


    // 2012.01.11
    // Kim Foong
    /// <summary>
    /// Click the workflow button and save. This is used by the 
    /// developer to programmatically execute the click and let
    /// the framework perform the rest of the screen updates, 
    /// processing automatically.
    /// 
    /// Note that a call to this will cause the page to validate
    /// if the workflow button is configured to cause validation;
    /// regardless of whether the original event that triggered 
    /// the call to this method causes validation or not.
    /// </summary>
    /// <param name="workflowEventName"></param>
    public void ClickWorkflowButton(string workflowEventName)
    {
        UIButton button = this.GetWorkflowActionButton(workflowEventName);

        if (button != null)
        {
            if (button.CausesValidation)
                Page.Validate();
            selectedWorkflowAction.Value = workflowEventName;
            buttOnValidateAndSave_Click(button, EventArgs.Empty);
        }
        else
            throw new Exception("There is no such event with the name '" + workflowEventName + "'.");
    }


    // 2011.02.07
    // Kim Foong
    /// <summary>
    /// Occurs when the previous button is clicked
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void PrevTabButton_Click(object sender, EventArgs e)
    {
        TabStrip.GoToPreviousTab();
    }

    // 2011.02.07
    // Kim Foong
    /// <summary>
    /// Occurs when the next button is clicked
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void NextTabButton_Click(object sender, EventArgs e)
    {
        TabStrip.GoToNextTab();
    }

    protected void buttonJournalTransaction_Click(object sender, EventArgs e)
    {
        OFunction f = OFunction.GetFunctionByObjectType("OGLAccount");

        string url = this.Page.ResolveUrl(f.EditUrl);

        url = url.Substring(0, url.IndexOf("glaccount/edit.aspx", StringComparison.CurrentCultureIgnoreCase));

        this.FocusWindow = false;
        Window.Open(url + "journaltransaction/journaltransaction.aspx?" + QueryString.New("objTYPE", Security.Encrypt(this.SessionObject.GetType().BaseType.Name.ToString())) + QueryString.New("objID", Security.Encrypt(this.SessionObject.ObjectID.ToString())));
    }

    protected void buttonDocumentLog_Click(object sender, EventArgs e)
    {
        OFunction f = OFunction.GetFunctionByObjectType("ODocumentTemplate");

        string url = this.Page.ResolveUrl(f.EditUrl);

        url = url.Substring(0, url.IndexOf("edit.aspx"));

        this.FocusWindow = false;
        Window.Open(url + "documentlog.aspx?" + QueryString.New("objTYPE", Security.Encrypt(this.SessionObject.GetType().BaseType.Name.ToString())) + QueryString.New("objID", Security.Encrypt(this.SessionObject.ObjectID.ToString())));
    }


    /// <summary>
    /// Validate attachments.
    /// </summary>
    protected void BindAttachments()
    {
        LogicLayerPersistentObject o = this.SessionObject as LogicLayerPersistentObject;
        if (o == null)
            return;
        if (objectAttachment == null)
            return;

        objectAttachment.GetType().GetMethod("BindAttachments", BindingFlags.Public | BindingFlags.Instance)
            .Invoke(objectAttachment, new object[] { this.SessionObject });
    }


    /// <summary>
    /// Validate attachments.
    /// </summary>
    protected void ValidateAttachments(String workflowAction)
    {
        if (objectAttachment == null)
            return;

        objectAttachment.GetType().GetMethod("ValidateAttachments", BindingFlags.Public | BindingFlags.Instance).Invoke(objectAttachment, new object[] { workflowAction });
    }


    //--------------------------------------------
    // button1
    //--------------------------------------------
    [Localizable(true)]
    public string Button1_Caption
    {
        get { return UIButton1.Text; }
        set { UIButton1.Text = value; }
    }

    public string Button1_ImageUrl
    {
        get { return UIButton1.ImageUrl; }
        set { UIButton1.ImageUrl = value; }
    }

    public string Button1_CommandName
    {
        get { return UIButton1.CommandName; }
        set { UIButton1.CommandName = value; }
    }

    public bool Button1_CausesValidation
    {
        get { return UIButton1.CausesValidation; }
        set { UIButton1.CausesValidation = value; }
    }

    public bool Button1_AlwaysEnabled
    {
        get { return UIButton1.AlwaysEnabled; }
        set { UIButton1.AlwaysEnabled = value; }
    }

    public bool Button1_Visible
    {
        get { return Button1Span.Visible; }
        set { Button1Span.Visible = value; }
    }

    [Localizable(true)]
    public string Button1_ConfirmText
    {
        get { return UIButton1.ConfirmText; }
        set { UIButton1.ConfirmText = value; }
    }


    //--------------------------------------------
    // button2
    //--------------------------------------------
    [Localizable(true)]
    public string Button2_Caption
    {
        get { return UIButton2.Text; }
        set { UIButton2.Text = value; }
    }

    public string Button2_ImageUrl
    {
        get { return UIButton2.ImageUrl; }
        set { UIButton2.ImageUrl = value; }
    }

    public string Button2_CommandName
    {
        get { return UIButton2.CommandName; }
        set { UIButton2.CommandName = value; }
    }

    public bool Button2_CausesValidation
    {
        get { return UIButton2.CausesValidation; }
        set { UIButton2.CausesValidation = value; }
    }

    public bool Button2_AlwaysEnabled
    {
        get { return UIButton2.AlwaysEnabled; }
        set { UIButton2.AlwaysEnabled = value; }
    }

    public bool Button2_Visible
    {
        get { return Button2Span.Visible; }
        set { Button2Span.Visible = value; }
    }

    [Localizable(true)]
    public string Button2_ConfirmText
    {
        get { return UIButton2.ConfirmText; }
        set { UIButton2.ConfirmText = value; }
    }


    //--------------------------------------------
    // button3
    //--------------------------------------------
    [Localizable(true)]
    public string Button3_Caption
    {
        get { return UIButton3.Text; }
        set { UIButton3.Text = value; }
    }

    public string Button3_ImageUrl
    {
        get { return UIButton3.ImageUrl; }
        set { UIButton3.ImageUrl = value; }
    }

    public string Button3_CommandName
    {
        get { return UIButton3.CommandName; }
        set { UIButton3.CommandName = value; }
    }

    public bool Button3_CausesValidation
    {
        get { return UIButton3.CausesValidation; }
        set { UIButton3.CausesValidation = value; }
    }

    public bool Button3_AlwaysEnabled
    {
        get { return UIButton3.AlwaysEnabled; }
        set { UIButton3.AlwaysEnabled = value; }
    }

    public bool Button3_Visible
    {
        get { return Button3Span.Visible; }
        set { Button3Span.Visible = value; }
    }

    [Localizable(true)]
    public string Button3_ConfirmText
    {
        get { return UIButton3.ConfirmText; }
        set { UIButton3.ConfirmText = value; }
    }



    //--------------------------------------------
    // button4
    //--------------------------------------------
    [Localizable(true)]
    public string Button4_Caption
    {
        get { return UIButton4.Text; }
        set { UIButton4.Text = value; }
    }

    public string Button4_ImageUrl
    {
        get { return UIButton4.ImageUrl; }
        set { UIButton4.ImageUrl = value; }
    }

    public string Button4_CommandName
    {
        get { return UIButton4.CommandName; }
        set { UIButton4.CommandName = value; }
    }

    public bool Button4_CausesValidation
    {
        get { return UIButton4.CausesValidation; }
        set { UIButton4.CausesValidation = value; }
    }

    public bool Button4_AlwaysEnabled
    {
        get { return UIButton4.AlwaysEnabled; }
        set { UIButton4.AlwaysEnabled = value; }
    }

    public bool Button4_Visible
    {
        get { return Button4Span.Visible; }
        set { Button4Span.Visible = value; }
    }

    [Localizable(true)]
    public string Button4_ConfirmText
    {
        get { return UIButton4.ConfirmText; }
        set { UIButton4.ConfirmText = value; }
    }


    //--------------------------------------------
    // button5
    //--------------------------------------------
    [Localizable(true)]
    public string Button5_Caption
    {
        get { return UIButton5.Text; }
        set { UIButton5.Text = value; }
    }

    public string Button5_ImageUrl
    {
        get { return UIButton5.ImageUrl; }
        set { UIButton5.ImageUrl = value; }
    }

    public string Button5_CommandName
    {
        get { return UIButton5.CommandName; }
        set { UIButton5.CommandName = value; }
    }

    public bool Button5_CausesValidation
    {
        get { return UIButton5.CausesValidation; }
        set { UIButton5.CausesValidation = value; }
    }

    public bool Button5_AlwaysEnabled
    {
        get { return UIButton5.AlwaysEnabled; }
        set { UIButton5.AlwaysEnabled = value; }
    }

    public bool Button5_Visible
    {
        get { return Button5Span.Visible; }
        set { Button5Span.Visible = value; }
    }

    [Localizable(true)]
    public string Button5_ConfirmText
    {
        get { return UIButton5.ConfirmText; }
        set { UIButton5.ConfirmText = value; }
    }


    //--------------------------------------------
    // button6
    //--------------------------------------------
    [Localizable(true)]
    public string Button6_Caption
    {
        get { return UIButton6.Text; }
        set { UIButton6.Text = value; }
    }

    public string Button6_ImageUrl
    {
        get { return UIButton6.ImageUrl; }
        set { UIButton6.ImageUrl = value; }
    }

    public string Button6_CommandName
    {
        get { return UIButton6.CommandName; }
        set { UIButton6.CommandName = value; }
    }

    public bool Button6_CausesValidation
    {
        get { return UIButton6.CausesValidation; }
        set { UIButton6.CausesValidation = value; }
    }

    public bool Button6_AlwaysEnabled
    {
        get { return UIButton6.AlwaysEnabled; }
        set { UIButton6.AlwaysEnabled = value; }
    }

    public bool Button6_Visible
    {
        get { return Button6Span.Visible; }
        set { Button6Span.Visible = value; }
    }

    [Localizable(true)]
    public string Button6_ConfirmText
    {
        get { return UIButton6.ConfirmText; }
        set { UIButton6.ConfirmText = value; }
    }


    //--------------------------------------------
    // button7
    //--------------------------------------------
    [Localizable(true)]
    public string Button7_Caption
    {
        get { return UIButton7.Text; }
        set { UIButton7.Text = value; }
    }

    public string Button7_ImageUrl
    {
        get { return UIButton7.ImageUrl; }
        set { UIButton7.ImageUrl = value; }
    }

    public string Button7_CommandName
    {
        get { return UIButton7.CommandName; }
        set { UIButton7.CommandName = value; }
    }

    public bool Button7_CausesValidation
    {
        get { return UIButton7.CausesValidation; }
        set { UIButton7.CausesValidation = value; }
    }

    public bool Button7_AlwaysEnabled
    {
        get { return UIButton7.AlwaysEnabled; }
        set { UIButton7.AlwaysEnabled = value; }
    }

    public bool Button7_Visible
    {
        get { return Button7Span.Visible; }
        set { Button7Span.Visible = value; }
    }

    [Localizable(true)]
    public string Button7_ConfirmText
    {
        get { return UIButton7.ConfirmText; }
        set { UIButton7.ConfirmText = value; }
    }


    //--------------------------------------------
    // button8
    //--------------------------------------------
    [Localizable(true)]
    public string Button8_Caption
    {
        get { return UIButton8.Text; }
        set { UIButton8.Text = value; }
    }

    public string Button8_ImageUrl
    {
        get { return UIButton8.ImageUrl; }
        set { UIButton8.ImageUrl = value; }
    }

    public string Button8_CommandName
    {
        get { return UIButton8.CommandName; }
        set { UIButton8.CommandName = value; }
    }

    public bool Button8_CausesValidation
    {
        get { return UIButton8.CausesValidation; }
        set { UIButton8.CausesValidation = value; }
    }

    public bool Button8_AlwaysEnabled
    {
        get { return UIButton8.AlwaysEnabled; }
        set { UIButton8.AlwaysEnabled = value; }
    }

    public bool Button8_Visible
    {
        get { return Button8Span.Visible; }
        set { Button8Span.Visible = value; }
    }

    [Localizable(true)]
    public string Button8_ConfirmText
    {
        get { return UIButton8.ConfirmText; }
        set { UIButton8.ConfirmText = value; }
    }


    void UIButton8_Click(object sender, EventArgs e)
    {
        //sli begin: if there is any sub-panel opens
        //need to process them first before proceed
        //otherwise, may cause yellow page error
        //e.g. from arinvoice to create matched credit note
        // with arinvoiceitem subpanel open, this subpanel will be loaded with credit note item.
        bool causesValidation = true;
        if (sender is UIButton)
            causesValidation = ((UIButton)sender).CausesValidation;

        if (causesValidation && !IsValid())
            return;

        bool isSubPanelValid = ProcessSubPanel(this.ObjectPanel);

        if (!causesValidation || (Page.IsValid && isSubPanelValid))
        {
            if (Click != null)
                Click(this, this.Button8_CommandName);
        }
    }

    void UIButton7_Click(object sender, EventArgs e)
    {
        //sli begin: if there is any sub-panel opens
        //need to process them first before proceed
        //otherwise, may cause yellow page error
        //e.g. from arinvoice to create matched credit note
        // with arinvoiceitem subpanel open, this subpanel will be loaded with credit note item.
        bool causesValidation = true;
        if (sender is UIButton)
            causesValidation = ((UIButton)sender).CausesValidation;

        if (causesValidation && !IsValid())
            return;

        bool isSubPanelValid = ProcessSubPanel(this.ObjectPanel);

        if (!causesValidation || (Page.IsValid && isSubPanelValid))
        {
            if (Click != null)
                Click(this, this.Button7_CommandName);
        }
    }

    void UIButton6_Click(object sender, EventArgs e)
    {
        //sli begin: if there is any sub-panel opens
        //need to process them first before proceed
        //otherwise, may cause yellow page error
        //e.g. from arinvoice to create matched credit note
        // with arinvoiceitem subpanel open, this subpanel will be loaded with credit note item.
        bool causesValidation = true;
        if (sender is UIButton)
            causesValidation = ((UIButton)sender).CausesValidation;

        if (causesValidation && !IsValid())
            return;

        bool isSubPanelValid = ProcessSubPanel(this.ObjectPanel);

        if (!causesValidation || (Page.IsValid && isSubPanelValid))
        {
            if (Click != null)
                Click(this, this.Button6_CommandName);
        }
    }

    void UIButton5_Click(object sender, EventArgs e)
    {
        //sli begin: if there is any sub-panel opens
        //need to process them first before proceed
        //otherwise, may cause yellow page error
        //e.g. from arinvoice to create matched credit note
        // with arinvoiceitem subpanel open, this subpanel will be loaded with credit note item.
        bool causesValidation = true;
        if (sender is UIButton)
            causesValidation = ((UIButton)sender).CausesValidation;

        if (causesValidation && !IsValid())
            return;

        bool isSubPanelValid = ProcessSubPanel(this.ObjectPanel);

        if (!causesValidation || (Page.IsValid && isSubPanelValid))
        {
            if (Click != null)
                Click(this, this.Button5_CommandName);
        }
    }

    void UIButton4_Click(object sender, EventArgs e)
    {
        //sli begin: if there is any sub-panel opens
        //need to process them first before proceed
        //otherwise, may cause yellow page error
        //e.g. from arinvoice to create matched credit note
        // with arinvoiceitem subpanel open, this subpanel will be loaded with credit note item.
        bool causesValidation = true;
        if (sender is UIButton)
            causesValidation = ((UIButton)sender).CausesValidation;

        if (causesValidation && !IsValid())
            return;

        bool isSubPanelValid = ProcessSubPanel(this.ObjectPanel);

        if (!causesValidation || (Page.IsValid && isSubPanelValid))
        {
            if (Click != null)
                Click(this, this.Button4_CommandName);
        }
    }

    void UIButton3_Click(object sender, EventArgs e)
    {
        //sli begin: if there is any sub-panel opens
        //need to process them first before proceed
        //otherwise, may cause yellow page error
        //e.g. from arinvoice to create matched credit note
        // with arinvoiceitem subpanel open, this subpanel will be loaded with credit note item.
        bool causesValidation = true;
        if (sender is UIButton)
            causesValidation = ((UIButton)sender).CausesValidation;

        if (causesValidation && !IsValid())
            return;

        bool isSubPanelValid = ProcessSubPanel(this.ObjectPanel);

        if (!causesValidation || (Page.IsValid && isSubPanelValid))
        {
            if (Click != null)
                Click(this, this.Button3_CommandName);
        }
    }

    void UIButton2_Click(object sender, EventArgs e)
    {
        //sli begin: if there is any sub-panel opens
        //need to process them first before proceed
        //otherwise, may cause yellow page error
        //e.g. from arinvoice to create matched credit note
        // with arinvoiceitem subpanel open, this subpanel will be loaded with credit note item.
        bool causesValidation = true;
        if (sender is UIButton)
            causesValidation = ((UIButton)sender).CausesValidation;

        if (causesValidation && !IsValid())
            return;

        bool isSubPanelValid = ProcessSubPanel(this.ObjectPanel);

        if (!causesValidation || (Page.IsValid && isSubPanelValid))
        {
            if (Click != null)
                Click(this, this.Button2_CommandName);
        }
    }

    void UIButton1_Click(object sender, EventArgs e)
    {
        //sli begin: if there is any sub-panel opens
        //need to process them first before proceed
        //otherwise, may cause yellow page error
        //e.g. from arinvoice to create matched credit note
        // with arinvoiceitem subpanel open, this subpanel will be loaded with credit note item.
        bool causesValidation = true;
        if (sender is UIButton)
            causesValidation = ((UIButton)sender).CausesValidation;

        if (causesValidation && !IsValid())
            return;

        bool isSubPanelValid = ProcessSubPanel(this.ObjectPanel);

        if (!causesValidation || (Page.IsValid && isSubPanelValid))
        {
            if (Click != null)
                Click(this, this.Button1_CommandName);
        }
    }

    protected void buttonReturn_Click(object sender, EventArgs e)
    {
        if (Window.HasNextReturnObject() != null)
        {
            //string returnString = Security.Decrypt(Request["RETURN"]);
            //string[] returnStrArr = returnString.Split(':');

            //if (returnStrArr.Length == 3)
            //{
            //    Window.OpenEditObjectPage(Page, returnStrArr[0], returnStrArr[1], QueryString.New("TAB", Security.Encrypt(returnStrArr[2])));
            //}
            Window.GoToNextReturnObject(this.Page);
        }
        else
        {
            Window.Close();
        }
    }


    /// <summary>
    /// Occurs when the user clicks on the workflow dialog box.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void WorkflowDialogBox_ButtonClicked(object sender, ButtonClickedEventArgs e)
    {
        if (e.CommandName == "Confirm")
        {
            // 2014.05.29
            // Kim Foong
            // Prevents the scenario where the user previously forgets 
            // 
            panelWorkflowPopup.ClearErrorMessages();

            // CONFIRM:
            // If the validation has failed, return.
            //
            textTaskCurrentComments.Validate();

            WorkflowDialogBox.Button1AutoClosesDialogBox = panelWorkflowPopup.IsValid;//not sure why this IsValid is not updated correctly

            if (string.IsNullOrEmpty(dropApprovalProcess.SelectedValue) && ((LogicLayerWorkflowPersistentObject)SessionObject).CurrentActivity.ApprovalProcessID == null && !this.SelectedWorkflowAction.Is("Cancel", "Suspend", "Resume"))
            {
                dropApprovalProcess.ErrorMessage = Resources.Errors.Workflow_NoApprovalProcess;
                WorkflowDialogBox.Button1AutoClosesDialogBox = false;
                return;
            }

            if (!panelWorkflowPopup.IsValid)
            {
                return;
            }


            // Update the current activity's comments and approval
            // hierarchy.
            //
            if (SessionObject is LogicLayerWorkflowPersistentObject)
            {
                ((LogicLayerWorkflowPersistentObject)SessionObject).CurrentActivity.TriggeringEventName = selectedWorkflowAction.Value;
                ((LogicLayerWorkflowPersistentObject)SessionObject).CurrentActivity.TaskCurrentComments = textTaskCurrentComments.Text;
                if (dropApprovalProcess.Visible)
                {
                    ((LogicLayerWorkflowPersistentObject)SessionObject).CurrentActivity.ApprovalProcessID = new Guid(dropApprovalProcess.SelectedValue);
                }
            }

            // Follow up with the button action.
            // 
            string clickedButton = (string)textClickedButton.Text;
            if (clickedButton == "buttOnValidateAndSave")
            {
                SaveObjectAndTransit();
            }
            else if (clickedButton == "buttOnValidateAndSaveAndNew")
            {
                if (SaveObjectAndTransit())
                {
                    string queryString = "";
                    foreach (string key in Page.Request.QueryString.Keys)
                        if (key != "TYPE" && key != "ID" && key != "MESSAGE")
                            queryString += (queryString == "" ? "" : "&") + key + "=" + HttpUtility.UrlEncode(Page.Request.QueryString[key]);
                    queryString += (queryString == "" ? "" : "&") + "MESSAGE=" + Resources.Messages.General_ItemSavedAndNew;

                    Window.OpenAddObjectPage(Page, GetRequestObjectType(), queryString);
                }
            }
            else if (clickedButton == "buttOnValidateAndSaveAndClose")
            {
                if (SaveObjectAndTransit())
                    Window.Close();
            }

            selectedWorkflowAction.Value = "";
        }
        else
        {
            // CANCEL:
            selectedWorkflowAction.Value = "";
        }
    }

    protected void ImportantNotesDialogBox_ButtonClicked(object sender, ButtonClickedEventArgs e)
    {
        ImportantNotesDialogBox.Hide();
        ImportantNotesLabel.Text = "";
    }

    //-------------------------FileScan

    /// <summary>
    /// get or set NewAttachmentsUploaded
    /// </summary>
    protected List<OAttachment> NewAttachmentsUploaded
    {
        get
        {
            LogicLayerPersistentObject o = this.SessionObject as LogicLayerPersistentObject;
            if (o == null)
                return null;
            List<OAttachment> attList = new List<OAttachment>();
            if (objectAttachment != null)
            {
                attList.AddRange(o.Attachments.FindAll(
                    delegate (OAttachment att)
                    {
                        return att.IsNew || att.WasNew;
                    }
                ));
            }
            //if (o.ObjectTypeName.ToLower() == "ohawkerassistantregistration")
            //{
            //    foreach (OHawkerAssistant ha in ((OHawkerAssistantRegistration)o).HawkerAssistants)
            //    {
            //        attList.AddRange(ha.Attachments.FindAll(
            //                            delegate(OAttachment att)
            //                            {
            //                                return att.WasNew || att.IsNew;
            //                            }
            //                        ));
            //    }
            //}
            return attList;
        }
    }
    protected void ScanAttachment(List<OAttachment> attList)
    {
        if (attList == null || attList.Count == 0) return;
        List<String> pathList = GetAttachmentFilePath(attList);
        if (pathList != null && pathList.Count > 0)
        {
            try
            {
                FileScan.RunScan(pathList);
                try
                {
                    if ((this.SessionObject as LogicLayerPersistentObject) != null)
                    {
                        (this.SessionObject as LogicLayerPersistentObject).Attachments.Invalidate();
                    }
                }
                catch { }
            }
            catch (Exception ex)
            {
                //hande file scan err
            }
        }
    }
    /// <summary>
    /// return list of filepath
    /// </summary>
    /// <param name="attList"></param>
    /// <returns></returns>
    protected List<String> GetAttachmentFilePath(List<OAttachment> attList)
    {
        if (attList == null || attList.Count == 0) return null;
        List<String> pathList = new List<string>();
        String columnName = "FileBytes";
        attList.ForEach(
            delegate (OAttachment att)
            {
                if (att == null) return;

                Byte[] fileByte = att.DataRow[columnName] as byte[];

                if (fileByte == null || fileByte.Length == 0) return;
                try
                {
                    String filename = Encoding.UTF8.GetString(fileByte);
                    String filepath = GetFolderPathFromDiskStorageFolder(att, filename, columnName);
                    pathList.Add(filepath + filename);
                }
                catch { }
            }
        );
        return pathList;
    }

    protected string GetFolderPathFromDiskStorageFolder(object o, string fileName, string columnName)
    {
        string fileTime = fileName.Split('_')[1];
        string year = fileTime.Substring(0, 2);
        string month = fileTime.Substring(2, 2);
        string date = fileTime.Substring(4, 2);
        System.Xml.XmlDocument document = new System.Xml.XmlDocument();
        document.Load(Server.MapPath("~") + "/web.config");
        System.Xml.XmlNodeList elemList = document.GetElementsByTagName("dataFramework");
        if (elemList == null || elemList.Count == 0) return "";
        System.Xml.XmlNode elem = elemList[0];

        string SchemaImageDiskStorageFolder = elem.Attributes["schemaImageDiskStorageFolder"].Value;
        string SchemaImageDiskStorageMonthSubFolder = (elem.Attributes["schemaImageDiskStorageMonthSubFolder"] == null ? "MM" : elem.Attributes["schemaImageDiskStorageMonthSubFolder"].Value);
        string SchemaImageDiskStorageDateSubFolder = (elem.Attributes["schemaImageDiskStorageDateSubFolder"] == null ? "dd" : elem.Attributes["schemaImageDiskStorageDateSubFolder"].Value);



        string folderPath = SchemaImageDiskStorageFolder +
                                ((PersistentObject)o).GetSchemaBase().SchemaInfo.tableName + "\\" + columnName + "\\";
        folderPath = folderPath + year + "\\";
        if (SchemaImageDiskStorageMonthSubFolder == "MM")
            folderPath = folderPath + month + "\\";
        if (SchemaImageDiskStorageDateSubFolder == "dd")
            folderPath = folderPath + date + "\\";
        return folderPath;
    }

    /// <summary>
    /// Get/Set visibility of Workflow Save Button
    /// 20121204 PTB
    /// </summary>
    public bool WorkflowSaveButtonVisible
    {
        get
        {
            UIButton saveButton = this.GetWorkflowActionButton("-");
            return saveButton == null ? false : saveButton.Visible;
        }
        set
        {
            UIButton saveButton = this.GetWorkflowActionButton("-");
            if (saveButton != null)
                saveButton.Visible = value;
            else if (saveButton == null && value == true)
                BindWorkflowTransitionDropdownList(this.SessionObject);
        }
    }

    protected bool autoHide = true;

    /// <summary>
    /// Auto Hide the error message
    /// </summary>
    public bool AutoHide
    {
        get
        {
            return autoHide;
        }
        set
        {
            autoHide = value;
        }
    }

    protected int autoHideDurationInSecond = 10;

    public int AutoHideDurationInSecond
    {
        get
        {
            return autoHideDurationInSecond;
        }
        set
        {
            autoHideDurationInSecond = value;
        }
    }

    protected void MassPrintingDialogBox_ButtonClicked(object sender, ButtonClickedEventArgs e)
    {
        if (textClickedButton.Text == "buttonPrint")
        {
            string id = CurrentDocumentTemplateID.Value;
            string url = this.ResolveUrl(Page.Request.ApplicationPath + "/components/document.aspx?templateID=" + HttpUtility.UrlEncode(id) + "&t=" + DateTime.Now.ToString("hhmmssfff") + "&session=" + GetObjectSessionKey() + "&format=" + HttpUtility.UrlEncode(Security.Encrypt("word")));

            if (e.CommandName == "Yes")
            {
                url += "&SendForPrint=" + HttpUtility.UrlEncode(Security.Encrypt("1"));
            }

            this.FocusWindow = false;
            Window.DownloadUrl(url);

            using (Connection c = new Connection())
            {
                //add a document template log
                ODocumentTemplateLog log = TablesLogic.tDocumentTemplateLog.Create();
                log.DocumentTemplateID = new Guid(Security.DecryptFromHex(id));
                log.AttachedObjectID = this.SessionObject.ObjectID;
                log.ObjectTypeName = this.SessionObject.GetType().BaseType.Name;

                log.Save();
                c.Commit();
            }

            PopulateDocumentTemplate(new Guid(Security.DecryptFromHex(id)));
            CurrentDocumentTemplateID.Value = "";
        }
    }

    bool renderingPDF = false;
    protected void buttonGeneratePDF_Click(object sender, EventArgs e)
    {
        renderingPDF = true;

        // Look for the update progress control and remove it.
        //
        Control c = FindUpdateProgressControl(this.Page);
        if (c != null)
            c.Parent.Controls.Remove(c);
        this.Page.Controls[this.Page.Controls.Count - 1].PreRender += new EventHandler(objectPanel_PreRender);
    }

    void objectPanel_PreRender(object sender, EventArgs e)
    {
        if (renderingPDF)
            RenderPdf();

    }

    protected Control FindUpdateProgressControl(Control c)
    {
        if (c.ID == "__updateProgress__")
            return c;
        foreach (Control child in c.Controls)
        {
            Control found = FindUpdateProgressControl(child);
            if (found != null)
                return found;
        }
        return null;
    }

    protected Control FindFooter(Control c)
    {
        if (c.GetType().Name == "objectPanel2")
            return c;
        foreach (Control child in c.Controls)
        {
            Control found = FindFooter(child);
            if (found != null)
                return found;
        }
        return null;
    }

    protected void RenderPdf()
    {
        StringWriter sw = new StringWriter();
        Html32TextWriter hw = new Html32TextWriter(sw);

        bool oldFooterVisible = false;
        Control c = FindFooter(this.Page);
        if (c != null)
        {
            oldFooterVisible = c.Visible;
            c.Visible = false;
        }
        bool oldButtonContainPanelVisible = this.buttonDiv.Visible;
        this.buttonDiv.Visible = false;
        //this.buttonShadow.Visible = false;

        this.Page.RenderControl(hw);

        this.buttonDiv.Visible = oldButtonContainPanelVisible;
        if (c != null)
            c.Visible = oldFooterVisible;

        //save output html to file
        string guid = Guid.NewGuid().ToString();

        string filePath = ConfigurationManager.AppSettings["TempFolder"] + guid + ".htm";
        StreamWriter fileWriter;
        if (File.Exists(filePath))
            fileWriter = File.AppendText(filePath);
        else
            fileWriter = File.CreateText(filePath);

        string html = sw.ToString();
        try
        {
            html = html.Replace("ss-container", "");
            html = html.Replace("</title>", "</title><style>.mdl-tabs__tab-bar { min-height: 500px } </style>");
            html = html.Replace(" href=\"..", " href=\"" + Server.MapPath(".") + "\\..");
            html = html.Replace(" src=\"..", " src=\"" + Server.MapPath(".") + "\\..");
            html = html.Replace(" href='" + Page.Request.ApplicationPath, " href='file:///" + Server.MapPath("~"));
            html = html.Replace(" href=\"" + Page.Request.ApplicationPath, " href=\"file:///" + Server.MapPath("~"));

            // Modify signature to display for work order
            if (GetRequestObjectType().Equals("OWork"))
            {
                OWork work = (OWork)this.SessionObject;
                if (work != null && work.RequestorSignatureID.HasValue)
                {
                    string filePathForSignature = work.RequestorSignature.ImageUrl;
                    int t = html.IndexOf("RequestorSignature_SignatureImage\"");
                    if (t > 0)
                    {
                        int tt = t;
                        while (html[tt] != '"') tt++;
                        tt++;
                        while (html[tt] != '"') tt++;

                        tt++;
                        int mtt = tt;
                        int ltt = 0;
                        while (html[tt] != '"')
                        {
                            ltt++;
                            tt++;
                        }
                        html = html.Remove(mtt, ltt).Insert(mtt, filePathForSignature);
                    }
                }
            }

            fileWriter.Write(html);
        }
        finally
        {
            fileWriter.Close();
        }



        //convert to PDF
        //string outputURL = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Path).Replace("document.aspx", "") + "../../../../GeneratedPDFs/" + guid + ".htm";

        //PdfSharp.Pdf.PdfDocument pdf = PdfGenerator.GeneratePdf(html, PdfSharp.PageSize.A4);               
        //string pdfURL = ConfigurationManager.AppSettings["TempFolder"] + Guid.NewGuid().ToString() + ".pdf";        
        //pdf.Save(pdfURL);

        //string pdfURL = WKHtmlToPdf(filePath);
        string pdfURL = Helpers.HtmlUrlToPdf(filePath);
        //File.Delete(filePath);
        Session["GeneratePDFFileName"] = pdfURL;

        this.Page.Response.Clear();
        this.Page.Response.ClearHeaders();
        this.Page.Response.WriteFile(pdfURL);
        this.Page.Response.AddHeader("content-disposition", "attachment; filename=" + this.Caption.Replace(" ", "_") + ".pdf");
        this.Page.Response.AddHeader("pragma", "public");
        this.Page.Response.ContentType = "application/pdf";
        this.Page.Response.End();

    }


    public override void RenderControl(HtmlTextWriter writer)
    {
        StringWriter sw = new StringWriter();
        Html32TextWriter hw = new Html32TextWriter(sw);
        base.RenderControl(hw);

        string s = sw.ToString();
        s = s.Replace(
            "onclick=\"__doPostBack('" + this.buttonGeneratePDF.LinkButton.UniqueID + "', '');btn.cl(this);return false;\"",
            "onclick=\"__doPostBack('" + this.buttonGeneratePDF.LinkButton.UniqueID + "', '');return false;\"");

        writer.Write(s);

    }


    public string WKHtmlToPdf(string Url)
    {
        var p = new System.Diagnostics.Process();

        string switches = "";
        switches += "--print-media-type ";
        switches += "--margin-top 10mm --margin-bottom 10mm --margin-right 10mm --margin-left 10mm ";
        switches += "--page-size Letter ";
        switches += "--disable-javascript ";
        // waits for a javascript redirect it there is one
        //switches += "--redirect-delay 100";

        // Utils.GenerateGloballyUniuqueFileName takes the extension from
        // basically returns a filename and prepends a GUID to it (and checks for some other stuff too)
        string fileName = ConfigurationManager.AppSettings["TempFolder"] + Guid.NewGuid().ToString() + ".pdf";

        var startInfo = new System.Diagnostics.ProcessStartInfo
        {
            FileName = Server.MapPath("~\\wkhtmltopdf\\wkhtmltopdf.exe"),
            Arguments = switches + " " + Path.GetFileName(Url) + " \"" + fileName + "\"",
            UseShellExecute = false, // needs to be false in order to redirect output
            RedirectStandardOutput = true,
            RedirectStandardError = true,
            RedirectStandardInput = true, // redirect all 3, as it should be all 3 or none
            WorkingDirectory = Path.GetDirectoryName(Url)
        };
        p.StartInfo = startInfo;
        p.Start();

        // doesn't work correctly...
        // read the output here...
        // string output = p.StandardOutput.ReadToEnd();

        //  wait n milliseconds for exit (as after exit, it can't read the output)
        p.WaitForExit(60000);

        // read the exit code, close process
        int returnCode = p.ExitCode;
        p.Close();

        // if 0, it worked
        //return (returnCode == 0) ? fileName : null;
        return fileName;
    }

    protected void buttonViewActivityHistory_Click(object sender, EventArgs e)
    {
        LogicLayerWorkflowPersistentObject logicLayerPersistentObject = this.SessionObject as LogicLayerWorkflowPersistentObject;
        if (logicLayerPersistentObject != null)
        {
            // The activity histories are delay-loaded when the user clicks on the
            // tab containing this control. As it is expected that the
            // activity history table can be very huge, this is to help
            // speed up the opening of the edit page of an object.
            //
            gridWorkStatusHistory.DataSource = logicLayerPersistentObject.ActivityHistories;
            gridWorkStatusHistory.DataBind();
        }
        dialogBoxActivityHistory.Visible = true;
        dialogBoxActivityHistory.Show();
    }

    protected void buttonViewWorkflow_Click(object sender, EventArgs e)
    {
        this.FocusWindow = false;

        LogicLayerWorkflowPersistentObject logicLayerPersistentObject = this.SessionObject as LogicLayerWorkflowPersistentObject;

        // 2011.10.11
        // Kim Foong
        // Load up the latest workflow instead.
        //
        int workflowVersionNumber = TablesWorkflow.tWorkflowRepositoryVersion.Select(
            TablesWorkflow.tWorkflowRepositoryVersion.WorkflowVersionNumber.Max())
            .Where(
            TablesWorkflow.tWorkflowRepositoryVersion.WorkflowRepository.ObjectTypeName == logicLayerPersistentObject.GetType().BaseType.Name);

        Window.Open(Page.Request.ApplicationPath + "/modules/adminsystem/workflowrepository/WFDiagram.aspx?" + QueryString.New("OBJTYPE", Security.Encrypt(this.SessionObject.GetType().BaseType.Name))
                                                                       + QueryString.New("OBJSTATUS", Security.Encrypt(logicLayerPersistentObject.CurrentActivity.ObjectName))
                                                                       + QueryString.New("WFVER", Security.Encrypt(workflowVersionNumber.ToString())), "_blank");

    }

    protected void dropTab_SelectedIndexChanged(object sender, EventArgs e)
    {
        UITabStrip tabStrip = this.TabStrip;

        if (tabStrip != null)
        {
            int tabIndex = Convert.ToInt32(dropTab.SelectedValue);

            var tabViews = tabStrip.TabViews;
            var tabView = tabViews.Find(x => x.TabIndex == tabIndex);

            if (tabView != null)
                tabStrip.ChangeTab(tabView, true);
        }
    }
</script>

<div class="mdl-title-bar-spacer" runat="server" id="buttonDiv">
    <div class="mdl-title-bar" >
        <div style="float: left">
            <ui:UIButton runat="server" ID="buttonReturn" ButtonType="RoundMiniFlat" ButtonColor="Default"
                IconCssClass="material-icons" IconText="arrow_back"
                ConfirmText="Return to the previous screen?"
                OnClick="buttonReturn_Click" AlwaysEnabled="true"></ui:UIButton>

            <div class="mdl-title-bar-title-and-tab">
                <asp:Label ID="labelCaption" runat="server" CssClass="mdl-title-bar-label" meta:resourcekey="labelCaptionResource1"
                    Text="Object Name" ></asp:Label>
                <asp:DropDownList runat="server" ID="dropTab" CssClass="mdl-title-bar-tab" AutoPostBack="true" OnSelectedIndexChanged="dropTab_SelectedIndexChanged"></asp:DropDownList>
            </div>
        </div>


        <%-- Show action sheet button --%>
        <div style="float:right" class="mdl-title-bar-toggle-extra-buttons">
            <ui:UIButton runat="server" ID="buttonExpandActionSheet" IconCssClass="material-icons" IconText="arrow_drop_down" ButtonType="RoundMiniFlat" ButtonColor="Default" OnClickScript="$('.mdl-title-bar-extra-buttons').toggle(300)" DisableAfterClick="false" DoPostBack="false" />
        </div>

        <div style="float: right" class="mdl-title-bar-extra-buttons">
            <div>
            <span runat="server" id="spanSaveButtons">
                <span runat="server" id="spanSaveButtonsInner">
                    <ui:UIButton runat="server" ID="buttOnValidateAndSave" ButtonType="NormalRaised" ButtonColor="Green"
                        ValidateAllTabs="true" IconCssClass="material-icons" IconText="save"
                        Text="Save" CausesValidation="true" OnClick="buttOnValidateAndSave_Click" meta:resourcekey="buttOnValidateAndSaveResource1"></ui:UIButton>
                    <ui:UIButton runat="server" ID="buttOnValidateAndSaveAndClose" ImageUrl="~/img/save_img.png"
                        ValidateAllTabs="true" CssClass="btnblack" DisabledCssClass="btnblack-d"
                        CausesValidation="true" Text="Save and Close" OnClick="buttOnValidateAndSaveAndClose_Click" Visible="false"
                        meta:resourcekey="buttOnValidateAndSaveAndCloseResource1"></ui:UIButton>
                    <ui:UIButton runat="server" ID="buttOnValidateAndSaveAndNew" ImageUrl="~/img/save_img.png"
                        ValidateAllTabs="true" CssClass="btnblack" DisabledCssClass="btnblack-d"
                        CausesValidation="true" Text="Save and New" OnClick="buttOnValidateAndSaveAndNew_Click" Visible="true"
                        ButtonType="NormalRaised" ButtonColor="Green"
                        IconCssClass="material-icons" IconText="save"
                        meta:resourcekey="buttOnValidateAndSaveAndNewResource1"></ui:UIButton>
                </span>
            </span>

            <span runat="server" id="spanCloneButtons">
                <ui:UIButton runat="server" ID="buttonClone" ButtonType="NormalFlat" ButtonColor="Green"
                    ConfirmText="Please remember to save this item first before cloning it.\n\nAre you sure you wish to continue?"
                    Text="Clone" CausesValidation='false' OnClick="buttonClone_Click"
                    IconCssClass="material-icons" IconText="file_copy"
                    meta:resourcekey="buttonCloneResource1" />
            </span>

            <asp:Panel runat="server" ID="panelWorkflowMenu" CssClass="mdl-dropdown-container">
                <ui:UIButton runat="server" ID="buttonWorkflowSave" ButtonType="NormalRaised" ButtonColor="Green"
                    ValidateAllTabs="true" IconCssClass="material-icons" IconText="save"
                    Text="Save" CausesValidation="true" OnClick="buttonWorkflowSave_Click" ></ui:UIButton>

                <ui:UIButton runat="server" ID="WorkflowButtons" ButtonType="NormalRaised"
                    IconCssClass="material-icons" IconText="call_split"
                    Text="Workflow" ButtonColor="Blue" DoPostBack="false" OnClickScript="popup.show('', '#objectPanelWorkflowMenu', '', event); return false;" TrailingIconCssClass="material-icons" TrailingIconText="arrow_drop_down" />

                <div class='mdl-dropdown mdl-dropdown-right'  id='objectPanelWorkflowMenu' style='display: none'>
                    <asp:DataList runat='server' ID='datalistWorkflowButtons' OnItemDataBound="datalistWorkflowButtons_ItemDataBound"
                        RepeatColumns="0" RepeatLayout="Flow" RepeatDirection="Horizontal" OnItemCommand="datalistWorkflowButtons_ItemCommand" meta:resourcekey="datalistWorkflowButtonsResource">
                        <ItemTemplate>
                            <ui:UIButton runat="server" ID="buttonWorkflowAction" ValidateAllTabs="true"
                                ButtonColor="Default" ButtonType="NormalFlat"
                                meta:resourcekey="buttonWorkflowActionResource" />
                        </ItemTemplate>
                    </asp:DataList>
                </div>
            </asp:Panel>

            <ui:UIPanel runat='server' ID="OtherButtonsPanel" CssClass="mdl-dropdown-container" meta:resourcekey="OtherButtonsPanelResource">
                <ui:UIButton runat="server" ID="MoreActionsButton" ButtonType="NormalFlat" ButtonColor="Default" Text="More Actions" IconCssClass="material-icons" IconText="build" TrailingIconCssClass="material-icons" TrailingIconText="arrow_drop_down" OnClickScript="popup.show('', '#objectPanelOtherButtonsMenu', '', event); return false;" />

                <div class='mdl-dropdown mdl-dropdown-right' id="objectPanelOtherButtonsMenu" style="display: none">
                    <span runat="server" id="Button1Span">
                        <ui:UIButton runat="server" ID="UIButton1" CssClass="btnblack" DisabledCssClass="btnblack-d" meta:resourcekey="UIButton1Resource" BackColor="White" />
                    </span><span runat="server" id="Button2Span">
                        <ui:UIButton runat="server" ID="UIButton2" CssClass="btnblack" DisabledCssClass="btnblack-d" meta:resourcekey="UIButton2Resource" />
                    </span><span runat="server" id="Button3Span">
                        <ui:UIButton runat="server" ID="UIButton3" CssClass="btnblack" DisabledCssClass="btnblack-d" meta:resourcekey="UIButton3Resource" />
                    </span><span runat="server" id="Button4Span">
                        <ui:UIButton runat="server" ID="UIButton4" CssClass="btnblack" DisabledCssClass="btnblack-d" meta:resourcekey="UIButton4Resource" />
                    </span><span runat="server" id="Button5Span">
                        <ui:UIButton runat="server" ID="UIButton5" CssClass="btnblack" DisabledCssClass="btnblack-d" meta:resourcekey="UIButton5Resource" />
                    </span><span runat="server" id="Button6Span">
                        <ui:UIButton runat="server" ID="UIButton6" CssClass="btnblack" DisabledCssClass="btnblack-d" meta:resourcekey="UIButton6Resource" />
                    </span><span runat="server" id="Button7Span">
                        <ui:UIButton runat="server" ID="UIButton7" CssClass="btnblack" DisabledCssClass="btnblack-d" meta:resourcekey="UIButton7Resource" />
                    </span><span runat="server" id="Button8Span">
                        <ui:UIButton runat="server" ID="UIButton8" CssClass="btnblack" DisabledCssClass="btnblack-d" meta:resourcekey="UIButton8Resource" />
                    </span>
                </div>
            </ui:UIPanel>


            <div runat="server" id="panelDocumentTemplates2" class="mdl-dropdown-container">
                <ui:UIButton runat="server" ID="PrintButton" ButtonType="NormalFlat" ButtonColor="Default" IconCssClass="material-icons" IconText="print" TrailingIconCssClass="material-icons" TrailingIconText="arrow_drop_down" Text="Print" OnClickScript="popup.show('', '#objectPanelDocumentTemplateMenu', '', event); return false;"  />
                <div class='mdl-dropdown mdl-dropdown-right' id='objectPanelDocumentTemplateMenu' style='display: none'>
                    <span runat="server" id="panelDocumentTemplates"></span>
                </div>
            </div>


            <div class="mdl-dropdown-container">
                <ui:UIButton runat="server" ID="MoreActionButton" DoPostBack="false" ButtonType="NormalFlat" Text="More Actions" ButtonColor="Default" IconCssClass="material-icons" IconText="more_vert" OnClickScript="popup.show('', '#objectPanelMoreMenu', '', event); return false;" TrailingIconCssClass="material-icons" TrailingIconText="arrow_drop_down"  />
                <div class='mdl-dropdown mdl-dropdown-right' id='objectPanelMoreMenu' style='display: none'>

                    <span runat="server" id="spanWorkflow" visible="false">
                        <div class="material-dropmenu-separator"><span></span></div>
                        <ui:UIButton runat="server" ID="buttonViewActivityHistory" OnClick="buttonViewActivityHistory_Click"
                            ButtonType="NormalFlat" ButtonColor="Default"
                            IconCssClass="material-icons" IconText="history"
                            CausesValidation="false" AlwaysEnabled="true"
                            Text="View Workflow History" meta:resourcekey="buttonViewActivityHistoryResource1"></ui:UIButton>
                        <ui:UIButton runat="server" ID="buttonViewWorkflow" OnClick="buttonViewWorkflow_Click"
                            ButtonType="NormalFlat" ButtonColor="Default"
                            IconCssClass="material-icons" IconText="call_split"
                            CausesValidation="false" AlwaysEnabled="true"
                            Text="View Workflow" meta:resourcekey="buttonViewWorkflowResource1"></ui:UIButton>
                    </span>

                    <span runat='server' id="AdditionalCommandsCell">
                        <ui:UIButton runat="server" ID="buttonDocumentLog" Text="View Document Print Log"
                            ButtonType="NormalFlat" ButtonColor="Default"
                            CausesValidation="false" AlwaysEnabled="true"
                            IconCssClass="material-icons" IconText="history"
                            OnClick="buttonDocumentLog_Click" meta:resourcekey="buttonDocumentLogResource1"></ui:UIButton>
                        <ui:UIButton runat="server" ID="buttonJournalTransaction" Text="View Journal Transaction"
                            ButtonType="NormalFlat" ButtonColor="Default"
                            CausesValidation="false" AlwaysEnabled="true"
                            IconCssClass="material-icons" IconText="history"
                            OnClick="buttonJournalTransaction_Click" meta:resourcekey="buttonJournalTransactionResource"></ui:UIButton>
                        <ui:UIButton runat="server" ID="buttonGeneratePDF" Text="Generate PDF" CausesValidation="false"
                            ButtonType="NormalFlat" ButtonColor="Default"
                            AlwaysEnabled="true" DisableAfterClick="false"
                            IconCssClass="material-icons" IconText="picture_as_pdf"
                            OnClick="buttonGeneratePDF_Click"></ui:UIButton>
                    </span>

                    <span runat="server" id="spanDelete">
                        <span runat='server' id="spanDelete2">
                            <span runat='server' id="spanDelete3">
                                <div class="material-dropmenu-separator" runat="server" id="spanDeleteSeparator"><span></span></div>
                                <ui:UIButton runat="server" ID="buttonDelete"
                                    ButtonType="NormalFlat" ButtonColor="Red" IconCssClass="material-icons" IconText="delete"
                                    CausesValidation="false" Text="Delete Item" OnClick="buttonDelete_Click"
                                    ConfirmText="Are you sure you wish to delete this?\n\nPlease note that related items may no longer be accessible if you choose to delete this item."
                                    meta:resourcekey="buttonDeleteResource1"></ui:UIButton>
                            </span>
                        </span>
                    </span>
                </div>
            </div>
            </div>
        </div>
    
        <div style="clear: both"></div>

    </div>
</div>

<%--
<div class="material-subtitle-white">
    <div style="float: left">
        <ul class="app-breadcrumb breadcrumb side">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i>Home</li>
            <li class="breadcrumb-item active"><a href="#">Admin</a></li>
            <li class="breadcrumb-item active"><a href="#">User</a></li>
        </ul>
    </div>
    <div style="clear: both"></div>

</div>
--%>
    


<div style="font-size: 0pt;" class="object-buttons-div">
    <ui:UIPanel runat="server" ID="mainPanel" meta:resourcekey="mainPanelResource" BackColor="White">
        <div id="FullPanel" style="margin-right: 0px;" runat='server' class="full-panel">
            <asp:Panel runat='server' ID='ButtonContainerPanel' CssClass="object-buttons full-panel" meta:resourcekey="ButtonContainerPanelResource" BackColor="White">
                <table cellpadding='0' cellspacing='0' border='0' style="display: none">
                    <tr valign="top">
                        <td class="auto-style2">
                            <ui:UIPanel runat='server' ID="ButtonsPanel" meta:resourcekey="ButtonsPanelResource" BackColor="White" HorizontalAlign="Right" Width="850px">
                                <span runat="server" id="PrevNextTabButtons" visible="true">
                                    <ui:UIButton runat="server" ID="PrevTabButton" Text="Previous" ImageUrl="~/images/arrowleft-white-16.png"
                                        CssClass="btnblack" DisabledCssClass="btnblack-d"
                                        CausesValidation="false" OnClick="PrevTabButton_Click" meta:resourcekey="PrevTabButtonResource1" Visible="False" />
                                    <ui:UIButton runat="server" ID="NextTabButton" Text="Next" ImageUrl="~/images/arrowright-white-16.png"
                                        CssClass="btnblack" DisabledCssClass="btnblack-d"
                                        CausesValidation="true" OnClick="NextTabButton_Click" meta:resourcekey="NextTabButtonResource1" Visible="False" />
                                </span>
                            </ui:UIPanel>
                            <ui:UIPanel runat='server' ID="RefreshButtonsPanel" meta:resourcekey="RefreshButtonsPanelResource">
                                <div style="display: none">
                                    <ui:UIButton runat="server" ID="buttonRefresh" ImageUrl="" Text="R" OnClick="buttonRefresh_Click"
                                        CausesValidation="false" meta:resourcekey="buttonRefreshResource"></ui:UIButton>
                                    <ui:UIButton runat="server" ID="buttonRefreshSession" ImageUrl="" Text="R" OnClick="buttonRefreshSession_Click"
                                        CausesValidation="false" meta:resourcekey="buttonRefreshSessionResource"></ui:UIButton>
                                </div>
                            </ui:UIPanel>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <ui:UIPanel runat='server' ID="panelUrgent" CssClass='div-urgent-no' Visible="false" meta:resourcekey="panelUrgentResource">
                <ui:UIFieldCheckBox runat="server" ID="PriorityFlag" ForeColor="White" Font-Bold="true"
                    Style="padding-left: 0px; padding-right: 0px"
                    Caption="Mark as urgent" InternalControlWidth="" FieldLayout="Flow" ShowCaption="false"
                    Text="URGENT"
                    Span="Half" PropertyName="CurrentActivity.PriorityFlag" OnCheckedChanged="PriorityFlag_CheckedChanged"
                    meta:resourcekey="PriorityFlagResource1">
                </ui:UIFieldCheckBox>
                <asp:Label runat='server' ID='labelUrgent' meta:resourcekey="labelUrgentResource"></asp:Label>
            </ui:UIPanel>



            <input id="CurrentDocumentTemplateID" type="hidden" runat="server" valu="" />
            <ui:UIDialogBox runat="server" ID="MassPrintingDialogBox" Title="Do you want to send this document for Mass Document Printing or Re-printing?"
                DialogWidth="800px" Button1Text="Yes" Button1CommandName="Yes" Button1CausesValidation="false"
                Button1AlwaysEnabled="true" Button2Text="No"
                Button2CommandName="No" Button2CausesValidation="false" Button2AlwaysEnabled="true"
                OnButtonClicked="MassPrintingDialogBox_ButtonClicked" meta:resourcekey="MassPrintingDialogBoxResource">
            </ui:UIDialogBox>

            <ui:UIDialogBox runat='server' ID="WorkflowDialogBox" Button1Text="Confirm" Button1CommandName="Confirm"
                DialogWidth="800px"
                Button2Text="Cancel" Button2CommandName="Cancel" OnButtonClicked="WorkflowDialogBox_ButtonClicked" meta:resourcekey="WorkflowDialogBoxResource">
                <ui:UIObjectPanel runat="server" ID="panelWorkflowPopup"
                    Width="730px" meta:resourcekey="panelWorkflowPopupResource">
                    <ui:UIFieldTextBox runat="server" ID="textTaskCurrentComments" Caption="Comments" ShowCharacterCounter="false"
                        MaxLength="255" TextMode="MultiLine" Rows="5" meta:resourcekey="textTaskCurrentCommentsResource1">
                    </ui:UIFieldTextBox>
                    <ui:UIHint runat="server" ID="HintRejectAction" Text="The system &lt;strong style=&quot;color: #bc1c48&quot;&gt;REJECT&lt;/strong&gt; option is for any approver or reviewer along the approval process to dismiss a request/submission/proposal made by the originator and this action will terminate the process. If this is not your intention and you are only making clarifications or requesting for amendments to details in this request/submission/proposal, please use the &lt;strong&gt;QUERY&lt;/strong&gt; option instead.&lt;br /&gt;&lt;br /&gt;Do you wish to proceed with the &lt;strong style=&quot;color: #bc1c48&quot;&gt;REJECT&lt;/strong&gt; option?"></ui:UIHint>
                    <ui:UIPanel runat="server" ID="panelApprovalProcess" meta:resourcekey="panelApprovalProcessResource">
                        <ui:UIFieldDropDownList runat="server" ID="dropApprovalProcess" Caption="Approval Process"
                            ValidateRequiredField="true" OnSelectedIndexChanged="dropApprovalProcess_SelectedIndexChanged" meta:resourcekey="dropApprovalProcessResource">
                        </ui:UIFieldDropDownList>
                        <ui:UIFieldLabel runat="server" ID="labelModeOfForwarding" Caption="Mode of Forwarding"
                            Visible="false" meta:resourcekey="labelModeOfForwardingResource">
                        </ui:UIFieldLabel>
                        <ui:UIFieldCheckBox runat="server" ID="checkSkipLevels" Caption="Skip Levels?" Text="Yes, skip to the last level that rejected this task."
                            Checked='true' OnCheckedChanged="checkSkipLevels_CheckedChanged" meta:resourcekey="checkSkipLevelsResource">
                        </ui:UIFieldCheckBox>
                        <table cellpadding='0' cellspacing='2' border='0' style="width: 100%; clear: both">
                            <tr>
                                <td>
                                    <div align='center'>
                                        <asp:Label runat="server" ID="labelApprovalNotRequired" Font-Bold="true" Text="No approval is required for this process." meta:resourcekey="labelApprovalNotRequiredResource"></asp:Label>
                                        <asp:Label runat="server" ID="labelApproved" Font-Bold="true" Text="You are the authorized approver." meta:resourcekey="labelApprovedResource"></asp:Label>
                                        <asp:Label runat="server" ID="labelNoApproversFound" Text="There are no approvers authorized to approve this process."
                                            ForeColor="red" Font-Bold="true" meta:resourcekey="labelNoApproversFoundResource"></asp:Label>
                                    </div>
                                    <ui:UIGridView runat="server" ID="gridApprovalProcess" Caption="Approver Hierarchy"
                                        AllowPaging="false" SortExpression="FinalApprovalLimit asc" CheckBoxColumnVisible="false"
                                        OnRowDataBound="gridApprovalProcess_RowDataBound" meta:resourcekey="gridApprovalProcessResource">
                                        <Columns>
                                            <ui:UIGridViewBoundColumn HeaderText="Approval Level" PropertyName="ApprovalLevel" meta:resourcekey="gridApprovalProcessUIGridViewBoundColumnApprovalLevelResource">
                                            </ui:UIGridViewBoundColumn>
                                            <ui:UIGridViewBoundColumn HeaderText="Approval Limit" PropertyName="FinalApprovalLimit"
                                                DataFormatString="{0:#,##0.00}" Visible="false" meta:resourcekey="gridApprovalProcessUIGridViewBoundColumnFinalApprovalLimitResource">
                                            </ui:UIGridViewBoundColumn>
                                            <ui:UIGridViewBoundColumn HeaderText="Users" PropertyName="UsersToBeAssignedNames" meta:resourcekey="gridApprovalProcessUIGridViewBoundColumnUsersToBeAssignedNamesResource">
                                            </ui:UIGridViewBoundColumn>
                                            <ui:UIGridViewBoundColumn HeaderText="Positions" PropertyName="PositionNamesWithUserNames" meta:resourcekey="gridApprovalProcessUIGridViewBoundColumnPositionNamesWithUserNamesResource">
                                            </ui:UIGridViewBoundColumn>
                                            <ui:UIGridViewBoundColumn HeaderText="Roles" PropertyName="RoleNames" meta:resourcekey="gridApprovalProcessUIGridViewBoundColumnRoleNamesResource">
                                            </ui:UIGridViewBoundColumn>
                                            <ui:UIGridViewBoundColumn HeaderText="Approvals Required" PropertyName="NumberOfApprovalsRequired" meta:resourcekey="gridApprovalProcessUIGridViewBoundColumnNumberOfApprovalsRequiredResource">
                                            </ui:UIGridViewBoundColumn>
                                        </Columns>
                                    </ui:UIGridView>
                                </td>
                            </tr>
                        </table>
                    </ui:UIPanel>
                </ui:UIObjectPanel>
            </ui:UIDialogBox>

            <%-- Show Object Activity History dialog box --%>
            <ui:UIDialogBox runat="server" ID="dialogBoxActivityHistory" Button1Text="Close"
                Button1AutoClosesDialogBox="true" Visible="false"
                DialogWidth="900px" Title="Workflow History" meta:resourcekey="dialogBoxActivityHistoryResource1">
                <ui:UIGridView runat="server" ID="gridWorkStatusHistory" Visible='true' SortExpression="CreatedDateTime desc"
                    PageSize="100" Caption="Status History" CheckBoxColumnVisible="false" KeyName="ObjectID"
                    meta:resourcekey="gridWorkStatusHistoryResource1">
                    <Columns>
                        <ui:UIGridViewBoundColumn PropertyName="CreatedDateTime" HeaderText="Date/Time" HeaderStyle-Width="140px"
                            DataFormatString="{0:dd-MMM-yyyy HH:mm:ss}">
                        </ui:UIGridViewBoundColumn>
                        <ui:UIGridViewBoundColumn PropertyName="CreatedUser" HeaderStyle-Width="200px" HeaderText="User"
                            meta:resourcekey="CreatedUserTimeResource1">
                        </ui:UIGridViewBoundColumn>
                        <ui:UIGridViewBoundColumn PropertyName="TriggeringEventName" HeaderStyle-Width="150px"
                            ResourceName="Resources.WorkflowEvents" HeaderText="Action"
                            meta:resourcekey="TriggeringEventNameResource1">
                        </ui:UIGridViewBoundColumn>
                        <ui:UIGridViewBoundColumn PropertyName="CurrentStateName" HeaderStyle-Width="150px"
                            ResourceName="Resources.WorkflowStates" HeaderText="Status"
                            meta:resourcekey="CurrentStateNameResource1">
                        </ui:UIGridViewBoundColumn>
                        <ui:UIGridViewBoundColumn HeaderText="Comments" PropertyName="TaskPreviousComments"
                            meta:resourcekey="TaskPreviousCommentsResource1">
                        </ui:UIGridViewBoundColumn>
                    </Columns>
                </ui:UIGridView>
            </ui:UIDialogBox>
            <asp:TextBox runat="server" ID="textSubPanelCount" Visible="false" meta:resourcekey="textSubPanelCountResource"></asp:TextBox>
            <asp:TextBox runat="server" ID="textClickedButton" Visible="false" meta:resourcekey="textClickedButtonResource"></asp:TextBox>
            <input id="CurrentObjectInstanceKey" type="hidden" runat="server" value="" />
        </div>
    </ui:UIPanel>
    <ui:UIDialogBox runat="server" ID="ImportantNotesDialogBox" Title="Important Notes Before Proceeding"
        DialogWidth="600px" DialogMaximumHeight="400px" Button1Text="Continue" OnButtonClicked="ImportantNotesDialogBox_ButtonClicked" meta:resourcekey="ImportantNotesDialogBoxResource">
        <table cellpadding='0' cellspacing='0' border='0' style="width: 100%; min-height: 350px">
            <tr valign="top" align="left">
                <td style="width: 70px" align="center">
                    <asp:Image runat="server" ID="ImportantNotesImage" ImageUrl="~/images/info-huge.png" meta:resourcekey="ImportantNotesImageResource" />
                </td>
                <td style="width: 530px">
                    <asp:Label runat="server" ID="ImportantNotesLabel" meta:resourcekey="ImportantNotesLabelResource"></asp:Label>
                </td>
            </tr>
        </table>
    </ui:UIDialogBox>
    <asp:HiddenField ID="selectedWorkflowAction" runat="server" meta:resourcekey="selectedWorkflowActionResource" />
</div>

<div id="mdl_toast" class="mdl-js-snackbar mdl-snackbar">
    <div class="mdl-snackbar__text"></div>
    <button class="mdl-snackbar__action" type="button"></button>
</div>
<asp:Label runat='server' ID='labelMessage' meta:resourcekey="labelMessageResource1" Visible="false"></asp:Label>

<script type="text/javascript">
    var toggleActionSheet = function () {
        console.log($(window).outerWidth() + " " + $(window).width())
        if ($(window).outerWidth() < 960) {
            $('.mdl-title-bar-toggle-extra-buttons').show();
            $('.mdl-title-bar-extra-buttons').hide();
        }
        else {
            $('.mdl-title-bar-toggle-extra-buttons').hide();
            $('.mdl-title-bar-extra-buttons').show();
        }
    }
    $(window).resize(toggleActionSheet)
</script>

