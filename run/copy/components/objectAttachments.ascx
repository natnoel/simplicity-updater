<%@ Control Language="C#" ClassName="objectAttachments" %>
<%@ Import Namespace="System" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="System.Web.UI" %>
<%@ Import Namespace="System.Web.UI.WebControls" %>
<%@ Import Namespace="System.ComponentModel" %>
<%@ Import Namespace="Anacle.DataFramework" %>
<%@ Import Namespace="LogicLayer" %>
<%@ Register Src="objectPanel.ascx" TagPrefix="web2" TagName="object" %>

<script runat="server">
    /// <summary>
    /// Gets or sets the document code type of the document that
    /// is to be uploaded.
    /// </summary>
    [Localizable(false), Browsable(true)]
    public string DocumentCodeType
    {
        get
        {
            //default to "DocumentType"
            if (ViewState["DocumentCodeType"] != null)
                return ViewState["DocumentCodeType"].ToString();
            else
                return "DocumentType";
        }
        set
        {
            //Default to DocumentType if not specified
            if (value == "" || value.Trim() == "")
                ViewState["DocumentCodeType"] = "DocumentType";
            else
                ViewState["DocumentCodeType"] = value;
        }
    }


    /// <summary>
    /// Initializes the control.
    /// </summary>
    /// <param name="e"></param>
    protected override void OnInit(EventArgs e)
    {

        base.OnInit(e);

        attachmentTypeIDDropDown = new UIFieldDropDownList[] {
                AttachmentTypeID01, AttachmentTypeID02, AttachmentTypeID03, AttachmentTypeID04, AttachmentTypeID05, 
                AttachmentTypeID06, AttachmentTypeID07, AttachmentTypeID08, AttachmentTypeID09, AttachmentTypeID10, AttachmentTypeID11 };
        attachmentInputFile = new UIFieldInputFile[] {
            InputFile01, InputFile02, InputFile03, InputFile04, InputFile05, 
            InputFile06, InputFile07, InputFile08, InputFile09, InputFile10, InputFile11};
        attachmentDescriptionTextBox = new UIFieldTextBox[] {
            AttachmentDescription01, AttachmentDescription02, AttachmentDescription03, AttachmentDescription04, AttachmentDescription05,
            AttachmentDescription06, AttachmentDescription07, AttachmentDescription08, AttachmentDescription09, AttachmentDescription10,
            AttachmentDescription11
        };

        // Register the buttonUpload button to force a full
        // postback whenever a file is uploaded.
        //
        if (Page is UIPageBase)
        {
            ((UIPageBase)this.Page).ScriptManager.RegisterPostBackControl(UploadAttachmentDialogBox.Button1.LinkButton);
        }


        Control currentControl = this;
        while (currentControl != null)
        {
            if (currentControl is UITabView)
                ((UITabView)currentControl).Click += new EventHandler(objectAttachments_Click);
            currentControl = currentControl.Parent;
        }


        getPanel(Page).objectAttachment = this;
    }



    /// <summary>
    /// Refresh the attachments tabview.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void objectAttachments_Click(object sender, EventArgs e)
    {
        BindAttachmentsDropDownList();
        ShowPendingAttachmentTypes();
    }

    UIFieldDropDownList[] attachmentTypeIDDropDown = null;
    UIFieldInputFile[] attachmentInputFile = null;
    UIFieldTextBox[] attachmentDescriptionTextBox = null;

    /// <summary>
    /// Loads a list of document types from the OCode table.
    /// </summary>
    /// <param name="e"></param>
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (!IsPostBack)
        {
            LogicLayerPersistentObject o = getPersistentObject(Page) as LogicLayerPersistentObject;
            BindAttachments(o);
            BindAttachmentsDropDownList();
            ShowPendingAttachmentTypes();
        }

        // This simply just copies over the dropdown list's selection to
        // the caption of the input file control.
        //
        for (int i = 0; i < attachmentTypeIDDropDown.Length; i++)
        {
            if (attachmentTypeIDDropDown[i].SelectedItem != null)
            {
                attachmentInputFile[i].Caption = attachmentTypeIDDropDown[i].SelectedItem.Text;
            }
            else
            {
                attachmentInputFile[i].Caption = "";
            }

            //restricts upload to 20MB.
            attachmentInputFile[i].ValidateFileSize = true;
            attachmentInputFile[i].ValidationFileSizeLimitInKilobytes = 20 * 1024;
        }

        // Mobile Web
        //
        if (this.Page is UIPageBase && ((UIPageBase)this.Page).MobileMode)
        {
            UploadAttachmentTable.Visible = false;
            UploadAttachmentTableMobile.Visible = true;
        }

        // 2014.09.03
        // Kim Foong
        //
        UploadAttachmentDialogBox.Button1.OnClickScript = "$('#__updateProgress__').show()";
    }

    public void ReloadAttachment(LogicLayerPersistentObject o)
    {

        BindAttachments(o);
        BindAttachmentsDropDownList();
        ShowPendingAttachmentTypes();

        // This simply just copies over the dropdown list's selection to
        // the caption of the input file control.
        //
        for (int i = 0; i < attachmentTypeIDDropDown.Length; i++)
        {
            if (attachmentTypeIDDropDown[i].SelectedItem != null)
                attachmentInputFile[i].Caption = attachmentTypeIDDropDown[i].SelectedItem.Text;
            else
                attachmentInputFile[i].Caption = "";
        }
    }

    /// <summary>
    /// Binds attachments to the gridview and 
    /// updates the tab view caption to reflect
    /// the number of attachments.
    /// </summary>
    /// <param name="o"></param>
    public void BindAttachments(LogicLayerPersistentObject o)
    {
        if (o != null)
        {
            gridDocument.DataSource = o.Attachments;
            gridDocument.DataBind();

            Control currentControl = this;
            while (currentControl != null)
            {
                if (currentControl is UITabView)
                {
                    string text = ((UITabView)currentControl).Caption;
                    int index = text.IndexOf(" <sup>");
                    if (index >= 0)
                        text = text.Substring(0, index);

                    objectPanel panel = getPanel(Page);
                    if (panel.SessionObject is LogicLayerPersistentObject)
                        ((UITabView)currentControl).Caption = text + " <sup>" +
                            ((LogicLayerPersistentObject)panel.SessionObject).Attachments.Count + "</sup>";
                }
                currentControl = currentControl.Parent;
            }

        }
    }

    protected List<OAttachmentType> cachedApplicableAttachmentTypes = null;

    /// <summary>
    /// Loads up a list of all applicable attachment types and cache it.
    /// </summary>
    /// <returns></returns>
    protected List<OAttachmentType> LoadApplicableAttachmentTypes()
    {
        if (cachedApplicableAttachmentTypes == null)
        {
            LogicLayerPersistentObject o = getPersistentObject(Page) as LogicLayerPersistentObject;
            List<OProcessSetupAttachmentType> processAttachmentTypes = o.GetApplicableAttachmentTypes();
            List<OAttachmentType> attachmentTypes = new List<OAttachmentType>();
            foreach (OProcessSetupAttachmentType processAttachmentType in processAttachmentTypes)
                attachmentTypes.Add(processAttachmentType.AttachmentType);
            cachedApplicableAttachmentTypes = attachmentTypes;
        }
        return cachedApplicableAttachmentTypes;
    }


    protected List<OAttachmentType> cachedPendingApplicableAttachmentTypes = null;

    /// <summary>
    /// Loads up a list of pending applicable attachment types and cache it.
    /// </summary>
    /// <returns></returns>
    protected List<OAttachmentType> LoadPendingApplicableAttachmentTypes()
    {
        if (cachedPendingApplicableAttachmentTypes == null)
        {
            LogicLayerPersistentObject o = getPersistentObject(Page) as LogicLayerPersistentObject;
            List<OProcessSetupAttachmentType> processAttachmentTypes = o.GetPendingApplicableAttachmentTypes();
            List<OAttachmentType> attachmentTypes = new List<OAttachmentType>();
            foreach (OProcessSetupAttachmentType processAttachmentType in processAttachmentTypes)
                attachmentTypes.Add(processAttachmentType.AttachmentType);
            cachedPendingApplicableAttachmentTypes = attachmentTypes;
        }
        return cachedPendingApplicableAttachmentTypes;
    }


    protected List<OAttachmentType> cachedPendingAttachmentTypes = null;

    /// <summary>
    /// Loads up a list of pending compulsory attachment types and cache it.
    /// </summary>
    /// <returns></returns>
    protected List<OAttachmentType> LoadPendingAttachmentTypes()
    {
        if (cachedPendingAttachmentTypes == null)
        {
            LogicLayerPersistentObject o = getPersistentObject(Page) as LogicLayerPersistentObject;
            List<OProcessSetupAttachmentType> processAttachmentTypes = o.GetPendingAttachmentTypes();
            List<OAttachmentType> attachmentTypes = new List<OAttachmentType>();
            foreach (OProcessSetupAttachmentType processAttachmentType in processAttachmentTypes)
                attachmentTypes.Add(processAttachmentType.AttachmentType);
            cachedPendingAttachmentTypes = attachmentTypes;
        }
        return cachedPendingAttachmentTypes;
    }


    /// <summary>
    /// Binds the attachments dropdown list.
    /// </summary>
    protected void BindAttachmentsDropDownList()
    {
        LogicLayerPersistentObject o = getPersistentObject(Page) as LogicLayerPersistentObject;
        if (o != null)
        {
            List<OAttachmentType> attachmentTypes = LoadApplicableAttachmentTypes();

            for (int i = 0; i < attachmentTypeIDDropDown.Length; i++)
                attachmentTypeIDDropDown[i].Bind(attachmentTypes, true);
        }
    }


    /// <summary>
    /// Show pending attachment types.
    /// </summary>
    protected void ShowPendingAttachmentTypes()
    {
        LogicLayerPersistentObject o = getPersistentObject(Page);
        if (o == null)
            return;

        // Load up the compulsory / applicable attachment types for comparison.
        //
        List<OAttachmentType> compulsoryAttachmentTypes = LoadPendingAttachmentTypes();
        List<OAttachmentType> applicableAttachmentTypes = LoadPendingApplicableAttachmentTypes();
        Hashtable compulsoryAttachmentTypesHash = new Hashtable();
        foreach (OAttachmentType attachmentType in compulsoryAttachmentTypes)
            compulsoryAttachmentTypesHash[attachmentType.ObjectID.Value] = attachmentType;

        // Enables and resets all attachment type dropdown list
        //
        for (int i = 0; i < attachmentTypeIDDropDown.Length; i++)
        {
            attachmentTypeIDDropDown[i].SelectedIndex = -1;
            attachmentTypeIDDropDown[i].Enabled = true;
            attachmentInputFile[i].Caption = "";
        }

        if (compulsoryAttachmentTypes.Count > 0 ||
            applicableAttachmentTypes.Count > 0)
        {
            string message = Resources.Messages.Attachment_CompulsoryDocumentsHint + "<br/><ul>";

            // Construct the message for compulsory documents,
            // and automatically select the dropdown list in 
            // the file upload pop-up box.
            //
            int count = 0;
            compulsoryAttachmentTypes.Sort("ObjectName");
            foreach (OAttachmentType attachmentType in compulsoryAttachmentTypes)
            {
                if (count < attachmentTypeIDDropDown.Length)
                {
                    attachmentTypeIDDropDown[count].SelectedValue = attachmentType.ObjectID.ToString();
                    attachmentTypeIDDropDown[count].Enabled = false;
                    if (attachmentTypeIDDropDown[count].SelectedItem != null)
                        attachmentInputFile[count].Caption = attachmentTypeIDDropDown[count].SelectedItem.Text;
                }
                message += "<li><b>" + attachmentType.ObjectName + " " + Resources.Messages.Attachment_Compulsory + "</b></li>";
                count++;
            }

            // Construct the message for the remaining applicable,
            // but non-compulsory documents.
            //
            foreach (OAttachmentType attachmentType in applicableAttachmentTypes)
            {
                if (compulsoryAttachmentTypesHash[attachmentType.ObjectID.Value] == null)
                {
                    message += "<li>" + attachmentType.ObjectName + " " + Resources.Messages.Attachment_Optional + "</li>";
                }
            }

            PendingAttachmentTypesHint.Text = message + "</ul><br/>Please ensure that each file does not exceed <b>10 Megabytes (10240 KB)</b>";

            PendingAttachmentTypesHint.Visible = true;
        }
        else
        {
            PendingAttachmentTypesHint.Visible = false;
        }
    }


    /// <summary>
    /// Occurs when the user clicks on a button in the UIGridView.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="commandName"></param>
    /// <param name="objectIds"></param>
    protected void gridDocument_Action(object sender, string commandName, System.Collections.Generic.List<object> objectIds)
    {
        if (commandName == "ViewDocument")
        {
            // View the document, so load it from
            // database and let user download it.
            //
            using (Connection c = new Connection())
            {
                foreach (Guid objectId in objectIds)
                {
                    string contentType = "";
                    string fileName = "";
                    LogicLayerPersistentObject o = getPersistentObject(Page) as LogicLayerPersistentObject;
                    if (o != null)
                        foreach (OAttachment b in o.Attachments)
                            if (b.ObjectID.Value == objectId)
                            {
                                getPanel(Page).FocusWindow = false;
                                Window.Download(b.FileBytes, b.Filename, b.ContentType);
                                break;
                            }
                }
            }
        }
        if (commandName == "DeleteDocument")
        {
            // remove the document from the database.
            //
            LogicLayerPersistentObject o = getPersistentObject(Page);
            if (o != null)
            {
                foreach (Guid objectId in objectIds)
                    o.Attachments.RemoveGuid(objectId);

                BindAttachments(o);
                ShowPendingAttachmentTypes();
            }

            if (Page is UIPageBase)
                ((UIPageBase)Page).SetModifiedFlag();
        }
        if (commandName == "UploadDocument")
        {
            LogicLayerPersistentObject o = getPersistentObject(Page) as LogicLayerPersistentObject;
            if (o != null)
            {
                ShowPendingAttachmentTypes();
            }

            UploadAttachmentDialogBox.Show();

        }
    }


    /// <summary>
    /// Finds and returns the objectPanel.ascx control.
    /// </summary>
    /// <param name="c"></param>
    /// <returns></returns>
    protected objectPanel getPanel(Control c)
    {
        if (c.GetType() == typeof(objectPanel))
            return (objectPanel)c;
        foreach (Control child in c.Controls)
        {
            objectPanel o = getPanel(child);
            if (o != null)
                return o;
        }
        return null;
    }


    // hunts for the objectPanel.ascx control and finds the PersistentObject
    // the OAttachment object to the attached object
    //
    /// <summary>
    /// Finds the objectPanel.ascx control, then returns the CurrentObject
    /// stored in that control.
    /// </summary>
    /// <param name="c"></param>
    /// <returns></returns>
    protected LogicLayerPersistentObject getPersistentObject(Control c)
    {
        if (c.GetType() == typeof(objectPanel))
        {
            //return (LogicLayerPersistentObject)((objectPanel)c).CurrentObject;
            return ((objectPanel)c).SessionObject as LogicLayerPersistentObject;
        }
        foreach (Control child in c.Controls)
        {
            LogicLayerPersistentObject o = getPersistentObject(child);
            if (o != null)
                return o;
        }
        return null;
    }


    /// <summary>
    /// Hides the input file control if this control is disabled.
    /// Also updates the tabview's caption to show the number
    /// of attachments in this object.
    /// </summary>
    /// <param name="e"></param>
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        //KL: display the allowed file types from application settings in hint
        string extension = "";
        foreach (string ext in OApplicationSetting.Current.AllowedFileTypeExtension.Split(','))
        {
            extension += ext.ToUpper() + ", ";
        }

        //extension end with ", ", so use length minus 2
        UploadHint.Text = string.Format(UploadHint.Text, extension.Substring(0, extension.Length - 2));
    }


    /// <summary>
    /// Adds the uploaded attachment into the
    /// attachment table.
    /// </summary>
    /// <param name="attachmentType"></param>
    /// <param name="postedFile"></param>
    protected OAttachment AddUploadedAttachment(string attachmentTypeID, HttpPostedFile postedFile, string description)
    {
        if (postedFile != null && postedFile.ContentLength > 0)
        {
            OAttachment a = TablesLogic.tAttachment.Create();
            byte[] fileBytes = new byte[postedFile.ContentLength];
            postedFile.InputStream.Position = 0;
            postedFile.InputStream.Read(fileBytes, 0, fileBytes.Length);

            a.FileBytes = fileBytes;
            a.Filename = Path.GetFileName(postedFile.FileName);
            a.FileSize = postedFile.ContentLength;
            a.ContentType = postedFile.ContentType;
            a.AttachmentTypeID = attachmentTypeID == "" ? (Guid?)null : new Guid(attachmentTypeID);
            a.FileDescription = description;

            objectPanel panel = getPanel(Page);
            LogicLayerPersistentObject o = panel.SessionObject as LogicLayerPersistentObject;
            if (o != null)
            {
                a.AttachedObjectTypeName = o.ObjectTypeName;

                // Look for any files that have been recently 
                // uploaded (before the object was saved).
                // If so, we assume it's a duplicate file 
                // and we won't add it to the attachments. 
                // This is used to prevent duplicates from being
                // added when the user refreshes the page, or
                // when the file was re-uploaded when the user clears
                // the IE security prompt to download files to the computer.
                //
                bool previouslyUploaded = false;
                foreach (OAttachment existingAttachment in o.Attachments)
                    if (existingAttachment.IsNew &&
                        existingAttachment.Filename == a.Filename &&
                        existingAttachment.FileSize == a.FileSize)
                    {
                        previouslyUploaded = true;
                        break;
                    }

                //if (a.AttachmentTypeID != null)
                if (!previouslyUploaded)
                {
                    o.Attachments.Add(a);

                    if (Page is UIPageBase)
                        ((UIPageBase)Page).SetModifiedFlag();
                }
            }
            return a;
        }
        return null;
    }


    /// <summary>
    /// REturns a flag to indicate if the extension is allowed.
    /// </summary>
    /// <param name="c"></param>
    /// <returns></returns>
    protected bool ExtensionAllowed(UIFieldInputFile c)
    {
        if (c != null && c.PostedFile != null && c.PostedFile.ContentLength > 0)
        {
            c.ErrorMessage = "";
            string extension = System.IO.Path.GetExtension(c.PostedFile.FileName).ToLower();

            bool allowed = false;
            foreach (string ext in OApplicationSetting.Current.AllowedFileTypeExtension.Split(','))
            {
                if (extension == "." + ext.ToLower())
                {
                    allowed = true;
                }
            }

            if (allowed)
            {
            }
            else
            {
                c.ErrorMessage = String.Format(Resources.Errors.General_ExtensionNotAllowed, extension);
                return false;
            }

            if (!FileFormats.HasMatchingHeader(extension, c.PostedFile.InputStream))
            {
                c.ErrorMessage = String.Format(Resources.Errors.General_FileFormatInvalid, c.PostedFile.FileName, extension);
                return false;
            }

            return true;
        }
        return true;
    }


    /// <summary>
    /// Occurs when the user clicks "Upload" or "Cancel" in the
    /// dialog box.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void UploadAttachmentDialogBox_ButtonClicked(object sender, ButtonClickedEventArgs e)
    {
        if (!UploadAttachmentDialogBox.IsValid)
            return;

        if (e.CommandName == "Upload")
        {
            // Validates to ensure that only matching extensions are allowed
            //
            bool extensionAllowed = true;
            for (int i = 0; i < attachmentInputFile.Length; i++)
                extensionAllowed = ExtensionAllowed(attachmentInputFile[i]) && extensionAllowed;

            if (!extensionAllowed)
                return;

            List<OAttachment> attachmentsUploaded = new List<OAttachment>();
            OAttachment att = null;

            for (int i = 0; i < attachmentTypeIDDropDown.Length; i++)
            {
                att = AddUploadedAttachment(
                    attachmentTypeIDDropDown[i].SelectedValue,
                    attachmentInputFile[i].PostedFile,
                    attachmentDescriptionTextBox[i].Text);
                if (att != null) attachmentsUploaded.Add(att);
            }

            /*            
                        // Validates to ensure that the attachment types are specified.
                        //
                        foreach (OAttachment attachment in attachmentsUploaded)
                        {
                            if (attachment.AttachmentTypeID == null)
                            {
                                getPanel(this.Page).Message = Resources.Errors.Attachment_DocumentType;
                                UploadAttachmentDialogBox.Show();
                                return;
                            }
                        }
                          */
            ShowPendingAttachmentTypes();

            if (attachmentsUploaded.Count > 0 && AttachmentUploaded != null)
            {
                AttachmentUploadedEventArgs e2 = new AttachmentUploadedEventArgs();
                e2.AttachmentsUploaded = attachmentsUploaded;
                AttachmentUploaded(e2);

            }

            LogicLayerPersistentObject o = getPersistentObject(Page) as LogicLayerPersistentObject;
            BindAttachments(o);
            UploadAttachmentDialogBox.Hide();
        }
    }
    
    /// <summary>
    /// Validates the attachment control.
    /// </summary>
    /// <param name="source"></param>
    /// <param name="args"></param>
    public void ValidateAttachments(String applicationWorkflowAction)
    {
        LogicLayerPersistentObject o = getPersistentObject(Page);
        objectPanel panel = getPanel(Page);

        //if (!(o is LogicLayerWorkflowPersistentObject))
        //    return;
		
		if (o == null)
            return;

        BindAttachments(o);
        BindAttachmentsDropDownList();
        ShowPendingAttachmentTypes();
        
        LogicLayerPersistentObject w = o as LogicLayerPersistentObject;
        List<OProcessSetupAttachmentType> attachmentTypes = w.GetPendingAttachmentTypes(applicationWorkflowAction);

        if (attachmentTypes.Count > 0)
        {
            gridDocument.ErrorMessage = Resources.Errors.Attachment_DocumentsNotComplete;

            // See if there are any new attachments, if so, 
            // add the message to tell user to click save instead.
            //
            foreach (OAttachment attachment in o.Attachments)
                if (attachment.IsNew)
                {
                    gridDocument.ErrorMessage += " " + Resources.Errors.Attachment_SaveOnly;
                    break;
                }
        }
    }

    public class AttachmentUploadedEventArgs
    {
        public List<OAttachment> AttachmentsUploaded;
    }

    public delegate void AttachmentUploadedEventHandler(AttachmentUploadedEventArgs e);

    public event AttachmentUploadedEventHandler AttachmentUploaded;


    /*
    protected void AttachmentTypeID01_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
     */

    protected void gridDocument_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header ||
            e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.Cells[3].Text.ToLower() == "false")
                e.Row.Cells[2].Controls[0].Visible = false;
            e.Row.Cells[3].Visible = false;
        }
    }
</script>

<ui:UISeparator runat="server" ID="AttachmentSeparator" Caption="Attachments" meta:resourcekey="AttachmentSeparatorResource1"/>
<ui:UIHint runat="server" ID="PendingAttachmentTypesHint" Text="" meta:resourcekey="PendingAttachmentTypesHintResource" >
</ui:UIHint>
<ui:UIDialogBox runat="server" ID="UploadAttachmentDialogBox"
    Title="Upload Attachments"
    Button1Text="Upload" Button1CommandName="Upload" Button1CausesValidation="true"
    Button1AutoClosesDialogBox="false"
    Button2Text="Cancel" Button2CommandName="Cancel" Button2CausesValidation="false"
    OnButtonClicked="UploadAttachmentDialogBox_ButtonClicked" DialogWidth="900px"
    meta:resourcekey="UploadAttachmentDialogBoxResource1">
    <table runat="server" id="UploadAttachmentTable" border='0' cellpadding='2' cellspacing='0'
        style="table-layout: fixed;" class='grid mdl-data-table'>
        <tr class='grid-header'>
            <th style="width: 300px">
                <asp:Label runat="server" ID="AttachmentTypeLabel" Text="Attachment Type" meta:resourcekey="AttachmentTypeLabelResource1"></asp:Label>
            </th>
            <th style="width: 300px">
                <asp:Label runat="server" ID="InputFileLabel" Text="File to Upload" meta:resourcekey="InputFileLabelResource1"></asp:Label>
            </th>
            <th style="width: 200px">
                <asp:Label runat="server" ID="AttachmentDescription" Text="Description" meta:resourcekey="AttachmentDescriptionResource1"></asp:Label>
            </th>
        </tr>
        <tr class='grid-row' valign="top">
            <td>
                <ui:UIFieldDropDownList runat="server" ID="AttachmentTypeID01" ShowCaption='false'
                    FieldLayout="Flow" Caption="Attachment Type"
                    Span="full" meta:resourcekey="AttachmentTypeID01Resource" >
                </ui:UIFieldDropDownList>
            </td>
            <td>
                <ui:UIFieldInputFile runat="server" ID="InputFile01" Caption="File" ShowCaption='false'
                    FieldLayout="Flow" meta:resourcekey="inputFileResource1">
                </ui:UIFieldInputFile>
            </td>
            <td>
                <ui:UIFieldTextBox runat="server" ID="AttachmentDescription01" Caption="Description"
                    ShowCaption='false' FieldLayout="Flow" meta:resourcekey="inputFileResource1">
                </ui:UIFieldTextBox>
            </td>
        </tr>
        <tr class='grid-row' valign="top">
            <td>
                <ui:UIFieldDropDownList runat="server" ID="AttachmentTypeID02" ShowCaption='false'
                    FieldLayout="Flow" Caption="Attachment Type"
                    Span="full" meta:resourcekey="AttachmentTypeID02Resource" >
                </ui:UIFieldDropDownList>
            </td>
            <td>
                <ui:UIFieldInputFile runat="server" ID="InputFile02" Caption="File" ShowCaption='false'
                    FieldLayout="Flow" meta:resourcekey="inputFileResource1">
                </ui:UIFieldInputFile>
            </td>
            <td>
                <ui:UIFieldTextBox runat="server" ID="AttachmentDescription02" Caption="Description"
                    ShowCaption='false' FieldLayout="Flow" meta:resourcekey="inputFileResource1">
                </ui:UIFieldTextBox>
            </td>
        </tr>
        <tr class='grid-row' valign="top">
            <td>
                <ui:UIFieldDropDownList runat="server" ID="AttachmentTypeID03" ShowCaption='false'
                    FieldLayout="Flow" Caption="Attachment Type"
                    Span="full" meta:resourcekey="AttachmentTypeID03Resource" >
                </ui:UIFieldDropDownList>
            </td>
            <td>
                <ui:UIFieldInputFile runat="server" ID="InputFile03" Caption="File" ShowCaption='false'
                    FieldLayout="Flow" meta:resourcekey="inputFileResource1">
                </ui:UIFieldInputFile>
            </td>
            <td>
                <ui:UIFieldTextBox runat="server" ID="AttachmentDescription03" Caption="Description"
                    ShowCaption='false' FieldLayout="Flow" meta:resourcekey="inputFileResource1">
                </ui:UIFieldTextBox>
            </td>
        </tr>
        <tr class='grid-row' valign="top">
            <td>
                <ui:UIFieldDropDownList runat="server" ID="AttachmentTypeID04" ShowCaption='false'
                    FieldLayout="Flow" Caption="Attachment Type"
                    Span="full" meta:resourcekey="AttachmentTypeID04Resource" >
                </ui:UIFieldDropDownList>
            </td>
            <td>
                <ui:UIFieldInputFile runat="server" ID="InputFile04" Caption="File" ShowCaption='false'
                    FieldLayout="Flow" meta:resourcekey="inputFileResource1">
                </ui:UIFieldInputFile>
            </td>
            <td>
                <ui:UIFieldTextBox runat="server" ID="AttachmentDescription04" Caption="Description"
                    ShowCaption='false' FieldLayout="Flow" meta:resourcekey="inputFileResource1">
                </ui:UIFieldTextBox>
            </td>
        </tr>
        <tr class='grid-row' valign="top">
            <td>
                <ui:UIFieldDropDownList runat="server" ID="AttachmentTypeID05" ShowCaption='false'
                    FieldLayout="Flow" Caption="Attachment Type"
                    Span="full" meta:resourcekey="AttachmentTypeID05Resource" >
                </ui:UIFieldDropDownList>
            </td>
            <td>
                <ui:UIFieldInputFile runat="server" ID="InputFile05" Caption="File" ShowCaption='false'
                    FieldLayout="Flow" meta:resourcekey="inputFileResource1">
                </ui:UIFieldInputFile>
            </td>
            <td>
                <ui:UIFieldTextBox runat="server" ID="AttachmentDescription05" Caption="Description"
                    ShowCaption='false' FieldLayout="Flow" meta:resourcekey="inputFileResource1">
                </ui:UIFieldTextBox>
            </td>
        </tr>
        <tr class='grid-row' valign="top">
            <td>
                <ui:UIFieldDropDownList runat="server" ID="AttachmentTypeID06" ShowCaption='false'
                    FieldLayout="Flow" Caption="Attachment Type"
                    Span="full" meta:resourcekey="AttachmentTypeID06Resource" >
                </ui:UIFieldDropDownList>
            </td>
            <td>
                <ui:UIFieldInputFile runat="server" ID="InputFile06" Caption="File" ShowCaption='false'
                    FieldLayout="Flow" meta:resourcekey="inputFileResource1">
                </ui:UIFieldInputFile>
            </td>
            <td>
                <ui:UIFieldTextBox runat="server" ID="AttachmentDescription06" Caption="Description"
                    ShowCaption='false' FieldLayout="Flow" meta:resourcekey="inputFileResource1">
                </ui:UIFieldTextBox>
            </td>
        </tr>
        <tr class='grid-row' valign="top">
            <td>
                <ui:UIFieldDropDownList runat="server" ID="AttachmentTypeID07" ShowCaption='false'
                    FieldLayout="Flow" Caption="Attachment Type"
                    Span="full" meta:resourcekey="AttachmentTypeID07Resource" >
                </ui:UIFieldDropDownList>
            </td>
            <td>
                <ui:UIFieldInputFile runat="server" ID="InputFile07" Caption="File" ShowCaption='false'
                    FieldLayout="Flow" meta:resourcekey="inputFileResource1">
                </ui:UIFieldInputFile>
            </td>
            <td>
                <ui:UIFieldTextBox runat="server" ID="AttachmentDescription07" Caption="Description"
                    ShowCaption='false' FieldLayout="Flow" meta:resourcekey="inputFileResource1">
                </ui:UIFieldTextBox>
            </td>
        </tr>
        <tr class='grid-row' valign="top">
            <td>
                <ui:UIFieldDropDownList runat="server" ID="AttachmentTypeID08" ShowCaption='false'
                    FieldLayout="Flow" Caption="Attachment Type"
                    Span="full" meta:resourcekey="AttachmentTypeID08Resource" >
                </ui:UIFieldDropDownList>
            </td>
            <td>
                <ui:UIFieldInputFile runat="server" ID="InputFile08" Caption="File" ShowCaption='false'
                    FieldLayout="Flow" meta:resourcekey="inputFileResource1">
                </ui:UIFieldInputFile>
            </td>
            <td>
                <ui:UIFieldTextBox runat="server" ID="AttachmentDescription08" Caption="Description"
                    ShowCaption='false' FieldLayout="Flow" meta:resourcekey="inputFileResource1">
                </ui:UIFieldTextBox>
            </td>
        </tr>
        <tr class='grid-row' valign="top">
            <td>
                <ui:UIFieldDropDownList runat="server" ID="AttachmentTypeID09" ShowCaption='false'
                    FieldLayout="Flow" Caption="Attachment Type"
                    Span="full" meta:resourcekey="AttachmentTypeID09Resource" >
                </ui:UIFieldDropDownList>
            </td>
            <td>
                <ui:UIFieldInputFile runat="server" ID="InputFile09" Caption="File" ShowCaption='false'
                    FieldLayout="Flow" meta:resourcekey="inputFileResource1">
                </ui:UIFieldInputFile>
            </td>
            <td>
                <ui:UIFieldTextBox runat="server" ID="AttachmentDescription09" Caption="Description"
                    ShowCaption='false' FieldLayout="Flow" meta:resourcekey="inputFileResource1">
                </ui:UIFieldTextBox>
            </td>
        </tr>
        <tr class='grid-row' valign="top">
            <td>
                <ui:UIFieldDropDownList runat="server" ID="AttachmentTypeID10" ShowCaption='false'
                    FieldLayout="Flow" Caption="Attachment Type"
                    Span="full" meta:resourcekey="AttachmentTypeID10Resource" >
                </ui:UIFieldDropDownList>
            </td>
            <td>
                <ui:UIFieldInputFile runat="server" ID="InputFile10" Caption="File" ShowCaption='false'
                    FieldLayout="Flow" meta:resourcekey="inputFileResource1">
                </ui:UIFieldInputFile>
            </td>
            <td>
                <ui:UIFieldTextBox runat="server" ID="AttachmentDescription10" Caption="Description"
                    ShowCaption='false' FieldLayout="Flow" meta:resourcekey="inputFileResource1">
                </ui:UIFieldTextBox>
            </td>
        </tr>
    </table>
    <table runat="server" id="UploadAttachmentTableMobile" border='0' cellpadding='2'
        cellspacing='0' style="table-layout: fixed; width: 100%" class='grid' visible="false">
        <tr>
            <td>
                <ui:UIFieldDropDownList runat="server" ID="AttachmentTypeID11" Caption="Attachment Type"
                    Span="full" meta:resourcekey="AttachmentTypeID11Resource" >
                </ui:UIFieldDropDownList>
                <ui:UIFieldInputFile runat="server" ID="InputFile11" Caption="File" Span="full" meta:resourcekey="InputFile11Resource" >
                </ui:UIFieldInputFile>
                <ui:UIFieldTextBox runat="server" ID="AttachmentDescription11" Caption="Description"
                    Span="full" meta:resourcekey="AttachmentDescription11Resource" >
                </ui:UIFieldTextBox>
            </td>
        </tr>
    </table>
    <ui:UIHint runat="server" ID="UploadHint" Text="Please ensure that each file does not exceed <b>10 Megabytes (10240 KB)</b>. Please upload only {0} files."
        meta:resourcekey ="UploadHintResource1">
    </ui:UIHint>
</ui:UIDialogBox>
<ui:UIGridView runat='server' ID="gridDocument" OnAction="gridDocument_Action" Caption="Attachments"
    CheckBoxColumnVisible="false"
    CaptionWidth="120px" KeyName="ObjectID" meta:resourcekey="gridDocumentResource1"
    OnRowDataBound="gridDocument_RowDataBound">
    <Commands>
        <ui:UIGridViewCommand CommandName="UploadDocument" CommandText="Upload" ImageUrl="~/images/upload-black-16.png"
            meta:resourcekey="UIGridViewCommandResource2" />
        <ui:UIGridViewCommand CommandName="DeleteDocument" CommandText="Delete" ImageUrl="../images/delete.gif"
            ActsOnSelectedItems="true" Visible="false"
            ConfirmText="Are you sure you wish to delete the selected documents?" meta:resourcekey="UIGridViewCommandResource1" />
    </Commands>
    <Columns>
        <ui:UIGridViewButtonColumn ImageUrl="~/images/download-black-16.png" CommandName="ViewDocument"
            HeaderText="" meta:resourcekey="UIGridViewColumnResource1" AlwaysEnabled="true">
        </ui:UIGridViewButtonColumn>
        <ui:UIGridViewButtonColumn ImageUrl="../images/delete.gif" CommandName="DeleteDocument"
            HeaderText="" ConfirmText="Are you sure you wish to delete this document?" meta:resourcekey="UIGridViewColumnResource2">
        </ui:UIGridViewButtonColumn>
        <ui:UIGridViewBoundColumn PropertyName="IsNew" HeaderText="" MobileDeviceVisibility="DesktopModeOnly" meta:resourcekey="gridDocumentUIGridViewBoundColumnIsNewResource" >
        </ui:UIGridViewBoundColumn>
        <ui:UIGridViewBoundColumn PropertyName="AttachmentType.ObjectName" HeaderText="Attachment Type"
            MobileDeviceVisibility="DesktopModeOnly" meta:resourcekey="gridDocumentColumnResource1">
        </ui:UIGridViewBoundColumn>
        <ui:UIGridViewBoundColumn PropertyName="Filename" HeaderText="File Name" meta:resourcekey="UIGridViewColumnResource3"
            MobileDeviceVisibility="DesktopModeOnly">
        </ui:UIGridViewBoundColumn>
        <ui:UIGridViewBoundColumn PropertyName="FileSize" HeaderText="File Size (bytes)"
            DataFormatString="{0:#,##0}" meta:resourcekey="UIGridViewColumnResource4" MobileDeviceVisibility="DesktopModeOnly">
        </ui:UIGridViewBoundColumn>
        <ui:UIGridViewBoundColumn PropertyName="FileDescription" HeaderText="File Description"
            MobileDeviceVisibility="DesktopModeOnly" meta:resourcekey="gridDocumentColumnResource2">
        </ui:UIGridViewBoundColumn>
        <ui:UIGridViewTemplateColumn ID="MobileColumn" MobileDeviceVisibility="MobileModeOnly" meta:resourcekey="MobileColumnResource" >
            <ItemTemplate>
                <small style='color: Gray;'><%# ((System.Data.DataRowView)(Container.DataItem))["AttachmentType.ObjectName"] %><br />
                </small>
                <b></b><%# ((System.Data.DataRowView)(Container.DataItem))["Filename"]%><br />
                <b>
                    <small><%# ((System.Data.DataRowView)(Container.DataItem))["FileDescription"] %></small>
            </ItemTemplate>
        </ui:UIGridViewTemplateColumn>
    </Columns>
</ui:UIGridView>
