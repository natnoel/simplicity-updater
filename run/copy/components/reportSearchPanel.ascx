<%@ Control Language="C#" ClassName="reportSearchPanel" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="System.Web.UI" %>
<%@ Import Namespace="System.Web.UI.WebControls" %>
<%@ Import Namespace="System.ComponentModel" %>
<%@ Import Namespace="Anacle.DataFramework" %>
<%@ Import Namespace="LogicLayer" %>
<%@ Import Namespace="Anacle.WorkflowFramework" %>

<script runat="server">
    /// <summary>
    /// SubCaption below the main caption.
    /// </summary>
    public string SubCaption
    {
        get
        {
            return this.labelSubCaption.Text;
        }
        set
        {
            this.labelSubCaption.Text = value;
        }
    }


    /// <summary>
    /// SubCaption Visible Flag
    /// </summary>
    public bool SubCaptionVisible
    {
        get
        {
            return this.labelSubCaption.Visible;
        }
        set
        {
            this.labelSubCaption.Visible = value;
        }
    }


    /// <summary>
    /// Gets or sets the caption that appears in the title of
    /// the objectSearchPanel.ascx control.
    /// </summary>
    [Localizable(true), Description("Gets or sets the caption that appears in the title of the objectSearchPanel.ascx control.")]
    public string Caption
    {
        get
        {
            return labelCaption.Text;
        }
        set
        {
            labelCaption.Text = value;
        }
    }


    /// <summary>
    /// Gets or sets the message that appears in the message bar. The message
    /// bar will show only if the Message property is not an empty string.
    /// </summary>
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    [Description("Gets or sets the message that appears in the message bar. The message bar will show only if the Message property is not an empty string.")]
    public string Message
    {
        get
        {
            return labelMessage.Text;
        }
        set
        {
            if (value != null && value.Trim() != "")
                panelMessage.Update();
            labelMessage.Text = value;
        }
    }

    /// <summary>
    /// Gets the UIObjectPanel that contains this edit panel.
    /// <para></para>
    /// Note: A UITabStrip is a subclass of the UIObjectPanel class.
    /// </summary>
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public UIObjectPanel ObjectPanel
    {
        get
        {
            // look for the containing UIForm control, and calls
            // the ValidateControls method.
            //
            Control c = this.Parent;
            while (c != null)
            {
                if (c is UIObjectPanel)
                    break;
                c = c.Parent;
            }
            if (c != null && c is UIObjectPanel)
                return (UIObjectPanel)c;
            else
                throw new Exception("The web:panel control must be placed in a UIObjectPanel control, but it is currently not.");
        }
    }



    bool commandVisibleSet = false;
    UIButton buttonAddSelected = new UIButton();
    Literal literalBreak = new Literal();
    UIGridViewCommand commandAddSelected = new UIGridViewCommand();


    /// <summary>
    /// Initialize.
    /// </summary>
    /// <param name="e"></param>
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
    }


    /// <summary>
    /// Load.
    /// </summary>
    /// <param name="e"></param>
    protected override void OnLoad(EventArgs e)
    {
        this.Message = "";

        base.OnLoad(e);

        if (!IsPostBack)
        {
            if (ConfigurationManager.AppSettings["LoadTesting"] == "false")
            {
                if (Request["TYPE"] != null)
                {
                    if (!AppSession.User.AllowSearch(GetRequestObjectType()))
                        Response.Redirect(Page.ResolveUrl("~/appobjectnoaccess.aspx"));

                    // Check for the salt. 
                    //
                    string[] args = Security.Decrypt(Request["TYPE"]).Split(':');
                    if (args.Length > 0 && args[args.Length - 1].StartsWith("SLT-"))
                    {
                        if (args[args.Length - 1] != AppSession.SaltID)
                            Response.Redirect(Page.ResolveUrl("~/appobjectnoaccess.aspx"));
                    }
                }
            }

            if (PopulateForm != null)
                PopulateForm(this, new EventArgs());

        }
    }


    /// <summary>
    /// Recursively gets the first tree list. 
    /// </summary>
    /// <param name="c"></param>
    /// <returns></returns>
    protected UIFieldTreeList GetUIFieldTreeList(Control c)
    {
        if (c is UIFieldTreeList)
            return (UIFieldTreeList)c;

        foreach (Control child in c.Controls)
        {
            UIFieldTreeList t = GetUIFieldTreeList(child);
            if (t != null)
                return t;
        }
        return null;
    }


    /// <summary>
    /// Pre-render.
    /// </summary>
    /// <param name="e"></param>
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        panelMessage.Visible = this.Message.Trim() != "";

        Window.WriteJavascript(
            "function Refresh() { document.getElementById('" + hiddenRefresh.ClientID + "').value='1'; " +
            Page.ClientScript.GetPostBackEventReference(buttonSearchNoFocus.LinkButton, "refresh") + " }");


    }


    /// <summary>
    /// Clear data entered into all controls.
    /// </summary>
    /// <param name="control"></param>
    void ClearControls(Control control)
    {
        if (control.GetType().IsSubclassOf(typeof(UIFieldBase)))
        {
            if (control.ID != "SimpleSearchDropDownList")
            {
                //do not clear the search for dropdownlist.
                ((UIFieldBase)control).ControlValue = null;
                ((UIFieldBase)control).ControlValueTo = null;
            }
        }
        else
        {
            foreach (Control childControl in control.Controls)
                ClearControls(childControl);
        }
    }


    /// <summary>
    /// The name of the base table as declared in the TablesLogic class.
    /// This should be something like "tWork", or "tLocation", or
    /// "tUser".
    /// </summary>
    protected string baseTable;


    /// <summary>
    /// Gets or sets the name of the base table as declared in the TablesLogic
    /// class. This should be something like "tWork", or "tLocation", or
    /// "tUser".
    /// </summary>
    public string BaseTable
    {
        get
        {
            return baseTable;
        }
        set
        {
            baseTable = value;
        }
    }


    SchemaBase table = null;


    public delegate void SearchEventHandler();

    /// <summary>
    /// Occurs when the user performs a search by clicking on the
    /// Perform Search button, or when the application calls
    /// the PerformSearch() method.
    /// </summary>
    [Browsable(true)]
    public event SearchEventHandler Search;


    /// <summary>
    /// The Process Result event handler.
    /// </summary>
    /// <param name="e"></param>
    public delegate void ProcessResultEventHandler(Object e);


    /// <summary>
    /// Occurs before the search result binds to the gridview
    /// in PerformSearch() method.
    /// </summary>
    [Browsable(true)]
    public event ProcessResultEventHandler ProcessResult;


    public delegate void PagePanelClickEventHandler(object sender, string commandName);

    /// <summary>
    /// Occurs when a button is clicked.
    /// </summary>
    [Browsable(true)]
    public event PagePanelClickEventHandler Click;


    /// <summary>
    /// Gets the currently searching object type.
    /// </summary>
    /// <returns></returns>
    private string GetRequestObjectType()
    {
        if (Request["TYPE"] == null)
            return "";
        return Security.Decrypt(Request["TYPE"]).Split(':')[0];
    }


    /// <summary>
    /// Reflect on the Tables class and get the object whose name is
    /// specified in the BaseTable property.
    /// </summary>
    private void GetBaseTable()
    {
        Type baseTableType = typeof(TablesLogic);
        string baseTableName = baseTable;
        if (baseTable.StartsWith("TablesLogic."))
        {
            baseTableType = typeof(TablesLogic);
            baseTableName = baseTableName.Replace("TablesLogic.", "");
        }
        else if (baseTable.StartsWith("TablesWorkflow."))
        {
            baseTableType = typeof(TablesWorkflow);
            baseTableName = baseTableName.Replace("TablesWorkflow.", "");
        }
        else if (baseTable.StartsWith("TablesAuditTrail."))
        {
            baseTableType = typeof(TablesAuditTrail);
            baseTableName = baseTableName.Replace("TablesAuditTrail.", "");
        }

        table = (SchemaBase)UIBinder.GetValue(baseTableType, baseTableName, true);
        if (table == null)
            throw new Exception("Unknown table '" + baseTable + "'");
    }



    bool refreshTree = false;


    /// <summary>
    /// Peforms a search by:
    /// <list>
    /// <item>1. Obtaining the search condition through the data entered 
    ///          into the fields.</item>
    /// <item>2. Call the custom search for any add-on conditions</item>
    /// <item>3. Construct and execute the query</item>
    /// <item>4. Bind the results to the grid.</item>
    /// </list>
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void buttonSearch_Click(object sender, EventArgs e)
    {
        this.Message = "";
        if (!ObjectPanel.IsValid)
        {
            this.Message = ObjectPanel.CheckErrorMessages();
            return;
        }

        // Checks if the SimpleSearch dropdown list is pointing
        // to the current object. If not, we redirect the user
        // to the search page that searches for the object selected
        // in the dropdown list.
        //
        //if (SimpleSearchDropDownList.Visible)
        //{
        //    string[] p = SimpleSearchDropDownList.SelectedValue.Split('\x255');
        //    if (SimpleSearchDropDownList.SelectedValue == "")
        //    {
        //        string url = Page.ResolveUrl(OApplicationSetting.Current.HomePageUrl);
        //        Window.Open(url +
        //            (url.IndexOf("?") > 0 ? "&" : "?") +
        //            "S=" + HttpUtility.UrlEncode(SearchTextBox.Text), "frameBottom");
        //        return;
        //    }
        //    else if (p[1] != GetRequestObjectType())
        //    {
        //        string url = Page.ResolveUrl(p[0]);
        //        string objectTypeName = p[1];
        //        Window.Open(url +
        //            (url.IndexOf("?") > 0 ? "&" : "?") +
        //            "S=" + HttpUtility.UrlEncode(SearchTextBox.Text) +
        //            "&TYPE=" + HttpUtility.UrlEncode(Security.Encrypt(objectTypeName)), "frameBottom");
        //        return;
        //    }
        //}


        try
        {
            System.Data.DataTable dt = null;
            IEnumerable ie = null;
            ArrayList al = new ArrayList();

            // call the delegate function to see if there are additional conditions,
            // or if there is a custom dataset queried by the delegate function.
            //
            if (Search != null)
            {
                Search();
                panelMain.Update();
            }

        }
        catch (Exception ex)
        {
            Helpers.LogException(ex, this.Request);
            if (Helpers.IsDebug())
                this.Message = ex.Message + "<br/>" + ex.StackTrace.Replace("\n", "<br/>");
            else
                this.Message = String.Format(Resources.Errors.General_ErrorOccurred, DateTime.Now);
        }
    }


    /// <summary>
    /// Programmatically performs a search, as if the user has clicked
    /// on the Reset Fields button.
    /// </summary>
    public void ResetFields()
    {
        buttonReset_Click(this, new EventArgs());
    }

    /// <summary>
    /// Programmatically performs a search, as if the user has clicked
    /// on the Perform Search button.
    /// </summary>
    public void PerformSearch()
    {
        buttonSearch_Click(this, new EventArgs());
    }


    /// <summary>
    /// Refresh the treeview if necessary, and re-do the search
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void buttonSearchNoFocus_Click(object sender, EventArgs e)
    {
        refreshTree = true;
        buttonSearch_Click(sender, e);
    }


    /// <summary>
    /// This event resets all fields in the page.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void buttonReset_Click(object sender, EventArgs e)
    {
        // recursively traverses through all controls in the list, and
        // sets the value to null to all controls.
        // 
        if (ObjectPanel != null)
            ClearControls(ObjectPanel);

    }



    /// <summary>
    /// Occurs when the user clicks on the Search button, 
    /// before the search is actually run against the database.
    /// </summary>
    public event EventHandler Validate;


    /// <summary>
    /// Occurs when the form is first loaded. This event is
    /// not called during post backs.
    /// </summary>
    public event EventHandler PopulateForm;

    /// <summary>
    /// Occurs after the user clicks the Add Selected button,
    /// and after all the necessary validations have taken
    /// place.
    /// </summary>
    public event EventHandler ValidateAndAddSelected;


    
</script>

<div class="mdl-title-bar-spacer mdl-title-bar-report">
    <div class="mdl-title-bar">
        <div class="mdl-title-bar-centralized-container">
            <div>
                <div style="float:left">
                    <asp:label runat="server" ID="labelCaption" CssClass="mdl-title-bar-label" Text="Announcements"></asp:label>
                    <asp:label runat="server" ID="labelSubCaption" CssClass="mdl-title-bar-label" Text="" Font-Size="X-Small" Visible="false"></asp:label>
                </div>
            </div>
            <div class="mdl-title-bar-buttons" style="clear:both">
                <ui:UIButton runat="server" ID="buttonSearch" ImageUrl="~/images/out-white-16.png" Text="Generate Report"
                    CausesValidation="true" OnClick="buttonSearch_Click" ButtonType="NormalRaised" ButtonColor="Blue" ConfirmText="" meta:resourcekey="buttonSearchResource1" IconCssClass="material-icons" IconText="play_arrow"
                    CssClass="btnblue" DisabledCssClass="btnblue-d"></ui:UIButton>
                <ui:UIButton runat="server" ID="buttonReset" ImageUrl="~/images/refresh-white-16.png" IconCssClass="material-icons" IconText="autorenew"
                    CssClass="btnblue" DisabledCssClass="btnblue-d" ButtonColor="White" Text="Reset Search Parameters"
                    OnClick="buttonReset_Click" ConfirmText="" meta:resourcekey="buttonResetResource1">
                </ui:UIButton>
            </div>
        </div>
    </div>
</div>
<ui:UIPanel runat="server" ID="panelMain">
    <ui:UIPanel runat="server" ID="panelMessage" HorizontalAlign="Center">
        <ui:UIFieldLabel runat='server' ID='labelMessage' meta:resourcekey="labelMessageResource1">
        </ui:UIFieldLabel>
    </ui:UIPanel>
</ui:UIPanel>
<ui:UIButton runat="server" ID="buttonSearchNoFocus" Text="" OnClick="buttonSearchNoFocus_Click"
    Style="display: none" ConfirmText="" meta:resourcekey="buttonSearchNoFocusResource1">
</ui:UIButton>
<asp:HiddenField ID="hiddenRefresh" runat="server"></asp:HiddenField>
