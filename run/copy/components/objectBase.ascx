<%@ Control Language="C#" ClassName="objectBase" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.Collections.ObjectModel" %>
<%@ Import Namespace="System.Drawing.Imaging" %>
<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="System.Web.UI" %>
<%@ Import Namespace="System.Web.UI.WebControls" %>
<%@ Import Namespace="System.Workflow.ComponentModel" %>
<%@ Import Namespace="System.Workflow.ComponentModel.Design" %>
<%@ Import Namespace="System.Workflow.Activities" %>
<%@ Import Namespace="System.Workflow.Runtime" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.ComponentModel" %>
<%@ Import Namespace="System.ComponentModel.Design" %>
<%@ Import Namespace="System.ComponentModel.Design.Serialization" %>
<%@ Import Namespace="Anacle.DataFramework" %>
<%@ Import Namespace="Anacle.WorkflowFramework" %>
<%@ Import Namespace="LogicLayer" %>
<%@ Register Src="objectPanel.ascx" TagName="object" TagPrefix="web2" %>

<script runat="server">

    /// <summary>
    /// Gets or sets the Object Name TextBox's Caption
    /// property.
    /// </summary>
    public string ObjectNameCaption
    {
        get
        {
            return ObjectName.Caption;
        }
        set
        {
            ObjectName.Caption = value;
        }
    }


    public string ObjectNameHint
    {
        get
        {
            return ObjectName.Hint;
        }
        set
        {
            ObjectName.Hint = value;
        }
    }


    public Unit ObjectNameCaptionInternalControlWidth
    {
        get
        {
            return _objectName.InternalControlWidth;
        }
        set
        {
            _objectName.InternalControlWidth = value;
        }
    }



    /// <summary>
    /// Gets or sets the Object Number TextBox's Caption
    /// property.
    /// </summary>
    public string ObjectNumberCaption
    {
        get
        {
            return ObjectNumber.Caption;
        }
        set
        {
            ObjectNumber.Caption = value;
        }
    }

    public string ObjectNumberHint
    {
        get
        {
            return ObjectNumber.Hint;
        }
        set
        {
            ObjectNumber.Hint = value;
        }
    }

    public Unit ObjectNumberCaptionInternalControlWidth
    {
        get
        {
            return _objectNumber.InternalControlWidth;
        }
        set
        {
            _objectNumber.InternalControlWidth = value;
        }
    }


    /// <summary>
    /// Gets or sets the Object Name TextBox's Tooltip
    /// property.
    /// </summary>
    public string ObjectNameTooltip
    {
        get
        {
            return _objectName.ToolTip;
        }
        set
        {
            _objectName.ToolTip = value;
        }
    }



    /// <summary>
    /// Gets or sets the Object Number TextBox's Tooltip
    /// </summary>
    public string ObjectNumberTooltip
    {
        get
        {
            return _objectNumber.ToolTip;
        }
        set
        {
            _objectNumber.ToolTip = value;
        }
    }


    /// <summary>
    /// Gets or sets the Object Name TextBox's MaxLength.
    /// </summary>
    public int ObjectNameMaxLength
    {
        get
        {
            return _objectName.MaxLength;
        }
        set
        {
            _objectName.MaxLength = value;
        }
    }


    /// <summary>
    /// Gets or sets the Object Name TextBox's MaxLength.
    /// </summary>
    public int ObjectNumberMaxLength
    {
        get
        {
            return _objectNumber.MaxLength;
        }
        set
        {
            _objectNumber.MaxLength = value;
        }
    }


    /// <summary>
    /// Gets or sets the Assigned User(s) TextBox's Visible property.
    /// </summary>
    [DefaultValue(true), Localizable(false)]
    public bool AssignedUserVisible
    {
        get
        {
            return panel_HideAssignedUser.Visible;
        }
        set
        {
            panel_HideAssignedUser.Visible = value;
        }
    }


    /// <summary>
    /// Gets or sets a flag to indicate whether the Object Name
    /// TextBox is visible.
    /// </summary>
    [DefaultValue(null), Localizable(false)]
    public bool ObjectNameVisible
    {
        get
        {
            return ObjectNamePanel.Visible;
        }
        set
        {
            ObjectNamePanel.Visible = value;
        }
    }


    /// <summary>
    /// Gets or sets a flag to indicate whether the Object Name
    /// TextBox is enabled.
    /// </summary>
    [DefaultValue(null), Localizable(false)]
    public bool ObjectNameEnabled
    {
        get
        {
            return _objectName.Enabled;
        }
        set
        {
            _objectName.Enabled = value;
        }
    }

    /// <summary>
    /// Gets or sets a flag to indicate whether the Object Name
    /// TextBox is a compulsory field.
    /// </summary>
    [DefaultValue(null), Localizable(false)]
    public bool ObjectNameValidateRequiredField
    {
        get
        {
            return _objectName.ValidateRequiredField;
        }
        set
        {
            _objectName.ValidateRequiredField = value;
        }
    }


    /// <summary>
    /// Gets or sets a flag to indicate whether the Object Number
    /// TextBox is visible.
    /// </summary>
    [DefaultValue(null), Localizable(false)]
    public bool ObjectNumberVisible
    {
        get
        {
            return ObjectNumberPanel.Visible;
        }
        set
        {
            ObjectNumberPanel.Visible = value;
        }
    }


    /// <summary>
    /// Gets or sets a flag to indicate whether the Object Number
    /// TextBox is enabled.
    /// </summary>
    [DefaultValue(null), Localizable(false)]
    public bool ObjectNumberEnabled
    {
        get
        {
            return _objectNumber.Enabled;
        }
        set
        {
            _objectNumber.Enabled = value;
        }
    }


    /// <summary>
    /// Gets or sets a flag to indicate whether the Object Number
    /// TextBox is a compulsory field.
    /// </summary>
    [DefaultValue(null), Localizable(false)]
    public bool ObjectNumberValidateRequiredField
    {
        get
        {
            return _objectNumber.ValidateRequiredField;
        }
        set
        {
            _objectNumber.ValidateRequiredField = value;
        }
    }

    [DefaultValue(null), Localizable(false)]
    public string ObjectNameErrorMessage
    {
        get
        {
            return _objectName.ErrorMessage;
        }
        set
        {
            _objectName.ErrorMessage = value;
        }
    }

    [DefaultValue(null), Localizable(false)]
    public string ObjectNumberErrorMessage
    {
        get
        {
            return _objectNumber.ErrorMessage;
        }
        set
        {
            _objectNumber.ErrorMessage = value;
        }
    }


    /// <summary>
    /// Gets the object name UIFieldTextBox control.
    /// </summary>
    [DefaultValue(null), Localizable(false)]
    public UIFieldTextBox ObjectName
    {
        get
        {
            return _objectName;
        }
    }

    /// <summary>
    /// Gets the object number UIFieldTextBox control.
    /// </summary>
    [DefaultValue(null), Localizable(false)]
    public UIFieldTextBox ObjectNumber
    {
        get
        {
            return _objectNumber;
        }
    }


    /// <summary>
    /// Gets the current object's state name.
    /// </summary>
    public string CurrentObjectState
    {
        get
        {
            PersistentObject o = getPanel(Page).SessionObject;
            if (!(o is LogicLayerWorkflowPersistentObject))
                return "";

            LogicLayerWorkflowPersistentObject logicLayerPersistentObject = o as LogicLayerWorkflowPersistentObject;

            if (logicLayerPersistentObject != null &&
                logicLayerPersistentObject.CurrentActivity != null &&
                logicLayerPersistentObject.CurrentActivity.ObjectName != null &&
                logicLayerPersistentObject.CurrentActivity.ObjectName.Trim() != "")
                return logicLayerPersistentObject.CurrentActivity.ObjectName;
            return "Start";
        }
    }



    /// <summary>
    /// Initialize events.
    /// </summary>
    /// <param name="e"></param>
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        objectPanel panel = getPanel(Page);
        panel.PopulateForm += new EventHandler(panel_PopulateForm);
        panel.ObjectBase = this;

    }


    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        // Mobile Web
        // 
        if (this.Page is UIPageBase && ((UIPageBase)this.Page).MobileMode)
        {
            this.ObjectName.Span = Span.Full;
            this.ObjectName.InternalControlWidth = Unit.Empty;

            this.ObjectNumber.Span = Span.Full;
            this.ObjectNumber.InternalControlWidth = Unit.Empty;

            Separator1.Visible = false;

        }
    }


    /// <summary>
    /// Populates the form.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="obj"></param>
    void panel_PopulateForm(object sender, EventArgs e)
    {
        // Clear the comments text box.
        //
        PersistentObject sessionObject = getPanel(Page).SessionObject;
        if (sessionObject is LogicLayerWorkflowPersistentObject)
        {
            if (((LogicLayerWorkflowPersistentObject)sessionObject).CurrentActivity != null)
            {
                ((LogicLayerWorkflowPersistentObject)sessionObject).CurrentActivity.TaskCurrentComments = "";

                if (((LogicLayerWorkflowPersistentObject)sessionObject).CurrentActivity.CurrentApprovalLevel != null)
                    labelCurrentApprovalLevel.Text = ((LogicLayerWorkflowPersistentObject)sessionObject).CurrentActivity.CurrentApprovalLevel.ToString();
                else
                    labelCurrentApprovalLevel.Text = "";

                if (((LogicLayerWorkflowPersistentObject)sessionObject).CurrentActivity.NumberOfApprovalsAtCurrentLevel != null)
                    labelApprovals.Text = ((LogicLayerWorkflowPersistentObject)sessionObject).CurrentActivity.NumberOfApprovalsAtCurrentLevel.ToString();
                else
                    labelApprovals.Text = "";

                if (((LogicLayerWorkflowPersistentObject)sessionObject).CurrentActivity.NumberOfApprovalsRequiredAtCurrentLevel != null)
                    labelApprovalsRequired.Text = ((LogicLayerWorkflowPersistentObject)sessionObject).CurrentActivity.NumberOfApprovalsRequiredAtCurrentLevel.ToString();
                else
                    labelApprovalsRequired.Text = "";

                // Added: Wang Yiyuan, 3/12/2012
                // Hide the Approve button if the currently login user
                // is the application created user
                //
                // CM : COMMENTED OUT 9/10/2012
                //objectPanel panel = getPanel(Page);
                //UIButton approve = (UIButton)panel.GetWorkflowActionButton("Approve");
                //if (approve != null)
                //{
                //    if (AppSession.User.ObjectID == ((LogicLayerPersistentObject)sessionObject).CreatedID)
                //    {
                //        approve.Visible = false;
                //    }
                //}

            }
        }

        BindWorkflowStatus(getPanel(Page).SessionObject);
        ShowWorkflow(getPanel(Page).SessionObject as LogicLayerWorkflowPersistentObject);

    }

    /// <summary>
    /// Initializes the workflow status label
    /// </summary>
    protected void BindWorkflowStatus(PersistentObject persistentObject)
    {

        if (!(persistentObject is IWorkflowEnabled) || !(persistentObject is LogicLayerPersistentObject))
            return;

        LogicLayerWorkflowPersistentObject logicLayerPersistentObject = persistentObject as LogicLayerWorkflowPersistentObject;


        if (logicLayerPersistentObject != null &&
            logicLayerPersistentObject.CurrentActivity != null &&
            logicLayerPersistentObject.CurrentActivity.WorkflowInstanceID != null)
        {
            // Translate and set the name of the current status to the
            // label on the user interface.
            //
            if (logicLayerPersistentObject.CurrentActivity != null)
            {
                string currentStateName = "";
                if (logicLayerPersistentObject.CurrentActivity.ObjectName != null)
                    currentStateName = logicLayerPersistentObject.CurrentActivity.ObjectName;

                string translatedStateName = Resources.WorkflowStates.ResourceManager.GetString(currentStateName);
                if (translatedStateName == null)
                    translatedStateName = logicLayerPersistentObject.CurrentActivity.ObjectName;

                //labelWorkflowName.Text = translatedStateName;
                panelStatus.Visible = true;

                // 2010.11.09
                // Kim Foong
                // Sets the color for the various states (currently hard-coded)
                //
                string cssClass = Helpers.GetWorkflowStateClass(currentStateName);

            }
        }

    }


    /// <summary>
    /// Finds and returns the objectPanel object.
    /// </summary>
    /// <param name="c">The control within which to find.</param>
    /// <returns>The objectPanel.ascx object.</returns>
    protected objectPanel getPanel(Control c)
    {
        if (c.GetType() == typeof(objectPanel))
            return (objectPanel)c;
        foreach (Control child in c.Controls)
        {
            objectPanel o = getPanel(child);
            if (o != null)
                return o;
        }
        return null;
    }


    protected string FindWorkflowStateCode(string currentStateName, XmlNodeList StateActivities)
    {
        foreach (XmlNode StateActivity in StateActivities)
        {
            if (StateActivity.Attributes["x:Name"] != null && StateActivity.Attributes["x:Name"].Value == currentStateName)
            {
                return StateActivity.Attributes["SimplifiedWorkflowStateCode"] == null ? "" : StateActivity.Attributes["SimplifiedWorkflowStateCode"].Value;
            }
        }

        return "";
    }


    protected string RenderWorkflowString(LogicLayerWorkflowPersistentObject o)
    {
        List<OActivityHistory> history = TablesLogic.tActivityHistory.LoadList(
        TablesLogic.tActivityHistory.AttachedObjectID == o.ObjectID,
        TablesLogic.tActivityHistory.CreatedDateTime.Asc);

        OWorkflowRepositoryVersion wf = null;
        wf = OWorkflowRepository.GetLatestWorkflowVersion(o.GetType().BaseType.Name);

        XmlDocument layout = new XmlDocument();
        XmlDocument xoml = new XmlDocument();

        //load .xoml file
        if (wf.WorkflowFile != null)
        {
            byte[] encodedString = Encoding.UTF8.GetBytes(wf.WorkflowFile);
            System.IO.MemoryStream ms = new System.IO.MemoryStream(encodedString);
            ms.Flush();
            ms.Position = 0;

            xoml.Load(ms);
        }
        else
        {
        }

        //Drawing all the actions within the state boxes.
        XmlNodeList StateActivities = xoml.ChildNodes[0].ChildNodes;

        XmlNode rootNode = xoml.ChildNodes[0];

        string[] simplifiedWorkflowNames = (rootNode.Attributes["SimplifiedWorkflowStateNames"] == null ? "" : rootNode.Attributes["SimplifiedWorkflowStateNames"].Value).Split(',');
        string[] simplifiedWorkflowCodes = (rootNode.Attributes["SimplifiedWorkflowStateCodes"] == null ? "" : rootNode.Attributes["SimplifiedWorkflowStateCodes"].Value).Split(',');
        Hashtable workflowNodesHash = new Hashtable();

        for (int i = 0; i < simplifiedWorkflowNames.Length && i < simplifiedWorkflowCodes.Length; i++)
            workflowNodesHash[simplifiedWorkflowCodes[i]] = simplifiedWorkflowNames[i];

        string currentStateCode = FindWorkflowStateCode(o.CurrentActivityName, StateActivities);

        string statusToDisplay = o.CurrentActivityName;

        // Special case for cancelled / rejected / voided statuses
        // (the workflow is terminated mid-way)
        // 
        bool terminatedStatus = false;
        if (currentStateCode == "X")
        {
            terminatedStatus = true;

            o.ActivityHistories.Sort("CreatedDateTime", false);
            for (int i = 0; i < o.ActivityHistories.Count; i++)
            {
                string previousStateName = o.ActivityHistories[i].CurrentStateName;
                currentStateCode = FindWorkflowStateCode(previousStateName, StateActivities);
                if (currentStateCode != "X")
                {
                    statusToDisplay = previousStateName;
                    break;
                }
            }
        }

        string output = "";
        output = "<div class='" + Helpers.GetWorkflowStateClass(statusToDisplay) + " " + (terminatedStatus ? "mdl-workflow-strikethrough" : "") + "'>";
        if (simplifiedWorkflowNames.Length >= 1)
        {
            output += "<ul class='mdl-workflow'>";
            for (int i = 0; i < simplifiedWorkflowNames.Length && i < simplifiedWorkflowCodes.Length; i++)
            {
                if (simplifiedWorkflowCodes[i] != "X" && workflowNodesHash[simplifiedWorkflowCodes[i]] != null)
                {
                    string workflowNodeName = (string)workflowNodesHash[simplifiedWorkflowCodes[i]];
                    string translatedWorkflowNodeName = Resources.WorkflowProgressNodeName.ResourceManager.GetString(workflowNodeName);

                    output += "<li class='" + (currentStateCode == simplifiedWorkflowCodes[i] ? "is-active" : "") + "'>" +
                        "<span class='mdl-workflow-number'>" + simplifiedWorkflowCodes[i] + "</span>" +
                        "<span class='mdl-workflow-name'>" + HttpUtility.HtmlEncode(translatedWorkflowNodeName ?? workflowNodeName) + "</span>" +
                        "</li>";
                }
            }
            output += "</ul>";
        }
        

        output += "<div class='mdl-workflow-status'>" + Helpers.TranslateWorkflowStateItem(statusToDisplay) + "</div>";

        if (terminatedStatus)
            output += "<div class='mdl-workflow-cancelled'>" +  Helpers.TranslateWorkflowStateItem(o.CurrentActivityName) + "</div>";


        output += "</div>";
        return output;
    }


    public void ShowWorkflow(LogicLayerWorkflowPersistentObject o)
    {
        if (o == null)
            return;

        WorkflowLiteral.Text = RenderWorkflowString(o);
    }



    /// <summary>
    /// Hides/shows elements
    /// </summary>
    /// <param name="e"></param>
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        panelNumberOfApprovals.Visible = labelApprovalsRequired.Text != "" && labelApprovalsRequired.Text != "0";
        panelApprovalLevel.Visible = labelCurrentApprovalLevel.Text != "";

        _objectName.CssClass = "field-textbox-large";
        _objectNumber.CssClass = "field-textbox-large";
        //panelStatus.Visible = panelStatus.Visible;

    }


</script>

<div class='mdl-objectbase'>
    <asp:Literal runat="server" ID="lit"></asp:Literal>
    <ui:UIPanel runat="server" ID="panelObjectNameNumber" BorderStyle="NotSet" meta:resourcekey="panelObjectNameNumberResource2">
        <div style="padding-top:36px">
            <ui:UIPanel runat='server' ID="panelStatus" CssClass='div-status' Visible="false">
                <asp:Literal runat="server" ID="WorkflowLiteral"></asp:Literal>

                <ui:UISeparator runat="server" ID="UISeparator1" SkinID="Line" />

            </ui:UIPanel>
        </div>

        <div style="padding-top:12px">
            <span runat='server' id="ObjectNumberPanel">
                <ui:UIFieldTextBox runat="server" ID="_objectNumber" Caption="Number" Span="Full"
                    CaptionPosition='Top' PropertyName="ObjectNumber" InternalControlWidth="150px"
                    CaptionWidth="150px" />
            </span>
            <span runat='server' id="ObjectNamePanel">
                <ui:UIFieldTextBox runat="server" ID="_objectName" Caption="Name" Span="Full" CaptionPosition='Top'
                    ValidateRequiredField="true"
                    PropertyName="ObjectName" InternalControlWidth="100%" MaxLength="255" CaptionWidth="150px" />
            </span>
        </div>
    </ui:UIPanel>

    <ui:UISeparator runat="server" ID="Separator1" SkinID="Line" />


    <ui:UIPanel runat="server" ID="panelWorkflow" Visible="false" meta:resourcekey="panelWorkflowResource1"
        BorderStyle="NotSet">
        <ui:UISeparator runat="server" ID="sep1" Caption="Task" meta:resourcekey="sep1Resource1" />
        <ui:UIPanel ID="panel_HideAssignedUser" runat="server" BorderStyle="NotSet" meta:resourcekey="panel_HideAssignedUserResource2">
        </ui:UIPanel>
        <table cellpadding='1' cellspacing='0' border='0' style='width: 49.5%; float: left'
            height="24px">
            <tr>
                <td style='width: 120px'></td>
                <td>
                    <asp:Panel runat="server" ID="panelApprovalLevel" Style='display: inline' meta:resourcekey="panelApprovalLevelResource1">
                        <asp:Label runat="server" ID="labelApprovalLevel1" Text="(Level " meta:resourcekey="labelApprovalLevel1Resource1"></asp:Label>
                        <ui:UIFieldLabel runat="server" ID="labelCurrentApprovalLevel" FieldLayout="Flow"
                            ShowCaption="False" InternalControlWidth="" DataFormatString="" meta:resourcekey="labelCurrentApprovalLevelResource1">
                        </ui:UIFieldLabel>
                        <asp:Label runat="server" ID="labelApprovalLevel2" Text=")" meta:resourcekey="labelApprovalLevel2Resource1"></asp:Label>
                    </asp:Panel>
                </td>
            </tr>
        </table>
        <asp:Panel runat="server" ID="panelNumberOfApprovals" meta:resourcekey="panelNumberOfApprovalsResource1">
            <table cellpadding='1' cellspacing='0' border='0' style='width: 49.5%; float: left'
                height="24px">
                <tr>
                    <td style='width: 150px'>
                        <asp:Label runat="server" ID="labelApprovalsGiven" Text="Approvals Given at this Level:"
                            meta:resourcekey="labelApprovalsGivenResource1"></asp:Label>
                    </td>
                    <td>
                        <ui:UIFieldLabel runat="server" ID="labelApprovals" ShowCaption="False" FieldLayout="Flow"
                            InternalControlWidth="10px" DataFormatString="" meta:resourcekey="labelApprovalsResource1">
                        </ui:UIFieldLabel>
                        <asp:Label runat="server" ID="labelOutOf" Text="out of" meta:resourcekey="labelOutOfResource1"></asp:Label>
                        <ui:UIFieldLabel runat="server" ID="labelApprovalsRequired" ShowCaption="False" FieldLayout="Flow"
                            InternalControlWidth="10px" DataFormatString="" meta:resourcekey="labelApprovalsRequiredResource1">
                        </ui:UIFieldLabel>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </ui:UIPanel>
</div>
