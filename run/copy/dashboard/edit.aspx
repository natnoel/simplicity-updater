<%@ Page Language="C#" Theme="Corporate" Inherits="PageBase" ValidateRequest="false"
    Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="Anacle.DataFramework" %>
<%@ Import Namespace="LogicLayer" %>
<%@ Import Namespace="System.Data" %>

<script runat="server">

    //---------------------------------------------------------------
    // event
    //---------------------------------------------------------------
    protected void panel_PopulateForm(object sender, EventArgs e)
    {
        ODashboard dashboard = (ODashboard)panel.SessionObject;
        //framePreview.Attributes["src"] = "jschart.aspx?ID=" + HttpUtility.UrlEncode(Security.EncryptGuid(dashboard.ObjectID.Value));

        if (dashboard.DashboardType != null && dashboard.DashboardType.Value == (int)JSChartType.Summarized)
        {
            framePreview.Attributes["src"] = "summarydashboard.aspx?ID=" + HttpUtility.UrlEncode(Security.EncryptGuid(dashboard.ObjectID.Value));
        } else
        {
            framePreview.Attributes["src"] = "plotlychart.aspx?ID=" + HttpUtility.UrlEncode(Security.EncryptGuid(dashboard.ObjectID.Value));
        }

        this.dropDashboardType.Control.Items.Clear();
        foreach (JSChartType t in Enum.GetValues(typeof(JSChartType)).Cast<JSChartType>())
        {
            ListItem i = new ListItem();
            if (t == JSChartType.Line3D)
            {
                i.Text = "3D Line";
            }
            else
            {
                i.Text = t.ToString();
            }

            i.Value = "" + (int)t;
            this.dropDashboardType.Control.Items.Add(i);
        }
        checkTrancateAxisLabel.Checked = dashboard.TrancateAxisLabels.HasValue && dashboard.TrancateAxisLabels != 0;

        dropDownListLinkedReport.Bind(OReport.GetAllReports(), "ReportName", "ObjectID");

        panel.ObjectPanel.BindObjectToControls(dashboard);
    }

    /// <summary>
    /// Validates and saves the dashboard object.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void panel_ValidateAndSave(object sender, EventArgs e)
    {
        using (Connection c = new Connection())
        {
            ODashboard dashboard = (ODashboard)panel.SessionObject;
            panel.ObjectPanel.BindControlsToObject(dashboard);

            // Validate
            //
            if (dashboard.IsDuplicateName())
                objectBase.ObjectName.ErrorMessage = Resources.Errors.General_NameDuplicate;
            if (!panel.ObjectPanel.IsValid)
                return;

            //KL: use the series name as the linking column
            foreach (ODashboardReportLinking drl in dashboard.DashboardReportLinkings)
            {
                drl.ColumnName = dashboard.SeriesColumnName;
            }
            foreach (ODashboardColumnLinking dcl in dashboard.DashboardColumnLinkings)
            {
                dcl.ColumnName = dashboard.SeriesColumnName;
            }
            //KL: end

            if (!checkTrancateAxisLabel.Checked)
            {
                dashboard.TrancateAxisLabels = null;
            }

            if (!DashboardHeight.Enabled)
            {
                dashboard.Height = 1;
            }

            // Save
            //
            dashboard.Save();

            List<OPinboard> pinboards = TablesLogic.tPinboard.LoadList(TablesLogic.tPinboard.PinnedObjectID == dashboard.ObjectID);

            foreach (OPinboard pinboard in pinboards)
            {
                pinboard.SizeRow = dashboard.Width;
                pinboard.SizeColumn = dashboard.Height;
                pinboard.Save();
            }

            tabPreview.Update();

            c.Commit();
        }
    }


    /// <summary>
    /// Occurs when the user checks/unchecks the Populate with Query? checkbox.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void IsPopulatedByQuery_CheckedChanged(object sender, EventArgs e)
    {

    }



    /// <summary>
    /// Occurs when the user selects an item in the Use C# radiobutton list.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void UseCSharpQuery_SelectedIndexChanged(object sender, EventArgs e)
    {
    }


    /// <summary>
    /// Hides/shows and enables/disables elements.
    /// </summary>
    /// <param name="e"></param>
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        // display/dashboard type tab
        //
        int dashboardType = Convert.ToInt32(dropDashboardType.SelectedValue);

        tabPreview.Visible = !panel.SessionObject.IsNew;

        textSeriesColumnName.Visible = textValueColumnName.Visible = radioSeriesByColumns.SelectedValue != "1";
        SeriesAsAllColumnsHint.Visible = radioSeriesByColumns.SelectedValue == "1";
        SeriesInOneColumnHint.Visible = radioSeriesByColumns.SelectedValue == "0";

        // Query tab
        //
        DashboardQuery.Visible = UseCSharpQuery.SelectedValue == "0";
        CSharpMethodName.Visible = UseCSharpQuery.SelectedValue == "1";
        hintReport.Visible = UseCSharpQuery.SelectedValue == "0";

        panelPopulatedByQuery.Visible = IsPopulatedByQuery.SelectedIndex == 1 || IsPopulatedByQuery.SelectedIndex == 2;
        ListQuery.Visible = IsPopulatedByQuery.SelectedIndex == 1;
        panelNotPopulatedByQuery.Visible = IsPopulatedByQuery.SelectedIndex == 0;
        ReportFieldCMethod.Visible = IsPopulatedByQuery.SelectedIndex == 2;
        if (tabObject.SelectedTab == tabQuery && UseCSharpQuery.SelectedIndex == 0)
            Window.WriteJavascript("loadTextInput('" + DashboardQuery.Control.ClientID + "');");


        // Dashboard tab
        //
        textTrancateAxisLabel.Visible = checkTrancateAxisLabel.Checked;

        //set preview iframe size to be propotional to current chart size
        //if (tabPreview.Visible)
        if (!panel.SessionObject.IsNew)
        {
            ODashboard dashboard = (ODashboard)panel.SessionObject;
            framePreview.Style.Add(HtmlTextWriterStyle.Width, (120 * (dashboard.Width ?? 4)) - 20 - 10 + "px");
            framePreview.Style.Add(HtmlTextWriterStyle.Height, (120 * (dashboard.Height ?? 4)) - 20 - 30 - 5 + "px");
        }

        DashboardColumnLinkingFieldsGridView.Visible = DashboardColumnLinksLinkedDashboardID.SelectedIndex > 0;
        DashboardColumnLinksLinkedDashboardID.Enabled = DashboardColumnLinkingFieldsGridView.Rows.Count == 0;

        foreach (GridViewRow row in DashboardColumnLinkingFieldsGridView.Rows)
        {
            UIFieldDropDownList DashboardColumnLinkingFieldsSourceType = row.FindControl("DashboardColumnLinkingFieldsSourceType") as UIFieldDropDownList;
            UIFieldDropDownList DashboardColumnLinkingFieldsSourceNameOrValueDropDownList = row.FindControl("DashboardColumnLinkingFieldsSourceNameOrValueDropDownList") as UIFieldDropDownList;
            UIFieldTextBox DashboardColumnLinkingFieldsSourceNameOrValueTextBox = row.FindControl("DashboardColumnLinkingFieldsSourceNameOrValueTextBox") as UIFieldTextBox;

            if (DashboardColumnLinkingFieldsSourceType.SelectedValue == LogicLayer.DashboardColumnLinkFieldSourceType.FromFilter.ToString())
            {
                DashboardColumnLinkingFieldsSourceNameOrValueDropDownList.PropertyName = "SourceNameOrValue";
                DashboardColumnLinkingFieldsSourceNameOrValueDropDownList.Visible = true;
                DashboardColumnLinkingFieldsSourceNameOrValueTextBox.PropertyName = "";
                DashboardColumnLinkingFieldsSourceNameOrValueTextBox.Visible = false;
            }
            else
            {
                DashboardColumnLinkingFieldsSourceNameOrValueDropDownList.PropertyName = "";
                DashboardColumnLinkingFieldsSourceNameOrValueDropDownList.Visible = false;
                DashboardColumnLinkingFieldsSourceNameOrValueTextBox.PropertyName = "SourceNameOrValue";
                DashboardColumnLinkingFieldsSourceNameOrValueTextBox.Visible = true;
            }
        }

        DashboardReportLinkingFieldsGridView.Visible = DashboardReportLinksLinkedReportID.SelectedIndex > 0;
        DashboardReportLinksLinkedReportID.Enabled = DashboardReportLinkingFieldsGridView.Rows.Count == 0;

        foreach (GridViewRow row in DashboardReportLinkingFieldsGridView.Rows)
        {
            UIFieldDropDownList DashboardReportLinkingFieldsSourceType = row.FindControl("DashboardReportLinkingFieldsSourceType") as UIFieldDropDownList;
            UIFieldDropDownList DashboardReportLinkingFieldsSourceNameOrValueDropDownList = row.FindControl("DashboardReportLinkingFieldsSourceNameOrValueDropDownList") as UIFieldDropDownList;
            UIFieldTextBox DashboardReportLinkingFieldsSourceNameOrValueTextBox = row.FindControl("DashboardReportLinkingFieldsSourceNameOrValueTextBox") as UIFieldTextBox;

            if (DashboardReportLinkingFieldsSourceType.SelectedValue == LogicLayer.DashboardColumnLinkFieldSourceType.FromFilter.ToString())
            {
                DashboardReportLinkingFieldsSourceNameOrValueDropDownList.PropertyName = "SourceNameOrValue";
                DashboardReportLinkingFieldsSourceNameOrValueDropDownList.Visible = true;
                DashboardReportLinkingFieldsSourceNameOrValueTextBox.PropertyName = "";
                DashboardReportLinkingFieldsSourceNameOrValueTextBox.Visible = false;
            }
            else
            {
                DashboardReportLinkingFieldsSourceNameOrValueDropDownList.PropertyName = "";
                DashboardReportLinkingFieldsSourceNameOrValueDropDownList.Visible = false;
                DashboardReportLinkingFieldsSourceNameOrValueTextBox.PropertyName = "SourceNameOrValue";
                DashboardReportLinkingFieldsSourceNameOrValueTextBox.Visible = true;
            }
        }
    }

    /// <summary>
    /// Populates the report field subpanel.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void subpanelDashboardField_PopulateForm(object sender, EventArgs e)
    {
        ODashboard dashboard = panel.SessionObject as ODashboard;
        ODashboardField dashboardField = subpanelDashboardField.SessionObject as ODashboardField;

        DisplayOrder.Items.Clear();
        for (int i = 0; i < dashboard.DashboardFields.Count + 1; i++)
            DisplayOrder.Items.Add(new ListItem((i + 1).ToString(), (i + 1).ToString()));

        CascadeControlID.Bind(dashboard.GetDropDownDashboardFields(dashboardField.ControlIdentifier), "ControlCaption", "ObjectID");

        if (dashboardField.DisplayOrder == null)
            dashboardField.DisplayOrder = DisplayOrder.Items.Count;

        subpanelDashboardField.ObjectPanel.BindObjectToControls(dashboardField);
    }


    /// <summary>
    /// Occurs when the user clicks on the remove button to remove
    /// report fields.
    /// </summary>
    /// <param name="sneder"></param>
    /// <param name="e"></param>
    protected void subpanelDashboardField_Removed(object sneder, EventArgs e)
    {
        ODashboard dashboard = panel.SessionObject as ODashboard;
        tabFilter.BindControlsToObject(dashboard);
        LogicLayer.Global.ReorderItems(dashboard.DashboardFields, null, "DisplayOrder");
        tabFilter.BindObjectToControls(dashboard);
    }


    /// <summary>
    /// Validates and inserts the report field object into the report object.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void subpanelDashboardField_ValidateAndUpdate(object sender, EventArgs e)
    {
        ODashboard dashboard = panel.SessionObject as ODashboard;
        tabFilter.BindControlsToObject(dashboard);

        ODashboardField dashboardField = subpanelDashboardField.SessionObject as ODashboardField;
        subpanelDashboardField.ObjectPanel.BindControlsToObject(dashboardField);

        // Validate
        //
        if (dashboardField.ControlIdentifier.Contains(" "))
            ControlIdentifier.ErrorMessage = Resources.Errors.Report_IdentifierHasSpaces;
        if (dashboardField.ControlIdentifier.Contains(","))
            ControlIdentifier.ErrorMessage = Resources.Errors.Report_IdentifierHasComma;
        if (dashboard.CascadeHasLoops())
        {
            CascadeControlID.ErrorMessage = Resources.Errors.Report_CascadeControlLoop;
            dashboardField.CascadeControl.Clear();
        }
        if (!dashboard.ValidateNoDuplicateIdentifiers(dashboardField))
            ControlIdentifier.ErrorMessage = Resources.Errors.Dashboard_DuplicateIdentifier;
        if (!subpanelDashboardField.ObjectPanel.IsValid)
            return;

        // Insert
        //
        dashboard.DashboardFields.Add(dashboardField);
        tabFilter.BindObjectToControls(dashboard);

        // Update
        LogicLayer.Global.ReorderItems(dashboard.DashboardFields, dashboardField, "DisplayOrder");
    }


    /// <summary>
    /// Occurs when the populated by query checkbox changes.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void IsPopulatedByQuery_SelectedIndexChanged(object sender, EventArgs e)
    {
    }


    /// <summary>
    /// Occurs when the series mode changes.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void radioSeriesByColumns_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Occurs when the user clicks on a command button in the grid.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="commandName"></param>
    /// <param name="dataKeys"></param>
    protected void gridRole_Action(object sender, string commandName, List<object> dataKeys)
    {
        if (commandName == "AddRoles")
        {
            DialogBox_RoleItem.Visible = true;
            DialogBox_RoleItem.Show();
        }
    }

    protected void DialogBox_RoleItem_Selected(object sender, EventArgs e)
    {
        ODashboard dashboard = panel.SessionObject as ODashboard;
        panel.ObjectPanel.BindControlsToObject(dashboard);
        panel.Message = "";

        foreach (Guid key in DialogBox_RoleItem.SelectedDataKeys)
        {
            ORole role = TablesLogic.tRole.Load(key);
            if (role != null)
                dashboard.Roles.Add(role);
        }

        panel.ObjectPanel.BindObjectToControls(dashboard);
        DialogBox_RoleItem.Visible = false;
        DialogBox_RoleItem.Hide();
    }

    protected void DialogBox_RoleItem_Search(object sender, SearchEventArgs e)
    {
        List<ColumnOrder> columns = new List<ColumnOrder>();
        columns.Add(TablesLogic.tRole.RoleName.Asc);
        e.CustomSortOrder = columns;
    }

    protected void gvColumnMapping_Action(object sender, string commandName, List<object> dataKeys)
    {
        ODashboard o = panel.SessionObject as ODashboard;
        panel.ObjectPanel.BindControlsToObject(o);
        if (commandName == "GenerateColumns")
        {
            List<LogicLayer.Parameter> paramList = new List<LogicLayer.Parameter>();

            DataTable dt = Analysis.DoQuery(o.DashboardQuery, paramList.ToArray());

        }
    }


    protected void sp_DashboardColumnLinkings_PopulateForm(object sender, EventArgs e)
    {
        ODashboardColumnLinking RCL = sp_DashboardColumnLinkings.SessionObject as ODashboardColumnLinking;
        DashboardColumnLinksLinkedDashboardID.Bind(ODashboard.GetAllDashboards(RCL.LinkedDashboardID), "ObjectName", "ObjectID");
        sp_DashboardColumnLinkings.ObjectPanel.BindObjectToControls(RCL);
    }

    protected void sp_DashboardColumnLinkings_ValidateAndUpdate(object sender, EventArgs e)
    {
        ODashboard Dashboard = panel.SessionObject as ODashboard;

        ODashboardColumnLinking RCL = (ODashboardColumnLinking)sp_DashboardColumnLinkings.SessionObject;
        sp_DashboardColumnLinkings.ObjectPanel.BindControlsToObject(RCL);

        // Insert
        //
        Dashboard.DashboardColumnLinkings.Add(RCL);
        DashboardColumnLinkingTabView.BindObjectToControls(Dashboard);
    }

    /// <summary>
    /// Occurs when the drill-down report is changed.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DashboardColumnLinksLinkedDashboardID_SelectedIndexChanged(object sender, EventArgs e) { }

    /// <summary>
    /// Occurs when the user adds a new object.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="commandName"></param>
    /// <param name="dataKeys"></param>
    protected void DashboardColumnLinkingFieldsGridView_Action(object sender, string commandName, List<object> dataKeys)
    {
        if (commandName == "AddRow")
        {
            ODashboardColumnLinking RCL = (ODashboardColumnLinking)sp_DashboardColumnLinkings.SessionObject;
            ODashboardColumnLinkingField field = TablesLogic.tDashboardColumnLinkingField.Create();

            sp_DashboardColumnLinkings.ObjectPanel.BindControlsToObject(RCL);
            RCL.DashboardColumnLinkingFields.Add(field);
            sp_DashboardColumnLinkings.ObjectPanel.BindObjectToControls(RCL);
        }
    }


    /// <summary>
    /// Binds the Dashboard fields to the dropdown list.
    /// </summary>
    List<ODashboardField> DashboardFields = null;
    List<ODashboardField> sourceDashboardFields = null;
    protected void DashboardColumnLinkingFieldsGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            if (DashboardFields == null)
            {
                ODashboardColumnLinking RCL = (ODashboardColumnLinking)sp_DashboardColumnLinkings.SessionObject;
                sp_DashboardColumnLinkings.ObjectPanel.BindControlsToObject(RCL);
                DashboardFields = ODashboard.GetAllDashboardFieldsExceptMultiSelect(RCL.LinkedDashboardID);
            }
            if (sourceDashboardFields == null)
            {
                ODashboardColumnLinking RCL = (ODashboardColumnLinking)sp_DashboardColumnLinkings.SessionObject;
                sp_DashboardColumnLinkings.ObjectPanel.BindControlsToObject(RCL);
                sourceDashboardFields = ODashboard.GetAllDashboardFieldsExceptMultiSelect(RCL.DashboardID);
            }

            UIFieldDropDownList TargetDashboardControlIdentifier = e.Row.FindControl("TargetDashboardControlIdentifier") as UIFieldDropDownList;
            TargetDashboardControlIdentifier.Bind(DashboardFields, "ControlIdentifier", "ControlIdentifier");

            UIFieldDropDownList DashboardColumnLinkingFieldsSourceNameOrValueDropDownList = e.Row.FindControl("DashboardColumnLinkingFieldsSourceNameOrValueDropDownList") as UIFieldDropDownList;
            DashboardColumnLinkingFieldsSourceNameOrValueDropDownList.Bind(sourceDashboardFields, "ControlIdentifier", "ControlIdentifier");
        }

    }

    protected void DashboardColumnLinkingFieldsSourceType_SelectedIndexChanged(object sender, EventArgs e)
    {
        UIFieldDropDownList DashboardColumnLinkingFieldsSourceType = sender as UIFieldDropDownList;
        GridViewRow row = DashboardColumnLinkingFieldsSourceType.Parent.Parent as GridViewRow;

        UIFieldDropDownList DashboardColumnLinkingFieldsSourceNameOrValueDropDownList = row.FindControl("DashboardColumnLinkingFieldsSourceNameOrValueDropDownList") as UIFieldDropDownList;
        UIFieldTextBox DashboardColumnLinkingFieldsSourceNameOrValueTextBox = row.FindControl("DashboardColumnLinkingFieldsSourceNameOrValueTextBox") as UIFieldTextBox;

        if (DashboardColumnLinkingFieldsSourceType.SelectedValue == LogicLayer.DashboardColumnLinkFieldSourceType.FromFilter.ToString())
        {
            DashboardColumnLinkingFieldsSourceNameOrValueDropDownList.PropertyName = "SourceNameOrValue";
            DashboardColumnLinkingFieldsSourceNameOrValueDropDownList.Visible = true;
            DashboardColumnLinkingFieldsSourceNameOrValueTextBox.PropertyName = "";
            DashboardColumnLinkingFieldsSourceNameOrValueTextBox.Visible = false;
        }
        else
        {
            DashboardColumnLinkingFieldsSourceNameOrValueDropDownList.PropertyName = "";
            DashboardColumnLinkingFieldsSourceNameOrValueDropDownList.Visible = false;
            DashboardColumnLinkingFieldsSourceNameOrValueTextBox.PropertyName = "SourceNameOrValue";
            DashboardColumnLinkingFieldsSourceNameOrValueTextBox.Visible = true;
        }

    }

    protected void sp_DashboardReportLinkings_PopulateForm(object sender, EventArgs e)
    {
        ODashboardReportLinking RCL = sp_DashboardReportLinkings.SessionObject as ODashboardReportLinking;
        DashboardReportLinksLinkedReportID.Bind(OReport.GetAllReports(RCL.LinkedReportID), "ReportName", "ObjectID");
        dropLinkedReportTemplate.Bind(OReportTemplate.GetReportTemplatesDataTable(RCL.LinkedReportID, RCL.LinkedReportTemplateID), "ObjectName", "ObjectID");
        sp_DashboardReportLinkings.ObjectPanel.BindObjectToControls(RCL);
    }

    protected void sp_DashboardReportLinkings_ValidateAndUpdate(object sender, EventArgs e)
    {
        ODashboard Dashboard = panel.SessionObject as ODashboard;

        ODashboardReportLinking RCL = (ODashboardReportLinking)sp_DashboardReportLinkings.SessionObject;
        sp_DashboardReportLinkings.ObjectPanel.BindControlsToObject(RCL);

        // Insert
        //
        Dashboard.DashboardReportLinkings.Add(RCL);
        DashboardReportLinkingTabView.BindObjectToControls(Dashboard);
    }

    /// <summary>
    /// Occurs when the drill-down report is changed.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DashboardReportLinksLinkedReportID_SelectedIndexChanged(object sender, EventArgs e)
    {
        ODashboardReportLinking rcl = sp_DashboardReportLinkings.SessionObject as ODashboardReportLinking;
        sp_DashboardReportLinkings.ObjectPanel.BindControlsToObject(rcl);
        dropLinkedReportTemplate.Bind(OReportTemplate.GetReportTemplatesDataTable(rcl.LinkedReportID, rcl.LinkedReportTemplateID), "ObjectName", "ObjectID");
        sp_DashboardReportLinkings.ObjectPanel.BindObjectToControls(rcl);
    }

    /// <summary>
    /// Occurs when the user adds a new object.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="commandName"></param>
    /// <param name="dataKeys"></param>
    protected void DashboardReportLinkingFieldsGridView_Action(object sender, string commandName, List<object> dataKeys)
    {
        if (commandName == "AddRow")
        {
            ODashboardReportLinking RCL = (ODashboardReportLinking)sp_DashboardReportLinkings.SessionObject;
            ODashboardReportLinkingField field = TablesLogic.tDashboardReportLinkingField.Create();

            sp_DashboardReportLinkings.ObjectPanel.BindControlsToObject(RCL);
            RCL.DashboardReportLinkingFields.Add(field);
            sp_DashboardReportLinkings.ObjectPanel.BindObjectToControls(RCL);
        }
    }


    /// <summary>
    /// Binds the Dashboard fields to the dropdown list.
    /// </summary>
    List<OReportField> ReportFields = null;
    //List<ODashboardField> sourceDashboardFields = null;
    protected void DashboardReportLinkingFieldsGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            if (ReportFields == null)
            {
                ODashboardReportLinking RCL = (ODashboardReportLinking)sp_DashboardReportLinkings.SessionObject;
                sp_DashboardReportLinkings.ObjectPanel.BindControlsToObject(RCL);
                ReportFields = OReport.GetAllReportFieldsExceptMultiSelect(RCL.LinkedReportID);
            }
            if (sourceDashboardFields == null)
            {
                ODashboardReportLinking RCL = (ODashboardReportLinking)sp_DashboardReportLinkings.SessionObject;
                sp_DashboardReportLinkings.ObjectPanel.BindControlsToObject(RCL);
                sourceDashboardFields = ODashboard.GetAllDashboardFieldsExceptMultiSelect(RCL.DashboardID);
            }

            UIFieldDropDownList TargetReportControlIdentifier = e.Row.FindControl("TargetReportControlIdentifier") as UIFieldDropDownList;
            TargetReportControlIdentifier.Bind(ReportFields, "ControlIdentifier", "ControlIdentifier");

            UIFieldDropDownList DashboardReportLinkingFieldsSourceNameOrValueDropDownList = e.Row.FindControl("DashboardReportLinkingFieldsSourceNameOrValueDropDownList") as UIFieldDropDownList;
            DashboardReportLinkingFieldsSourceNameOrValueDropDownList.Bind(sourceDashboardFields, "ControlIdentifier", "ControlIdentifier");
        }

    }

    protected void DashboardReportLinkingFieldsSourceType_SelectedIndexChanged(object sender, EventArgs e)
    {
        UIFieldDropDownList DashboardReportLinkingFieldsSourceType = sender as UIFieldDropDownList;
        GridViewRow row = DashboardReportLinkingFieldsSourceType.Parent.Parent as GridViewRow;

        UIFieldDropDownList DashboardReportLinkingFieldsSourceNameOrValueDropDownList = row.FindControl("DashboardReportLinkingFieldsSourceNameOrValueDropDownList") as UIFieldDropDownList;
        UIFieldTextBox DashboardReportLinkingFieldsSourceNameOrValueTextBox = row.FindControl("DashboardReportLinkingFieldsSourceNameOrValueTextBox") as UIFieldTextBox;

        //reuse DashboardColumenLinkFieldSourceType
        if (DashboardReportLinkingFieldsSourceType.SelectedValue == LogicLayer.DashboardColumnLinkFieldSourceType.FromFilter.ToString())
        {
            DashboardReportLinkingFieldsSourceNameOrValueDropDownList.PropertyName = "SourceNameOrValue";
            DashboardReportLinkingFieldsSourceNameOrValueDropDownList.Visible = true;
            DashboardReportLinkingFieldsSourceNameOrValueTextBox.PropertyName = "";
            DashboardReportLinkingFieldsSourceNameOrValueTextBox.Visible = false;
        }
        else
        {
            DashboardReportLinkingFieldsSourceNameOrValueDropDownList.PropertyName = "";
            DashboardReportLinkingFieldsSourceNameOrValueDropDownList.Visible = false;
            DashboardReportLinkingFieldsSourceNameOrValueTextBox.PropertyName = "SourceNameOrValue";
            DashboardReportLinkingFieldsSourceNameOrValueTextBox.Visible = true;
        }

    }

    protected void checkTrancateAxisLabel_CheckedChanged(object sender, EventArgs e)
    {

    }

    protected void gridDashboardColors_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //ODashboard o = panel.SessionObject as ODashboard;
            //((UIFieldTextBox)e.Row.FindControl("textOrder")).ControlValue = o.ColorScheme.Rows[e.Row.RowIndex]["Order"];
            //((UIFieldTextBox)e.Row.FindControl("textStroke")).ControlValue = o.ColorScheme.Rows[e.Row.RowIndex]["Stroke"];
            //((UIFieldTextBox)e.Row.FindControl("textHighlight")).ControlValue = o.ColorScheme.Rows[e.Row.RowIndex]["Highlight"];
            //((UIFieldTextBox)e.Row.FindControl("textFill")).ControlValue = o.ColorScheme.Rows[e.Row.RowIndex]["Fill"];
        }
    }

    protected void gridDashboardColors_Action(object sender, string commandName, List<object> dataKeys)
    {
        ODashboard o = panel.SessionObject as ODashboard;
        if (commandName == "Add")
        {
            panel.ObjectPanel.BindControlsToObject(o);

            ODashboardColor color = TablesLogic.tDashboardColor.Create();
            color.Order = o.DashboardColors.Count + 1;
            o.DashboardColors.Add(color);

            panel.ObjectPanel.BindObjectToControls(o);
        }
        else if (commandName == "RemoveObject")
        {
            panel.ObjectPanel.BindControlsToObject(o);
            foreach (object key in dataKeys)
                o.DashboardColors.RemoveGuid((Guid)key);
            panel.ObjectPanel.BindObjectToControls(o);
        }
    }

    protected void tabColor_PreRender(object sender, EventArgs e)
    {
        Window.WriteJavascript("updateAll();");
    }

    protected void dropDashboardType_Load(object sender, EventArgs e)
    {
        toggleSummarizedFieldVisibility();
    }

    protected void dropDashboardType_SelectedIndexChanged(object sender, EventArgs e)
    {
        toggleSummarizedFieldVisibility();
    }

    private void toggleSummarizedFieldVisibility()
    {
        string selectedType = dropDashboardType.SelectedItem.Text;
        if (selectedType == "Summarized")
        {
            panelSeries.Visible = false;
            summarizedFields.Visible = true;
            DashboardHeight.Text = "1";
            DashboardHeight.Enabled = false;
        } else
        {
            panelSeries.Visible = true;
            summarizedFields.Visible = false;
            DashboardHeight.Enabled = true;
        }
    }

    protected void showProgressBar_Load(object sender, EventArgs e)
    {
        toggleProgressBarTitleVisibility();
    }

    protected void showProgressBar_CheckedChanged(object sender, EventArgs e)
    {
        toggleProgressBarTitleVisibility();
    }

    private void toggleProgressBarTitleVisibility()
    {
        if (showProgressBar.Checked)
        {
            progressBarTitle.Visible = true;
        } else
        {
            progressBarTitle.Visible = false;
        }
    }

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Anacle.EAM</title>
    <meta http-equiv="pragma" content="no-cache" />

    <script language=Anacle.EAM</title>
    <meta http-equiv="pragma" content="no-cache" />

    <script language=Anacle.EAM</title>
    <meta http-equiv="pragma" content="no-cache" />

    <script language=Anacle.EAM</title>
    <meta http-equiv="pragma" content="no-cache" />

    <script language=Anacle.EAM</title>
    <meta http-equiv="pragma" content="no-cache" />

    <script language=Anacle.EAM</title>
    <meta http-equiv="pragma" content="no-cache" />

    <script language=Anacle.EAM</title>
    <meta http-equiv="pragma" content="no-cache" />

    <script language=Anacle.EAM</title>
    <meta http-equiv="pragma" content="no-cache" />

    <script language=Anacle.EAM</title>
    <meta http-equiv="pragma" content="no-cache" />

    <script language=Anacle.EAM</title>
    <meta http-equiv="pragma" content="no-cache" />

    <script language=Anacle.EAM</title>
    <meta http-equiv="pragma" content="no-cache" />

    <script language=Anacle.EAM</title>
    <meta http-equiv="pragma" content="no-cache" />

    <script language=Anacle.EAM</title>
    <meta http-equiv="pragma" content="no-cache" />

    <script language=Anacle.EAM</title>
    <meta http-equiv="pragma" content="no-cache" />

    <script language=Anacle.EAM</title>
    <meta http-equiv="pragma" content="no-cache" />

    <script language=Anacle.EAM</title>
    <meta http-equiv="pragma" content="no-cache" />

    <script language=Anacle.EAM</title>
    <meta http-equiv="pragma" content="no-cache" />

    <script language=Anacle.EAM</title>
    <meta http-equiv="pragma" content="no-cache" />

    <script language=Anacle.EAM</title>
    <meta http-equiv="pragma" content="no-cache" />

    <script language=Anacle.EAM</title>
    <meta http-equiv="pragma" content="no-cache" />

    <script language="Javascript" type="text/javascript" src="../../../edit_area/edit_area_full.js"></script>
    <script language="Javascript" type="text/javascript">
        function loadTextInput(var1) {
            editAreaLoader.init({
                id: var1
			, start_highlight: true
			, toolbar: "search, go_to_line, fullscreen, |, undo, redo, |, select_font,|, word_wrap, |, help, save"
			, browsers: "all"
			, language: "en"
			, syntax: "sql"
			, allow_toggle: true
			, min_width: 600
			, min_height: 200
			, save_callback: "my_save"
			, change_callback: "my_change"
			, word_wrap: "true"
            });
        }

        // callback functions
        function my_save(id, content) {
            var text = document.getElementById(id);
            text.innerHtml = text.innerText = text.value = content;
        }

        // callback functions
        function my_change(id) {
            window.frames['frame_' + id].document.getElementById('a_save').click();
        }

        function changeColor(e) {
            var $e = $(e).parent();
            var argb = $e.find('input');
            var color = 'rgba(' + argb[0].value + ',' + argb[1].value + ',' + argb[2].value + ',' + argb[3].value / 255 + ')';
            $e.find(".color")
                .css('background-color', 'transparent')
                .css('background-color', color);
        }

        function updateAll() {
            $('.color-bg').each(function () {
                changeColor(this);
            });
        }
    </script>
    <style>
        .color {
            width: 100%;
            height: 20px;
            border: 1px solid #000;
            margin-top: 8px;
            padding: 1px;
        }

        .color-bg {
            background-color: white;
        }

        .mdl-layout-flow {
            width: 52px;
        }
    </style>
</head>
<body class="body-bg">
    <form id="form1" runat="server">
        <ui:UIObjectPanel runat="server" ID="panelMain" BorderStyle="NotSet"
            meta:resourcekey="panelMainResource">
            <web:object runat="server" ID="panel" Caption="Dashboard" BaseTable="tDashboard"
                OnPopulateForm="panel_PopulateForm" OnValidateAndSave="panel_ValidateAndSave"
                meta:resourcekey="panelResource"></web:object>
            <div class="mdl-edit-div">
                <ui:UITabStrip runat="server" ID="tabObject" SkinID="tabstripEdit" WizardStyle="true"
                    meta:resourcekey="tabObjectResource" BorderStyle="NotSet">
                    <web:base ID="objectBase" runat="server" ObjectNumberVisible="false" ObjectNameCaption="Dashboard Name"
                        ObjectNumberValidateRequiredField="true" meta:resourcekey="objectBaseResource"></web:base>
                    <ui:UITabView ID="tabFilter" runat="server" Caption="Filter Fields" SkinID="tabviewEdit"
                        BorderStyle="NotSet" meta:resourcekey="tabFilterResource">
                        <ui:UISeparator runat='server' ID="UISeparator7" Caption="Filter Fields" meta:resourcekey="UISeparator7Resource" />
                        <ui:UIGridView ID="gridDashboardFields" runat="server" ShowCaption="false" Caption="Filter Fields"
                            PropertyName="DashboardFields"
                            SortExpression="DisplayOrder" KeyName="ObjectID" Width="100%"
                            PagingEnabled="True" DataKeyNames="ObjectID" GridLines="Both"
                            ImageRowErrorUrl="" meta:resourcekey="gridDashboardFieldsResource"
                            RowErrorColor="" Style="clear: both;">
                            <PagerSettings Mode="NumericFirstLast" />
                            <Commands>
                                <ui:UIGridViewCommand AlwaysEnabled="False" CausesValidation="False"
                                    CommandName="RemoveObject" CommandText="Delete"
                                    ConfirmText="Are you sure you wish to remove the selected items?"
                                    ImageUrl="~/images/delete.gif" meta:resourcekey="gridDashboardFieldsUIGridViewCommandDeleteResource" />
                                <ui:UIGridViewCommand AlwaysEnabled="False" CausesValidation="False"
                                    CommandName="AddObject" CommandText="Add" ImageUrl="~/images/add.gif"
                                    meta:resourcekey="gridDashboardFieldsUIGridViewCommandAddResource" />
                            </Commands>
                            <Columns>
                                <ui:UIGridViewButtonColumn ButtonType="Image" CommandName="EditObject"
                                    ImageUrl="~/images/edit.gif" meta:resourcekey="gridDashboardFieldsUIGridViewButtonColumnEditObjectResource">
                                    <HeaderStyle HorizontalAlign="Left" Width="16px" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </ui:UIGridViewButtonColumn>
                                <ui:UIGridViewButtonColumn ButtonType="Image" CommandName="RemoveObject"
                                    ConfirmText="Are you sure you wish to remove this item?"
                                    ImageUrl="~/images/delete.gif"
                                    meta:resourcekey="gridDashboardFieldsUIGridViewButtonColumnRemoveObjectResource">
                                    <HeaderStyle HorizontalAlign="Left" Width="16px" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </ui:UIGridViewButtonColumn>
                                <ui:UIGridViewBoundColumn DataField="DisplayOrder" HeaderText="Display Order"
                                    meta:resourcekey="gridDashboardFieldsUIGridViewBoundColumnDisplayOrderResource"
                                    PropertyName="DisplayOrder"
                                    ResourceAssemblyName="" SortExpression="DisplayOrder">
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </ui:UIGridViewBoundColumn>
                                <ui:UIGridViewBoundColumn DataField="ControlCaption"
                                    HeaderText="Control Caption" meta:resourcekey="gridDashboardFieldsUIGridViewBoundColumnControlCaptionResource"
                                    PropertyName="ControlCaption" ResourceAssemblyName=""
                                    SortExpression="ControlCaption">
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </ui:UIGridViewBoundColumn>
                                <ui:UIGridViewBoundColumn DataField="ControlIdentifier"
                                    HeaderText="Control Identifier"
                                    meta:resourcekey="gridDashboardFieldsUIGridViewBoundColumnControlIdentifierResource"
                                    PropertyName="ControlIdentifier" ResourceAssemblyName=""
                                    SortExpression="ControlIdentifier">
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </ui:UIGridViewBoundColumn>
                            </Columns>
                        </ui:UIGridView>
                        <ui:UIDialogBox runat="server" ID="dialogBoxDashboardField">
                        <ui:UIObjectPanel ID="panelDashboardField" runat="server" BorderStyle="NotSet"
                            meta:resourcekey="panelDashboardFieldResource">
                            <web:subpanel runat="server" ID="subpanelDashboardField" GridViewID="gridDashboardFields"
                                OnPopulateForm="subpanelDashboardField_PopulateForm" OnRemoved="subpanelDashboardField_Removed"
                                OnValidateAndUpdate="subpanelDashboardField_ValidateAndUpdate" meta:resourcekey="subpanelDashboardFieldResource"></web:subpanel>
                            <ui:UIFieldDropDownList ID="DisplayOrder" runat="server" Caption="Display Order"
                                PropertyName="DisplayOrder" Span="Half" ValidateRequiredField="True"
                                ToolTip="The order of display of the search field. Lower appears first."
                                meta:resourcekey="DisplayOrderResource">
                            </ui:UIFieldDropDownList>
                            <ui:UIFieldTextBox ID="ControlIdentifier" runat="server" Caption="Identifier" PropertyName="ControlIdentifier"
                                ValidateRequiredField="True"
                                ToolTip="The identifier that can be used in the SQL query to perform a filter."
                                InternalControlWidth="95%" meta:resourcekey="ControlIdentifierResource" />
                            <ui:UIFieldTextBox runat="server" ID="textControlCaption" Caption="Control Caption"
                                PropertyName="ControlCaption" InternalControlWidth="95%"
                                meta:resourcekey="textControlCaptionResource" />
                            <ui:UIFieldDropDownList ID="DataType" runat="server" Caption="Data Type" PropertyName="DataType"
                                Span="Half" ValidateRequiredField="True"
                                ToolTip="The data type of the search field."
                                meta:resourcekey="DataTypeResource">
                                <Items>
                                    <asp:ListItem Selected="True" Value="0" Text="String"
                                        meta:resourcekey="DataTypeListItem0Resource"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Integer" meta:resourcekey="DataTypeListItem1Resource"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Decimal" meta:resourcekey="DataTypeListItem2Resource"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="Double" meta:resourcekey="DataTypeListItem3Resource"></asp:ListItem>
                                    <asp:ListItem Value="4" Text="DateTime" meta:resourcekey="DataTypeListItem4Resource"></asp:ListItem>
                                    <asp:ListItem Value="5" Text="Object Identifier"
                                        meta:resourcekey="DataTypeListItem5Resource"></asp:ListItem>
                                </Items>
                            </ui:UIFieldDropDownList>
                            <ui:UIFieldCheckBox runat="server" ID="checkIsInsertBlank" Caption="Insert Blank?"
                                Text="Yes, insert blank row to the dropdown list" PropertyName="IsInsertBlank"
                                Span="Full" meta:resourcekey="checkIsInsertBlankResource">
                            </ui:UIFieldCheckBox>
                            <ui:UIFieldListBox runat="server" ID="CascadeControlID" PropertyName="CascadeControl"
                                Caption="Cascading Control" Span="Half"
                                ToolTip="Indicates the dropdown list control that is updated if this dropdown list changes."
                                meta:resourcekey="CascadeControlIDResource"></ui:UIFieldListBox>
                            <br />
                            <ui:UIPanel ID="panelListQuery" runat="server" BorderStyle="NotSet"
                                meta:resourcekey="panelListQueryResource">
                                <ui:UISeparator runat="server" ID="UISeparator4" Caption="List Query"
                                    meta:resourcekey="UISeparator4Resource" />
                                <ui:UIFieldRadioList ID="IsPopulatedByQuery" runat="server" Caption="Populate with Query?"
                                    PropertyName="IsPopulatedByQuery"
                                    ToolTip="Indicates if the dropdown list should be populated with a query, or populated with a list of comma-separated values."
                                    OnSelectedIndexChanged="IsPopulatedByQuery_SelectedIndexChanged"
                                    meta:resourcekey="IsPopulatedByQueryResource" TextAlign="Right">
                                    <Items>
                                        <asp:ListItem Value="0" meta:resourcekey="IsPopulatedByQueryListItem0Resource">Populate with a List of Comma-separated values</asp:ListItem>
                                        <asp:ListItem Value="1" meta:resourcekey="IsPopulatedByQueryListItem1Resource">Populate with a query</asp:ListItem>
                                        <asp:ListItem Value="2" meta:resourcekey="IsPopulatedByQueryListItem2Resource">Populate with a C# Method</asp:ListItem>
                                    </Items>
                                </ui:UIFieldRadioList>
                                <ui:UIFieldTextBox runat="server" ID="ReportFieldCMethod" ValidateRequiredField="True"
                                    Caption="C# Method" PropertyName="CSharpMethodName"
                                    InternalControlWidth="95%" meta:resourcekey="ReportFieldCMethodResource" />
                                <ui:UIPanel ID="panelNotPopulatedByQuery" runat="server" BorderStyle="NotSet"
                                    meta:resourcekey="panelNotPopulatedByQueryResource">
                                    <ui:UIFieldTextBox ID="TextList" runat="server" Caption="Text List" PropertyName="TextList"
                                        ValidateRequiredField="True" ToolTip="A comma separated list of text that will appear to the user for selection"
                                        MaxLength="0" InternalControlWidth="95%"
                                        meta:resourcekey="TextListResource" />
                                    <ui:UIFieldTextBox ID="ValueList" PropertyName="ValueList" runat="server" Caption="Value List"
                                        ToolTip="The comma separated list of values that corresponds to the text list."
                                        MaxLength="0" ValidateRequiredField="True" InternalControlWidth="95%"
                                        meta:resourcekey="ValueListResource" />
                                </ui:UIPanel>
                                <ui:UIPanel ID="panelPopulatedByQuery" runat="server" BorderStyle="NotSet"
                                    meta:resourcekey="panelPopulatedByQueryResource">
                                    <ui:UIFieldTextBox ID="ListQuery" runat="server" Caption="List SQL" PropertyName="ListQuery"
                                        Rows="15" TextMode="MultiLine" ValidateRequiredField="True" MaxLength="0"
                                        ToolTip="The query in SQL that is used to populate the dropdown list. The default {TreeviewID}, {TreeviewObject}, {UserID} are default fields available for the SQL query."
                                        InternalControlWidth="95%" meta:resourcekey="ListQueryResource" />
                                    <ui:UIFieldTextBox ID="DataValueField" runat="server" Caption="Data Value Field"
                                        PropertyName="DataValueField" ValidateRequiredField="True" Span="Half"
                                        ToolTip="The column of the result of the query that will be displayed to the user."
                                        InternalControlWidth="95%" meta:resourcekey="DataValueFieldResource" />
                                    <ui:UIFieldTextBox ID="DataTextField" runat="server" Caption="Data Text Field" PropertyName="DataTextField"
                                        ValidateRequiredField="True" Span="Half"
                                        ToolTip="The column of the result of the query that will be used as a value in the dashboard query."
                                        InternalControlWidth="95%" meta:resourcekey="DataTextFieldResource" />
                                </ui:UIPanel>
                                &nbsp; &nbsp;
                            </ui:UIPanel>
                        </ui:UIObjectPanel>
                        </ui:UIDialogBox>
                    </ui:UITabView>
                    <ui:UITabView runat="server" ID="tabQuery" Caption="Query" SkinID="tabviewEdit"
                        BorderStyle="NotSet" meta:resourcekey="tabQueryResource">
                        <ui:UISeparator runat='server' ID="UISeparator6" Caption="Query" meta:resourcekey="UISeparator6Resource" />
                        <ui:UIPanel runat="server" ID="panelCSharp" BorderStyle="NotSet"
                            meta:resourcekey="panelCSharpResource">
                            <ui:UIFieldRadioList runat="server" ID="UseCSharpQuery" PropertyName="UseCSharpQuery"
                                Caption="Use C#" RepeatColumns="0"
                                OnSelectedIndexChanged="UseCSharpQuery_SelectedIndexChanged"
                                meta:resourcekey="UseCSharpQueryResource" TextAlign="Right">
                                <Items>
                                    <asp:ListItem Value="0" meta:resourcekey="UseCSharpQueryListItem0Resource">No</asp:ListItem>
                                    <asp:ListItem Value="1" meta:resourcekey="UseCSharpQueryListItem1Resource">Yes</asp:ListItem>
                                </Items>
                            </ui:UIFieldRadioList>
                            <ui:UIFieldTextBox runat="server" ID="CSharpMethodName" PropertyName="CSharpMethodName"
                                ValidateRequiredField="True" ToolTip="Enter the C# method name in the LogicLayer.Reports class that is to be called to generate the report."
                                Caption="C# Method Name" InternalControlWidth="95%"
                                meta:resourcekey="CSharpMethodNameResource">
                            </ui:UIFieldTextBox>
                        </ui:UIPanel>
                        <ui:UIFieldTextBox ID="DashboardQuery" runat="server" Caption="Dashboard SQL" PropertyName="DashboardQuery"
                            MaxLength="0"
                            Rows="35" TextMode="MultiLine" ValidateRequiredField="True" ToolTip="The query in SQL that is used to populate the dashboard. Note: You can preview your dashboard in the 'Preview' tab."
                            meta:resourcekey="DashboardQueryResource" InternalControlWidth="95%" />
                        <ui:UIHint runat="server" ID="hintReport"
                            Text="&lt;b&gt;Eg. &lt;/b&gt;
                            &lt;br/&gt;&nbsp;&nbsp;&nbsp;&nbsp;SELECT u.LanguageName, COUNT(*) FROM [User] u WHERE u.ObjectName LIKE '%' + {Filter_UserName} + '%' GROUP BY u.LanguageName
                            &lt;br/&gt;
                            &lt;br/&gt;
                            &lt;b&gt;Specifying filter conditions in your SQL query: &lt;/b&gt;
                            &lt;br/&gt;&nbsp;&nbsp;&nbsp;&nbsp;for string fields: 
                            &lt;br/&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(column like '%' + {filter} + '%')
                            &lt;br/&gt;&nbsp;&nbsp;&nbsp;&nbsp;for int, objectidentifier, decimal, double, date/time fields: 
                            &lt;br/&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;({filter} is NULL or column like '%' + {filter} + '%')
                            &lt;br/&gt;&nbsp;&nbsp;&nbsp;&nbsp;for listboxes: 
                            &lt;br/&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;({filter:COUNT} = 0 or column in {filter})
                            &lt;br/&gt;
                            &lt;br/&gt;
                            &lt;b&gt;You can also use the following fields in your query: &lt;/b&gt;
                            &lt;br/&gt;&nbsp;&nbsp;&nbsp;&nbsp;{UserID} - currently logged on user ID
                            &lt;br/&gt;&nbsp;&nbsp;&nbsp;&nbsp;{DashboardID} - current dashboard ID
                            &lt;br/&gt;
                            &lt;br/&gt;
                            Note: You can preview your dashboard in the 'Preview' tab."
                            meta:resourcekey="hintReportResource" />
                    </ui:UITabView>
                    <ui:UITabView runat="server" ID="tabDashboard" Caption="Dashboard Type" SkinID="tabviewEdit"
                        BorderStyle="NotSet" meta:resourcekey="tabDashboardResource">
                        <ui:UISeparator runat="server" ID="UISeparator11" Caption="Dashboard Size"
                            meta:resourcekey="UISeparator11Resource" />
                        <ui:UIFieldTextBox ID="DashboardWidth" runat="server" Caption="DashboardWidth"
                            PropertyName="Width" Span="Full" InternalControlWidth="200px" ValidateRequiredField="True"
                            Hint="The width of the dashboard must be a numeric value, minimum 2 and maximum 8."
                            meta:resourcekey="DashboardWidthResource" ValidationRangeMax="8" ValidationRangeMin="2"
                            ValidationRangeType="Integer" ValidateRangeField="true" />
                        <ui:UIFieldTextBox ID="DashboardHeight" runat="server" Caption="DashboardHeight"
                            PropertyName="Height" Span="Full" InternalControlWidth="200px" ValidateRequiredField="True"
                            Hint="The height of the dashboard must be a numeric value, minimum 2 and maximum 8."
                            meta:resourcekey="DashboardHeightResource" ValidationRangeMax="8" ValidationRangeMin="2"
                            ValidationRangeType="Integer" ValidateRangeField="true" />
                        <ui:UISeparator runat='server' ID="UISeparator10" Caption="Dashboard Type" meta:resourcekey="UISeparator10Resource" />
                        <ui:UIFieldDropDownList ID="dropDashboardType" runat="server" Caption="Dashboard Type" 
                            OnSelectedIndexChanged="dropDashboardType_SelectedIndexChanged" OnLoad="dropDashboardType_Load"
                            PropertyName="DashboardType" ValidateRequiredField="True" ToolTip="The type of dashboard."
                            Span="Half"
                            meta:resourcekey="dropDashboardTypeResource">
                        </ui:UIFieldDropDownList>
                        <ui:UIPanel runat="server" ID="summarizedFields">
                            <ui:UIFieldTextBox runat="server" Caption="Summary Title" ID="summaryTitle" PropertyName="SummaryTitle"></ui:UIFieldTextBox>
                            <ui:UIFieldCheckBox runat="server" Caption="Show Summary Progress Bar" ID="showProgressBar" PropertyName="ShowSummaryProgressBar" 
                                OnCheckedChanged="showProgressBar_CheckedChanged" OnLoad="showProgressBar_Load">
                            </ui:UIFieldCheckBox>
                            <ui:UIFieldTextBox runat="server" Caption="Summary Progress Bar Title" ID="progressBarTitle" PropertyName="SummaryProgressBarTitle"></ui:UIFieldTextBox>
                            <ui:UIFieldDropDownList ID="dropDownListLinkedReport" runat="server" Caption="Summary Linked Report" 
                                PropertyName="SummaryLinkedReportID" ToolTip="Report to link to">
                            </ui:UIFieldDropDownList>
                        
                            <table cellpadding='0' cellspacing='0' border='0' style='width: 100%'>
                                <tr>
                                    <td style='width: 160px'></td>
                                    <td>
                                        <ui:UIHint runat="server" ID="UIHint1" Text="
                                        Your query should return data in the following format:
                                        &lt;br/&gt;
                                        &lt;br/&gt;
                                        &lt;table style='border-width:1px'&gt;
                                            &lt;tr&gt;
                                                &lt;td&gt;&lt;b&gt;Number&lt;/b&gt;&lt;/td&gt;
                                                &lt;td&gt;&lt;b&gt;Percentage&lt;/b&gt;&lt;/td&gt;
                                                &lt;td&gt;...&lt;/td&gt;
                                            &lt;/tr&gt;
                                            &lt;tr&gt;
                                                &lt;td&gt;1234&lt;/td&gt;
                                                &lt;td&gt;80&lt;/td&gt;
                                                &lt;td&gt;...&lt;/td&gt;
                                            &lt;/tr&gt;
                                        &lt;/table&gt;
                                        ">
                                        </ui:UIHint>
                                    </td>
                                </tr>
                            </table>
                        
                        </ui:UIPanel>
                        <ui:UIPanel runat="server" ID="panelSeries" BorderStyle="NotSet"
                            meta:resourcekey="panelSeriesResource">
                            <ui:UISeparator runat="server" ID="UISeparator2" Caption="Label"
                                meta:resourcekey="UISeparator2Resource" />
                            <ui:UIFieldCheckBox runat="server" Caption="Trancate Axis Label" ID="checkTrancateAxisLabel"
                                OnCheckedChanged="checkTrancateAxisLabel_CheckedChanged" meta:resourcekey="checkTrancateAxisLabelResource">
                            </ui:UIFieldCheckBox>
                            <ui:UIFieldTextBox runat="server" Caption="Max. Label Length" ID="textTrancateAxisLabel"
                                PropertyName="TrancateAxisLabels"
                                ValidateRequiredField="true" ValidateDataTypeCheck="true" ValidationDataType="Integer"
                                ValidateRangeField="true" ValidationRangeMin="1" ValidationRangeType="Integer"
                                ValidationRangeMinInclusive="true" meta:resourcekey="textTrancateAxisLabelResource">
                            </ui:UIFieldTextBox>
                            <ui:UIFieldCheckBox runat="server" Caption="Show Legend" ID="checkShowLegend" PropertyName="ShowLegend"
                                meta:resourcekey="checkShowLegendResource">
                            </ui:UIFieldCheckBox>
                            <ui:UISeparator runat="server" ID="UISeparator1" Caption="Data Structure"
                                meta:resourcekey="UISeparator1Resource" />
                            <ui:UIFieldRadioList runat="server" ID="radioSeriesByColumns" Caption="Mode"
                                OnSelectedIndexChanged="radioSeriesByColumns_SelectedIndexChanged"
                                ValidateRequiredField="True" PropertyName="SeriesByColumns"
                                meta:resourcekey="radioSeriesByColumnsResource" TextAlign="Right">
                                <Items>
                                    <asp:ListItem Value="0"
                                        Text="Names of series and data is defined specific column"
                                        Selected="True"
                                        meta:resourcekey="radioSeriesByColumnsListItem0Resource">
                                    </asp:ListItem>
                                    <asp:ListItem Value="1"
                                        Text="Names of series and data is defined in columns"
                                        meta:resourcekey="radioSeriesByColumnsListItem1Resource">
                                    </asp:ListItem>
                                </Items>
                            </ui:UIFieldRadioList>
                            <ui:UIFieldTextBox ID="textCatagoryColumnName" runat="server" Caption="Catagory Column Name / X-Axis"
                                PropertyName="CatagoryColumnName" Span="Full" ValidateRequiredField="True"
                                meta:resourcekey="textCatagoryColumnNameResource" />
                            <ui:UIFieldTextBox ID="textSeriesColumnName" runat="server" Caption="Series Column Name / Y-Axis"
                                PropertyName="SeriesColumnName" Span="Full" ValidateRequiredField="True"
                                meta:resourcekey="textSeriesColumnNameResource" />
                            <ui:UIFieldTextBox ID="textValueColumnName" runat="server" Caption="Value Column Name / Z-Axis"
                                PropertyName="ValueColumnName" Span="Full" ValidateRequiredField="True"
                                meta:resourcekey="textValueColumnNameResource" />
                            <table cellpadding='0' cellspacing='0' border='0' style='width: 100%'>
                                <tr>
                                    <td style='width: 160px'></td>
                                    <td>
                                        <ui:UIHint runat="server" ID="SeriesAsAllColumnsHint" Text="
                                        Your query should return data in the following format:
                                        &lt;br/&gt;
                                        &lt;br/&gt;
                                        &lt;table style='border-width:1px'&gt;
                                            &lt;tr&gt;
                                                &lt;td&gt;&lt;b&gt;Category&lt;/b&gt;&lt;/td&gt;
                                                &lt;td&gt;&lt;b&gt;Series 1&lt;/b&gt;&lt;/td&gt;
                                                &lt;td&gt;&lt;b&gt;Series 2&lt;/b&gt;&lt;/td&gt;
                                                &lt;td&gt;&lt;b&gt;Series 3&lt;/b&gt;&lt;/td&gt;
                                                &lt;td&gt;...&lt;/td&gt;
                                            &lt;/tr&gt;
                                            &lt;tr&gt;
                                                &lt;td&gt;First&lt;/td&gt;
                                                &lt;td&gt;10&lt;/td&gt;
                                                &lt;td&gt;20&lt;/td&gt;
                                                &lt;td&gt;30&lt;/td&gt;
                                                &lt;td&gt;...&lt;/td&gt;
                                            &lt;/tr&gt;
                                            &lt;tr&gt;
                                                &lt;td&gt;Second&lt;/td&gt;
                                                &lt;td&gt;20&lt;/td&gt;
                                                &lt;td&gt;30&lt;/td&gt;
                                                &lt;td&gt;40&lt;/td&gt;
                                                &lt;td&gt;...&lt;/td&gt;
                                            &lt;/tr&gt;
                                            &lt;tr&gt;
                                                &lt;td&gt;Third&lt;/td&gt;
                                                &lt;td&gt;30&lt;/td&gt;
                                                &lt;td&gt;40&lt;/td&gt;
                                                &lt;td&gt;50&lt;/td&gt;
                                                &lt;td&gt;...&lt;/td&gt;
                                            &lt;/tr&gt;
                                            &lt;tr&gt;
                                                &lt;td&gt;...&lt;/td&gt;
                                                &lt;td&gt;...&lt;/td&gt;
                                                &lt;td&gt;...&lt;/td&gt;
                                                &lt;td&gt;...&lt;/td&gt;
                                                &lt;td&gt;...&lt;/td&gt;
                                            &lt;/tr&gt;
                                        &lt;/table&gt;
                                        "
                                            meta:resourcekey="SeriesAsAllColumnsHintResource">
                                        </ui:UIHint>
                                        <ui:UIHint runat="server" ID="SeriesInOneColumnHint" Text="
                                        Your query should return data in the following format:
                                        &lt;br/&gt;
                                        &lt;br/&gt;
                                        &lt;table style='border-width:1px'&gt;
                                            &lt;tr&gt;
                                                &lt;td&gt;&lt;b&gt;Category&lt;/b&gt;&lt;/td&gt;
                                                &lt;td&gt;&lt;b&gt;Series&lt;/b&gt;&lt;/td&gt;
                                                &lt;td&gt;&lt;b&gt;Value&lt;/b&gt;&lt;/td&gt;
                                            &lt;/tr&gt;
                                            &lt;tr&gt;
                                                &lt;td&gt;First&lt;/td&gt;
                                                &lt;td&gt;Series 1&lt;/td&gt;
                                                &lt;td&gt;10&lt;/td&gt;
                                            &lt;/tr&gt;
                                            &lt;tr&gt;
                                                &lt;td&gt;First&lt;/td&gt;
                                                &lt;td&gt;Series 2&lt;/td&gt;
                                                &lt;td&gt;20&lt;/td&gt;
                                            &lt;/tr&gt;
                                            &lt;tr&gt;
                                                &lt;td&gt;First&lt;/td&gt;
                                                &lt;td&gt;Series 3&lt;/td&gt;
                                                &lt;td&gt;30&lt;/td&gt;
                                            &lt;/tr&gt;
                                            &lt;tr&gt;
                                                &lt;td&gt;Second&lt;/td&gt;
                                                &lt;td&gt;Series 1&lt;/td&gt;
                                                &lt;td&gt;20&lt;/td&gt;
                                            &lt;/tr&gt;
                                            &lt;tr&gt;
                                                &lt;td&gt;Second&lt;/td&gt;
                                                &lt;td&gt;Series 2&lt;/td&gt;
                                                &lt;td&gt;30&lt;/td&gt;
                                            &lt;/tr&gt;
                                            &lt;tr&gt;
                                                &lt;td&gt;Second&lt;/td&gt;
                                                &lt;td&gt;Series 3&lt;/td&gt;
                                                &lt;td&gt;40&lt;/td&gt;
                                            &lt;/tr&gt;
                                            &lt;tr&gt;
                                                &lt;td&gt;Third&lt;/td&gt;
                                                &lt;td&gt;Series 1&lt;/td&gt;
                                                &lt;td&gt;30&lt;/td&gt;
                                            &lt;/tr&gt;
                                            &lt;tr&gt;
                                                &lt;td&gt;Third&lt;/td&gt;
                                                &lt;td&gt;Series 2&lt;/td&gt;
                                                &lt;td&gt;40&lt;/td&gt;
                                            &lt;/tr&gt;
                                            &lt;tr&gt;
                                                &lt;td&gt;Third&lt;/td&gt;
                                                &lt;td&gt;Series 3&lt;/td&gt;
                                                &lt;td&gt;50&lt;/td&gt;
                                            &lt;/tr&gt;
                                            &lt;tr&gt;
                                                &lt;td&gt;...&lt;/td&gt;
                                                &lt;td&gt;...&lt;/td&gt;
                                                &lt;td&gt;...&lt;/td&gt;
                                            &lt;/tr&gt;
                                        &lt;/table&gt;
                                        "
                                            meta:resourcekey="SeriesInOneColumnHintResource">
                                        </ui:UIHint>
                                    </td>
                                </tr>
                            </table>
                        </ui:UIPanel>
                    </ui:UITabView>
                    <ui:UITabView runat="server" ID="tabColor" Caption="Color Scheme" SkinID="tabviewEdit"
                        BorderStyle="NotSet" OnPreRender="tabColor_PreRender" meta:resourcekey="tabColorResource">

                        <ui:UIGridView runat="server" Span="Half" Caption="Color Scheme for this Dashboard"
                            BindObjectsToRows="True" AllowPaging="false" OnAction="gridDashboardColors_Action"
                            PropertyName="DashboardColors" ID="gridDashboardColors" GridLines="Both"
                            SortExpression="Order"
                            OnRowDataBound="gridDashboardColors_RowDataBound">
                            <PagerSettings Mode="NumericFirstLast" />
                            <Commands>
                                <ui:UIGridViewCommand AlwaysEnabled="False" CausesValidation="false" CommandName="Add"
                                    CommandText="Add" ImageUrl="~/images/add.gif" />
                                <ui:UIGridViewCommand CausesValidation="False"
                                    CommandName="RemoveObject" CommandText="Remove"
                                    ConfirmText="Are you sure you wish to remove this item?"
                                    ImageUrl="~/images/delete.gif" />
                            </Commands>
                            <Columns>
                                <ui:UIGridViewButtonColumn ButtonType="Image" CommandName="RemoveObject"
                                    ConfirmText="Are you sure you wish to remove this item?"
                                    ImageUrl="~/images/delete.gif">
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </ui:UIGridViewButtonColumn>
                                <ui:UIGridViewTemplateColumn HeaderText="Order" SortExpression="Order">
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <ui:UIFieldTextBox runat="server" ID="textOrder" ShowCaption="false" PropertyName="Order"
                                            FieldLayout="Flow" ValidateRequiredField="true" Enabled="false" InternalControlWidth="100%" />
                                    </ItemTemplate>
                                </ui:UIGridViewTemplateColumn>
                                <%-- Removed Stroke and Highlight for plotly
                                <ui:UIGridViewTemplateColumn HeaderText="Stroke" SortExpression="Stroke">
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <ui:UIFieldTextBox runat="server" ID="textStrokeR" Caption="R" PropertyName="StrokeR"
                                            InternalControlWidth="100%"
                                            FieldLayout="Flow" ValidateRequiredField="true" onkeyup="changeColor(this);"
                                            ValidationRangeType="Integer"
                                            ValidateRangeField="true" ValidationRangeMinInclusive="true" ValidationRangeMaxInclusive="true"
                                            ValidationRangeMin="0" ValidationRangeMax="255" ValidateDataTypeCheck="true"
                                            ValidationDataType="Integer" />
                                        <br />
                                        <ui:UIFieldTextBox runat="server" ID="textStrokeG" Caption="G" PropertyName="StrokeG"
                                            InternalControlWidth="100%"
                                            FieldLayout="Flow" ValidateRequiredField="true" onkeyup="changeColor(this);"
                                            ValidationRangeType="Integer"
                                            ValidateRangeField="true" ValidationRangeMinInclusive="true" ValidationRangeMaxInclusive="true"
                                            ValidationRangeMin="0" ValidationRangeMax="255" ValidateDataTypeCheck="true"
                                            ValidationDataType="Integer" />
                                        <br />
                                        <ui:UIFieldTextBox runat="server" ID="textStrokeB" Caption="B" PropertyName="StrokeB"
                                            InternalControlWidth="100%"
                                            FieldLayout="Flow" ValidateRequiredField="true" onkeyup="changeColor(this);"
                                            ValidationRangeType="Integer"
                                            ValidateRangeField="true" ValidationRangeMinInclusive="true" ValidationRangeMaxInclusive="true"
                                            ValidationRangeMin="0" ValidationRangeMax="255" ValidateDataTypeCheck="true"
                                            ValidationDataType="Integer" />
                                        <br />
                                        <ui:UIFieldTextBox runat="server" ID="textStrokeA" Caption="A" PropertyName="StrokeA"
                                            InternalControlWidth="100%"
                                            FieldLayout="Flow" ValidateRequiredField="true" onkeyup="changeColor(this);"
                                            ValidationRangeType="Integer"
                                            ValidateRangeField="true" ValidationRangeMinInclusive="true" ValidationRangeMaxInclusive="true"
                                            ValidationRangeMin="0" ValidationRangeMax="255" ValidateDataTypeCheck="true"
                                            ValidationDataType="Integer" />
                                        <br />
                                        <div class="color-bg">
                                            <div class="color"></div>
                                        </div>
                                    </ItemTemplate>
                                </ui:UIGridViewTemplateColumn>
                                <ui:UIGridViewTemplateColumn HeaderText="Highlight" SortExpression="Highlight">
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <ui:UIFieldTextBox runat="server" ID="textHighlightR" Caption="R" PropertyName="HighlightR"
                                            InternalControlWidth="100%"
                                            FieldLayout="Flow" ValidateRequiredField="true" onkeyup="changeColor(this);"
                                            ValidationRangeType="Integer"
                                            ValidateRangeField="true" ValidationRangeMinInclusive="true" ValidationRangeMaxInclusive="true"
                                            ValidationRangeMin="0" ValidationRangeMax="255" ValidateDataTypeCheck="true"
                                            ValidationDataType="Integer" />
                                        <br />
                                        <ui:UIFieldTextBox runat="server" ID="textHighlightG" Caption="G" PropertyName="HighlightG"
                                            InternalControlWidth="100%"
                                            FieldLayout="Flow" ValidateRequiredField="true" onkeyup="changeColor(this);"
                                            ValidationRangeType="Integer"
                                            ValidateRangeField="true" ValidationRangeMinInclusive="true" ValidationRangeMaxInclusive="true"
                                            ValidationRangeMin="0" ValidationRangeMax="255" ValidateDataTypeCheck="true"
                                            ValidationDataType="Integer" />
                                        <br />
                                        <ui:UIFieldTextBox runat="server" ID="textHighlightB" Caption="B" PropertyName="HighlightB"
                                            InternalControlWidth="100%"
                                            FieldLayout="Flow" ValidateRequiredField="true" onkeyup="changeColor(this);"
                                            ValidationRangeType="Integer"
                                            ValidateRangeField="true" ValidationRangeMinInclusive="true" ValidationRangeMaxInclusive="true"
                                            ValidationRangeMin="0" ValidationRangeMax="255" ValidateDataTypeCheck="true"
                                            ValidationDataType="Integer" />
                                        <br />
                                        <ui:UIFieldTextBox runat="server" ID="textHighlightA" Caption="A" PropertyName="HighlightA"
                                            InternalControlWidth="100%"
                                            FieldLayout="Flow" ValidateRequiredField="true" onkeyup="changeColor(this);"
                                            ValidationRangeType="Integer"
                                            ValidateRangeField="true" ValidationRangeMinInclusive="true" ValidationRangeMaxInclusive="true"
                                            ValidationRangeMin="0" ValidationRangeMax="255" ValidateDataTypeCheck="true"
                                            ValidationDataType="Integer" />
                                        <br />
                                        <div class="color-bg">
                                            <div class="color"></div>
                                        </div>
                                    </ItemTemplate>
                                </ui:UIGridViewTemplateColumn>
                                --%>
                                <ui:UIGridViewTemplateColumn HeaderText="Fill" SortExpression="Fill">
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="labelR" Text="R"></asp:Label>
                                        <ui:UIFieldTextBox runat="server" ID="textFillR" Caption="R" PropertyName="FillR"
                                            InternalControlWidth="100%"
                                            FieldLayout="Flow" ValidateRequiredField="true" onkeyup="changeColor(this);"
                                            ValidationRangeType="Integer"
                                            ValidateRangeField="true" ValidationRangeMinInclusive="true" ValidationRangeMaxInclusive="true"
                                            ValidationRangeMin="0" ValidationRangeMax="255" ValidateDataTypeCheck="true"
                                            ValidationDataType="Integer" />
                                        &nbsp;
                                        <asp:Label runat="server" ID="labelG" Text="G"></asp:Label>
                                        <ui:UIFieldTextBox runat="server" ID="textFillG" Caption="G" PropertyName="FillG"
                                            InternalControlWidth="100%"
                                            FieldLayout="Flow" ValidateRequiredField="true" onkeyup="changeColor(this);"
                                            ValidationRangeType="Integer"
                                            ValidateRangeField="true" ValidationRangeMinInclusive="true" ValidationRangeMaxInclusive="true"
                                            ValidationRangeMin="0" ValidationRangeMax="255" ValidateDataTypeCheck="true"
                                            ValidationDataType="Integer" />
                                        &nbsp;
                                        <asp:Label runat="server" ID="labelB" Text="B"></asp:Label>
                                        <ui:UIFieldTextBox runat="server" ID="textFillB" Caption="B" PropertyName="FillB"
                                            InternalControlWidth="100%"
                                            FieldLayout="Flow" ValidateRequiredField="true" onkeyup="changeColor(this);"
                                            ValidationRangeType="Integer"
                                            ValidateRangeField="true" ValidationRangeMinInclusive="true" ValidationRangeMaxInclusive="true"
                                            ValidationRangeMin="0" ValidationRangeMax="255" ValidateDataTypeCheck="true"
                                            ValidationDataType="Integer" />
                                        &nbsp;
                                        <asp:Label runat="server" ID="labelA" Text="A"></asp:Label>
                                        <ui:UIFieldTextBox runat="server" ID="textFillA" Caption="A" PropertyName="FillA"
                                            InternalControlWidth="100%"
                                            FieldLayout="Flow" ValidateRequiredField="true" onkeyup="changeColor(this);"
                                            ValidationRangeType="Integer"
                                            ValidateRangeField="true" ValidationRangeMinInclusive="true" ValidationRangeMaxInclusive="true"
                                            ValidationRangeMin="0" ValidationRangeMax="255" ValidateDataTypeCheck="true"
                                            ValidationDataType="Integer" />
                                        <div class="color-bg">
                                            <div class="color"></div>
                                        </div>
                                    </ItemTemplate>
                                </ui:UIGridViewTemplateColumn>
                            </Columns>
                        </ui:UIGridView>
                        <ui:UIHint runat="server" Text="R => Red (0-255), G = > Green (0-255), B => Blue (0-255), A => Alpha/Opacity (0-255)">
                        </ui:UIHint>
                    </ui:UITabView>
                    <ui:UITabView runat="server" ID="tabAccess" Caption="Access" SkinID="tabviewEdit"
                        meta:resourcekey="tabAccessResource" BorderStyle="NotSet">
                        <ui:UISeparator runat='server' ID="UISeparator9" Caption="Access" meta:resourcekey="UISeparator9Resource" />
                        <ui:UIGridView runat="server" Span="Half" Caption="Roles Granted This Dashboard"
                            BindObjectsToRows="True"
                            SortExpression="RoleName ASC"
                            PropertyName="Roles" ID="gridRoles"
                            PageSize="500" DataKeyNames="ObjectID" GridLines="Both"
                            OnAction="gridRole_Action" meta:resourcekey="gridRolesResource">
                            <PagerSettings Mode="NumericFirstLast" />
                            <Commands>
                                <ui:UIGridViewCommand AlwaysEnabled="False" CausesValidation="false" CommandName="AddRoles"
                                    CommandText="Add Role" ImageUrl="~/images/add.gif" meta:resourcekey="gridRolesUIGridViewCommandAddRoleResource" />
                                <ui:UIGridViewCommand AlwaysEnabled="False" CausesValidation="False"
                                    CommandName="RemoveObject" CommandText="Remove"
                                    ConfirmText="Are you sure you wish to remove this item?"
                                    ImageUrl="~/images/delete.gif" meta:resourcekey="gridRolesUIGridViewCommandRemoveResource" />
                            </Commands>
                            <Columns>
                                <ui:UIGridViewButtonColumn ButtonType="Image" CommandName="RemoveObject"
                                    ConfirmText="Are you sure you wish to remove this item?"
                                    ImageUrl="~/images/delete.gif" meta:resourcekey="gridRolesUIGridViewButtonColumnRemoveObjectResource">
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </ui:UIGridViewButtonColumn>
                                <ui:UIGridViewBoundColumn HeaderText="Role"
                                    PropertyName="RoleName" SortExpression="RoleName" LinkedObjectIDPropertyName="ObjectID"
                                    LinkedObjectTypeNamePropertyName="=ORole" meta:resourcekey="gridRolesUIGridViewBoundColumnRoleNameResource">
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </ui:UIGridViewBoundColumn>
                            </Columns>
                        </ui:UIGridView>
                        <ui:UIDialogBox runat="server" ID="dialogBoxRole">
                        <ui:UIObjectPanel ID="panelRole" runat="server" BorderStyle="NotSet" meta:resourcekey="panelRoleResource">
                            <web:subpanel runat="server" ID="subpanelRole" GridViewID="gridRoles" meta:resourcekey="subpanelRoleResource" />
                        </ui:UIObjectPanel>
                        </ui:UIDialogBox>
                        <ui:UISearchDialogBox runat="server" ID="DialogBox_RoleItem" SearchOnDemandBaseTableName="tRole"
                            SearchBehavior="SearchOnDemand" Width="800" Title="Role Access List" AllowMultipleSelection="true"
                            SearchOnDemandPropertyNames="RoleName" SearchOnDemandAutoSearchOnShow="true"
                            OnSelected="DialogBox_RoleItem_Selected" OnSearch="DialogBox_RoleItem_Search"
                            meta:resourcekey="DialogBox_RoleItemResource">
                            <Columns>
                                <ui:UIGridViewBoundColumn HeaderStyle-Width="800" HeaderText="Role Name" PropertyName="RoleName"
                                    SortExpression="RoleName" meta:resourcekey="gridRolesUIGridViewBoundColumnRoleNameResource">
                                </ui:UIGridViewBoundColumn>
                            </Columns>
                        </ui:UISearchDialogBox>
                    </ui:UITabView>
                    <ui:UITabView runat="server" ID="DashboardColumnLinkingTabView" Caption="Drill-Down (Dashboard)"
                        SkinID="tabviewEdit" BorderStyle="NotSet" meta:resourcekey="DashboardColumnLinkingTabViewResource">
                        <ui:UIHint runat="server" ID="DashboardColumnLinkingHint" Text="Set up the Dashboards that the user can drill-down to."
                            meta:resourcekey="DashboardColumnLinkingHintResource">
                        </ui:UIHint>
                        <ui:UIGridView ID="gv_DashboardColumnLinkings" runat="server" PropertyName="DashboardColumnLinkings"
                            Caption="Drill-Down Dashboards" SortExpression="" KeyName="ObjectID" Width="100%"
                            PagingEnabled="True" DataKeyNames="ObjectID" GridLines="Both" RowErrorColor=""
                            Style="clear: both;" meta:resourcekey="gv_DashboardColumnLinkingsResource">
                            <PagerSettings Mode="NumericFirstLast" />
                            <Commands>
                                <ui:UIGridViewCommand AlwaysEnabled="False" CausesValidation="False" CommandName="DeleteObject"
                                    CommandText="Delete" ConfirmText="Are you sure you wish to delete the selected items?"
                                    ImageUrl="~/images/delete.gif" meta:resourcekey="gv_DashboardColumnLinkingsUIGridViewCommandDeleteResource" />
                                <ui:UIGridViewCommand AlwaysEnabled="False" CausesValidation="False" CommandName="AddObject"
                                    CommandText="Add" ImageUrl="~/images/add.gif" meta:resourcekey="gv_DashboardColumnLinkingsUIGridViewCommandAddResource" />
                            </Commands>
                            <Columns>
                                <ui:UIGridViewButtonColumn ButtonType="Image" CommandName="EditObject" ImageUrl="~/images/edit.gif"
                                    meta:resourcekey="gv_DashboardColumnLinkingsUIGridViewButtonColumnEditObjectResource">
                                    <HeaderStyle HorizontalAlign="Left" Width="16px" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </ui:UIGridViewButtonColumn>
                                <ui:UIGridViewButtonColumn ButtonType="Image" CommandName="DeleteObject" ImageUrl="~/images/delete.gif"
                                    meta:resourcekey="gv_DashboardColumnLinkingsUIGridViewButtonColumnDeleteObjectResource">
                                    <HeaderStyle HorizontalAlign="Left" Width="16px" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </ui:UIGridViewButtonColumn>
                                <ui:UIGridViewBoundColumn HeaderText="Drill-Down to Dashboard" PropertyName="LinkedDashboard.ObjectName"
                                    meta:resourcekey="gv_DashboardColumnLinkingsUIGridViewBoundColumnLinkedDashboardObjectNameResource">
                                </ui:UIGridViewBoundColumn>
                            </Columns>
                        </ui:UIGridView>
                        <ui:UIDialogBox runat="server" ID="dialogBoxDashboardColumnLinkings">
                        <ui:UIObjectPanel ID="op_DashboardColumnLinkings" runat="server" BorderStyle="NotSet"
                            meta:resourcekey="op_DashboardColumnLinkingsResource">
                            <web:subpanel runat="server" ID="sp_DashboardColumnLinkings" GridViewID="gv_DashboardColumnLinkings"
                                OnPopulateForm="sp_DashboardColumnLinkings_PopulateForm" OnValidateAndUpdate="sp_DashboardColumnLinkings_ValidateAndUpdate"
                                meta:resourcekey="sp_DashboardColumnLinkingsResource"></web:subpanel>
                            <ui:UIFieldSearchableDropDownList ID="DashboardColumnLinksLinkedDashboardID" runat="server"
                                Caption="Drill-Down to Dashboard" PropertyName="LinkedDashboardID" ValidateRequiredField="True"
                                OnSelectedIndexChanged="DashboardColumnLinksLinkedDashboardID_SelectedIndexChanged"
                                meta:resourcekey="DashboardColumnLinksLinkedDashboardIDResource">
                            </ui:UIFieldSearchableDropDownList>
                            <ui:UIGridView runat="server" ID="DashboardColumnLinkingFieldsGridView" PropertyName="DashboardColumnLinkingFields"
                                OnAction="DashboardColumnLinkingFieldsGridView_Action" Caption="Filters Passed to Drill-Down Dashboard"
                                OnRowDataBound="DashboardColumnLinkingFieldsGridView_RowDataBound" BindObjectsToRows="true"
                                meta:resourcekey="DashboardColumnLinkingFieldsGridViewResource">
                                <Commands>
                                    <ui:UIGridViewCommand AlwaysEnabled="False" CausesValidation="False" CommandName="DeleteObject"
                                        CommandText="Delete" ConfirmText="Are you sure you wish to delete the selected items?"
                                        ImageUrl="~/images/delete.gif" meta:resourcekey="DashboardColumnLinkingFieldsGridViewUIGridViewCommandDeleteResource" />
                                    <ui:UIGridViewCommand AlwaysEnabled="False" CausesValidation="False" CommandName="AddRow"
                                        CommandText="Add" ImageUrl="~/images/add.gif" meta:resourcekey="DashboardColumnLinkingFieldsGridViewUIGridViewCommandAddResource" />
                                </Commands>
                                <Columns>
                                    <ui:UIGridViewButtonColumn ButtonType="Image" CommandName="EditObject" ImageUrl="~/images/edit.gif"
                                        meta:resourcekey="DashboardColumnLinkingFieldsGridViewUIGridViewButtonColumnEditObjectResource">
                                        <HeaderStyle HorizontalAlign="Left" Width="16px" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </ui:UIGridViewButtonColumn>
                                    <ui:UIGridViewButtonColumn ButtonType="Image" CommandName="DeleteObject" ImageUrl="~/images/delete.gif"
                                        meta:resourcekey="DashboardColumnLinkingFieldsGridViewUIGridViewButtonColumnDeleteObjectResource">
                                        <HeaderStyle HorizontalAlign="Left" Width="16px" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </ui:UIGridViewButtonColumn>
                                    <ui:UIGridViewTemplateColumn HeaderText="Source Type" meta:resourcekey="DashboardColumnLinkingFieldsGridViewUIGridViewTemplateColumnSourceTypeResource">
                                        <ItemTemplate>
                                            <ui:UIFieldDropDownList runat="server" ID="DashboardColumnLinkingFieldsSourceType"
                                                PropertyName="SourceType" Caption="Source Type" ShowCaption="false" FieldLayout="Flow"
                                                InternalControlWidth="300px" OnSelectedIndexChanged="DashboardColumnLinkingFieldsSourceType_SelectedIndexChanged"
                                                meta:resourcekey="DashboardColumnLinkingFieldsSourceTypeResource">
                                                <Items>
                                                    <asp:ListItem Value="0" Text="From Column in Data Source" meta:resourcekey="DashboardColumnLinkingFieldsSourceTypeListItem0Resource"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="From Value Entered by User in a Filter Field" meta:resourcekey="DashboardColumnLinkingFieldsSourceTypeListItem1Resource"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="Static Value" meta:resourcekey="DashboardColumnLinkingFieldsSourceTypeListItem2Resource"></asp:ListItem>
                                                </Items>
                                            </ui:UIFieldDropDownList>
                                        </ItemTemplate>
                                    </ui:UIGridViewTemplateColumn>
                                    <ui:UIGridViewTemplateColumn HeaderText="Filter Field Control Identifier / Column Name / Static Value"
                                        meta:resourcekey="DashboardColumnLinkingFieldsSourceTypeUIGridViewTemplateColumnFilterFieldControlIdentifierColumnNameStaticValueResource">
                                        <ItemTemplate>
                                            <ui:UIFieldTextBox runat="server" ID="DashboardColumnLinkingFieldsSourceNameOrValueTextBox"
                                                PropertyName="SourceNameOrValue" Caption="Filter Field Control Identifier / Column Name"
                                                ShowCaption="false" FieldLayout="Flow" ValidateRequiredField="true" InternalControlWidth="100%"
                                                meta:resourcekey="DashboardColumnLinkingFieldsSourceNameOrValueTextBoxResource">
                                            </ui:UIFieldTextBox>
                                            <ui:UIFieldDropDownList runat="server" ID="DashboardColumnLinkingFieldsSourceNameOrValueDropDownList"
                                                PropertyName="SourceNameOrValue" Caption="Filter Field Control Identifier / Column Name"
                                                ShowCaption="false" FieldLayout="Flow" ValidateRequiredField="true" meta:resourcekey="DashboardColumnLinkingFieldsSourceNameOrValueDropDownListResource">
                                            </ui:UIFieldDropDownList>
                                        </ItemTemplate>
                                    </ui:UIGridViewTemplateColumn>
                                    <ui:UIGridViewTemplateColumn HeaderText="Drill-Down Dashboard Filter Field Control Identifier"
                                        meta:resourcekey="DashboardColumnLinkingFieldsSourceNameOrValueDropDownListUIGridViewTemplateColumnDrillDownDashboardFilterFieldControlIdentifierResource">
                                        <ItemTemplate>
                                            <ui:UIFieldDropDownList runat="server" ID="TargetDashboardControlIdentifier" PropertyName="TargetDashboardControlIdentifier"
                                                Caption="Drill-Down Dashboard Field Control Identifier / Column Name" ShowCaption="false"
                                                FieldLayout="Flow" ValidateRequiredField="true" meta:resourcekey="TargetDashboardControlIdentifierResource">
                                            </ui:UIFieldDropDownList>
                                        </ItemTemplate>
                                    </ui:UIGridViewTemplateColumn>
                                </Columns>
                            </ui:UIGridView>
                            <ui:UIDialogBox runat="server" ID="DashboardColumnLinkingFieldsDialogBox">
                            <ui:UIObjectPanel runat="server" ID="DashboardColumnLinkingFieldsObjectPanel" meta:resourcekey="DashboardColumnLinkingFieldsObjectPanelResource">
                                <web:subpanel runat="server" ID="DashboardColumnLinkingFieldsSubPanel" GridViewID="DashboardColumnLinkingFieldsGridView"
                                    meta:resourcekey="DashboardColumnLinkingFieldsSubPanelResource" />
                            </ui:UIObjectPanel>
                            </ui:UIDialogBox>
                        </ui:UIObjectPanel>
                        </ui:UIDialogBox>
                    </ui:UITabView>
                    <ui:UITabView runat="server" ID="DashboardReportLinkingTabView" Caption="Drill-Down (Report)"
                        SkinID="tabviewEdit" BorderStyle="NotSet" meta:resourcekey="DashboardReportLinkingTabViewResource">
                        <ui:UIHint runat="server" ID="DashboardReportLinkingHint" Text="Set up the Reports linking that the user can drill-down to."
                            meta:resourcekey="DashboardReportLinkingHintResource">
                        </ui:UIHint>
                        <ui:UIGridView ID="gv_DashboardReportLinkings" runat="server" PropertyName="DashboardReportLinkings"
                            Caption="Drill-Down Reports" SortExpression="" KeyName="ObjectID" Width="100%"
                            PagingEnabled="True" DataKeyNames="ObjectID" GridLines="Both" RowErrorColor=""
                            Style="clear: both;" meta:resourcekey="gv_DashboardReportLinkingsResource">
                            <PagerSettings Mode="NumericFirstLast" />
                            <Commands>
                                <ui:UIGridViewCommand AlwaysEnabled="False" CausesValidation="False" CommandName="DeleteObject"
                                    CommandText="Delete" ConfirmText="Are you sure you wish to delete the selected items?"
                                    ImageUrl="~/images/delete.gif" meta:resourcekey="gv_DashboardReportLinkingsUIGridViewCommandDeleteResource" />
                                <ui:UIGridViewCommand AlwaysEnabled="False" CausesValidation="False" CommandName="AddObject"
                                    CommandText="Add" ImageUrl="~/images/add.gif" meta:resourcekey="gv_DashboardReportLinkingsUIGridViewCommandAddResource" />
                            </Commands>
                            <Columns>
                                <ui:UIGridViewButtonColumn ButtonType="Image" CommandName="EditObject" ImageUrl="~/images/edit.gif"
                                    meta:resourcekey="gv_DashboardReportLinkingsUIGridViewButtonColumnEditObjectResource">
                                    <HeaderStyle HorizontalAlign="Left" Width="16px" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </ui:UIGridViewButtonColumn>
                                <ui:UIGridViewButtonColumn ButtonType="Image" CommandName="DeleteObject" ImageUrl="~/images/delete.gif"
                                    meta:resourcekey="gv_DashboardReportLinkingsUIGridViewButtonColumnDeleteObjectResource">
                                    <HeaderStyle HorizontalAlign="Left" Width="16px" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </ui:UIGridViewButtonColumn>
                                <ui:UIGridViewBoundColumn HeaderText="Drill-Down to Report" PropertyName="LinkedReport.ReportName"
                                    meta:resourcekey="gv_DashboardReportLinkingsUIGridViewBoundColumnLinkedReportReportNameResource">
                                </ui:UIGridViewBoundColumn>
                            </Columns>
                        </ui:UIGridView>
                        <ui:UIDialogBox runat="server" ID="dialogBoxDashboardReportLinkings">
                        <ui:UIObjectPanel ID="op_DashboardReportLinkings" runat="server" BorderStyle="NotSet"
                            meta:resourcekey="op_DashboardReportLinkingsResource">
                            <web:subpanel runat="server" ID="sp_DashboardReportLinkings" GridViewID="gv_DashboardReportLinkings"
                                OnPopulateForm="sp_DashboardReportLinkings_PopulateForm" OnValidateAndUpdate="sp_DashboardReportLinkings_ValidateAndUpdate"
                                meta:resourcekey="sp_DashboardReportLinkingsResource"></web:subpanel>
                            <ui:UIFieldSearchableDropDownList ID="DashboardReportLinksLinkedReportID" runat="server"
                                Caption="Drill-Down to Report" PropertyName="LinkedReportID" ValidateRequiredField="True"
                                OnSelectedIndexChanged="DashboardReportLinksLinkedReportID_SelectedIndexChanged"
                                meta:resourcekey="DashboardReportLinksLinkedReportIDResource">
                            </ui:UIFieldSearchableDropDownList>
                            <ui:UIFieldSearchableDropDownList runat="server" ID="dropLinkedReportTemplate" PropertyName="LinkedReportTemplateID"
                                Caption="Using Template">
                            </ui:UIFieldSearchableDropDownList>
                            <ui:UIGridView runat="server" ID="DashboardReportLinkingFieldsGridView" PropertyName="DashboardReportLinkingFields"
                                OnAction="DashboardReportLinkingFieldsGridView_Action" Caption="Filters Passed to Drill-Down Dashboard"
                                OnRowDataBound="DashboardReportLinkingFieldsGridView_RowDataBound" BindObjectsToRows="true"
                                meta:resourcekey="DashboardReportLinkingFieldsGridViewResource">
                                <Commands>
                                    <ui:UIGridViewCommand AlwaysEnabled="False" CausesValidation="False" CommandName="DeleteObject"
                                        CommandText="Delete" ConfirmText="Are you sure you wish to delete the selected items?"
                                        ImageUrl="~/images/delete.gif" meta:resourcekey="DashboardReportLinkingFieldsGridViewUIGridViewCommandDeleteResource" />
                                    <ui:UIGridViewCommand AlwaysEnabled="False" CausesValidation="False" CommandName="AddRow"
                                        CommandText="Add" ImageUrl="~/images/add.gif" meta:resourcekey="DashboardReportLinkingFieldsGridViewUIGridViewCommandAddResource" />
                                </Commands>
                                <Columns>
                                    <ui:UIGridViewButtonColumn ButtonType="Image" CommandName="EditObject" ImageUrl="~/images/edit.gif"
                                        meta:resourcekey="DashboardReportLinkingFieldsGridViewUIGridViewButtonColumnEditObjectResource">
                                        <HeaderStyle HorizontalAlign="Left" Width="16px" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </ui:UIGridViewButtonColumn>
                                    <ui:UIGridViewButtonColumn ButtonType="Image" CommandName="DeleteObject" ImageUrl="~/images/delete.gif"
                                        meta:resourcekey="DashboardReportLinkingFieldsGridViewUIGridViewButtonColumnDeleteObjectResource">
                                        <HeaderStyle HorizontalAlign="Left" Width="16px" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </ui:UIGridViewButtonColumn>
                                    <ui:UIGridViewTemplateColumn HeaderText="Source Type" meta:resourcekey="DashboardReportLinkingFieldsGridViewUIGridViewTemplateColumnSourceTypeResource">
                                        <ItemTemplate>
                                            <ui:UIFieldDropDownList runat="server" ID="DashboardReportLinkingFieldsSourceType"
                                                PropertyName="SourceType" Caption="Source Type" ShowCaption="false" FieldLayout="Flow"
                                                InternalControlWidth="300px" OnSelectedIndexChanged="DashboardReportLinkingFieldsSourceType_SelectedIndexChanged"
                                                meta:resourcekey="DashboardReportLinkingFieldsSourceTypeResource">
                                                <Items>
                                                    <asp:ListItem Value="0" Text="From Column in Data Source" meta:resourcekey="DashboardReportLinkingFieldsSourceTypeListItem0Resource"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="From Value Entered by User in a Filter Field" meta:resourcekey="DashboardReportLinkingFieldsSourceTypeListItem1Resource"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="Static Value" meta:resourcekey="DashboardReportLinkingFieldsSourceTypeListItem2Resource"></asp:ListItem>
                                                </Items>
                                            </ui:UIFieldDropDownList>
                                        </ItemTemplate>
                                    </ui:UIGridViewTemplateColumn>
                                    <ui:UIGridViewTemplateColumn HeaderText="Filter Field Control Identifier / Column Name / Static Value"
                                        meta:resourcekey="DashboardReportLinkingFieldsSourceTypeUIGridViewTemplateColumnFilterFieldControlIdentifierColumnNameStaticValueResource">
                                        <ItemTemplate>
                                            <ui:UIFieldTextBox runat="server" ID="DashboardReportLinkingFieldsSourceNameOrValueTextBox"
                                                PropertyName="SourceNameOrValue" Caption="Filter Field Control Identifier / Column Name"
                                                ShowCaption="false" FieldLayout="Flow" ValidateRequiredField="true" InternalControlWidth="100%"
                                                meta:resourcekey="DashboardReportLinkingFieldsSourceNameOrValueTextBoxResource">
                                            </ui:UIFieldTextBox>
                                            <ui:UIFieldDropDownList runat="server" ID="DashboardReportLinkingFieldsSourceNameOrValueDropDownList"
                                                PropertyName="SourceNameOrValue" Caption="Filter Field Control Identifier / Column Name"
                                                ShowCaption="false" FieldLayout="Flow" ValidateRequiredField="true" meta:resourcekey="DashboardReportLinkingFieldsSourceNameOrValueDropDownListResource">
                                            </ui:UIFieldDropDownList>
                                        </ItemTemplate>
                                    </ui:UIGridViewTemplateColumn>
                                    <ui:UIGridViewTemplateColumn HeaderText="Drill-Down Report Filter Field Control Identifier"
                                        meta:resourcekey="DashboardReportLinkingFieldsSourceNameOrValueDropDownListUIGridViewTemplateColumnDrillDownReportFilterFieldControlIdentifierResource">
                                        <ItemTemplate>
                                            <ui:UIFieldDropDownList runat="server" ID="TargetReportControlIdentifier" PropertyName="TargetReportControlIdentifier"
                                                Caption="Drill-Down Report Field Control Identifier / Column Name" ShowCaption="false"
                                                FieldLayout="Flow" ValidateRequiredField="true" meta:resourcekey="TargetReportControlIdentifierResource">
                                            </ui:UIFieldDropDownList>
                                        </ItemTemplate>
                                    </ui:UIGridViewTemplateColumn>
                                </Columns>
                            </ui:UIGridView>
                            <ui:UIDialogBox runat="server" ID="DashboardReportLinkingFieldsDialogBox">
                            <ui:UIObjectPanel runat="server" ID="DashboardReportLinkingFieldsObjectPanel" meta:resourcekey="DashboardReportLinkingFieldsObjectPanelResource">
                                <web:subpanel runat="server" ID="DashboardReportLinkingFieldsSubPanel" GridViewID="DashboardReportLinkingFieldsGridView"
                                    meta:resourcekey="DashboardReportLinkingFieldsSubPanelResource" />
                            </ui:UIObjectPanel>
                            </ui:UIDialogBox>
                        </ui:UIObjectPanel>
                        </ui:UIDialogBox>
                    </ui:UITabView>
                    <ui:UITabView runat="server" ID="tabPreview" Caption="Preview" SkinID="tabviewEdit"
                        meta:resourcekey="tabPreviewResource" BorderStyle="NotSet">
                        <ui:UISeparator runat='server' ID="UISeparator8" Caption="Preview" meta:resourcekey="UISeparator8Resource" />
                        <iframe runat="server" id="framePreview" style="width: 600px; height: 600px; border: solid 1px #e0e0e0" scrolling="no"
                            frameborder="0"></iframe>
                    </ui:UITabView>
                    <web:audit runat="server" ID="Audit" meta:resourcekey="AuditResource" />
                </ui:UITabStrip>
                <web:object2 runat="server" ID="panel2" meta:resourcekey="panel2Resource" />
            </div>
        </ui:UIObjectPanel>
    </form>
</body>
</html>
