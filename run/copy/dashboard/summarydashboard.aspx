﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PageBase" %>

<%@ Import Namespace="System.Data" %>

<!DOCTYPE html>

<script runat="server">    
    protected void Page_Load(object sender, EventArgs e)
    {
        string[] args = Security.Decrypt(Request["ID"]).Split(':');
        ODashboard dashboard = TablesLogic.tDashboard.Load(new Guid(args[0]));
        DataTable dt = GetDataTable(dashboard);
        DataRow firstRow = dt.Rows[0];

        summaryFigure.InnerText = firstRow[0].ToString();
        summaryTitle.InnerText = dashboard.SummaryTitle;
        progressbarTitle.InnerText = dashboard.SummaryProgressBarTitle;
        progressbarPercentage.InnerText = firstRow[1].ToString() + "%";
        progressbarInner.Style["width"] = firstRow[1].ToString() + "%";

        if (dashboard.ShowSummaryProgressBar == 0)
        {
            progressbar.Style["display"] = "none";
        }
    }

    protected void SummaryRedirect_Click(Object sender, EventArgs e)
    {
        string[] args = Security.Decrypt(Request["ID"]).Split(':');
        ODashboard dashboard = TablesLogic.tDashboard.Load(new Guid(args[0]));

        if (dashboard.SummaryLinkedReportID != null && dashboard.SummaryLinkedReportID.Trim() != "")
        {
            string reportUrl =  this.ResolveUrl("~/modules/reportviewer/search.aspx") + "?ID=" +
                            HttpUtility.UrlEncode(Security.Encrypt(dashboard.SummaryLinkedReportID.ToString() + ":" + AppSession.SaltID));
            Window.Open(reportUrl, "AnacleEAM_Window");
        }
    }

    protected DataTable GetDataTable(ODashboard dashboard)
    {
        DataTable dt = null;
        List<LogicLayer.Parameter> paramList = ConstructParameters(dashboard);

        if (dashboard.UseCSharpQuery == 0 || dashboard.UseCSharpQuery == null)
        {
            dt = Analysis.DoQuery(dashboard.DashboardQuery, paramList.ToArray());
        }
        else
        {
            object dataSource = Analysis.InvokeMethod(dashboard.CSharpMethodName, paramList.ToArray());
            if (dataSource is DataTable)
                dt = (DataTable)dataSource;
            else if (dataSource is DataSet)
                dt = ((DataSet)dataSource).Tables[0];
        }
        return dt;
    }

    protected List<LogicLayer.Parameter> ConstructParameters(ODashboard dashboard)
    {
        List<LogicLayer.Parameter> paramList = new List<LogicLayer.Parameter>();
        paramList.Add(LogicLayer.Parameter.New("UserID", System.Data.DbType.Guid, 16, AppSession.User.ObjectID.Value));
        paramList.Add(LogicLayer.Parameter.New("DashboardID", System.Data.DbType.Guid, 16, dashboard.ObjectID.Value));
        return paramList;
    }


</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
        p 
        {
            color: #6c6c6c;
            text-transform: uppercase;
            margin: 0;
        }

        #progressbar p 
        {
            font-size: 9px;
        }
    </style>

    <script>
        function summaryRedirect() {
            __doPostBack('SummaryRedirect', 'OnClick');
        }
    </script>
</head>
<body onclick="summaryRedirect()" style="cursor: pointer;">
    <form id="form1" runat="server">
    <div style="display: none;">
        <asp:Button ID="SummaryRedirect" runat="server" OnClick="SummaryRedirect_Click" />
    </div>
    <div style="display: block; margin: 15px 20px 5px 20px;">
        <div>
            <div><span runat="server" id="summaryFigure" style="font-size: 20px; color: #e38f57;"/></div>
            <div><p runat="server" id="summaryTitle" style="font-size: 11px; margin: 0;"/></div>
        </div>
        <div runat="server" id="progressbar">
            <div id="progressbarOuter" style="background-color: #dddddd; height: 3px; width: 100%; border-radius: 3px;">
                <div runat="server" id="progressbarInner" style="background-color: #e38f57; height: 3px; border-radius: 3px;"></div>
            </div>
            <div>
                <p runat="server" id="progressbarTitle" style="float: left;"/>
                <p runat="server" id="progressbarPercentage" style="float: right;"/>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
