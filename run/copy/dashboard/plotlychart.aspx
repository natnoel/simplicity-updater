﻿<%@ Page Language="C#" Inherits="PageBase" Theme="Corporate" AutoEventWireup="true" %>

<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Reflection" %>
<%@ Import Namespace="System.Resources" %>
<%@ Import Namespace="LogicLayer" %>
<%@ Import Namespace="System.Runtime.Serialization" %>
<%@ Import Namespace="System.Runtime.Serialization.Json" %>
<%@ Import Namespace="Newtonsoft.Json.Linq" %>
<%@ Import Namespace="Newtonsoft.Json" %>

<script runat="server">      

            public override bool DisableLoadingScreen
            {
                get
                {
                    return true;
                }
            }

            public int DrillCount
            {
                get
                {
                    if (Session[Security.Decrypt(this.ViewStateKey.Text) + "_DrillCount"] == null)
                        Session[Security.Decrypt(this.ViewStateKey.Text) + "_DrillCount"] = 0;

                    return (int)Session[Security.Decrypt(this.ViewStateKey.Text) + "_DrillCount"];
                }
                set
                {
                    Session[Security.Decrypt(this.ViewStateKey.Text) + "_DrillCount"] = value;
                }
            }

            public List<Guid> DrillIDs
            {
                get
                {
                    if (Session[Security.Decrypt(this.ViewStateKey.Text) + "_DrillIDs"] == null)
                        Session[Security.Decrypt(this.ViewStateKey.Text) + "_DrillIDs"] = new List<Guid>();

                    return (List<Guid>)Session[Security.Decrypt(this.ViewStateKey.Text) + "_DrillIDs"];
                }
                set
                {
                    Session[Security.Decrypt(this.ViewStateKey.Text) + "_DrillIDs"] = value;
                }
            }


            public Hashtable DrillDownArguments
            {
                get
                {
                    return (Hashtable)Session[Security.Decrypt(this.ViewStateKey.Text) + "_DRILL"];
                }
                set
                {
                    Session[Security.Decrypt(this.ViewStateKey.Text) + "_DRILL"] = value;
                }
            }


            bool refreshChart = false;

            protected override void OnInit(EventArgs e)
            {
                base.OnInit(e);

                if (!IsPostBack)
                {
                    DrillCount = 0;
                    DrillIDs = new List<Guid>();
                    DrillDownArguments = new Hashtable();

                    string[] args = Security.Decrypt(Request["ID"]).Split(':');
                    DrillIDs.Add(new Guid(args[0]));

                    refreshChart = true;
                }

            }

            protected override void OnLoad(EventArgs e)
            {
                base.OnLoad(e);

            }

            /// <summary>
            /// Get the dashboard ID from the request query string.
            /// </summary>
            /// <returns></returns>
            protected Guid GetRequestDashboardID()
            {
                return (DrillIDs[DrillIDs.Count - 1]);
            }

            /// <summary>
            /// Get the dashboard ID from the request query string.
            /// </summary>
            /// <returns></returns>
            protected Guid GetFirstDashboardID()
            {
                return (DrillIDs[0]);
            }

            /// <summary>
            /// Generates the Javascript to display the Plotly chart.
            /// </summary>
            protected void ShowPlotlyChart()
            {
                ODashboard dashboard = TablesLogic.tDashboard.Load(GetRequestDashboardID());
                parameterList = ConstructParameters(dashboard);
                string jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(parameterList);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "plotly", @"
            $.post(
                window.location.href.replace('plotlychart.aspx', 'plotlychartdata.aspx') + '&generate=JSON" + (DrillCount > 0 ? "&drillcount=" + DrillCount : "") + @"', 
                'params=" + HttpUtility.UrlEncode(jsonString) + @"' ,
                function(data) {
                    document.getElementById('loading-gif').style.visibility = 'hidden';
console.log(data);
                    var obj = JSON.parse(data);
                    if (!obj.error)
                    {
                        var gd = getPlainChart();

                        plotlyChartDiv = document.getElementById('plotlyChart');
                        plotlyChartDiv.appendChild(gd);

                        Plotly.newPlot(gd, obj.data, obj.layout, {displayModeBar: false});

                        " + addDrillOnClickJavaScript() + jsShowOrHideOptionsBtn() + @"

                        var bars = $('.barlayer > .bars > .points > .point');
                        bars.hide();
                        fadeInSequence(bars, 500 / bars.length);
                        var lines = $('.scatterlayer .trace');
                        lines.hide();
                        console.log('lines duration: ' + 500 / lines.length);
                        fadeInSequence(lines, 500 / lines.length);
                        var slices = $('.pielayer .slice');
                        slices.hide();
                        console.log('slices duration: ' + 500 / slices.length);
                        fadeInSequence(slices, 500 / slices.length);
                    }
                    else
                    {
                        plotlyChartDiv = document.getElementById('plotlyChart');
                        plotlyChartDiv.innerHTML = '<div style=""width: 100vw; height: 100vh; margin-top: 40vh"" align=""center"">Unable to display the chart</div>';
                            }
        });
   
            ", true);

    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (this.Controls[0] is LiteralControl)
        {
            LiteralControl literal = (LiteralControl)this.Controls[0];
            literal.Text = ""; // Removes the DOCTYPE tag as this causes problems with Plotly.
        }

        BackButton.Visible = (DrillCount > 0);
        CreateAllDrillDownFilterControls(true);

        if (refreshChart)
            ShowPlotlyChart();
    }

    string addDrillOnClickJavaScript()
    {
        string drillOnClickScript = "";
        ODashboard dashboard = TablesLogic.tDashboard.Load(GetRequestDashboardID());

        // If chart has drill feature
        //if (DrillCount < dashboard.DashboardColumnLinkings.Count)
        {
            drillOnClickScript = @" 

gd.on('plotly_click', function(data){
    //console.log('click: ' + X + ',' + Y);
    //console.log(data.points.length)
    var customdata = '';
    for (var i = 0; i < data.points.length; i++)
    {
        var pointNumber = data.points[i].pointNumber;
        var curveNumber = data.points[i].curveNumber;
        customdata = data.points[i].data.customdata[pointNumber];
    }

console.log(customdata)
    $('#DrillParameterTextBox').val(customdata);
    //$('#DrillButton').click();
});";
        }

        return drillOnClickScript;
    }

    private int colorIndex;

    /// <summary>
    /// Height of the legend
    /// </summary>
    public int? legendHeight;

    /// <summary>
    /// render a new chart
    /// </summary>
    public bool RenderNew = false;

    /// <summary>
    /// Client-side JavaScript to execute 
    /// when options button clicked
    /// </summary>
    public string OptionsClickClientScript;

    PlotlyChart plotlyChart;

    private Hashtable parameters = new Hashtable();

    private Hashtable loadedDashboards = new Hashtable();


    /// <summary>
    /// Loads the ODashboard object from the cache or from the database.
    /// </summary>
    /// <param name="dashboardID"></param>
    /// <returns></returns>
    protected ODashboard GetCachedDashboardByID(Guid dashboardID)
    {
        ODashboard dashboard = null;
        if (loadedDashboards[dashboardID] == null)
        {
            dashboard = TablesLogic.tDashboard.Load(dashboardID);
            loadedDashboards[dashboardID] = dashboard;
        }
        dashboard = (ODashboard)loadedDashboards[dashboardID];
        return dashboard;
    }


    /// <summary>
    /// Creates a panel containing all the filter controls for a single drill-down dashboard.
    /// </summary>
    /// <param name="drillCount"></param>
    /// <param name="dashboard"></param>
    /// <param name="visible"></param>
    protected ODashboard CreateFilterControls(int drillCount, Guid dashboardID, bool visible)
    {
        // Loads a copy of the dashboard by it's ID from cache, or from the
        // database.
        //
        ODashboard dashboard = GetCachedDashboardByID(dashboardID);

        // If the Table containing the filters already exist, don't
        // create it again.
        //
        string id = "_" + drillCount + dashboardID.ToString();
        if (PanelControl.FindControl(id) != null)
            return dashboard;

        if (dashboard == null)
            return dashboard;

        FilterButton.Visible = false;
        List<ODashboardField> dashboardFields = dashboard.GetDashboardFieldsInOrder();
        if (dashboardFields.Count == 0)
            return dashboard;

        if (visible)
            FilterButton.Visible = true;

        // Creates all the filter controls.
        //
        Table t = new Table();
        t.ID = id;

        t.Width = Unit.Percentage(100);
        t.CellPadding = 0;
        t.CellSpacing = 0;
        t.Visible = visible;

        PanelControl.Controls.Add(t);
        PanelControl.ControlStyle.CssClass = "controls";
        TableRow tr = null;

        foreach (ODashboardField field in dashboardFields)
        {
            UIFieldBase control = null;

            if (field.ControlType == (int)ReportControlTypeEnum.MonthYear)
            {
                control = new UIFieldDateTime();
                ((UIFieldDateTime)control).SelectMonthYear = true;
                ((UIFieldDateTime)control).DateTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            }
            else if (field.ControlType == (int)ReportControlTypeEnum.DropdownList)
            {
                control = new UIFieldSearchableDropDownList();
                control.EnableViewState = true;
            }
            else if (field.ControlType == (int)ReportControlTypeEnum.RadioButtonList)
            {
                control = new UIFieldRadioList();
                control.EnableViewState = true;
            }
            else if (field.ControlType == (int)ReportControlTypeEnum.DateTime)
            {
                control = new UIFieldDateTime();
                ((UIFieldDateTime)control).ShowTimeControls = true;
            }
            else if (field.ControlType == (int)ReportControlTypeEnum.Date)
            {
                control = new UIFieldDateTime();
                ((UIFieldDateTime)control).ShowTimeControls = false;
            }

            if (control != null && (field.ControlType == (int)ReportControlTypeEnum.DropdownList
                || field.ControlType == (int)ReportControlTypeEnum.RadioButtonList))
            {
                control.ID = "_" + drillCount + field.ControlIdentifier;
                control.EnableViewState = true;

                if (field.ControlType == (int)ReportControlTypeEnum.RadioButtonList)
                {
                    ((UIFieldRadioList)control).RepeatColumns = 0;
                }

                // Create a new cell for the control
                // and insert the control into it.
                //
                tr = new TableRow();
                tr.VerticalAlign = VerticalAlign.Middle;
                t.Rows.Add(tr);

                System.Web.UI.WebControls.Label labelCaption = new System.Web.UI.WebControls.Label();
                TableCell td = new TableCell();
                td.VerticalAlign = VerticalAlign.Middle;
                tr.Cells.Add(td);
                tr.Cells[0].Controls.Add(control);

                control.ControlInfo = field.ControlIdentifier;

                control.Caption = TranslateDashboardItem(field.ControlCaption);
                control.Span = Span.Full;

                // set up validation
                //
                if (field.DataType == 0)
                    control.ValidateDataTypeCheck = false;
                else
                    control.ValidateDataTypeCheck = true;

                /*
                                // attach event to dropdown list for cascading drop down
                                //
                                if (field.ControlType == (int)ReportControlTypeEnum.DropdownList)
                                    ((UIFieldSearchableDropDownList)control).SelectedIndexChanged += new EventHandler(listControl_SelectedIndexChanged);
                                if (field.ControlType == (int)ReportControlTypeEnum.RadioButtonList)
                                    ((UIFieldRadioList)control).SelectedIndexChanged += new EventHandler(listControl_SelectedIndexChanged);*/
            }

        }

        return dashboard;
    }


    /// <summary>
    /// Creates the filter controls for all drill-down dashboards.
    /// </summary>
    protected void CreateAllDrillDownFilterControls(bool bindData)
    {
        int drillCount = DrillIDs.Count - 1;
        if (drillCount < 0)
            drillCount = 0;

        for (int i = 0; i < DrillIDs.Count; i++)
        {
            Guid dashboardID = DrillIDs[i];
            ODashboard dashboard = CreateFilterControls(i, dashboardID, i == drillCount);

            if (dashboard != null && i == drillCount && bindData)
            {
                if (ViewState["LastDrillFiltersCreated"] == null)
                    ViewState["LastDrillFiltersCreated"] = -1;
                int lastDrillFiltersCreated = (int)ViewState["LastDrillFiltersCreated"];

                if (lastDrillFiltersCreated != drillCount)
                {
                    BindDropValue(dashboard);

                    if (drillCount > 0)
                    {
                        ODashboard previousDashboard = GetCachedDashboardByID(DrillIDs[i - 1]);
                        BindDrillDownData(previousDashboard);
                    }

                    ViewState["LastDrillFiltersCreated"] = drillCount;
                }
            }
        }
    }

    /// <summary>
    /// Creates all the filter controls based on the current dashboard ID.
    /// </summary>
    protected override void CreateChildControls()
    {
        base.CreateChildControls();
        CreateAllDrillDownFilterControls(false);
    }

    /*
    ODashboard dashboard = TablesLogic.tDashboard.Load(GetRequestDashboardID());
    if (dashboard == null)
    {
        return;
    }

    if (dashboard.GetDashboardFieldsInOrder().Count == 0)
    {
        return;
    }

    // Creates all the filter controls.
    //
    Table t = new Table();
    t.ID = "_" + DrillCount + GetRequestDashboardID().ToString();
    t.Width = Unit.Percentage(100);
    t.CellPadding = 0;
    t.CellSpacing = 0;

    PanelControl.Controls.Add(t);
    PanelControl.ControlStyle.CssClass = "controls";
    TableRow tr = null;

    foreach (ODashboardField field in dashboard.GetDashboardFieldsInOrder())
    {
        UIFieldBase control = null;

        if (field.ControlType == (int)ReportControlTypeEnum.MonthYear)
        {
            control = new UIFieldDateTime();
            ((UIFieldDateTime)control).SelectMonthYear = true;
            ((UIFieldDateTime)control).DateTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
        }
        else if (field.ControlType == (int)ReportControlTypeEnum.DropdownList)
        {
            control = new UIFieldSearchableDropDownList();
            control.EnableViewState = true;
        }
        else if (field.ControlType == (int)ReportControlTypeEnum.RadioButtonList)
        {
            control = new UIFieldRadioList();
            control.EnableViewState = true;
        }
        else if (field.ControlType == (int)ReportControlTypeEnum.DateTime)
        {
            control = new UIFieldDateTime();
            ((UIFieldDateTime)control).ShowTimeControls = true;
        }
        else if (field.ControlType == (int)ReportControlTypeEnum.Date)
        {
            control = new UIFieldDateTime();
            ((UIFieldDateTime)control).ShowTimeControls = false;
        }

        if (control != null && (field.ControlType == (int)ReportControlTypeEnum.DropdownList
            || field.ControlType == (int)ReportControlTypeEnum.RadioButtonList))
        {
            control.ID = "_" + DrillCount + field.ControlIdentifier;
            control.EnableViewState = true;

            if (field.ControlType == (int)ReportControlTypeEnum.RadioButtonList)
            {
                ((UIFieldRadioList)control).RepeatColumns = 0;
            }

            // Create a new cell for the control
            // and insert the control into it.
            //
            tr = new TableRow();
            tr.VerticalAlign = VerticalAlign.Middle;
            t.Rows.Add(tr);

            System.Web.UI.WebControls.Label labelCaption = new System.Web.UI.WebControls.Label();
            TableCell td = new TableCell();
            td.VerticalAlign = VerticalAlign.Middle;
            tr.Cells.Add(td);
            tr.Cells[0].Controls.Add(control);

            control.ControlInfo = field.ControlIdentifier;

            control.Caption = TranslateDashboardItem(field.ControlCaption);
            //control.CaptionWidth = 100;
            //if (control.Caption.Trim() == "")
            //    control.ShowCaption = false;

            control.Span = Span.Full;

            // set up validation
            //
            if (field.DataType == 0)
                control.ValidateDataTypeCheck = false;
            else
                control.ValidateDataTypeCheck = true;

            // create the OdbcParameter for single value controls
            //
            LogicLayer.Parameter p = LogicLayer.Parameter.New(field.ControlIdentifier, DbType.String, 0, null, field.ControlCaption);
            if (field.DataType == 0)
            {
                control.ValidationDataType = ValidationDataType.String;
                p.DataType = DbType.String;//tessa
                p.Size = 255;
            }
            else if (field.DataType == 1)
            {
                control.ValidationDataType = ValidationDataType.Integer;
                p.DataType = DbType.Int32;//tessa change from System.Data.Odbc.OdbcType.Int to DbType.Int32
                p.Size = 4;
            }
            else if (field.DataType == 2)
            {
                control.ValidationDataType = ValidationDataType.Currency;
                p.DataType = DbType.Decimal; //tessa
                p.Size = 8;
            }
            else if (field.DataType == 3)
            {
                control.ValidationDataType = ValidationDataType.Double;
                p.DataType = DbType.Double;//tessa
                p.Size = 8;
            }
            else if (field.DataType == 4)
            {
                control.ValidationDataType = ValidationDataType.Date;
                p.DataType = DbType.DateTime;//Tessa
                p.Size = 8;
            }
            else if (field.DataType == 5)
            {
                control.ValidationDataType = ValidationDataType.String;
                p.DataType = DbType.Guid;//tessa
                p.Size = 8;
            }
            if (field.ControlType == (int)ReportControlTypeEnum.MultiSelectList)
                p.IsSingleValue = false;
            else
                p.IsSingleValue = true;

            parameters[field.ControlIdentifier] = p;

                            // attach event to dropdown list for cascading drop down
                            //
                            if (field.ControlType == (int)ReportControlTypeEnum.DropdownList)
                                ((UIFieldSearchableDropDownList)control).SelectedIndexChanged += new EventHandler(listControl_SelectedIndexChanged);
                            if (field.ControlType == (int)ReportControlTypeEnum.RadioButtonList)
                                ((UIFieldRadioList)control).SelectedIndexChanged += new EventHandler(listControl_SelectedIndexChanged);
        }
        else if (control != null && field.ControlType == (int)ReportControlTypeEnum.Date)
        {
            control.ID = "_" + DrillCount + field.ControlIdentifier;
            control.EnableViewState = true;
            ((UIFieldDateTime)control).DateTime = DateTime.Now;

            // Create a new cell for the control
            // and insert the control into it.
            //
            tr = new TableRow();
            tr.VerticalAlign = VerticalAlign.Middle;
            t.Rows.Add(tr);

            System.Web.UI.WebControls.Label labelCaption = new System.Web.UI.WebControls.Label();
            TableCell td = new TableCell();
            td.VerticalAlign = VerticalAlign.Middle;
            tr.Cells.Add(td);
            tr.Cells[0].Controls.Add(control);

            control.ControlInfo = field.ControlIdentifier;

            control.Caption = TranslateDashboardItem(field.ControlCaption);
            //control.CaptionWidth = 100;
            //if (control.Caption.Trim() == "")
            //    control.ShowCaption = false;

            LogicLayer.Parameter p = LogicLayer.Parameter.New(field.ControlIdentifier, DbType.String, 0, null, field.ControlCaption);
            if (field.DataType == (int)ReportDataTypeEnum.DateTime)
            {
                control.ValidationDataType = ValidationDataType.Date;
                p.DataType = DbType.DateTime;//Tessa
                p.Size = 8;
            }
            if (field.ControlType == (int)ReportControlTypeEnum.Date)
                ((UIFieldDateTime)control).Control.Change += new EventHandler(dateControl_DateTimeChanged);
            parameters[field.ControlIdentifier] = p;
        }

        else if (control != null)
        {
            control.ID = "_" + DrillCount + field.ControlIdentifier;
            control.EnableViewState = true;

            // Create a new cell for the control
            // and insert the control into it.
            //
            tr = new TableRow();
            tr.VerticalAlign = VerticalAlign.Middle;
            t.Rows.Add(tr);

            System.Web.UI.WebControls.Label labelCaption = new System.Web.UI.WebControls.Label();
            TableCell td = new TableCell();
            td.VerticalAlign = VerticalAlign.Middle;
            tr.Cells.Add(td);
            tr.Cells[0].Controls.Add(control);

            control.ControlInfo = field.ControlIdentifier;

            control.Caption = TranslateDashboardItem(field.ControlCaption);
            //control.CaptionWidth = 100;
            //if (control.Caption.Trim() == "")
            //    control.ShowCaption = false;

            control.Span = Span.Full;

            // set up validation
            //
            if (field.DataType == 0)
                control.ValidateDataTypeCheck = false;
            else
                control.ValidateDataTypeCheck = true;

            // create the OdbcParameter for single value controls
            //
            LogicLayer.Parameter p = LogicLayer.Parameter.New(field.ControlIdentifier, DbType.String, 0, null, field.ControlCaption);
            if (field.DataType == (int)ReportDataTypeEnum.String)
            {
                control.ValidationDataType = ValidationDataType.String;
                p.DataType = DbType.String;//tessa
                p.Size = 255;
            }
            else if (field.DataType == (int)ReportDataTypeEnum.Integer)
            {
                control.ValidationDataType = ValidationDataType.Integer;
                p.DataType = DbType.Int32;//tessa change from System.Data.Odbc.OdbcType.Int to DbType.Int32
                p.Size = 4;
            }
            else if (field.DataType == (int)ReportDataTypeEnum.Decimal)
            {
                control.ValidationDataType = ValidationDataType.Currency;
                p.DataType = DbType.Decimal; //tessa
                p.Size = 8;
            }
            else if (field.DataType == (int)ReportDataTypeEnum.Double)
            {
                control.ValidationDataType = ValidationDataType.Double;
                p.DataType = DbType.Double;//tessa
                p.Size = 8;
            }
            else if (field.DataType == (int)ReportDataTypeEnum.DateTime)
            {
                control.ValidationDataType = ValidationDataType.Date;
                p.DataType = DbType.DateTime;//Tessa
                p.Size = 8;
            }
            else if (field.DataType == 5)
            {
                control.ValidationDataType = ValidationDataType.String;
                p.DataType = DbType.Guid;//tessa
                p.Size = 8;
            }
            if (field.ControlType == (int)ReportDataTypeEnum.UniqueIdentifier)
                p.IsSingleValue = false;
            else
                p.IsSingleValue = true;
            parameters[field.ControlIdentifier] = p;

            // attach event to dropdown list for cascading drop down
            //
            if (field.ControlType == (int)ReportControlTypeEnum.DropdownList)
                ((UIFieldSearchableDropDownList)control).SelectedIndexChanged += new EventHandler(listControl_SelectedIndexChanged);
            if (field.ControlType == (int)ReportControlTypeEnum.RadioButtonList)
                ((UIFieldRadioList)control).SelectedIndexChanged += new EventHandler(listControl_SelectedIndexChanged);

            if (field.ControlType == (int)ReportControlTypeEnum.DateTime ||
                field.ControlType == (int)ReportControlTypeEnum.Date ||
                field.ControlType == (int)ReportControlTypeEnum.MonthYear)
            {
                ((UIFieldDateTime)control).Control.Change += new EventHandler(dateControl_DateTimeChanged);
                ((UIFieldDateTime)control).Control.ImageClearUrl = ResolveUrl("~/images/cross.gif");
            }

            //if (field.ControlType == (int)ReportControlTypeEnum.MultiSelectList)
            //    ((UIFieldCheckBoxListPanel)control).SelectedIndexChanged += new EventHandler(listControl_SelectedIndexChanged);
        }
    }
    */



    private string jsShowOrHideOptionsBtn()
    {
        ODashboard dashboard = TablesLogic.tDashboard.Load(GetRequestDashboardID());

        if (dashboard.DashboardFields.Count > 0)
            return "$('#options_btn').show();";
        return "";
    }

    protected void listControl_SelectedIndexChanged(object sender, EventArgs e)
    {
        // re-insert the original logic for cascading controls
    }


    protected void dateControl_DateTimeChanged(object sender, EventArgs e)
    {
        // re-insert the original logic for cascading controls
    }


    /// <summary>
    /// Translates the specified text from the dashboards.resx file.
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    protected string TranslateDashboardItem(string text)
    {
        string translatedText = Resources.Dashboards.ResourceManager.GetString(text);
        if (translatedText == null || translatedText == "")
            return text;
        return translatedText;
    }

    /// <summary>
    /// Binds values in the dropdown list.
    /// </summary>
    protected void BindDropValue(ODashboard dashboard)
    {
        if (dashboard == null)
            return;

        // Populates all list controls.
        //
        foreach (ODashboardField field in dashboard.DashboardFields)
        {
            Control control = this.FindControl("_" + DrillCount + field.ControlIdentifier);

            if (control != null)
            {
                if (field.ControlType == (int)ReportControlTypeEnum.DropdownList)
                    BindToList(control as UIFieldSearchableDropDownList, dashboard, field);
                else if (field.ControlType == (int)ReportControlTypeEnum.RadioButtonList)
                    BindToList(control as UIFieldRadioList, dashboard, field);
            }
        }
    }

    List<LogicLayer.Parameter> parameterList;
    /// ------------------------------------------------------------------
    /// <summary>
    /// Bind to a dropdown list
    /// </summary>
    /// <param name="c"></param>
    /// <param name="dataSource"></param>
    /// <param name="textField"></param>
    /// <param name="valueField"></param>
    /// ------------------------------------------------------------------
    protected void BindToList(UIFieldSearchableDropDownList c, ODashboard dashboard, ODashboardField field)
    {
        parameterList = ConstructParameters(dashboard);
        LogicLayer.Parameter[] paramList = parameterList.ToArray();
        //LogicLayer.Parameter[] paramList = ConstructParameters(dashboard).ToArray();
        try
        {
            using (Connection conn = new Connection())
            {
                bool insertBlank = (field.IsInsertBlank == 1);

                if (field.IsPopulatedByQuery == 1)
                    c.Bind(
                        Analysis.DoQuery(field.ListQuery, ConstructParameters(dashboard).ToArray()), field.DataTextField, field.DataValueField, insertBlank);
                //Rachel. Allow to populate using C#
                else if (field.IsPopulatedByQuery == 2)
                {
                    if (field.CSharpMethodName != null)
                    {
                        DataTable dt = GetDataTableFromCMethod(field.CSharpMethodName, paramList);
                        if (dt != null)
                            c.Bind(dt, field.DataTextField, field.DataValueField, insertBlank);
                    }
                }
                else
                    c.Bind(field.ConstructTextValueTable(), "Text", "Value", insertBlank);
            }
        }
        catch (Exception ex)
        {
            Helpers.LogException(ex, this.Request);
            if (Helpers.IsDebug())
            {
                c.ErrorMessage = String.Format(Resources.Errors.Report_PopulateQueryError, ex.Message);
            }
            else
            {
                c.ErrorMessage = Resources.Errors.Report_PopulateQueryErrorNonDebug;
            }
        }
    }

    /// ------------------------------------------------------------------
    /// <summary>
    /// Bind to a dropdown list
    /// </summary>
    /// <param name="c"></param>
    /// <param name="dataSource"></param>
    /// <param name="textField"></param>
    /// <param name="valueField"></param>
    /// ------------------------------------------------------------------
    protected void BindToList(UIFieldDropDownList c, ODashboard dashboard, ODashboardField field)
    {
        LogicLayer.Parameter[] paramList = ConstructParameters(dashboard).ToArray();
        try
        {
            using (Connection conn = new Connection())
            {
                bool insertBlank = (field.IsInsertBlank == 1);

                if (field.IsPopulatedByQuery == 1)
                    c.Bind(
                        Analysis.DoQuery(field.ListQuery, ConstructParameters(dashboard).ToArray()), field.DataTextField, field.DataValueField, insertBlank);
                //Rachel. Allow to populate using C#
                else if (field.IsPopulatedByQuery == 2)
                {
                    if (field.CSharpMethodName != null)
                    {
                        DataTable dt = GetDataTableFromCMethod(field.CSharpMethodName, paramList);
                        if (dt != null)
                            c.Bind(dt, field.DataTextField, field.DataValueField, insertBlank);
                    }
                }
                else
                    c.Bind(field.ConstructTextValueTable(), "Text", "Value", insertBlank);
            }
        }
        catch (Exception ex)
        {
            Helpers.LogException(ex, this.Request);
            if (Helpers.IsDebug())
            {
                c.ErrorMessage = String.Format(Resources.Errors.Report_PopulateQueryError, ex.Message);
            }
            else
            {
                c.ErrorMessage = Resources.Errors.Report_PopulateQueryErrorNonDebug;
            }
        }
    }

    /// ------------------------------------------------------------------
    /// <summary>
    /// Bind to a radio list
    /// </summary>
    /// <param name="c"></param>
    /// <param name="dataSource"></param>
    /// <param name="textField"></param>
    /// <param name="valueField"></param>
    /// ------------------------------------------------------------------
    protected void BindToList(UIFieldRadioList c, ODashboard dashboard, ODashboardField field)
    {
        LogicLayer.Parameter[] paramList = ConstructParameters(dashboard).ToArray();
        try
        {
            using (Connection conn = new Connection())
            {
                if (field.IsPopulatedByQuery == 1)
                    c.Bind(
                        Analysis.DoQuery(field.ListQuery, ConstructParameters(dashboard).ToArray()), field.DataTextField, field.DataValueField);
                //Rachel. Allow to populate using C#
                else if (field.IsPopulatedByQuery == 2)
                {
                    if (field.CSharpMethodName != null)
                    {
                        DataTable dt = GetDataTableFromCMethod(field.CSharpMethodName, paramList);
                        if (dt != null)
                            c.Bind(dt, field.DataTextField, field.DataValueField);
                    }
                }
                else
                    c.Bind(field.ConstructTextValueTable(), "Text", "Value");
            }
        }
        catch (Exception ex)
        {
            Helpers.LogException(ex, this.Request);
            if (Helpers.IsDebug())
            {
                c.ErrorMessage = String.Format(Resources.Errors.Report_PopulateQueryError, ex.Message);
            }
            else
            {
                c.ErrorMessage = Resources.Errors.Report_PopulateQueryErrorNonDebug;
            }
        }
    }

    List<String> controlTextList;
    List<String> parameterDisplayStrings;


    /// <summary>
    /// Constructs a single parameter based on the DashboardField and the UIFieldBase objects.
    /// </summary>
    /// <param name="control"></param>
    /// <param name="field"></param>
    /// <returns></returns>
    protected LogicLayer.Parameter ConstructParameter(UIFieldBase control, ODashboardField field)
    {
        // create the OdbcParameter for single value controls
        //
        LogicLayer.Parameter p = LogicLayer.Parameter.New(field.ControlIdentifier, DbType.String, 0, null, field.ControlCaption);
        if (field.DataType == (int)ReportDataTypeEnum.String)
        {
            control.ValidationDataType = ValidationDataType.String;
            p.DataType = DbType.String;//tessa
            p.Size = 255;
        }
        else if (field.DataType == (int)ReportDataTypeEnum.Integer)
        {
            control.ValidationDataType = ValidationDataType.Integer;
            p.DataType = DbType.Int32;//tessa change from System.Data.Odbc.OdbcType.Int to DbType.Int32
            p.Size = 4;
        }
        else if (field.DataType == (int)ReportDataTypeEnum.Decimal)
        {
            control.ValidationDataType = ValidationDataType.Currency;
            p.DataType = DbType.Decimal; //tessa
            p.Size = 8;
        }
        else if (field.DataType == (int)ReportDataTypeEnum.Double)
        {
            control.ValidationDataType = ValidationDataType.Double;
            p.DataType = DbType.Double;//tessa
            p.Size = 8;
        }
        else if (field.DataType == (int)ReportDataTypeEnum.DateTime)
        {
            control.ValidationDataType = ValidationDataType.Date;
            p.DataType = DbType.DateTime;//Tessa
            p.Size = 8;
        }
        else if (field.DataType == 5)
        {
            control.ValidationDataType = ValidationDataType.String;
            p.DataType = DbType.Guid;//tessa
            p.Size = 8;
        }

        return p;
    }

    /// ------------------------------------------------------------------
    /// <summary>
    /// Construct the parameters to be passed in to the SQL for doing
    /// query.
    /// </summary>
    /// <param name="report"></param>
    /// <returns></returns>
    /// ------------------------------------------------------------------
    protected List<LogicLayer.Parameter> ConstructParameters(ODashboard dashboard)
    {
        List<LogicLayer.Parameter> paramList = new List<LogicLayer.Parameter>();
        controlTextList = new List<String>();
        parameterDisplayStrings = new List<String>();
        int i=0;
        foreach (ODashboardField field in dashboard.GetDashboardFieldsInOrder())
        {
            Control c = this.FindControl("_" + DrillCount + field.ControlIdentifier);

            if (c is UIFieldBase && !(c is UIFieldTreeList))
            {
                UIFieldBase fieldBase = c as UIFieldBase;
                LogicLayer.Parameter p = ConstructParameter(fieldBase, field);
                p.Value = ConvertToType(p.DataType, fieldBase.ControlValue);

                if (fieldBase.ControlText != null)
                    controlTextList.Add(fieldBase.ControlText);
                else if (p.Value != null)
                    controlTextList.Add(p.Value.ToString());

                parameterDisplayStrings.Add(p.ParameterDisplay);
                paramList.Add(p);
            }
        }

        paramList.Add(LogicLayer.Parameter.New("UserID", System.Data.DbType.Guid, 16, AppSession.User.ObjectID.Value));
        paramList.Add(LogicLayer.Parameter.New("DashboardID", System.Data.DbType.Guid, 16, dashboard.ObjectID.Value));
        return paramList;
    }

    /// <summary>
    /// Convert given object to a list of object and convert each item from DbType to Type
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="itemType"></param>
    /// <returns></returns>
    protected List<object> ConvertObjectToList(object obj, DbType itemType)
    {
        List<object> genericList = new List<object>();

        if (obj is IList && obj.GetType().IsGenericType)
        {
            IList nonGenericList = (IList)obj;

            if (nonGenericList != null)
            {
                for (int x = 0; x < nonGenericList.Count; x++)
                {
                    genericList.Add(ConvertToType(itemType, nonGenericList[x]));
                    //genericList.Add(nonGenericList[x]);
                }
            }
        }
        return genericList;
    }


    /// ------------------------------------------------------------------
    /// <summary>
    /// Convert object from the string type to the specified type.
    /// </summary>
    /// <param name="type"></param>
    /// <param name="x"></param>
    /// <returns></returns>
    /// ------------------------------------------------------------------
    protected object ConvertToType(DbType type, object x)
    {
        try
        {
            if (type == DbType.String)
                return x.ToString();
            else if (type == DbType.Int32)
                return Convert.ToInt32(x.ToString());
            else if (type == DbType.Decimal)
                return Convert.ToDecimal(x.ToString());
            else if (type == DbType.Double)
                return Convert.ToDouble(x.ToString());
            else if (type == DbType.DateTime)
                return Convert.ToDateTime(x.ToString());
            else if (type == DbType.Guid)
                return new Guid(x.ToString());
        }
        catch
        {
            return DBNull.Value;
        }
        return DBNull.Value;
    }


    public DataTable GetDataTableFromCMethod(string MethodName, LogicLayer.Parameter[] paramList)
    {
        using (Connection c = new Connection())
        {
            object result = Analysis.InvokeMethod(MethodName, paramList);
            if (result is DataSet)
            {
                return ((DataSet)result).Tables[0];
            }
            if (result is DataTable)
            {
                return (DataTable)result;
            }
            return null;
        }
    }

    protected void BindData(string arg)
    {
        if (string.IsNullOrEmpty(arg))
            return;

        Hashtable result = new Hashtable();
        string[] s1 = new string[] { "&" };
        string[] s2 = new string[] { "=" };
        string[] args = arg.Split(s1, StringSplitOptions.None);
        foreach (string s in args)
        {
            string[] temp = s.Split(s2, StringSplitOptions.None);
            if (!result.ContainsKey(temp[0]))
                result[HttpUtility.UrlDecode(temp[0])] = "";

            result[HttpUtility.UrlDecode(temp[0])] += HttpUtility.UrlDecode(temp[1]).Trim();
        }

        foreach (string key in result.Keys)
        {
            var c = this.FindControl(key);
            if (c != null)
                if (c.Parent is UIFieldBase)
                {
                    UIFieldBase p = (UIFieldBase)c.Parent;
                    p.ControlValue = result[key];
                }
                else if (c.Parent != null
                    && c.Parent.Parent is UIFieldBase)
                {
                    UIFieldBase p = (UIFieldBase)c.Parent.Parent;
                    p.ControlValue = result[key];
                }
                else if (c.Parent != null
                    && c.Parent.Parent != null
                    && c.Parent.Parent.Parent is UISearchableDropDownList
                    && key.Contains("_textItem"))
                {
                    UISearchableDropDownList p = (UISearchableDropDownList)c.Parent.Parent.Parent;
                    p.SelectedValue = p.Items.FindByText((string)result[key]).Value;
                }
        }
    }

    Hashtable dashboardColumnMappings = new Hashtable();
    protected DataTable GetDataTable(ODashboard dashboard)
    {
        DataTable dt = null;
        foreach (ODashboardColumnMapping dashboardColumnMapping in dashboard.DashboardColumnMappings)
            dashboardColumnMappings[dashboardColumnMapping.ColumnName] = dashboardColumnMapping;
        // Runs the query against the database and gets
        // the result as a DataTable.
        //

        //List<LogicLayer.Parameter> paramList = ConstructParameters(dashboard);
        List<LogicLayer.Parameter> paramList = new List<LogicLayer.Parameter>();

        string paramString = this.Request.Form["params"];
        if (paramString != "null" && paramString != null)
            paramList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<LogicLayer.Parameter>>(paramString);
        else
            paramList = ConstructParameters(dashboard);     // creates an empty parameter list

        if (dashboard.UseCSharpQuery == 0 || dashboard.UseCSharpQuery == null)
        {
            dt = Analysis.DoQuery(dashboard.DashboardQuery, paramList.ToArray());
        }
        else
        {
            object dataSource = Analysis.InvokeMethod(dashboard.CSharpMethodName, paramList.ToArray());
            if (dataSource is DataTable)
                dt = (DataTable)dataSource;
            else if (dataSource is DataSet)
                dt = ((DataSet)dataSource).Tables[0];

        }

        MapReportDataFields(dt);

        return dt;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="dt"></param>
    protected void MapReportDataFields(DataTable dt)
    {
        int totalColumns = dt.Columns.Count;
        for (int i = 0; i < totalColumns; i++)
        {
            string clName = dt.Columns[i].ColumnName;
            ODashboardColumnMapping column = dashboardColumnMappings[clName] as ODashboardColumnMapping;

            if (column != null && dt.Columns[i].DataType == typeof(String)
                && column.ResourceName != null && column.ResourceName.Trim() != "")
            {
                foreach (DataRow dr in dt.Rows)
                {
                    string resourceAssemblyName = column.ResourceAssemblyName;
                    Assembly asm;
                    if (resourceAssemblyName.Trim() == "")
                        asm = Assembly.Load("App_GlobalResources");
                    else
                        asm = Assembly.Load(resourceAssemblyName);

                    if (asm != null)
                    {
                        ResourceManager rm = new ResourceManager(column.ResourceName, asm);
                        if (rm != null && dr[clName] != null && dr[clName].ToString() != "")
                        {
                            string a = dr[clName].ToString();
                            string translatedText = rm.GetString(a, System.Threading.Thread.CurrentThread.CurrentUICulture);
                            if (translatedText != null && translatedText != "")
                                dr[clName] = translatedText;
                        }
                    }
                }
            }
        }
    }

    protected void BindDrillDownData(ODashboard dashboard)
    {
        object t, t2;
        foreach (ODashboardColumnLinking l in dashboard.DashboardColumnLinkings)
        {
            foreach (ODashboardColumnLinkingField f in l.DashboardColumnLinkingFields)
            {
                switch (f.SourceType)
                {
                    case DashboardColumnLinkFieldSourceType.FromDataColumn:
                        if (DrillDownArguments != null)
                        {
                            t2 = PanelControl.FindControl("_" + DrillCount + f.TargetDashboardControlIdentifier);
                            ((UIFieldBase)t2).ControlValue = DrillDownArguments[f.SourceNameOrValue];
                        }
                        break;
                    case DashboardColumnLinkFieldSourceType.FromFilter:
                        t = PanelControl.FindControl("_" + (DrillCount - 1) + f.SourceNameOrValue);
                        t2 = PanelControl.FindControl("_" + DrillCount + f.TargetDashboardControlIdentifier);
                        ((UIFieldBase)t2).ControlValue = ((UIFieldBase)t).ControlValue;
                        break;
                    case DashboardColumnLinkFieldSourceType.Static:
                        t2 = PanelControl.FindControl("_" + DrillCount + f.TargetDashboardControlIdentifier);
                        ((UIFieldBase)t2).ControlValue = f.SourceNameOrValue;
                        break;
                }
            }
        }
    }


    protected void ApplyButton_Click(object sender, EventArgs e)
    {
        refreshChart = true;
    }

    protected void DrillButton_Click(object sender, EventArgs e)
    {
        ODashboard dashboard = TablesLogic.tDashboard.Load(GetRequestDashboardID());
        if (DrillCount < dashboard.DashboardColumnLinkings.Count)    // If chart has drill feature
        {
            DrillIDs.Add(dashboard.DashboardColumnLinkings[DrillCount].LinkedDashboardID.Value);

            DrillDownArguments = new Hashtable();
            foreach (string s in DrillParameterTextBox.Text.Split('#'))
            {
                if (!string.IsNullOrEmpty(s))
                {
                    string[] temp = s.Split(':');
                    DrillDownArguments[temp[0]] = temp[1];
                }
            }

            DrillCount++;
            refreshChart = true;
        }

    }

    protected void BackButton_Click(object sender, EventArgs e)
    {
        if (DrillCount > 0) // If its inside a drill
        {
            DrillIDs.RemoveAt(DrillIDs.Count - 1);

            DrillCount--;

            refreshChart = true;
        }
    }

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="../../../css/v7-chart.css" rel="stylesheet" type="text/css" />
    <script>

        function hideFilter(e) {
            $('#DashboardFilterBack').removeClass('mdl-dialog-backdrop-visible');
            setTimeout(function () {
                $('#DashboardFilterBack').hide();
            }, 300);
            
        }

        function showFilter(e) {
            $('#DashboardFilterBack').show();
            setTimeout(function () {
                $('#DashboardFilterBack').addClass('mdl-dialog-backdrop-visible');
                $('#DashboardFilterContent').scrollTop(selectedIndex * 49);
            }, 1);
        }


        // temporary workaround to reload the chart
        function reloadChart() {
            $('.apply-btn').click();
        }

        //Leon: To write this function to simplify
        function getPlainChart() {
            var d3 = Plotly.d3;

            var WIDTH_IN_PERCENT_OF_PARENT = 100,
                HEIGHT_IN_PERCENT_OF_PARENT = 100;

            var gd3 = d3.select('body')
                .append('div')
                .style({
                    width: WIDTH_IN_PERCENT_OF_PARENT + 'vw',
                    'margin-left': (100 - WIDTH_IN_PERCENT_OF_PARENT) / 2 + 'vw',

                    height: HEIGHT_IN_PERCENT_OF_PARENT + 'vh',
                    'margin-top': (100 - HEIGHT_IN_PERCENT_OF_PARENT) / 2 + 'vh',
                });

            return gd3.node();
        }

    </script>
    <style>
        #plotlyChart .js-plotly-plot {
            margin: 20px 10px 10px 30px !important;
            height: 90% !important;
            width: 90% !important;
        }

        #chart_options {
            top: 10px;
            left: 10px;
            position: absolute;
            z-index: 1;
            width: calc(100vw - 20px);
        }

        html, body, form
        {
            height: 100%;
        }

        #loading-gif {
            position: absolute;
            display: block;
            z-index: 4;
            width: 40px;
            left: calc(50vw - 20px);
            top: calc(50vw - 20px);
        }
    </style>
</head>
<body class="bg-color-white">
    <script type="text/javascript" src="../../../scripts/plotly.min.js"></script>
    <img id="loading-gif" src="../../../images/loading.gif" alt="chart is loading">

    <form id="Form1" runat="server">

        <div id="DashboardFilterBack" class="mdl-dialog-backdrop" style="display:none" onclick="hideFilter(event); return false;">                        
            <div id="DashboardFilterDialog" class="mdl-dialog mdl-dialog-select" onclick="stopPropagation(event); return false;">  
                <div class="mdl-dialog__title-container"> 
                    <h6 id="DashboardFilterTitle" class="mdl-dialog__title">Select</h6>           
                    <ui:UIButton runat="server" ID="HideFilterButton" ButtonColor="White"  ButtonType="RoundMiniFlat" IconCssClass="material-icons" IconText="close" DoPostBack="false" OnClickScript="hideFilter();" />
                </div>
                <div id="DashboardFilterContentContainer" class="dialog-main">                                                                     
                    <ui:UIPanel runat="server" ID="PanelControl">

                    </ui:UIPanel>
                </div>                                                                                                                                             
                <div class="mdl-dialog__actions">        
                    <ui:UIButton runat="server" id="ApplyButton" OnClick="ApplyButton_Click" ButtonColor="Primary"  ButtonType="NormalRaised" Text="Apply" />
                </div>                                                                                                                                             
            </div>                                                                                                                                                 
        </div>                                                                                                                                                   

        <div style="" id="chart_options">
            <div style="float:left">
                <ui:UIButton runat="server" Text="Back" ID="BackButton" OnClick="BackButton_Click" ButtonColor="Default"  ButtonType="RoundMiniFlat" IconCssClass="material-icons" IconText="chevron_left" />
            </div>
            <div style="float:right">
                <ui:UIButton runat="server" ID="FilterButton" ButtonColor="Default"  ButtonType="RoundMiniFlat" IconCssClass="material-icons" IconText="menu" DoPostBack="false" OnClickScript="showFilter();" />
            </div>
            <div style="clear:both"></div>
        </div>
        <div id="plotlyChart"></div>

        <div style="display:none">
            <asp:Button ID="DrillButton" runat="server" Text="Drill" OnClick="DrillButton_Click" CssClass="filter-hidden" />
            <asp:TextBox ID="DrillParameterTextBox" runat="server" CssClass="filter-hidden"></asp:TextBox>
        </div>
    </form>

    <script>

        function fadeInSequence(elements, duration) {
            if (elements.length > 0) {
                console.log('duration: ' + duration);
                elements.eq(0).fadeIn(duration, function () {
                    elements.splice(0, 1);
                    fadeInSequence(elements, duration);
                });
            }
        }

    </script>
</body>
</html>
