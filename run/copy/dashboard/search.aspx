<%@ Page Language="C#" Theme="Corporate" Inherits="PageBase" Culture="auto" meta:resourcekey="PageResource1"
    UICulture="auto" %>

<%@ Register Src="~/components/menu.ascx" TagPrefix="web" TagName="menu" %>
<%@ Register Src="~/components/objectsearchpanel.ascx" TagPrefix="web" TagName="search" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="Anacle.DataFramework" %>

<script runat="server">
    protected void panel_Search(objectSearchPanel.SearchEventArgs e)
    {
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        if (!IsPostBack)
        {
            foreach (JSChartType t in Enum.GetValues(typeof(JSChartType)).Cast<JSChartType>())
            {
                ListItem i = new ListItem();
                i.Text = t.ToString();
                i.Value = "" + (int)t;
                this.dropDashboardType.Control.Items.Add(i);
            }
        }
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Anacle.EAM</title>
    <meta http-equiv="pragma" content="no-cache" />

</head>
<body class="body-search-bg">
    <form id="form2" runat="server">
        <ui:UIObjectPanel runat="server" ID="panelMain" meta:resourcekey="panelMainResource" >
            <web:search runat="server" ID="panel" Caption="Dashboard" GridViewID="gridResults"
                SearchType="ObjectQuery" SearchTextBoxVisible="true" SearchTextBoxPropertyNames="ObjectName"
                SearchTextBoxHint="what is the dashboard name?" AdvancedSearchPanelID="AdvancedSearchPanel"
                BaseTable="tDashboard" OnSearch="panel_Search"  meta:resourcekey="panelResource">
            </web:search>
                <ui:UIDialogBox  runat="server" ID="AdvancedSearchDialogBox">
                <ui:UIPanel runat="server" ID="AdvancedSearchPanel" meta:resourcekey="AdvancedSearchPanelResource" >
                    <ui:UIFieldTextBox runat='server' ID='ObjectName' PropertyName="ObjectName" Caption="Dashboard Name"
                        ToolTip="The dashboard name." MaxLength="255" meta:resourcekey="ObjectNameResource" />
                    <ui:UIFieldDropDownList runat='server' ID='dropDashboardType' PropertyName="DashboardType"
                        Caption="Dashboard Type" ToolTip="The dashboard type."
                        meta:resourcekey="dropDashboardTypeResource">
                        <Items>
                            <asp:ListItem Value="" meta:resourcekey="dropDashboardTypeListItemAnyResource" ></asp:ListItem>
                        </Items>
                    </ui:UIFieldDropDownList>
                </ui:UIPanel>
                </ui:UIDialogBox>
                <ui:UIGridView runat="server" ID="gridResults" KeyName="ObjectID" meta:resourcekey="gridResultsResource"
                    Width="100%" SortExpression="ObjectName">
                    <Columns>
                        <ui:UIGridViewButtonColumn ImageUrl="~/images/delete.gif"
                            CommandName="DeleteObject" HeaderText="" ConfirmText="Are you sure you wish to delete this item?"
                            meta:resourcekey="gridResultsUIGridViewButtonColumnDeleteObjectResource">
                        </ui:UIGridViewButtonColumn>
                        <ui:UIGridViewBoundColumn PropertyName="ObjectName" HeaderText="Dashboard Name" DisallowFromColumnChoosing="true"
                            meta:resourcekey="gridResultsUIGridViewBoundColumnObjectNameResource">
                        </ui:UIGridViewBoundColumn>
                        <ui:UIGridViewBoundColumn PropertyName="DashboardTypeString" HeaderText="Dashboard Type"
                            meta:resourcekey="gridResultsUIGridViewBoundColumnDashboardTypeStringResource">
                        </ui:UIGridViewBoundColumn>
                    </Columns>
                    <Commands>
                        <ui:UIGridViewCommand CommandText="Delete Selected" ConfirmText="Are you sure you wish to delete the selected items?"
                            ImageUrl="~/images/delete.gif" CommandName="DeleteObject" meta:resourcekey="gridResultsUIGridViewCommandDeleteSelectedResource">
                        </ui:UIGridViewCommand>
                    </Commands>
                </ui:UIGridView>
        </ui:UIObjectPanel>
    </form>
</body>
</html>
