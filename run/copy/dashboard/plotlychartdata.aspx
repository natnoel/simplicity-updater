﻿<%@ Page Language="C#" Theme="Corporate" AutoEventWireup="true" %>

<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Reflection" %>
<%@ Import Namespace="System.Resources" %>
<%@ Import Namespace="LogicLayer" %>
<%@ Import Namespace="System.Runtime.Serialization" %>
<%@ Import Namespace="System.Runtime.Serialization.Json" %>
<%@ Import Namespace="Newtonsoft.Json.Linq" %>
<%@ Import Namespace="Newtonsoft.Json" %>

<script runat="server">      

    public Guid dashboardID;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        string[] args = Security.Decrypt(Request["ID"]).Split(':');
        dashboardID = new Guid(args[0]);
        //Response.Write("OnInit<br>");
        //writeDrillData();
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (!IsPostBack)
        {
            bool completed = false;
            //try
            {

                if (Request["drillcount"] != null && Request["drillcount"] != "null")
                {
                    int DrillCount = Int32.Parse(Request["drillcount"]);

                    if (DrillCount > 0)
                    {
                        ODashboard dashboard = TablesLogic.tDashboard.Load(dashboardID);
                        dashboardID = dashboard.DashboardColumnLinkings[DrillCount - 1].LinkedDashboardID.Value;
                    }
                }

                Response.Write(getPlotlyDataAndLayoutJSON());
                completed = true;
                Response.End();
            }
            /*catch(Exception ex)
            {
                if (!completed)
                {
                    Response.Write("{\"error\" : \"ERR\"}");
                    Response.End();
                }
            }*/
        }
    }

    /// <summary>
    /// Get the dashboard ID from the request query string.
    /// </summary>
    /// <returns></returns>
    protected Guid GetRequestDashboardID()
    {
        return dashboardID;
    }

    PlotlyChart plotlyChart;

    public string getPlotlyDataAndLayoutJSON()
    {
        UpdateDashboard(GetRequestDashboardID());

        return "{\"data\":" + plotlyChart.getPlotlyJSON() + ", \"layout\":" + plotlyChart.getPlotlyLayoutJSON() + "}";
    }


    List<String> controlTextList;
    List<String> parameterDisplayStrings;

    /// <summary>
    /// Convert given object to a list of object and convert each item from DbType to Type
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="itemType"></param>
    /// <returns></returns>
    protected List<object> ConvertObjectToList(object obj, DbType itemType)
    {
        List<object> genericList = new List<object>();

        if (obj is IList && obj.GetType().IsGenericType)
        {
            IList nonGenericList = (IList)obj;

            if (nonGenericList != null)
            {
                for (int x = 0; x < nonGenericList.Count; x++)
                {
                    genericList.Add(ConvertToType(itemType, nonGenericList[x]));
                    //genericList.Add(nonGenericList[x]);
                }
            }
        }
        return genericList;
    }


    /// ------------------------------------------------------------------
    /// <summary>
    /// Convert object from the string type to the specified type.
    /// </summary>
    /// <param name="type"></param>
    /// <param name="x"></param>
    /// <returns></returns>
    /// ------------------------------------------------------------------
    protected object ConvertToType(DbType type, object x)
    {
        try
        {
            if (type == DbType.String)
                return x.ToString();
            else if (type == DbType.Int32)
                return Convert.ToInt32(x.ToString());
            else if (type == DbType.Decimal)
                return Convert.ToDecimal(x.ToString());
            else if (type == DbType.Double)
                return Convert.ToDouble(x.ToString());
            else if (type == DbType.DateTime)
                return Convert.ToDateTime(x.ToString());
            else if (type == DbType.Guid)
                return new Guid(x.ToString());
        }
        catch
        {
            return DBNull.Value;
        }
        return DBNull.Value;
    }


    public DataTable GetDataTableFromCMethod(string MethodName, LogicLayer.Parameter[] paramList)
    {
        using (Connection c = new Connection())
        {
            object result = Analysis.InvokeMethod(MethodName, paramList);
            if (result is DataSet)
            {
                return ((DataSet)result).Tables[0];
            }
            if (result is DataTable)
            {
                return (DataTable)result;
            }
            return null;
        }
    }

    protected void UpdateDashboard(Guid dashboardID)
    {
        ODashboard dashboard = TablesLogic.tDashboard.Load(dashboardID);
        DataTable dt = GetDataTable(dashboard);

        plotlyChart = new PlotlyChart();
        plotlyChart.SeriesByColumns = dashboard.SeriesByColumns == 1;
        plotlyChart.showLegend = dashboard.ShowLegend == 1;
        plotlyChart.SeriesColumn = dashboard.SeriesColumnName;
        plotlyChart.ValueColumn = dashboard.ValueColumnName;
        plotlyChart.CatagoryColumn = dashboard.CatagoryColumnName;
        plotlyChart.DataSource = dt;
        plotlyChart.jsChartType = (JSChartType)dashboard.DashboardType;
        plotlyChart.DrillColumns = new List<string>();

        if (dashboard.DashboardColors.Count > 0)
        {
            plotlyChart.ColorScheme = dashboard.GetColorScheme();
            plotlyChart.useCustomColors = true;
        }
        plotlyChart.MaxAxisLabelLength = dashboard.TrancateAxisLabels ?? 0;
        plotlyChart.DrillColumns = new List<string>();
        int count = 0;
        foreach (ODashboardColumnLinking l in dashboard.DashboardColumnLinkings)
        {
            foreach (ODashboardColumnLinkingField f in l.DashboardColumnLinkingFields)
            {
                count++;
                if (f.SourceType == LogicLayer.DashboardColumnLinkFieldSourceType.FromDataColumn)
                {
                    plotlyChart.DrillColumns.Add(f.SourceNameOrValue);
                }
            }
        }
        if (count == 0)
            plotlyChart.DrillColumns = null;
    }


    protected void BindData(string arg)
    {
        if (string.IsNullOrEmpty(arg))
            return;

        Hashtable result = new Hashtable();
        string[] s1 = new string[] { "&" };
        string[] s2 = new string[] { "=" };
        string[] args = arg.Split(s1, StringSplitOptions.None);
        foreach (string s in args)
        {
            string[] temp = s.Split(s2, StringSplitOptions.None);
            if (!result.ContainsKey(temp[0]))
                result[HttpUtility.UrlDecode(temp[0])] = "";

            result[HttpUtility.UrlDecode(temp[0])] += HttpUtility.UrlDecode(temp[1]).Trim();
        }

        foreach (string key in result.Keys)
        {
            var c = this.FindControl(key);
            if (c != null)
                if (c.Parent is UIFieldBase)
                {
                    UIFieldBase p = (UIFieldBase)c.Parent;
                    p.ControlValue = result[key];
                }
                else if (c.Parent != null
                    && c.Parent.Parent is UIFieldBase)
                {
                    UIFieldBase p = (UIFieldBase)c.Parent.Parent;
                    p.ControlValue = result[key];
                }
                else if (c.Parent != null
                    && c.Parent.Parent != null
                    && c.Parent.Parent.Parent is UISearchableDropDownList
                    && key.Contains("_textItem"))
                {
                    UISearchableDropDownList p = (UISearchableDropDownList)c.Parent.Parent.Parent;
                    p.SelectedValue = p.Items.FindByText((string)result[key]).Value;
                }
        }
    }

    Hashtable dashboardColumnMappings = new Hashtable();
    protected DataTable GetDataTable(ODashboard dashboard)
    {
        DataTable dt = null;
        foreach (ODashboardColumnMapping dashboardColumnMapping in dashboard.DashboardColumnMappings)
            dashboardColumnMappings[dashboardColumnMapping.ColumnName] = dashboardColumnMapping;
        // Runs the query against the database and gets
        // the result as a DataTable.
        //

        //List<LogicLayer.Parameter> paramList = ConstructParameters(dashboard);
        List<LogicLayer.Parameter> paramList = new List<LogicLayer.Parameter>();

        string paramString = this.Request.Form["params"];
        if (paramString != "null" && paramString != null)
            paramList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<LogicLayer.Parameter>>(paramString);

        if (dashboard.UseCSharpQuery == 0 || dashboard.UseCSharpQuery == null)
        {
            dt = Analysis.DoQuery(dashboard.DashboardQuery, paramList.ToArray());
        }
        else
        {
            object dataSource = Analysis.InvokeMethod(dashboard.CSharpMethodName, paramList.ToArray());
            if (dataSource is DataTable)
                dt = (DataTable)dataSource;
            else if (dataSource is DataSet)
                dt = ((DataSet)dataSource).Tables[0];

        }

        MapReportDataFields(dt);

        return dt;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="dt"></param>
    protected void MapReportDataFields(DataTable dt)
    {
        int totalColumns = dt.Columns.Count;
        for (int i = 0; i < totalColumns; i++)
        {
            string clName = dt.Columns[i].ColumnName;
            ODashboardColumnMapping column = dashboardColumnMappings[clName] as ODashboardColumnMapping;

            if (column != null && dt.Columns[i].DataType == typeof(String)
                && column.ResourceName != null && column.ResourceName.Trim() != "")
            {
                foreach (DataRow dr in dt.Rows)
                {
                    string resourceAssemblyName = column.ResourceAssemblyName;
                    Assembly asm;
                    if (resourceAssemblyName.Trim() == "")
                        asm = Assembly.Load("App_GlobalResources");
                    else
                        asm = Assembly.Load(resourceAssemblyName);

                    if (asm != null)
                    {
                        ResourceManager rm = new ResourceManager(column.ResourceName, asm);
                        if (rm != null && dr[clName] != null && dr[clName].ToString() != "")
                        {
                            string a = dr[clName].ToString();
                            string translatedText = rm.GetString(a, System.Threading.Thread.CurrentThread.CurrentUICulture);
                            if (translatedText != null && translatedText != "")
                                dr[clName] = translatedText;
                        }
                    }
                }
            }
        }
    }


</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
</head>
<body>
</body>
</html>
