﻿<%@ Page Language="C#" Inherits="PageBase" Theme="Corporate" AutoEventWireup="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%--
<script runat="server">
    
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (!IsPostBack)
        {
            webDashboard.DashboardID = GetRequestDashboardID();
            webDashboard.UniqueKey = GetRequestDashboardUniqueID();
        }
    }

    /// <summary>
    /// Get the report ID from the request query string.
    /// </summary>
    /// <returns></returns>
    protected Guid GetRequestDashboardID()
    {
        string[] args = Security.Decrypt(Request["ID"]).Split(':');
        if (args.Length > 0)
            return new Guid(args[0]);
        return Guid.Empty;
    }

    /// <summary>
    /// Get the report ID from the request query string.
    /// </summary>
    /// <returns></returns>
    protected String GetRequestDashboardUniqueID()
    {
        string[] args = Security.Decrypt(Request["ID"]).Split(':');
        if (args.Length > 1)
            return args[1];
        return "";
    }
    
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body class="bg-color-white">
    <form runat="server">
    <ui:UIPanel runat="server" ID="Panel">
        <div class="dashboard" style="display: none;">
            <ui:UIButton runat="server" ID="buttonReturn" Text="Go Back" Visible="false" CausesValidation="false"
                ImageUrl="../images/right-black-12.png" AlwaysEnabled="true" CssClass="btnblue" />
            <web:webpartDashboard runat="server" ID="webDashboard" OnDrillDown="webDashboard_DrillDown">
            </web:webpartDashboard>
        </div>
    </ui:UIPanel>
    </form>

    <script src="../../../scripts/maphighlight/jquery.maphilight.js"type="text/javascript"></script>

    <script type="text/javascript">


        function initmhl() {
            $('.dnc').maphilight({
                fill: true,
                fillColor: 'eeeeee',
                fillOpacity: 0.3,
                stroke: true,
                strokeColor: 'e51400',
                strokeOpacity: 0.7,
                strokeWidth: 2,
                fade: true,
                alwaysOn: false,
                neverOn: false,
                groupBy: false,
                wrapClass: false,
                shadow: true,
                shadowX: 0,
                shadowY: 0,
                shadowRadius: 6,
                shadowColor: '000000',
                shadowOpacity: 0.8,
                shadowPosition: 'inside',
                shadowFrom: false
            });
        }
    </script>

    <script src="../../../scripts/jtip.js" type='text/javascript'></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.dashboard').fadeIn(500);
            initmhl();
        });

        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function() {
            $('.dashboard').show();
            $('.dashboard').fadeIn(500);
            initmhl();
        });
    </script>

</body>
</html>
--%>