﻿<%@ Page Language="C#" Inherits="PageBase" Theme="Corporate" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Reflection" %>
<%@ Import Namespace="System.Resources" %>
<%@ Import Namespace="LogicLayer" %>

<script runat="server">
    public int DrillCount
    {
        get
        {
            if (ViewState["DrillCount"] == null)
                ViewState["DrillCount"] = 0;

            return (int)ViewState["DrillCount"];
        }
        set
        {
            ViewState["DrillCount"] = value;
        }
    }

    public List<Guid> DrillIDs
    {
        get
        {
            if (ViewState["DrillIDs"] == null)
                ViewState["DrillIDs"] = new List<Guid>();

            return (List<Guid>)ViewState["DrillIDs"];
        }
        set
        {
            ViewState["DrillIDs"] = value;
        }
    }

    public List<string> FilterData
    {
        get
        {
            if (ViewState["FilterData"] == null)
                ViewState["FilterData"] = new List<string>();

            return (List<string>)ViewState["FilterData"];
        }
        set
        {
            ViewState["FilterData"] = value;
        }
    }

    public string CurrentFilterValues
    {
        get
        {
            return (string)Session[Security.Decrypt(this.ViewStateKey.Text)];
        }
        set
        {
            Session[Security.Decrypt(this.ViewStateKey.Text)] = value;
        }
    }

    public Hashtable DrillDownArguments
    {
        get
        {
            return (Hashtable)Session[Security.Decrypt(this.ViewStateKey.Text) + "_DRILL"];
        }
        set
        {
            Session[Security.Decrypt(this.ViewStateKey.Text) + "_DRILL"] = value;
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (!IsPostBack)
        {
            string[] args = Security.Decrypt(Request["ID"]).Split(':');
            DrillIDs.Add(new Guid(args[0]));
            ODashboard dashboard = TablesLogic.tDashboard.Load(GetRequestDashboardID());
            CreateChart(dashboard, false);
        }
    }

    /// <summary>
    /// Get the dashboard ID from the request query string.
    /// </summary>
    /// <returns></returns>
    protected Guid GetRequestDashboardID()
    {
        return (DrillIDs[DrillIDs.Count - 1]);
    }

    protected void CreateChart(ODashboard dashboard, bool drilled)
    {
        chart.Width = Unit.Percentage(100);
        chart.Height = Unit.Percentage(100);
        chart.OptionsClickClientScript = "showFilter(this);";
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        if (!IsPostBack)
        {
            Window.WriteJavascript("CallbackFunction = " + chart.CallbackFunctionName + ";");
            Window.WriteJavascript(chart.ClientSideOnDrillFunctionName + " = OnDrillFunction;");
            Window.WriteJavascript(chart.ClientSideOnBackFunctionName + " = OnBackFunction;");
        }
    }

    private Hashtable parameters = new Hashtable();

    protected override void CreateChildControls()
    {
        base.CreateChildControls();
        ODashboard dashboard = TablesLogic.tDashboard.Load(GetRequestDashboardID());

        if (dashboard == null)
        {
            return;
        }

        if (dashboard.GetDashboardFieldsInOrder().Count == 0)
        {
            return;
        }

        // Creates all the filter controls.
        //
        Table t = new Table();
        t.ID = "_" + DrillCount + GetRequestDashboardID().ToString();
        t.Width = Unit.Percentage(100);
        t.CellPadding = 0;
        t.CellSpacing = 0;

        PanelControl.Controls.Add(t);
        PanelControl.ControlStyle.CssClass = "controls";
        TableRow tr = null;

        foreach (ODashboardField field in dashboard.GetDashboardFieldsInOrder())
        {
            UIFieldBase control = null;

            if (field.ControlType == (int)ReportControlTypeEnum.MonthYear)
            {
                control = new UIFieldDateTime();
                ((UIFieldDateTime)control).SelectMonthYear = true;
                ((UIFieldDateTime)control).DateTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            }
            else if (field.ControlType == (int)ReportControlTypeEnum.DropdownList)
                control = new UIFieldSearchableDropDownList();
            else if (field.ControlType == (int)ReportControlTypeEnum.RadioButtonList)
                control = new UIFieldRadioList();
            else if (field.ControlType == (int)ReportControlTypeEnum.DateTime)
            {
                control = new UIFieldDateTime();
                ((UIFieldDateTime)control).ShowTimeControls = true;
            }
            else if (field.ControlType == (int)ReportControlTypeEnum.Date)
            {
                control = new UIFieldDateTime();
                ((UIFieldDateTime)control).ShowTimeControls = false;
            }

            if (control != null && (field.ControlType == (int)ReportControlTypeEnum.DropdownList
                || field.ControlType == (int)ReportControlTypeEnum.RadioButtonList))
            {
                control.ID = "_" + DrillCount + field.ControlIdentifier;
                control.EnableViewState = true;

                if (field.ControlType == (int)ReportControlTypeEnum.RadioButtonList)
                {
                    ((UIFieldRadioList)control).RepeatColumns = 0;
                }

                // Create a new cell for the control
                // and insert the control into it.
                //
                tr = new TableRow();
                tr.VerticalAlign = VerticalAlign.Middle;
                t.Rows.Add(tr);

                System.Web.UI.WebControls.Label labelCaption = new System.Web.UI.WebControls.Label();
                TableCell td = new TableCell();
                td.VerticalAlign = VerticalAlign.Middle;
                tr.Cells.Add(td);
                tr.Cells[0].Controls.Add(control);

                control.ControlInfo = field.ControlIdentifier;

                control.Caption = TranslateDashboardItem(field.ControlCaption);
                control.CaptionWidth = 100;
                if (control.Caption.Trim() == "")
                    control.ShowCaption = false;

                control.Span = Span.Full;

                // set up validation
                //
                if (field.DataType == 0)
                    control.ValidateDataTypeCheck = false;
                else
                    control.ValidateDataTypeCheck = true;

                // create the OdbcParameter for single value controls
                //
                LogicLayer.Parameter p = LogicLayer.Parameter.New(field.ControlIdentifier, DbType.String, 0, null, field.ControlCaption);
                if (field.DataType == 0)
                {
                    control.ValidationDataType = ValidationDataType.String;
                    p.DataType = DbType.String;//tessa
                    p.Size = 255;
                }
                else if (field.DataType == 1)
                {
                    control.ValidationDataType = ValidationDataType.Integer;
                    p.DataType = DbType.Int32;//tessa change from System.Data.Odbc.OdbcType.Int to DbType.Int32
                    p.Size = 4;
                }
                else if (field.DataType == 2)
                {
                    control.ValidationDataType = ValidationDataType.Currency;
                    p.DataType = DbType.Decimal; //tessa
                    p.Size = 8;
                }
                else if (field.DataType == 3)
                {
                    control.ValidationDataType = ValidationDataType.Double;
                    p.DataType = DbType.Double;//tessa
                    p.Size = 8;
                }
                else if (field.DataType == 4)
                {
                    control.ValidationDataType = ValidationDataType.Date;
                    p.DataType = DbType.DateTime;//Tessa
                    p.Size = 8;
                }
                else if (field.DataType == 5)
                {
                    control.ValidationDataType = ValidationDataType.String;
                    p.DataType = DbType.Guid;//tessa
                    p.Size = 8;
                }
                if (field.ControlType == (int)ReportControlTypeEnum.MultiSelectList)
                    p.IsSingleValue = false;
                else
                    p.IsSingleValue = true;

                parameters[field.ControlIdentifier] = p;
                /*
                                // attach event to dropdown list for cascading drop down
                                //
                                if (field.ControlType == (int)ReportControlTypeEnum.DropdownList)
                                    ((UIFieldSearchableDropDownList)control).SelectedIndexChanged += new EventHandler(listControl_SelectedIndexChanged);
                                if (field.ControlType == (int)ReportControlTypeEnum.RadioButtonList)
                                    ((UIFieldRadioList)control).SelectedIndexChanged += new EventHandler(listControl_SelectedIndexChanged);*/
            }
            else if (control != null && field.ControlType == (int)ReportControlTypeEnum.Date)
            {
                control.ID = "_" + DrillCount + field.ControlIdentifier;
                control.EnableViewState = true;
                ((UIFieldDateTime)control).DateTime = DateTime.Now;

                // Create a new cell for the control
                // and insert the control into it.
                //
                tr = new TableRow();
                tr.VerticalAlign = VerticalAlign.Middle;
                t.Rows.Add(tr);

                System.Web.UI.WebControls.Label labelCaption = new System.Web.UI.WebControls.Label();
                TableCell td = new TableCell();
                td.VerticalAlign = VerticalAlign.Middle;
                tr.Cells.Add(td);
                tr.Cells[0].Controls.Add(control);

                control.ControlInfo = field.ControlIdentifier;

                control.Caption = TranslateDashboardItem(field.ControlCaption);
                control.CaptionWidth = 100;
                if (control.Caption.Trim() == "")
                    control.ShowCaption = false;

                LogicLayer.Parameter p = LogicLayer.Parameter.New(field.ControlIdentifier, DbType.String, 0, null, field.ControlCaption);
                if (field.DataType == (int)ReportDataTypeEnum.DateTime)
                {
                    control.ValidationDataType = ValidationDataType.Date;
                    p.DataType = DbType.DateTime;//Tessa
                    p.Size = 8;
                }
                //if (field.ControlType == (int)ReportControlTypeEnum.Date)
                //    ((UIFieldDateTime)control).Control.Change += new EventHandler(dateControl_DateTimeChanged);
                parameters[field.ControlIdentifier] = p;
            }

            else if (control != null)
            {
                control.ID = "_" + DrillCount + field.ControlIdentifier;
                control.EnableViewState = true;

                // Create a new cell for the control
                // and insert the control into it.
                //
                tr = new TableRow();
                tr.VerticalAlign = VerticalAlign.Middle;
                t.Rows.Add(tr);

                System.Web.UI.WebControls.Label labelCaption = new System.Web.UI.WebControls.Label();
                TableCell td = new TableCell();
                td.VerticalAlign = VerticalAlign.Middle;
                tr.Cells.Add(td);
                tr.Cells[0].Controls.Add(control);

                control.ControlInfo = field.ControlIdentifier;

                control.Caption = TranslateDashboardItem(field.ControlCaption);
                control.CaptionWidth = 100;
                if (control.Caption.Trim() == "")
                    control.ShowCaption = false;

                control.Span = Span.Full;

                // set up validation
                //
                if (field.DataType == 0)
                    control.ValidateDataTypeCheck = false;
                else
                    control.ValidateDataTypeCheck = true;

                // create the OdbcParameter for single value controls
                //
                LogicLayer.Parameter p = LogicLayer.Parameter.New(field.ControlIdentifier, DbType.String, 0, null, field.ControlCaption);
                if (field.DataType == (int)ReportDataTypeEnum.String)
                {
                    control.ValidationDataType = ValidationDataType.String;
                    p.DataType = DbType.String;//tessa
                    p.Size = 255;
                }
                else if (field.DataType == (int)ReportDataTypeEnum.Integer)
                {
                    control.ValidationDataType = ValidationDataType.Integer;
                    p.DataType = DbType.Int32;//tessa change from System.Data.Odbc.OdbcType.Int to DbType.Int32
                    p.Size = 4;
                }
                else if (field.DataType == (int)ReportDataTypeEnum.Decimal)
                {
                    control.ValidationDataType = ValidationDataType.Currency;
                    p.DataType = DbType.Decimal; //tessa
                    p.Size = 8;
                }
                else if (field.DataType == (int)ReportDataTypeEnum.Double)
                {
                    control.ValidationDataType = ValidationDataType.Double;
                    p.DataType = DbType.Double;//tessa
                    p.Size = 8;
                }
                else if (field.DataType == (int)ReportDataTypeEnum.DateTime)
                {
                    control.ValidationDataType = ValidationDataType.Date;
                    p.DataType = DbType.DateTime;//Tessa
                    p.Size = 8;
                }
                else if (field.DataType == 5)
                {
                    control.ValidationDataType = ValidationDataType.String;
                    p.DataType = DbType.Guid;//tessa
                    p.Size = 8;
                }
                if (field.ControlType == (int)ReportDataTypeEnum.UniqueIdentifier)
                    p.IsSingleValue = false;
                else
                    p.IsSingleValue = true;
                parameters[field.ControlIdentifier] = p;

                // attach event to dropdown list for cascading drop down
                //
                /*if (field.ControlType == (int)ReportControlTypeEnum.DropdownList)
                    ((UIFieldSearchableDropDownList)control).SelectedIndexChanged += new EventHandler(listControl_SelectedIndexChanged);
                if (field.ControlType == (int)ReportControlTypeEnum.RadioButtonList)
                    ((UIFieldRadioList)control).SelectedIndexChanged += new EventHandler(listControl_SelectedIndexChanged);

                if (field.ControlType == (int)ReportControlTypeEnum.DateTime ||
                    field.ControlType == (int)ReportControlTypeEnum.Date ||
                    field.ControlType == (int)ReportControlTypeEnum.MonthYear)
                {
                    ((UIFieldDateTime)control).Control.Change += new EventHandler(dateControl_DateTimeChanged);
                    ((UIFieldDateTime)control).Control.ImageClearUrl = ResolveUrl("~/images/cross.gif");
                }

                if (field.ControlType == (int)ReportControlTypeEnum.MultiSelectList)
                    ((UIFieldCheckBoxListPanel)control).SelectedIndexChanged += new EventHandler(listControl_SelectedIndexChanged);*/
            }
        }
        BindDropValue(dashboard);
    }

    /// <summary>
    /// Translates the specified text from the dashboards.resx file.
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    protected string TranslateDashboardItem(string text)
    {
        string translatedText = Resources.Dashboards.ResourceManager.GetString(text);
        if (translatedText == null || translatedText == "")
            return text;
        return translatedText;
    }
    
    /// <summary>
    /// Binds values in the dropdown list.
    /// </summary>
    protected void BindDropValue(ODashboard dashboard)
    {
        if (dashboard == null)
            return;

        // Populates all list controls.
        //
        foreach (ODashboardField field in dashboard.DashboardFields)
        {
            Control control = this.FindControl("_" + DrillCount + field.ControlIdentifier);

            if (control != null)
            {
                if (field.ControlType == (int)ReportControlTypeEnum.DropdownList)
                    BindToList(control as UIFieldSearchableDropDownList, dashboard, field);
                else if (field.ControlType == (int)ReportControlTypeEnum.RadioButtonList)
                    BindToList(control as UIFieldRadioList, dashboard, field);
            }
        }
    }

    /// ------------------------------------------------------------------
    /// <summary>
    /// Bind to a dropdown list
    /// </summary>
    /// <param name="c"></param>
    /// <param name="dataSource"></param>
    /// <param name="textField"></param>
    /// <param name="valueField"></param>
    /// ------------------------------------------------------------------
    protected void BindToList(UIFieldSearchableDropDownList c, ODashboard dashboard, ODashboardField field)
    {
        LogicLayer.Parameter[] paramList = ConstructParameters(dashboard).ToArray();
        try
        {
            using (Connection conn = new Connection())
            {
                bool insertBlank = (field.IsInsertBlank == 1);

                if (field.IsPopulatedByQuery == 1)
                    c.Bind(
                        Analysis.DoQuery(field.ListQuery, ConstructParameters(dashboard).ToArray()), field.DataTextField, field.DataValueField, insertBlank);
                //Rachel. Allow to populate using C#
                else if (field.IsPopulatedByQuery == 2)
                {
                    if (field.CSharpMethodName != null)
                    {
                        DataTable dt = GetDataTableFromCMethod(field.CSharpMethodName, paramList);
                        if (dt != null)
                            c.Bind(dt, field.DataTextField, field.DataValueField, insertBlank);
                    }
                }
                else
                    c.Bind(field.ConstructTextValueTable(), "Text", "Value", insertBlank);
            }
        }
        catch (Exception ex)
        {
            Helpers.LogException(ex, this.Request);
            if (Helpers.IsDebug())
            {
                c.ErrorMessage = String.Format(Resources.Errors.Report_PopulateQueryError, ex.Message);
            }
            else
            {
                c.ErrorMessage = Resources.Errors.Report_PopulateQueryErrorNonDebug;
            }
        }
    }

    /// ------------------------------------------------------------------
    /// <summary>
    /// Bind to a dropdown list
    /// </summary>
    /// <param name="c"></param>
    /// <param name="dataSource"></param>
    /// <param name="textField"></param>
    /// <param name="valueField"></param>
    /// ------------------------------------------------------------------
    protected void BindToList(UIFieldDropDownList c, ODashboard dashboard, ODashboardField field)
    {
        LogicLayer.Parameter[] paramList = ConstructParameters(dashboard).ToArray();
        try
        {
            using (Connection conn = new Connection())
            {
                bool insertBlank = (field.IsInsertBlank == 1);

                if (field.IsPopulatedByQuery == 1)
                    c.Bind(
                        Analysis.DoQuery(field.ListQuery, ConstructParameters(dashboard).ToArray()), field.DataTextField, field.DataValueField, insertBlank);
                //Rachel. Allow to populate using C#
                else if (field.IsPopulatedByQuery == 2)
                {
                    if (field.CSharpMethodName != null)
                    {
                        DataTable dt = GetDataTableFromCMethod(field.CSharpMethodName, paramList);
                        if (dt != null)
                            c.Bind(dt, field.DataTextField, field.DataValueField, insertBlank);
                    }
                }
                else
                    c.Bind(field.ConstructTextValueTable(), "Text", "Value", insertBlank);
            }
        }
        catch (Exception ex)
        {
            Helpers.LogException(ex, this.Request);
            if (Helpers.IsDebug())
            {
                c.ErrorMessage = String.Format(Resources.Errors.Report_PopulateQueryError, ex.Message);
            }
            else
            {
                c.ErrorMessage = Resources.Errors.Report_PopulateQueryErrorNonDebug;
            }
        }
    }

    /// ------------------------------------------------------------------
    /// <summary>
    /// Bind to a radio list
    /// </summary>
    /// <param name="c"></param>
    /// <param name="dataSource"></param>
    /// <param name="textField"></param>
    /// <param name="valueField"></param>
    /// ------------------------------------------------------------------
    protected void BindToList(UIFieldRadioList c, ODashboard dashboard, ODashboardField field)
    {
        LogicLayer.Parameter[] paramList = ConstructParameters(dashboard).ToArray();
        try
        {
            using (Connection conn = new Connection())
            {
                if (field.IsPopulatedByQuery == 1)
                    c.Bind(
                        Analysis.DoQuery(field.ListQuery, ConstructParameters(dashboard).ToArray()), field.DataTextField, field.DataValueField);
                //Rachel. Allow to populate using C#
                else if (field.IsPopulatedByQuery == 2)
                {
                    if (field.CSharpMethodName != null)
                    {
                        DataTable dt = GetDataTableFromCMethod(field.CSharpMethodName, paramList);
                        if (dt != null)
                            c.Bind(dt, field.DataTextField, field.DataValueField);
                    }
                }
                else
                    c.Bind(field.ConstructTextValueTable(), "Text", "Value");
            }
        }
        catch (Exception ex)
        {
            Helpers.LogException(ex, this.Request);
            if (Helpers.IsDebug())
            {
                c.ErrorMessage = String.Format(Resources.Errors.Report_PopulateQueryError, ex.Message);
            }
            else
            {
                c.ErrorMessage = Resources.Errors.Report_PopulateQueryErrorNonDebug;
            }
        }
    }

    List<String> controlTextList;
    List<String> parameterDisplayStrings;

    /// ------------------------------------------------------------------
    /// <summary>
    /// Construct the parameters to be passed in to the SQL for doing
    /// query.
    /// </summary>
    /// <param name="report"></param>
    /// <returns></returns>
    /// ------------------------------------------------------------------
    protected List<LogicLayer.Parameter> ConstructParameters(ODashboard dashboard)
    {
        List<LogicLayer.Parameter> paramList = new List<LogicLayer.Parameter>();
        controlTextList = new List<String>();
        parameterDisplayStrings = new List<String>();
        foreach (ODashboardField field in dashboard.GetDashboardFieldsInOrder())
        {
            Control c = this.FindControl("_" + DrillCount + field.ControlIdentifier);

            if (c is UIFieldBase && !(c is UIFieldTreeList))
            {
                UIFieldBase fieldBase = c as UIFieldBase;
                if (parameters[fieldBase.ControlInfo] != null)
                {
                    LogicLayer.Parameter p = parameters[fieldBase.ControlInfo] as LogicLayer.Parameter;
                    if (field.ControlType == (int)ReportControlTypeEnum.MultiSelectList)
                    {
                        p.List = ConvertObjectToList(fieldBase.ControlValue, p.DataType);
                        if (fieldBase.ControlText != null)
                            controlTextList.Add(fieldBase.ControlText);
                    }
                    else
                    {
                        p.Value = ConvertToType(p.DataType, fieldBase.ControlValue);
                        if (fieldBase.ControlText != null)
                            controlTextList.Add(fieldBase.ControlText);
                        else if (p.Value != null)
                            controlTextList.Add(p.Value.ToString());
                    }
                    parameterDisplayStrings.Add(p.ParameterDisplay);
                    paramList.Add(p);
                }
            }
        }

        paramList.Add(LogicLayer.Parameter.New("UserID", System.Data.DbType.Guid, 16, AppSession.User.ObjectID.Value));
        paramList.Add(LogicLayer.Parameter.New("DashboardID", System.Data.DbType.Guid, 16, dashboard.ObjectID.Value));
        return paramList;
    }

    /// <summary>
    /// Convert given object to a list of object and convert each item from DbType to Type
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="itemType"></param>
    /// <returns></returns>
    protected List<object> ConvertObjectToList(object obj, DbType itemType)
    {
        List<object> genericList = new List<object>();

        if (obj is IList && obj.GetType().IsGenericType)
        {
            IList nonGenericList = (IList)obj;

            if (nonGenericList != null)
            {
                for (int x = 0; x < nonGenericList.Count; x++)
                {
                    genericList.Add(ConvertToType(itemType, nonGenericList[x]));
                    //genericList.Add(nonGenericList[x]);
                }
            }
        }
        return genericList;
    }


    /// ------------------------------------------------------------------
    /// <summary>
    /// Convert object from the string type to the specified type.
    /// </summary>
    /// <param name="type"></param>
    /// <param name="x"></param>
    /// <returns></returns>
    /// ------------------------------------------------------------------
    protected object ConvertToType(DbType type, object x)
    {
        try
        {
            if (type == DbType.String)
                return x.ToString();
            else if (type == DbType.Int32)
                return Convert.ToInt32(x.ToString());
            else if (type == DbType.Decimal)
                return Convert.ToDecimal(x.ToString());
            else if (type == DbType.Double)
                return Convert.ToDouble(x.ToString());
            else if (type == DbType.DateTime)
                return Convert.ToDateTime(x.ToString());
            else if (type == DbType.Guid)
                return new Guid(x.ToString());
        }
        catch
        {
            return DBNull.Value;
        }
        return DBNull.Value;
    }


    public DataTable GetDataTableFromCMethod(string MethodName, LogicLayer.Parameter[] paramList)
    {
        using (Connection c = new Connection())
        {
            object result = Analysis.InvokeMethod(MethodName, paramList);
            if (result is DataSet)
            {
                return ((DataSet)result).Tables[0];
            }
            if (result is DataTable)
            {
                return (DataTable)result;
            }
            return null;
        }
    }

    protected void UpdateDashboard(Guid dashboardID)
    {
        ODashboard dashboard = TablesLogic.tDashboard.Load(dashboardID);
        DataTable dt = GetDataTable(dashboard);
        chart.SeriesByColumns = dashboard.SeriesByColumns == 1;
        chart.showLegend = dashboard.ShowLegend == 1;
        chart.SeriesColumn = dashboard.SeriesColumnName;
        chart.ValueColumn = dashboard.ValueColumnName;
        chart.CatagoryColumn = dashboard.CatagoryColumnName;
        chart.DataSource = dt;
        chart.ChartType = (JSChartType)dashboard.DashboardType;
        if (dashboard.DashboardColors.Count > 0)
            chart.ColorScheme = dashboard.GetColorScheme();
        chart.MaxAxisLabelLength = dashboard.TrancateAxisLabels ?? 0;
        chart.DrillColumns = new List<string>();
        int count = 0;
        foreach (ODashboardColumnLinking l in dashboard.DashboardColumnLinkings)
        {
            foreach (ODashboardColumnLinkingField f in l.DashboardColumnLinkingFields)
            {
                count++;
                if (f.SourceType == LogicLayer.DashboardColumnLinkFieldSourceType.FromDataColumn)
                {
                    chart.DrillColumns.Add(f.SourceNameOrValue);
                }
            }
        }
        if (count == 0)
            chart.DrillColumns = null;
    }

    /// <summary>
    /// This is triggered by callback, no viewstate is loaded
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ChartLoadedEventHandler(object sender, JSChartEventArgs e)
    {
        UpdateDashboard(GetRequestDashboardID());
    }

    /// <summary>
    /// This is triggered by callback, no viewstate is loaded
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ChartCustomEvent(object sender, JSChartEventArgs e)
    {
        CurrentFilterValues = e.CustomArguments;
        BindData(e.CustomArguments);
        UpdateDashboard(GetRequestDashboardID());
    }

    /// <summary>
    /// This is triggered by callback, no viewstate is loaded
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ChartDrill(object sender, JSChartEventArgs e)
    {
        JSChart chart = e.Chart;
        //parse drill arguments

        DrillDownArguments = new Hashtable();
        foreach (string s in e.CustomArguments.Split('#'))
        {
            if (!string.IsNullOrEmpty(s))
            {
                string[] temp = s.Split(':');
                DrillDownArguments[temp[0]] = temp[1];
            }
        }

        ODashboard dashboard = TablesLogic.tDashboard.Load(GetRequestDashboardID());
        BindData(CurrentFilterValues);
        // no view state is triggered
        DrillCount++;
        DrillIDs.Add(dashboard.DashboardColumnLinkings[0].LinkedDashboardID.Value);
        CreateChildControls();
        // prepare data to bind
        BindDrillDownData(dashboard);
        UpdateDashboard(dashboard.DashboardColumnLinkings[0].LinkedDashboardID.Value);
        chart.RenderNew = true;
    }

    protected void BindData(string arg)
    {
        if (string.IsNullOrEmpty(arg))
            return;

        Hashtable result = new Hashtable();
        string[] s1 = new string[] { "&" };
        string[] s2 = new string[] { "=" };
        string[] args = arg.Split(s1, StringSplitOptions.None);
        foreach (string s in args)
        {
            string[] temp = s.Split(s2, StringSplitOptions.None);
            if (!result.ContainsKey(temp[0]))
                result[HttpUtility.UrlDecode(temp[0])] = "";

            result[HttpUtility.UrlDecode(temp[0])] += HttpUtility.UrlDecode(temp[1]).Trim();
        }

        foreach (string key in result.Keys)
        {
            var c = this.FindControl(key);
            if (c != null)
                if (c.Parent is UIFieldBase)
                {
                    UIFieldBase p = (UIFieldBase)c.Parent;
                    p.ControlValue = result[key];
                }
                else if (c.Parent != null 
                    && c.Parent.Parent is UIFieldBase)
                {
                    UIFieldBase p = (UIFieldBase)c.Parent.Parent;
                    p.ControlValue = result[key];
                }
                else if (c.Parent != null 
                    && c.Parent.Parent != null 
                    && c.Parent.Parent.Parent is UISearchableDropDownList
                    && key.Contains("_textItem"))
                {
                    UISearchableDropDownList p = (UISearchableDropDownList)c.Parent.Parent.Parent;
                    p.SelectedValue = p.Items.FindByText((string)result[key]).Value;
                }
        }
    }

    Hashtable dashboardColumnMappings = new Hashtable();
    protected DataTable GetDataTable(ODashboard dashboard)
    {
        DataTable dt = null;
        foreach (ODashboardColumnMapping dashboardColumnMapping in dashboard.DashboardColumnMappings)
            dashboardColumnMappings[dashboardColumnMapping.ColumnName] = dashboardColumnMapping;
        // Runs the query against the database and gets
        // the result as a DataTable.
        //
        List<LogicLayer.Parameter> paramList = ConstructParameters(dashboard);
        if (dashboard.UseCSharpQuery == 0 || dashboard.UseCSharpQuery == null)
        {
            dt = Analysis.DoQuery(dashboard.DashboardQuery, paramList.ToArray());
        }
        else
        {
            object dataSource = Analysis.InvokeMethod(dashboard.CSharpMethodName, paramList.ToArray());
            if (dataSource is DataTable)
                dt = (DataTable)dataSource;
            else if (dataSource is DataSet)
                dt = ((DataSet)dataSource).Tables[0];
        }

        MapReportDataFields(dt);

        return dt;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="dt"></param>
    protected void MapReportDataFields(DataTable dt)
    {
        int totalColumns = dt.Columns.Count;
        for (int i = 0; i < totalColumns; i++)
        {
            string clName = dt.Columns[i].ColumnName;
            ODashboardColumnMapping column = dashboardColumnMappings[clName] as ODashboardColumnMapping;

            if (column != null && dt.Columns[i].DataType == typeof(String)
                && column.ResourceName != null && column.ResourceName.Trim() != "")
            {
                foreach (DataRow dr in dt.Rows)
                {
                    string resourceAssemblyName = column.ResourceAssemblyName;
                    Assembly asm;
                    if (resourceAssemblyName.Trim() == "")
                        asm = Assembly.Load("App_GlobalResources");
                    else
                        asm = Assembly.Load(resourceAssemblyName);

                    if (asm != null)
                    {
                        ResourceManager rm = new ResourceManager(column.ResourceName, asm);
                        if (rm != null && dr[clName] != null && dr[clName].ToString() != "")
                        {
                            string a = dr[clName].ToString();
                            string translatedText = rm.GetString(a, System.Threading.Thread.CurrentThread.CurrentUICulture);
                            if (translatedText != null && translatedText != "")
                                dr[clName] = translatedText;
                        }
                    }
                }
            }
        }
    }

    protected void BindDrillDownData(ODashboard dashboard)
    {
        object t, t2;
        foreach (ODashboardColumnLinking l in dashboard.DashboardColumnLinkings)
        {
            foreach (ODashboardColumnLinkingField f in l.DashboardColumnLinkingFields)
            {
                switch (f.SourceType)
                {
                    case DashboardColumnLinkFieldSourceType.FromDataColumn:
                        t2 = PanelControl.FindControl("_" + DrillCount + f.TargetDashboardControlIdentifier);
                        ((UIFieldBase)t2).ControlValue = DrillDownArguments[f.SourceNameOrValue];
                        break;
                    case DashboardColumnLinkFieldSourceType.FromFilter:
                        t = PanelControl.FindControl("_" + (DrillCount - 1) + f.SourceNameOrValue);
                        t2 = PanelControl.FindControl("_" + DrillCount + f.TargetDashboardControlIdentifier);
                        ((UIFieldBase)t2).ControlValue = ((UIFieldBase)t).ControlValue;
                        break;
                    case DashboardColumnLinkFieldSourceType.Static:
                        t2 = PanelControl.FindControl("_" + DrillCount + f.TargetDashboardControlIdentifier);
                        ((UIFieldBase)t2).ControlValue = f.SourceNameOrValue;
                        break;
                }
            }
        }
    }

    protected void refresh_Click(object sender, EventArgs e)
    {
        var t = PanelControl.FindControl("_" + DrillCount + GetRequestDashboardID().ToString());
        if (t != null)
            t.Visible = false;
        FilterData.Add((string)CurrentFilterValues);
        DrillCount++;
        ODashboard dashboard = TablesLogic.tDashboard.Load(GetRequestDashboardID());
        DrillIDs.Add(dashboard.DashboardColumnLinkings[0].LinkedDashboardID.Value);
        // init control data
        CurrentFilterValues = "";
        CreateChildControls();
        BindDrillDownData(dashboard);
    }

    protected void back_Click(object sender, EventArgs e)
    {
        var t = PanelControl.FindControl("_" + DrillCount + GetRequestDashboardID().ToString());
        if (t != null)
            PanelControl.Controls.Remove(t);
        DrillCount--;
        DrillIDs.RemoveAt(DrillIDs.Count - 1);
        CreateChildControls();
        CurrentFilterValues = FilterData[DrillCount];
        BindData(FilterData[DrillCount]);
        FilterData.RemoveAt(DrillCount);
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="../../../css/v7-chart.css" rel="stylesheet" type="text/css" />

    <script>
        // initialize in onload
        var CallbackFunction;

        function OnDrillFunction(ctx) {
            $('#' + ctx).parent().find('#refresh_bt').click();
        }

        function OnBackFunction(ctx) {
            $('#' + ctx).parent().find('#back_bt').click();
        }

        function closeFilter(e) {
            $('#PanelFilter').hide();
        }

        function showFilter(e) {
            $('#PanelFilter').show();
        }

        function submit(e) {
            CallbackFunction($('form').serialize());
            $('#PanelFilter').hide();
        }

        // temporary workaround to reload the chart
        function reloadChart() {
            $('.apply-btn').click();
        }
    </script>

</head>
<body class="bg-color-white">
    <form id="Form1" runat="server">
    <div id="PanelFilter" style="display: none; height:100%;" class="filter-control">
        <div class="filter-title">
            Filters</div>
        <div class="filter-close" onclick="closeFilter(this);">
            ×</div>
        <ui:UIPanel runat="server" ID="PanelControl">
            <ui:UIButton runat="server" ID="refresh" Text="Refresh" CssClass="filter-hidden"
                OnClick="refresh_Click" />
            <ui:UIButton runat="server" ID="back" Text="Back" CssClass="filter-hidden" OnClick="back_Click" />
        </ui:UIPanel>
        <div class="filter-footer">
            <a type="button" class="btn btn-primary apply-btn" onclick="submit(this);">Apply</a>
        </div>
    </div>
    <ui:JSChart runat="server" ID="chart" OnChartLoaded="ChartLoadedEventHandler" LoadingImage="../../../images/loading.gif"
        OptionsImage="../../../images/options.svg" OnCustomEvent="ChartCustomEvent" OnDrill="ChartDrill" />
    </form>
    <style>
        html, body, form
        {
            height: 100%;
        }
        body
        {
            min-width: 0 !important;
            overflow: hidden;
        }
        .filter-control
        {
            position: absolute;
            z-index: 1;
            background-color: #eee;
            border-radius: 5px;
            padding: 0 5px 5px 5px;
            border: 1px solid#ccc;
            overflow-y: auto;
            max-height: 97%;
        }
        .filter-close
        {
            cursor: pointer;
            float: right;
            width: 18px;
            height: 30px;
            font-size: 20px;
            font-weight: bold;
        }
        .filter-title
        {
            text-align: center;
            float: left;
            width: 18px;
            height: 30px;
            font-size: 20px;
            font-weight: bold;
        }
        .filter-close:hover
        {
            color: #27F;
        }
        .field-textbox
        {
            padding: 0;
        }
        .filter-control table
        {
            width: 100%;
        }
        .filter-control td
        {
            vertical-align: middle;
        }
        .filter-footer
        {
            text-align: center;
            padding-top: 5px;
        }
        .filter-hidden
        {
            display: none;
        }
        .btn-primary
        {
            color: #fff;
            background-color: #337ab7 !important;
            border-color: #2e6da4;
        }
        .btn
        {
            display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: 400;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
        }
        #AUFCal, #AUFMY
        {
            z-index: 1;
        }
    </style>
</body>
</html>
