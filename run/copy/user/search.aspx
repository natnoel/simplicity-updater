 <%@ Page Language="C#" Theme="Corporate" Inherits="PageBase" Culture="auto" meta:resourcekey="PageResource1"
    UICulture="auto" %>

<%@ Register Src="~/components/menu.ascx" TagPrefix="web" TagName="menu" %>
<%@ Register Src="~/components/objectsearchpanel.ascx" TagPrefix="web" TagName="search" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="Anacle.DataFramework" %>
<%@ Import Namespace="LogicLayer" %>
<%@ Import Namespace="System.IO" %>



<script runat="server">

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        labelUserLicenseCount.Text = OUserBase.GetUserLicenseText();

        foreach (UIGridViewCommand command in gridResults.Commands)
        {
            if (command.CommandName == "DownloadExcelForUser" || command.CommandName == "UploadExcelForUser" ||
                command.CommandName == "DownloadExcelForUserPositionAssignment" || command.CommandName == "UploadExcelForUserPositionAssignment")
            {
                command.Visible = AppSession.User.AllowCreate(GetRequestObjectType());
            }
        }
    }


    /// <summary>
    /// Populates the form.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void panel_PopulateForm(object sender, EventArgs e)
    {
        List<OLocation> locations = new List<OLocation>();
        foreach (OPosition position in AppSession.User.GetPositionsByObjectType("OUser"))
            foreach (OLocation location in position.LocationAccess)
                locations.Add(location);

        listPositions.Bind(OPosition.GetPositionsAtOrBelowLocations(locations));
        listRoles.Bind(ORole.GetAllRoles(), "RoleName", "ObjectID");
        
    }

    
    /// <summary>
    /// Filters the users by what the currently logged on user
    /// has access to.
    /// </summary>
    /// <param name="e"></param>
    protected void panel_Search(objectSearchPanel.SearchEventArgs e)
    {
        List<OLocation> locations = new List<OLocation>();
        foreach (OPosition position in AppSession.User.GetPositionsByObjectType("OUser"))
            foreach(OLocation location in position.LocationAccess)
                locations.Add(location);

        List<OPosition> positions = OPosition.GetPositionsAtOrBelowLocations(locations);

        TUser u = TablesLogic.tUser;
        TUser u2 = new TUser();

        e.CustomCondition =
            u2.Select(u2.Positions.ObjectID.Count()).Where(u2.ObjectID == u.ObjectID & u2.Positions.IsDeleted == 0) ==
            u2.Select(u2.Positions.ObjectID.Count()).Where(u2.ObjectID == u.ObjectID & u2.Positions.IsDeleted == 0 & u2.Positions.ObjectID.In(positions));

                    
    }


    protected void ExcelUploadDownload_Action(object sender, string commandName, List<object> dataKeys)
    {
        if (commandName == "DownloadExcelForUser" || commandName == "DownloadExcelForUserPositionAssignment")
        {
            int successfulIndicator = 1;
            String msg;

            if (commandName == "DownloadExcelForUser")
                msg = OUser.ExportExcel(out successfulIndicator, true);
            else
                msg = OUser.ExportExcel(out successfulIndicator, false);
            
            if (successfulIndicator == 0)
                panel.Message = msg;
            else
            {
                if (!File.Exists(msg))
                {
                    panel.Message = Resources.Errors.Journal_ErrExportingExcel;
                    return;
                }
                FileStream fs = new FileStream(msg, FileMode.Open, FileAccess.Read);

                byte[] ImageData = new byte[fs.Length];
                fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));
                fs.Close();

                Window.Download(ImageData, Path.GetFileName(msg).Replace(" ", "_"), "application/vnd.ms-excel");

                File.Delete(msg);
            }
        }

        
        if (commandName == "UploadExcelForUser")
            UploadExcelForUserDialogBox.Show();

        //To be finished later: user position assignment
        if (commandName == "UploadExcelForUserPositionAssignment")
            UploadExcelForUserPositionAssignmentDialogBox.Show();
    }

    protected void UploadExcelForUserDialogBox_Uploaded(object sender, EventArgs e)
    {
        List<HttpPostedFile> listOfUploadedFile = UploadExcelForUserDialogBox.GetUploadFiles();
        if (listOfUploadedFile.Count == 1 && listOfUploadedFile[0].ContentLength == 0)
        {
            panel.Message = Resources.Errors.ExcelUpload_NoExcelUploaded;
            return;
        }
        String tempDirectory = (String)new System.Configuration.AppSettingsReader().GetValue("ReportTempFolder", typeof(String));
        List<String> listOfTempFile = new List<string>();

        foreach (HttpPostedFile uploadedFile in listOfUploadedFile)
        {
            string tempSavePath = tempDirectory + Path.GetFileName(uploadedFile.FileName);
            try
            {
                uploadedFile.SaveAs(tempSavePath);
                listOfTempFile.Add(tempSavePath);
            }
            catch
            {
            }
        }
        String msg = OUser.ImportExcelFile(listOfTempFile, true, null);
        if (msg != "")
            panel.Message = msg;
        else
            panel.Message = Resources.Messages.General_UploadSuccessfully;

    }

    protected void UploadExcelForUserPositionAssignmentDialogBox_Uploaded(object sender, EventArgs e)
    {
        List<HttpPostedFile> listOfUploadedFile = UploadExcelForUserPositionAssignmentDialogBox.GetUploadFiles();
        if (listOfUploadedFile.Count == 1 && listOfUploadedFile[0].ContentLength == 0)
        {
            panel.Message = Resources.Errors.ExcelUpload_NoExcelUploaded;
            return;
        }
        String tempDirectory = (String)new System.Configuration.AppSettingsReader().GetValue("ReportTempFolder", typeof(String));
        List<String> listOfTempFile = new List<string>();

        foreach (HttpPostedFile uploadedFile in listOfUploadedFile)
        {
            string tempSavePath = tempDirectory + Path.GetFileName(uploadedFile.FileName);
            try
            {
                uploadedFile.SaveAs(tempSavePath);
                listOfTempFile.Add(tempSavePath);
            }
            catch
            {
            }
        }
        List<OPosition> positions = AppSession.User.GetPositionsByObjectType("OUser");
        String msg = OUser.ImportExcelFile(listOfTempFile, false, positions);
        if (msg != "")
            panel.Message = msg;
        else
            panel.Message = Resources.Messages.General_UploadSuccessfully;

    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <meta http-equiv="pragma" content="no-cache" />
    
</head>
<body class="body-search-bg">
    <form id="form1" runat="server">
        <ui:UIObjectPanel runat="server" ID="formMain" 
            meta:resourcekey="formMainResource" BorderStyle="NotSet">
            <web:search runat="server" ID="panel" Caption="User" GridViewID="gridResults" 
                BaseTable="tUser" SearchType="ObjectQuery"
                meta:resourcekey="panelResource" OnPopulateForm="panel_PopulateForm" OnSearch="panel_Search" 
                SearchTextBoxVisible="true"
                AdvancedSearchPanelID="panelSearch" 
                SearchTextBoxPropertyNames="ObjectName,UserBase.LoginName,UserBase.Cellphone,UserBase.Email,UserBase.Fax,UserBase.Phone,UserBase.AddressCountry,UserBase.AddressState,UserBase.AddressCity,UserBase.Address"
                SearchTextBoxHint="what is the user name, login name, contact information or address?"
                CustomPageSize="true" MaximumNumberOfResults="_500" ResultsPerPage="_20"
                ></web:search>


            <div class="mdl-search-div">
                <ui:UIDialogBox runat="server" ID="dialogSearch">
                <ui:UIPanel runat="server" ID="panelSearch" meta:resourcekey="panelSearchResource" >
                    <ui:uifieldlistbox runat="server" id="listPositions" PropertyName="PermanentPositions.Position.ObjectID" Caption="Positions" meta:resourcekey="listPositionsResource" Rows="4"></ui:uifieldlistbox>
                    <ui:uifieldlistbox runat="server" id="listRoles" PropertyName="PermanentPositions.Position.Role.ObjectID" Caption="Roles" meta:resourcekey="listRolesResource" Rows="4"></ui:uifieldlistbox>
                    <web:search2 runat="server" ID="panelSearchButtons"  meta:resourcekey="panelSearchButtonsResource" />
                </ui:UIPanel>
                </ui:UIDialogBox>
                <ui:UIGridView runat="server" ID="gridResults" KeyName="ObjectID"
                                meta:resourcekey="gridResultsResource" Width="100%" 
                                DataKeyNames="ObjectID" GridLines="Both" ImageRowErrorUrl="" RowErrorColor="" 
                                style="clear:both;" OnAction="ExcelUploadDownload_Action" UseDataTablesScript="true"
                                    >
                                <PagerSettings Mode="NumericFirstLast" />
                                <commands>
                                    <ui:UIGridViewCommand AlwaysEnabled="False" CausesValidation="False" 
                                        CommandName="DeleteObject" CommandText="Delete Selected" 
                                        ConfirmText="Are you sure you wish to delete the selected items?" 
                                        IconCssClass="material-icons" IconText="delete" 
                                        meta:resourcekey="gridResultsUIGridViewCommandDeleteSelectedResource" />
                                    <ui:UIGridViewCommand AlwaysEnabled="False" CausesValidation="False"
                                            CommandName="DownloadExcelForUser" CommandText="Download Template for Users"
                                            ImageUrl="~/images/download-black-16.png" meta:resourcekey="gridResultsUIGridViewCommandDownloadExcelTemplateForUser"/>
                                    <ui:UIGridViewCommand CommandText="Upload Excel for Users" ImageUrl="~/images/upload-black-16.png" CommandName="UploadExcelForUser" 
                                        meta:resourcekey="gridResultsUIGridViewCommandUploadExcelTemplateForUser"/>
                                    <ui:UIGridViewCommand CommandName ="DownloadExcelForUserPositionAssignment" CommandText="Download Template for User Position Assignments" ImageUrl="~/images/download-black-16.png" />
                                    <ui:UIGridViewCommand CommandName="UploadExcelForUserPositionAssignment" CommandText="Upload Excel for User Position Assignments" ImageUrl="~/images/upload-black-16.png" />
                                </Commands>
                                <Columns>
                                    <ui:UIGridViewButtonColumn ButtonType="Image" CommandName="DeleteObject" 
                                        ConfirmText="Are you sure you wish to delete this item?" 
                                        ImageUrl="~/images/delete.gif" meta:resourcekey="gridResultsUIGridViewButtonColumnDeleteObjectResource">
                                        <HeaderStyle HorizontalAlign="Left" Width="16px" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </ui:UIGridViewButtonColumn>
                                    <ui:UIGridViewBoundColumn DataField="ObjectName" HeaderText="User Name"  MobileDeviceVisibility="DesktopModeOnly"
                                        meta:resourcekey="gridResultsUIGridViewBoundColumnObjectNameResource" PropertyName="ObjectName" 
                                        ResourceAssemblyName="" SortExpression="ObjectName" DisallowFromColumnChoosing="true">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </ui:UIGridViewBoundColumn>
                                    <ui:UIGridViewBoundColumn DataField="UserBase.LoginName"  MobileDeviceVisibility="DesktopModeOnly"
                                        HeaderText="Login Name" meta:resourcekey="gridResultsUIGridViewBoundColumnUserBaseLoginNameResource" 
                                        PropertyName="UserBase.LoginName" ResourceAssemblyName="" 
                                        SortExpression="UserBase.LoginName">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </ui:UIGridViewBoundColumn>
                                    <ui:UIGridViewBoundColumn DataField="UserBase.Cellphone"  MobileDeviceVisibility="DesktopModeOnly"
                                        HeaderText="Cellphone" meta:resourcekey="gridResultsUIGridViewBoundColumnUserBaseCellphoneResource" 
                                        PropertyName="UserBase.Cellphone" ResourceAssemblyName="" 
                                        SortExpression="UserBase.Cellphone">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </ui:UIGridViewBoundColumn>
                                    <ui:UIGridViewBoundColumn DataField="UserBase.Email" HeaderText="Email"  MobileDeviceVisibility="DesktopModeOnly"
                                        meta:resourcekey="gridResultsUIGridViewBoundColumnUserBaseEmailResource" PropertyName="UserBase.Email" 
                                        ResourceAssemblyName="" SortExpression="UserBase.Email">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </ui:UIGridViewBoundColumn>
                                    <ui:UIGridViewBoundColumn DataField="UserBase.Fax" HeaderText="Fax"  MobileDeviceVisibility="DesktopModeOnly"
                                        meta:resourcekey="gridResultsUIGridViewBoundColumnUserBaseFaxResource" PropertyName="UserBase.Fax" 
                                        ResourceAssemblyName="" SortExpression="UserBase.Fax">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </ui:UIGridViewBoundColumn>
                                    <ui:UIGridViewBoundColumn DataField="UserBase.Phone" HeaderText="Phone" MobileDeviceVisibility="DesktopModeOnly"
                                        meta:resourcekey="gridResultsUIGridViewBoundColumnUserBasePhoneResource" PropertyName="UserBase.Phone" 
                                        ResourceAssemblyName="" SortExpression="UserBase.Phone">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </ui:UIGridViewBoundColumn>
                                    <ui:UIGridViewTemplateColumn MobileDeviceVisibility="MobileModeOnly">
                                        <ItemTemplate>
                                            <b><%# ((DataRowView)Container.DataItem)["ObjectName"] %><br /></b>
                                            <small><%# ((DataRowView)Container.DataItem)["UserBase.Cellphone"]%></small>
                                            <small><%# ((DataRowView)Container.DataItem)["UserBase.Phone"]%></small>
                                            <small><%# ((DataRowView)Container.DataItem)["UserBase.Email"]%></small>
                                            <small><%# ((DataRowView)Container.DataItem)["UserBase.Fax"]%></small>
                                        </ItemTemplate>
                                    </ui:UIGridViewTemplateColumn>
                                </Columns>
                            
                            </ui:UIGridView>
                            <web:fileuploaddialogbox runat="server" ID="UploadExcelForUserDialogBox" NumberOfFileUploads="1"
                            Title="Upload Excel File For User" OnUploaded="UploadExcelForUserDialogBox_Uploaded"  meta:resourcekey="UploadExcelForUserDialogBoxResource" /> 
                            <web:fileuploaddialogbox runat="server" ID="UploadExcelForUserPositionAssignmentDialogBox" NumberOfFileUploads="1"
                            Title="Upload Excel File For User Position Assignments" OnUploaded="UploadExcelForUserPositionAssignmentDialogBox_Uploaded"  meta:resourcekey="UploadExcelForUserPositionAssignmentDialogBoxResource" />

                            <br />
                            <div style="padding: 20px">
                                <asp:Label runat="server" ID="labelUserLicense" meta:resourcekey="labelUserLicenseResource"
                                    Text="Licenses: "></asp:Label>
                                <asp:Label runat="server" ID="labelUserLicenseCount" meta:resourcekey="labelUserLicenseCountResource"></asp:Label>
                            </div>
               
            </div>
            
            </ui:UIObjectPanel>
            
        
    </form>
</body>
</html>
