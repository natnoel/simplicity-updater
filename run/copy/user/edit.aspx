<%@ Page Language="C#" Theme="Corporate" Inherits="PageBase" Culture="auto" meta:resourcekey="PageResource1"
    UICulture="auto" %>

<%@ Register Src="~/components/menu.ascx" TagPrefix="web" TagName="menu" %>
<%@ Register Src="~/components/objectpanel.ascx" TagPrefix="web" TagName="object" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Collections" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="Anacle.DataFramework" %>
<%@ Import Namespace="LogicLayer" %>

<script runat="server">
    /// <summary>
    /// Populates the form controls.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="obj"></param>
    protected void panel_PopulateForm(object sender, EventArgs e)
    {
        
        OUser user = panel.SessionObject as OUser;

        ViewState["WasBanned"] = user.IsBanned;

        List<OLocation> locations = new List<OLocation>();
        foreach (OPosition position in AppSession.User.GetPositionsByObjectType("OUser"))
        {
            foreach (OLocation location in position.LocationAccess)
                locations.Add(location);
        }

        Craft.Bind(OCraft.GetAllCraft());

        // Hide the password textboxes
        // if the system is configured to use Windows Authentication
        // for logging on users.
        //
        if (ConfigurationManager.AppSettings["AuthenticateWithWindowsLogon"].ToLower() == "true")
        {
            panelPassword.Visible = false;
        }
        else
        {
            Password1.ValidateRequiredField = user.IsNew;
            Password2.ValidateRequiredField = user.IsNew;
        }

        // 2014.08.13
        // Kim Foong
        // Fixes the serious security loophole where the user is allowed to edit the
        // user account and grant himself administrator rights.
        //
        string arg = Security.Decrypt(Request["ID"]);
        string[] args = arg.Split(':');
        if (args[2] == "EDITPROFILE")
        {
            tabMemo.Visible = false;
            tabAttachments.Visible = false;
            gv_Position.Commands[0].Visible = false;
            gv_Position.Commands[1].Visible = false;
            gv_Position.Commands[2].Visible = false;
            gv_Position.Columns[0].Visible = false;

            gridDelegatedToOthersPositions.Commands[0].Visible = false;

            panel.DeleteButtonVisible = false;
            checkIsBanned.Visible = false;
            BannedDateFrom.Visible = false;
            UserLoginName.Enabled = false;
            objectBase.ObjectNameEnabled = false;
            chkResetPassword.Visible = false;
            IsActiveDirectoryUser.Enabled = false;
            ActiveDirectoryDomain.Enabled = false;
            panel.SaveAndCloseButtonVisible = false;
            panel.SaveAndNewButtonVisible = false;
        }
        else
            hintDelegation.Visible = false;

        if (user.IsNew)
            user.IsActiveDirectoryUser = 0;

        ddl_Language.Bind(OLanguage.GetAllLanguages(), "ObjectName", "CultureCode");

        panel.ObjectPanel.BindObjectToControls(user);
        this.checkIsBanned.Checked = user.IsBanned == 1;
        this.BannedDateFrom.DateTime = user.BannedDateFrom;

        if (OApplicationSetting.Current.ActiveDirectoryDomain != null && OApplicationSetting.Current.ActiveDirectoryDomain != string.Empty)
            ActiveDirectoryDomain.Text = OApplicationSetting.Current.ActiveDirectoryDomain.ToString();
    }



    /// <summary>
    /// Validates and saves the user object into the database.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void panel_ValidateAndSave(object sender, EventArgs e)
    {
        using (Connection c = new Connection())
        {
            OUser user = (OUser)panel.SessionObject;
            panel.ObjectPanel.BindControlsToObject(user);
            OApplicationSetting applicationSetting = OApplicationSetting.Current;

            // Ensure no duplicate users with the same
            // name and login ID.
            //
            if (user.IsDuplicateUser())
            {
                this.UserLoginName.ErrorMessage = Resources.Errors.User_DuplicateUser;
                this.objectBase.ObjectName.ErrorMessage = Resources.Errors.User_DuplicateUser;
            }

            // Validate's the user password
            //
            if (Password1.Text.ToString() != "" || Password2.Text.ToString() != "")
            {
                if (Password1.Text.ToString() != Password2.Text.ToString())
                {
                    Password1.ErrorMessage = Resources.Errors.User_PasswordDifferent;
                    Password2.ErrorMessage = Resources.Errors.User_PasswordDifferent;
                }
                else
                {
                    // Ensures that the password adheres to the minimum
                    // length requirement.
                    //
                    if (applicationSetting.PasswordMinimumLength != null &&
                        Password1.Text.Length < applicationSetting.PasswordMinimumLength.Value)
                    {
                        Password1.ErrorMessage =
                            Password2.ErrorMessage =
                            String.Format(Resources.Errors.User_PasswordMinimumLength,
                            applicationSetting.PasswordMinimumLength.Value);
                    }

                    // Ensures that the password has the required
                    // valid characters.
                    //
                    if (!OUserPasswordHistory.ValidatePasswordCharacters(Password1.Text))
                    {
                        if (applicationSetting.PasswordRequiredCharacters == 1)
                            Password1.ErrorMessage =
                                Password2.ErrorMessage =
                                Resources.Errors.User_PasswordMustContainAlphaNumericCharacters;
                        else if (applicationSetting.PasswordRequiredCharacters == 2)
                            Password1.ErrorMessage =
                                Password2.ErrorMessage =
                                Resources.Errors.User_PasswordMustContainAlphaNumericSpecialCharacters;
                    }

                    // Ensures that the password does not exist
                    // in the history of passwords.
                    //
                    string strHashedNewPassword = Security.HashPassword(user.ObjectID.Value.ToString(), Password1.Text);
                    if (OUserPasswordHistory.DoesPasswordExist(user.ObjectID.Value, strHashedNewPassword))
                    {
                        Password1.ErrorMessage =
                            Password2.ErrorMessage =
                            String.Format(Resources.Errors.User_PasswordHistoryExists,
                            applicationSetting.PasswordHistoryKept);
                    }
                }
            }
            /*
            //check duplicate user email if email is entered
            if (textEmail.Text != String.Empty && textEmail.Text != null && user.IsDuplicateUserEmail() == true)
                textEmail.ErrorMessage = Resources.Errors.User_DuplicatedUserEmail;
             * */

            // Get email from AD
            // Wang Yiyuan, 2012.4.23
            //
            if (applicationSetting.GetUserEmailFromActiveDirectory == 1)
            {
                string adDomainName = applicationSetting.ADDomainName;
                ActiveDirectory ad = new ActiveDirectory(adDomainName);
                // If need to impersonate to AD, set logon account name and password
                //
                if (applicationSetting.UseImpersonatorToAD == 1)
                {
                    ad.LogonAccName = applicationSetting.ADLogOnDomainAccount;
                    ad.LogonPassword = applicationSetting.ADLogOnPassword;
                }
                
                // Search ad by group name settings and nric
                //
                string nric = user.UserBase.LoginName.Trim();
                string group = applicationSetting.ActiveDirectoryUserGroupNames;
                ActiveDirectoryUser adUser = new ActiveDirectoryUser();
                if (group != null && group != string.Empty)
                    adUser = ad.GetADUser(group, nric);
                else
                    adUser = ad.GetADUser(nric);

                if (adUser.LoginName != null && adUser.LoginName != string.Empty)
                {
                    user.UserBase.Email = adUser.Email;
                    user.ObjectName = adUser.UserName;

                    label1.Visible = false;
                    labelEmailErrorMsg.Visible = false;
                    labelEmailErrorMsg.Text = "";

                }
                else
                {
                    labelEmailErrorMsg.Text = "Active Directory user " + user.ObjectName + " with NRIC " + user.UserBase.LoginName
                                            + " cannot be found in active directory group.";
                    labelEmailErrorMsg.Visible = true;
                    label1.Visible = true;
                }
            }
            if (!panel.ObjectPanel.IsValid)
                return; 
            
            if (chkResetPassword.Checked && !IsActiveDirectoryUser.Checked)
            {
                user.ResetPassword();
                chkResetPassword.Checked = false;
            }
            else
            {
                // Sets the user password
                //
                if (Password1.Text.ToString() != "" &&
                    Password2.Text.ToString() != "" &&
                    Password1.Text.ToString() == Password2.Text.ToString())
                {
                    //user.SetNewPasswordForCapitaland(Password1.Text.ToString(), true);
                    user.SetNewPassword(Password1.Text.ToString(), true);
                }


            }

            // Save
            // 
            user.ActivateAndSaveCurrentPositions();
            //January 4 2013
            //Reinheart Sadie
            user.BanUser(checkIsBanned.Checked ? 1 : 0, UserBanReasonCode.InactivateByAdministrator, this.BannedDateFrom.DateTime);
            user.Save();
            c.Commit();
        }
    }


    /// <summary>
    /// Occurs when the user checks the reset password checkbox.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chkResetPassword_CheckedChange(object sender, EventArgs e)
    {
        Password1.Text = "";
        Password2.Text = "";
    }


    /// <summary>
    /// Occurs when the user checks the Banned checkbox.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void checkIsBanned_CheckedChanged(object sender, EventArgs e)
    {
        int? wasBanned = (int?)ViewState["WasBanned"];

        if (wasBanned == 1 && !checkIsBanned.Checked)
        {
            OUser user = (OUser)panel.SessionObject;
            user.LoginRetries = 0;
            user.LastLoginTimeForAutoBanned = DateTime.Today;
        }

        if (!checkIsBanned.Checked)
            BannedDateFrom.ControlValue = null;
    }


    /// <summary>
    /// Hides/shows controls
    /// </summary>
    /// <param name="e"></param>
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        OApplicationSetting applicationSetting = OApplicationSetting.Current;
        // Wang Yiyuan, 2012.4.23
        // Disable textEmail and user name if App Setting GetUserEmailFromActiveDirectory == 1
        //
        textEmail.Enabled = applicationSetting.GetUserEmailFromActiveDirectory == 0;
        textEmail.ValidateRequiredField = (chkResetPassword.Checked || applicationSetting.IsUserEmailCompulsory == 1)
                                            && applicationSetting.GetUserEmailFromActiveDirectory == 0;

        objectBase.ObjectNameEnabled = applicationSetting.GetUserEmailFromActiveDirectory == 0;
        
        Password1.Visible = !chkResetPassword.Checked;
        Password2.Visible = !chkResetPassword.Checked;

        if (this.IsActiveDirectoryUser.Checked)
        {
            chkResetPassword.Visible = false;
            //Password1.Visible = false;
            //Password2.Visible = false;
            ActiveDirectoryDomain.Visible = false;
        }
        else
        {
            if (Request["MODE"] == null)
                chkResetPassword.Visible = true;
            //Password1.Visible = true;
            //Password2.Visible = true;
            ActiveDirectoryDomain.Visible = false;
        }

        BannedDateFrom.Visible = checkIsBanned.Checked;

        panelPassword.Visible = OApplicationSetting.Current.IsUsingActiveDirectory != 1;


    }



    protected void gv_Position_Action(object sender, string commandName, List<object> objectIds)
    {
        if (commandName == "RemoveObject")
        {
            OUser u = (OUser)panel.SessionObject;
            foreach (Guid id in objectIds)
            {
                OUserPermanentPosition permanentPosition = u.PermanentPositions.Find(id);

                // Removes all delegated positions (with the same position ID)
                // to other users 
                //
                for (int i = u.DelegatedToOthersPositions.Count - 1; i >= 0; i--)
                    if (u.DelegatedToOthersPositions[i].PositionID == permanentPosition.PositionID)
                        u.DelegatedToOthersPositions.Remove(u.DelegatedToOthersPositions[i]);

                u.PermanentPositions.Remove(permanentPosition);
            }

            panel.ObjectPanel.BindControlsToObject(u);
            panel.ObjectPanel.BindObjectToControls(u);
        }
        if (commandName == "DelegatePosition")
        {
            OUser u = (OUser)panel.SessionObject;
            if (dropGrantedToUser.Items.Count == 0)
                dropGrantedToUser.Bind(u.GetAllValidUserForDelegating(u.ObjectID));
                //dropGrantedToUser.Bind(OUser.GetAllNonTenantUsersExceptSpecified(AppSession.User.ObjectID));
            dropGrantedToUser.SelectedIndex = 0;

            objectPanelDelegatePositions.Show();
            //popupDelegatePositions.Show();
        }

        if (commandName == "AddObjectPosition")
        {
            DialogBox_PositionItem.Visible = true;

            OUser user = panel.SessionObject as OUser;
            panel.ObjectPanel.BindControlsToObject(user);

            List<OLocation> locations = new List<OLocation>();
            foreach (OPosition position in AppSession.User.GetPositionsByObjectType("OUser"))
            {
                foreach (OLocation location in position.LocationAccess)
                    locations.Add(location);
            }            
            DialogBox_PositionItem.DataSource = OPosition.GetPositionsAtOrBelowLocations(locations);
            DialogBox_PositionItem.DataBind();
            DialogBox_PositionItem.Show();
        }
    }

    

    protected void DialogBox_PositionItem_Selected(object sender, EventArgs e)
    {
        OUser user = panel.SessionObject as OUser;
        panel.ObjectPanel.BindControlsToObject(user);

        foreach (Guid key in DialogBox_PositionItem.SelectedDataKeys)
        {
            OUserPermanentPosition found = user.PermanentPositions.Find(x => x.PositionID == new Guid(key.ToString()));
            if (found == null)
            {
                OUserPermanentPosition p = TablesLogic.tUserPermanentPosition.Create();
                p.PositionID = new Guid(key.ToString());
                user.PermanentPositions.Add(p);
            }
        }

        panel.ObjectPanel.BindObjectToControls(user);
        DialogBox_PositionItem.Visible = false;
        DialogBox_PositionItem.Hide();
    }

    protected void buttonDelegateCancel_Click(object sender, EventArgs e)
    {
    }

    protected void buttonDelegateConfirm_Click(object sender, EventArgs e)
    {

    }

    protected void IsActiveDirectoryUser_checkChanged(object sender, EventArgs e)
    {

    }

    protected void gridDelegatedToOthersPositions_Action(object sender, string commandName, List<object> dataKeys)
    {
        if (commandName == "RemoveObject")
        {
            OUser u = (OUser)panel.SessionObject;
            tabPosition.BindControlsToObject(u);

            // Bug Fix: fixed the problem that delete button next to individual line item
            // cannot work properly.
            // Wang Yiyuan, 09/04/2012
            //
            //foreach (Guid id in gridDelegatedToOthersPositions.GetSelectedKeys())
            foreach (Guid id in dataKeys)
                u.DelegatedToOthersPositions.RemoveGuid(id);

            tabPosition.BindObjectToControls(u);
        }
    }


    protected void objectPanelDelegatePositions_ButtonClicked(object sender, ButtonClickedEventArgs e)
    {
        if (e.CommandName == "Confirm")
        {
            if (!objectPanelDelegatePositions.IsValid)
                return;
            OUser u = (OUser)panel.SessionObject;
            tabPosition.BindControlsToObject(u);

            List<object> permanentPositionIds = gv_Position.GetSelectedKeys();

            foreach (Guid permanentPositionId in permanentPositionIds)
            {
                OUserPermanentPosition permanentPosition = u.PermanentPositions.Find(permanentPositionId);

                if (permanentPosition != null)
                {
                    OUserDelegatedPosition dp = TablesLogic.tUserDelegatedPosition.Create();
                    dp.DelegatedByUserID = u.ObjectID;
                    dp.UserID = new Guid(dropGrantedToUser.SelectedValue);
                    dp.PositionID = permanentPosition.PositionID;
                    dp.StartDate = dateDelegateStartDate.DateTime;
                    dp.EndDate = dateDelegateEndDate.DateTime;
                    u.DelegatedToOthersPositions.Add(dp);
                }
            }
            tabPosition.BindObjectToControls(u);

            objectPanelDelegatePositions.Hide();
            //popupDelegatePositions.Hide();
        }
    }

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta http-equiv="pragma" content="no-cache" />
    <link rel='stylesheet' href='../../../css/tipso.css' />
</head>
<body class="body-bg">
    <form id="form1" runat="server">
    <ui:UIObjectPanel runat="server" ID="panelMain" meta:resourcekey="panelMainResource"
        BorderStyle="NotSet">
        <web:object runat="server" ID="panel" Caption="User" BaseTable="tUser" meta:resourcekey="panelResource"
            OnPopulateForm="panel_PopulateForm" OnValidateAndSave="panel_ValidateAndSave">
        </web:object>

            <div class="mdl-edit-div">
                <ui:UITabStrip runat="server" ID="tabObject" meta:resourcekey="tabObjectResource"
                    BorderStyle="NotSet" SkinID="tabstripEdit">
                    <web:base runat="server" ID="objectBase" ObjectNumberVisible="false" ObjectNameCaption="User Name"
                        ObjectNameMaxLength="50" meta:resourcekey="objectBaseResource"></web:base>
                    <ui:UITabView runat="server" ID="tabDetails" Caption="Details" meta:resourcekey="tabDetailsResource"
                        BorderStyle="NotSet">
                        <ui:UISeparator runat='server' ID="UISeparator3" Caption="Details"  meta:resourcekey="UISeparator3Resource" />
                        <ui:UIFieldTextBox runat="server" ID="textDescription" PropertyName="Description"
                            Caption="Description" MaxLength="255" InternalControlWidth="95%" meta:resourcekey="textDescriptionResource">
                        </ui:UIFieldTextBox>
                        <ui:UIFieldTextBox runat="server" ID="textDesignation" PropertyName="Designation"
                            Caption="Description" MaxLength="50" Span="Half" meta:resourcekey="textDesignationResource">
                        </ui:UIFieldTextBox>
                        <ui:UIFieldTextBox runat="server" ID="textDepartment" PropertyName="Department"
                            Caption="Description" MaxLength="50" Span="Half" meta:resourcekey="textDepartmentResource">
                        </ui:UIFieldTextBox>
                        <ui:UIFieldTextBox runat="server" ID="uifieldstring4" PropertyName="UserBase.Cellphone"
                            Caption="Cell Phone" Span="Half" meta:resourcekey="uifieldstring4Resource" InternalControlWidth="95%" />
                        <ui:UIFieldTextBox runat="server" ID="textEmail" PropertyName="UserBase.Email" Caption="Email"
                            Span="Half" meta:resourcekey="textEmailResource" InternalControlWidth="95%" />
                        <ui:UIFieldLabel runat="server" ID="label1" Span="Half" Visible="false" meta:resourcekey="label1Resource" ></ui:UIFieldLabel>
                        <ui:UIFieldLabel runat="server" ID="labelEmailErrorMsg" Span="Half" Text = "" Visible="false" meta:resourcekey="labelEmailErrorMsgResource" ></ui:UIFieldLabel>
                        <ui:UIFieldTextBox runat="server" ID="uifieldstring6" PropertyName="UserBase.Fax"
                            Caption="Fax" Span="Half" meta:resourcekey="uifieldstring6Resource" InternalControlWidth="95%" />
                        <ui:UIFieldTextBox runat="server" ID="UIFieldTextBox1" PropertyName="UserBase.Phone"
                            Caption="Phone" Span="Half" meta:resourcekey="UIFieldTextBox1Resource" InternalControlWidth="95%" />
                        <ui:UIFieldTextBox runat="server" ID="uifieldstring7" PropertyName="UserBase.AddressCountry"
                            Caption="Country" Span="Half" meta:resourcekey="uifieldstring7Resource" MaxLength="255"
                            InternalControlWidth="95%" />
                        <ui:UIFieldTextBox runat="server" ID="uifieldstring8" PropertyName="UserBase.AddressState"
                            Caption="State" Span="Half" meta:resourcekey="uifieldstring8Resource" MaxLength="255"
                            InternalControlWidth="95%" />
                        <ui:UIFieldTextBox runat="server" ID="uifieldstring9" PropertyName="UserBase.AddressCity"
                            Caption="City" Span="Half" meta:resourcekey="uifieldstring9Resource" MaxLength="255"
                            InternalControlWidth="95%" />
                        <ui:UIFieldTextBox runat="server" ID="uifieldstring10" PropertyName="UserBase.Address"
                            Caption="Address" Span="Half" meta:resourcekey="uifieldstring10Resource" MaxLength="255"
                            InternalControlWidth="95%" />
                        <ui:UIFieldDropDownList runat="server" ID="Craft" PropertyName="CraftID" Caption="Craft"
                            DataTextField="Name" Span="Half" ToolTip="The craft this technician belongs to."
                            meta:resourcekey="CraftResource">
                        </ui:UIFieldDropDownList>
                        <br />
                        <ui:UISeparator runat='server' ID="UISeparator1" meta:resourcekey="UISeparator1Resource" />
                        <ui:UIFieldDropDownList runat="server" ID="ddl_Language" PropertyName="LanguageName"
                            Caption="Language" ToolTip="Changes to the theme will only take effect on the next logon."
                            meta:resourcekey="ddl_LanguageResource" ValidateRequiredField="True">
                        </ui:UIFieldDropDownList>

                        <ui:UIFieldDropDownList runat='server' ID="firstPageDropdown" PropertyName="FirstPage"
                            Caption="First Page" MaxLength="255" ToolTip="Specifies the first page to display upon launch of the application."
                            InternalControlWidth="95%" >
                            <Items>
                                <asp:ListItem Text="Inbox" Value="0"/>
                                <asp:ListItem Text="Pinboard" Value="1"/>
                            </Items>
                        </ui:UIFieldDropDownList>
                        <ui:UIFieldDropDownList runat='server' ID="themeDropdown" PropertyName="Theme"
                            Caption="Theme" MaxLength="255" ToolTip="The design of the layout"
                            InternalControlWidth="95%" >
                            <Items>
                                <asp:ListItem Text="Blue" Value="0"/>
                                <asp:ListItem Text="Green" Value="1"/>
                                <asp:ListItem Text="Yellow" Value="2"/>
                                <asp:ListItem Text="Red" Value="3"/>
                                <asp:ListItem Text="Black" Value="4"/>
                                <asp:ListItem Text="White" Value="5"/>
                            </Items>
                        </ui:UIFieldDropDownList>
                        <ui:UIHint runat="server" ID="LanguageHint" Text="Changes to the language and theme will take effect on your next login">
                            
                        </ui:UIHint>


                    </ui:UITabView>
                    <ui:UITabView runat="server" ID="tabPosition" Caption="Positions" meta:resourcekey="tabPositionResource"
                        BorderStyle="NotSet" SkinID="tabviewEdit">
                        <ui:UISeparator runat='server' ID="UISeparator2" Caption="Positions"  meta:resourcekey="UISeparator2Resource" />
                        <ui:UIHint runat="server" ID="hintDelegation" meta:resourcekey="hintDelegationResource"
                            Visible="false" Text="
                                &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;If you are going on leave from work and are unable to access your account, 
                                you can delegate all or part of your granted positions to someone else in your 
                                company.&lt;br __designer:mapid=&quot;a0&quot; /&gt;&lt;br __designer:mapid=&quot;a1&quot; /&gt; 
                                To do so, select the positions you want to delegate and click on the 'Delegate Selected Positions' button. Then select the person in your company followed by the valid period (usually the time that you are away) you'd like that person to take over those positions.&lt;br __designer:mapid=&quot;a2&quot; /&gt;&lt;br __designer:mapid=&quot;a3&quot; /&gt;
                                NOTE: You can only delegate positions that you have been granted.
                            ">
                        </ui:UIHint>
                        <ui:UIGridView runat="server" ID="gv_Position" PropertyName="PermanentPositions"
                            OnAction="gv_Position_Action" Caption="Granted Positions" ValidateRequiredField="True"
                            KeyName="ObjectID" BindObjectsToRows="True" meta:resourcekey="gv_PositionResource"
                            DataKeyNames="ObjectID" GridLines="Both" RowErrorColor="" Style="clear: both;"
                            ImageRowErrorUrl="" >
                            <PagerSettings Mode="NumericFirstLast" />
                            <Commands>
                                <ui:UIGridViewCommand AlwaysEnabled="False" CausesValidation="False" CommandName="AddObjectPosition"
                                    CommandText="Add Positions" ImageUrl="~/images/add.gif"  meta:resourcekey="gv_PositionUIGridViewCommandAddPositionsResource" />
                                <ui:UIGridViewCommand AlwaysEnabled="False" CausesValidation="False" CommandName="RemoveObject"
                                    CommandText="Remove" ConfirmText="Are you sure you wish to remove the selected items?"
                                    ImageUrl="~/images/delete.gif" meta:resourcekey="gv_PositionUIGridViewCommandRemoveResource" />
                                <ui:UIGridViewCommand AlwaysEnabled="True" CausesValidation="False" CommandName="DelegatePosition"
                                    CommandText="Delegate Selected Positions" ImageUrl="~/images/right.png" meta:resourcekey="gv_PositionUIGridViewCommandDelegateSelectedPositionsResource"
                                    />
                                
                            </Commands>
                            <Columns>
                                <ui:UIGridViewButtonColumn ButtonType="Image" CommandName="RemoveObject" ConfirmText="Are you sure you wish to remove this item?"
                                    ImageUrl="~/images/delete.gif" meta:resourcekey="gv_PositionUIGridViewButtonColumnRemoveObjectResource">
                                    <HeaderStyle HorizontalAlign="Left" Width="16px" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </ui:UIGridViewButtonColumn>
                                <ui:UIGridViewBoundColumn DataField="Position.ObjectName" HeaderText="Name" meta:resourcekey="gv_PositionUIGridViewBoundColumnPositionObjectNameResource"
                                    PropertyName="Position.ObjectName" ResourceAssemblyName="" SortExpression="Position.ObjectName" LinkedObjectPropertyName ="Position">
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </ui:UIGridViewBoundColumn>
                                <ui:UIGridViewTemplateColumn HeaderText="Start Date" meta:resourcekey="gv_PositionUIGridViewTemplateColumnStartDateResource">
                                    <ItemTemplate>
                                        <ui:UIFieldDateTime ID="textStartDate" runat="server" Caption="Start Date" FieldLayout="Flow"
                                            PropertyName="StartDate" ShowCaption="False" ValidateCompareField="True"
                                            ValidationCompareControl="textEndDate" ValidationCompareOperator="LessThanEqual"
                                            ValidationCompareType="Date" meta:resourcekey="textStartDateResource" ShowDateControls="True">
                                        </ui:UIFieldDateTime>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="180px" />
                                </ui:UIGridViewTemplateColumn>
                                <ui:UIGridViewTemplateColumn HeaderText="End Date" meta:resourcekey="textStartDateUIGridViewTemplateColumnEndDateResource">
                                    <ItemTemplate>
                                        <ui:UIFieldDateTime ID="textEndDate" runat="server" Caption="End Date" FieldLayout="Flow"
                                            PropertyName="EndDate" ShowCaption="False" ValidateCompareField="True"
                                            ValidationCompareControl="textStartDate" ValidationCompareOperator="GreaterThanEqual"
                                            ValidationCompareType="Date" meta:resourcekey="textEndDateResource" ShowDateControls="True">
                                        </ui:UIFieldDateTime>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="180px" />
                                </ui:UIGridViewTemplateColumn>
                            </Columns>
                        </ui:UIGridView>
                        <ui:UIGridView runat='server' ID="gridDelegatedByOthersPositions" PropertyName="DelegatedByOthersPositions"
                            Caption="Positions Delegated to Me" CheckBoxColumnVisible="False" DataKeyNames="ObjectID"
                            GridLines="Both" RowErrorColor="" Style="clear: both;" meta:resourcekey="gridDelegatedByOthersPositionsResource"
                            >
                            <PagerSettings Mode="NumericFirstLast" />
                            <Columns>
                                <ui:UIGridViewBoundColumn DataField="DelegatedByUser.ObjectName" HeaderText="User"
                                    PropertyName="DelegatedByUser.ObjectName" ResourceAssemblyName="" SortExpression="DelegatedByUser.ObjectName" LinkedObjectPropertyName ="DelegatedByUser" meta:resourcekey="gridDelegatedByOthersPositionsUIGridViewBoundColumnDelegatedByUserObjectNameResource" >
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </ui:UIGridViewBoundColumn>
                                <ui:UIGridViewBoundColumn DataField="Position.ObjectName" HeaderText="Position"
                                    PropertyName="Position.ObjectName" ResourceAssemblyName="" SortExpression="Position.ObjectName"
                                    meta:resourcekey="gridDelegatedByOthersPositionsUIGridViewBoundColumnPositionObjectNameResource" LinkedObjectPropertyName ="Position">
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </ui:UIGridViewBoundColumn>
                                <ui:UIGridViewBoundColumn DataField="StartDate" DataFormatString="{0:dd-MMM-yyyy}"
                                    HeaderText="Start Date" PropertyName="StartDate" ResourceAssemblyName="" SortExpression="StartDate"
                                    meta:resourcekey="gridDelegatedByOthersPositionsUIGridViewBoundColumnStartDateResource">
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </ui:UIGridViewBoundColumn>
                                <ui:UIGridViewBoundColumn DataField="EndDate" DataFormatString="{0:dd-MMM-yyyy}"
                                    HeaderText="End Date" PropertyName="EndDate" ResourceAssemblyName="" SortExpression="EndDate"
                                    meta:resourcekey="gridDelegatedByOthersPositionsUIGridViewBoundColumnEndDateResource">
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </ui:UIGridViewBoundColumn>
                            </Columns>
                        </ui:UIGridView>
                        <ui:UIGridView runat='server' ID="gridDelegatedToOthersPositions" PropertyName="DelegatedToOthersPositions"
                            Caption="Positions Delegated to Other Users" OnAction="gridDelegatedToOthersPositions_Action"
                            DataKeyNames="ObjectID" GridLines="Both" RowErrorColor="" Style="clear: both;"
                            ImageRowErrorUrl="" meta:resourcekey="gridDelegatedToOthersPositionsResource"
                            >
                            <PagerSettings Mode="NumericFirstLast" />
                            <Commands>
                                <ui:UIGridViewCommand AlwaysEnabled="True" CausesValidation="False" CommandName="RemoveObject"
                                    CommandText="Remove" ConfirmText="Are you sure you wish to remove the selected items?"
                                    ImageUrl="~/images/delete.gif" meta:resourcekey="gridDelegatedToOthersPositionsUIGridViewCommandRemoveResource" />
                            </Commands>
                            <Columns>
                                <ui:UIGridViewButtonColumn AlwaysEnabled="True" ButtonType="Image" CommandName="RemoveObject"
                                    ConfirmText="Are you sure you wish to remove this item?" ImageUrl="~/images/delete.gif"
                                    meta:resourcekey="gridDelegatedToOthersPositionsUIGridViewButtonColumnRemoveObjectResource">
                                    <HeaderStyle HorizontalAlign="Left" Width="16px" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </ui:UIGridViewButtonColumn>
                                <ui:UIGridViewBoundColumn DataField="User.ObjectName" HeaderText="User" PropertyName="User.ObjectName"
                                    ResourceAssemblyName="" SortExpression="User.ObjectName" meta:resourcekey="gridDelegatedToOthersPositionsUIGridViewBoundColumnUserObjectNameResource" LinkedObjectPropertyName ="User">
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </ui:UIGridViewBoundColumn>
                                <ui:UIGridViewBoundColumn DataField="Position.ObjectName" HeaderText="Position"
                                    PropertyName="Position.ObjectName" ResourceAssemblyName="" SortExpression="Position.ObjectName"
                                    meta:resourcekey="gridDelegatedToOthersPositionsUIGridViewBoundColumnPositionObjectNameResource" LinkedObjectPropertyName ="Position">
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </ui:UIGridViewBoundColumn>
                                <ui:UIGridViewBoundColumn DataField="StartDate" DataFormatString="{0:dd-MMM-yyyy}"
                                    HeaderText="Start Date" PropertyName="StartDate" ResourceAssemblyName="" SortExpression="StartDate"
                                    meta:resourcekey="gridDelegatedToOthersPositionsUIGridViewBoundColumnStartDateResource">
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </ui:UIGridViewBoundColumn>
                                <ui:UIGridViewBoundColumn DataField="EndDate" DataFormatString="{0:dd-MMM-yyyy}"
                                    HeaderText="End Date" PropertyName="EndDate" ResourceAssemblyName="" SortExpression="EndDate"
                                    meta:resourcekey="gridDelegatedToOthersPositionsUIGridViewBoundColumnEndDateResource">
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </ui:UIGridViewBoundColumn>
                            </Columns>
                        </ui:UIGridView>
                    </ui:UITabView>
                
                    <ui:UISearchDialogBox runat="server" ID="DialogBox_PositionItem" 
                            SearchBehavior="DataBound" Title="User Positions" AllowMultipleSelection="true"
                            SearchOnDemandPropertyNames="ObjectName" SearchOnDemandAutoSearchOnShow="true" OnSelected="DialogBox_PositionItem_Selected" SearchOnDemandMaximumNumberOfResults="500" meta:resourcekey="DialogBox_PositionItemResource" >
                            <Columns>
                                <ui:UIGridViewBoundColumn HeaderStyle-Width="800" HeaderText="User Position Name" PropertyName="ObjectName"
                                    SortExpression="ObjectName" meta:resourcekey="gridDelegatedToOthersPositionsUIGridViewBoundColumnObjectNameResource" >
                                </ui:UIGridViewBoundColumn>
                            </Columns>
                    </ui:UISearchDialogBox>
                    
                    <ui:UITabView runat="server" ID="tabCredentials" Caption="Credentials" meta:resourcekey="tabCredentialsResource"
                        BorderStyle="NotSet" SkinID="tabviewEdit">
                        <ui:UISeparator runat='server' ID="UISeparator4" Caption="Credentials"  meta:resourcekey="UISeparator4Resource" />
                        <ui:UIFieldCheckBox runat="server" ID="checkIsBanned" Caption="Inactivate Account?"
                            Text="Yes, inactivate this account so that user CANNOT log on to the system." OnCheckedChanged="checkIsBanned_CheckedChanged"
                            TextAlign="Right" meta:resourcekey="checkIsBannedResource" >
                        </ui:UIFieldCheckBox>
                        <ui:UIFieldDateTime ID="BannedDateFrom" runat="server" Caption="Inactive Date" PropertyName="BannedDateFrom" meta:resourcekey="BannedDateFromResource" ></ui:UIFieldDateTime>
                        <ui:UIFieldTextBox runat="server" ID="UserLoginName" PropertyName="UserBase.LoginName"
                            Caption="Login Name" ValidateRequiredField="True" meta:resourcekey="UserLoginNameResource"
                            InternalControlWidth="95%" />
                        <ui:UIFieldCheckBox runat="server" ID="IsActiveDirectoryUser" Caption="Active Directory"
                            PropertyName="IsActiveDirectoryUser" Text="Yes, this user is an Active Directory User"
                            OnCheckedChanged="IsActiveDirectoryUser_checkChanged" meta:resourcekey="IsActiveDirectoryUserResource"
                            TextAlign="Right" Visible="false">
                        </ui:UIFieldCheckBox>
                        <ui:UIFieldTextBox runat="server" ID="ActiveDirectoryDomain" PropertyName="ActiveDirectoryDomain"
                            Caption="Active Directory Domain" ValidateRequiredField="True" meta:resourcekey="ActiveDirectoryDomainResource"
                            InternalControlWidth="95%" />
                        <ui:UIPanel runat="server" ID="panelPassword" meta:resourcekey="panelPasswordResource" >
                            <ui:UISeparator runat="server" ID="sep1" meta:resourcekey="sep1Resource" />
                            <ui:UIFieldCheckBox runat="server" ID="chkResetPassword" Caption="Reset Password"
                                Text="Yes, generate a new password and send it via e-mail to the user when I save."
                                OnCheckedChanged="chkResetPassword_CheckedChange" meta:resourcekey="chkResetPasswordResource"
                                TextAlign="Right" />
                            <ui:UIFieldTextBox runat="server" ID="Password1" Caption="Password" TextMode="Password"
                                Span="Half" meta:resourcekey="Password1Resource" MaxLength="30" InternalControlWidth="95%" />
                            <ui:UIFieldTextBox runat="server" ID="Password2" Caption="Confirm Password" TextMode="Password"
                                Span="Half" meta:resourcekey="Password2Resource" MaxLength="30" InternalControlWidth="95%" />
                        </ui:UIPanel>
                        <br />
                        <br />
                    </ui:UITabView>

                    <ui:UITabView runat="server" ID="tabMemo" Caption="Comments"
                        BorderStyle="NotSet" SkinID="tabviewEdit" meta:resourcekey="tabMemoResource" >
                        <web:memo ID="Memo1" runat="server" meta:resourcekey="Memo1Resource" ></web:memo>
                    </ui:UITabView>
                    <ui:UITabView runat="server" ID="tabAttachments" Caption="Attachments" meta:resourcekey="tabAttachmentsResource"
                        BorderStyle="NotSet" SkinID="tabviewEdit">
                        <web:attachments runat="server" ID="attachments" meta:resourcekey="attachmentsResource" ></web:attachments>
                    </ui:UITabView>
                    <web:audit runat="server" ID="Audit"  meta:resourcekey="AuditResource" />
                </ui:UITabStrip>
                <web:object2 runat="server" ID="panel2"  meta:resourcekey="panel2Resource" />
                <ui:UIDialogBox runat="server" ID="objectPanelDelegatePositions" Title="Delegate Selected Positions" 
                    Button1CommandName="Confirm" Button1Text="Confirm" Button1AutoClosesDialogBox="false" Button1CausesValidation="true"
                    Button2CommandName="Cancel" Button2Text="Cancel"  Button2AutoClosesDialogBox="true" Button2CausesValidation="false"
                    OnButtonClicked="objectPanelDelegatePositions_ButtonClicked" meta:resourcekey="objectPanelDelegatePositionsResource" >
                    <div>
                        <ui:UIFieldSearchableDropDownList runat="server" ID="dropGrantedToUser" Caption="User"
                            ValidateRequiredField="True" meta:resourcekey="dropGrantedToUserResource" SearchInterval="300">
                        </ui:UIFieldSearchableDropDownList>
                        <ui:UIFieldDateTime runat="server" ID="dateDelegateStartDate" Caption="Start Date"
                            ValidateRequiredField="True" meta:resourcekey="dateDelegateStartDateResource"
                            ShowDateControls="True">
                        </ui:UIFieldDateTime>
                        <ui:UIFieldDateTime runat="server" ID="dateDelegateEndDate" Caption="End Date" ValidateRequiredField="True"
                            meta:resourcekey="dateDelegateEndDateResource" ShowDateControls="True">
                        </ui:UIFieldDateTime>
                    </div>
                </ui:UIDialogBox>
            </div>
    </ui:UIObjectPanel>
    </form>
</body>
</html>
