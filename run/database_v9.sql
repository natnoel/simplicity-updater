set nocount on
declare @xmls nvarchar(max)
print 'Synching: Dashboard (21 of 642)'
   set @xmls = cast('' as nvarchar(max)) + 
      '<sch><nm>CatagoryColumnName</nm><d>nvarchar(255)</d><t>nvarchar</t><s>255</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>CreatedDateTime</nm><d>datetime</d><t>datetime</t><s>8</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>CreatedUser</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>CSharpMethodName</nm><d>nvarchar(255)</d><t>nvarchar</t><s>255</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>DashboardQuery</nm><d>ntext</d><t>ntext</t><s>16777216</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>DashboardType</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>Height</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>IsDeleted</nm><d>bit</d><t>bit</t><s>1</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>ModifiedDateTime</nm><d>datetime</d><t>datetime</t><s>8</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>ModifiedUser</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>ObjectID</nm><d>uniqueidentifier</d><t>uniqueidentifier</t><s>16</s><p>0</p><l>0</l><n>N</n><pk>Y</pk><part></part></sch>' + 
      '<sch><nm>ObjectName</nm><d>nvarchar(255)</d><t>nvarchar</t><s>255</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>ObjectNumber</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>SeriesByColumns</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>SeriesColumnName</nm><d>nvarchar(255)</d><t>nvarchar</t><s>255</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>ShowLegend</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>ShowSummaryProgressBar</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>SummaryLinkedReportID</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>SummaryProgressBarTitle</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>SummaryTitle</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>TestField</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>TrancateAxisLabels</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>UseCSharpQuery</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>ValueColumnName</nm><d>nvarchar(255)</d><t>nvarchar</t><s>255</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>VersionNumber</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>Width</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
   '';
   exec sp_schemasynctableandcolumns 'Dashboard', @xmls
print '    Table: Dashboard synchronized'
print 'Synching: ApplicationSetting (331 of 642)'
   set @xmls = cast('' as nvarchar(max)) + 
      '<sch><nm>ActiveDirectoryDomain</nm><d>nvarchar(255)</d><t>nvarchar</t><s>255</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>ActiveDirectoryPath</nm><d>nvarchar(255)</d><t>nvarchar</t><s>255</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>ActiveDirectoryUserGroupNames</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>ADDomainName</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>ADLogOnDomainAccount</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>ADLogOnPassword</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>AllowConcurrentLogin</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>AllowedFileTypeExtension</nm><d>nvarchar(255)</d><t>nvarchar</t><s>255</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>AllowPerLineItemTaxInPO</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>APInvoiceRecoverableAfterApprovalStates</nm><d>nvarchar(255)</d><t>nvarchar</t><s>255</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>APInvoiceRecoverableBeforeApprovalStates</nm><d>nvarchar(255)</d><t>nvarchar</t><s>255</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>BackgroundServiceAdminEmail</nm><d>nvarchar(255)</d><t>nvarchar</t><s>255</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>BaseCurrencyID</nm><d>uniqueidentifier</d><t>uniqueidentifier</t><s>16</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>CaseEventAfterDocumentClosedOrCancelled</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>ContactUsText</nm><d>nvarchar(1000)</d><t>nvarchar</t><s>1000</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>CreatedDateTime</nm><d>datetime</d><t>datetime</t><s>8</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>CreatedUser</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>DefaultBudgetDeductionPolicy</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>DefaultBudgetSpendingPolicy</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>DefaultEndCount</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>DefaultEndUnit</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>DefaultNumberOfDaysInAdvanceToCreateFixedWorks</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>DefaultRequiredCount</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>DefaultRequiredUnit</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>DefaultScheduledWorkTypeOfWorkID</nm><d>uniqueidentifier</d><t>uniqueidentifier</t><s>16</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>DefaultTypeOfWorkID</nm><d>uniqueidentifier</d><t>uniqueidentifier</t><s>16</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>DummyLoginName</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>DummyPassword</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>DummyUsername</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>EmailDomain</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>EmailExchangeWebServiceUrl</nm><d>nvarchar(255)</d><t>nvarchar</t><s>255</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>EmailPassword</nm><d>nvarchar(255)</d><t>nvarchar</t><s>255</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>EmailPort</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>EmailServer</nm><d>nvarchar(255)</d><t>nvarchar</t><s>255</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>EmailServerType</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>EmailUserName</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>EnableEmail</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>EnableReceiveEmail</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>EnableSms</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>EnableSSL</nm><d>bit</d><t>bit</t><s>1</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>EquipmentTemplateID</nm><d>uniqueidentifier</d><t>uniqueidentifier</t><s>16</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>EquipmentUnitOfMeasureID</nm><d>uniqueidentifier</d><t>uniqueidentifier</t><s>16</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>ExcelReaderUseWebService</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>ExcelReaderWebServiceURL</nm><d>nvarchar(255)</d><t>nvarchar</t><s>255</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>GetUserEmailFromActiveDirectory</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>HomePageUrl</nm><d>nvarchar(255)</d><t>nvarchar</t><s>255</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>InventoryDefaultCostingType</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>IsDeleted</nm><d>bit</d><t>bit</t><s>1</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>IsImpersonateAccountForReport</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>IsUserEmailCompulsory</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>IsUsingActiveDirectory</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>IsUsingCaptcha</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>LoginLogoID</nm><d>uniqueidentifier</d><t>uniqueidentifier</t><s>16</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>LoginTitle</nm><d>nvarchar(255)</d><t>nvarchar</t><s>255</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>LogReportGeneration</nm><d>bit</d><t>bit</t><s>1</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>MaxUnauthorizedAccessCount</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>MessageEmailCC</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>MessageEmailSender</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>MessageNumberOfTries</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>MessageSmsBaudRate</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>MessageSmsComPort</nm><d>nvarchar(10)</d><t>nvarchar</t><s>10</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>MessageSmsDataBits</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>MessageSmsDeleteCommands</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>MessageSmsHandshake</nm><d>nvarchar(30)</d><t>nvarchar</t><s>30</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>MessageSmsInitASCIICommand</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>MessageSmsInitCommands</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>MessageSmsInitUCS2Command</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>MessageSmsLocalNumberDigits</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>MessageSmsLogFilePath</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>MessageSmsNewLine</nm><d>nvarchar(5)</d><t>nvarchar</t><s>5</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>MessageSmsParity</nm><d>nvarchar(20)</d><t>nvarchar</t><s>20</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>MessageSmsReceiveCommands</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>MessageSmsSendCommands</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>MessageSmsStopBits</nm><d>nvarchar(20)</d><t>nvarchar</t><s>20</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>MessageSmtpPort</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>MessageSmtpRequiresAuthentication</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>MessageSmtpServer</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>MessageSmtpServerPassword</nm><d>nvarchar(255)</d><t>nvarchar</t><s>255</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>MessageSmtpServerUserName</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>ModifiedDateTime</nm><d>datetime</d><t>datetime</t><s>8</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>ModifiedUser</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>NumberOfDaysToKeepBackgroundServiceLog</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>NumberOfDaysToKeepLoginHistory</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>NumberOfDaysToKeepMessageHistory</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>ObjectID</nm><d>uniqueidentifier</d><t>uniqueidentifier</t><s>16</s><p>0</p><l>0</l><n>N</n><pk>Y</pk><part></part></sch>' + 
      '<sch><nm>ObjectName</nm><d>nvarchar(255)</d><t>nvarchar</t><s>255</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>ObjectNumber</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>PasswordDaysToExpiry</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>PasswordHistoryKept</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>PasswordMaximumTries</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>PasswordMinimumAge</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>PasswordMinimumLength</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>PasswordRequiredCharacters</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>PaymentModeCashID</nm><d>uniqueidentifier</d><t>uniqueidentifier</t><s>16</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>PaymentModeChequeID</nm><d>uniqueidentifier</d><t>uniqueidentifier</t><s>16</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>PerformanceNumberOfConcurrentReports</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>PerformanceSaturdayWorkingHoursEndTime</nm><d>datetime</d><t>datetime</t><s>8</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>PerformanceSaturdayWorkingHoursStartTime</nm><d>datetime</d><t>datetime</t><s>8</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>PerformanceSundayWorkingHoursEndTime</nm><d>datetime</d><t>datetime</t><s>8</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>PerformanceSundayWorkingHoursStartTime</nm><d>datetime</d><t>datetime</t><s>8</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>PerformanceWeekdayWorkingHoursEndTime</nm><d>datetime</d><t>datetime</t><s>8</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>PerformanceWeekdayWorkingHoursStartTime</nm><d>datetime</d><t>datetime</t><s>8</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>PinboardPageUrl</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>PORecoverableAfterApprovalStates</nm><d>nvarchar(255)</d><t>nvarchar</t><s>255</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>PORecoverableBeforeApprovalStates</nm><d>nvarchar(255)</d><t>nvarchar</t><s>255</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>PosToPrmsPath</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>PrmsToPosPath</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>RecoverableMode</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>ReportDomain</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>ReportPassword</nm><d>nvarchar(255)</d><t>nvarchar</t><s>255</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>ReportSystemName</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>ReportUserName</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>RFQRecoverableAfterApprovalStates</nm><d>nvarchar(255)</d><t>nvarchar</t><s>255</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>RFQRecoverableBeforeApprovalStates</nm><d>nvarchar(255)</d><t>nvarchar</t><s>255</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>SafetyIncidentNumberOfMCDays</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>SMSRelayWSURL</nm><d>nvarchar(255)</d><t>nvarchar</t><s>255</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>SMSSendType</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>SupplierPortalIsUnderMaintenace</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>SurveyURL</nm><d>nvarchar(255)</d><t>nvarchar</t><s>255</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>SystemUrl</nm><d>nvarchar(255)</d><t>nvarchar</t><s>255</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>TraceUserAction</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>UseImpersonatorToAD</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>VendorPortalURL</nm><d>nvarchar(1000)</d><t>nvarchar</t><s>1000</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>VersionNumber</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>WebAppIsUnderMaintenace</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>WORecoverableAfterApprovalStates</nm><d>nvarchar(255)</d><t>nvarchar</t><s>255</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>WORecoverableBeforeApprovalStates</nm><d>nvarchar(255)</d><t>nvarchar</t><s>255</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
   '';
   exec sp_schemasynctableandcolumns 'ApplicationSetting', @xmls
print '    Table: ApplicationSetting synchronized'
print 'Synching: User (353 of 642)'
   set @xmls = cast('' as nvarchar(max)) + 
      '<sch><nm>ActiveDirectoryDomain</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>BannedDateFrom</nm><d>datetime</d><t>datetime</t><s>8</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>CraftID</nm><d>uniqueidentifier</d><t>uniqueidentifier</t><s>16</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>CreatedDateTime</nm><d>datetime</d><t>datetime</t><s>8</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>CreatedUser</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>Department</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>Description</nm><d>nvarchar(255)</d><t>nvarchar</t><s>255</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>Designation</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>FirstPage</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>HidePinboardMessageIndicator</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>IsActiveDirectoryUser</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>IsBanned</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>IsDeleted</nm><d>bit</d><t>bit</t><s>1</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>IsPasswordChangeRequired</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>LanguageName</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>LastLoginTime</nm><d>datetime</d><t>datetime</t><s>8</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>LastLoginTimeForAutoBanned</nm><d>datetime</d><t>datetime</t><s>8</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>LoginRetries</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>ModifiedDateTime</nm><d>datetime</d><t>datetime</t><s>8</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>ModifiedUser</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>ObjectID</nm><d>uniqueidentifier</d><t>uniqueidentifier</t><s>16</s><p>0</p><l>0</l><n>N</n><pk>Y</pk><part></part></sch>' + 
      '<sch><nm>ObjectName</nm><d>nvarchar(255)</d><t>nvarchar</t><s>255</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>ObjectNumber</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>PasswordLastChanged</nm><d>datetime</d><t>datetime</t><s>8</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>SessionID</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>SiteID</nm><d>uniqueidentifier</d><t>uniqueidentifier</t><s>16</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>SuperiorID</nm><d>uniqueidentifier</d><t>uniqueidentifier</t><s>16</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>Theme</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>UserAssignmentID</nm><d>uniqueidentifier</d><t>uniqueidentifier</t><s>16</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>UserBaseID</nm><d>uniqueidentifier</d><t>uniqueidentifier</t><s>16</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>UserType</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>VendorID</nm><d>uniqueidentifier</d><t>uniqueidentifier</t><s>16</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>VersionNumber</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
   '';
   exec sp_schemasynctableandcolumns 'User', @xmls
print '    Table: User synchronized'
print 'Synching: UserSearchGridViewPreference (361 of 642)'
   set @xmls = cast('' as nvarchar(max)) + 
      '<sch><nm>Columns</nm><d>nvarchar(1000)</d><t>nvarchar</t><s>1000</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>CreatedDateTime</nm><d>datetime</d><t>datetime</t><s>8</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>CreatedUser</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>IsDeleted</nm><d>bit</d><t>bit</t><s>1</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>MaxResultCount</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>ModifiedDateTime</nm><d>datetime</d><t>datetime</t><s>8</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>ModifiedUser</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>ObjectID</nm><d>uniqueidentifier</d><t>uniqueidentifier</t><s>16</s><p>0</p><l>0</l><n>N</n><pk>Y</pk><part></part></sch>' + 
      '<sch><nm>ObjectName</nm><d>nvarchar(255)</d><t>nvarchar</t><s>255</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>ObjectNumber</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>ObjectTypeName</nm><d>nvarchar(50)</d><t>nvarchar</t><s>50</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>PagingCount</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>UserID</nm><d>uniqueidentifier</d><t>uniqueidentifier</t><s>16</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
      '<sch><nm>VersionNumber</nm><d>int</d><t>int</t><s>4</s><p>0</p><l>0</l><n>Y</n><pk>N</pk><part></part></sch>' + 
   '';
   exec sp_schemasynctableandcolumns 'UserSearchGridViewPreference', @xmls
print '    Table: UserSearchGridViewPreference synchronized'
set nocount off