<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%@ Page Language="C#" Theme="Corporate" Inherits="PageBase" UICulture="Auto" %>
<%@ Import Namespace="System.Data" %>

<script runat="server">

    DataTable dtMenu;
    DataTable dtReports;

    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);

        // Forces the menu to render correctly in Chrome/Safari
        // browsers.
        //
        this.Page.ClientTarget = "uplevel";
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (!IsPostBack)
        {
            dtMenu = Helpers.GetCachedMenusAccessibleByUser(AppSession.User);
            dtReports = Helpers.GetCachedReportsAccessibleByUser(AppSession.User);

            if (Request.QueryString["url"] == null)
            {
                if (AppSession.User.FirstPage == null || AppSession.User.FirstPage == 0)
                {
                    frameBottom.Attributes["src"] = ResolveUrl(OApplicationSetting.Current.HomePageUrl);
                } else
                {
                    frameBottom.Attributes["src"] = ResolveUrl(OApplicationSetting.Current.PinboardPageUrl);
                }
            }
            else
            {
                frameBottom.Attributes["src"] = Request.QueryString["url"];
            }

            InboxCountLabel.Text = OActivity.GetOutstandingActivitiesForInbox(
                    AppSession.User, new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 23, 59, 59), "%%", "%%", "%%").Rows.Count.ToString();

            AnnouncementsCountLabel.Text = OAnnouncement.GetAnnouncementsTable(AppSession.User, DateTime.Now).Rows.Count.ToString();

            // set up the link URL
            //
            OFunction userFunction = OFunction.GetFunctionByObjectType("OUser");
            string url =
                userFunction.EditUrl +
                (userFunction.EditUrl.Contains("?") ? "&" : "?") +
                "ID=" + HttpUtility.UrlEncode(Security.Encrypt("EDIT:" + AppSession.User.ObjectID.ToString() + ":EDITPROFILE:" + AppSession.SaltID)) +
                "&TYPE=" + HttpUtility.UrlEncode(Security.Encrypt("OUser" + ":" + AppSession.SaltID));

            EditUserProfileButton.Attributes["onclick"] = "openPage('" + this.ResolveUrl(url) + "', 'AnacleEAM_Window'); return false;";

            // Generate the JS initializing script for the auto complete
            //
            string functionNames = "";
            bool first = true;
            foreach (DataRow dr in dtMenu.Rows)
            {
                functionNames += (first ? "" : ",") + "'" + TranslateMenuItem(dr["FunctionName"].ToString()).Replace("'", "\\'") + "'";
                first = false;
            }



        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        string themeStyleSheet = null;

        switch(AppSession.User.Theme)
        {
            case (int)OUser.Themes.Green:
                themeStyleSheet = "~/css/materialsimp-green.css";
                break;
            case (int)OUser.Themes.Yellow:
                themeStyleSheet = "~/css/materialsimp-yellow.css";
                break;
            case (int)OUser.Themes.Red:
                themeStyleSheet = "~/css/materialsimp-red.css";
                break;
            case (int)OUser.Themes.Black:
                themeStyleSheet = "~/css/materialsimp-black.css";
                break;
            case (int)OUser.Themes.White:
                themeStyleSheet = "~/css/materialsimp-white.css";
                break;
            case (int)OUser.Themes.Blue:
            case null:
            default:
                themeStyleSheet = "~/css/materialsimp-blue.css";
                break;
        }

        Page.Header.Controls.Add(
            new System.Web.UI.LiteralControl("<link rel=\"stylesheet\" type=\"text/css\" href=\"" + ResolveUrl(themeStyleSheet) + "\" />"));
    }

    /// <summary>
    /// Translates the specified text from the objects.resx file.
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    protected string TranslateMenuItem(string text)
    {
        if (text == "##Reports##")
            return Resources.Strings.Menu_Reports;

        string translatedText = Resources.Objects.ResourceManager.GetString(text);
        if (translatedText == null || translatedText == "")
            return text;
        return translatedText;
    }


    /// <summary>
    /// Gets the first level category menus
    /// </summary>
    /// <returns></returns>
    protected DataTable GetFirstLevelMenus()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("CategoryName", typeof(string));

        Hashtable ht = new Hashtable();
        foreach (DataRow dr in dtMenu.Rows)
        {
            if (ht[dr["CategoryName"] as String] == null)
            {
                dt.Rows.Add(dr["CategoryName"]);
                ht[dr["CategoryName"] as String] = 1;
            }
        }

        return dt;
    }


    /// <summary>
    /// Gets the second level category menus
    /// </summary>
    /// <returns></returns>
    protected DataTable GetSecondLevelMenu(string categoryName)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("CategoryName", typeof(string));
        dt.Columns.Add("SubCategoryOrFunctionName", typeof(string));
        dt.Columns.Add("ObjectTypeName", typeof(string));
        dt.Columns.Add("MainUrl", typeof(string));
        dt.Columns.Add("MainMobileUrl", typeof(string));
        dt.Columns.Add("HasSubMenu", typeof(int));

        Hashtable ht = new Hashtable();
        foreach (DataRow dr in dtMenu.Rows)
        {
            if (dr["CategoryName"] as String == categoryName)
            {
                if (dr["SubCategoryName"] == DBNull.Value || dr["SubCategoryName"].ToString() == "")
                {
                    dt.Rows.Add(dr["CategoryName"], dr["FunctionName"], dr["ObjectTypeName"], dr["MainUrl"], dr["MainMobileUrl"], 0);
                }
                else
                {
                    if (ht[dr["SubCategoryName"] as String] == null)
                    {
                        dt.Rows.Add(dr["CategoryName"], dr["SubCategoryName"], DBNull.Value, DBNull.Value, DBNull.Value, 1);
                        ht[dr["SubCategoryName"] as String] = 1;
                    }
                }
            }
        }

        return dt;
    }

    /// <summary>
    /// Gets the leaf level category menus
    /// </summary>
    /// <returns></returns>
    protected DataTable GetThirdLevelMenu(string categoryName, string subCategoryName)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("CategoryName", typeof(string));
        dt.Columns.Add("SubCategoryName", typeof(string));
        dt.Columns.Add("FunctionName", typeof(string));
        dt.Columns.Add("ObjectTypeName", typeof(string));
        dt.Columns.Add("MainUrl", typeof(string));
        dt.Columns.Add("MainMobileUrl", typeof(string));

        foreach (DataRow dr in dtMenu.Rows)
        {
            if (dr["CategoryName"] as String == categoryName &&
                dr["SubCategoryName"] as String == subCategoryName)
            {
                dt.Rows.Add(categoryName, subCategoryName, dr["FunctionName"], dr["ObjectTypeName"], dr["MainUrl"], dr["MainMobileUrl"]);
            }
        }
        return dt;

    }

    protected string GetUrl(string objectTypeName, string mainUrl)
    {
        return
            mainUrl + (mainUrl.Contains("?") ? "&" : "?") +
            "TYPE=" +
            HttpUtility.UrlEncode(Security.Encrypt(objectTypeName + ":" + AppSession.SaltID));

    }

    protected void LogoutButton_Click(object sender, EventArgs e)
    {
        OUser user = Session["User"] as OUser;
        if (Session["SessionId"] != null)
        {
            try
            {
                Security.Logoff((Guid)Session["SessionId"]);
            }
            catch
            {
                // Since the user is logging off from the system
                // we should worry about the log off method
                // throwing exceptions due to SQL failures.
                //
                // So we just ignore, and proceed to clear the
                // session and direct the user back to the
                // log in page.
            }
        }
        Session.Clear();

        // AddedBy: Yiyuan
        // AddedDate: 13-Jan-2012
        // Set the user LastAccessTime to null when clicking logoff button
        //
        using (Connection c = new Connection())
        {
            OUser u = TablesLogic.tUser.Load(user.ObjectID);
            //u.LastAccessTime = null;
            u.SessionID = null;
            u.Save();
            c.Commit();
        }

        // 2010.09.27
        // Kim Foong
        // If we are logging on with Windows, then when the user clicks on
        // the log out button, instead of redirecting him/her back to the
        // log in page (which will cause him/her to auto login again), 
        // we simply just close the window.
        //
        if (ConfigurationManager.AppSettings["AuthenticateWithWindowsLogon"].ToLower() == "true")
            Window.Close();
        else
            Response.Redirect(ResolveUrl("~/applogin.aspx"));
    }

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title></title>
</head>

<body style="overflow: hidden">
    <style>
        .mdl-layout
        {
            overflow-y: unset;
            overflow-x: unset; 
        }
        .mdl-layout__container, .mdl-layout
        {
            height: auto;
        }
        .mdl-layout__header-search 
        {
            white-space: nowrap;
            /*background: #1976D2;*/
            padding: 1px 1px 1px 15px;
            border-radius: 50px;
        }

        .mdl-layout__header-search  input
        {
            /*background: #1976D2;*/
            -webkit-appearance: none;
            border: 0;
            color: white;
            outline: 0;
        }

        .mdl-navigation {
            /* display: -webkit-flex; */
            display: -ms-flexbox;
            /* display: flex; */
            /* -webkit-flex-wrap: nowrap; */
            -ms-flex-wrap: nowrap;
            /* flex-wrap: nowrap; */
            box-sizing: border-box;
            width: 280px;
            display: block;
            
        }

        .mdl-drawer
        {
            position: absolute;
            top: 56px;
            left: 0px;
            /*background-color: #1976D2;*/
            overflow-y: auto;
            overflow-x: hidden;
            width: 280px;
            height: calc(100vh - 56px);
            z-index:10;
            transition: width 0.3s;
        }

        .mdl-navigation__link {
            color: rgb(66,66,66);
            text-decoration: none;
            margin: 0;
            font-size: 14px;
            font-weight: 400;
            line-height: 24px;
            letter-spacing: 0;
            opacity: 0.87;
            padding: 12px 22px;
            display: block;
            /* width: 100%; */
            color: white;
            transition: background-color 0.5s;
            overflow: hidden;
        }


        .mdl-navigation__link span,
        .mdl-navigation__link:visited span
        {
            color: white;
        }

        .mdl-navigation__link:hover
        {
            /*background-color: #2196F3;*/
            transition: background-color 0.5s;
        }

        .mdl-navigation__link i 
        {
            padding-right: 8px;
            font-size: 20px;
            width: 28px;
            text-align: center
        }

        .mdl-bottom-height
        {
            height: calc(100vh - 56px);
        }


        .mdl-bottom-wrapper:not(.mdl-bottom-small) .mdl-navigation__label
        {
            opacity: 1.0;
            transition: opacity 0.5s;
        }

        .mdl-bottom-wrapper:not(.mdl-bottom-small) .mdl-drawer
        {
            width: 280px;
        }

       .mdl-bottom-wrapper:not(.mdl-bottom-small) .badge
        {
            padding: 0px 10px 2px 8px;
            background-color: #ff4081;
            right: 24px;
            position: absolute;
            border-radius: 40px;
        }

        .mdl-bottom-wrapper.mdl-bottom-small .mdl-navigation__label
        {
            opacity: 0.0;
            transition: opacity 0.5s;
        }
        

        .mdl-bottom-wrapper.mdl-bottom-small .badge
        {
            padding: 0px 6px 0px 5px;
            background-color: #ff4081;
            right: 220px;
            top: 5px;
            position: absolute;
            border-radius: 40px;
            font-size: 12px;
            line-height: 16px;
        }

        .mdl-drawer .ss-content
        {
            overflow-x: hidden;
        }

        @media only screen and (min-width: 0px) and (max-width: 599px)
        {
            .mdl-frame-container
            {
                position: absolute;
                top: 56px;
                left: 0px;
            }

            .mdl-frame
            {
                width: calc(100vw - 0px);
                height: calc(100vh - 56px);
                border: 0;
            }

            .mdl-bottom-wrapper.mdl-bottom-small .mdl-drawer
            {
                width: 0px;
                transition: width 0.3s;
            }

        }
        
        @media only screen and (min-width: 600px) 
        {
            .mdl-frame-container
            {
                position: absolute;
                top: 56px;
                left: 72px;
            }

            .mdl-frame
            {
                width: calc(100vw - 72px);
                height: calc(100vh - 56px);
                border: 0;
            }

            .mdl-bottom-wrapper.mdl-bottom-small .mdl-drawer
            {
                width: 72px;
                transition: width 0.3s;
            }
        
        }
        

        .mdl-frame-overlay
        {
            position: fixed;
            z-index: 5;
            width: calc(100vw);
            height: calc(100vh - 56px);
            top: 56px;
            left: 0;
            background-color: rgba(255, 255, 255, 0.8);
        }


        .mdl-search
        {
            width: 300px;
        }

        .mdl-dropdown-apptop-search
        {
            width: 300px;
        }

        @media only screen and (min-width: 0px) and (max-width: 599px)
        {
            .mdl-search
            {
                width: calc(100vw - 200px);
            }

            .mdl-dropdown-apptop-search
            {
                left: 0px;
            }

            .mdl-layout-title
            {
                display: none;
            }
        }

        @media only screen and (min-width: 0px) and (max-width: 449px)
        {
            .mdl-dropdown-apptop-search
            {
                left: calc(((100vw - 300px) / 2) - 100px);
            }

        }
    </style>
    <script type="text/javascript">
        function doSearch()
        {
            popup.hide();
            openPage('apptopsearch.aspx?QUERY=' + encodeURIComponent(document.getElementById('SearchTextBox').value), 'frameBottom');
        }

        function doFilter(e) {
            stopPropagation(e);
            window.setTimeout(function () {
                var searchText = $('#SearchTextBox').val();
                $('#SearchText .search-item').text(searchText);

                $('#SearchAutoComplete .search-item').each(function () {
                    if ($(this).text().toLowerCase().indexOf(searchText.toLowerCase()) >= 0) {
                        $(this).css({ "display": "block" });
                        $(this).addClass('search-vis');
                    }
                    else {
                        $(this).css({ "display": "none" });
                        $(this).removeClass('search-vis');
                    }
                });

                $('#SearchAutoComplete .search-group').each(function () {
                    if ($(this).find('.search-item.search-vis').length == 0)
                        $(this).hide();
                    else
                        $(this).show();
                })
            }, 10);
        }
        

        function doSelectAndSearch(e, changeModule)
        {
            var evt = e ? e : window.event;

            var selectedText = $(evt.target).text();
            if (changeModule)
                selectedText += ":";
            $('#SearchTextBox').val(selectedText);
            doSearch();
        }


        function showAutoComplete(e)
        {
            var el = document.getElementById('SearchAutoComplete');
            if (!el.style || !el.style.display || el.style.display === 'none')
                popup.show("", "#SearchAutoComplete", "", null, null, false);
            //$('#SearchAutoComplete').show(300);
        }

        function toggleMenu() {
            $('.mdl-bottom-wrapper').toggleClass('mdl-bottom-small');

            if ($('.mdl-bottom-wrapper').hasClass('mdl-bottom-small')) {
                $('.mdl-frame-overlay').fadeOut(300);
            }
            else {
                $('.mdl-frame-overlay').fadeIn(300);
            }
        }

        function hideMenu() {
            $('.mdl-bottom-wrapper').addClass('mdl-bottom-small');
            $('.mdl-frame-overlay').fadeOut(300);
        }
    </script>
    <form id="form1" runat="server">
        <!-- Navbar-->
        <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
            <header class="mdl-layout__header">
                <div class="mdl-layout__header-row">

                    <ui:UIButton runat="server" ID="MenuButton" ButtonColor="White" ButtonType="RoundMiniFlat" IconCssClass="material-icons" IconText="menu" DoPostBack="false" OnClickScript="toggleMenu()"  />
                    &nbsp;
                    &nbsp;
                    <span class="mdl-layout-title"><asp:Label runat="server" ID="TitleLabel" Text="Simplicity"></asp:Label></span>

                    <div class="mdl-layout-spacer"></div>

                    <!-- Navbar Right Menu -->
                    <div class="mdl-layout__header-search" runat="server">
                        <div class="mdl-dropdown-container">
                            <div id="SearchAutoComplete" class="mdl-dropdown mdl-dropdown-apptop-search" >
                                <ul id="SearchText" class="search-group  mdl-js-ripple-effect">
                                    <li class="search-header mdl-dropdown-header">Search</li>
                                    <li class="search-item mdl-dropdown-item" onclick="doSelectAndSearch(event, false)"></li>
                                </ul>
                                <ul id="SearchFunctions" class="search-group mdl-js-ripple-effect">
                                    <li class="search-header mdl-dropdown-header">Functions</li>
                                    <% foreach(DataRow dr in dtMenu.Rows) { %>
                                        <li class="search-item mdl-dropdown-item" style="display:block" onclick="doSelectAndSearch(event, true)"><%= dr["FunctionCode"].ToString() %> <%= TranslateMenuItem(dr["FunctionName"].ToString()) %></li>
                                    <% } %>
                                </ul>
                                <ul id="SearchReports" class="search-group mdl-js-ripple-effect">
                                    <li class="search-header mdl-dropdown-header">Reports</li>
                                    <% foreach(DataRow dr in dtReports.Rows) { %>
                                        <li class="search-item mdl-dropdown-item" style="display:block" onclick="doSelectAndSearch(event, true)"><%= dr["ReportCode"].ToString() %> <%= TranslateMenuItem(dr["ReportName"].ToString()) %></li>
                                    <% } %>
                                </ul>
                            </div>
                        </div>
                        <div style="position: relative; display: inline">
                            <asp:TextBox CssClass="mdl-search" ID="SearchTextBox" runat="server" onkeypress="showAutoComplete(event); if(event.keyCode === 13) { doSearch(); }" onkeydown="doFilter(event);" AutoCompleteType="Disabled"></asp:TextBox>
                        </div>
                        <ui:UIButton runat="server" ID="SearchButton" ButtonColor="White" ButtonType="RoundMiniFlat" IconCssClass="material-icons" IconText="search" DoPostBack="false" OnClickScript="doSearch();" />
                    </div>

                    <button class='mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-js-ripple-effect mdl-button--white' onclick="popup.show('', '#userMenu', '', event, null, true); return false;" >
                        <i class='material-icons'>person</i>
                    </button>
                    <div class="mdl-dropdown-container">
                        <div class='mdl-dropdown mdl-dropdown-right' id='userMenu' style='display: none'>
                            <button runat="server" id="EditUserProfileButton" class='mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--default'>
                                <i class='fas fa-user-cog'></i>
                                <span>User Profile</span>
                            </button>
                            <ui:UIButton runat="server" ID="LogoutButton" ButtonColor="Red" ButtonType="NormalFlat" IconCssClass="material-icons" IconText="person" Text="Logout" OnClick="LogoutButton_Click" />
                        </div>
                    </div>
                </div>
            </header>
        </div>
        
        <!-- Sidebar menu-->
        <div class="mdl-bottom-wrapper mdl-bottom-small">
            <div runat="server" class="mdl-drawer mdl-bottom-height" ss-container>
                <div class="mdl-navigation" runat="server">
                    <a class="mdl-navigation__link mdl-custom-ripple mdl-js-ripple-effect" href="javascript:hideMenu(); openPage('home.aspx', 'frameBottom')" ><i class="fas fa-inbox"></i><asp:Label runat="server" ID="InboxLabel" CssClass="mdl-navigation__label" Text="Inbox"></asp:Label><span class="badge"><asp:Label ID="InboxCountLabel" runat="server" Text="0"></asp:Label></span></a>
                    <a class="mdl-navigation__link mdl-custom-ripple mdl-js-ripple-effect" href="javascript:hideMenu(); openPage('homemonthview.aspx', 'frameBottom')" ><i class="fas fa-calendar-alt"></i><asp:Label runat="server" ID="CalendarLabel" CssClass="mdl-navigation__label" Text="Calendar"></asp:Label></a>
                    <a class="mdl-navigation__link mdl-custom-ripple mdl-js-ripple-effect" href="javascript:hideMenu(); openPage('homeannouncements.aspx', 'frameBottom')" ><i class="fas fa-bullhorn"></i><asp:Label runat="server" ID="AnnouncementsLabel" CssClass="mdl-navigation__label" Text="Announcements"></asp:Label><span class="badge"><asp:Label ID="AnnouncementsCountLabel" runat="server" Text="0"></asp:Label></span></a>
                    <a class="mdl-navigation__link mdl-custom-ripple mdl-js-ripple-effect" href="javascript:hideMenu(); openPage('homepinboards.aspx', 'frameBottom')" ><i class="fas fa-th-large"></i><asp:Label runat="server" ID="PinboardLabel" CssClass="mdl-navigation__label" Text="Pinboard"></asp:Label></a>
                    <div class="mdl-navigation__separator"></div>
                    <% foreach (DataRow dr in GetFirstLevelMenus().Rows)
                        { %>
                    <a class="mdl-navigation__link mdl-custom-ripple mdl-js-ripple-effect" href="javascript:hideMenu(); openPage('<%= 
                        HttpUtility.UrlEncode(ResolveUrl("~/functionlisting.aspx?ID=" + HttpUtility.UrlEncode(Security.Encrypt(dr["CategoryName"].ToString() + ":" + AppSession.SaltID)))) %>', 'frameBottom')">
                        <%= Helpers.CreateHtmlIcon(dr["CategoryName"] as String) %><span class="mdl-navigation__label"><%= TranslateMenuItem(dr["CategoryName"] as String) %></span></a>
                    <% } %>
                    <% if (dtReports.Rows.Count > 0)
                        { %>
                    <a class="mdl-navigation__link mdl-custom-ripple mdl-js-ripple-effect" href="javascript:hideMenu(); openPage('<%= 
                        HttpUtility.UrlEncode(ResolveUrl("~/functionlisting.aspx?ID=" + HttpUtility.UrlEncode(Security.Encrypt("##Reports##" + ":" + AppSession.SaltID)))) %>', 'frameBottom')" ><i class="fas fa-table"></i><asp:Label runat="server" ID="ReportLabel" CssClass="mdl-navigation__label" Text="Reports"></asp:Label></a>
                    <% } %>
                </div>
                <ul class="app-menu" style="display:none">

                    <li class="treeview">
                        <a runat="server" class="app-menu__item" id="A1" href="#" data-toggle="treeview">
                            <i class="app-menu__icon fa fa-home"></i>
                            <span class="app-menu__label">Home</span>
                            <i class="treeview-indicator fa fa-angle-right"></i>
                        </a>
                        <ul class="treeview-menu" style="display: block;">
                            <li><a class="app-menu__item treeview-item" href="home.aspx" target="frameBottom">
                                <i class="app-menu__icon icon fa fa-circle-o"></i>
                                <asp:Label runat="server" ID="labelInbox" Text="Inbox" CssClass="app-menu__homelabel"></asp:Label>
                            </li>
                            <li><a class="app-menu__item treeview-item" href="homemonthview.aspx" target="frameBottom">
                                <i class="app-menu__icon icon fa fa-circle-o"></i>
                                <asp:Label runat="server" ID="labelCalendar" Text="Calendar" CssClass="app-menu__homelabel"></asp:Label></a>
                            </li> 
                            <li><a class="app-menu__item treeview-item" href="homeannouncements.aspx" target="frameBottom">
                                <i class="app-menu__icon icon fa fa-circle-o"></i>
                                <asp:Label runat="server" ID="labelAnnouncements" Text="Announcements" CssClass="app-menu__homelabel"></asp:Label>
                            </li>
                            <li><a class="app-menu__item treeview-item" href="homepinboards.aspx" target="frameBottom">
                                <i class="app-menu__icon icon fa fa-circle-o"></i>
                                <asp:Label runat="server" ID="labelPinboard" Text="Pinboard" CssClass="app-menu__homelabel"></asp:Label></a>
                            </li>
                        </ul>
                    </li>

                    <% foreach (DataRow dr in GetFirstLevelMenus().Rows) { %>
                    <li class="treeview">
                        <a class="app-menu__item" id="A2" href="#" data-toggle="treeview">
                            <i class="app-menu__icon fa fa-home"></i>
                            <span class="app-menu__label"><%= TranslateMenuItem(dr["CategoryName"] as String) %></span>
                            <i class="treeview-indicator fa fa-angle-right"></i>
                        </a>
                        <ul class="treeview-menu" style="display: block;">
                            <% foreach (DataRow dr2 in GetSecondLevelMenu(dr["CategoryName"] as String).Rows) { %>
                            <li class="treeview">
                                <% if ((int)dr2["HasSubMenu"] == 1)
                                    { %>
                                <a class="app-menu__item" href="#" data-toggle="treeview">
                                    <i class="app-menu__icon"></i>
                                    <span class="app-menu__label"><%= TranslateMenuItem(dr2["SubCategoryOrFunctionName"] as String) %></span>
                                    <i class="treeview-indicator fa fa-angle-right"></i>
                                </a>
                                <ul class="treeview-menu" style="display: block;">
                                    <% foreach (DataRow dr3 in GetThirdLevelMenu(dr2["CategoryName"] as String, dr2["SubCategoryOrFunctionName"] as String).Rows) { %>
                                        <li><a class="app-menu__item treeview-item" href="javascript:void(0)" onclick="openPage('<%= GetUrl(dr3["ObjectTypeName"] as String, ResolveUrl(dr3["MainUrl"] as String)) %>', 'frameBottom')">
                                            <i class="app-menu__icon"></i>
                                            <span class="app-menu__label"><%= TranslateMenuItem(dr3["FunctionName"] as String) %></span>
                                        </a></li> 
                                    <% } %>
                                </ul>
                                <% } else { %>
                                    <li><a class="app-menu__item treeview-item" href="javascript:void(0)" onclick="openPage('<%= GetUrl(dr2["ObjectTypeName"] as String, ResolveUrl(dr2["MainUrl"] as String)) %>', 'frameBottom')">
                                        <i class="app-menu__icon"></i>
                                        <span class="app-menu__label"><%= TranslateMenuItem(dr2["SubCategoryOrFunctionName"] as String) %></span>
                                    </a></li> 
                                <% } %>
                            </li>
                            <% } %>
                        </ul>
                    </li>
                    <% } %>
                </ul>
            </div>

            <div class="mdl-frame-overlay" style="display:none" onclick="toggleMenu()">

            </div>

            <div class="mdl-frame-container mdl-frame" >
                <iframe name="AnacleEAM_Window" id="AnacleEAM_Window" border="0" runat="server" class="mdl-frame" style="display:none" src="anacleeamframe.html">
                </iframe>
                <iframe name="frameBottom" id="frameBottom" border="0" runat="server" class="mdl-frame">
                </iframe>
            </div>
        </div>
    </form>

    <script type="text/javascript">
        $(document.body).click(function () {
            $('.mdl-dropdown').hide(300);
        });

        $(document).ready(function () {
            var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;

            if (iOS) {
                var resizedHeight = 0;
                var resizeFrames = function () {
                    if (window.innerHeight != resizedHeight) {
                        $('.mdl-frame, .mdl-drawer').css({ height: "calc(" + window.innerHeight + "px - 56px" });
                        resizedHeight = window.innerHeight;
                    }
                    setTimeout(resizeFrames, 50);
                };
                resizeFrames();
            }
        });
    </script>
    
    
</body>

</html>
