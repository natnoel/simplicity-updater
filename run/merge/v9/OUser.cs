//========================================================================
// $Product: Anacle Enterprise Asset Management
// $Version: 7.1
//
// Copyright 2006 (c) Anacle Systems Pte. Ltd.
// All rights reserved.
//========================================================================
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.Sql;
using System.Data.SqlClient;

using Anacle.DataFramework;
using System.Security.Cryptography;
using System.IO;
using System.Reflection;


/// <summary>
/// Summary description for UserAccount
/// </summary>

namespace LogicLayer
{
    [Database("#database"), Map("User")]
    [Serializable]
    public partial class TUser : LogicLayerSchema<OUser>
    {
        public SchemaString LanguageName;
        public SchemaGuid UserBaseID;
        public SchemaGuid SuperiorID;
        public SchemaGuid CraftID;
        
        [Default(0)]
        public SchemaInt IsPasswordChangeRequired;
        public SchemaDateTime PasswordLastChanged;
        [Default(0)]
        public SchemaInt LoginRetries;
        public SchemaInt IsBanned;
        public SchemaDateTime BannedDateFrom;

        [Default(0)]
        public SchemaInt IsActiveDirectoryUser;
        public SchemaString ActiveDirectoryDomain;

        // To prevent concurrentlogin for the same user
        //public SchemaDateTime LastAccessTime;
        public SchemaDateTime LastLoginTime;
        public SchemaDateTime LastLoginTimeForAutoBanned;
        
        public SchemaInt HidePinboardMessageIndicator;
        public SchemaInt Theme;

        public SchemaString SessionID;
        [Size(255)]
        public SchemaString Description;
        public SchemaGuid SiteID;
        public SchemaInt FirstPage;

        public TUserPasswordHistory PasswordHistory { get { return OneToMany<TUserPasswordHistory>("UserID"); } }
        public TPosition Positions { get { return ManyToMany<TPosition>("UserPosition", "UserID", "PositionID"); } }
        public TUserBase UserBase { get { return OneToOne<TUserBase>("UserBaseID"); } }
        public TUser Superior { get { return OneToOne<TUser>("SuperiorID"); } }
        public TDashboard Dashboards { get { return ManyToMany<TDashboard>("UserDashboard", "UserID", "DashboardID"); } }
        public TCraft Craft { get { return OneToOne<TCraft>("CraftID"); } }
        public TUserDelegatedPosition DelegatedToOthersPositions { get { return OneToMany<TUserDelegatedPosition>("DelegatedByUserID"); } }
        public TUserDelegatedPosition DelegatedByOthersPositions { get { return OneToMany<TUserDelegatedPosition>("UserID"); } }
        public TUserPermanentPosition PermanentPositions { get { return OneToMany<TUserPermanentPosition>("UserID"); } }
        public TLocation Site { get { return OneToOne<TLocation>("SiteID"); } }

        [Default(LogicLayer.UserType.Internal)]
        public SchemaInt UserType;

        public SchemaGuid VendorID;
        public TVendor Vendor { get { return OneToOne<TVendor>("VendorID"); } }

        public SchemaGuid UserAssignmentID;
        public TUserAssignment UserAssignment { get { return OneToOne<TUserAssignment>("UserAssignmentID"); } }

        public SchemaString Designation;
        public SchemaString Department;
    }


    /// <summary>
    /// Represents a user account in the system. Details
    /// about the user, including his/her contact details and login
    /// credentials can be found in the UserBase property, which
    /// is an OUserBase object.
    /// </summary>
    public abstract partial class OUser : LogicLayerPersistentObject, IAuditTrailEnabled
    {

        /// <summary>
        /// [Column] Gets or sets the language code that this user has selected 
        /// to be his/her default language when using the system. An example of 
        /// language codes are 'en-US', 'ja-JP', 'zh-CN', etc.
        /// </summary>
        public abstract string LanguageName { get; set; }


        /// <summary>
        /// [Column] Gets the indicator to show/hide the message above the pinboard
        /// null or 0 - Show
        /// 1 - Hide
        /// </summary>
        public abstract int? HidePinboardMessageIndicator{ get; set; }

        /// <summary>
        /// The theme of the UI
        /// </summary>
        public abstract int? Theme { get; set; }

        public enum Themes
        {
            Blue = 0,
            Green = 1,
            Yellow = 2,
            Red = 3,
            Black = 4,
            White = 5
        }

        /// <summary>
        /// [Column] Gets or sets the foreign key to the UserBase table that contains 
        /// a set of properties common for all users.
        /// </summary>
        public abstract Guid? UserBaseID { get; set; }

        /// <summary>
        /// [Column] Gets or sets the foreign key to the User table that indicates 
        /// the superior of this current user.
        /// </summary>
        public abstract Guid? SuperiorID { get; set; }

        /// <summary>
        /// [Column] Gets or sets the foreign key to the Craft table to indicate the 
        /// craft of this user. This must have a value if the current user has 
        /// 'Technician' as one of its roles.
        /// </summary>
        public abstract Guid? CraftID { get; set; }

        /// <summary>
        /// [Column] Gets or sets a flag to indicate that the user's 
        /// password must be changed during the next login.
        /// </summary>
        public abstract int? IsPasswordChangeRequired { get; set; }

        /// <summary>
        /// [Column] Gets or sets the date when password is last updated by user. 
        /// </summary>
        public abstract DateTime? PasswordLastChanged { get; set; }

        /// <summary>
        /// [Column] Gets or sets the number of attempted and unsuccessful logins by user.
        /// </summary>
        public abstract int? LoginRetries { get; set; }

        /// <summary>
        /// [Column] Gets or sets a flag that indicates whether
        /// this user has been banned from the system.
        /// <para></para>
        /// This can be set manually by the user administrator, or
        /// it can be set when this user's failed login attempts
        /// exceeds the configured maximum.
        /// </summary>
        public abstract int? IsBanned { get; set; }

        /// <summary>
        /// Date when user is banned from the system. 
        /// User will not be allowed to log-in the system if
        /// IsBanned == 1 and BannedDateFrom is null or
        /// IsBanned == 1 and BannedDateFrom less than or equal today.
        /// </summary>
        public abstract DateTime? BannedDateFrom { get; set; }

        /// <summary>
        /// [Column] Gets or sets a flag indicating whether the 
        /// user is an active directory user.
        /// </summary>
        public abstract int? IsActiveDirectoryUser { get; set; }

        /// <summary>
        /// [Column] Gets the domain of the active directory.
        /// </summary>
        public abstract String ActiveDirectoryDomain { get; set; }

        /// <summary>
        /// [Colunm] Gets the Last Access time to the webserver
        /// </summary>
        //public abstract DateTime? LastAccessTime { get; set; }
        public abstract string SessionID { get; set; }

        public abstract String Description { get; set; }

        /// <summary>
        /// [Colunm] Gets the Last Login time to the webserver
        /// </summary>
        public abstract DateTime? LastLoginTime { get; set; }

        /// <summary>
        /// [Colunm] Gets the Last Login time for the auto banned
        /// </summary>
        public abstract DateTime? LastLoginTimeForAutoBanned { get; set; }

        public abstract Guid? SiteID { get; set; }

        public abstract int? FirstPage { get; set; }

        /// <summary>
        /// Gets a one-to-many list of OPassword objects that represents the list 
        /// of passwords that this user has created before.
        /// </summary>
        public abstract DataList<OUserPasswordHistory> PasswordHistory { get; }

        /// <summary>
        /// Gets a many-to-many list of OPosition objects that represents a list of 
        /// positions that are assigned to this user
        /// </summary>
        public abstract DataList<OPosition> Positions { get; set; }

        /// <summary>
        /// Gets or sets the OUserBase object that represents an inherited record 
        /// containing a set of properties common for all users.
        /// </summary>
        public abstract OUserBase UserBase { get; set; }

        /// <summary>
        /// Gets or sets the OUser object that represents the superior of this
        /// current user.
        /// </summary>
        public abstract OUser Superior { get; set; }

        /// <summary>
        /// Gets a one-to-many list of ODashboard objects that represents the list 
        /// of dashboards that this user has access to.
        /// </summary>
        public abstract DataList<ODashboard> Dashboards { get; }

        public abstract OCraft Craft { get; }

        public abstract OLocation Site { get; set; }

        /// <summary>
        /// Gets a one-to-many list of OUserDelegatedPosition objects that represents a list of 
        /// positions that are assigned by this user to other users.
        /// </summary>
        public abstract DataList<OUserDelegatedPosition> DelegatedToOthersPositions { get; set; }

        /// <summary>
        /// Gets a one-to-many list of OUserDelegatedPosition objects that represent a list
        /// of possitions assigned to this user by other users.
        /// </summary>
        public abstract DataList<OUserDelegatedPosition> DelegatedByOthersPositions { get; set; }

        /// <summary>
        /// Gets a one-to-many list of OUserPermanentPosition objects that represents a list of 
        /// permanent positions that are assigned to this user by the administrator.
        /// </summary>
        public abstract DataList<OUserPermanentPosition> PermanentPositions { get; set; }

        /// <summary>
        /// User type, Vendor 0 or Internal 1,
        /// K - No longer need this one. Remove this later
        /// </summary>
        public abstract int? UserType { get; set; }

        /// <summary>
        /// VendorID if UserType is Vendor
        /// </summary>
        public abstract Guid? VendorID { get; set; }
        public abstract OVendor Vendor { get; set; }

        public abstract Guid? UserAssignmentID { get; set; }
        public abstract OUserAssignment UserAssignment { get; set; }

        /// <summary>
        /// [Colunm] Gets the Designation of the user
        /// </summary>
        public abstract string Designation { get; set; }

        /// <summary>
        /// [Colunm] Gets the Department of the user
        /// </summary>
        public abstract string Department { get; set; }


        private Guid? currentBusinessEntityID;
        public Guid? CurrentBusinessEntityID
        {
            get
            {
                return currentBusinessEntityID;
            }

            set
            {
                currentBusinessEntityID = value;
            }
        }


        public static OUser NonRegisteredUser
        {
            get
            {
                OApplicationSetting settings = TablesLogic.tApplicationSetting.LoadAll(false, null)[0];
                string dummyUserName = settings.DummyUsername;
                if (string.IsNullOrEmpty(dummyUserName))
                    return null;
                OUser user = TablesLogic.tUser.Load(TablesLogic.tUser.ObjectName == dummyUserName);

                if (user == null)
                {
                    string dummyUserLoginName = settings.DummyLoginName;
                    string dummyUserLoginPassword = settings.DummyUsername;

                    if (string.IsNullOrEmpty(dummyUserLoginName) || string.IsNullOrEmpty(dummyUserLoginPassword))
                        return null;
                    user = TablesLogic.tUser.Create();
                    user.ObjectName = dummyUserName;
                    user.UserBase.LoginName = dummyUserLoginName;
                    user.UserBase.LoginPassword = Security.HashString(dummyUserLoginPassword);
                    user.UserType = LogicLayer.UserType.Vendor;
                    foreach (OPosition p in OPosition.GetPositionsByRoleCode(LogicLayer.RoleCode.VendorNonRegistered))
                    {
                        OUserPermanentPosition up = TablesLogic.tUserPermanentPosition.Create();
                        up.PositionID = p.ObjectID;
                        user.PermanentPositions.Add(up);
                    }
                    using (Connection c = new Connection())
                    {
                        user.Save();
                        c.Commit();
                    }
                }
                return user;
            }
        }

        public bool IsBannedToday
        {
            get
            {
                if (this.IsBanned == 1)
                {
                    if (this.BannedDateFrom == null || this.BannedDateFrom <= DateTime.Today)
                        return true;
                }
                return false;
            }
        }

        public bool IsMoreThan90Days
        {
            get
            {
                // 2017.06.21
                // Kien Trung
                // FIX:
                // to overcome auto ban if more than 90 days
                // if the user account had been banned before
                //
                if (this.IsBanned != 1 && this.LastLoginTimeForAutoBanned != null)
                {
                    int days = DateTime.Today.Subtract(this.LastLoginTimeForAutoBanned.Value).Days;
                    if (days > 90)
                        return true;
                    else
                        return false;
                }
                return false;
            }
        }


        /// <summary>
        /// Disallow deleting if the current logged on user 
        /// is the same as the one he/she is trying to delete.
        /// </summary>
        /// <returns></returns>
        public override bool IsDeactivable()
        {
            if (Workflow.CurrentUser != null && Workflow.CurrentUser.ObjectID == this.ObjectID)
                return false;

            return true;
        }

        public List<ODMSDocument> GetAccessibleDocument()
        {
            return
                TablesLogic.tDMSDocument.LoadList
                (
                (TablesLogic.tDMSDocument.DocumentAccess.UserID == this.ObjectID |
                TablesLogic.tDMSDocument.DocumentAccess.Position.Users.ObjectID == this.ObjectID |
                TablesLogic.tDMSDocument.DocumentAccess.Role.Positions.Users.ObjectID == this.ObjectID) &
                (TablesLogic.tDMSDocument.DocumentAccess.AllowFullControl == 1 |
                TablesLogic.tDMSDocument.DocumentAccess.AllowModify == 1 |
                TablesLogic.tDMSDocument.DocumentAccess.AllowRead == 1 |
                TablesLogic.tDMSDocument.DocumentAccess.AllowSpecialPermission == 1),
                TablesLogic.tDMSDocument.DocumentIndicator.Asc,
                TablesLogic.tDMSDocument.ObjectName.Asc);
        }

        /// <summary>
        /// A cache of the role functions loaded from the database.
        /// </summary>
        private Guid? cachedDocumentId = null;
        private DataTable cachedDocumentAccess = null;

        /// <summary>
        /// Checks if the user is allowed create/edit/delete/view 
        /// access to an object of the specified type.
        /// </summary>
        /// <param name="objectType"></param>
        /// <param name="access"></param>
        /// <returns></returns>
        protected bool AllowAccess(ODMSDocument document, string access)
        {

            ExpressionCondition cond1 = Query.False;
            ExpressionCondition cond2 = Query.False;
            ExpressionCondition cond3 = Query.True;
            if (cachedDocumentId != document.ObjectID || cachedDocumentAccess == null)
            {
                cachedDocumentId = document.ObjectID;
                cond1 = cond1 | TablesLogic.tDMSDocumentAccess.DMSDocumentID == document.ObjectID;

                //foreach (ODMSDocument d in this.GetAccessibleDocument())
                cond2 = cond2 | document.HierarchyPath.Like(TablesLogic.tDMSDocumentAccess.Document.HierarchyPath + "%");

                cond3 = cond3 &
                    (TablesLogic.tDMSDocumentAccess.UserID == this.ObjectID |
                    TablesLogic.tDMSDocumentAccess.Position.Users.ObjectID == this.ObjectID |
                    TablesLogic.tDMSDocumentAccess.Role.Positions.Users.ObjectID == this.ObjectID);

                cachedDocumentAccess = TablesLogic.tDMSDocumentAccess.SelectTop
                    (1, 
                    Case
                    .When
                    (TablesLogic.tDMSDocumentAccess.Document.CreatedUserID == this.ObjectID, 1)
                    .Else(
                    TablesLogic.tDMSDocumentAccess.AllowFullControl)
                    .End.As("AllowFullControl"),
                    TablesLogic.tDMSDocumentAccess.AllowModify,
                    TablesLogic.tDMSDocumentAccess.AllowRead,
                    TablesLogic.tDMSDocumentAccess.AllowSpecialPermission)
                    .Where
                    (cond1 & cond3)
                    .OrderBy
                    (TablesLogic.tDMSDocumentAccess.Document.HierarchyPath.Length().Desc);

                if (cachedDocumentAccess.Rows.Count == 0)
                    cachedDocumentAccess = TablesLogic.tDMSDocumentAccess.SelectTop
                    (1,
                    Case
                    .When
                    (TablesLogic.tDMSDocumentAccess.Document.CreatedUserID == this.ObjectID, 1)
                    .Else(
                    TablesLogic.tDMSDocumentAccess.AllowFullControl)
                    .End.As("AllowFullControl"),
                    TablesLogic.tDMSDocumentAccess.AllowModify,
                    TablesLogic.tDMSDocumentAccess.AllowRead,
                    TablesLogic.tDMSDocumentAccess.AllowSpecialPermission)
                    .Where
                    (cond2 & cond3)
                    .OrderBy
                    (TablesLogic.tDMSDocumentAccess.Document.HierarchyPath.Length().Desc);
            }

            //foreach (DataRow documentAccess in cachedDocumentAccess.Rows)
            if (cachedDocumentAccess.Rows.Count > 0 &&
                (int?)cachedDocumentAccess.Rows[0][access] == 1)
                //if (((int?)documentAccess[access]) == 1)
                return true;
            return false;
        }

        public bool AllowFullControl(ODMSDocument document)
        {
            return AllowAccess(document, "AllowFullControl");
        }

        public bool AllowRead(ODMSDocument document)
        {
            return AllowAccess(document, "AllowRead");
        }

        public bool AllowModify(ODMSDocument document)
        {
            return AllowAccess(document, "AllowModify");
        }

        public bool AllowSpecialPermission(ODMSDocument document)
        {
            return AllowAccess(document, "AllowSpecialPermission");
        }

        public string IsActiveDirectoryUserText
        {
            get
            {
                return this.IsActiveDirectoryUser == 1 ? Resources.Strings.General_Yes : Resources.Strings.General_No;
            }
        }


        public override void Created()
        {
            base.Created();
            UserBase = TablesLogic.tUserBase.Create();          // attach the user object
        }


        /// --------------------------------------------------------------
        /// <summary>
        /// Deactivate the UserBase object as well
        /// </summary>
        /// --------------------------------------------------------------
        public override void Deactivating()
        {
            base.Deactivating();
            UserBase.Deactivate();
        }

        /// --------------------------------------------------------------
        /// <summary>
        /// Get all available roles from the system.
        /// </summary>
        /// <returns></returns>
        /// --------------------------------------------------------------
        public static List<ORole> GetRoles()
        {
            return TablesLogic.tRole[Query.True];
        }


        /// --------------------------------------------------------------
        /// <summary>
        /// Returns a list of all active users in the system.
        /// </summary>
        /// <returns></returns>
        /// --------------------------------------------------------------
        public static List<OUser> GetAllUsers()
        {
            return TablesLogic.tUser[Query.True];
        }

        /// --------------------------------------------------------------
        /// <summary>
        /// Load the user object by the login name.
        /// </summary>
        /// <param name="loginName"></param>
        /// <returns></returns>
        /// --------------------------------------------------------------
        public static OUser GetUserByLoginName(string loginName)
        {
            List<OUser> list = TablesLogic.tUser[TablesLogic.tUser.UserBase.LoginName == loginName & TablesLogic.tUser.IsDeleted == 0];

            if (list.Count > 0)
                return list[0];
            else
                return null;
        }


        /// <summary>
        /// Gets a list of users by roles.
        /// </summary>
        /// <param name="positions"></param>
        /// <returns></returns>
        [Obsolete]
        // TODO: Remove references to this.
        public static List<OUser> GetUsersByRole(string roleCode)
        {
            return
                TablesLogic.tUser.LoadList(
                TablesLogic.tUser.Positions.Role.RoleCode == roleCode);
        }

        /// <summary>
        /// Gets a list of users by a list of roles separated by commma.
        /// </summary>
        /// <param name="positions"></param>
        /// <returns></returns>
        public static List<OUser> GetUsersByRoles(string roleCode)
        {
            if (roleCode != null)
            {
                string[] roleCodes = roleCode.Split(',');
                return
                    TablesLogic.tUser.LoadList(
                        TablesLogic.tUser.Positions.Role.RoleCode.In(roleCodes));
            }
            return null;
        }


        /// <summary>
        /// Get a list of users with the specified role, and 
        /// tied to a location at or above the one specified.
        /// </summary>
        /// <param name="location"></param>
        /// <param name="roleCode"></param>
        /// <returns></returns>
        public static List<OUser> GetUsersByRoleAndAboveLocation(OLocation location, string roleCode)
        {
            if (location != null)
            {
                string[] roleCodes = roleCode.Split(',');
                return TablesLogic.tUser.LoadList(
                    TablesLogic.tUser.Positions.Role.RoleCode.In(roleCodes) &
                    ((ExpressionDataString)location.HierarchyPath).Like(TablesLogic.tUser.Positions.LocationAccess.HierarchyPath + "%"));
            }
            return null;
        }

        /// <summary>
        /// Get a list of users with the specified role, and 
        /// tied to a location at or above the one specified.
        /// If also adds the includedUser in the list, regardless
        /// of whether the user has been deleted or not.
        /// </summary>
        /// <param name="includedUser"></param>
        /// <param name="location"></param>
        /// <param name="roleCode"></param>
        /// <returns></returns>
        public static List<OUser> GetUsersByRoleAndAboveLocation(OUser includedUser, OLocation location, string roleCode)
        {
            if (location != null)
            {
                return TablesLogic.tUser.LoadList(
                    (includedUser != null ? TablesLogic.tUser.ObjectID == includedUser.ObjectID : Query.False) |
                    (TablesLogic.tUser.IsDeleted == 0 &
                    TablesLogic.tUser.Positions.Role.RoleCode == roleCode &
                    ((ExpressionDataString)location.HierarchyPath).Like(TablesLogic.tUser.Positions.LocationAccess.HierarchyPath + "%")),
                    true);
            }
            return null;
        }

        /// <summary>
        /// Get a list of users with the specified role, and 
        /// tied to a location at or below the one specified.
        /// </summary>
        /// <param name="includedUser"></param>
        /// <param name="location"></param>
        /// <param name="roleCode"></param>
        /// <returns></returns>
        public static List<OUser> GetUsersByRoleAndBelowLocation(OUser includedUser, OLocation location, string roleCode)
        {
            if (location != null)
            {
                return TablesLogic.tUser.LoadList(
                    (includedUser != null ? TablesLogic.tUser.ObjectID == includedUser.ObjectID : Query.False) |
                    (TablesLogic.tUser.IsDeleted == 0 &
                    TablesLogic.tUser.Positions.Role.RoleCode == roleCode &
                    TablesLogic.tUser.Positions.LocationAccess.HierarchyPath.Like(location.HierarchyPath + "%")),
                    true);
            }
            return null;
        }

        /// <summary>
        /// Get a list of users with the specified role, and 
        /// tied to a location at or above the one specified.
        /// </summary>
        /// <param name="location"></param>
        /// <param name="roleCode"></param>
        /// <returns></returns>
        public static List<OUser> GetUsersByRoleAndAboveBusinessEntity(OGLSegmentValue businessEntity, string roleCode)
        {
            if (businessEntity != null)
            {
                string[] roleCodes = roleCode.Split(',');
                return TablesLogic.tUser.LoadList(
                    TablesLogic.tUser.Positions.Role.RoleCode.In(roleCodes) &
                    ((ExpressionDataString)businessEntity.HierarchyPath).Like(TablesLogic.tUser.Positions.BusinessEntity.HierarchyPath + "%"));
            }
            return null;
        }

        /// <summary>
        /// Get a list of users with the specified role, and 
        /// tied to a location at or above the one specified.
        /// If also adds the includedUser in the list, regardless
        /// of whether the user has been deleted or not.
        /// </summary>
        /// <param name="includedUser"></param>
        /// <param name="location"></param>
        /// <param name="roleCode"></param>
        /// <returns></returns>
        public static List<OUser> GetUsersByRoleAndAboveBusinessEntity(OUser includedUser, OGLSegmentValue businessEntity, string roleCode)
        {
            if (businessEntity != null)
            {
                return TablesLogic.tUser.LoadList(
                    (includedUser != null ? TablesLogic.tUser.ObjectID == includedUser.ObjectID : Query.False) |
                    (TablesLogic.tUser.IsDeleted == 0 &
                    TablesLogic.tUser.Positions.Role.RoleCode == roleCode &
                    ((ExpressionDataString)businessEntity.HierarchyPath).Like(TablesLogic.tUser.Positions.BusinessEntity.HierarchyPath + "%")),
                    true);
            }
            return null;
        }

        /// <summary>
        /// Get a list of users with the specified role, and 
        /// tied to a location at or below the one specified.
        /// </summary>
        /// <param name="includedUser"></param>
        /// <param name="location"></param>
        /// <param name="roleCode"></param>
        /// <returns></returns>
        public static List<OUser> GetUsersByRoleAndBelowBusinessEntity(OUser includedUser, OGLSegmentValue businessEntity, string roleCode)
        {
            if (businessEntity != null)
            {
                return TablesLogic.tUser.LoadList(
                    (includedUser != null ? TablesLogic.tUser.ObjectID == includedUser.ObjectID : Query.False) |
                    (TablesLogic.tUser.IsDeleted == 0 &
                    TablesLogic.tUser.Positions.Role.RoleCode == roleCode &
                    TablesLogic.tUser.Positions.BusinessEntity.HierarchyPath.Like(businessEntity.HierarchyPath + "%")),
                    true);
            }
            return null;
        }

        /// <summary>
        /// Gets a list of users by their positions.
        /// </summary>
        /// <param name="positions"></param>
        /// <returns></returns>
        public static List<OUser> GetUsersByPositions(List<OPosition> positions)
        {
            return TablesLogic.tUser.LoadList(
                TablesLogic.tUser.Positions.ObjectID.In(positions));
        }


        /// <summary>
        /// Gets a list of users by their positions.
        /// </summary>
        /// <param name="positions"></param>
        /// <returns></returns>
        public static List<OUser> GetUsersByPositionsAndCraft(List<OPosition> positions, OCraft craft)
        {
            return TablesLogic.tUser.LoadList(
                (craft == null ? TablesLogic.tUser.CraftID == null : TablesLogic.tUser.CraftID == craft.ObjectID) &
                TablesLogic.tUser.Positions.ObjectID.In(positions));
        }


        /// <summary>
        /// Convert a single OUser object to a list of OUser object.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static implicit operator List<OUser>(OUser user)
        {
            List<OUser> userList = new List<OUser>();
            userList.Add(user);
            return userList;
        }


        /// --------------------------------------------------------------
        /// <summary>
        /// Determines if the current user has been assigned the 
        /// specified role.
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        /// --------------------------------------------------------------
        public bool HasRole(string roleCode)
        {
            foreach (OPosition position in this.Positions)
                if (position.Role.RoleCode == roleCode)
                    return true;
            return false;
        }



        /// --------------------------------------------------------------
        /// <summary>
        /// Determines if the current user has been assigned the 
        /// specified role.
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        /// --------------------------------------------------------------
        public bool HasRole(params string[] roleCodes)
        {
            foreach (OPosition position in this.Positions)
                if (position.Role.RoleCode.Is(roleCodes))
                    return true;
            return false;
        }


        /// --------------------------------------------------------------
        /// <summary>
        /// Looks in the database for any users with the same duplicate
        /// login name and user name. Returns true if found, false 
        /// otherwise.
        /// </summary>
        /// <returns></returns>
        /// --------------------------------------------------------------
        public bool IsDuplicateUser()
        {
            if (TablesLogic.tUser[
                (TablesLogic.tUser.ObjectName == this.ObjectName |
                TablesLogic.tUser.UserBase.LoginName == this.UserBase.LoginName) &
                TablesLogic.tUser.ObjectID != this.ObjectID].Count > 0)
                return true;

            return false;
        }


        /// <summary>
        /// Gets a list of positions of this current user
        /// such that the roles of those positions
        /// have been granted access (regardless of read/write/view/delete)
        /// to the function of the specified objectType.
        /// </summary>
        /// <param name="objectType">The object type as defined in the OFunction</param>
        /// <returns></returns>
        public List<OPosition> GetPositionsByObjectTypeAndRoleCodes(string objectType, List<string> roleCodes)
        {
            List<OPosition> positions = new List<OPosition>();
            Hashtable hashRolesCodes = new Hashtable();

            foreach (string roleCode in OFunction.GetRoleCodes(objectType))
                hashRolesCodes[roleCode] = 1;
            if (roleCodes != null)
                foreach (string roleCode in roleCodes)
                    hashRolesCodes[roleCode] = 1;

            foreach (OPosition position in this.Positions)
            {
                if (hashRolesCodes[position.Role.RoleCode] != null)
                    positions.Add(position);
            }
            return positions;
        }

        public Hashtable hasAccessToLocationsAsRole(string roleCode, List<Guid> locationGuids)
        {
            ExpressionCondition ec = Query.False;

            foreach (OPosition p in this.Positions)
            {
                if (p.Role.RoleCode.Trim().ToUpper().Equals(roleCode.Trim().ToUpper()))
                {
                    foreach (OLocation l in p.LocationAccess)
                    {
                        ec = ec | TablesLogic.tLocation.HierarchyPath.Like(l.HierarchyPath+'%');
                    }
                }
            }

            List<OLocation> accessList= 
                TablesLogic.tLocation.LoadList
                (
                    TablesLogic.tLocation.ObjectID.In(locationGuids) 
                    & 
                    (
                        ec 
                    )
                );

            Hashtable ht=new Hashtable();
            foreach (OLocation l in accessList)
            {
                ht[l.ObjectID.ToString()] = 1;
            }

            return ht;
        }



        /// <summary>
        /// Gets a list of positions of this current user
        /// such that the roles of those positions
        /// have been granted access (regardless of read/write/view/delete)
        /// to the function of the specified objectType.
        /// </summary>
        /// <param name="objectType">The object type as defined in the OFunction</param>
        /// <returns></returns>
        public List<OPosition> GetPositionsByReportIDAndRoleCodes(Guid? reportId, params object[] roleCodes)
        {
            List<OPosition> positions = new List<OPosition>();
            Hashtable hashRolesCodes = new Hashtable();

            foreach (string roleCode in OReport.GetRoleCodes(reportId))
                hashRolesCodes[roleCode] = 1;

            if (roleCodes != null)
            {
                foreach (string roleCode in roleCodes)
                    hashRolesCodes[roleCode] = 1;
            }

            foreach (OPosition position in this.Positions)
            {
                if (hashRolesCodes[position.Role.RoleCode] != null)
                    positions.Add(position);
            }
            return positions;
        }


        /// <summary>
        /// Gets a list of positions of this current user
        /// such that the roles of those positions
        /// have been granted access (regardless of read/write/view/delete)
        /// to the function of the specified objectType.
        /// </summary>
        /// <param name="objectType">The object type as defined in the OFunction</param>
        /// <returns></returns>
        public List<OPosition> GetPositionsByObjectType(string objectType)
        {
            List<OPosition> positions = new List<OPosition>();
            Hashtable hashRolesCodes = new Hashtable();

            foreach (string roleCode in OFunction.GetRoleCodes(objectType))
                hashRolesCodes[roleCode] = 1;

            foreach (OPosition position in this.Positions)
            {
                if (hashRolesCodes[position.Role.RoleCode] != null)
                    positions.Add(position);
            }
            return positions;
        }


        /// <summary>
        /// Gets a list of positions of this current user
        /// such that the roles of those positions
        /// have been granted access (regardless of read/write/view/delete)
        /// to the function of the specified objectType.
        /// </summary>
        /// <param name="objectType">The object type as defined in the OFunction</param>
        /// <returns></returns>
        public List<OPosition> GetPositionsFromPermanentPositionsByObjectType(string objectType)
        {
            List<OPosition> positions = new List<OPosition>();
            Hashtable hashRolesCodes = new Hashtable();

            foreach (string roleCode in OFunction.GetRoleCodes(objectType))
                hashRolesCodes[roleCode] = 1;

            foreach (OUserPermanentPosition p in this.PermanentPositions)
            {
                if ((p.StartDate == null || p.StartDate <= DateTime.Today) &&
                    (p.EndDate == null || p.EndDate.Value.AddDays(1) > DateTime.Today) &&
                    hashRolesCodes[p.Position.Role.RoleCode] != null)
                    positions.Add(p.Position);
            }
            return positions;
        }


        /// <summary>
        /// Gets a list of positions of this current user
        /// of the object's current state assigned positions
        /// </summary>
        /// <param name="objectType">The object type as defined in the OFunction</param>
        /// <returns></returns>
        public List<OPosition> GetPositionsByObjectCurrentStateAssignedPositions(LogicLayerPersistentObject o1)
        {
            List<OPosition> positions = new List<OPosition>();
            if (o1 is LogicLayerWorkflowPersistentObject)
            {
                LogicLayerWorkflowPersistentObject o = o1 as LogicLayerWorkflowPersistentObject;
                if (o.CurrentActivity == null)
                    return positions;
                DataList<OPosition> assignedPositions = o.CurrentActivity.Positions;

                foreach (OPosition position in this.Positions)
                {
                    if (assignedPositions.Find(position.ObjectID.Value) != null)
                        positions.Add(position);
                }
                return positions;
            }
            else
            {
                return positions;
            }
        }


        /// <summary>
        /// Gets an array list of role codes currently
        /// associated with this user.
        /// </summary>
        /// <returns></returns>
        public ArrayList GetRoleCodes()
        {
            ArrayList roleCodes = new ArrayList();
            foreach (OPosition position in this.Positions)
                if (!roleCodes.Contains(position.Role.RoleCode))
                    roleCodes.Add(position.Role.RoleCode);
            return roleCodes;
        }


        /// <summary>
        /// A cache of the role functions loaded from the database.
        /// </summary>
        private string cachedObjectType = "";
        private List<ORoleFunction> cachedRoleFunctions = null;

        /// <summary>
        /// Checks if the user is allowed create/edit/delete/view 
        /// access to an object of the specified type.
        /// </summary>
        /// <param name="objectType"></param>
        /// <param name="access"></param>
        /// <returns></returns>
        protected bool AllowAccess(string objectType, string access)
        {
            ArrayList alRoleIds = new ArrayList();

            if (cachedObjectType != objectType || cachedRoleFunctions == null)
            {
                cachedObjectType = objectType;
                foreach (OPosition position in this.Positions)
                    alRoleIds.Add(position.Role.RoleCode);
                cachedRoleFunctions = TablesLogic.tRoleFunction.LoadList(
                    TablesLogic.tRoleFunction.Role.RoleCode.In(alRoleIds) &
                    TablesLogic.tRoleFunction.Function.ObjectTypeName == objectType);
            }

            foreach (ORoleFunction roleFunction in cachedRoleFunctions)
                if (roleFunction.Function.ObjectTypeName == objectType && 
                    (access == "" || ((int?)roleFunction.DataRow[access]) == 1))
                    return true;
            return false;
        }


        /// <summary>
        /// Checks if the user is allowed to create an object 
        /// of the specified type.
        /// </summary>
        /// <param name="objectType"></param>
        /// <returns></returns>
        public bool AllowSearch(string objectType)
        {
            return AllowAccess(objectType, "");
        }


        /// <summary>
        /// Checks if the user is allowed to create an object 
        /// of the specified type.
        /// </summary>
        /// <param name="objectType"></param>
        /// <returns></returns>
        public bool AllowCreate(string objectType, String AttachedObjectTypeName = null)
        {
            if (AttachedObjectTypeName == "DepositListingForOperations")
                return true;

            return AllowAccess(objectType, "AllowCreate");
        }


        /// <summary>
        /// Checks if the user is allowed to create an object 
        /// of the specified type.
        /// </summary>
        /// <param name="objectType"></param>
        /// <returns></returns>
        public bool AllowDeleteAll(string objectType)
        {
            return AllowAccess(objectType, "AllowDeleteAll");
        }


        /// <summary>
        /// Checks if the user is allowed to create an object 
        /// of the specified type.
        /// </summary>
        /// <param name="objectType"></param>
        /// <returns></returns>
        public bool AllowEditAll(string objectType)
        {
            return AllowAccess(objectType, "AllowEditAll");
        }


        /// <summary>
        /// Checks if the user is allowed to create an object 
        /// of the specified type.
        /// </summary>
        /// <param name="objectType"></param>
        /// <returns></returns>
        public bool AllowViewAll(string objectType)
        {
            return AllowAccess(objectType, "AllowViewAll");
        }

        /// <summary>
        /// Sli: This function checks Data level access right
        /// if the object is not an IAccessibleByBusinessEntity object, skip this check by return true;
        /// otherwise, check whether user has access right to BusinessEntity of the PersistentObject.
        /// </summary>
        /// <param name="persistentObject"></param>
        /// <returns></returns>
        public bool AllowBusinessEntityLevelAccess(PersistentObject persistentObject)
        {
            if (persistentObject == null)
                return true;            

            if (persistentObject is LogicLayerWorkflowPersistentObject)
            {
                LogicLayerWorkflowPersistentObject ow = persistentObject as LogicLayerWorkflowPersistentObject;
                if (ow.CurrentActivity != null && ow.CurrentActivity.IsAssignedToUser(this))
                    return true;
            }

            if (!(persistentObject is IAccessibleByBusinessEntity))
                return true;

            IAccessibleByBusinessEntity o = persistentObject as IAccessibleByBusinessEntity;
            OGLSegmentValue businessEntity = null;

            //Sli: cannot think of any document has more than 1 TaskBusinessEntity
            if (o.TaskBusinessEntity != null && o.TaskBusinessEntity.Count > 0)
                businessEntity = o.TaskBusinessEntity[0];

            if (businessEntity == null)
            {
                PropertyInfo pi = persistentObject.GetType().GetProperty("BusinessEntityID");
                if (pi != null)
                {
                    Guid? objectBusinessEntityID = (Guid?)pi.GetValue(persistentObject, null);
                    if(objectBusinessEntityID != null)
                        businessEntity = TablesLogic.tGLSegmentValue.Load(
                            TablesLogic.tGLSegmentValue.ObjectID == objectBusinessEntityID.Value &
                            TablesLogic.tGLSegmentValue.SegmentTypeCode == GLSegmentTypeCode.BusinessEntity);
                    
                }
            }

            //if the document has no BE defined, then anyone can access the document.
            if (businessEntity == null)
                return true;

            List<OPosition> positionList = this.GetPositionsByObjectType(persistentObject.GetType().BaseType.Name);

            if (positionList == null || positionList.Count == 0)
                return false;

            foreach(OPosition position in positionList)
            {
                foreach(OGLSegmentValue be in position.BusinessEntity)
                {
                    if(businessEntity.HierarchyPath.IndexOf(be.HierarchyPath) == 0)
                        return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Increments the number of login retries. If the retries
        /// exceeds the maximum number of retries, then the user
        /// account will be banned.
        /// </summary>
        public void IncrementFailedLoginRetries()
        {
            using (Connection c = new Connection())
            {
                if (this.LoginRetries == null)
                    this.LoginRetries = 0;

                this.LoginRetries = this.LoginRetries + 1;
                if (OApplicationSetting.Current.PasswordMaximumTries != null &&
                    this.LoginRetries >= OApplicationSetting.Current.PasswordMaximumTries)
                //Jan 4 2013
                //Reinheart Sadie
                {
                    this .BanUser (1, UserBanReasonCode.ExceedNumberOfLoginTries , DateTime.Today);
                }

                this.Save();
                c.Commit();
            }
        }


        /// <summary>
        /// Sets the number of failed login retries to zero,
        /// and saves the user object.
        /// </summary>
        public void ClearLoginRetries()
        {
            using (Connection c = new Connection())
            {
                this.LoginRetries = 0;
                this.Save();
                c.Commit();
            }
        }


        /// <summary>
        /// Checks if the current user's password has expired.
        /// </summary>
        /// <returns>Returns true if the password has expired,
        /// false otherwise.</returns>
        public bool HasPasswordExpired()
        {
            if (OApplicationSetting.Current.PasswordDaysToExpiry == null ||
                this.PasswordLastChanged == null)
                return false;

            int numberOfDaysToExpiry = OApplicationSetting.Current.PasswordDaysToExpiry.Value;
            if (this.PasswordLastChanged.Value.AddDays(numberOfDaysToExpiry) <= DateTime.Today)
                return true;

            return false;
        }


        /// <summary>
        /// Gets the user's current password age.
        /// </summary>
        /// <returns></returns>
        public int GetPasswordAge()
        {
            if (this.PasswordLastChanged == null)
                return 0;
            return ((TimeSpan)DateTime.Today.Subtract(this.PasswordLastChanged.Value)).Days;
        }


        /// <summary>
        /// Checks if the current user's password is old enough
        /// for the user to change.
        /// </summary>
        /// <returns></returns>
        public bool IsPasswordOldEnoughToChange()
        {
            int passwordAge = GetPasswordAge();

            if (OApplicationSetting.Current.PasswordMinimumAge != null &&
                passwordAge < Convert.ToInt32(OApplicationSetting.Current.PasswordMinimumAge))
                return false;
            return true;
        }


        /// <summary>
        /// Sets the new password of the user. This assumes
        /// </summary>
        /// <param name="newPassword"></param>
        public void SetNewPassword(string newPassword, bool savePasswordHistory)
        {
            using (Connection c = new Connection())
            {
                //this.Reload();
                this.UserBase.LoginPassword = Security.HashPassword(this.UserBase.ObjectID.Value.ToString(), newPassword);
                this.PasswordLastChanged = DateTime.Today;
                this.IsPasswordChangeRequired = this.IsNew ? 1 : 0;
                this.Save();

                // If the password must be saved as a history, then save it.
                // Usually the saving of password histories is required when
                // the user or the administrator updates the password manually.
                //
                // However, when the user or administrator chooses to
                // reset the password by auto-generating a new password,
                // then this password history should NOT be saved.
                //
                if (savePasswordHistory)
                {
                    OUserPasswordHistory.AddPasswordHistory(
                        this.ObjectID.Value, this.UserBase.LoginPassword);
                    OUserPasswordHistory.ClearPasswordHistory(this.ObjectID.Value);
                }

                c.Commit();
            }
        }


        /// <summary>
        /// Resets the user's password by auto-generating the password.
        /// Also e-mails the reset password to the user.
        /// </summary>
        public void ResetPassword()
        {
            using (Connection c = new Connection())
            {
                string strNewPassword = GenerateRandomPassword();

                this.UserBase.LoginPassword = Security.HashString(strNewPassword);
                this.PasswordLastChanged = DateTime.Today;
                this.IsPasswordChangeRequired = 1;

                OUserPasswordHistory.EmailResetPassword(
                    this.ObjectName, this.UserBase.Email, strNewPassword);
                this.Save();
                c.Commit();
            }
        }

        /// <summary>
        /// The different types of password characters.
        /// </summary>
        private static readonly string[] characters = {
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz", 
            "1234567890",
            "!@#$%^&*.?"};


        /// <summary>
        /// Generates a random password.
        /// </summary>
        /// <returns></returns>
        public static string GenerateRandomPassword()
        {
            int intPasswordMinimumLength = Convert.ToInt32(OApplicationSetting.Current.PasswordMinimumLength);

            if (intPasswordMinimumLength < 6)
                intPasswordMinimumLength = 6;

            if (intPasswordMinimumLength > 255)
                intPasswordMinimumLength = 255;

            // Ensure that all characters of every type is created in the
            // password.
            //
            int[] characterType = new int[intPasswordMinimumLength];
            for (int i = 0; i < intPasswordMinimumLength / 3; i++)
            {
                characterType[i] = 1;
                characterType[i + intPasswordMinimumLength / 3] = 2;
            }

            for (int i = 0; i < intPasswordMinimumLength * 2; i++)
            {
                int pos1 = SecureRandom((byte)intPasswordMinimumLength);
                int pos2 = SecureRandom((byte)intPasswordMinimumLength);
                int temp = characterType[pos1];
                characterType[pos1] = characterType[pos2];
                characterType[pos2] = temp;
            }

            // Then create the password based on the order of
            // the character types in the caharacterType array.
            //
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < intPasswordMinimumLength; i++)
            {
                int type = characterType[i];
                char ch = characters[type][SecureRandom((byte)characters[type].Length)];
                sb.Append(ch);
            }

            return sb.ToString();
        }

        private static RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider();

        private static byte SecureRandom(byte maxVal)
        {
            if (maxVal < 0)
                throw new ArgumentOutOfRangeException("Invalid input: " + maxVal);

            byte[] randomNumber = new byte[1];
            do
            {
                rngCsp.GetBytes(randomNumber);
            }
            while (!IsFair(randomNumber[0], maxVal));

            return (byte)((randomNumber[0] % maxVal));
        }

        private static bool IsFair(byte r, byte max)
        {
            int fullSetsOfValues = Byte.MaxValue / max;
            return r < max * fullSetsOfValues;
        }



        // 2010.07.10
        // Kim Foong
        public override void Saved()
        {
            base.Saved();

            List<Guid> userIdList = new List<Guid>();

            // Deactivate the permanent position records removed
            // from this user.
            //
            List<OUserPermanentPosition> ps = TablesLogic.tUserPermanentPosition.LoadList(
                TablesLogic.tUserPermanentPosition.UserID == null |
                TablesLogic.tUserPermanentPosition.PositionID == null);
            foreach (OUserPermanentPosition p in ps)
            {
                p.UserID = null;
                p.PositionID = null;
                p.Deactivate();
            }

            // Deactivate the delegate position records removed
            // from this user.
            //
            List<OUserDelegatedPosition> removedDelegatedPositions = TablesLogic.tUserDelegatedPosition.LoadList(
                TablesLogic.tUserDelegatedPosition.DelegatedByUserID == null |
                TablesLogic.tUserDelegatedPosition.UserID == null |
                TablesLogic.tUserDelegatedPosition.PositionID == null);
            foreach (OUserDelegatedPosition removedDelegatedPosition in removedDelegatedPositions)
            {
                if (removedDelegatedPosition.UserID != null)
                    userIdList.Add(removedDelegatedPosition.UserID.Value);

                removedDelegatedPosition.PositionID = null;
                removedDelegatedPosition.UserID = null;
                removedDelegatedPosition.DelegatedByUserID = null;
                removedDelegatedPosition.Deactivate();
            }

            // Get the list of all users in the delegate list.
            //
            foreach (OUserDelegatedPosition p in this.DelegatedToOthersPositions)
                if (p.UserID != null)
                    userIdList.Add(p.UserID.Value);

            userIdList.Add(this.ObjectID.Value);
            OUser.ActivateAndSaveCurrentPositions(userIdList);
        }

        // 2010.07.10
        // Kim Foong
        /// <summary>
        /// Overrides the Deactivated method to deactivate invalid OUserPermanentPosition
        /// objects.
        /// </summary>
        public override void Deactivated()
        {
            base.Deactivated();
            using (Connection c = new Connection())
            {
                List<Guid> userIdList = new List<Guid>();
                List<OUserPermanentPosition> ps = TablesLogic.tUserPermanentPosition.LoadList(
                    TablesLogic.tUserPermanentPosition.UserID == this.ObjectID);
                foreach (OUserPermanentPosition p in ps)
                {
                    if (p.UserID != null)
                        userIdList.Add(p.UserID.Value);
                    p.UserID = null;
                    p.PositionID = null;
                    p.Deactivate();
                }


                // Deactivate the delegate position records removed
                // from this user.
                //
                List<OUserDelegatedPosition> removedDelegatedPositions = TablesLogic.tUserDelegatedPosition.LoadList(
                    TablesLogic.tUserDelegatedPosition.DelegatedByUserID == this.ObjectID |
                    TablesLogic.tUserDelegatedPosition.UserID == this.ObjectID);
                foreach (OUserDelegatedPosition removedDelegatedPosition in removedDelegatedPositions)
                {
                    if (removedDelegatedPosition.UserID != null)
                        userIdList.Add(removedDelegatedPosition.UserID.Value);

                    removedDelegatedPosition.PositionID = null;
                    removedDelegatedPosition.UserID = null;
                    removedDelegatedPosition.DelegatedByUserID = null;
                    removedDelegatedPosition.Deactivate();
                }


                // Get the list of all users in the delegate list.
                //
                foreach (OUserDelegatedPosition p in this.DelegatedToOthersPositions)
                    if (p.UserID != null)
                        userIdList.Add(p.UserID.Value);

                userIdList.Add(this.ObjectID.Value);

                OUser.ActivateAndSaveCurrentPositions(userIdList);

                c.Commit();
            }
        }

        /// <summary>
        /// This method automatically activates the current position
        /// of the user based on the permanent position list and the
        /// temporary positions list. 
        /// <para>
        /// </para>
        /// It automatically inserts records directly into the UserPosition
        /// table using SQL.
        /// </summary>
        public void ActivateAndSaveCurrentPositions()
        {
            List<Guid> ids = new List<Guid>();
            ids.Add(this.ObjectID.Value);
            OUser.ActivateAndSaveCurrentPositions(ids);

        }


        /// <summary>
        /// This method automatically activates the current position
        /// of the user based on the permanent position list and the
        /// temporary positions list. 
        /// <para>
        /// </para>
        /// It automatically inserts records directly into the UserPosition
        /// table using SQL.
        /// </summary>
        /// <param name="userIds"></param>
        public static void ActivateAndSaveCurrentPositions(List<Guid> userIds)
        {
            using (Connection c = new Connection())
            {
                string getDateFunction = "getdate()";
                if (Connection.DatabaseProvider == DatabaseProvider.Oracle)
                    getDateFunction = "systimestamp";

                StringBuilder sb = new StringBuilder();
                sb.Append("null");
                foreach (Guid id in userIds)
                {
                    sb.Append("," + Renderer.RenderPrimitive(id, 255));
                }
                string userIdList = "(" + sb.ToString() + ")";

                // These updates will not be written into
                // the audit trail table.
                //
                Connection.ExecuteNonQuery("#database",
                    "DELETE FROM UserPosition WHERE " + Anacle.DataFramework.Renderer.GetActualColumnName("UserPosition", "UserID") + " IN " + userIdList);

                // This method simply re-constructs the UserPosition
                // table for the given user IDs.
                //
                Connection.ExecuteNonQuery("#database",
                    "INSERT INTO UserPosition (" +
                    Renderer.GetActualColumnName("UserPosition", "UserID") + "," +
                    Renderer.GetActualColumnName("UserPosition", "PositionID") + ") " +
                    "SELECT UserID, PositionID FROM ( " +
                    "    SELECT " +
                        Renderer.GetActualColumnName("UserPermanentPosition", "UserID") + " AS UserID, " +
                        Renderer.GetActualColumnName("UserPermanentPosition", "PositionID") + " AS PositionID " +
                        " FROM UserPermanentPosition WHERE " +
                        Renderer.GetActualColumnName("UserPermanentPosition", "UserID") + " IS NOT NULL AND " +
                        Renderer.GetActualColumnName("UserPermanentPosition", "PositionID") + " IS NOT NULL AND (" +
                        Renderer.GetActualColumnName("UserPermanentPosition", "StartDate") + " IS NULL OR " +
                        Renderer.GetActualColumnName("UserPermanentPosition", "StartDate") + " <= " + getDateFunction + ") AND (" +
                        Renderer.GetActualColumnName("UserPermanentPosition", "EndDate") + " IS NULL OR " +
                        " dateadd(dd, 1, " + Renderer.GetActualColumnName("UserPermanentPosition", "EndDate") + ") > " + getDateFunction + ") AND " +
                        Renderer.GetActualColumnName("UserPermanentPosition", "UserID") + " IN " + userIdList + " " +

                    "    UNION " +

                    "    SELECT " +
                        Renderer.GetActualColumnName("UserDelegatedPosition", "UserID") + ", " +
                        Renderer.GetActualColumnName("UserDelegatedPosition", "PositionID") + " AS PositionID " +
                        " FROM UserDelegatedPosition WHERE " +
                        Renderer.GetActualColumnName("UserDelegatedPosition", "UserID") + " IS NOT NULL AND " +
                        Renderer.GetActualColumnName("UserDelegatedPosition", "PositionID") + " IS NOT NULL AND (" +
                        Renderer.GetActualColumnName("UserDelegatedPosition", "StartDate") + " IS NULL OR " +
                        Renderer.GetActualColumnName("UserDelegatedPosition", "StartDate") + " <= " + getDateFunction + ") AND (" +
                        Renderer.GetActualColumnName("UserDelegatedPosition", "EndDate") + " IS NULL OR " +
                        " dateadd(dd, 1, " + Renderer.GetActualColumnName("UserDelegatedPosition", "EndDate") + ") > " + getDateFunction + ") AND " +
                        Renderer.GetActualColumnName("UserDelegatedPosition", "UserID") + " IN " + userIdList + " AND " +
                        Renderer.GetActualColumnName("UserDelegatedPosition", "DelegatedByUserID") + " IS NOT NULL " +
                    ") t ");

                c.Commit();
            }
        }

        /// <summary>
        /// Get the Server time
        /// </summary>
        /// <returns></returns>
        public static DateTime GetServerTime()
        {
            string queryString = "select getdate()";
            if (Connection.DatabaseProvider == DatabaseProvider.Oracle)
                queryString = "select systimestamp from dual";

            DateTime serverTime;
            DataSet dt = Connection.ExecuteQuery("#database", queryString);
            serverTime = (DateTime)dt.Tables[0].Rows[0][0];
            return serverTime;
        }

        /// <summary>
        /// AddedBy: Yiyuan
        /// AddedDate: 13-Jan-2012
        /// Check whether the current user is logged in the system
        /// </summary>
        /// <returns></returns>
        //public bool IsLoggedIn(int minuteBuffer)
        //{
        //    // LastAccessTime is cleared when logging off
        //    if (this.LastAccessTime == null)
        //        return false;

        //    // User is not properly logged off by clicking logoff button
        //    // LastAccessTime is updated every 1 minute.
        //    // When Comparing with the current server time, add minuteBuffer minutes
        //    // buffer to the LastAccessTime.
        //    DateTime currentServerTime = OUser.GetServerTime();
        //    DateTime lastAccessTime = LastAccessTime.Value;
        //    if (currentServerTime > lastAccessTime.AddMinutes(minuteBuffer))
        //        return false;

        //    return true;
        //}

        /// <summary>
        /// Get a list of users who are not tenant, nor the user himself/herself,
        /// nor the users who delegate to this user for delegating to
        /// </summary>
        /// <returns></returns>
        public List<OUser> GetAllValidUserForDelegating(Guid? userId)
        {
            List<Guid?> delegatedByUserIDs = new List<Guid?>();
            foreach (OUserDelegatedPosition o in this.DelegatedByOthersPositions)
            {
                delegatedByUserIDs.Add(o.DelegatedByUserID);
            }
            return TablesLogic.tUser[
                //TablesLogic.tUser.isTenant == 0 &
                TablesLogic.tUser.ObjectID != userId &
                !TablesLogic.tUser.ObjectID.In(delegatedByUserIDs)];
        }
        //Jan 03 2013
        //Reinheart Sadie
        public void BanUser(int isBanned, string reasonCode, DateTime? banDateFrom)
        {
                if (this.IsBanned != isBanned)
                {
                    this.IsBanned = isBanned;
                    this.BannedDateFrom = banDateFrom;
                    this.Save();

                    // Create the ban log
                    OUserBanLog userBanLog = TablesLogic.tUserBanLog.Create();
                    userBanLog.UserID = this.ObjectID;
                    userBanLog.ReasonCode = reasonCode;
                    userBanLog.BannedDateFrom = banDateFrom;
                    userBanLog.DateOfBan = DateTime.Now;
                    userBanLog.IsBannedIndicator = isBanned;
                    userBanLog.Save();
            }
        }


        public List<OLocation> GetAllAccessibleLocation(string locationTypeName, string objectType)
        {
            // 2010.05.28
            // Kim Foong
            // Loading from a huge location table by first finding out the LocationTypeID
            // is much faster.
            //
            Guid? locationTypeId = TablesLogic.tLocationType.Select(TablesLogic.tLocationType.ObjectID).Where(TablesLogic.tLocationType.ObjectName == locationTypeName);

            ExpressionCondition locCondition = Query.False;
            ExpressionCondition locationTypeCondition = (locationTypeName == null || locationTypeName.Trim().Length == 0) ?
                Query.True : TablesLogic.tLocation.LocationTypeID == locationTypeId;

            foreach (OPosition position in this.GetPositionsByObjectType(objectType))
            {
                foreach (OLocation location in position.LocationAccess)
                    locCondition = locCondition | TablesLogic.tLocation.HierarchyPath.Like(location.HierarchyPath + "%");
            }

            return TablesLogic.tLocation.LoadList(locCondition & locationTypeCondition);
        }


        /*public List<OBudgetGroup> GetAllAccessibleBudgetGroup(string objectType, Guid? includingBudgetGroupId)
        {
            return TablesLogic.tBudgetGroup.LoadList(
                TablesLogic.tBudgetGroup.IsDeleted == 0 &
                (TablesLogic.tBudgetGroup.Positions.ObjectID.In(this.GetPositionsByObjectType(objectType)) |
                TablesLogic.tBudgetGroup.ObjectID == includingBudgetGroupId),
                true,
                TablesLogic.tBudgetGroup.ObjectName.Asc
                );
        }*/

        /// <summary>
        /// Gets a list of users given their e-mail addresses.
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static List<OUser> GetUserByEmail(string email)
        {
            return TablesLogic.tUser.Load(TablesLogic.tUser.UserBase.Email == email
                & TablesLogic.tUser.IsDeleted == 0);
        }



        /// <summary>
        /// Gets a list of unbanned users given their e-mail addresses.
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static List<OUser> GetUnbannedUsersByEmail(string email)
        {
            return TablesLogic.tUser.Load(
                TablesLogic.tUser.UserBase.Email == email &
                (
                    TablesLogic.tUser.IsBanned == 0 |
                    TablesLogic.tUser.BannedDateFrom > DateTime.Today) &
                TablesLogic.tUser.IsDeleted == 0);

        }

        /// <summary>
        /// Returns true if the user is currently assigned
        /// to the activity.
        /// </summary>
        /// <param name="activity"></param>
        /// <returns></returns>
        public bool IsApprovalUser(OActivity activity)
        {

            foreach (OUser approvalUser in activity.Users)
            {
                if (approvalUser.ObjectID == this.ObjectID)
                    return true;
            }
            foreach (OPosition position in activity.Positions)
            {
                foreach (OPosition userPosition in this.Positions)
                {
                    if (userPosition.ObjectID == position.ObjectID)
                        return true;
                }
            }
            return false;
        }

        public bool HasAccess(OLocation loc)
        {
            bool hasaccess = false;

            if (this.SiteID != null && loc != null)
            {
                if (HasSite(loc) != null && HasSite(Site) != null)
                    return true;
            }
            return hasaccess;
        }

        public OLocation HasSite(OLocation loc)
        {
            if (loc.LocationType.ObjectName == "")
                return loc;
            else if (loc.ParentID != null)
                return HasSite(loc.Parent);
            else
                return null;
        }


        /// <summary>
        /// Gets all UNBANNED users.
        /// </summary>
        /// <returns></returns>
        public static List<OUser> GetAllUnbannedUsers()
        {
            return TablesLogic.tUser.LoadList(
                TablesLogic.tUser.IsBanned == 0 |
                TablesLogic.tUser.BannedDateFrom > DateTime.Today);
        }

        public static List<OUser> GetAllUnbannedStaffs()
        {
            return TablesLogic.tUser.LoadList(
                (TablesLogic.tUser.IsBanned == 0 |
                TablesLogic.tUser.BannedDateFrom > DateTime.Today)
                & (TablesLogic.tUser.UserType == null | !TablesLogic.tUser.UserType.In((int)LogicLayer.UserType.Tenant, LogicLayer.UserType.Vendor))
                );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="includedUser"></param>
        /// <param name="location"></param>
        /// <param name="roleCode"></param>
        /// <returns></returns>
        public static List<OUser> GetPermanentPositionUsersByRoleAndAboveLocation(OUser includedUser, OLocation location, string roleCode)
        {
            if (location != null)
            {
                return TablesLogic.tUser.LoadList(
                    (includedUser != null ? TablesLogic.tUser.ObjectID == includedUser.ObjectID : Query.False) |
                    (TablesLogic.tUser.IsDeleted == 0 &
                    TablesLogic.tUser.PermanentPositions.Position.Role.RoleCode == roleCode &
                    ((ExpressionDataString)location.HierarchyPath).Like(TablesLogic.tUser.PermanentPositions.Position.LocationAccess.HierarchyPath + "%")),
                    true);
            }
            return null;
        }

        public static bool ValidateAccessToPositions(List<OPosition> positionList, string locationPath, string equipmentPath)
        {
            //Access rights
            bool locationAccess = false;
            bool equipmentAccess = false;

            foreach (OPosition position in positionList)
            {
                locationAccess = false;
                equipmentAccess = false;

                if (locationPath != null)
                {
                    foreach (OLocation location in position.LocationAccess)
                    {
                        if (locationPath.StartsWith(location.Path))
                        {
                            locationAccess = true;
                            break;
                        }
                    }
                }
                else
                {
                    locationAccess = true;
                }

                if (equipmentPath != null)
                {
                    foreach (OEquipment equipment in position.EquipmentAccess)
                    {
                        if (equipmentPath.StartsWith(equipment.Path))
                        {
                            equipmentAccess = true;
                            break;
                        }
                    }
                }
                else
                {
                    equipmentAccess = true;
                }

                if (equipmentAccess && locationAccess)
                {
                    break;
                }
            }

            return locationAccess && equipmentAccess;
        }


        /// <summary>
        /// Handles the template downloading.
        /// </summary>
        /// <param name="successfulIndicator"></param>
        /// <param name="forUser">true for user template, false for position template</param>
        /// <returns>excel template file path</returns>
        public static String ExportExcel(out int successfulIndicator, bool forUser)
        {
            String msg = "";

            DataTable dt;
            if (forUser)
                dt = GenerateExportTableForUser();
            else
                dt = GenerateExportTableForUserPositionAssignment();

            try
            {
                successfulIndicator = 0;

                DataSet ds = new DataSet();
                ds.Tables.Add(dt);

                String tempDirectory = (String)new System.Configuration.AppSettingsReader().GetValue("ReportTempFolder", typeof(String));
                String tempSavePath;
                if (forUser)
                    tempSavePath = tempDirectory + "User_" + DateTime.Now.ToString("yyyy-MM-dd_HHmmss") + ".xls";
                else
                    tempSavePath = tempDirectory + "User_Positions_" + DateTime.Now.ToString("yyyy-MM-dd_HHmmss") + ".xls";

                int i = 0;
                String tempPath = tempSavePath;
                while (File.Exists(tempPath))
                {
                    tempPath = tempDirectory + "/" + System.IO.Path.GetFileNameWithoutExtension(tempSavePath) + "(" + i + ")" + System.IO.Path.GetExtension(tempSavePath);
                    i++;
                }
                tempPath = tempSavePath;
                ExcelWriter.GenerateExcelFile(ds, tempDirectory, System.IO.Path.GetFileNameWithoutExtension(tempPath), 1);

                msg = tempSavePath;
            }
            catch (Exception ex)
            {
                successfulIndicator = 0;
                return Resources.Errors.Journal_ErrExport;
            }
            successfulIndicator = 1;
            return msg;
        }

        public static DataTable GenerateExportTableForUser()
        {
            DataTable dt = new DataTable();
            dt.TableName = "User - Create and Update";
            dt.Columns.Add("Login Name*", typeof(String));
            dt.Columns.Add("User Name*", typeof(String));
            dt.Columns.Add("Description", typeof(String));
            dt.Columns.Add("Designation", typeof(String));
            dt.Columns.Add("Department", typeof(String));
            dt.Columns.Add("Cell Phone", typeof(String));
            dt.Columns.Add("Email", typeof(String));
            dt.Columns.Add("Fax", typeof(String));
            dt.Columns.Add("Phone", typeof(String));
            dt.Columns.Add("Country", typeof(String));
            dt.Columns.Add("State", typeof(String));
            dt.Columns.Add("City", typeof(String));
            dt.Columns.Add("Address", typeof(String));
            dt.Columns.Add("Craft", typeof(String));
            dt.Columns.Add("Language*", typeof(String));
            dt.Columns.Add("Reset Password via Email? (Yes/No)*", typeof(int));
            dt.Columns.Add("Password", typeof(String));
            return dt;
        }

        public static DataTable GenerateExportTableForUserPositionAssignment()
        {
            DataTable dt = new DataTable();
            dt.TableName = "User Position Assigment";
            dt.Columns.Add("Login Name*", typeof(String));
            dt.Columns.Add("Position Name*", typeof(String));
            dt.Columns.Add("Start Date", typeof(String));
            dt.Columns.Add("End Date", typeof(String));
            return dt;
        }

        /// <summary>
        /// Handles the uploading of excel files.
        /// </summary>
        /// <param name="excelFilePaths"></param>
        /// <param name="forUser">True for user excel upload, false for user position assignment excel upload.</param>
        /// <param name="positions">Additional data for user position assignment excel upload, null otherwise.</param>
        /// <returns></returns>
        public static string ImportExcelFile(List<String> excelFilePaths, bool forUser, List<OPosition> positions)
        {
            String errMsg = "";
            DataTable dt; // change to DataSet if the file has more than one sheet

            foreach (String excelFilePath in excelFilePaths)
            {
                try
                {
                    dt = ExcelReader.LoadExcelFile(excelFilePath);
                }
                catch
                {
                    return Resources.Errors.Journal_ErrReadingFile;
                }
                if (dt == null) return Resources.Errors.Journal_ErrReadingFile;

                if (forUser)
                    errMsg = ProcessExcelFileForUser(dt);
                else
                    errMsg = ProcessExcelFileForUserPositionAssignment(dt, positions);

                File.Delete(excelFilePath);
            }
            return errMsg;
        }

        /// <summary>
        /// Handles the processing of excel files for User Positions
        /// </summary>
        /// <param name="datatable"></param>
        /// <returns></returns>
        public static string ProcessExcelFileForUserPositionAssignment(DataTable datatable, List<OPosition> positions)
        {
            int rowNumber = 0;
            List<OUser> userList = new List<OUser>();

            foreach (DataRow dr in datatable.Rows)
            {
                ++rowNumber;
                String ColLoginName = dr[0].ToString();
                String ColPositionName = dr[1].ToString();
                String ColStartDate = dr[2].ToString();
                String ColEndDate = dr[3].ToString();

                if (ColLoginName.IsNullOrTrimEmpty() && ColPositionName.IsNullOrTrimEmpty())
                    continue;

                if (ColLoginName.IsNullOrTrimEmpty() || ColPositionName.IsNullOrTrimEmpty())
                    return String.Format(Resources.Errors.SpaceMassUpload_RequiredFieldNotFilled, rowNumber);

                // get the user
                OUser user;
                if (userList.Find(x => x.UserBase.LoginName == ColLoginName) == null)
                    user = TablesLogic.tUser.Load(TablesLogic.tUser.UserBase.LoginName == ColLoginName);
                else
                    user = userList.Find(x => x.UserBase.LoginName == ColLoginName);

                if (user == null)
                    return String.Format(Resources.Errors.User_LoginNameNotExist, rowNumber);

                // get the position
                OPosition position = TablesLogic.tPosition.Load(TablesLogic.tPosition.ObjectName == ColPositionName);
                if (position == null)
                    return String.Format(Resources.Errors.User_PositionNameNotExist, rowNumber);

                // get accessible positions for this user
                List<OLocation> locations = new List<OLocation>();
                foreach (OPosition p in positions)
                {
                    foreach (OLocation location in p.LocationAccess)
                        locations.Add(location);
                }
                List<OPosition> AvailablePositions = OPosition.GetPositionsAtOrBelowLocations(locations);
                if (AvailablePositions.Find(x => x.ObjectID == position.ObjectID) == null)
                    return String.Format(Resources.Errors.User_NoAccessToPosition, rowNumber);

                // updating data
                try
                {
                    OUserPermanentPosition permanentPosition = user.PermanentPositions.Find(x => x.PositionID == position.ObjectID);
                    if (permanentPosition == null)
                    {
                        permanentPosition = TablesLogic.tUserPermanentPosition.Create();
                        permanentPosition.UserID = user.ObjectID;
                        permanentPosition.PositionID = position.ObjectID;
                        user.PermanentPositions.Add(permanentPosition);
                    }
                    if(!ColStartDate.IsNullOrTrimEmpty())
                        permanentPosition.StartDate = DateTime.ParseExact(ColStartDate, ExcelReaderDateTimeFormat.DefaultFormat, null);
                    if(!ColEndDate.IsNullOrTrimEmpty())
                        permanentPosition.EndDate = DateTime.ParseExact(ColEndDate, ExcelReaderDateTimeFormat.DefaultFormat, null);
                    if (!ColStartDate.IsNullOrTrimEmpty() && !ColEndDate.IsNullOrTrimEmpty())
                    {
                        if (DateTime.Compare(permanentPosition.StartDate.Value, permanentPosition.EndDate.Value) > 0)
                            return String.Format(Resources.Errors.User_InvalidDates, rowNumber);
                    }
                    
                }
                catch(FormatException) {
                    return string.Format(Resources.Errors.SpaceMassUpload_IncorrectedFormat, rowNumber);
                }
                userList.Add(user);
            }

            using (Connection c = new Connection())
            {
                for (int counter = 0; counter < userList.Count; ++counter)
                {
                    OUser user = userList[counter];
                    user.ActivateAndSaveCurrentPositions();
                    user.Save();
                }
                c.Commit();
            }
            return "";
        }


        /// <summary>
        /// Handles the processing of excel file for Users.
        /// </summary>
        /// <param name="datatable"></param>
        /// <returns></returns>
        public static string ProcessExcelFileForUser(DataTable datatable)
        {
            int rowNumber = 0;
            List<OUser> userList = new List<OUser>();
            OApplicationSetting applicationSetting = OApplicationSetting.Current;
            List<Boolean> ResetViaEmail = new List<Boolean>();
            List<String> Passwords = new List<String>();
            List<Boolean> ResetPassword = new List<Boolean>();

            foreach (DataRow dr in datatable.Rows)
            {
                ++rowNumber;

                String ColLoginName = dr[0].ToString();
                String ColUserName = dr[1].ToString();
                String ColDescription = dr[2].ToString();
                String ColDesignation = dr[3].ToString();
                String ColDepartment = dr[4].ToString();
                String ColCellPhone = dr[5].ToString();
                String ColEmail = dr[6].ToString();
                String ColFax = dr[7].ToString();
                String ColPhone = dr[8].ToString();
                String ColCountry = dr[9].ToString();
                String ColState = dr[10].ToString();
                String ColCity = dr[11].ToString();
                String ColAddress = dr[12].ToString();
                String ColCraft = dr[13].ToString();
                String ColLanguage = dr[14].ToString();
                String ColResetPassword = dr[15].ToString();
                String ColPassword = dr[16].ToString();

                if(ColLoginName.IsNullOrTrimEmpty() && ColUserName.IsNullOrTrimEmpty() && ColDescription.IsNullOrTrimEmpty()
                    && ColDesignation.IsNullOrTrimEmpty() && ColDepartment.IsNullOrTrimEmpty() && ColCellPhone.IsNullOrTrimEmpty()
                    && ColEmail.IsNullOrTrimEmpty() && ColFax.IsNullOrTrimEmpty() && ColPhone.IsNullOrTrimEmpty() && ColCountry.IsNullOrTrimEmpty()
                    && ColState.IsNullOrTrimEmpty() && ColCity.IsNullOrTrimEmpty() && ColAddress.IsNullOrTrimEmpty() && ColCraft.IsNullOrTrimEmpty()
                    && ColLanguage.IsNullOrTrimEmpty() && ColResetPassword.IsNullOrTrimEmpty() && ColPassword.IsNullOrTrimEmpty())
                    continue;

                OUser user = TablesLogic.tUser.Load(TablesLogic.tUser.UserBase.LoginName == ColLoginName);
                

                OLanguage lang = TablesLogic.tLanguage.Load(TablesLogic.tLanguage.ObjectName == ColLanguage.Trim());
                if (lang == null)
                    lang = TablesLogic.tLanguage.Load(TablesLogic.tLanguage.CultureCode == ColLanguage.Trim());

                if (lang == null)
                    return String.Format(Resources.Errors.User_LanguageNotExists, rowNumber);
                
                if(String.Compare(ColResetPassword.Trim(), "Yes", true) == 0 && ColEmail.IsNullOrTrimEmpty())
                    return String.Format(Resources.Errors.User_EmailRequired, rowNumber);

                if (String.Compare(ColResetPassword.Trim(), "No", true) == 0 && ColPassword.IsNullOrTrimEmpty() && user == null)
                    return String.Format(Resources.Errors.User_PasswordRequired, rowNumber);

                OCraft craft = TablesLogic.tCraft.Load(TablesLogic.tCraft.ObjectName == ColCraft.Trim());

                if (craft == null && !ColCraft.IsNullOrTrimEmpty())
                    return String.Format(Resources.Errors.User_CraftNotExists, rowNumber);

                if (!ColLoginName.IsNullOrTrimEmpty() && !ColUserName.IsNullOrTrimEmpty() && !ColLanguage.IsNullOrTrimEmpty())
                {
                    try
                    {
                        if (user == null)
                            user = TablesLogic.tUser.Create();

                        user.UserBase.LoginName = ColLoginName;
                        user.ObjectName = ColUserName;
                        user.Description = ColDescription;
                        user.Designation = ColDesignation;
                        user.Department = ColDepartment;
                        user.UserBase.Cellphone = ColCellPhone;
                        user.UserBase.Email = ColEmail;
                        user.UserBase.Fax = ColFax;
                        user.UserBase.Phone = ColPhone;
                        user.UserBase.AddressCountry = ColCountry;
                        user.UserBase.AddressState = ColState;
                        user.UserBase.AddressCity = ColCity;
                        user.UserBase.Address = ColAddress;
                        if(craft != null)
                            user.CraftID = craft.ObjectID;
                        user.LanguageName = lang.CultureCode;
                    }
                    catch (FormatException)
                    {
                        return string.Format(Resources.Errors.SpaceMassUpload_IncorrectedFormat, rowNumber);
                    }

                    if (user.IsDuplicateUser())
                        return Resources.Errors.User_DuplicateUser;

                    // Ensures that the password adheres to the minimum
                    // length requirement.

                    if (applicationSetting.PasswordMinimumLength != null && ColPassword.Length < applicationSetting.PasswordMinimumLength.Value)
                        String.Format(Resources.Errors.User_PasswordMinimumLength, applicationSetting.PasswordMinimumLength.Value);

                    // Ensures that the password has the required
                    // valid characters.

                    if (!OUserPasswordHistory.ValidatePasswordCharacters(ColPassword))
                    {
                        if (applicationSetting.PasswordRequiredCharacters == 1)
                            return Resources.Errors.User_PasswordMustContainAlphaNumericCharacters;
                        else if (applicationSetting.PasswordRequiredCharacters == 2)
                            return Resources.Errors.User_PasswordMustContainAlphaNumericSpecialCharacters;
                    }


                    // Ensures that the password does not exist
                    // in the history of passwords.
                    string strHashedNewPassword = Security.HashPassword(user.ObjectID.Value.ToString(), ColPassword);
                    if (OUserPasswordHistory.DoesPasswordExist(user.ObjectID.Value, strHashedNewPassword))
                        return String.Format(Resources.Errors.User_PasswordHistoryExists, applicationSetting.PasswordHistoryKept);

                    // Get email from AD

                    if (applicationSetting.GetUserEmailFromActiveDirectory == 1)
                    {
                        string adDomainName = applicationSetting.ADDomainName;
                        ActiveDirectory ad = new ActiveDirectory(adDomainName);
                        // If need to impersonate to AD, set logon account name and password
                        //
                        if (applicationSetting.UseImpersonatorToAD == 1)
                        {
                            ad.LogonAccName = applicationSetting.ADLogOnDomainAccount;
                            ad.LogonPassword = applicationSetting.ADLogOnPassword;
                        }

                        // Search ad by group name settings and nric
                        //
                        string nric = user.UserBase.LoginName.Trim();
                        string group = applicationSetting.ActiveDirectoryUserGroupNames;
                        ActiveDirectoryUser adUser = new ActiveDirectoryUser();
                        if (group != null && group != string.Empty)
                            adUser = ad.GetADUser(group, nric);
                        else
                            adUser = ad.GetADUser(nric);

                        if (adUser.LoginName != null && adUser.LoginName != string.Empty)
                        {
                            user.UserBase.Email = adUser.Email;
                            user.ObjectName = adUser.UserName;
                        }
                        else
                        {
                            return "Active Directory user " + user.ObjectName + " with NRIC " + user.UserBase.LoginName
                                                   + " cannot be found in active directory group.";
                        }
                    }

                    // save all passwords first because we will cancel the whole batch if there is something wrong with the file
                    ResetViaEmail.Add(String.Compare(ColResetPassword.Trim(), "Yes", true) == 0);
                    ResetPassword.Add(!ColPassword.IsNullOrTrimEmpty());
                    Passwords.Add(ColPassword);

                    userList.Add(user);
                }
                else
                {
                    return string.Format(Resources.Errors.SpaceMassUpload_RequiredFieldNotFilled, rowNumber);
                }
            }

            using (Connection c = new Connection())
            {
                int counter = 0;
                foreach (OUser user in userList)
                {
                    if (ResetViaEmail[counter])
                        user.ResetPassword();
                    else if(ResetPassword[counter])
                        user.SetNewPassword(Passwords[counter], true);
                    ++counter;

                    //user.ActivateAndSaveCurrentPositions();
                    user.Save();
                }
                c.Commit();
            }
            return "";
        }
    }
}
