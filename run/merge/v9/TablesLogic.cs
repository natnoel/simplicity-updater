﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Anacle.DataFramework;

namespace LogicLayer
{
    public partial class TablesLogic
    {
        public static TApplicationSetting tApplicationSetting = SchemaFactory.Get<TApplicationSetting>();
        public static TApplicationSettingService tApplicationSettingService = SchemaFactory.Get<TApplicationSettingService>();
        public static TApplicationSettingSmsKeywordHandler tApplicationSettingSmsKeywordHandler = SchemaFactory.Get<TApplicationSettingSmsKeywordHandler>();
        public static TApplicationSettingMenuItem tApplicationSettingMenuItem = SchemaFactory.Get<TApplicationSettingMenuItem>();

        public static TCraft tCraft = SchemaFactory.Get<TCraft>();

        public static TCustomizedAttributeField tCustomizedAttributeField = SchemaFactory.Get<TCustomizedAttributeField>();
        public static TCustomizedAttributeFieldValue tCustomizedAttributeFieldValue = SchemaFactory.Get<TCustomizedAttributeFieldValue>();
        public static TCustomizedRecordObject tCustomizedRecordObject = SchemaFactory.Get<TCustomizedRecordObject>();
        public static TCustomizedRecordField tCustomizedRecordField = SchemaFactory.Get<TCustomizedRecordField>();
        public static TCustomizedRecordFieldValue tCustomizedRecordFieldValue = SchemaFactory.Get<TCustomizedRecordFieldValue>();

        public static TFunction tFunction = SchemaFactory.Get<TFunction>();
        public static TLanguage tLanguage = SchemaFactory.Get<TLanguage>();

        public static TPosition tPosition = SchemaFactory.Get<TPosition>();

        public static TRole tRole = SchemaFactory.Get<TRole>();
        public static TRoleFunction tRoleFunction = SchemaFactory.Get<TRoleFunction>();

        public static TRunningNumber tRunningNumber = SchemaFactory.Get<TRunningNumber>();
        public static TRunningNumberGenerator tRunningNumberGenerator = SchemaFactory.Get<TRunningNumberGenerator>();

        public static TShift tShift = SchemaFactory.Get<TShift>();
        public static TTechnicianRoster tTechnicianRoster = SchemaFactory.Get<TTechnicianRoster>();
        public static TTechnicianRosterItem tTechnicianRosterItem = SchemaFactory.Get<TTechnicianRosterItem>();
        public static TTechnicianRosterItemDetail tTechnicianRosterItemDetail = SchemaFactory.Get<TTechnicianRosterItemDetail>();
        public static TTechnicianRosterReplacementItem tTechnicianRosterReplacementItem = SchemaFactory.Get<TTechnicianRosterReplacementItem>();

        public static TUser tUser = SchemaFactory.Get<TUser>();
        public static TVendorUser tVendorUser = SchemaFactory.Get<TVendorUser>();
        public static TUserActionTracing tUserActionTracing = SchemaFactory.Get<TUserActionTracing>();
        public static TUserBanLog tUserBanLog = SchemaFactory.Get<TUserBanLog>();
        public static TUserDelegatedPosition tUserDelegatedPosition = SchemaFactory.Get<TUserDelegatedPosition>();
        public static TUserPasswordHistory tUserPasswordHistory = SchemaFactory.Get<TUserPasswordHistory>();
        public static TUserPermanentPosition tUserPermanentPosition = SchemaFactory.Get<TUserPermanentPosition>();
        public static TUserSearchPreference tUserSearchPreference = SchemaFactory.Get<TUserSearchPreference>();
        public static TUserSearchGridViewPreference tUserSearchGridViewPreference = SchemaFactory.Get<TUserSearchGridViewPreference>();
        public static TUserWebPartsPersonalization tUserWebPartsPersonalization = SchemaFactory.Get<TUserWebPartsPersonalization>();

       // public static TPinboard tPinboard = SchemaFactory.Get<TPinboard>();
        public static TPinboardDetail tPinboardDetail = SchemaFactory.Get<TPinboardDetail>();

        public static TReminder tReminder = SchemaFactory.Get<TReminder>();

        public static TUserSignIn tUserSignIn = SchemaFactory.Get<TUserSignIn>();
        public static TUserSignInHistory tUserSignInHistory = SchemaFactory.Get<TUserSignInHistory>();
        public static TUserAssignment tUserAssignment = SchemaFactory.Get<TUserAssignment>();
    }
}
