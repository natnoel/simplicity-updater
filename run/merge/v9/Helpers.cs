﻿using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Text;
using Anacle.DataFramework;
using Anacle.UIFramework;
using LogicLayer;

/// <summary>
/// Summary description for Helpers
/// </summary>
public class Helpers
{
    public Helpers()
    {
    }

    public static void ShowMessages(Control c, Hashtable OutputObject)
    {
        foreach (Control child in c.Controls)
        {
            string key = null;

            if (child is UIFieldBase)
            {
                key = "ERR:" + child.ID.ToString();
                if(OutputObject.ContainsKey(key))
                {
                   ((UIFieldBase)child).ErrorMessage = OutputObject[key].ToString();
                }

                key = "ERRS:" + child.ID.ToString();
                if(OutputObject.ContainsKey(key))
                {
                    if (child.GetType() == typeof(Anacle.UIFramework.UIFieldTextBox))
                    {
                        ((Anacle.UIFramework.UIFieldTextBox)child).Text = 
                            OutputObject[key].ToString();
                    }
                    else
                    {
                        ((UIFieldBase)child).ErrorMessage = 
                            OutputObject[key].ToString();
                    }
                }

                key = "WARN:" + child.ID.ToString();
                if (OutputObject.ContainsKey(key))
                {
                   //TODO: Add code to display warning on relevant control
                }
            }
            ShowMessages(child, OutputObject);
        }
    }

    /// <summary>
    /// Select the default value for the searchable drop down list
    /// ONLY when the current selected value is null or blank
    /// </summary>
    /// <param name="ddl">1. drop down list</param>
    /// <param name="ddl">2. default value</param>
    public static void SetDefaultValueForDropDownList(UIFieldSearchableDropDownList ddl, string defaultValue)
    {
        if (ddl.Visible && ddl.IsContainerEnabled() && ddl.Enabled && ddl.Items.Count > 0
            && (ddl.SelectedValue == null || ddl.SelectedValue == ""))
            ddl.SelectedValue = defaultValue;
    }

    /// <summary>
    /// Select the ONLY non-blank item as the default item for the searchable drop down list
    /// ASSUMPTION: assuming there is at most only one blank item, and it can only be the first item
    /// </summary>
    /// <param name="ddl">1. drop down list</param>
    public static void SetDefaultValueForDropDownList(UIFieldSearchableDropDownList ddl)
    {
        if (ddl.Visible && ddl.IsContainerEnabled() && ddl.Enabled && ddl.Items.Count > 0
            && (ddl.SelectedValue == null || ddl.SelectedValue == ""))
        {
            if(ddl.Items.Count == 1 && ddl.Items[0].Value != null && ddl.Items[0].Value.ToString() != "")
                ddl.SelectedIndex = 0;
            else if (ddl.Items.Count == 2 && (ddl.Items[0].Value == null || ddl.Items[0].Value.ToString() == ""))
                ddl.SelectedIndex = 1;
        }
    }

    /// <summary>
    /// Select the default value for the drop down list
    /// ONLY when the current selected value is null or blank
    /// </summary>
    /// <param name="ddl">1. drop down list</param>
    /// <param name="ddl">2. default value</param>
    public static void SetDefaultValueForDropDownList(UIFieldDropDownList ddl, string defaultValue)
    {
        if (ddl.Visible && ddl.IsContainerEnabled() && ddl.Enabled && ddl.Items.Count > 0
            && (ddl.SelectedValue == null || ddl.SelectedValue == ""))
            ddl.SelectedValue = defaultValue;
    }

    /// <summary>
    /// Select the ONLY non-blank item as the default item for the drop down list
    /// ASSUMPTION: assuming there is at most only one blank item, and it can only be the first item
    /// </summary>
    /// <param name="ddl">1. drop down list</param>
    public static void SetDefaultValueForDropDownList(UIFieldDropDownList ddl)
    {
        if (ddl.Visible && ddl.IsContainerEnabled() && ddl.Enabled && ddl.Items.Count > 0
            && (ddl.SelectedValue == null || ddl.SelectedValue == ""))
        {
            if (ddl.Items.Count == 1 && ddl.Items[0].Value != null && ddl.Items[0].Value.ToString() != "")
                ddl.SelectedIndex = 0;
            else if (ddl.Items.Count == 2 && (ddl.Items[0].Value == null || ddl.Items[0].Value.ToString() == ""))
                ddl.SelectedIndex = 1;
        }
    }


    /// <summary>
    /// Returns a flag to indicate if any of the strings is contained
    /// within the original string.
    /// </summary>
    /// <param name="strings"></param>
    /// <returns></returns>
    private static bool ContainsAny(string originalString, params string[] strings)
    {
        if (strings != null)
            foreach (string s in strings)
                if (originalString.Contains(s))
                    return true;
        return false;
    }

    /// <summary>
    /// Translates the specified text from the objects.resx file.
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    public static string TranslateWorkflowStateItem(string text)
    {
        string translatedText = Resources.WorkflowStates.ResourceManager.GetString(text);
        if (translatedText == null || translatedText == "")
            return text;
        return translatedText;
    }


    /*
    /// <summary>
    /// Gets a colour that represents a workflow state.
    /// </summary>
    /// <param name="?"></param>
    /// <returns></returns>
    [Obsolete]
    public static System.Drawing.Color GetWorkflowStateColor(string currentStateName)
    {
        System.Drawing.Color workflowColor = System.Drawing.Color.Gray;
        
        // Initial states
        if (ContainsAny(currentStateName, "Start", "Draft"))
            workflowColor = System.Drawing.Color.Gray;

        // Pending states
        else if (ContainsAny(currentStateName, "Pending"))
            workflowColor = System.Drawing.Color.FromArgb(0x0f, 0x35, 0x7b);

        // Complete states
        else if (ContainsAny(currentStateName, 
            "Approved", "Confirmed", "Acknowledged", "Awarded", "Close", 
            "Complete", "Commit", "Submit", "Served"))
            workflowColor = System.Drawing.Color.FromArgb(0x19, 0xa2, 0x0b);

        // Query state
        else if (ContainsAny(currentStateName, "Query"))
            workflowColor = System.Drawing.Color.Orange;

        // Rejection states
        else if (ContainsAny(currentStateName, "Rejected", "Cancelled", "Voided"))
            workflowColor = System.Drawing.Color.FromArgb(0xcb, 0x18, 0x18);

        return workflowColor;
    }
    */

    /// <summary>
    /// Gets a CSS class that represents the workflow state
    /// </summary>
    static List<DictionaryEntry> stateClassEntries = null;
    public static string GetWorkflowStateClass(string currentStateName)
    {
        if (stateClassEntries == null)
        {
            stateClassEntries = new List<DictionaryEntry>();
            foreach (DictionaryEntry d in Resources.WorkflowStatesClass.ResourceManager.GetResourceSet(System.Threading.Thread.CurrentThread.CurrentCulture, true, true))
                stateClassEntries.Add(d);
            stateClassEntries.Sort(delegate (DictionaryEntry x, DictionaryEntry y)
            {
                int xLengthAlpha = x.Key.ToString().Replace("_", "").Length;
                int xLengthSymbol = x.Key.ToString().Length - xLengthAlpha;

                int yLengthAlpha = y.Key.ToString().Replace("_", "").Length;
                int yLengthSymbol = y.Key.ToString().Length - yLengthAlpha;

                int xScore = xLengthAlpha * 100 - xLengthSymbol;
                int yScore = yLengthAlpha * 100 - yLengthSymbol;

                if (xScore < yScore)
                    return 1;
                else if (yScore < xScore)
                    return -1;
                else
                    return (x.Key.ToString().CompareTo(y.Key.ToString()));
            });
        }

        foreach (DictionaryEntry state in stateClassEntries)
        {
            if (state.Key.ToString() == currentStateName)
                return state.Value.ToString();

            if (state.Key.ToString().StartsWith("_") && state.Key.ToString().EndsWith("_") && currentStateName.Contains(state.Key.ToString().Replace("_", "")))
                return state.Value.ToString();

            if (state.Key.ToString().EndsWith("_") && currentStateName.StartsWith(state.Key.ToString().Replace("_", "")))
                return state.Value.ToString();

            if (state.Key.ToString().StartsWith("_") && currentStateName.EndsWith(state.Key.ToString().Replace("_", "")))
                return state.Value.ToString();
        }
        return "mdl-chip-gray";
    }


    /// <summary>
    /// Returns a flag indicating if the web compilation debug
    /// mode is set to true.
    /// </summary>
    /// <returns></returns>
    public static bool IsDebug()
    {
        System.Web.Configuration.CompilationSection compilationSection =
            (System.Web.Configuration.CompilationSection)System.Configuration.ConfigurationManager.GetSection(@"system.web/compilation");

        return compilationSection.Debug;
    }


    public static void LogException(Exception ex, System.Web.HttpRequest Request)
    {
        try
        {
            string err = "";
            err = err + DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss") + "\r\n";
            while (ex != null)
            {
                err = err + "Error in     :" + Request.Url.ToString() + "\r\n";
                err = err + "Error Message:" + ex.Message.ToString() + "\r\n";
                err = err + "Stack Trace  :" + "\r\n" + ex.StackTrace.ToString() + "\r\n";
                if (ex is ObjectModifiedException)
                {
                    ObjectModifiedException oex = ex as ObjectModifiedException;
                    err = err + "Object ID    : " + oex.ObjectID + "\r\n";
                    err = err + "Object Name  : " + oex.ObjectName + "\r\n";
                    err = err + "Object Number: " + oex.ObjectNumber + "\r\n";
                    err = err + "Object Type  : " + oex.ObjectType + "\r\n";
                }
                ex = ex.InnerException;
            }

            using (System.IO.StreamWriter sw = new System.IO.StreamWriter(
                String.Format(System.Configuration.ConfigurationManager.AppSettings["ErrorLogPath"], DateTime.Now), true))
            {
                sw.WriteLine(err);
                sw.Close();
            }
        }
        catch
        {
        }
    }

    public static string ConvertExceptionToString(Exception ex)
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            Exception currentException = ex;
            while (currentException != null)
            {
                sb.Append(currentException.Message + "\n" + currentException.StackTrace + "\n\n");
                currentException = currentException.InnerException;
            }

            return sb.ToString();
        }
        catch
        {
        }

        return string.Empty;
    }

    public static void LogException(string message)
    {
        try
        {
            using (System.IO.StreamWriter sw = new System.IO.StreamWriter(
                String.Format(System.Configuration.ConfigurationManager.AppSettings["ErrorLogPath"], DateTime.Now), true))
            {
                sw.WriteLine(message);
                sw.Close();
            }
        }
        catch
        {
        }
    }



    public static void SetButtonVisibility(UIButton button, bool visible)
    {
        if (button != null)
            button.Visible = visible;
    }


    /// <summary>
    /// Populates statuses into a listbox.
    /// </summary>
    /// <param name="listStatus"></param>
    /// <param name="objectTypeName"></param>
    public static void PopulateStatuses(UIFieldListBox listStatus, string objectTypeName)
    {
        listStatus.Bind(OActivity.GetStatuses(objectTypeName), "ObjectName", "ObjectName");
        foreach (ListItem item in listStatus.Items)
        {
            string translated = Resources.WorkflowStates.ResourceManager.GetString(item.Text);
            if (translated != null && translated != "")
                item.Text = translated;
        }
    }

    /// <summary>
    /// Set a given tab as default when the object is in the given state list
    /// </summary>
    /// <param name="tabStrip"></param>
    /// <param name="o"></param>
    /// <param name="ifTrueTabView"></param>
    /// <param name="elseTabView"></param>
    /// <param name="workflowStates"></param>
    public static void SetDefaultTabForWorkflowStates(
        UITabStrip tabStrip,
        LogicLayerWorkflowPersistentObject o,
        UITabView defaultTab, 
        params string[] workflowStates)
    {
        if(!string.IsNullOrEmpty(o.CurrentActivityName) && o.CurrentActivityName.Is(workflowStates))
            tabStrip.SelectedTab = defaultTab;       
    }


    /// <summary>
    /// Creates a HTML for the given menu name.
    /// </summary>
    /// <param name="menuName"></param>
    /// <returns></returns>
    public static string CreateHtmlIcon(string menuName)
    {
        return "<i class='" + GetIconCssClass(menuName) + "'>" + GetIconText(menuName) + "</i>";
    }

    /// <summary>
    /// Creates a HTML for the given menu name.
    /// </summary>
    /// <param name="menuName"></param>
    /// <returns></returns>
    public static string GetIconCssClass(string menuName)
    {
        string s = "";
        foreach (char c in menuName)
            if (c == '&')
                s += 'N';
            else if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))
                s += c;
            else
                s += "_";

        string result = Resources.MenuIcons.ResourceManager.GetString(s);
        if (result == null)
            return "";
        string[] cssText = result.Split('|');

        return cssText[0];
    }

    /// <summary>
    /// Creates a HTML for the given menu name.
    /// </summary>
    /// <param name="menuName"></param>
    /// <returns></returns>
    public static string GetIconText(string menuName)
    {
        string s = "";
        foreach (char c in menuName)
            if (c == '&')
                s += 'N';
            else if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))
                s += c;
            else
                s += "_";

        string result = Resources.MenuIcons.ResourceManager.GetString(s);
        if (result == null)
            return "";
        string[] cssText = result.Split('|');

        if (cssText.Length > 1)
            return cssText[1];
        else
            return "";
    }


    /// <summary>
    /// Caches and returns the list of menu items accessible by the user.
    /// </summary>
    /// <param name="user"></param>
    /// <returns></returns>
    public static DataTable GetCachedMenusAccessibleByUser(OUser user)
    {
        if (HttpContext.Current.Session["User_AccessibleMenus"] == null)
        {
            HttpContext.Current.Session["User_AccessibleMenus"] = OFunction.GetMenusAccessibleByUser(user);
        }
        return (DataTable)HttpContext.Current.Session["User_AccessibleMenus"];
    }


    /// <summary>
    /// Caches and returns the list of menu items accessible by the user.
    /// </summary>
    /// <param name="user"></param>
    /// <returns></returns>
    public static DataTable GetCachedMenusAccessibleByUser(OUser user, string categoryName)
    {
        if (HttpContext.Current.Session["User_AccessibleMenus"] == null)
        {
            HttpContext.Current.Session["User_AccessibleMenus"] = OFunction.GetMenusAccessibleByUser(user);
        }

        DataTable full = (DataTable)HttpContext.Current.Session["User_AccessibleMenus"];
        DataTable dt = full.Clone();

        foreach (DataRow dr in full.Rows)
        {
            if (dr["CategoryName"].ToString() == categoryName)
                dt.Rows.Add(dr.ItemArray);
        }
        return dt;
    }


    /// <summary>
    /// Caches and returns the list of reports accessible by the user.
    /// </summary>
    /// <param name="user"></param>
    /// <returns></returns>
    public static DataTable GetCachedReportsAccessibleByUser(OUser user)
    {
        if (HttpContext.Current.Session["User_AccessibleReports"] == null)
        {
            HttpContext.Current.Session["User_AccessibleReports"] = OReport.GetAllReportsByUserAndCategoryName(user);
        }
        return (DataTable)HttpContext.Current.Session["User_AccessibleReports"];
    }


    public static string HtmlToPdf(string html)
    {
        string htmlFileName = ConfigurationManager.AppSettings["TempFolder"] + Guid.NewGuid().ToString() + ".html";

        using (StreamWriter sw = new StreamWriter(htmlFileName))
        {
            sw.Write(html);
            sw.Close();
        }
        return HtmlUrlToPdf(htmlFileName);
    }

    public static string HtmlUrlToPdf(string url)
    {
        var p = new System.Diagnostics.Process();

        /*
        if (!url.StartsWith("http"))
        {
            string content;
            using (StreamReader sr = new StreamReader(url))
            {
                content = sr.ReadToEnd();
                if (!content.Contains("<header>"))
                {
                    content =
                        @"<style>
@page {
  margin: 0;
}
@media print {
body { margin: 1.6cm; }
  footer {
    position: fixed;
    bottom: 0;
  }
  header {
    position: fixed;
    top: 0;
  }
}</style>" + content;
                    content = "<header></header>" + content + "<footer></footer>";
                }
            }
            using (StreamWriter sw = new StreamWriter(url))
            {
                sw.Write(content);
                sw.Close();
            }
        }
        */

        string outputFileName = ConfigurationManager.AppSettings["TempFolder"] + Guid.NewGuid().ToString() + ".pdf";

        string arguments = "--virtual-time-budget=5000 --no-sandbox --bwsi --headless --disable-gpu  \"" + url + "\" --print-to-pdf=\"" + outputFileName + "\" ";

        var startInfo = new System.Diagnostics.ProcessStartInfo(ConfigurationManager.AppSettings["ChromiumPath"], arguments);
        startInfo.CreateNoWindow = true;
        startInfo.FileName = ConfigurationManager.AppSettings["ChromiumPath"];

        startInfo.UseShellExecute = false;
        startInfo.WorkingDirectory = System.IO.Path.GetDirectoryName(ConfigurationManager.AppSettings["ChromiumPath"]);

        startInfo.Domain = ConfigurationManager.AppSettings["ChromiumDomain"];
        startInfo.UserName = ConfigurationManager.AppSettings["ChromiumUser"];
        var ss = new System.Security.SecureString();
        var pwd = Security.Decrypt(ConfigurationManager.AppSettings["ChromiumPassword"]);
        foreach (char c in pwd)
            ss.AppendChar(c);
        startInfo.Password = ss;

        p.StartInfo = startInfo;
        p.Start();

        //  wait n milliseconds for exit (as after exit, it can't read the output)
        p.WaitForExit(60000);

        // read the exit code, close process
        int returnCode = p.ExitCode;
        p.Close();

        // Wait for about 20 seconds until the file is generated.
        // 
        int waitCount = 20;
        while (waitCount > 0)
        {
            if (System.IO.File.Exists(outputFileName))
                return outputFileName;
            System.Threading.Thread.Sleep(1000);
            waitCount--;
        }

        return null;
    }

}
