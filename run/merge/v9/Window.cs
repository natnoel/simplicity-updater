//========================================================================
// $Product: Anacle Enterprise Asset Management
// $Version: 5.0
//
// Copyright 2006 (c) Anacle Systems Pte. Ltd.
// All rights reserved.
//========================================================================
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;

using System.Web;

using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Anacle.DataFramework;
using LogicLayer;
using System.Security.Cryptography;

/// <summary>
/// Summary description for Window
/// </summary>
public class Window
{
    public const int popupWidth = 1000;
    public const int popupHeight = 680;

    public const string JavaScriptStart = "<script type='text/javascript'>";
    public const string JavaScriptEnd = "</script>";

    [ThreadStatic]
    public static bool RedirectOccurred = false;

    private static readonly System.Text.RegularExpressions.Regex scriptTagRegex = new
        System.Text.RegularExpressions.Regex("script",
        System.Text.RegularExpressions.RegexOptions.IgnoreCase |
        System.Text.RegularExpressions.RegexOptions.Multiline); 

    /// <summary> 
    /// Processes the provided string, creating a quoted JavaScript string literal. 
    /// </summary> 
    /// <param name="str">The string to process</param> 
    /// <returns>A string containing a quoted JavaScript string literal</returns> 
    public static string JavaScriptStringLiteral(string str)
    {
        var sb = new System.Text.StringBuilder();
        sb.Append("\"");
        foreach (char c in str)
        {
            switch (c)
            {
                case '\"': sb.Append("\\\""); break;
                case '\\': sb.Append("\\\\"); break;
                case '\b': sb.Append("\\b"); break;
                case '\f': sb.Append("\\f"); break;
                case '\n': sb.Append("\\n"); break;
                case '\r': sb.Append("\\r"); break;
                case '\t': sb.Append("\\t"); break;
                default:
                    int i = (int)c;
                    if (i < 32 || i > 127)
                    {
                        sb.AppendFormat("\\u{0:X04}", i);
                    }
                    else
                    {
                        sb.Append(c);
                    }
                    break;
            }
        }
        sb.Append("\"");

        // If a Javascript tag contains "</script>", then it terminates a     
        // script block.  
        // Start by replacing each 's'/'S' with an escape     
        // sequence so it doesn't trigger this.     
        return scriptTagRegex.Replace(
            sb.ToString(),         
            m => (m.Value[0] == 's' ? "\\u0073" : "\\u0053") + m.Value.Substring(1)); 

        return sb.ToString();
    }
                                                                                                                           /// 

    /// <summary>
    /// FIX: 2011.03.29, Kien Trung
    /// Escape the special characters in the encoded string
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public static string escMap(string path)
    {
        string[] esc = {" ","<",">","#","{","}","|","\\","^",
                     "~","[", "]","\'",";","/","?",":","@","=",
                     "&","$"};

        string[] value = {"%20","%3C","%3E","%23","%7B",
                          "%7D","%7C","%5C","%5E","%7E","%5B",
                          "%5D","%27","%3B","%2F","%3F","%3A",
                          "%40","%3D","%26","%24"};

        for (int i = 0; i < 21; i++)
        {
            if (path.Contains(esc[i]))
            {
                path = path.Replace(esc[i], value[i]);
            }
        }

        return path;

    }


    /// <summary>
    /// Emits javascript to the page.
    /// </summary>
    /// <param name="s"></param>
    public static void WriteJavascript( string s )
    {
        ScriptManager.RegisterClientScriptBlock(
            HttpContext.Current.Handler as Page, typeof(Page),
            "output" + Guid.NewGuid(), JavaScriptStart + s + JavaScriptEnd, false);
    }


    /// <summary>
    /// Emits javascript to the page.
    /// </summary>
    /// <param name="s"></param>
    public static void WriteStartupJavascript(string s)
    {
        ScriptManager.RegisterStartupScript(
            HttpContext.Current.Handler as Page, typeof(Page),
            "output" + Guid.NewGuid(), JavaScriptStart + s + JavaScriptEnd, false);
    }



    /// <summary>
    /// Emits javascript to close the current browser window.
    /// </summary>
    public static void Close()
    {
        WriteJavascript( "closePage();" );
    }

    

    /// <summary>
    /// Pop-up a page that allows the user to download a file
    /// given the binary image of that file.
    /// </summary>
    /// <param name="url"></param>
    /// <param name="target"></param>
    public static void Download(byte[] binaryImage, string fileName, string contentType)
    {
        Page page = HttpContext.Current.Handler as Page;
        
        // Fix: 2/25/2012, Wang Yiyuan
        // catch exception when binaryImage is null
        try
        {
            Anacle.UIFramework.DiskCache.Add("FILEBINARY", binaryImage);
        }
        catch (Exception e)
        {
            Window.Open(page.Request.ApplicationPath + "/appErrorFileNotFound.aspx");
            return;
        }

        /// FIX: 2011.03.29, Kien Trung
        // Escape special characters of filename.
        //fileName = escMap(fileName);

        // 2015.05.01
        // KL
        // fix a security issue which allows user to download file by changing the URL
        //
        RNGCryptoServiceProvider r = new RNGCryptoServiceProvider();
        byte[] data = new byte[4];
        r.GetBytes(data);
        string salt = BitConverter.ToString(data);
        fileName = Security.Encrypt(salt + fileName);
        contentType = Security.Encrypt(salt + contentType);
        string key = Security.Encrypt(salt + "FILEBINARY");

        // pop-up document in a new window
        DownloadUrl(
            page.Request.ApplicationPath + "/components/document.aspx" +
            // FIX: 201.01.30
            // Kim Foong
            // Fix to prevent double encoding of the % sign,
            // which causes problems for some proxy servers. (there might be 404 errors)
            //
            "?n=" + escMap(HttpUtility.UrlEncode(fileName)) +
            "&k=" + escMap(HttpUtility.UrlEncode(key)) +
            "&m=" + escMap(HttpUtility.UrlEncode(contentType)));
    }


    /// <summary>
    /// Pop-up a page that allows the user to download a file
    /// given the text of that file.
    /// </summary>
    /// <param name="url"></param>
    /// <param name="target"></param>
    public static void Download(string textContent, string fileName, string contentType)
    {
        Page page = HttpContext.Current.Handler as Page;
        Anacle.UIFramework.DiskCache.Add("FILETEXT", textContent);

        /// FIX: 2011.03.29, Kien Trung
        // Escape special characters of filename.
        //fileName = escMap(fileName);

        // 2015.05.01
        // KL
        // fix a security issue which allows user to download file by changing the URL
        //
        RNGCryptoServiceProvider r = new RNGCryptoServiceProvider();
        byte[] data = new byte[4];
        r.GetBytes(data);
        string salt = BitConverter.ToString(data);
        fileName = Security.Encrypt(salt + fileName);
        contentType = Security.Encrypt(salt + contentType);
        string key = Security.Encrypt(salt + "FILETEXT");

        // pop-up document in a new window
        DownloadUrl(
            page.Request.ApplicationPath + "/components/document.aspx" +
            // FIX: 201.01.30
            // Kim Foong
            // Fix to prevent double encoding of the % sign,
            // which causes problems for some proxy servers. (there might be 404 errors)
            //
            "?n=" + escMap(HttpUtility.UrlEncode(fileName)) +
            "&k=" + escMap(HttpUtility.UrlEncode(key)) +
            "&m=" + escMap(HttpUtility.UrlEncode(contentType)));
    }


    /// <summary>
    /// Pop-up a page that allows the user to download a file
    /// given the file path of the file (relative to the
    /// application's physical directory).
    /// </summary>
    /// <param name="url"></param>
    /// <param name="target"></param>
    public static void DownloadFile(string filePath, string fileName, string contentType)
    {
        Page page = HttpContext.Current.Handler as Page;

        string filename = System.IO.Path.GetFileName(filePath);

        /// FIX: 2011.03.29, Kien Trung
        // Escape special characters of filename.
        //fileName = escMap(fileName);

        // 2015.05.01
        // KL
        // fix a security issue which allows user to download file by changing the URL
        //
        RNGCryptoServiceProvider r = new RNGCryptoServiceProvider();
        byte[] data = new byte[4];
        r.GetBytes(data);
        string salt = BitConverter.ToString(data);
        fileName = Security.Encrypt(salt + fileName);
        contentType = Security.Encrypt(salt + contentType);
        filePath = Security.Encrypt(salt + filePath);

        // pop-up document in a new window
        DownloadUrl(
            page.Request.ApplicationPath + "/components/document.aspx" +
            // FIX: 201.01.30
            // Kim Foong
            // Fix to prevent double encoding of the % sign,
            // which causes problems for some proxy servers. (there might be 404 errors)
            //
            "?n=" + escMap(HttpUtility.UrlEncode(fileName)) +
            "&p=" + escMap(HttpUtility.UrlEncode(filePath)) +
            "&m=" + escMap(HttpUtility.UrlEncode(contentType)));
    }


    /// <summary>
    /// Downloads a file url by redirecting the user to it.
    /// 
    /// Compared to the traditional window.open, 
    /// this helps to prevent IE from blocking the download if the
    /// user's "Automatic prompting for file download" is disabled.
    /// </summary>
    /// <param name="url"></param>
    public static void DownloadUrl(string url)
    {
        // pop-up document in a new window
        WriteJavascript(
            "var __up__ = document.getElementById('__updateprogress__'); if( __up__ ) __up__.style.display = 'none'; " +
            "window.location = " +
            JavaScriptStringLiteral(url) + ";");
    }


    /// <summary>
    /// Create a datatable to store list of return objects.
    /// </summary>
    public static void CreateReturnDataTable()
    {
        DataTable dt = new DataTable();

        dt.Columns.Add("ObjectType");
        dt.Columns.Add("ObjectID");
        dt.Columns.Add("TabIndex");
        dt.Columns.Add("ViewEdit");
        

        HttpContext.Current.Session["ReturnDataTable"] = dt;
    }

    /// <summary>
    /// Clear the datatable, when an new object is opened from the search page.
    /// </summary>
    public static void ClearReturnDataTable()
    {
        if (HttpContext.Current.Session["ReturnDataTable"] != null)
        {
            DataTable dt = (DataTable)HttpContext.Current.Session["ReturnDataTable"];
            dt.Rows.Clear();
        }
    }

    /// <summary>
    /// Return to the next return object.
    /// </summary>
    public static void GoToNextReturnObject(Page p)
    {
        if (HttpContext.Current.Session["ReturnDataTable"] != null)
        {
            DataTable dt = (DataTable)HttpContext.Current.Session["ReturnDataTable"];
            if (dt.Rows.Count > 0)
            {
                DataRow r = dt.Rows[dt.Rows.Count - 1];
                
                Window.OpenViewObjectPage(p, r["ObjectType"].ToString(), r["ObjectID"].ToString(), QueryString.New("TAB", Security.Encrypt(r["TabIndex"].ToString())));
                dt.Rows.RemoveAt(dt.Rows.Count - 1);
            }
        }
    }

    /// <summary>
    /// Check if there is a return object, returns null if none, return objecttypename if there is.
    /// </summary>
    public static String HasNextReturnObject()
    {
        if (HttpContext.Current.Session["ReturnDataTable"] != null)
        {
            DataTable dt = (DataTable)HttpContext.Current.Session["ReturnDataTable"];
            if (dt.Rows.Count > 0)
            {
                return dt.Rows[dt.Rows.Count - 1]["ObjectType"].ToString();
            }
             

        }
        return null;
    }

    /// <summary>
    /// Add a return object to the datatable.
    /// </summary>
    public static void PushReturnObject(String objectType, String objectID, int tabIndex)
    {
        if (HttpContext.Current.Session["ReturnDataTable"] == null)
            Window.CreateReturnDataTable();

        DataTable dt = (DataTable)HttpContext.Current.Session["ReturnDataTable"];

        dt.Rows.Add(objectType, objectID, tabIndex);
    }


    /// <summary>
    /// Emits javascript to open a URL in a new browser window.
    /// </summary>
    /// <param name="url">The URL to open.</param>
    /// <param name="target">The target window to open. Leave blank to open
    public static void Open(string url, string target)
    {
        WriteJavascript("openPage('" + url + "', '" + target + "');");
    }


    /// <summary>
    /// Emits javascript to open a URL in the same browser window.
    /// </summary>
    /// <param name="url">The URL to open.</param>
    public static void Open(string url)
    {
        WriteJavascript( "window.open('" + url + "');" );
    }


    //KL: from trung dashboard code
    /// <summary>
    /// Emits javascript to open a URL in a new browser window.
    /// </summary>
    /// <param name="url">The URL to open.</param>
    /// <param name="target">The target window to open. Leave blank to open
    public static void Open(string url, string target, string additionalString)
    {
        WriteJavascript("openPage('" + url + "', '" + target + "');");
    }
    //KL: end


    /// <summary>
    /// Emits javascript to open the edit page in NEW (create) mode.
    /// </summary>
    /// <param name="page">The Page object.</param>
    /// <param name="objectType">The type of object.</param>
    /// <param name="additionalQueryString">Additional query strings to be
    /// appended to the end of the URL.</param>
    public static void OpenAddObjectPage(Page page, string objectType, string additionalQueryString)
    {
        // Mobile Web
        //
        bool mobileMode = false;
        if (page is Anacle.UIFramework.UIPageBase && ((Anacle.UIFramework.UIPageBase)page).MobileMode)
            mobileMode = true;

        OFunction function = OFunction.GetFunctionByObjectType(objectType);
        string url = function == null? string.Empty : (mobileMode ? function.EditMobileUrl : function.EditUrl);

        if (url == null || url.Trim() == "")
        {
            if (mobileMode)
                Window.WriteJavascript("alert(" + Window.JavaScriptStringLiteral(Resources.Errors.General_UnableToNavigateAsTheMobileUrlIsNotSetup) + ");");
            else
                Window.WriteJavascript("alert(" + Window.JavaScriptStringLiteral(Resources.Errors.General_UnableToNavigateAsTheDesktopUrlIsNotSetup) + ");");
            return;
        }

        RedirectOccurred = true;
        Window.Open(
            page.ResolveUrl(url) + "?ID=" +
            HttpUtility.UrlEncode(Security.Encrypt("NEW:" + ":" + AppSession.SaltID)) +
            "&TYPE=" + HttpUtility.UrlEncode(Security.Encrypt(objectType + ":" + AppSession.SaltID)) +
            "&" + additionalQueryString, "AnacleEAM_Window");
    }


    /// <summary>
    /// Emits javascript to open the edit page in NEW (create) mode, but
    /// the edit page does not automatically create the object.
    /// <para>
    /// </para>
    /// NOTE: DO NOT pass in N=? as part of the querystring, as this will
    /// cause the new object passed in to be lost.
    /// </summary>
    /// <param name="page">The Page object.</param>
    /// <param name="newObject">The new PersistentObject created outside of the object panel.</param>
    /// <param name="additionalQueryString">Additional query strings to be
    /// appended to the end of the URL.</param>
    public static void OpenAddObjectPage(Page page, PersistentObject newObject, string additionalQueryString)
    {
        // Mobile Web
        //
        bool mobileMode = false;
        if (page is Anacle.UIFramework.UIPageBase && ((Anacle.UIFramework.UIPageBase)page).MobileMode)
            mobileMode = true;

        page.Session["::SessionObject::"] = newObject;
        OFunction function = OFunction.GetFunctionByObjectType(newObject.GetType().BaseType.Name); 
        string url = null;

        if (function != null)
            url = mobileMode ? function.EditMobileUrl : function.EditUrl;

        if (url == null || url.Trim() == "")
        {
            if (mobileMode)
                Window.WriteJavascript("alert(" + Window.JavaScriptStringLiteral(Resources.Errors.General_UnableToNavigateAsTheMobileUrlIsNotSetup) + ");");
            else
                Window.WriteJavascript("alert(" + Window.JavaScriptStringLiteral(Resources.Errors.General_UnableToNavigateAsTheDesktopUrlIsNotSetup) + ");");
            return;
        }

        RedirectOccurred = true;
        Window.Open(
            page.ResolveUrl(url) + "?ID=" +
            HttpUtility.UrlEncode(Security.Encrypt("NEW2:" + AppSession.SaltID)) +
            "&TYPE=" + HttpUtility.UrlEncode(Security.Encrypt(newObject.GetType().BaseType.Name + ":" + AppSession.SaltID)) +
            "&" + additionalQueryString, "AnacleEAM_Window");
    }

    /// <summary>
    /// Emits javascript to open the edit page in NEW (create) mode.
    /// </summary>
    /// <param name="page">The Page object.</param>
    /// <param name="objectType">The type of object.</param>
    /// <param name="returnObject">The object you want to return to.</param>
    /// <param name="returnTabIndex">The tab index which you want to return to.</param>
    /// <param name="additionalQueryString">Additional query strings to be
    /// appended to the end of the URL.</param>
    public static void OpenAddObjectPageWithReturn(Page page, string objectType, PersistentObject returnObject, int returnTabIndex, string additionalQueryString)
    {
        if (returnObject.IsNew)
        {
            Window.WriteJavascript("alert(" + Window.JavaScriptStringLiteral(
                Resources.Errors.General_SaveObjectBeforeNavigatingToOther) + ");");
            return;
        }
        Window.PushReturnObject(returnObject.GetType().BaseType.Name, returnObject.ObjectID.ToString(), returnTabIndex);
        OpenAddObjectPage(page, objectType, additionalQueryString);
    }

    /// <summary>
    /// Emits javascript to open the edit page in NEW (create) mode, but
    /// the edit page does not automatically create the object.
    /// <para>
    /// </para>
    /// NOTE: DO NOT pass in N=? as part of the querystring, as this will
    /// cause the new object passed in to be lost.
    /// </summary>
    /// <param name="page">The Page object.</param>
    /// <param name="newObject">The new PersistentObject created outside of the object panel.</param>
    /// <param name="returnObject">The object you want to return to.</param>
    /// <param name="returnTabIndex">The tab index which you want to return to.</param>
    /// <param name="additionalQueryString">Additional query strings to be
    /// appended to the end of the URL.</param>
    public static void OpenAddObjectPageWithReturn(Page page, PersistentObject newObject, PersistentObject returnObject, int returnTabIndex, string additionalQueryString)
    {
        if (returnObject.IsNew)
        {
            Window.WriteJavascript("alert(" + Window.JavaScriptStringLiteral(
                Resources.Errors.General_SaveObjectBeforeNavigatingToOther) + ");");
            return;
        }
        Window.PushReturnObject(returnObject.GetType().BaseType.Name, returnObject.ObjectID.ToString(), returnTabIndex);
        OpenAddObjectPage(page, newObject, additionalQueryString);        
    }


    /// <summary>
    /// Emits javascript to open the edit page in EDIT mode.
    /// </summary>
    /// <param name="page">The Page object.</param>
    /// <param name="objectType">The type of object.</param>
    /// <param name="additionalQueryString">Additional query strings to be
    /// appended to the end of the URL.</param>
    public static void OpenEditObjectPage(Page page, string objectType, string unencryptedGuid, string additionalQueryString)
    {
        // Mobile Web
        //
        bool mobileMode = false;
        if (page is Anacle.UIFramework.UIPageBase && ((Anacle.UIFramework.UIPageBase)page).MobileMode)
            mobileMode = true;

        OFunction function = OFunction.GetFunctionByObjectType(objectType);
        string url = null;

        if (function != null)
            url = mobileMode ? function.EditMobileUrl : function.EditUrl;

        if (url == null || url.Trim() == "")
        {
            if (mobileMode)
                Window.WriteJavascript("alert(" + Window.JavaScriptStringLiteral(Resources.Errors.General_UnableToNavigateAsTheMobileUrlIsNotSetup) + ");");
            else
                Window.WriteJavascript("alert(" + Window.JavaScriptStringLiteral(Resources.Errors.General_UnableToNavigateAsTheDesktopUrlIsNotSetup) + ");");
            return;
        }

        RedirectOccurred = true;
        //Window.Open(
        //    page.ResolveUrl(url) + "?ID=" +
        //    HttpUtility.UrlEncode(Security.Encrypt("EDIT:" + unencryptedGuid + ":" + AppSession.SaltID)) +
        //    "&TYPE=" + HttpUtility.UrlEncode(Security.Encrypt(objectType + ":" + AppSession.SaltID)) +
        //    "&" + additionalQueryString, "AnacleEAM_Window");
        Window.Open(
            page.ResolveUrl(url) + "?ID=" +
            HttpUtility.UrlEncode(Security.Encrypt("EDIT:" + unencryptedGuid + ":" + AppSession.SaltID)) +
            "&TYPE=" + HttpUtility.UrlEncode(Security.Encrypt(objectType + ":" + AppSession.SaltID)) +
            "&" + additionalQueryString, "AnacleEAM_Window");
    }

    /// <summary>
    /// Emits javascript to open the edit page in EDIT mode.
    /// </summary>
    /// <param name="page">The Page object.</param>
    /// <param name="objectType">The type of object.</param>
    /// <param name="returnObject">The object you want to return to.</param>
    /// <param name="returnTabIndex">The tab index which you want to return to.</param>
    /// <param name="additionalQueryString">Additional query strings to be
    /// appended to the end of the URL.</param>
    public static void OpenEditObjectPageWithReturn(Page page, string objectType, string unencryptedGuid, PersistentObject returnObject, int returnTabIndex, string additionalQueryString)
    {
        if (returnObject.IsNew)
        {
            Window.WriteJavascript("alert(" + Window.JavaScriptStringLiteral(
                Resources.Errors.General_SaveObjectBeforeNavigatingToOther) + ");");
            return;
        }
        Window.PushReturnObject(returnObject.GetType().BaseType.Name, returnObject.ObjectID.ToString(), returnTabIndex);
        OpenEditObjectPage(page, objectType, unencryptedGuid, additionalQueryString);
    }


    /// <summary>
    /// Emits javascript to open the edit page in VIEW (read-only) mode.
    /// </summary>
    /// <param name="page">The Page object.</param>
    /// <param name="objectType">The type of object.</param>
    /// <param name="additionalQueryString">Additional query strings to be
    /// appended to the end of the URL.</param>
    public static void OpenViewObjectPage(Page page, string objectType, string unencryptedGuid, string additionalQueryString)
    {
        // Mobile Web
        //
        bool mobileMode = false;
        if (page is Anacle.UIFramework.UIPageBase && ((Anacle.UIFramework.UIPageBase)page).MobileMode)
            mobileMode = true;

        OFunction function = OFunction.GetFunctionByObjectType(objectType);
        string url = null;

        if (function != null)
            url = mobileMode ? function.EditMobileUrl : function.EditUrl;

        if (url == null || url.Trim() == "")
        {
            if (mobileMode)
                Window.WriteJavascript("alert(" + Window.JavaScriptStringLiteral(Resources.Errors.General_UnableToNavigateAsTheMobileUrlIsNotSetup) + ");");
            else
                Window.WriteJavascript("alert(" + Window.JavaScriptStringLiteral(Resources.Errors.General_UnableToNavigateAsTheDesktopUrlIsNotSetup) + ");");
            return;
        }

        RedirectOccurred = true;
        Window.Open(
            page.ResolveUrl(url) + "?ID=" +
            HttpUtility.UrlEncode(Security.Encrypt("VIEW:" + unencryptedGuid + ":" + AppSession.SaltID)) +
            "&TYPE=" + HttpUtility.UrlEncode(Security.Encrypt(objectType + ":" + AppSession.SaltID)) +
            "&" + additionalQueryString, "AnacleEAM_Window");
    }

    /// <summary>
    /// Emits javascript to open the edit page in VIEW (read-only) mode.
    /// </summary>
    /// <param name="page">The Page object.</param>
    /// <param name="objectType">The type of object.</param>
    /// <param name="returnObject">The object you want to return to.</param>
    /// <param name="returnTabIndex">The tab index which you want to return to.</param>
    /// <param name="additionalQueryString">Additional query strings to be
    /// appended to the end of the URL.</param>
    public static void OpenViewObjectPageWithReturn(Page page, string objectType, string unencryptedGuid, PersistentObject returnObject, int returnTabIndex, string additionalQueryString)
    {
        if (returnObject.IsNew)
        {
            Window.WriteJavascript("alert(" + Window.JavaScriptStringLiteral(
                Resources.Errors.General_SaveObjectBeforeNavigatingToOther) + ");");
            return;
        }
        Window.PushReturnObject(returnObject.GetType().BaseType.Name, returnObject.ObjectID.ToString(), returnTabIndex);
        OpenViewObjectPage(page, objectType, unencryptedGuid, additionalQueryString);
    }

    /// <summary>
    /// Emits javascript to open the edit page in VIEW (read-only) mode in new window.
    /// </summary>
    /// <param name="page">The Page object.</param>
    /// <param name="objectType">The type of object.</param>
    /// <param name="additionalQueryString">Additional query strings to be
    /// appended to the end of the URL.</param>
    public static void OpenViewObjectPageInNewWindow(Page page, string objectType, string unencryptedGuid, string additionalQueryString)
    {
        // Mobile Web
        //
        bool mobileMode = false;
        if (page is Anacle.UIFramework.UIPageBase && ((Anacle.UIFramework.UIPageBase)page).MobileMode)
            mobileMode = true;

        OFunction function = OFunction.GetFunctionByObjectType(objectType.Split(':')[0]);
        string url = null;

        if (function != null)
            url = mobileMode ? function.EditMobileUrl : function.EditUrl;

        if (url == null || url.Trim() == "")
        {
            if (mobileMode)
                Window.WriteJavascript("alert(" + Window.JavaScriptStringLiteral(Resources.Errors.General_UnableToNavigateAsTheMobileUrlIsNotSetup) + ");");
            else
                Window.WriteJavascript("alert(" + Window.JavaScriptStringLiteral(Resources.Errors.General_UnableToNavigateAsTheDesktopUrlIsNotSetup) + ");");
            return;
        }

        RedirectOccurred = true;
        Window.Open(
            page.ResolveUrl(url) + "?N=" + HttpUtility.UrlEncode(Security.Encrypt("NewView")) + "&ID=" +
            HttpUtility.UrlEncode(Security.Encrypt("VIEW:" + unencryptedGuid + ":" + AppSession.SaltID)) +
            "&TYPE=" + HttpUtility.UrlEncode(Security.Encrypt(objectType + ":" + AppSession.SaltID)) +
            "&" + additionalQueryString, "AnacleEAM_Window_New");

    }
    /// <summary>
    /// Represents the current browser window's Opener object.
    /// This Opener object exposes only a subset of the methods
    /// of its javascript counterpart, and contains additional
    /// set of methods for populating and refreshing the opener.
    /// </summary>
    private static Opener opener = new Opener();

    /// <summary>
    /// Gets the current browser window's opener object.
    /// This Opener object exposes only a subset of the methods
    /// of its javascript counterpart, and contains additional
    /// set of methods for populating and refreshing the opener.
    /// </summary>
    public static Opener Opener
    {
        get
        {
            return opener;
        }
    }

    /// <summary>
    /// This method returns a data table to the opener page for the
    /// MultiSelectGrid.
    /// </summary>
    /// <param name="dt"></param>
    [Obsolete]
    public static void ReturnDataTable(Page page, DataTable dt)
    {
        if (page.Request["CONTROLID"] != null)
        {
            page.Session["MASSADD"] = dt;
            string controlId = Security.Decrypt(page.Request["CONTROLID"]);
            Window.WriteJavascript(
                "if( window.top.opener.document.getElementById('" + controlId + "').onchange )\n" +
                "  window.top.opener.document.getElementById('" + controlId + "').onchange();\n" +
                "window.top.close();\n");
        }
    }
}


/// <summary>
/// Represents the current browser window's Opener object.
/// This Opener object exposes only a subset of the methods
/// of its javascript counterpart, and contains additional
/// set of methods for populating and refreshing the opener.
/// </summary>
public class Opener
{
    /// <summary>
    /// Populates two HTML input elements in the opener with
    /// the specified Guid and a text name.
    /// </summary>
    /// <param name="page">The current Page object.</param>
    /// <param name="value">A GUID value to set to the control whose ID is passed in via the 'vid' query string.</param>
    /// <param name="objectName">A text to set to the control whose ID is passed in via the 'tid' query string.</param>
    public void Populate(string value)
    {
        Page page = HttpContext.Current.Handler as Page;
        if (page != null)
        {
            string vid = page.Request["vid"];

            Window.WriteJavascript(
                "window.top.opener.document.getElementById('" + vid + "').value = '" + value.Replace("\\", "\\\\").Replace("'", "\\'") + "';\n" +
                "if( window.top.opener.document.getElementById('" + vid + "').onchange )\n" +
                "  window.top.opener.document.getElementById('" + vid + "').onchange();\n" +
                "window.top.close();\n");
        }
    }


    /// <summary>
    /// Emits a javascript to click on a link button in the
    /// parent opener. 
    /// <para></para>
    /// NOTE: You must supply the ClientID of the UIButton
    /// (which is usually the same as the ID of the UIButton,
    /// if the UIButton has not been placed within Repeaters,
    /// TemplateColumns, .ascx controls or any other form 
    /// of naming containers).
    /// </summary>
    /// <param name="page"></param>
    /// <param name="buttonId"></param>
    public void ClickUIButton(string buttonId)
    {
        buttonId = buttonId + "_bt";
        Window.WriteJavascript(
            "if( window.top && " +
            "window.top.opener && " +
            "(window.top.opener.closed == false) && " +
            "window.top.opener.document && " +
            "window.top.opener.document.getElementById('" + buttonId + "') && " +
            "window.top.opener.document.getElementById('" + buttonId + "').onclick ) " +
            "window.top.opener.document.getElementById('" + buttonId + "').onclick();");
    }


    /// <summary>
    /// Emits a javascript to get the opener to open a new window.
    /// </summary>
    /// <param name="url"></param>
    /// <param name="target"></param>
    public void Open(string url, string target)
    {
        Window.WriteJavascript(
            "if(window.top && window.top.opener && window.top.opener.open && (window.top.opener.closed == false)) window.top.opener.open('" + url + "', '" + target + "');");
    }


    /// <summary>
    /// Emits a javascript to get to opener to refresh itself.
    /// </summary>
    public void Refresh()
    {
        Window.WriteJavascript(
            "if( window.top && window.top.opener && (window.top.opener.closed == false) && window.top.opener.Refresh ) window.top.opener.Refresh();");
    }


    /// <summary>
    /// Emits a javascript to get the opener and the opener's opener to
    /// refresh themselves.
    /// </summary>
    public void Refresh_ThreeLevel()
    {
        Window.WriteJavascript(
            "if( window.top && window.top.opener && (window.top.opener.closed == false) && window.top.opener.top && window.top.opener.top.opener && (window.top.opener.top.opener.closed == false) && window.top.opener.top.opener.Refresh ) window.top.opener.top.opener.Refresh();");
    }

    /// <summary>
    /// Emits a javascript to get to opener to refresh itself.
    /// </summary>
    /// <param name="id"></param>
    [Obsolete]
    public void Refresh(string id)
    {
        Window.WriteJavascript(
            "if( window.top && window.top.opener && (window.top.opener.closed == false) && window.top.opener.RefreshTreeByID ) window.top.opener.RefreshTreeByID('" + id + "');");
    }
}


/// <summary>
/// Contains a method to create a new query string that can be
/// appended to the end of a URL.
/// </summary>
public class QueryString
{
    /// <summary>
    /// Creates and returns a new name/value pair formatted 
    /// as part of a query string.
    /// </summary>
    /// <param name="key">The key of the query string item.</param>
    /// <param name="value">The value of the query string item.</param>
    /// <returns>Returns the string '{name}={value}&'</returns>
    public static string New(string key, string value)
    {
        return key + "=" + HttpUtility.UrlEncode(value) + "&";
    }
}
