//========================================================================
// $Product: Anacle Enterprise Asset Management
// $Version: 6.0
//
// Copyright 2006 (c) Anacle Systems Pte. Ltd.
// All rights reserved.
//========================================================================
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Anacle.DataFramework;
using Anacle.UIFramework;

namespace LogicLayer
{
    /// <summary>
    /// Summary description for OChecklist
    /// </summary>
    [Database("#database"), Map("Dashboard")]
    [Serializable]
    public partial class TDashboard : LogicLayerSchema<ODashboard>
    {
        public SchemaInt DashboardType;
        public SchemaText DashboardQuery;

        [Size(255)]
        public SchemaString CatagoryColumnName;
        [Size(255)]
        public SchemaString ValueColumnName;
        [Size(255)]
        public SchemaString SeriesColumnName;

        public SchemaInt Width;
        public SchemaInt Height;

        [Default(0)]
        public SchemaInt TrancateAxisLabels;

        [Default(0)]
        public SchemaInt ShowLegend;

        [Default(0)]
        public SchemaInt SeriesByColumns;

        public SchemaString TestField;

        public SchemaInt UseCSharpQuery;
        [Size(255)]
        public SchemaString CSharpMethodName;

        public SchemaString SummaryTitle;
        public SchemaInt ShowSummaryProgressBar;
        public SchemaString SummaryProgressBarTitle;
        //public SchemaString SummaryRedirectUrl;
        public SchemaString SummaryLinkedReportID;

        public TRole Roles { get { return ManyToMany<TRole>("DashboardRole", "DashboardID", "RoleID"); } }
        public TDashboardField DashboardFields { get { return OneToMany<TDashboardField>("DashboardID"); } }
        public TDashboardColumnMapping DashboardColumnMappings { get { return OneToMany<TDashboardColumnMapping>("DashboardID"); } }
        public TDashboardColumnLinking DashboardColumnLinkings { get { return OneToMany<TDashboardColumnLinking>("DashboardID"); } }

        public TDashboardReportMapping DashboardReportMappings { get { return OneToMany<TDashboardReportMapping>("DashboardID"); } }
        public TDashboardReportLinking DashboardReportLinkings { get { return OneToMany<TDashboardReportLinking>("DashboardID"); } }

        public TDashboardColor DashboardColors { get { return OneToMany<TDashboardColor>("DashboardID"); } }
    }


    [Serializable]
    public abstract partial class ODashboard : LogicLayerPersistentObject, ICloneable
    {
        public abstract String CategoryName { get; set; }
        public abstract int? DashboardType { get; set; }
        public abstract string DashboardQuery { get; set; }

        public abstract int? Width { get; set; }
        public abstract int? Height { get; set; }

        public abstract String TestField { get; set; }

        public abstract int? TrancateAxisLabels { get; set; }
        public abstract int? ShowLegend { get; set; }

        public abstract String CatagoryColumnName { get; set; }
        public abstract String ValueColumnName { get; set; }
        public abstract String SeriesColumnName { get; set; }

        public abstract int? SeriesByColumns { get; set; }

        public abstract int? UseCSharpQuery { get; set; }
        public abstract string CSharpMethodName { get; set; }

        public abstract string SummaryTitle { get; set; }
        public abstract int? ShowSummaryProgressBar { get; set; }
        public abstract string SummaryProgressBarTitle { get; set; }
        //public abstract string SummaryRedirectUrl { get; set; }
        public abstract string SummaryLinkedReportID { get; set; }

        public abstract DataList<ORole> Roles { get; }
        public abstract DataList<ODashboardField> DashboardFields { get; }
        public abstract DataList<ODashboardColumnMapping> DashboardColumnMappings { get; }
        public abstract DataList<ODashboardColumnLinking> DashboardColumnLinkings { get; }

        public abstract DataList<ODashboardReportMapping> DashboardReportMappings { get; }
        public abstract DataList<ODashboardReportLinking> DashboardReportLinkings { get; }

        public abstract DataList<ODashboardColor> DashboardColors { get; }

        /// <summary>
        /// Gets role names assigned to this report.
        /// </summary>
        public String AssignedRoleNames
        {
            get
            {
                string roleNames = "";
                foreach (ORole role in this.Roles)
                    roleNames += (roleNames == "" ? "" : ", ") + role.RoleCode;

                return roleNames;
            }
        }

        /// <summary>
        /// Gets a list of dashboards accessible by the 
        /// specified user based on his user roles.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static List<ODashboard> GetDashboardByUserRoles(OUser user)
        {
            return TablesLogic.tDashboard[
                TablesLogic.tDashboard.Roles.RoleCode.In(user.GetRoleCodes())];
        }

        /// <summary>
        /// Returns a list of fields but exclude the multi-select list box controls.
        /// </summary>
        /// <param name="DashboardID"></param>
        /// <param name="IncludeID"></param>
        /// <returns></returns>
        public static List<ODashboardField> GetAllDashboardFieldsExceptMultiSelect(Guid? DashboardID)
        {
            return TablesLogic.tDashboardField.LoadList(
                (DashboardID == null ? Query.False : TablesLogic.tDashboardField.DashboardID == DashboardID) &
                TablesLogic.tDashboardField.ControlType != 5,
                TablesLogic.tDashboardField.DisplayOrder.Asc);
        }

        /// <summary>
        /// Loads all dashboards from the database.
        /// </summary>
        /// <returns></returns>
        public static List<ODashboard> GetAllDashboards()
        {
            return TablesLogic.tDashboard[Query.True];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IncludeID"></param>
        /// <returns></returns>
        public static List<ODashboard> GetAllDashboards(Guid? IncludeID)
        {
            return TablesLogic.tDashboard.LoadList
                (TablesLogic.tDashboard.IsDeleted == 0 |
                TablesLogic.tDashboard.ObjectID == IncludeID);

        }

        /// <summary>
        /// Get all fields that are dropdowns only
        /// </summary>
        /// <returns></returns>
        public List<ODashboardField> GetDropDownDashboardFields()
        {
            List<ODashboardField> dashboardFields = new List<ODashboardField>();
            if (this.DashboardFields.Count > 0)
            {
                foreach (ODashboardField field in this.DashboardFields)
                {
                    if (field.ControlType == 2 || field.ControlType == 3 || field.ControlType == 5)
                        dashboardFields.Add(field);
                }
            }
            return dashboardFields;
        }

        /// <summary>
        /// Get all fields that are dropdowns only
        /// </summary>
        /// <returns></returns>
        public List<ODashboardField> GetDropDownDashboardFields(string excludeFieldName)
        {
            List<ODashboardField> dashboardFields = new List<ODashboardField>();
            if (this.DashboardFields.Count > 0)
            {
                foreach (ODashboardField field in this.DashboardFields)
                {
                    if ((field.ControlType == 2 || field.ControlType == 3 || field.ControlType == 5) &&
                        field.ControlIdentifier != excludeFieldName)
                        dashboardFields.Add(field);
                }
            }
            return dashboardFields;
        }

        /// <summary>
        /// Traverse into the cascading fields to search for loops.
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        protected bool TraverseCascade(Hashtable h, ODashboardField field, DataTable cascadeTable)
        {
            Hashtable used = new Hashtable();
            ODashboardField currentField = field;

            while (currentField != null)
            {
                if (used[currentField.ObjectID.Value] != null)
                    return true;  // a loop is found

                used[currentField.ObjectID.Value] = true;
                int count = 0;
                if (currentField.CascadeControl.Count > 0)
                {
                    foreach (ODashboardField f in currentField.CascadeControl)
                    {
                        if (used[f.ObjectID.Value] != null ||
                             IsLoopCascade(cascadeTable, field, f))
                            return true;  // a loop is found
                        count++;
                        used[f.ObjectID.Value] = true;
                        cascadeTable.Rows.Add(new object[] { field.ObjectID.Value.ToString(), f.ObjectID.Value.ToString() });
                        if (count == currentField.CascadeControl.Count)
                            currentField = null;
                    }
                }
                else
                    break;
            }
            return false;
        }

        /// <summary>
        /// Checks to see if any of the fields has cascade loops.
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        public bool CascadeHasLoops()
        {
            Hashtable h = new Hashtable();
            DataTable cascadeTable = new DataTable();
            cascadeTable.Columns.Add("Field");
            cascadeTable.Columns.Add("CascadeTo");
            foreach (ODashboardField field in this.DashboardFields)
                h[field.ObjectID.Value] = field;

            foreach (ODashboardField field in this.DashboardFields)
                if (TraverseCascade(h, field, cascadeTable))
                    return true;
            return false;
        }

        /// <summary>
        /// Checks if there is a loop cascade between a pair of report fields.
        /// </summary>
        /// <param name="cascadeTable"></param>
        /// <param name="field"></param>
        /// <param name="cascadeTo"></param>
        /// <returns></returns>
        public bool IsLoopCascade(DataTable cascadeTable, ODashboardField field, ODashboardField cascadeTo)
        {
            foreach (DataRow row in cascadeTable.Rows)
            {
                if (row["Field"].ToString() == cascadeTo.ObjectID.Value.ToString()
                    && row["CascadeTo"].ToString() == field.ObjectID.Value.ToString())
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Get all the dashboard fields for this dashboard in display order
        /// </summary>
        /// <returns></returns>
        public List<ODashboardField> GetDashboardFieldsInOrder()
        {
            List<ODashboardField> dashboardFields = new List<ODashboardField>();
            foreach (ODashboardField field in this.DashboardFields)
                dashboardFields.Add(field);
            dashboardFields.Sort("DisplayOrder", true);
            return dashboardFields;
        }

        /// <summary>
        /// Validates to ensure that no two fields have the same identifiers.
        /// </summary>
        /// <returns></returns>
        public bool ValidateNoDuplicateIdentifiers(ODashboardField field)
        {
            foreach (ODashboardField dashboardField in this.DashboardFields)
            {
                if (dashboardField.ObjectID != field.ObjectID &&
                    dashboardField.ControlIdentifier.ToLower() == field.ControlIdentifier.ToLower())
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Returns a clone copy of this object.
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            ODashboard dashboard = TablesLogic.tDashboard.Create();

            dashboard.ShallowCopy(this);

            foreach (ODashboardField o in this.DashboardFields)
            {
                ODashboardField n = TablesLogic.tDashboardField.Create();
                n.ShallowCopy(o);
                dashboard.DashboardFields.Add(n);
            }

            foreach (ORole o in this.Roles)
                dashboard.Roles.Add(o);


            for (int i = 0; i < this.DashboardFields.Count; i++)
            {
                foreach (ODashboardField o in this.DashboardFields[i].CascadeControl)
                {
                    // Set up the cascaded controls
                    // 2016.12.01,
                    // Kien Trung
                    // BUG: this cloning cascade controls
                    // caused the report field cascade error.
                    //ODashboardField n = this.DashboardFields.Find(p => p.DisplayOrder == o.DisplayOrder);
                    ODashboardField n = dashboard.DashboardFields.Find(
                        p => p.DisplayOrder == o.DisplayOrder &&
                        p.ControlIdentifier == o.ControlIdentifier);

                    if (n != null)
                        dashboard.DashboardFields[i].CascadeControl.Add(n);
                }
            }

            return dashboard;
        }

        public string DashboardTypeString
        {
            get
            {
                return ((JSChartType)this.DashboardType).ToString();
            }
        }

        public ColorSet[] GetColorScheme()
        {
            List<ColorSet> colorScheme = new List<ColorSet>();
            this.DashboardColors.Sort("Order", true);
            foreach(ODashboardColor c in this.DashboardColors)
                colorScheme.Add(new ColorSet(c.Stroke, c.Highlight, c.Fill));

            return colorScheme.ToArray();
        }
    }
}

