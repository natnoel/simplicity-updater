﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Text;
using Anacle.DataFramework;
using Anacle.UIFramework;
using LogicLayer;

/// <summary>
/// Summary description for Helpers
/// </summary>
public class Helpers
{
    public Helpers()
    {
    }

    public static void ShowMessages(Control c, Hashtable OutputObject)
    {
        foreach (Control child in c.Controls)
        {
            string key = null;

            if (child is UIFieldBase)
            {
                key = "ERR:" + child.ID.ToString();
                if(OutputObject.ContainsKey(key))
                {
                   ((UIFieldBase)child).ErrorMessage = OutputObject[key].ToString();
                }

                key = "ERRS:" + child.ID.ToString();
                if(OutputObject.ContainsKey(key))
                {
                    if (child.GetType() == typeof(Anacle.UIFramework.UIFieldTextBox))
                    {
                        ((Anacle.UIFramework.UIFieldTextBox)child).Text = 
                            OutputObject[key].ToString();
                    }
                    else
                    {
                        ((UIFieldBase)child).ErrorMessage = 
                            OutputObject[key].ToString();
                    }
                }

                key = "WARN:" + child.ID.ToString();
                if (OutputObject.ContainsKey(key))
                {
                   //TODO: Add code to display warning on relevant control
                }
            }
            ShowMessages(child, OutputObject);
        }
    }

    /// <summary>
    /// Select the default value for the searchable drop down list
    /// ONLY when the current selected value is null or blank
    /// </summary>
    /// <param name="ddl">1. drop down list</param>
    /// <param name="ddl">2. default value</param>
    public static void SetDefaultValueForDropDownList(UIFieldSearchableDropDownList ddl, string defaultValue)
    {
        if (ddl.Visible && ddl.IsContainerEnabled() && ddl.Enabled && ddl.Items.Count > 0
            && (ddl.SelectedValue == null || ddl.SelectedValue == ""))
            ddl.SelectedValue = defaultValue;
    }

    /// <summary>
    /// Select the ONLY non-blank item as the default item for the searchable drop down list
    /// ASSUMPTION: assuming there is at most only one blank item, and it can only be the first item
    /// </summary>
    /// <param name="ddl">1. drop down list</param>
    public static void SetDefaultValueForDropDownList(UIFieldSearchableDropDownList ddl)
    {
        if (ddl.Visible && ddl.IsContainerEnabled() && ddl.Enabled && ddl.Items.Count > 0
            && (ddl.SelectedValue == null || ddl.SelectedValue == ""))
        {
            if(ddl.Items.Count == 1 && ddl.Items[0].Value != null && ddl.Items[0].Value.ToString() != "")
                ddl.SelectedIndex = 0;
            else if (ddl.Items.Count == 2 && (ddl.Items[0].Value == null || ddl.Items[0].Value.ToString() == ""))
                ddl.SelectedIndex = 1;
        }
    }

    /// <summary>
    /// Select the default value for the drop down list
    /// ONLY when the current selected value is null or blank
    /// </summary>
    /// <param name="ddl">1. drop down list</param>
    /// <param name="ddl">2. default value</param>
    public static void SetDefaultValueForDropDownList(UIFieldDropDownList ddl, string defaultValue)
    {
        if (ddl.Visible && ddl.IsContainerEnabled() && ddl.Enabled && ddl.Items.Count > 0
            && (ddl.SelectedValue == null || ddl.SelectedValue == ""))
            ddl.SelectedValue = defaultValue;
    }

    /// <summary>
    /// Select the ONLY non-blank item as the default item for the drop down list
    /// ASSUMPTION: assuming there is at most only one blank item, and it can only be the first item
    /// </summary>
    /// <param name="ddl">1. drop down list</param>
    public static void SetDefaultValueForDropDownList(UIFieldDropDownList ddl)
    {
        if (ddl.Visible && ddl.IsContainerEnabled() && ddl.Enabled && ddl.Items.Count > 0
            && (ddl.SelectedValue == null || ddl.SelectedValue == ""))
        {
            if (ddl.Items.Count == 1 && ddl.Items[0].Value != null && ddl.Items[0].Value.ToString() != "")
                ddl.SelectedIndex = 0;
            else if (ddl.Items.Count == 2 && (ddl.Items[0].Value == null || ddl.Items[0].Value.ToString() == ""))
                ddl.SelectedIndex = 1;
        }
    }


    /// <summary>
    /// Returns a flag to indicate if any of the strings is contained
    /// within the original string.
    /// </summary>
    /// <param name="strings"></param>
    /// <returns></returns>
    private static bool ContainsAny(string originalString, params string[] strings)
    {
        if (strings != null)
            foreach (string s in strings)
                if (originalString.Contains(s))
                    return true;
        return false;
    }


    /// <summary>
    /// Gets a colour that represents a workflow state.
    /// </summary>
    /// <param name="?"></param>
    /// <returns></returns>
    public static System.Drawing.Color GetWorkflowStateColor(string currentStateName)
    {
        System.Drawing.Color workflowColor = System.Drawing.Color.Gray;
        
        // Initial states
        if (ContainsAny(currentStateName, "Start", "Draft"))
            workflowColor = System.Drawing.Color.Gray;

        // Pending states
        else if (ContainsAny(currentStateName, "Pending"))
            workflowColor = System.Drawing.Color.FromArgb(0x0f, 0x35, 0x7b);

        // Complete states
        else if (ContainsAny(currentStateName, 
            "Approved", "Confirmed", "Acknowledged", "Awarded", "Close", 
            "Complete", "Commit", "Submit", "Served"))
            workflowColor = System.Drawing.Color.FromArgb(0x19, 0xa2, 0x0b);

        // Query state
        else if (ContainsAny(currentStateName, "Query"))
            workflowColor = System.Drawing.Color.Orange;

        // Rejection states
        else if (ContainsAny(currentStateName, "Rejected", "Cancelled", "Voided"))
            workflowColor = System.Drawing.Color.FromArgb(0xcb, 0x18, 0x18);

        return workflowColor;
    }


    /// <summary>
    /// Returns a flag indicating if the web compilation debug
    /// mode is set to true.
    /// </summary>
    /// <returns></returns>
    public static bool IsDebug()
    {
        System.Web.Configuration.CompilationSection compilationSection =
            (System.Web.Configuration.CompilationSection)System.Configuration.ConfigurationManager.GetSection(@"system.web/compilation");

        return compilationSection.Debug;
    }


    public static void LogException(Exception ex, System.Web.HttpRequest Request)
    {
        try
        {
            string err = "";
            err = err + DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss") + "\r\n";
            while (ex != null)
            {
                err = err + "Error in     :" + Request.Url.ToString() + "\r\n";
                err = err + "Error Message:" + ex.Message.ToString() + "\r\n";
                err = err + "Stack Trace  :" + "\r\n" + ex.StackTrace.ToString() + "\r\n";
                if (ex is ObjectModifiedException)
                {
                    ObjectModifiedException oex = ex as ObjectModifiedException;
                    err = err + "Object ID    : " + oex.ObjectID + "\r\n";
                    err = err + "Object Name  : " + oex.ObjectName + "\r\n";
                    err = err + "Object Number: " + oex.ObjectNumber + "\r\n";
                    err = err + "Object Type  : " + oex.ObjectType + "\r\n";
                }
                ex = ex.InnerException;
            }

            using (System.IO.StreamWriter sw = new System.IO.StreamWriter(
                String.Format(System.Configuration.ConfigurationManager.AppSettings["ErrorLogPath"], DateTime.Now), true))
            {
                sw.WriteLine(err);
                sw.Close();
            }
        }
        catch
        {
        }
    }

    public static string ConvertExceptionToString(Exception ex)
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            Exception currentException = ex;
            while (currentException != null)
            {
                sb.Append(currentException.Message + "\n" + currentException.StackTrace + "\n\n");
                currentException = currentException.InnerException;
            }

            return sb.ToString();
        }
        catch
        {
        }

        return string.Empty;
    }

    public static void LogException(string message)
    {
        try
        {
            using (System.IO.StreamWriter sw = new System.IO.StreamWriter(
                String.Format(System.Configuration.ConfigurationManager.AppSettings["ErrorLogPath"], DateTime.Now), true))
            {
                sw.WriteLine(message);
                sw.Close();
            }
        }
        catch
        {
        }
    }



    public static void SetButtonVisibility(UIButton button, bool visible)
    {
        if (button != null)
            button.Visible = visible;
    }


    /// <summary>
    /// Populates statuses into a listbox.
    /// </summary>
    /// <param name="listStatus"></param>
    /// <param name="objectTypeName"></param>
    public static void PopulateStatuses(UIFieldListBox listStatus, string objectTypeName)
    {
        listStatus.Bind(OActivity.GetStatuses(objectTypeName), "ObjectName", "ObjectName");
        foreach (ListItem item in listStatus.Items)
        {
            string translated = Resources.WorkflowStates.ResourceManager.GetString(item.Text);
            if (translated != null && translated != "")
                item.Text = translated;
        }
    }

    /// <summary>
    /// Set a given tab as default when the object is in the given state list
    /// </summary>
    /// <param name="tabStrip"></param>
    /// <param name="o"></param>
    /// <param name="ifTrueTabView"></param>
    /// <param name="elseTabView"></param>
    /// <param name="workflowStates"></param>
    public static void SetDefaultTabForWorkflowStates(
        UITabStrip tabStrip,
        LogicLayerWorkflowPersistentObject o,
        UITabView defaultTab, 
        params string[] workflowStates)
    {
        if(!string.IsNullOrEmpty(o.CurrentActivityName) && o.CurrentActivityName.Is(workflowStates))
            tabStrip.SelectedTab = defaultTab;       
    }
}
