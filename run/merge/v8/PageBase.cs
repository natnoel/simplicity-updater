//========================================================================
// $Product: Anacle Enterprise Asset Management
// $Version: 5.0
//
// Copyright 2006 (c) Anacle Systems Pte. Ltd.
// All rights reserved.
//========================================================================
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Anacle.UIFramework;
using Anacle.DataFramework;
using LogicLayer;

/// <summary>
/// Summary description for PageBase
/// </summary>
public class PageBase : Anacle.UIFramework.UIPageBase
{

    protected TextBox ViewStateKey;

    public PageBase()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    /// ------------------------------------------------------------------
    /// <summary>
    /// Initialize the page culture.
    /// </summary>
    /// ------------------------------------------------------------------
    protected override void InitializeCulture()
    {
        CultureInfo ci = null;

        // Creates the new culture info object based on the currently
        // logged on user's language.
        //
        if (AppSession.User != null &&
            AppSession.User.LanguageName != null &&
            AppSession.User.LanguageName.Trim() != "")
            ci = new CultureInfo(AppSession.User.LanguageName);
        else
            ci = new CultureInfo("");
        if (ci == null)
            throw new Exception("invalid ci");
        // Initialize currency symbols
        //
        OCurrency currency = OApplicationSetting.Current.BaseCurrency;
        if (currency != null)
            ci.NumberFormat.CurrencySymbol = currency.CurrencySymbol;

        // Initialize the page culture.
        //
        this.Culture = ci.Name;
        this.UICulture = ci.Name;

        // Then sets the culture across all libraries.
        //
        Resources.Errors.Culture = null;
        Resources.Objects.Culture = null;
        Resources.Messages.Culture = null;
        Resources.Roles.Culture = null;
        Resources.Strings.Culture = null;
        Resources.Dashboards.Culture = null;
        Resources.Reports.Culture = null;
        Resources.ReportHeaders.Culture = null;
        Resources.WorkflowStates.Culture = null;
        Resources.WorkflowEvents.Culture = null;

        LogicLayer.Global.SetCulture(null);
        Anacle.UIFramework.Global.SetCulture(null);

        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;

        base.InitializeCulture();
    }


    /// ------------------------------------------------------------------
    /// <summary>
    /// Set the theme.
    /// </summary>
    /// <param name="e"></param>
    /// ------------------------------------------------------------------
    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);
        this.Theme = "Corporate";

        // 2014.10.13
        // Kim Foong
        // Apparently there seems to be a different behavior in different versions of .NET
        // where the thread static variables (Audit.UserName, Workflow.CurrentUser) updated
        // in the AppGlobal's Application_AcquireRequestState runs in a different from the 
        // actual page processing. Thus, we need to set the above thread static variables
        // here in the OnPreInit event of PageBase.
        // 
        Audit.UserName = null;
        Workflow.CurrentUser = null;
        if (AppSession.User != null)
        {
            Audit.UserName = AppSession.User.ObjectName;
            Workflow.CurrentUser = AppSession.User;
        }

        //KL: clear thread level cache before each request
        MemoryCache.CurrentThreadCache.Clear();

        // Mobile Web
        //
        if (Session["MobileMode"] != null)
            this.MobileMode = (bool)Session["MobileMode"];

    }


    /// ------------------------------------------------------------------
    /// <summary>
    /// Initialize the page.
    /// </summary>
    /// <param name="e"></param>
    /// ------------------------------------------------------------------
    protected override void OnInit(EventArgs e)
    {
        // insert a hidden field for tracking session view state
        //
        if (this.Form != null)
        {
            ViewStateKey = new TextBox();
            ViewStateKey.ID = "__ViewStateKey__";
            ViewStateKey.Style["display"] = "none";

            if (System.Configuration.ConfigurationManager.AppSettings["LoadTesting"] == "true")
            {
                if (!IsPostBack)
                {
                    string strHashedPath = Security.HashString(Page.Request.RawUrl);
                    //string strHashedPath = Page.Request.RawUrl;
                    ViewStateKey.Text = Security.Encrypt(strHashedPath);
                    DiskCache.Remove(strHashedPath);

                }
            }
            else
            {
                if (!IsPostBack)
                    ViewStateKey.Text = Security.Encrypt(Guid.NewGuid().ToString() + Convert.ToString(DateTime.Now.Ticks, 0x10));
            }
            System.Globalization.CultureInfo ci = System.Threading.Thread.CurrentThread.CurrentUICulture;
            if (ci.TextInfo.IsRightToLeft)
                this.Page.Form.Style[HtmlTextWriterStyle.Direction] = "rtl";

            this.Form.Controls.AddAt(0, ViewStateKey);

            // 2013.04.11 
            // Kim Foong
            // The following helps to solve a bug with Microsoft AJAX with Chrome and Safari browsers.
            // (where the AJAX Please Wait loader does not go away.)
            //
            if (this is UIPageBase)
            {
                ((UIPageBase)this).ScriptManager.Scripts.Add(new ScriptReference("~/scripts/webkit.js"));
            }
        }

        base.OnInit(e);

        Response.Cache.SetCacheability(HttpCacheability.NoCache);
    }


    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        //this.ClientTarget = "uplevel";
        if (!IsPostBack)
        {
            InitializeCurrencyCaptions(Page);

            //KL: css and javascript to prevent clickjacking
            this.Page.Header.Controls.Add(
                new LiteralControl(
                @"<style id='antiClickjack' type='text/css'>
                        body
                        {
                            display:none !important;
                        }
                </style>
                <script type='text/javascript'>
                    var topOrigin = window.top.location.protocol + '//' + window.top.location.hostname + ':' + window.top.location.port;
                    var selfOrigin = window.self.location.protocol + '//' + window.self.location.hostname + ':' + window.self.location.port;

                    if (selfOrigin === topOrigin) {
                        var antiClickjack = document.getElementById('antiClickjack');
                        antiClickjack.parentNode.removeChild(antiClickjack);
                    } else {
                        window.top.location = 'about:blank';
                    }
                </script>"
                ));
            this.Page.Form.Controls.Add(
            new LiteralControl(@"
                <script type='text/javascript'>
                    $(document).ready(function () {
                        $('#block').fadeOut(500);
                    });
                </script>
                <div id='block' style='top:0; left:0; width:100%; height:100%; background-color:white; position:fixed; z-index: 100000;'></div>"
            ));
        }
        this.Page.Title = System.Configuration.ConfigurationManager.AppSettings["PageTitle"];
        //CheckNonEnglishCharacters(Page);

        if (OApplicationSetting.Current.TraceUserAction == 1)
            TraceUserAction();
    }


    /// <summary>
    /// Tracks user actions and saves it to the database.
    /// </summary>
    void TraceUserAction()
    {//120208(LiShan)
        object target = Request.Form["__EVENTTARGET"];
        object argument = Request.Form["__EVENTARGUMENT"];

        Guid? oid = null;
        string mode = "";
        if (Request["ID"] != null)
        {
            string arg = Security.Decrypt(Request["ID"]);
            if (arg != null)
            {
                string[] args = arg.Split(':');
                mode = args[0];
                if (args.Length > 1 && args[1] != null && args[1] != "")
                    //if (!mode.StartsWith("NEW"))
                    try
                    {
                        oid = new Guid(args[1]);
                    }
                    catch (Exception) { }
                if (Request.Path.EndsWith("reportviewer/search.aspx"))
                {
                    try
                    {
                        oid = new Guid(args[0]);
                    }
                    catch (Exception) { }
                    mode = "";
                }
            }
        }
        string type = "";
        if (Request["TYPE"] != null)
        {
            type = GetRequestObjectType();
            if (type == null)
                type = "";
        }

        //120208(kuang)
        //if (target == null && argument == null) return;
        using (Connection c = new Connection())
        {
            OUserActionTracing tracing = TablesLogic.tUserActionTracing.Create();
            if (AppSession.User != null)
                tracing.User = AppSession.User.ObjectName;
            tracing.Mode = mode;
            tracing.OpenedObjectID = oid;
            tracing.OpenedObjectTypeName = type;
            tracing.Time = DateTime.Now;
            tracing.Url = Request.Path;
            tracing.IpAddress = Request.UserHostAddress;
            if (!IsPostBack)
            {
                tracing.Action = "ID:Page,Load,";
            }
            else
            {
                tracing.Action = "";
                if (target != null && this.FindControl(target.ToString()) != null)
                {
                    if (typeof(Button) == this.FindControl(target.ToString()).GetType())
                    {
                        string text = ((Button)this.FindControl(target.ToString())).Text;
                        string id = this.FindControl(target.ToString()).ID;
                        tracing.Action = string.Format("ID:{0},{1},{2}", id, text, argument != null ? argument.ToString() : "");
                    }
                    else if (typeof(LinkButton) == this.FindControl(target.ToString()).GetType())
                    {
                        string text = ((LinkButton)this.FindControl(target.ToString())).Text;
                        string id = this.FindControl(target.ToString()).ID;
                        tracing.Action = string.Format("ID:{0},{1},{2}", id, text, argument != null ? argument.ToString() : "");
                    }
                    else if (typeof(GridView) == this.FindControl(target.ToString()).GetType())
                    {
                        string text = this.FindControl(target.ToString()).ToString();
                        string id = this.FindControl(target.ToString()).ID;
                        tracing.Action = string.Format("ID:{0},{1},{2}", id, text, argument != null ? argument.ToString() : "");
                    }
                    else if (typeof(UIGridView) == this.FindControl(target.ToString()).GetType())
                    {
                        string text = this.FindControl(target.ToString()).ToString();
                        string id = this.FindControl(target.ToString()).ID;
                        tracing.Action = string.Format("ID:{0},{1},{2}", id, text, argument != null ? argument.ToString() : "");
                    }
                }
                if (tracing.Action == "")
                    return;
            }
            tracing.Save();
            c.Commit();
        }
    }


    /// <summary>
    /// Gets the current object type.
    /// </summary>
    /// <returns></returns>
    public string GetRequestObjectType()
    {
        if (Request["TYPE"] == null)
            return "";
        return Security.Decrypt(Request["TYPE"]).Split(':')[0];
    }


    /// <summary>
    /// The system automatically replaces all control's caption and headertext containing the "($)" symbol
    /// with the system's base currency symbol.
    /// </summary>
    protected void InitializeCurrencyCaptions(Control c)
    {
        if (OApplicationSetting.Current.BaseCurrency != null)
        {
            if (c is UIFieldBase)
            {
                if (((UIFieldBase)c).Caption.Contains("($)"))
                    ((UIFieldBase)c).Caption = ((UIFieldBase)c).Caption.Replace("($)", "(" + OApplicationSetting.Current.BaseCurrency.CurrencySymbol + ")");
            }
            if (c is GridView)
            {
                foreach (DataControlField f in ((GridView)c).Columns)
                    f.HeaderText = f.HeaderText.Replace("($)", "(" + OApplicationSetting.Current.BaseCurrency.CurrencySymbol + ")");
            }
            foreach (Control child in c.Controls)
                InitializeCurrencyCaptions(child);

        }
    }


    /// ------------------------------------------------------------------
    /// <summary>
    /// Enumerate the themes available in the system.
    /// </summary>
    /// <returns></returns>
    /// ------------------------------------------------------------------
    protected List<ThemeName> GetThemes()
    {
        string[] themes = Directory.GetDirectories(Page.MapPath("~/App_Themes"));
        List<ThemeName> themeList = new List<ThemeName>();

        foreach (string theme in themes)
        {
            string t = Path.GetFileNameWithoutExtension(theme);
            themeList.Add(new ThemeName(t, t));
        }

        return themeList;
    }


    /// ------------------------------------------------------------------
    /// <summary>
    /// Override the default page state persister to use the Session state
    /// for persistence.
    /// </summary>
    /// <returns></returns>
    /// ------------------------------------------------------------------
    protected override object LoadPageStateFromPersistenceMedium()
    {
        string viewStateKey = Security.Decrypt(Request.Form["__ViewStateKey__"]);
        LosFormatter los = new LosFormatter();
        string value = DiskCache.GetValue(viewStateKey);
        object o = los.Deserialize(value);
        return o;

    }


    /// ------------------------------------------------------------------
    /// <summary>
    /// Override the default page state persister to use the Session state
    /// for persistence.
    /// </summary>
    /// <param name="state"></param>
    /// ------------------------------------------------------------------
    protected override void SavePageStateToPersistenceMedium(object state)
    {
        string viewStateKey = Security.Decrypt(ViewStateKey.Text);
        LosFormatter los = new LosFormatter();
        StringWriter sw = new StringWriter();
        los.Serialize(sw, state);
        DiskCache.Add(viewStateKey, sw.ToString());
    }


    /// <summary>
    /// Modifies the output to show the entire form, if and only if
    /// it has been completely downloaded.
    /// </summary>
    /// <param name="writer"></param>
    protected override void Render(HtmlTextWriter writer)
    {
        if (!Page.Request.RawUrl.Contains("report.aspx"))
        {
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            base.Render(hw);

            string s =
                hw.InnerWriter.ToString()
                .Replace("<html", "<html style='display:none' ")
                .Replace("<form", "<form style='display:none' ")
                .Replace("</form>", "</form><script type='text/javascript'>" + 
                "document.forms[0].style.display = ''; if(self.location.toString().toLowerCase().replace('http://','').replace('https://','').split(/[/?#]/)[0] == top.location.toString().toLowerCase().replace('http://','').replace('https://','').split(/[/?#]/)[0] ) {document.documentElement.style.display = 'block'; } " + 
                // 2016.11.29, Kien Trung
                // FIXED: Ignore "enter" key by default
                // Each control keypress still remain functional as it is
                //
                "$(document).on('keypress', function(e){ if (e.target && e.target.nodeName.toUpperCase() != 'TEXTAREA' && (e.keyCode == 13 || e.which == 13)) e.preventDefault(); });" +
                "</script>");

            writer.Write(s);
        }
        else
        {
            base.Render(writer);
        }

    }

    /// <summary>
    /// Find every text box within the page and check to see if it contains any non English characters
    /// </summary>
    protected void CheckNonEnglishCharacters(Control c)
    {
        if (c is UIFieldTextBox)
        {
            UIFieldTextBox tb = c as UIFieldTextBox;
            if (tb.ValidateRegexField == false)
            {
                tb.ValidateRegexField = true;
                tb.ValidationRegexString = "^[-a-zA-Z0-9~!@#$%^&*()_=?:;><+.,`{}|'/\"\\]\\[\\\\\t\n\x0B\f\r ]*$";
                tb.ValidationRegexErrorMessage = Resources.Errors.PageBase_NonEnglishCharacterExistsInTextBox;
            }
        }
        foreach (Control child in c.Controls)
            CheckNonEnglishCharacters(child);
    }


    // 2012.11.18
    // Kim Foong
    // Object Linking Feature
    private Control GetFirstTabStrip(Control c)
    {
        if (c is UITabStrip)
            return c;

        foreach (Control childControl in c.Controls)
        {
            Control tabStrip = GetFirstTabStrip(childControl);
            if (tabStrip != null)
                return tabStrip;
        }
        return null;
    }


    // 2012.11.18
    // Kim Foong
    // Object Linking Feature
    public override void OnLinkClicked(LinkClickedEventArgument e)
    {
        base.OnLinkClicked(e);

        PersistentObject o = null;

        // Gets the current object session key.
        //
        if (Request["ID"] != null)
        {
            string arg = Security.Decrypt(Request["ID"]);
            string[] args = arg.Split(':');
            string sessionKey = "";
            if (Request["N"] != null)
                sessionKey = "::SessionObject::" + "_" + Request["N"];
            else
                sessionKey = "::SessionObject::";

            // Gets the currently editing persistent object.
            //
            o = this.Session[sessionKey] as PersistentObject;
        }

        // Finds the first tabstrip, and gets the current tab index
        //
        UITabStrip firstTabStrip = GetFirstTabStrip(this) as UITabStrip;

        if (o == null)
            Window.OpenViewObjectPage(this, e.ObjectTypeName, e.ObjectID.ToString(), "");
        else
            Window.OpenViewObjectPageWithReturn(this, e.ObjectTypeName, e.ObjectID.ToString(), o,
                firstTabStrip != null ? firstTabStrip.SelectedIndex : 0, null);
    }


    protected int GetFirstTabStripCurrentTabIndex()
    {
        // Finds the first tabstrip, and gets the current tab index
        //
        UITabStrip firstTabStrip = GetFirstTabStrip(this) as UITabStrip;

        if (firstTabStrip != null)
            return firstTabStrip.SelectedIndex;
        else
            return 0;
    }

}



public class ThemeName
{
    public string Name;
    public string Value;

    public ThemeName(string name, string value)
    {
        Name = name;
        Value = value;
    }
}
