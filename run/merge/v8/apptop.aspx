<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%@ Page Language="C#" Theme="Corporate" Inherits="PageBase" UICulture="Auto" %>

<script runat="server">
    
    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);

        // Forces the menu to render correctly in Chrome/Safari
        // browsers.
        //
        this.Page.ClientTarget = "uplevel";
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (!IsPostBack)
        {
            if (Request.QueryString["url"] == null)
            {
                if (OApplicationSetting.Current.HomePageUrl != null && OApplicationSetting.Current.HomePageUrl != "")
                {
                    frameBottom.Attributes["src"] = ResolveUrl(OApplicationSetting.Current.HomePageUrl);
                }
            }
            else
            {
                frameBottom.Attributes["src"] = Request.QueryString["url"];
            }
        }
    }
    
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="css/v7-quicksearch.css" />
</head>
<body style="overflow: auto;" class="menu-apptop" onunload="window_onunload()">
    <script type='text/javascript'>
        if (window.top) {
            if (window.top.name.indexOf('AnacleEAM') == 0) {
                if (window.top.opener) {
                    try {
                        window.top.opener.location = 'apptop.aspx';
                        window.top.opener.focus();
                    }
                    catch (e) {
                    }
                }
                window.top.close();
            }

            if (window.top.location != window.location)
                window.top.location = window.location;
        }
    </script>
    <form id="form1" runat="server" style="border-top: 0px; border-bottom: 0px">
        <div id="divmenu">
            <web:menu runat='server' ID="menu" />
        </div>
        <div id='frameBottomDiv'>
            <iframe src="home.aspx" name="frameBottom" id="frameBottom" runat="server" style="display: block;
                background-color: #fff; border: 0; width: 100%; min-width: 800px;"></iframe>
        </div>
    </form>

    <%-- IM should not be inside a form --%>
    <%-- 
        2014.10.22
        KL
        The IM component is still not fully functional, temporary removing it.
    
    <div id="divim">
        <web:im runat='server' ID="im" />
    </div>
    --%>

    <%-- requires jquery which is injected to head by pagebase,
         so we cannot put this in head --%>
    <script type="text/javascript" src="scripts/quicksearch.js"></script>
</body>

<script type="text/javascript">

    function getIFrameDocument(id) {
        var rv = null;
        var frame = document.getElementById(id);
        if (frame.contentDocument)
            rv = frame.contentDocument;
        else
            rv = document.frames[id].document;
        return rv;
    }

    function resizeFrame() {
        var theWidth, theHeight;

        if (window.innerHeight) {
            theHeight = window.innerHeight;
        }
        else if (document.documentElement && document.documentElement.clientHeight) {
            theHeight = document.documentElement.clientHeight;
        }
        else if (document.body) {
            theHeight = document.body.clientHeight;
        }

        var f = document.getElementById("frameBottom");
        var d = document.getElementById("divmenu");

        f.height = theHeight - d.offsetHeight;
    }

    window.onresize = function (event) {
        resizeFrame();
    };

    $(document).ready(function () {
        resizeFrame();
    });

    function window_onunload() {

    }

</script>

</html>
