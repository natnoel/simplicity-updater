//========================================================================
// $Product: Anacle Enterprise Asset Management
// $Version: 7.1
//
// Copyright 2006 (c) Anacle Systems Pte. Ltd.
// All rights reserved.
//========================================================================
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Text;
using Anacle.DataFramework;
using NPOI.SS.Formula.Functions;

namespace LogicLayer
{
    public partial class TApplicationSetting : LogicLayerSchema<OApplicationSetting>
    {
        // Login
        //
        [Size(255)]
        public SchemaString LoginTitle;
        public SchemaGuid LoginLogoID;
        [Size(255)]
        public SchemaString HomePageUrl;

        // Admin Center
        //
        public SchemaInt PasswordMinimumLength;
        [Default(0)]
        public SchemaInt PasswordRequiredCharacters;
        public SchemaInt PasswordMaximumTries;
        public SchemaInt PasswordDaysToExpiry;
        public SchemaInt PasswordMinimumAge;
        public SchemaInt PasswordHistoryKept;
        [Default(90)]
        public SchemaInt NumberOfDaysToKeepMessageHistory;
        [Default(90)]
        public SchemaInt NumberOfDaysToKeepLoginHistory;
        [Default(90)]
        public SchemaInt NumberOfDaysToKeepBackgroundServiceLog;
        [Size(255)]
        public SchemaString SystemUrl;
        public SchemaInt IsUserEmailCompulsory;
        public SchemaInt TraceUserAction;
        public SchemaBit LogReportGeneration;


        // Asset Center
        //

        ////Integration Tab
        //[Size(255)]
        //public SchemaString BuildingFilePath;
        //[Size(255)]
        //public SchemaString VacantListFilePath;
        //[Size(255)]
        //public SchemaString OccupiedListFilePath;
        //[Size(255)]
        //public SchemaString BackupPath;
        //[Size(255)]
        //public SchemaString AdministratorEmail;
        //[Size(255)]
        //public SchemaString ErrorReason;

        // Inventory Center
        //
        public SchemaGuid EquipmentUnitOfMeasureID;
        public SchemaInt InventoryDefaultCostingType;

        // Procurement Center
        //
        public SchemaGuid BaseCurrencyID;
        public SchemaInt DefaultBudgetSpendingPolicy;
        public SchemaInt DefaultBudgetDeductionPolicy;
        public SchemaInt DefaultRequiredCount;
        public SchemaInt DefaultRequiredUnit;
        public SchemaInt DefaultEndCount;
        public SchemaInt DefaultEndUnit;
        public SchemaInt AllowPerLineItemTaxInPO;

        // Work Center
        //
        public SchemaInt DefaultNumberOfDaysInAdvanceToCreateFixedWorks;
        public SchemaGuid DefaultTypeOfWorkID;
        public SchemaGuid DefaultScheduledWorkTypeOfWorkID;
        public SchemaString CaseEventAfterDocumentClosedOrCancelled;

        // Message
        //
        [Default(1)]
        public SchemaInt EnableEmail;
        [Default(1)]
        public SchemaInt EnableSms;
        [Default(0)]
        public SchemaInt SMSSendType;
        [Size(255)]
        public SchemaString SMSRelayWSURL;
        [Default(3)]
        public SchemaInt MessageNumberOfTries;

        public SchemaString MessageSmtpServer;
        [Default(25)]
        public SchemaInt MessageSmtpPort;
        [Default(0)]
        public SchemaBit EnableSSL;
        public SchemaInt MessageSmtpRequiresAuthentication;
        public SchemaString MessageSmtpServerUserName;
        [Size(255)]
        public SchemaString MessageSmtpServerPassword;

        public SchemaInt EnableReceiveEmail;
        public SchemaInt EmailServerType;
        [Size(255)]
        public SchemaString EmailServer;
        public SchemaString EmailUserName;
        [Size(255)]
        public SchemaString EmailPassword;
        public SchemaInt EmailPort;
        [Size(255)]
        public SchemaString EmailExchangeWebServiceUrl;
        public SchemaString EmailDomain;

        [Size(10)]
        public SchemaString MessageSmsComPort;
        [Default(9600)]
        public SchemaInt MessageSmsBaudRate;
        [Default("None"), Size(20)]
        public SchemaString MessageSmsParity;
        public SchemaInt MessageSmsDataBits;
        [Default("One"), Size(20)]
        public SchemaString MessageSmsStopBits;
        [Default("None"), Size(30)]
        public SchemaString MessageSmsHandshake;

        [Default(8)]
        public SchemaInt MessageSmsLocalNumberDigits;
        [Default("ATZ;ATE0;AT+CMGF=1")]
        public SchemaString MessageSmsInitCommands;
        [Default("AT+CMGS={0}")]
        public SchemaString MessageSmsSendCommands;
        [Default("AT+CMGR={0}")]
        public SchemaString MessageSmsReceiveCommands;
        [Default("AT+CMGD={0}")]
        public SchemaString MessageSmsDeleteCommands;
        [Default("AT+CSMP=17,167,0,0")]
        public SchemaString MessageSmsInitASCIICommand;
        [Default("AT+CSMP=17,167,0,8")]
        public SchemaString MessageSmsInitUCS2Command;
        [Default("\\r"), Size(5)]
        public SchemaString MessageSmsNewLine;
        [Default("c:\\log\\smslog-{0:yyyyMMdd}.txt")]
        public SchemaString MessageSmsLogFilePath;
        [Default("sender_eam@anacle.com"), Size(50)]
        public SchemaString MessageEmailSender;
        public SchemaString MessageEmailCC;

        //For POS Interface
        public SchemaString PosToPrmsPath;
        public SchemaString PrmsToPosPath;

        //Active directory
        public SchemaInt IsUsingActiveDirectory;
        [Size(255)]
        public SchemaString ActiveDirectoryDomain;
        [Size(255)]
        public SchemaString ActiveDirectoryPath;

        // Impersonate a windows account for reporting.
        //
        public SchemaInt IsImpersonateAccountForReport;
        public SchemaString ReportDomain;
        public SchemaString ReportUserName;
        [Size(255)]
        public SchemaString ReportPassword;

        public SchemaString ReportSystemName;


        //Service Center: Performance Survey
        //
        [Default("http://???/???/modules/surveyplanner/surveyformload.aspx"), Size(255)]
        public SchemaString SurveyURL;

        //Background Service
        //
        [Size(255)]
        public SchemaString BackgroundServiceAdminEmail;

        public SchemaGuid PaymentModeCashID;
        public SchemaGuid PaymentModeChequeID;

        public TAttachedImage LoginLogo { get { return OneToOne<TAttachedImage>("LoginLogoID"); } }
        public TCurrency BaseCurrency { get { return OneToOne<TCurrency>("BaseCurrencyID"); } }

        public TApplicationSettingService ApplicationSettingServices { get { return OneToMany<TApplicationSettingService>("ApplicationSettingID"); } }
        public TApplicationSettingSmsKeywordHandler ApplicationSettingSmsKeywordHandlers { get { return OneToMany<TApplicationSettingSmsKeywordHandler>("ApplicationSettingID"); } }

        //Excel Reader Web Service
        //
        [Size(255)]
        public SchemaString ExcelReaderWebServiceURL;
        [Default(0)]
        public SchemaInt ExcelReaderUseWebService;

        //Login Captcha
        [Default(1)]
        public SchemaInt IsUsingCaptcha;
        [Size(1)]
        public SchemaInt AllowConcurrentLogin;

        // AD Integration
        // Added: Wang Yiyuan, 4/23/2012
        // Populating User information from Active Directory
        //
        [Default(1)]
        public SchemaInt GetUserEmailFromActiveDirectory;
        public SchemaInt UseImpersonatorToAD;
        public SchemaString ADLogOnDomainAccount;
        public SchemaString ADLogOnPassword;
        public SchemaString ADDomainName;
        [Default("")]
        public SchemaString ActiveDirectoryUserGroupNames;

        // Performance tab
        // Kelvin
        // Manage performance of report generation
        public SchemaDateTime PerformanceSundayWorkingHoursStartTime;
        public SchemaDateTime PerformanceSundayWorkingHoursEndTime;
        public SchemaDateTime PerformanceSaturdayWorkingHoursStartTime;
        public SchemaDateTime PerformanceSaturdayWorkingHoursEndTime;
        public SchemaDateTime PerformanceWeekdayWorkingHoursStartTime;
        public SchemaDateTime PerformanceWeekdayWorkingHoursEndTime;

        [Default(1)]
        public SchemaInt PerformanceNumberOfConcurrentReports;

        // Allowed File Types
        [Size(255)]
        public SchemaString AllowedFileTypeExtension;


        [Default(5)]
        public SchemaInt MaxUnauthorizedAccessCount;

        [Default("dummy user")]
        public SchemaString DummyUsername;
        [Default("dummy_user")]
        public SchemaString DummyLoginName;
        [Default("123456")]
        public SchemaString DummyPassword;

        [Default(0)]
        public SchemaInt SupplierPortalIsUnderMaintenace;

        [Default(0)]
        public SchemaInt WebAppIsUnderMaintenace;

        [Size(1000)]
        public SchemaString ContactUsText;

        [Size(1000)]
        public SchemaString VendorPortalURL;

        public SchemaInt SafetyIncidentNumberOfMCDays;

        /// <summary>
        /// 0: Only allow to add Recoverable after PO/AP Invoice is approved, or WO is closed. Will only have GL Posting
        /// 1: Only allow to add Recoverable when RFQ/PO/AP Invoice/WO is still in processing
        /// </summary>
        [Default(0)]
        public SchemaInt RecoverableMode;
        [Size(255)]
        public SchemaString RFQRecoverableBeforeApprovalStates;
        [Size(255)]
        public SchemaString RFQRecoverableAfterApprovalStates;
        [Size(255)]
        public SchemaString PORecoverableBeforeApprovalStates;
        [Size(255)]
        public SchemaString PORecoverableAfterApprovalStates;
        [Size(255)]
        public SchemaString APInvoiceRecoverableBeforeApprovalStates;
        [Size(255)]
        public SchemaString APInvoiceRecoverableAfterApprovalStates;
        [Size(255)]
        public SchemaString WORecoverableBeforeApprovalStates;
        [Size(255)]
        public SchemaString WORecoverableAfterApprovalStates;

        public SchemaGuid EquipmentTemplateID;
        public TAttachment EquipmentTemplate { get { return OneToOne<TAttachment>("EquipmentTemplateID"); } }

        public TApplicationSettingMenuItem MenuItems { get { return OneToMany<TApplicationSettingMenuItem>("ApplicationSettingID"); } }
    }

    public abstract partial class OApplicationSetting : LogicLayerPersistentObject, IAuditTrailEnabled
    {             
        ////Integration Tab
        //public abstract string BuildingFilePath { get; set; }
        //public abstract string VacantListFilePath { get; set; }
        //public abstract string OccupiedListFilePath { get; set; }
        //public abstract string BackupPath { get; set; }
        //public abstract string AdministratorEmail { get; set; }
        //public abstract string ErrorReason { get; set; }

        /// <summary>
        /// Gets or sets a short title text to be displayed above
        /// the login controls.
        /// </summary>
        public abstract string LoginTitle { get; set; }

        /// <summary>
        /// Gets or sets the foreign key to the AttachedImage
        /// object for the login logo.
        /// </summary>
        public abstract Guid? LoginLogoID { get; set; }

        /// <summary>
        /// Gets or sets the URL to the home page (the page that
        /// displays the inbox of tasks).
        /// </summary>
        public abstract string HomePageUrl { get; set; }

        /// <summary>
        /// [Column] Gets or sets the minimum length for the password.
        /// </summary>
        public abstract int? PasswordMinimumLength { get; set; }

        /// <summary>
        /// [Column] Gets or sets the password type:
        /// 0: password contains alphabet only. This setting is default.
        /// 1: password contains both alphabet and number.
        /// </summary>
        public abstract int? PasswordRequiredCharacters { get; set; }

        /// <summary>
        /// [Column] Gets or sets the maximum number of attempted logins.
        /// </summary>
        public abstract int? PasswordMaximumTries { get; set; }

        /// <summary>
        /// [Column] Gets or sets the number of days to pass before user is required to update password. 
        /// Set to 0 to disable expiry check
        /// </summary>
        public abstract int? PasswordDaysToExpiry { get; set; }

        /// <summary>
        /// [Column] Gets or sets the number of days to pass before user can update password again.
        /// </summary>
        public abstract int? PasswordMinimumAge { get; set; }

        /// <summary>
        /// [Column] Gets or sets the number of passwords in history to ensure difference from current password.
        /// </summary>
        public abstract int? PasswordHistoryKept { get; set; }

        /// <summary>
        /// [Column Gets or sets the number of days to keep the message history
        /// before removing them. The message history is deleted whenever the
        /// a user logs on to the system.
        /// </summary>
        public abstract int? NumberOfDaysToKeepMessageHistory { get; set; }

        /// <summary>
        /// [Column Gets or sets the number of days to keep the login history
        /// before removing them. The login history is deleted whenever the
        /// a user logs on to the system.
        /// </summary>
        public abstract int? NumberOfDaysToKeepLoginHistory { get; set; }

        /// <summary>
        /// [Column Gets or sets the number of days to keep the background 
        /// service log before removing them. 
        /// </summary>
        public abstract int? NumberOfDaysToKeepBackgroundServiceLog { get; set; }

        /// <summary>
        /// [Column] Gets or sets the URL of the current system. This is normally 
        /// used by the message templates to notify the user where to log on
        /// to.
        /// </summary>
        public abstract String SystemUrl { get; set; }

        /// <summary>
        /// [Column] Gets or sets a flag indicating whether the
        /// user e-mail is compulsory when create a user account.
        /// </summary>
        public abstract int? IsUserEmailCompulsory { get; set; }

        /// <summary>
        /// [Column] Gets or sets a flag indicating whether user
        /// action should be traced.
        /// </summary>
        public abstract int? TraceUserAction { get; set; }

        /// <summary>
        /// [Column] Gets or sets a flag indicating whether generations
        /// of report should be logged.
        /// </summary>
        public abstract int? LogReportGeneration { get; set; }

        /// <summary>
        /// [Column] Gets or sets the foreign key of the
        /// base currency used by this instance of the system.
        /// </summary>
        public abstract Guid? BaseCurrencyID { get; set; }

        /// <summary>
        /// [Column] Gets or sets the future budget spending policy.
        /// <list>
        ///     <item>0 - Disallow spending from budget periods that have not been created. </item>
        ///     <item>1 - Allow spending from budget periods that have not been created. </item>
        /// </list>
        /// </summary>
        public abstract int? DefaultBudgetSpendingPolicy { get; set; }

        /// <summary>
        /// [Column] Gets or sets at which point in time the budget is deducted.
        /// <list>
        ///     <item>0 - Deducted at the point of submission of any purchase object. </item>
        ///     <item>1 - Deducted at the point of approval of any purchase object. </item>
        /// </list>
        /// </summary>
        public abstract int? DefaultBudgetDeductionPolicy { get; set; }

        public abstract int? DefaultRequiredCount { get; set; }
        public abstract int? DefaultRequiredUnit { get; set; }
        public abstract int? DefaultEndCount { get; set; }
        public abstract int? DefaultEndUnit { get; set; }
        public abstract int? AllowPerLineItemTaxInPO { get; set; }


        /// <summary>
        /// [Column] Gets or sets the default number of days
        /// in advance to create fixed works.
        /// </summary>
        public abstract int? DefaultNumberOfDaysInAdvanceToCreateFixedWorks { get; set; }

        /// <summary>
        /// [Column] Gets or sets the foreign key to the Code
        /// table that represents the default type of work
        /// that will be assigned to a Work object when it
        /// is first created.
        /// </summary>
        public abstract Guid? DefaultTypeOfWorkID { get; set; }

        /// <summary>
        /// [Column] Gets or sets the foreign key to the Code
        /// table that represents the default type of work
        /// that will be assigned to a Scheduled Work object when it
        /// is first created.
        /// </summary>
        public abstract Guid? DefaultScheduledWorkTypeOfWorkID { get; set; }

        /// <summary>
        /// [Column] Gets or sets the event that is triggered on the case objectm
        /// after the child work is close or cancelled.
        /// </summary>
        public abstract string CaseEventAfterDocumentClosedOrCancelled { get; set; }

        /// <summary>
        /// [Column] Gets or sets the unit of measure used
        /// for identifying the default unit of measure
        /// for equipment catalogs in the Inventory Center. 
        /// </summary>
        public abstract Guid? EquipmentUnitOfMeasureID { get; set; }

        /// <summary>
        /// [Column] Gets or sets the default costing type, 
        /// or the accounting method of an item of this catalogue in this store
        /// when an item is first checked in to the store.
        /// <para></para>
        /// 	<list>
        /// 		<item>0 / StoreItemCostingType.FIFO: First-in-first-out </item>
        /// 		<item>1 / StoreItemCostingType.LIFO: Last-in-first-out</item>
        /// 		<item>3 / StoreItemCostingType.StandardCosting: Standard costing</item>
        /// 		<item>4 / StoreItemCostingType.AverageCosting: Average costing</item>
        /// 	</list>
        /// </summary>
        public abstract int? InventoryDefaultCostingType { get; set; }

        /// <summary>
        /// [Columnn] Gets or sets a flag that indicates 
        /// whether e-mails will be sent out.
        /// </summary>
        public abstract int? EnableEmail { get; set; }

        /// <summary>
        /// [Column] Gets or sets a flag that indicates
        /// whether SMSes will be sent out.
        /// </summary>
        public abstract int? EnableSms { get; set; }

        /// <summary>
        /// [Column] Gets or sets a flag that indicates
        /// whether SMSes are direct from modem (0) 
        /// or relay to a web service (1).
        /// </summary>
        public abstract int? SMSSendType { get; set; }

        /// <summary>
        /// [Column] Gets or sets a flag that indicates
        /// SMSRelayWSURL that sms will send to
        /// </summary>
        public abstract String SMSRelayWSURL { get; set; }

        /// <summary>
        /// [Column] Gets or sets the total number of tries
        /// the message service will try to send a failed
        /// message before giving up.
        /// </summary>
        public abstract int? MessageNumberOfTries { get; set; }

        /// <summary>
        /// [Column] Gets or sets the SMTP server address.
        /// </summary>
        public abstract string MessageSmtpServer { get; set; }

        /// <summary>
        /// [Column] Gets or sets the SMTP port.
        /// </summary>
        public abstract int? MessageSmtpPort { get; set; }

        /// <summary>
        /// [Column] Gets or sets a flag indicating whether the SMTP 
        /// server requires authentication with SSL
        /// </summary>
        public abstract int? EnableSSL { get; set; }

        /// <summary>
        /// [Column] Gets or sets a flag indicating whether the SMTP 
        /// server requires authentication before allow us to send e-mail
        /// through it.
        /// </summary>
        public abstract int? MessageSmtpRequiresAuthentication { get; set; }

        /// <summary>
        /// [Column] Gets or sets the SMTP server user
        /// name used for server authentication.
        /// </summary>
        public abstract String MessageSmtpServerUserName { get; set; }

        /// <summary>
        /// [Column] Gets or sets the SMTP server password
        /// used for server authentication.
        /// </summary>
        public abstract String MessageSmtpServerPassword { get; set; }

        /// <summary>
        /// [Column] Gets or sets a flag indicating that the system is
        /// capable of receiving e-mail from an e-mail server.
        /// </summary>
        public abstract int? EnableReceiveEmail { get; set; }

        /// <summary>
        /// [Column] Gets or sets the e-mail server type.
        /// <list>
        ///    <item>0 - POP3 </item>
        ///    <item>1 - Microsoft Exchange 2007 </item>
        /// </list>
        /// </summary>
        public abstract int? EmailServerType { get; set; }

        /// <summary>
        /// [Column] Gets or sets the address of the incoming e-mail server (POP3).
        /// </summary>
        public abstract String EmailServer { get; set; }

        /// <summary>
        /// [Column] Gets or sets the user name required to authenticate 
        /// against the incoming e-mail server.
        /// </summary>
        public abstract String EmailUserName { get; set; }

        /// <summary>
        /// [Column] Gets or sets the encrypted password required to authenticate 
        /// against the incoming e-mail server.
        /// </summary>
        public abstract String EmailPassword { get; set; }

        /// <summary>
        /// [Column] Gets or sets the incoming e-mail server port number.
        /// </summary>
        public abstract int? EmailPort { get; set; }

        /// <summary>
        /// [Column] Gets or sets the incoming e-mail server Exchange
        /// Web Services URL.
        /// </summary>
        public abstract String EmailExchangeWebServiceUrl { get; set; }

        /// <summary>
        /// [Column] Gets or sets the domain used
        /// to authenticate against the incoming e-mail server .
        /// </summary>
        public abstract String EmailDomain { get; set; }

        /// <summary>
        /// [Column] Gets or sets the COM port in which the
        /// SMS modem is connected to.
        /// </summary>
        public abstract String MessageSmsComPort { get; set; }

        /// <summary>
        /// [Column] Gets or sets the SMS baud rate that
        /// the SMS modem will be communicating at.
        /// </summary>
        public abstract int? MessageSmsBaudRate { get; set; }

        /// <summary>
        /// [Column] Gets or sets the parity per byte 
        /// of information transferred.
        /// </summary>
        public abstract String MessageSmsParity { get; set; }

        /// <summary>
        /// [Column] Gets or sets the number of data bits
        /// in each byte of information transferred.
        /// </summary>
        public abstract int? MessageSmsDataBits { get; set; }

        /// <summary>
        /// [Column] Gets or sets the stop bits per byte
        /// of information transferred.
        /// </summary>
        public abstract String MessageSmsStopBits { get; set; }

        /// <summary>
        /// [Column] Gets or sets the handshaking protocol.
        /// </summary>
        public abstract String MessageSmsHandshake { get; set; }

        /// <summary>
        /// [Column] Gets or sets the number of digits for
        /// local numbers. When trying to send SMSes where the
        /// number of digits is not the same as this value, 
        /// the system will add a '+' sign to indicate that
        /// it is an overseas number.
        /// </summary>
        public abstract int? MessageSmsLocalNumberDigits { get; set; }

        /// <summary>
        /// [Column] Gets or sets the SMS modem initialization
        /// AT commands, each separated by a semi-colon.
        /// </summary>
        public abstract String MessageSmsInitCommands { get; set; }

        /// <summary>
        /// [Column] Gets or sets the SMS modem AT command to
        /// send a message to a recipient through the {0} placeholder.
        /// </summary>
        public abstract String MessageSmsSendCommands { get; set; }

        /// <summary>
        /// [Column] Gets or sets the SMS modem AT command
        /// to read a message in the SIM card memory at an index 
        /// specified through the {0} placeholder.
        /// </summary>
        public abstract String MessageSmsReceiveCommands { get; set; }

        /// <summary>
        /// [Column] Gets or sets the SMS modem AT command
        /// to remove a read message in the SIM card memory at
        /// an index specified through the {0} placeholder.
        /// </summary>
        public abstract String MessageSmsDeleteCommands { get; set; }

        /// <summary>
        /// [Column] Gets or sets the SMS modem AT command
        /// to set the modem's mode to send ASCII messages.
        /// </summary>
        public abstract String MessageSmsInitASCIICommand { get; set; }

        /// <summary>
        /// [Column] Gets or sets the SMS modem AT command
        /// to set the modem's mode to send UCS (Unicode) messages.
        /// </summary>
        public abstract String MessageSmsInitUCS2Command { get; set; }

        /// <summary>
        /// [Column] Gets or sets the string representing the
        /// new line character sent to/from the modem.
        /// </summary>
        public abstract String MessageSmsNewLine { get; set; }

        /// <summary>
        /// [Column] Gets or sets the log file path for SMS.
        /// </summary>
        public abstract String MessageSmsLogFilePath { get; set; }

        /// <summary>
        /// [Column] Gets or sets the e-mail address of the sender
        /// for all e-mails sent out by the Anacle.EAM system.
        /// </summary>
        public abstract String MessageEmailSender { get; set; }
        public abstract String MessageEmailCC { get; set; }

        /// for POS interface folder path
        public abstract String PosToPrmsPath { get; set; }
        public abstract String PrmsToPosPath { get; set; }

        /// <summary>
        /// [Column] Gets or sets the url for performance survey.
        /// </summary>
        public abstract String SurveyURL { get; set; }

        /// <summary>
        /// Gets the login logo.
        /// </summary>
        public abstract OAttachedImage LoginLogo { get; set; }

        /// <summary>
        /// Gets the base currency used by this instance of the system.
        /// </summary>
        public abstract OCurrency BaseCurrency { get; set; }

        /// <summary>
        /// Gets a list of OApplicationSettingService objects
        /// that represent that list of services applicable
        /// for this application.
        /// </summary>
        public abstract DataList<OApplicationSettingService> ApplicationSettingServices { get; }

        /// <summary>
        /// Gets a list of OApplicationSettingSmsKeywordHandler objects
        /// that represent the SMS keyword handlers applicable for
        /// this application.
        /// </summary>
        public abstract DataList<OApplicationSettingSmsKeywordHandler> ApplicationSettingSmsKeywordHandlers { get; }

        /// <summary>
        /// [Column] Gets or sets a flag indicating that the system
        /// will use Active Directory to authenticate users when they
        /// log in.
        /// </summary>
        public abstract Int32? IsUsingActiveDirectory { get; set; }

        /// <summary>
        /// [Column] Gets or sets the name for the Active Directory Domain
        /// </summary>
        public abstract String ActiveDirectoryDomain { get; set; }

        /// <summary>
        /// [Column] Gets or sets the LDAP path for the Active Directory Domain
        /// </summary>
        public abstract String ActiveDirectoryPath { get; set; }

        /// <summary>
        /// [Column] Gets or sets a flag indicating if the application
        /// should impersonate a Windows account before running reports.
        /// This should be set to 1 if the database_readonly connection 
        /// string is set up to use Trusted_Connection, 
        /// or the Integrated Security=SSPI.
        /// </summary>
        public abstract int? IsImpersonateAccountForReport { get; set; }

        /// <summary>
        /// [Column] The windows user name of the account
        /// used to access the reports.
        /// </summary>
        public abstract String ReportDomain { get; set; }

        /// <summary>
        /// [Column] The windows user name of the account
        /// used to access the reports.
        /// </summary>
        public abstract String ReportUserName { get; set; }

        /// <summary>
        /// [Column] The windows password of the account
        /// used to access the reports.
        /// </summary>
        public abstract String ReportPassword { get; set; }

        /// <summary>
        /// [Column] The name of the report generation system
        /// </summary>
        public abstract String ReportSystemName { get; set; }

        /// <summary>
        /// [Column] Gets or sets Background Service Administrator Email
        /// </summary>
        public abstract String BackgroundServiceAdminEmail { get; set; }

        public abstract Guid? PaymentModeCashID { get; set; }

        public abstract Guid? PaymentModeChequeID { get; set; }

        /// <summary>
        /// [Column] Gets or sets ExcelReaderWebService URL
        /// </summary>
        public abstract String ExcelReaderWebServiceURL { get; set; }
        /// <summary>
        /// [Column] Indicate whether to use excel web service 
        /// </summary>
        public abstract int? ExcelReaderUseWebService { get; set; }


        /// <summary>
        /// [Column] Indicate whether to use captcha
        /// </summary>
        public abstract Int32? IsUsingCaptcha { get; set; }

        /// <summary>
        /// [Column] get or set whether to allow concurrent login with the same username and password
        /// 1 - Yes; 0 - No
        /// </summary>
        public abstract int? AllowConcurrentLogin { get; set; }

        // AD Integration
        // Yiyuan, 2012-6-14
        //
        public abstract int? GetUserEmailFromActiveDirectory { get; set; }
        public abstract int? UseImpersonatorToAD { get; set; }
        public abstract string ADLogOnDomainAccount { get; set; }
        public abstract string ADLogOnPassword { get; set; }
        public abstract string ADDomainName { get; set; }
        public abstract string ActiveDirectoryUserGroupNames { get; set; }

        // Performance tab
        public abstract DateTime? PerformanceSundayWorkingHoursStartTime { get; set; }
        public abstract DateTime? PerformanceSundayWorkingHoursEndTime { get; set; }
        public abstract DateTime? PerformanceSaturdayWorkingHoursStartTime { get; set; }
        public abstract DateTime? PerformanceSaturdayWorkingHoursEndTime { get; set; }
        public abstract DateTime? PerformanceWeekdayWorkingHoursStartTime { get; set; }
        public abstract DateTime? PerformanceWeekdayWorkingHoursEndTime { get; set; }

        public abstract int? PerformanceNumberOfConcurrentReports { get; set; }

        //KL: use thread level cache instead of global cache
        //private static OApplicationSetting current = null;

        public abstract string AllowedFileTypeExtension { get; set; }

        public abstract int? MaxUnauthorizedAccessCount { get; set; }

        public abstract int? SafetyIncidentNumberOfMCDays { get; set; }

        public abstract string DummyUsername { get; set; }
        public abstract string DummyLoginName { get; set; }
        public abstract string DummyPassword { get; set; }

        public abstract int? WebAppIsUnderMaintenace { get; set; }
        public abstract int? SupplierPortalIsUnderMaintenace { get; set; }

        public abstract string ContactUsText { get; set; }
        public abstract string VendorPortalURL { get; set; }

        public abstract int? RecoverableMode { get; set; }
        public abstract string RFQRecoverableBeforeApprovalStates { get; set; }
        public abstract string RFQRecoverableAfterApprovalStates { get; set; }
        public abstract string PORecoverableBeforeApprovalStates { get; set; }
        public abstract string PORecoverableAfterApprovalStates { get; set; }
        public abstract string APInvoiceRecoverableBeforeApprovalStates { get; set; }
        public abstract string APInvoiceRecoverableAfterApprovalStates { get; set; }
        public abstract string WORecoverableBeforeApprovalStates { get; set; }
        public abstract string WORecoverableAfterApprovalStates { get; set; }

        public abstract Guid? EquipmentTemplateID { get; set; }
        public abstract OAttachment EquipmentTemplate { get; set; }

        public abstract DataList<OApplicationSettingMenuItem> MenuItems { get; }

        public string TempReportPassword { get; set; }

        /// <summary>
        /// Loads the current application settings from the 
        /// database.
        /// </summary>
        /// 
        public static OApplicationSetting Current
        {
            //KL: load from thread cache
            get
            {
                OApplicationSetting current = (OApplicationSetting)MemoryCache.CurrentThreadCache.Get(typeof(OApplicationSetting).ToString());
                if (current == null)
                {
                    using (Connection c = new Connection())
                    {
                        // Loads the application setting object
                        // with a lock on the ApplicationSetting
                        // table so that no other thread can select
                        // from the table at the same time.
                        //
                        //c.SetLockMode(LockMode.UpdateLock);
                        current = TablesLogic.tApplicationSetting.Load(Query.True);
                        //c.SetLockMode(LockMode.Default);

                        if (current == null)
                        {
                            current = TablesLogic.tApplicationSetting.Create();
                            current.Save();
                        }
                        c.Commit();
                    }

                    MemoryCache.CurrentThreadCache.Set(typeof(OApplicationSetting).ToString(), current);
                }
                return current;
            }
        }

        public override void Saving()
        {
            base.Saving();

            foreach (OApplicationSettingService oss in this.ApplicationSettingServices)
            {
                if (oss.BackgroundServiceRun == null)
                    oss.Touch();
            }

            if (_tempMenuItems != null)
            {
                MenuItems.Clear();
                MenuItems.AddRange(_tempMenuItems);
            }

            if (!string.IsNullOrEmpty(TempReportPassword))
                this.ReportPassword = Security.Encrypt(TempReportPassword);
        }


        /// <summary>
        /// Overrides the saved method for this application domain
        /// to reflect the new settings immediately.
        /// <para></para>
        /// Application domains that this object was not saved in
        /// will not use the new settings until the application is
        /// restarted. For example, services and web applications in
        /// a server farm must be restarted manually in order for
        /// the new settings to take effect.
        /// </summary>
        public override void Saved()
        {
            base.Saved();

            //current = TablesLogic.tApplicationSetting.Load(Query.True);
            MemoryCache.CurrentThreadCache.Set(typeof(OApplicationSetting).ToString(), TablesLogic.tApplicationSetting.Load(Query.True));
            InvalidateBackgroundServiceRuns();

            //remove the background services
            //if the service gets removed from the application setting
            List<OApplicationSettingService> applicationSettingServices =
                TablesLogic.tApplicationSettingService.LoadList(TablesLogic.tApplicationSettingService.ApplicationSettingID == null);

            foreach (OApplicationSettingService applicationSettingService in applicationSettingServices)
            {
                OBackgroundServiceRun bsr = applicationSettingService.BackgroundServiceRun;

                if (bsr != null)
                    bsr.Delete();

                applicationSettingService.Delete();
            }
        }


        /// <summary>
        /// Invalidates the cached background service runs object
        /// and forces them to be reloaded whenever they are accessed
        /// again.
        /// </summary>
        public void InvalidateBackgroundServiceRuns()
        {
            foreach (OApplicationSettingService service in this.ApplicationSettingServices)
                service.cachedBackgroundServiceRun = null;
        }


        Hashtable services = null;


        /// <summary>
        /// Gets a flag indicating whether the service
        /// of the specified name is enabled.
        /// </summary>
        /// <param name="serviceName"></param>
        /// <returns></returns>
        public bool IsServiceEnabled(string serviceName)
        {
            if (services == null)
            {
                services = new Hashtable();
                foreach (OApplicationSettingService service in this.ApplicationSettingServices)
                    services[service.ServiceName] = service;
            }

            OApplicationSettingService appService = services[serviceName] as OApplicationSettingService;
            if (appService != null)
                return appService.IsEnabled == 1;
            return false;
        }


        /// <summary>
        /// Gets the timer interval of the specified service.
        /// </summary>
        /// <param name="serviceName"></param>
        /// <returns></returns>
        public string GetServiceTimerInterval(string serviceName)
        {
            if (services == null)
            {
                services = new Hashtable();
                foreach (OApplicationSettingService service in this.ApplicationSettingServices)
                    services[service.ServiceName] = service;
            }

            OApplicationSettingService appService = services[serviceName] as OApplicationSettingService;
            if (appService != null)
                return appService.TimerInterval;
            return "5 minutes";
        }

        private List<OApplicationSettingMenuItem> _tempMenuItems;
        public List<OApplicationSettingMenuItem> TempMenuItems
        {
            get
            {
                if (_tempMenuItems != null)
                    return _tempMenuItems;

                _tempMenuItems = new List<OApplicationSettingMenuItem>();
                Dictionary<string, OApplicationSettingMenuItem> dict = new Dictionary<string, OApplicationSettingMenuItem>();
                OApplicationSettingMenuItem reportItem = null;
                foreach (OApplicationSettingMenuItem item in MenuItems)
                {
                    dict.Add(item.MenuName, item);
                    if (item.MenuName == "##Reports##")
                        reportItem = item;
                }

                if (reportItem == null)
                {
                    reportItem = TablesLogic.tApplicationSettingMenuItem.Create();
                    reportItem.MenuName = "##Reports##";
                    reportItem.BrightColor = "white";
                    reportItem.DarkColor = "white";
                }

                DataTable dt = OFunction.GetAllCategories();
                foreach (DataRow row in dt.Rows)
                {
                    string category = row["CategoryName"].ToString();
                    if (dict.ContainsKey(category))
                    {
                        _tempMenuItems.Add(dict[category]);
                    }
                    else
                    {
                        OApplicationSettingMenuItem item = TablesLogic.tApplicationSettingMenuItem.Create();
                        item.MenuName = category;
                        item.BrightColor = "white";
                        item.DarkColor = "white";
                        _tempMenuItems.Add(item);
                    }
                }

                _tempMenuItems.Add(reportItem);

                return _tempMenuItems;
            }
        }
    }

    public enum EnumApplicationGeneral
    {
        No = 0,
        Yes = 1
    }

    public class RecoverableModes
    {
        public const int AddRecoverableAfterApproved = 0;
        public const int AddRecoverableBeforeApproval = 1;
    }
}
