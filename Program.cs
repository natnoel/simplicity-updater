using DiffMatchPatch;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Data.SqlClient;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;

namespace Simplicity_Updater
{
    class Program
    {
        static int totalEditFiles = 0, unchangedEdit = 0, totalSearchFiles = 0, unchangedSearch = 0, unchangedStates = 0, progress = 0;
        static string rootDir = @"C:\Projects\abell.80-ForNYP";
        static WorkflowStateMapper mapper = new WorkflowStateMapper(@"test\states.csv");
        static string[,] srcDestList =
            {
                { @"copy\root", "" },
                {@"copy\webapp", @"\webapp" },
                {@"copy\components", @"\webapp\components" },
                {@"copy\user", @"\webapp\modules\adminsystem\user" },
                {@"copy\dashboard", @"\webapp\modules\report\dashboard" },
                {@"copy\AdminSystem", @"\LogicLayer\DefaultObjects\AdminSystem" },
                {@"copy\App_GlobalResources", @"\webapp\App_GlobalResources" },
                {@"copy\css", @"\webapp\css" },
                {@"copy\images", @"\webapp\images" },
                {@"copy\img", @"\webapp\img" },
            };
        static string dbServer = "localhost", dbName = "abell.80_Dev", dbUserID = "sa", dbPw = "123";

        static void Main(string[] args)
        {
            // The code provided will print ‘Hello World’ to the console.
            // Press Ctrl+F5 (or go to Debug > Start Without Debugging) to run your app.
            Console.WriteLine("Hello World!");
            //Console.ReadKey();

            // Go to http://aka.ms/dotnet-get-started-console to continue learning how to build a console app! 

            try
            {
                UpdateAllSearch();
                UpdateAllEdit();
                //Console.WriteLine("Total files updated: " + ((totalSearchFiles + totalEditFiles) - (unchangedSearch + unchangedEdit)) + " total files unchanged: " + (unchangedSearch + unchangedEdit));
                copyFiles();
                //getWorkflowStates();
                //getChangedStates();
                UpdateAllWorkflowStates();
                CreateWorkProgressNodeNameFile();
                MergeFiles();
                UpdateWebConfig();

                Console.WriteLine("Progress " + progress + " of " + GetTotalNumOfFiles());

                UpdateSchema();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private static int GetTotalNumOfFiles()
        {
            int totalCount =
                Directory.GetFiles(rootDir + @"\webapp\modules", "search.aspx", SearchOption.AllDirectories).Count() +
                Directory.GetFiles(rootDir + @"\webapp\modules", "edit.aspx", SearchOption.AllDirectories).Count() +
                Directory.GetFiles(rootDir, "*.xoml", SearchOption.AllDirectories).Count() +
                Directory.GetFiles(@"copy", "*", SearchOption.AllDirectories).Count() +
                Directory.GetFiles(@"merge\v8").Count()
                + 2; //Create WorkflowProgressNodeName.resx and Update Web.Config

            /*Console.WriteLine("Search: " + Directory.GetFiles(rootDir + @"\webapp\modules", "search.aspx", SearchOption.AllDirectories).Count());
            Console.WriteLine("Edit: " + Directory.GetFiles(rootDir + @"\webapp\modules", "edit.aspx", SearchOption.AllDirectories).Count());
            Console.WriteLine("xoml: " + Directory.GetFiles(rootDir, "*.xoml", SearchOption.AllDirectories).Count());*/

            /*int copyNo = 0;
            for (int i = 0; i < srcDestList.GetLength(0); i++)
            {
                copyNo += Directory.GetFiles(srcDestList[i, 0], "*", SearchOption.AllDirectories).Length;
                Console.WriteLine(srcDestList[i,0] + " " + Directory.GetFiles(srcDestList[i, 0], "*", SearchOption.AllDirectories).Length);
            }

            Console.WriteLine(copyNo);
            Console.WriteLine(Directory.GetFiles(@"copy", "*", SearchOption.AllDirectories).Length);*/

            return totalCount;
        }

        private static bool UpdateSearch(string input, string output)
        {
            var doc = new HtmlDocument();
            doc.Load(input);
            doc.OptionOutputOriginalCase = true;
            doc.OptionWriteEmptyNodes = true;
            bool changed = false;

            //Replace <body ...> tag with <body class="body-search-bg">
            var bodyNode = doc.DocumentNode.SelectSingleNode("//body");
            if (null != bodyNode)
            {
                if (bodyNode.Attributes["class"] == null)   //Add attr if no exist
                {
                    bodyNode.Attributes.Add("class", "body-search-bg");
                    changed = true;
                }
                else if (!bodyNode.Attributes["class"].Value.Equals("body-search-bg"))
                {
                    bodyNode.Attributes["class"].Value = "body-search-bg";    //else reset class;
                    changed = true;
                }
            }

            //Replace <div class=”main”> tag with <div class="mdl-search-div">
            var mainNode = doc.DocumentNode.SelectSingleNode("//div[@class='main']");   //Will have trouble if more than 1 classes
            if (mainNode != null)
            {
                mainNode.Attributes["class"].Value = "mdl-search-div";
                changed = true;
            }

            //var panelNode = doc.DocumentNode.SelectSingleNode("//ui:UIPanel[@ID='AdvancedSearchPanel']");
            //This cannot work since cannot search xpath containing :

            //Wrap AdvancedSearchDialogBox around the advanced search Panel:
            HtmlNode parentNode = null, searchNode = null;
            foreach (var nNode in doc.DocumentNode.Descendants("ui:uIPanel"))
            {
                if (nNode.NodeType == HtmlNodeType.Element)
                {
                    searchNode = nNode;
                    parentNode = nNode.ParentNode;
                    break;  //We only expect on instance
                }
            }

            if (searchNode != null && !parentNode.Name.Equals("ui:UIDialogBox", StringComparison.OrdinalIgnoreCase))
            {
                HtmlNode wrapperNode = HtmlNode.CreateNode("<ui:UIDialogBox runat=\"server\" ID=\"AdvancedSearchDialogBox\"></ui:UIDialogBox>");
                wrapperNode.AppendChild(searchNode);
                parentNode.ReplaceChild(wrapperNode, searchNode);
                changed = true;
            }

            /*
             * Remove the following columns:
               <ui:UIGridViewButtonColumn CommandName=”EditObject"  … />
               <ui:UIGridViewButtonColumn CommandName=”ViewObject"  … />
             */
            List<HtmlNode> removeList = new List<HtmlNode>();
            foreach (var node in bodyNode.Descendants("web:search2"))
                removeList.Add(node);
            foreach (var node in bodyNode.Descendants("ui:UIGridViewButtonColumn"))
            {
                if (node.Attributes["CommandName"] != null)
                {
                    if (node.Attributes["CommandName"].Value.Equals("EditObject") || node.Attributes["CommandName"].Value.Equals("ViewObject"))
                        removeList.Add(node);
                }
            }

            foreach (var node in removeList)
                node.Remove();

            if (removeList.Count > 0)
                changed = true;

            if (changed)
                doc.Save(output);

            return changed;
        }

        private static bool UpdateSearch(string file)
        {
            return UpdateSearch(file, file);
        }

        private static void UpdateAllSearch()
        {
            Console.WriteLine("Updating search.aspx");
            // Get list of files in the specific directory.
            string[] files = Directory.GetFiles(rootDir + @"\webapp\modules", "search.aspx", SearchOption.AllDirectories);

            unchangedSearch = 0;
            // Display all the files.
            foreach (string file in files)
            {
                Console.Write("Updating " + file + "... ");
                if (!UpdateSearch(file))
                {
                    Console.WriteLine("No change");
                    unchangedSearch++;
                }
                else
                {
                    Console.WriteLine("Done");
                }
                progress++;
            }
            Console.WriteLine("Updated " + (files.Length - unchangedSearch) + " files " + unchangedSearch + " unchanged");
            Console.WriteLine("---------------------------------------------------------------------------");
            totalSearchFiles = files.Length;
        }

        private static bool UpdateEdit(string input, string output)
        {
            var doc = new HtmlDocument();
            doc.Load(input);
            doc.OptionOutputOriginalCase = true;
            bool changed = false;

            //Replace <body ...> tag with <body class="body-bg">
            HtmlNode bodyNode = doc.DocumentNode.SelectSingleNode("//body");
            if (bodyNode != null)
            {
                if (bodyNode.Attributes["class"] == null)   //Add attr if no exist
                {
                    bodyNode.Attributes.Add("class", "body-bg");
                    changed = true;
                }
                else if (!bodyNode.Attributes["class"].Value.Equals("body-bg", StringComparison.OrdinalIgnoreCase))
                {
                    bodyNode.Attributes["class"].Value = "body-bg";    //else replace the class(es) with body-bg;
                    changed = true;
                }
            }

            //Replace <div class=”main”> tag with <div class="mdl-edit-div">
            bool foundMain = false;
            foreach (var node in doc.DocumentNode.Descendants("div"))
            {
                if (node.Attributes["class"] != null && node.Attributes["class"].Value.Contains("main"))
                {
                    string[] classes = node.Attributes["class"].Value.Split(' ');
                    for (int i = 0; i < classes.Length; i++)
                    {
                        if (classes[i].Equals("main", StringComparison.OrdinalIgnoreCase) || classes[i].Equals("div-main", StringComparison.OrdinalIgnoreCase))
                        {
                            foundMain = true;
                            //Console.WriteLine(node.OuterHtml);
                            classes[i] = "mdl-edit-div";
                            node.Attributes["class"].Value = string.Join(" ", classes);
                            //Console.WriteLine(string.Join(" ", classes));
                            changed = true;
                            break;
                        }
                    }
                }
                if (foundMain)
                    break;
            }

            //Wrap with DialogBox
            List<HtmlNode> wrapList = new List<HtmlNode>();
            foreach (HtmlNode node in doc.DocumentNode.Descendants("ui:UIObjectPanel"))
            {
                if (node.Element("web:subpanel") != null && !node.ParentNode.Name.Equals("ui:UIDialogBox", StringComparison.OrdinalIgnoreCase))
                {
                    wrapList.Add(node);
                }
            }

            foreach (HtmlNode node in wrapList)
            {
                if (node.Attributes["ID"] != null)
                {
                    string dialogBoxID = node.Attributes["ID"].Value + "_DialogBox";
                    HtmlNode wrapperNode = HtmlNode.CreateNode("<ui:UIDialogBox runat=\"server\" ID=\"" + dialogBoxID + "\"></ui:UIDialogBox>");
                    HtmlNode parentNode = node.ParentNode;
                    wrapperNode.AppendChild(node);
                    parentNode.ReplaceChild(wrapperNode, node);
                    changed = true;
                }
            }

            if (changed)
                doc.Save(output);

            return changed;
        }

        private static bool UpdateEdit(string file)
        {
            return UpdateEdit(file, file);
        }

        private static void UpdateAllEdit()
        {
            Console.WriteLine("Updating edit.aspx");
            totalEditFiles = 0;
            unchangedEdit = 0;
            RecurseDirectoryUpdateEdit(rootDir + @"\webapp\modules");
            Console.WriteLine(totalEditFiles - unchangedEdit + " updated " + unchangedEdit + " unchanged\n");
            Console.WriteLine("---------------------------------------------------------------------------");
        }

        // Process all files in the directory passed in, recurse on any directories 
        // that are found, and process the files they contain.
        public static void RecurseDirectoryUpdateEdit(string targetDirectory)
        {
            // Process the list of files found in the directory.
            string[] fileEntries = Directory.GetFiles(targetDirectory, "edit.aspx");
            foreach (string fileName in fileEntries)
            {
                Console.Write("Updating " + fileName + "... ");
                //totalEditFiles++;
                if (!UpdateEdit(fileName))
                {
                    Console.WriteLine("No change");
                    unchangedEdit++;
                }
                else
                {
                    Console.WriteLine("Done");
                }
                progress++;
            }
            totalEditFiles += fileEntries.Length;

            // Recurse into subdirectories of this directory.
            string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);
            foreach (string subdirectory in subdirectoryEntries)
                RecurseDirectoryUpdateEdit(subdirectory);
        }

        static void copyFiles()
        {
            for (int i = 0; i < srcDestList.GetLength(0); i++)
            {
                if (System.IO.Directory.Exists(srcDestList[i, 0]))
                {
                    string[] files = System.IO.Directory.GetFiles(srcDestList[i, 0], "*", SearchOption.AllDirectories);

                    string destPath = rootDir + srcDestList[i, 1];

                    Console.WriteLine("\nFrom " + srcDestList[i, 0] + " to " + destPath);

                    // Copy the files and overwrite destination files if they already exist.
                    foreach (string s in files)
                    {
                        // Use static Path methods to extract only the file name from the path.
                        string fileName = System.IO.Path.GetFileName(s);
                        //string destFile = System.IO.Path.Combine(destPath, fileName);
                        string destFile = s.Replace(srcDestList[i, 0], destPath);
                        Console.WriteLine("Copying " + fileName + " to " + destFile);

                        string dir = Path.GetDirectoryName(destFile);
                        if (!Directory.Exists(dir))
                        {
                            Console.WriteLine("Detected directory not exist! Creating directory " + Path.GetDirectoryName(dir));
                            Directory.CreateDirectory(dir);
                        }
                        System.IO.File.Copy(s, destFile, true);
                        progress++;
                    }
                }
                else
                {
                    Console.WriteLine("Source path does not exist!");
                }
            }
            Console.WriteLine("---------------------------------------------------------------------------");
        }

        static void getWorkflowStates()
        {
            string[] files = Directory.GetFiles(@"C:\Projects\Simplicity\Simplicity\Web Application",
                    "*.xoml",
                    SearchOption.AllDirectories);

            if (File.Exists(@"test/allstates.csv"))
            {
                File.Delete(@"test/allstates.csv");
            }
            StringBuilder sb = new StringBuilder();
            sb.Append("Workflow file,States\n");
            foreach (string file in files)
            {
                //Console.WriteLine(file);
                sb.Append(file).Append(",");

                XmlDocument doc = new XmlDocument();
                doc.Load(file);

                List<string> stateList = new List<string>();
                foreach (XmlNode node in doc.GetElementsByTagName("StateActivity"))
                {
                    //Console.WriteLine(node.Attributes["x:Name"].Value);
                    //sb.Append(node.Attributes["x:Name"].Value).Append(",");
                    stateList.Add(node.Attributes["x:Name"].Value);
                }
                stateList.Sort();
                foreach (string s in stateList)
                {
                    sb.Append(s).Append(" ");
                }
                sb.Append("\n");
            }
            File.WriteAllText(@"test/allstates.csv", sb.ToString());
        }

        static bool UpdateStates(string input, string output, WorkflowStateMapper mapper)
        {
            bool changed = false;

            HtmlDocument doc = new HtmlDocument();
            doc.OptionOutputOriginalCase = true;
            doc.Load(input);

            //Console.WriteLine(doc.DocumentNode.OuterHtml);
            //HtmlNode bodyNode = doc.DocumentNode.SelectSingleNode("//StateActivity");
            //Console.WriteLine(bodyNode.OuterHtml);

            //Get all the current file workflow states
            List<string> docStates = new List<string>();
            foreach (HtmlNode node in doc.DocumentNode.Descendants("StateActivity"))
                docStates.Add(node.Attributes["x:Name"].Value);

            /*int rowNo;
            Console.WriteLine(rowNo = mapper.matchPattern(docStates));
            Console.WriteLine(mapper.getSimplifiedWorkflowStateNames(rowNo));
            Console.WriteLine(mapper.getSimplifiedWorkflowStateCodes(rowNo));*/

            string stateNames = "", codes = "";

            //If it does not match, return
            if (!mapper.matchPattern(docStates, ref stateNames, ref codes))
                return changed;

            /*Console.WriteLine(stateNames + " " + codes);
            docStates.Sort();
            Console.WriteLine(String.Join(" ", docStates));*/

            //Update the state and code attributes
            foreach (HtmlNode node in doc.DocumentNode.Descendants("ns0:AnacleStateMachineWorkflow"))
            {
                if (node.Attributes["SimplifiedWorkflowStateNames"] == null || !node.Attributes["SimplifiedWorkflowStateNames"].Value.Equals(stateNames))
                {
                    node.SetAttributeValue("SimplifiedWorkflowStateNames", stateNames);
                    changed = true;
                }
                if (node.Attributes["SimplifiedWorkflowStateCodes"] == null || !node.Attributes["SimplifiedWorkflowStateCodes"].Value.Equals(codes))
                {
                    node.SetAttributeValue("SimplifiedWorkflowStateCodes", codes);
                    changed = true;
                }
            }

            //Change StateActivity tag name and add attribute SimplifiedWorkflowStateCode
            foreach (HtmlNode node in doc.DocumentNode.Descendants("StateActivity"))
            {
                node.Name = "ns1:AnacleStateActivity";
                //Console.WriteLine("State: " + node.Attributes["x:Name"].Value + " code: " + mapper.mapStateToCode(node.Attributes["x:Name"].Value));
                node.SetAttributeValue("SimplifiedWorkflowStateCode", mapper.mapStateToCode(node.Attributes["x:Name"].Value));
                changed = true;
            }

            if (changed)
                doc.Save(output);

            return changed;
        }

        static void UpdateAllWorkflowStates()
        {
            string[] files = Directory.GetFiles(rootDir, "*.xoml", SearchOption.AllDirectories);

            Console.WriteLine("Updating WorkflowStates");
            foreach (string file in files)
            {
                Console.Write("Updating " + file + ".. ");
                if (!UpdateStates(file, file, mapper))
                {
                    Console.WriteLine("No change");
                    unchangedStates++;
                }
                else
                {
                    Console.WriteLine("Done");
                }
                progress++;
            }

            Console.WriteLine(files.Count() + " processed " + unchangedStates + " unchanged");
            Console.WriteLine("---------------------------------------------------------------------------");
        }

        static void getChangedStates()
        {
            string[] files = Directory.GetFiles(@"C:\Projects\Simplicityv9\Web Application",
                    "*.xoml",
                    SearchOption.AllDirectories);

            StringBuilder sb = new StringBuilder();

            foreach (string file in files)
            {
                HtmlDocument doc = new HtmlDocument();
                doc.Load(file);

                List<string> docStates = new List<string>();
                foreach (HtmlNode node in doc.DocumentNode.Descendants("ns1:AnacleStateActivity"))
                {
                    docStates.Add(node.Attributes["x:Name"].Value);
                }

                if (docStates.Count > 0)
                {
                    docStates.Sort();
                    sb.Append(file).Append(",").Append(String.Join(" ", docStates)).Append("\n");
                }
            }

            File.WriteAllText(@"test/changedstates.csv", sb.ToString());
        }

        static void CreateWorkProgressNodeNameTemplate()
        {
            HtmlDocument doc = new HtmlDocument();
            doc.OptionOutputOriginalCase = true;
            doc.Load(@"C:\Projects\Simplicityv9\Web Application\webapp\App_GlobalResources\WorkflowProgressNodeName.resx");

            //Console.WriteLine(doc.DocumentNode.InnerHtml);
            HtmlNode rootNode = doc.DocumentNode.SelectSingleNode("//root");

            if (rootNode != null)
                Console.WriteLine("exist");
            else
                Console.WriteLine("no exist");

            HtmlNodeCollection dataNodes = doc.DocumentNode.SelectNodes("/root/data");

            foreach (HtmlNode node in dataNodes)
            {
                node.Remove();
            }

            doc.Save("test/WorkflowProgressNodeName.resx");
        }

        static void CreateWorkProgressNodeNameFile()
        {
            HtmlDocument doc = new HtmlDocument();
            doc.OptionOutputOriginalCase = true;

            doc.Load(@"test\WorkflowProgressNodeName_template.resx");

            HtmlNode rootNode = doc.DocumentNode.SelectSingleNode("/root");
            HtmlNode newlineNode = HtmlNode.CreateNode("\r\n");

            foreach (string state in mapper.allStates)
            {
                HtmlNode dataNode = doc.CreateElement("data");
                dataNode.SetAttributeValue("name", state);
                dataNode.SetAttributeValue("xml:space", "preserve");
                //dataNode.AppendChild(newlineNode);
                dataNode.AppendChild(HtmlNode.CreateNode("<value>" + state + "</value>"));
                //dataNode.AppendChild(newlineNode);
                //Console.WriteLine(dataNode.OuterHtml);
                rootNode.AppendChild(dataNode);
                rootNode.AppendChild(newlineNode);
            }

            if (File.Exists(rootDir + @"\webapp\App_GlobalResources\WorkflowProgressNodeName.resx"))
                Console.WriteLine("File WorkflowProgressNodeName.resx exists. Overriding file...");
            else
                Console.WriteLine("Created WorkflowProgressNodeName.resx");

            //doc.Save("test/WorkflowProgressNodeName.resx");
            doc.Save(rootDir + @"\webapp\App_GlobalResources\WorkflowProgressNodeName.resx");
            progress++;

            Console.WriteLine("---------------------------------------------------------------------------");
        }

        /*static void MergeFiles()
        {
            string[,] mergeSrcDest =
            {
                {"Strings.resx", @"webapp\App_GlobalResources\Strings.resx" }
            };
            
            for (int i = 0; i < mergeSrcDest.GetLength(0); i++)
            {
                string v9File = @"merge\v9\" + mergeSrcDest[i, 0], v8File = @"merge\v8\" + mergeSrcDest[i, 0], dest = rootDir + mergeSrcDest[i, 1];
                string v9Content = File.ReadAllText(v9File), v8Content = File.ReadAllText(v8File), destContent = File.ReadAllText(dest);

                diff_match_patch dmp = new diff_match_patch();

                //Difference between original v8 and v9 content
                List<Diff> diff = dmp.diff_main(v8Content, v9Content);

                dmp.diff_cleanupSemantic(diff);

                Console.WriteLine("There are " + diff.Count + " differences");

                //Create patches to update destination v8 file which may contain other changes from original v8
                List<Patch> patches = dmp.patch_make(destContent, diff);

                //Apply patches
                Object[] obj = dmp.patch_apply(patches, destContent);
                
                //Check if patches are applied successfully
                bool[] bArr = (bool []) obj[1];
                foreach (bool b in bArr)
                    Console.WriteLine(b);

                //Saves the new file
                string outfile = @"merge\new\" + mergeSrcDest[i, 0];
                File.WriteAllText(outfile, obj[0].ToString());
            }
        }*/

        static void MergeFiles()
        {
            string[,] mergeSrcDest =
            {
                {"Strings.resx", @"\webapp\App_GlobalResources\Strings.resx" },
                {"SkinFile.skin", @"\webapp\App_Themes\Corporate\SkinFile.skin" },
                {"OApplicationSetting.cs", @"\LogicLayer\DefaultObjects\AdminSystem\OApplicationSetting.cs" },
                {"OFunction.cs", @"\LogicLayer\DefaultObjects\AdminSystem\OFunction.cs" },
                {"OUser.cs", @"\LogicLayer\DefaultObjects\AdminSystem\OUser.cs" },
                {"TablesLogic.cs", @"\LogicLayer\DefaultObjects\AdminSystem\TablesLogic.cs" },
                {"OActivity.cs", @"\LogicLayer\DefaultObjects\Workflow\OActivity.cs" },
                {"Helpers.cs", @"\webapp\App_Code\components\Helpers.cs" },
                {"PageBase.cs", @"\webapp\App_Code\components\PageBase.cs" },
                {"Window.cs", @"\webapp\App_Code\components\Window.cs" },
                {"ODashboard.cs", @"\LogicLayer\DefaultObjects\Dashboard\ODashboard.cs" },
                {"LogicLayer.csproj", @"\LogicLayer\LogicLayer.csproj" },
                {"Analysis.cs", @"\webapp\App_Code\components\Analysis.cs" },
                {"AppGlobal.cs", @"\webapp\App_Code\components\AppGlobal.cs" },
                {"v7-chart.css", @"\webapp\css\v7-chart.css" },
                {"v7-dragdrop.css", @"\webapp\css\v7-dragdrop.css" },
                {"StyleSheet.css", @"\webapp\App_Themes\Corporate\StyleSheet.css" },
                {"apptop.aspx", @"\webapp\apptop.aspx" },
            };

            Console.WriteLine("Merging changes...");
            for (int i = 0; i < mergeSrcDest.GetLength(0); i++)
            {
                string v9File = @"merge\v9\" + mergeSrcDest[i, 0], v8File = @"merge\v8\" + mergeSrcDest[i, 0], targetFile = rootDir + mergeSrcDest[i, 1];
                string v9Content = File.ReadAllText(v9File), v8Content = File.ReadAllText(v8File), destContent = File.ReadAllText(targetFile);

                //Copying the current project file
                string fileName = System.IO.Path.GetFileName(targetFile);
                string destFile = System.IO.Path.Combine(@"merge\curr\", fileName);
                Console.Write("Merging changes in " + fileName + " to " + destFile + "... ");
                if (!Directory.Exists(@"merge\curr\"))
                    Directory.CreateDirectory(@"merge\curr\");
                System.IO.File.Copy(targetFile, destFile, true);

                //Console.WriteLine("Gonna start");

                System.Diagnostics.Process p = new System.Diagnostics.Process();

                System.Diagnostics.ProcessStartInfo info = new System.Diagnostics.ProcessStartInfo();
                info.UseShellExecute = false;
                info.RedirectStandardOutput = true;
                info.FileName = @"bin\diff3";
                info.Arguments = v9File + " " + v8File + " " + destFile + " -m";
                
                p.StartInfo = info;
                //Console.WriteLine(p.StartInfo.FileName + " " + p.StartInfo.Arguments);
                p.Start();
                string output = p.StandardOutput.ReadToEnd();
                p.WaitForExit();

                int conflict = p.ExitCode;
                if (conflict != 0)
                    Console.WriteLine("Conflict detected with code " + conflict);
                else
                    Console.WriteLine("Done");

                if (!Directory.Exists(@"merge\new\"))
                    Directory.CreateDirectory(@"merge\new\");

                File.WriteAllText(@"merge\new\" + mergeSrcDest[i, 0], output);

                if (conflict == 0)
                    File.WriteAllText(rootDir + mergeSrcDest[i, 1], output);
                else
                    File.WriteAllText(rootDir + mergeSrcDest[i, 1] + ".mergeconflict", output);

                progress++;
            }
            Console.WriteLine("---------------------------------------------------------------------------");
        }

        static void UpdateSchema()
        {
            Console.WriteLine("Updating database schema...");
            string sqlConnectionString = $"Data Source={dbServer};Initial Catalog={dbName};User ID={dbUserID};Password={dbPw}";

            SqlConnection conn = new SqlConnection(sqlConnectionString);

            Server server = new Server(new ServerConnection(conn));

            Console.Write("Reading from database_v9.sql... ");
            string script = File.ReadAllText(@"database_v9.sql");
            Console.WriteLine("Done");

            Console.Write("Applying schema this may take awhile... ");
            server.ConnectionContext.ExecuteNonQuery(script);
            Console.WriteLine("Done");
            /*SqlConnection cnn;
            cnn = new SqlConnection(sqlConnectionString);
            cnn.Open();
            Console.WriteLine("Connection Open  !");

            string query = "select * from UserBase where LoginName = 'alex.lau';";
            SqlCommand command = new SqlCommand(query, cnn);
            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                Console.WriteLine(reader["Email"].ToString() + " ");
            }

            reader.Close();
            command.Dispose();
            cnn.Close();*/
        }

        static bool UpdateWebConfig()
        {
            string webconfigFile = rootDir + @"\webapp\Web.config";

            Console.Write("Updating Web.config... ");

            var doc = new HtmlDocument();
            doc.Load(webconfigFile);
            doc.OptionOutputOriginalCase = true;
            
            bool changed = false;

            var ajaxNode = doc.DocumentNode.SelectSingleNode("//ajax");

            ajaxNode.SetAttributeValue("updateProgressContent", "[div class='loading-container1'][/div][div class='loading-container2'][div class='loading'][div class='loading-bar'][/div][div class='loading-bar'][/div][div class='loading-bar'][/div][div class='loading-bar'][/div][/div][/div]");
            ajaxNode.SetAttributeValue("updateProgressImage", "");

            /*string[,] keysVal =
            {
                { "ImageUrl_FixedRateBook", "~/images/tv-otype.png" },
                { "ImageUrl_FixedRateLogical", "~/images/tv-ogroup.png" },
                { "ImageUrl_FixedRatePhysical", "~/images/tv-otype.png" },
                { "ImageUrl_ChecklistLogical", "~/images/tv-ogroup.png" },
                { "ImageUrl_ChecklistPhysical", "~/images/tv-otype.png" },
                { "ImageUrl_LocationLogical", "~/images/tv-ogroup.png" },
                { "ImageUrl_LocationPhysical", "~/images/tv-location.png" },
                { "ImageUrl_EquipmentLogical", "~/images/tv-ogroup.png" },
                { "ImageUrl_EquipmentPhysical", "~/images/tv-otype.png" },
                { "ImageUrl_ObjectGroup", "~/images/tv-ogroup.png" },
                { "ImageUrl_ObjectType", "~/images/tv-otype.png" },
                { "ImageUrl_TaskFolder", "~/images/folder.png" },
                { "ImageUrl_TaskItem", "~/images/document_add.png" },
                { "ImageUrl_PointTriggerLogical", "~/images/tv-ogroup.png" },
                { "ImageUrl_PointTriggerPhysical", "~/images/tv-otype.png" }
            };
            var add = doc.DocumentNode.SelectSingleNode("//add[@key='ImageUrl_FixedRateBook']");

            Console.WriteLine(add.OuterHtml);*/

            var docv9 = new HtmlDocument();
            docv9.Load(@"test\Web.config");
            docv9.OptionOutputOriginalCase = true;

            string[] keys =
            {
                "ImageUrl_FixedRateBook", "ImageUrl_FixedRateLogical", "ImageUrl_FixedRatePhysical", "ImageUrl_ChecklistLogical", "ImageUrl_ChecklistPhysical",
                "ImageUrl_LocationLogical", "ImageUrl_LocationPhysical", "ImageUrl_EquipmentLogical", "ImageUrl_EquipmentPhysical", "ImageUrl_ObjectGroup",
                "ImageUrl_ObjectType", "ImageUrl_TaskFolder", "ImageUrl_TaskItem", "ImageUrl_PointTriggerLogical", "ImageUrl_PointTriggerPhysical"
            };

            foreach (string key in keys)
            {
                var addNodev9 = docv9.DocumentNode.SelectSingleNode("//add[@key='"+ key + @"']");
                var addNode = doc.DocumentNode.SelectSingleNode("//add[@key='" + key + @"']");

                if (addNodev9 != null && addNode != null && addNodev9.Attributes["value"] != null)
                {
                    //Console.WriteLine(addNode.Attributes["value"].Value + "\t" + addNodev9.Attributes["value"].Value);
                    addNode.SetAttributeValue("value", addNodev9.Attributes["value"].Value);
                }
            }

            doc.Save(webconfigFile);

            string contents = File.ReadAllText(webconfigFile);
            contents = contents.Replace("<clear></clear>", "<clear/>");
            File.WriteAllText(webconfigFile, contents);

            progress++;

            Console.WriteLine("Done");
            Console.WriteLine("---------------------------------------------------------------------------");
            return changed;
        }
    }
}
