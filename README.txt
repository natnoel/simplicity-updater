1. Copy the contents of run folder into bin/Debug (the location of ‘Simplicity Update.exe’)
2. In Program.cs change the value of rootDir to the location of your Web Application (eg. ‘C:\Projects\abell.80-ForNYP’ or ‘C:\Projects\Simplicity\Simplicity\Web Application’)
3. Set the database server, name, user ID and password into the variables dbServer, dbName, dbUserID, dbPw
4. Run the program
